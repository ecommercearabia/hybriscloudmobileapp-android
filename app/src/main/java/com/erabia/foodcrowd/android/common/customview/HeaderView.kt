package com.erabia.foodcrowd.android.common.customview

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.erabia.foodcrowd.android.R


class HeaderView : FrameLayout {

    private var imageViewStartImage: ImageView? = null
    private var imageViewArrow: ImageView? = null
    private var textViewTitle: TextView? = null
    private var container: ConstraintLayout? = null
    var srcImage: Drawable? = null
        set(value) {
            field = value
            imageViewStartImage?.background = srcImage
            postInvalidate()
        }
    var titleColor: Int? = null
        set(value) {
            field = value
            textViewTitle?.setTextColor(titleColor ?: R.color.app_color)
            postInvalidate()
        }
    var titleText: String? = null
        set(value) {
            field = value
            textViewTitle?.text = titleText
            postInvalidate()
        }
    var isExpanded: Boolean = false
        set(value) {
            field = value
            if (isExpanded) {
                imageViewArrow?.background = context.getDrawable(R.drawable.ic_arrow_drop_down)
            } else {
                imageViewArrow?.background = context.getDrawable(R.drawable.ic_arrow_drop_right)
            }
            postInvalidate()
        }
    var isHeaderViewEnabled: Boolean = false
        set(value) {
            field = value
//            container?.isClickable = !value
//            container?.isFocusable = !value
//            postInvalidate()
        }

    constructor(context: Context) : super(context) {
        prepareView(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        prepareView(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        prepareView(context, attrs)
    }

    private fun prepareView(context: Context, attrs: AttributeSet?) {
        if (attrs == null) return

        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.HeaderView, 0, 0
        )

        titleText = a.getString(R.styleable.HeaderView_titleText)
        var defaultTitleColor: Int
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            defaultTitleColor = context.resources.getColor(R.color.blackText, null)
        } else {
            defaultTitleColor = context.resources.getColor(R.color.blackText)
        }
        titleColor =
            a.getColor(
                R.styleable.HeaderView_titleColor,
                defaultTitleColor
            )

        srcImage = a.getDrawable(R.styleable.HeaderView_srcImage)
        isHeaderViewEnabled = a.getBoolean(R.styleable.HeaderView_isHeaderViewEnabled, false)

        a.recycle()

        val inflater = context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.merg_header_layout, this, true)
        imageViewArrow = findViewById(R.id.imageView_arrow)
        textViewTitle = findViewById(R.id.text_view_header)
        imageViewStartImage = findViewById(R.id.imageView_start)
        container = findViewById(R.id.headerViewContainer)
        textViewTitle?.text = titleText
        textViewTitle?.setTextColor(titleColor ?: R.color.app_color)
        imageViewStartImage?.background = srcImage
        imageViewArrow?.background = context.getDrawable(R.drawable.ic_arrow_drop_right)
//        container?.isClickable = !isHeaderViewEnabled
//        container?.isFocusable = !isHeaderViewEnabled

    }

}