package com.erabia.foodcrowd.android.checkout.ccavenue

import android.os.Parcel
import android.os.Parcelable

data class CCAvenueBillingAddress(
    var name: String? = null,
    var address: String? = null,
    var city: String? = null,
    var state: String? = null,
    var country: String? = null,
    var telephone: String? = null,
    var email: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(country)
        parcel.writeString(telephone)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CCAvenueBillingAddress> {
        override fun createFromParcel(parcel: Parcel): CCAvenueBillingAddress {
            return CCAvenueBillingAddress(parcel)
        }

        override fun newArray(size: Int): Array<CCAvenueBillingAddress?> {
            return arrayOfNulls(size)
        }
    }
}