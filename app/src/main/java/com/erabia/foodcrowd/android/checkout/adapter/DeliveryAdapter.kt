package com.erabia.foodcrowd.android.checkout.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.model.DeliveryModeType
import com.erabia.foodcrowd.android.databinding.CheckoutDeliveryTimeRowItemBinding
import com.erabia.foodcrowd.android.databinding.RowDeliveryModeTypesBinding
import kotlinx.android.synthetic.main.checkout_delivery_time_row_item.view.*

class DeliveryAdapter (val viewModel: CheckoutViewModel) :
    RecyclerView.Adapter<DeliveryAdapter.MyViewHolder>(){
    private var deliveryModeType: DeliveryModeType? = null
    var binding: RowDeliveryModeTypesBinding? = null

    private var deliveryTimeBinding: CheckoutDeliveryTimeRowItemBinding? = null


    var cart: Cart? = null
        set(value) {
            field = value
            deliveryTimeBinding?.cart = value
        }

    //preSelect deliveryMode from cart repsonse if exist
    var preSelectedDeliveryMode: String = ""
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var deliveryModeTypeList: List<DeliveryModeType>? = null
        set(value) {
            field = value
            selectedPosition = -1
            notifyDataSetChanged()
        }

    lateinit var context: Context
    var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_delivery_mode_types,
            parent,
            false
        )
        return MyViewHolder(binding!!)
    }

    override fun getItemCount(): Int {
        return deliveryModeTypeList?.size ?: 0
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val deliveryMode = deliveryModeTypeList?.get(position)
        holder.itemBinding.checkoutVM = viewModel


        //PreSelected from cart api response
        if (preSelectedDeliveryMode == deliveryMode?.code) {
            holder.itemBinding.radioButton.isChecked = preSelectedDeliveryMode == deliveryMode?.code
            selectedPosition = position
        }

        holder.itemBinding.deliveryModeType = deliveryMode
        holder.itemBinding.radioButton.isChecked = selectedPosition == position

        holder.itemBinding.radioButton.setOnClickListener {
//            Toast.makeText(context,"hiii there",Toast.LENGTH_SHORT).show()
            viewModel.setDeliveryTypes(
                "", "",
                deliveryModeTypeList?.get(position)?.code ?: "",
                Constants.FIELDS_FULL
            )
            selectedPosition = position
            deliveryModeType = deliveryModeTypeList?.get(position)
            notifyDataSetChanged()
        }
    }

    class MyViewHolder(var itemBinding: RowDeliveryModeTypesBinding) :
        RecyclerView.ViewHolder(itemBinding.root)
}