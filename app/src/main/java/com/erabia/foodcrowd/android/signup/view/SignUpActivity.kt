package com.erabia.foodcrowd.android.signup.view

//import com.erabia.foodcrowd.android.databinding.FragmentSignUpBinding
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivitySignUpBinding
import com.erabia.foodcrowd.android.home.remote.service.LoginService
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.otp.view.OtpActivity
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import com.erabia.foodcrowd.android.signup.viewmodel.SignUpViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    companion object {
        const val TAG = "SignUpActivity"
    }

    private lateinit var viewModel: SignUpViewModel
    var compositeDisposable = CompositeDisposable()
    var mIsoCode: String = ""
    var mTitleCode: String = ""
    var mNationalityCode: String = ""
    private lateinit var binding: ActivitySignUpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up)

        val factory = SignUpViewModel.Factory(
            SignUpRepository(
                RequestManager.getClient().create(SignUpService::class.java)
            ), LoginRepository(
                RequestManager.getClient().create(LoginService::class.java),
                RequestManager.getClient().create(CartService::class.java)
            )
        )
        viewModel = ViewModelProvider(this, factory).get(SignUpViewModel::class.java)
        binding.lifecycleOwner = this
        binding.signUpViewModel = viewModel



        val redString = resources.getString(R.string.by_clicking_create_account)
        textView3.text = Html.fromHtml(redString)

        textView3.makeLinks(
            Pair(resources.getString(R.string.terms_of_use), View.OnClickListener {
                val intent = Intent(this, TermsAndPrivacyWebViewActivity::class.java)
                intent.putExtra("url", getString(R.string.terms_url))
                startActivity(intent)

            }),
            Pair(resources.getString(R.string.privacy_policys), View.OnClickListener {
                val intent = Intent(this, TermsAndPrivacyWebViewActivity::class.java)
                intent.putExtra("url", getString(R.string.privacy_url))
                startActivity(intent)


            })

        )
        observerVaildation()
        observerRegistration()
        observerNationalityFlagConfig()

        viewModel.onBackImageObserver.observe(this, Observer {
            finish()
        })
        viewModel.countryCodeList.observe(this, Observer {
            it.countries?.let { it1 -> DialogData.getRegisterCountryCode(this, it1, viewModel) }
        })

        viewModel.isoCodeLiveData.observe(this, Observer {
            mIsoCode = it
        })
        viewModel.countryCodeLiveData.observe(this, Observer {
            binding.mCodeTextEditText.setText(it)
        })
        viewModel.selectedCountry.observe(this, Observer {
            binding?.mCodeTextEditText?.setText(it.isdcode)
            mIsoCode = it.isocode.toString()
            Log.d(
                TAG,
                "onCreate: countryName ${it.name} countryISO ${it.isocode} countryISD ${it.isdcode}"
            )
        })

        viewModel.mTitleCode.observe(this, Observer {
            mTitleCode = it
        })

        viewModel.getNationalitiesConfig()
        if (SharedPreference.getInstance().getNationalityEnabled()) {
            viewModel.getNationalitiesList()
            binding.mNationalitiesSpinner.visibility = View.VISIBLE
        } else{
            binding.mNationalitiesSpinner.visibility = View.GONE
        }

            viewModel.mNationalityCode.observe(this, Observer {
            mNationalityCode = it
        })

        viewModel.mValidationSuccess.observe(this, Observer {
            var intent = Intent(this, OtpActivity::class.java)
            Log.d(TAG, "onCreate: countryisoCode $mIsoCode")
            intent.putExtra("mobile_number", textView_mobileNumber.text.toString())
            intent.putExtra("countryisoCode", mIsoCode)
            intent.putExtra("first_name", mFirstNameEditText.text.toString())
            intent.putExtra("referral_code", mReferralCodeEditText.text.toString())
            intent.putExtra("last_name", mLastNameEditText.text.toString())
            intent.putExtra("email", mEmailEditText.text.toString())
            intent.putExtra("password", mPasswordEditText.text.toString())
            intent.putExtra("title", mTitleCode)
            intent.putExtra("nationality", mNationalityCode)
            startActivityForResult(intent, 99)
        })


    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()

        }
    }

    private fun observerNationalityFlagConfig() {
        viewModel.mSignupNationalityFlag.observe(this, Observer {
            Log.d("teem Nationalities flag", it.toString())
            if(it == true){
                viewModel.getNationalitiesList()
                binding.mNationalitiesSpinner.visibility = View.VISIBLE
            }
            else{
                binding.mNationalitiesSpinner.visibility = View.GONE
            }
        })
    }

    private fun observerRegistration() {
        viewModel.mRegisterLiveData.observe(this, Observer {
            AppUtil.showToastySuccess(this, R.string.success)

            viewModel.doLogin()
        })

        viewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)

        })

        viewModel.mLoginLiveData.observe(this, Observer {
            Log.e("mmmmmmmmm", it.refreshToken ?: "")
            SharedPreference.getInstance().setRefreshToken(it.refreshToken ?: "")
            SharedPreference.getInstance().setUser_TOKEN(it.accessToken ?: "")
            SharedPreference.getInstance().setApp_TOKEN(it.accessToken ?: "")
//            SharedPreference.getInstance().setLOGIN_User(mEmailEditText.text.toString())
            SharedPreference.getInstance().setLOGIN_EMAIL(mEmailEditText.text.toString())

            startActivity(Intent(this, OtpActivity::class.java))
            finish()

//            NavHostFragment.findNavController(this)
//                .navigate(R.id.action_signUpFragment_to_otpFragment)
        })

    }

    private fun observerVaildation() {
        viewModel.mErrorFirstNameValidationMessage.observe(this, Observer {
            mFirstNameEditText.error = it
        })

        viewModel.mErrorLastNameValidationMessage.observe(this, Observer {
            mLastNameEditText.error = it
        })

        viewModel.mErrorEmailValidationMessage.observe(this, Observer {
            mEmailEditText.error = it
        })


        viewModel.mErrorCodeNumberValidationMessage.observe(this, Observer {
            mCodeTextEditText.error = it
        })

        viewModel.mErrorMobileNumberValidationMessage.observe(this, Observer {
            textView_mobileNumber.error = it
        })

        viewModel.mErrorPasswordValidationMessage.observe(this, Observer {
            mPasswordEditText.error = it
        })

        viewModel.mErrorConfirmPasswordValidationMessage.observe(this, Observer {
            mConfirmPasswordEditText.error = it
        })

        viewModel.mErrorTermsValidationMessage.observe(this, Observer {

            AppUtil.showToastyWarning(this ,R.string.you_should_accept_terms)

        })
    }

    fun TextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(this.text)
        var startIndexOfLink = -1
        for (link in links) {
            val clickableSpan = object : ClickableSpan() {
                override fun updateDrawState(textPaint: TextPaint) {
                    // use this to change the link color
                    textPaint.color = textPaint.linkColor
                    // toggle below value to enable/disable
                    // the underline shown below the clickable text
                    textPaint.isUnderlineText = true
                }

                override fun onClick(view: View) {
                    Selection.setSelection((view as TextView).text as Spannable, 0)
                    view.invalidate()
                    link.second.onClick(view)
                }
            }
            startIndexOfLink = this.text.toString().indexOf(link.first, startIndexOfLink + 1)
//      if(startIndexOfLink == -1) continue // todo if you want to verify your texts contains links text
            spannableString.setSpan(
                clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        this.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        this.setText(spannableString, TextView.BufferType.SPANNABLE)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}