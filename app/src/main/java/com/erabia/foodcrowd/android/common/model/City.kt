package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class City(
    @SerializedName("areas")
    var areas: List<Area>? = null,
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("name")
    var name: String? = null
) {
    fun getCityName(countryList: List<City>): List<String> {
        var cityNameList: MutableList<String> = ArrayList()
        for (i in 0 until countryList.size) {
            cityNameList.add(countryList.get(i).name ?: "")
        }
        return cityNameList
    }

    fun findCityByName(cityList: List<City>?, name: String): String? {
        return cityList?.filter { it.name == name }?.firstOrNull()?.code
    }

    fun areaList(cityList: List<City>?, name: String): List<Area>? {
        return cityList?.filter { it.name == name }?.firstOrNull()?.areas
    }
}