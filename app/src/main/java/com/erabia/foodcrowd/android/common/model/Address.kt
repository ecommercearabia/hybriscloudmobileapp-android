package com.erabia.foodcrowd.android.common.model


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


data class Address(
    @SerializedName("addressName")
    var addressName: String? = null,

    @SerializedName("buildingName")
    var buildingName: String? = "",
    @SerializedName("apartmentNumber")
    var apartmentNumber: String? = "",
    @SerializedName("streetName")
    var streetName: String? = "",

    @SerializedName("area")
    var area: Area? = null,
    @SerializedName("cellphone")
    var cellphone: String? = null,
    @SerializedName("city")
    var city: City? = null,
    @SerializedName("companyName")
    var companyName: String? = null,
    @SerializedName("country")
    var country: Country? = null,
    @SerializedName("defaultAddress")
    var defaultAddress: Boolean? = null,
    @SerializedName("district")
    var district: String? = null,
    @SerializedName("email")
    var email: String? = null,
    @SerializedName("firstName")
    var firstName: String? = null,
    @SerializedName("formattedAddress")
    var formattedAddress: String? = null,
    @SerializedName("id")
    var id: String = "",
    @SerializedName("lastName")
    var lastName: String? = null,
    @SerializedName("line1")
    var line1: String? = null,
    @SerializedName("line2")
    var line2: String? = null,
    @SerializedName("mobileCountry")
    var mobileCountry: MobileCountry? = null,
    @SerializedName("mobileNumber")
    var mobileNumber: String? = null,
    @SerializedName("nearestLandmark")
    var nearestLandmark: String? = null,
    @SerializedName("phone")
    var phone: String? = null,
    @SerializedName("postalCode")
    var postalCode: String? = null,
    @SerializedName("region")
    var region: Region? = null,
    @SerializedName("shippingAddress")
    var shippingAddress: Boolean? = null,
    @SerializedName("title")
    var title: String? = null,
    @SerializedName("titleCode")
    var titleCode: String? = null,
    @SerializedName("town")
    var town: String? = null,
    @SerializedName("visibleInAddressBook")
    var visibleInAddressBook: Boolean? = null
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("area"),
        parcel.readString(),
        TODO("city"),
        parcel.readString(),
        TODO("country"),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()?:"",
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("mobileCountry"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        TODO("region"),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(addressName)
        parcel.writeString(buildingName)
        parcel.writeString(apartmentNumber)
        parcel.writeString(streetName)
        parcel.writeString(cellphone)
        parcel.writeString(companyName)
        parcel.writeValue(defaultAddress)
        parcel.writeString(district)
        parcel.writeString(email)
        parcel.writeString(firstName)
        parcel.writeString(formattedAddress)
        parcel.writeString(id)
        parcel.writeString(lastName)
        parcel.writeString(line1)
        parcel.writeString(line2)
        parcel.writeString(mobileNumber)
        parcel.writeString(nearestLandmark)
        parcel.writeString(phone)
        parcel.writeString(postalCode)
        parcel.writeValue(shippingAddress)
        parcel.writeString(title)
        parcel.writeString(titleCode)
        parcel.writeString(town)
        parcel.writeValue(visibleInAddressBook)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Address> {
        override fun createFromParcel(parcel: Parcel): Address {
            return Address(parcel)
        }

        override fun newArray(size: Int): Array<Address?> {
            return arrayOfNulls(size)
        }
    }
}