package com.erabia.foodcrowd.android.storecredit.model

import retrofit2.Response

sealed class SuperResponse {

    data class SuccessAval(val data: Response<AvailableBalanceResponse>) : SuperResponse()
    data class SuccessStore(val data: Response<StoreCreditListResponse>) : SuperResponse()
    data class Error(val t: Throwable) : SuperResponse()
}