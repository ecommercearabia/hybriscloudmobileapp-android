package com.erabia.foodcrowd.android.checkout.model

data class PaymentInfo(
    val accessCode: String = "",
    val amount: String = "",
    val cancelUrl: String = "",
    val currency: String = "",
    val customerIdentifier: String = "",
    val deliveryAddress: String = "",
    val deliveryCity: String = "",
    val deliveryCountry: String = "",
    val deliveryName: String = "",
    val deliveryState: String = "",
    val deliveryZip: String = "",
    val deliveryTel: String = "",
    val integrationType: String = "",
    val language: String = "",
    val merchantId: String = "",
    val merchantParam1: String = "",
    val merchantParam2: String = "",
    val merchantParam3: String = "",
    val merchantParam4: String = "",
    val orderId: String = "",
    val promoCode: String = "",
    val redirectUrl: String = "",
    val workingKey: String = ""
)