package com.erabia.foodcrowd.android.category.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.model.Subcategory
import com.erabia.foodcrowd.android.category.viewmodel.CategoryViewModel
import com.erabia.foodcrowd.android.databinding.SubCategoryRowBinding


class SubCategoryAdapter(var categoryViewModel: CategoryViewModel) :
    RecyclerView.Adapter<SubCategoryAdapter.ViewHolder>() {
    lateinit var itemBinding: SubCategoryRowBinding
    var subCategoryList: List<Subcategory>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        itemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.sub_category_row, parent, false
        )
        return ViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return subCategoryList?.size ?: 0

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.category = subCategoryList?.get(position)
        holder.itemBinding.categoryVM = categoryViewModel
    }


    override fun getItemId(position: Int): Long {
        return subCategoryList?.get(position)?.id?.toLong() ?: 0

    }

    class ViewHolder(val itemBinding: SubCategoryRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root)
}