package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("url")
    val url: String = ""
)