package com.erabia.foodcrowd.android.order.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.FragmentDetailsOrderBinding
import com.erabia.foodcrowd.android.databinding.FragmentOrderItemBinding
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.disposables.CompositeDisposable


class OrderDetailsFragment(mOrderDetailsResponse:OrderDetailsResponse) : Fragment() {
    var mDetailsResponse=mOrderDetailsResponse

    lateinit var mViewModel: MyOrderViewModel
    var compositeDisposable = CompositeDisposable()
    lateinit var binding: FragmentDetailsOrderBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = MyOrderViewModel.Factory(
            MyOrderRepository(
                RequestManager.getClient().create(MyOrderService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(MyOrderViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        val view =inflater.inflate(R.layout.fragment_details_order, container, false)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details_order, container, false)
        binding.lifecycleOwner = this
        binding.mMyOrderViewModel = mViewModel
        binding.orderDetails =  mDetailsResponse

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        binding.mDatePlacedValueTextView.text=  AppUtil.formatDate(
//            "yyyy-MM-dd'T'HH:mm:ssZ",
//            "MMM dd, yyyy HH:mm a",(activity as AppCompatActivity?)!!.intent.getStringExtra("date_placed")?:"")

    }
}