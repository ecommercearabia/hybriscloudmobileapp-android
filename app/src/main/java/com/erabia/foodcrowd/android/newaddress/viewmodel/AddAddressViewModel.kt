package com.erabia.foodcrowd.android.newaddress.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.newaddress.model.*
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import io.reactivex.disposables.CompositeDisposable

class AddAddressViewModel(val mAddressRepository: AddressRepository) : ViewModel() {
    companion object {
        const val TAG = "AddAddressViewModel"
    }

    val loadingVisibility: MutableLiveData<Int> = mAddressRepository.loadingVisibility
    var compositeDisposable = CompositeDisposable()
    var mCountryListLiveData: LiveData<List<CountryResponse.Country>> = mAddressRepository.mCountryListLiveData
    val mMobileCountryListLiveData: LiveData<List<MobileCountryResponse.Country>> =
        mAddressRepository.mMobileCountryListLiveData
    val mCityListLiveData: LiveData<CityListResponse> = mAddressRepository.mCityListLiveData
    val mErrorEvent: LiveData<String> = mAddressRepository.mErrorEvent
    val mAreaListLiveData: LiveData<AreaListResponse> = mAddressRepository.mAreaListLiveData
    val areaListForPickerLiveData = MutableLiveData<AreaListResponse?>()
    val mTitleListLiveData: LiveData<TitleResponse> = mAddressRepository.mTitleListLiveData
    val mPersonalDetailsLiveData: LiveData<PersonalDetailsModel> =
        mAddressRepository.mPersonalDetailsLiveData

    val mAddAddressLiveEvent: LiveData<Address> = mAddressRepository.mAddAdressLiveEvent


    val mErrorAreaAndCreateEvent: SingleLiveEvent<String> = mAddressRepository.error()
    val resetAndHideAdapter: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    var mTitleCode = ""
    var mTitleName = ""
    var mCitySelection = CityListResponse.City()
    var mCountrySelection = CountryResponse.Country()
    val mAreaSelection: MutableLiveData<AreaListResponse.Area> by lazy { MutableLiveData<AreaListResponse.Area>() }

    var mMobileCodeSelection = MobileCountryResponse.Country()
    var mMobileNumber: String = ""
    var mLineOne: String = ""
    var mLineTwo: String = ""
    var mAddressName: String = ""
    var mNearestLand: String = ""
    var mStreetNumber: String = ""
    var mBuildingNumber: String = ""
    var mApartmentNumber: String = ""
    var mDefaultAddressChecked: Boolean = false
    var mComeFrom: String = ""
    var mNavigateToMap = SingleLiveEvent<Int>()

    var mCityFromMap: String = ""
    var mAreaFromMap: String = ""

    var area = ""

    //this flag to know if we get the data from map for the first time or not
    var isFirstTimeSelectedCity = true
    var isFirstTimeSelectedArea = true

    val mEnableButton: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    fun getAllDataSpinner() {
        mAddressRepository.getAllDataSpinnerForAddAddress()
    }

    fun setAdminArea(adminArea: String) {
        mLineOne = adminArea
        mStreetNumber = adminArea
    }


    fun setCityAndAreaFromMap(city: String, area: String) {
        mCityFromMap = city
        //    mAreaFromMap = ""
    }


    fun onSelectItemTitle(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {

        mTitleCode =
            (pos?.let { it1 -> parent?.adapter?.getItem(it1) } as TitleResponse.Title).code.toString()
        mTitleName =
            (pos.let { it1 -> parent?.adapter?.getItem(it1) } as TitleResponse.Title).name.toString()
    }


    fun onSelectItemCountry(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        mCountrySelection = parent?.adapter?.getItem(pos) as CountryResponse.Country

        if (!mCountrySelection.isocode.isNullOrEmpty())
            mCountrySelection.isocode?.let { mAddressRepository.getCityListAndArea(it) }
    }

    fun onSelectItemCountry2() {
        mCountrySelection.isocode = "AE"
        if (!mCountrySelection.isocode.isNullOrEmpty())
            mCountrySelection.isocode?.let { mAddressRepository.getCityListAndArea(it) }
    }


    fun onSelectItemCity(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {

        if (isFirstTimeSelectedCity) {
            val position =
                mCityListLiveData.value?.cities?.indexOfFirst { city -> city?.name == mCityFromMap }
            if (position != null) {
                if (position != -1) {
                    isFirstTimeSelectedCity = false
                    spinner?.setSelection(position ?: 0)
                    mCitySelection =
                        parent?.adapter?.getItem(position ?: 0) as CityListResponse.City
                    val area =
                        (parent?.adapter?.getItem(position) as CityListResponse.City).areas?.first()
                    mAreaSelection.postValue(AreaListResponse.Area(area?.code, area?.name))
                } else {
                    isFirstTimeSelectedCity = false
                    spinner?.setSelection(0)
                    mCitySelection =
                        parent?.adapter?.getItem(0) as CityListResponse.City
                    val area = (parent?.adapter?.getItem(0) as CityListResponse.City).areas?.first()
                    mAreaSelection.postValue(AreaListResponse.Area(area?.code, area?.name))
                }
            } else {
                isFirstTimeSelectedCity = true

            }
        } else {

            mCitySelection = parent?.adapter?.getItem(pos) as CityListResponse.City
            val area = (parent?.adapter?.getItem(pos) as CityListResponse.City).areas?.first()
            mAreaSelection.postValue(AreaListResponse.Area(area?.code, area?.name))
            if (!mCitySelection.code.isNullOrEmpty())
                mCitySelection.code?.let {
                    mCountrySelection.isocode?.let { it1 ->
                        mAddressRepository.getArea(
                            it1, it
                        )
                    }
                }
            isFirstTimeSelectedArea = false
        }

    }

//    fun onSelectItemArea(
//        parent: AdapterView<*>?,
//        spinner: Spinner?,
//        pos: Int,
//        id: Long
//    ) {
//        try {
//
//            if (isFirstTimeSelectedArea) {
//                val position =
//                    mAreaListLiveData.value?.areas?.indexOfFirst { area -> area?.name == mAreaFromMap }
//
//                if (position != null) {
//                    if (position != -1) {
//                        isFirstTimeSelectedArea = false
//                        spinner?.setSelection(position ?: 0)
//                        mAreaSelection =
//                            parent?.adapter?.getItem(position ?: 0) as AreaListResponse.Area
//                    }else{
//                        isFirstTimeSelectedArea = false
//                        spinner?.setSelection(0)
//                        mAreaSelection =
//                            parent?.adapter?.getItem(0) as AreaListResponse.Area
//                    }
//                } else {
//                    isFirstTimeSelectedArea = true
//                }
//            } else {
//            Log.d(TAG, "onSelectItemArea: position ${pos}")
//            mAreaSelection = parent?.adapter?.getItem(pos) as AreaListResponse.Area
//            }
//        } catch (e: Exception) {
//            Log.d(TAG, "onSelectItemArea: error -> ${e.message}")
//        }
//
//    }

    fun onSelectMobileNumber(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        mMobileCodeSelection = parent?.adapter?.getItem(pos) as MobileCountryResponse.Country
    }

    fun onCheckedChange(button: CompoundButton?, check: Boolean) {
        Log.d("Z1D1", "onCheckedChange: $check")
        mDefaultAddressChecked = check
    }

    fun emptyText(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (mPersonalDetailsLiveData.value?.firstName.toString().isNotEmpty()
                    && mPersonalDetailsLiveData.value?.lastName.toString().isNotEmpty()
                    && mMobileNumber.isNotEmpty()
//                    && mLineOne.isNotEmpty()
                    && mAddressName.isNotEmpty()
                    && mNearestLand.isNotEmpty()
                    && mStreetNumber.isNotEmpty()
//                    && mBuildingNumber.isNotEmpty()
                    && mApartmentNumber.isNotEmpty()
                ) {
                    mEnableButton.postValue(true)

                } else {
                    mEnableButton.postValue(false)

                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }

    fun createAddress() {
        var address = AddressRequest()

        address.addressName = mAddressName
        address.apartmentNumber = mApartmentNumber
//        address.buildingName = mBuildingNumber
        address.defaultAddress = mDefaultAddressChecked
        address.firstName = mPersonalDetailsLiveData.value?.firstName
        address.lastName = mPersonalDetailsLiveData.value?.lastName
        address.line1 = mStreetNumber
//        address.line2 = mLineTwo
        address.mobileNumber = mMobileNumber
        address.nearestLandmark = mNearestLand
        address.streetName = mStreetNumber
        address.title = mTitleName
        address.titleCode = mTitleCode

        var area = AddressRequest.Area()
        area.code = mAreaSelection.value?.code
        area.name = mAreaSelection.value?.name

        address.city?.code = mCitySelection.code
        address.city?.name = mCitySelection.name
        address.city?.areas = mCitySelection.areas

        address.mobileCountry?.isdcode = mMobileCodeSelection.isdcode
        address.mobileCountry?.isocode = mMobileCodeSelection.isocode
        address.mobileCountry?.name = mMobileCodeSelection.name

        var country = AddressRequest.Country()
        country.name = mCountrySelection.name
        country.isocode = mCountrySelection.isocode
        country.isdcode = mCountrySelection.isdcode

        address.country = country
        address.city = address.city
        address.area = area
        address.mobileCountry = address.mobileCountry
        Log.d(TAG, "createAddress: area -> code : ${area.code} name : ${area.name}")
        mAddressRepository.createAddress(address, mComeFrom)
    }

    fun navigateToMap() {
        mNavigateToMap.postValue(1)
    }

    class Factory(
        val mAddressRepository: AddressRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AddAddressViewModel(mAddressRepository) as T
        }
    }


    fun areaListForPicker() {
        areaListForPickerLiveData.postValue(mAreaListLiveData.value)
    }

    fun onContainerLayoutClick() {
        resetAndHideAdapter.postValue(true)
    }
}