package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class AppliedVoucher(
    @SerializedName("appliedValue")
    val appliedValue: AppliedValue = AppliedValue(),
    @SerializedName("code")
    val code: String = "",
    @SerializedName("currency")
    val currency: Currency = Currency(),
    @SerializedName("description")
    val description: String = "",
    @SerializedName("freeShipping")
    val freeShipping: Boolean = false,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("value")
    val value: Double = 0.0,
    @SerializedName("valueFormatted")
    val valueFormatted: String = "",
    @SerializedName("valueString")
    val valueString: String = "",
    @SerializedName("voucherCode")
    val voucherCode: String = ""
)