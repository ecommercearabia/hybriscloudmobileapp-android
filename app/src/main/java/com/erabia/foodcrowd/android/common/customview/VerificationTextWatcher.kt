package com.erabia.foodcrowd.android.common.customview

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class VerificationTextWatcher(
    private val editTextNext: EditText,
    private val editTextPrevious: EditText
) :
    TextWatcher {

    override fun afterTextChanged(editable: Editable) {
        val text = editable.toString()
        if (text.length == 1) {
            editTextNext.requestFocus()
        } else if (text.length == 0) {
            editTextPrevious.requestFocus()
        }
    }

    override fun beforeTextChanged(
        arg0: CharSequence?,
        arg1: Int,
        arg2: Int,
        arg3: Int
    ) {
    }

    override fun onTextChanged(
        arg0: CharSequence?,
        arg1: Int,
        arg2: Int,
        arg3: Int
    ) {
    }
}