package com.erabia.foodcrowd.android.category.view

import android.content.Intent
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.adapter.CategoryAdapter
import com.erabia.foodcrowd.android.category.adapter.CategoryFilterAdapter
import com.erabia.foodcrowd.android.category.di.DaggerICategoryComponent
import com.erabia.foodcrowd.android.category.di.ICategoryComponent
import com.erabia.foodcrowd.android.category.model.Subcategory
import com.erabia.foodcrowd.android.category.viewmodel.CategoryViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.BreadCrumb
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.Sort
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentCategoryBinding
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.listing.adapter.ListingAdapter
import com.erabia.foodcrowd.android.listing.view.SortDialogFragment
import com.erabia.foodcrowd.android.login.view.LoginActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import java.net.URLDecoder
import javax.inject.Inject

class CategoryFragment : Fragment(), CategoryAdapter.IOnClick,
    DiscreteScrollView.OnItemChangedListener<CategoryAdapter.ViewHolder>,
    SortDialogFragment.IOnClick, ListingAdapter.IListingItemClick,
    CategoryFilterAdapter.OnFilterChecked, CategoryFilterAdapter.OnFilterUnChecked {

    companion object {
        private const val MARGIN_TIME: Int = 500
    }

    var products: List<Product>? = listOf()
    var totalResults = 0

    //check
    private var categoryFilterAdapter: CategoryFilterAdapter? = null
    private var mSpanCount: Int = 2
    private lateinit var layoutManager: GridLayoutManager
    private var organic: String = ":Organic:true"
    private var query: String = ""
    private val daggerComponent: ICategoryComponent by lazy {
        DaggerICategoryComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)
    }

    lateinit var categoryViewModel: CategoryViewModel

    @Inject
    lateinit var factory: CategoryViewModel.Factory

    var skipOrganicOnCheckedListener = true
    var skipGlutenFreeOnCheckedListener = true
    var skipVeganOnCheckedListener = true

    var skipDuplicateProductListObserver = true
    var categorySelected: CategoryAdapter.ViewHolder? = null
    lateinit var binding: FragmentCategoryBinding
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var wrapper: InfiniteScrollAdapter<*>
    private lateinit var listingAdapter: ListingAdapter
    private var facetList: List<Facet> = emptyList()
    private var breadCrumbList: List<BreadCrumb>? = emptyList()
    private var facetSwitch: ArrayList<String> = arrayListOf()
    private var sortList: List<Sort> = emptyList()
    var lastPage = 0
    var sortSelected = ""
    var isComeFromFilter = false
    private var categoryId: String = ""
    private var urlLink: String = ""
    private var filterQuery = ""
        set(value) {
            if (isComeFromFilter) {
                field = "$value"
            } else if (value.isNotEmpty() && !value.contains(":relevance")) {
                field = "$value"
            } else {
                field = value
            }
        }
    private var discreteItemsLoadedOnTheFirstTime = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        categoryViewModel = ViewModelProvider(this, factory).get(CategoryViewModel::class.java)
        if (categoryId.isNotEmpty()) {
            listingAdapter.onListingItemClick = null
            categoryViewModel.sort = sortSelected
            categoryViewModel.getProductList(
                categoryId,
                categoryViewModel.currentPage,
                getQuery(),
                true,
                "listing"
            )
        }

    }

    override fun onResume() {
        super.onResume()
//        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? MainActivity)?.setSupportActionBar((activity as? MainActivity)?.toolbar)
        (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as? MainActivity)?.toolbar?.setNavigationIcon(R.drawable.ic_sort_by)


    }

    var switchVeganVisiability = true
    var switchGlutenFreeVisiability = true
    var switchOrganicVisiability = true
    var isVegan = ""
    var isGlutenFree = ""
    var isOrganic = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        (activity as? MainActivity)?.toolbar?.setNavigationIcon(R.drawable.ic_sort_by)
        observeProgressBar()
        observeBadgeNumber()
        observeError()
        observeShowHint()
        observeCategory()
        observeProductList()
        observeAddToCart()
        observeSpanCount()
        observeBottomGroupLayoutVisibility()
//        buttonClicks()
        observeAddToWishlistEvent()
        observeStartLogin()
        pagination()
        observeListingNavigation()

        setFragmentResultListener(Constants.REQUEST_FILTER_KEY) { key, bundle ->
            categoryViewModel.currentPage = 0
            isComeFromFilter = true
            filterQuery = bundle.getString(Constants.FILTER_KEY) ?: ""
            if (filterQuery == ":" + sortSelected) {
                if (binding.switchOrganic.isChecked) {
                    skipOrganicOnCheckedListener = false
                    binding.switchOrganic.isChecked = false
                }
                if (binding.switchGlutenFree.isChecked) {
                    skipGlutenFreeOnCheckedListener = false
                    binding.switchGlutenFree.isChecked = false

                }
                if (binding.switchVegan.isChecked) {
                    skipVeganOnCheckedListener = false
                    binding.switchVegan.isChecked = false
                }
            }
            if (categoryId.isNotEmpty()) {
                categoryViewModel.sort = sortSelected
                categoryViewModel.getProductList(
                    categoryId,
                    categoryViewModel.currentPage,
                    getQuery(),
                    true,
                    "listing"
                )
            }
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (!this::binding.isInitialized) {
            binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_category, container, false)
            (activity as? MainActivity)?.setSupportActionBar((activity as? MainActivity)?.toolbar)
            (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            (activity as? MainActivity)?.toolbar?.setNavigationIcon(R.drawable.ic_sort_by)
            filterQuery = ""
//            "fromDetails",
//            bundleOf("isRefresh" to "true")
//            setFragmentResultListener("fromDetails") { key, bundle ->
//                val categoryId = bundle.getString("categoryId") ?: ""
//                if (categoryId.isNotEmpty()) {
//                    if (categoryViewModel.currentPage != 0) {
//                        categoryViewModel.getProductList(
//                            categoryId,
//                            categoryViewModel.currentPage,
//                            getQuery(),
//                            "", false
//                        )
//                    }
//                }
////                    if (products?.size!! < totalResults) {
//
////                    }
////            Toast.makeText(context, filterQuery, Toast.LENGTH_LONG).show()
////            Toast.makeText(context, filterQuery, Toast.LENGTH_LONG)
////                .let { it.showToastySuccess(it, context).show() }
//
//            }


            // categoryViewModel = ViewModelProvider(this, factory).get(CategoryViewModel::class.java)
            binding.viewModel = categoryViewModel


//        val target = createTarget()

//        binding.switchOrganic.setOnCheckedChangeListener { _, isChecked ->
//            categoryViewModel.currentPage = 0
//
//            categoryViewModel.getProductList(
//                categoryViewModel.currentPage,
//                getQuery(),
//                ""
//            )
//
//
//        }


            if (!arguments?.getString(Constants.API_KEY.CATEGORY_ID).isNullOrEmpty()) {
                urlLink = arguments?.getString(Constants.API_KEY.CATEGORY_ID) ?: ""
                categoryId =
                    arguments?.getString(Constants.API_KEY.CATEGORY_ID)?.substringAfterLast("c/")
                        ?: ""
                binding.picker.visibility = View.GONE
                binding.view.visibility = View.GONE
                categoryViewModel.currentPage = 0
                categoryViewModel.sort = sortSelected
                filterQuery = getQuery() + "relevance"
                categoryViewModel.getProductList(
                    categoryId,
                    categoryViewModel.currentPage,
                    getQuery(),
                    true,
                    "listing"
                )
            } else {
                binding.picker.visibility = View.VISIBLE
                binding.view.visibility = View.VISIBLE
                categoryAdapter = CategoryAdapter(categoryViewModel)
                categoryViewModel.getCategoryList()
                categoryAdapter.onClick = this
                //categoryAdapter
                if (binding.picker.adapter == null) {
                    wrapper = InfiniteScrollAdapter.wrap(categoryAdapter)
                    binding.picker.adapter = wrapper
                    binding.picker.setSlideOnFling(true)
                    binding.picker.setItemTransformer(
                        ScaleTransformer.Builder()
                            .setMaxScale(1.05f)
                            .setMinScale(0.75f)
                            .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                            .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                            .build()
                    )
                }
            }


            discreteItemsLoadedOnTheFirstTime = true
            if (!this::listingAdapter.isInitialized) {
                listingAdapter = ListingAdapter(categoryViewModel)
                listingAdapter.onListingItemClick = null
                listingAdapter.setHasStableIds(true)
                listingAdapter.notifyDataSetChanged()


                layoutManager =
                    GridLayoutManager(context, mSpanCount, LinearLayoutManager.VERTICAL, false)
                binding.recyclerViewProduct.layoutManager = layoutManager

                binding.recyclerViewProduct.adapter = listingAdapter
            }
            listingAdapter.notifyDataSetChanged()



            binding.switchVegan.setOnCheckedChangeListener { buttonView, isChecked ->
                if (skipVeganOnCheckedListener) {
                    if (isChecked) {
                        isVegan = ":Vegan:true"
                    } else {
                        isVegan = ""
                        if (filterQuery.contains(":Vegan:true"))
                            filterQuery = filterQuery.replace(":Vegan:true", "")
                    }
                    if (filterQuery == ":relevance") {
                        if (filterQuery != ":$sortSelected") {
                            filterQuery = ":$sortSelected"
                        }
                    }
                    filterQuery = filterQuery + isVegan
                    categoryViewModel.currentPage = 0

                    categoryViewModel.sort = sortSelected
                    categoryViewModel.getProductList(
                        this.categoryId,
                        categoryViewModel.currentPage,
                        ":$filterQuery",
                        true, "listing"
                    )
                } else {
                    skipVeganOnCheckedListener = true
                }

            }

            binding.switchGlutenFree.setOnCheckedChangeListener { buttonView, isChecked ->
                if (skipGlutenFreeOnCheckedListener) {
                    if (isChecked) {
                        isGlutenFree = ":GlutenFree:true"
                    } else {
                        isGlutenFree = ""
                        if (filterQuery.contains(":GlutenFree:true"))
                            filterQuery = filterQuery.replace(":GlutenFree:true", "")
                    }
                    if (filterQuery == ":relevance") {
                        if (filterQuery != sortSelected) {
                            filterQuery = sortSelected
                        }
                    }
                    filterQuery = filterQuery + isGlutenFree
                    categoryViewModel.currentPage = 0

                    categoryViewModel.sort = sortSelected
                    categoryViewModel.getProductList(
                        this.categoryId,
                        categoryViewModel.currentPage,
                        ":$filterQuery",
                        true, "listing"
                    )
                } else {
                    skipGlutenFreeOnCheckedListener = true
                }

            }
            binding.switchOrganic.setOnCheckedChangeListener { buttonView, isChecked ->
                if (skipOrganicOnCheckedListener) {
                    if (isChecked) {
                        isOrganic = ":Organic:true"
                    } else {
                        isOrganic = ""
                        if (filterQuery.contains(":Organic:true"))
                            filterQuery = filterQuery.replace(":Organic:true", "")
                    }
                    categoryViewModel.currentPage = 0
                    if (filterQuery == ":relevance") {
                        if (filterQuery != sortSelected) {
                            filterQuery = sortSelected
                        }
                    }

                    filterQuery = filterQuery + isOrganic
                    categoryViewModel.sort = sortSelected
                    categoryViewModel.getProductList(
                        this.categoryId,
                        categoryViewModel.currentPage,
                        ":$filterQuery",
                        true, "listing"
                    )
                } else {
                    skipOrganicOnCheckedListener = true
                }

            }


        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.category_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                val sortDialogFragment = SortDialogFragment(sortList)
                sortDialogFragment.show(childFragmentManager, "")
                sortDialogFragment.onClick = this
            }
            R.id.filter -> {
                findNavController().navigate(
                    R.id.action_navigate_to_filter,
                    bundleOf(
                        Constants.INTENT_KEY.FACETS to facetList,
                        "query" to filterQuery,
                        "sort_selected" to ":$sortSelected",
                        "categoryid" to categoryId
                    )
                )
            }
            R.id.search -> {
                findNavController().navigate(
                    R.id.action_navigate_to_search
                )
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
        return true
    }

    private fun pagination() {

        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (binding.recyclerViewProduct.adapter?.getItemViewType(
                    position
                )) {
                    listingAdapter.VIEW_TYPE_NORMAL -> 1
                    listingAdapter.VIEW_TYPE_LOADING -> 2
                    else -> 1
                }
            }
        }

        binding.recyclerViewProduct.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                Log.d("total", totalItemCount.toString() + "")
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                Log.d("total_last", lastVisibleItem.toString() + "")
                Log.d(TAG, "onScrolled: dy = ${dy}")
                if ((lastVisibleItem == totalItemCount - 1) && (categoryViewModel.currentPage < lastPage - 1 && dy > 0)) {
                    categoryViewModel.currentPage++
                    Log.d("current", categoryViewModel.currentPage.toString())
                    this@CategoryFragment.categoryId =
                        categorySelected?.itemBinding?.category?.id ?: ""

                    listingAdapter.onListingItemClick = null
                    categoryViewModel.getProductList(
                        this@CategoryFragment.categoryId,
                        categoryViewModel.currentPage,
                        getQuery(), false, "listing"
                    )
//                    observeProductList()
                }
            }
        })
    }

    private fun observeListingNavigation() {
        categoryViewModel.categoryId.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_listing,
                bundleOf(Constants.API_KEY.CATEGORY_ID to it)
            )
            categoryViewModel.categoryId
        })
    }

    private fun observeCategory() {
        categoryViewModel.categoryList.observe(viewLifecycleOwner, Observer {

            val mFirebaseAnalytics =
                FirebaseAnalytics.getInstance(requireContext())


            val params = Bundle()
            params.putString(
                FirebaseAnalytics.Param.ITEM_LIST_ID,
                it?.id ?: ""
            )

            params.putString(
                FirebaseAnalytics.Param.ITEM_LIST_NAME,
                it?.name ?: ""
            )

            mFirebaseAnalytics.logEvent(
                FirebaseAnalytics.Event.VIEW_ITEM_LIST,
                params
            )
            if (categoryAdapter.categoryList == null) {

                var newSubCategoryList = it?.subcategories

                for (i in 0 until (newSubCategoryList?.size ?: 0)) {
                    if (newSubCategoryList?.get(i)?.id.equals("NEW")) {
                        var subListObj = newSubCategoryList?.get(i)
                        val secSubCategoryList = newSubCategoryList?.toMutableList()?.apply {
                            removeAt(i)
                        }
                        if (subListObj != null) {
                            secSubCategoryList?.add(0, subListObj)
                        }
                        var newSubCategoryList2: List<Subcategory>? = secSubCategoryList
                        newSubCategoryList = newSubCategoryList2
                    }
                }

                categoryAdapter.categoryList = newSubCategoryList

                if (isComeFromFilter) {
//                    categoryId = it?.subcategories?.firstOrNull()?.id ?: "0"
                    categoryId = categorySelected?.itemBinding?.category?.id ?: ""
                    listingAdapter.onListingItemClick = null
                    categoryViewModel.getProductList(
                        this.categoryId,
                        categoryViewModel.currentPage,
                        query.plus(getQuery()), true, "listing"
                    )
                    isComeFromFilter = false

                }
            }
        })
    }


    private fun observeError() {
        categoryViewModel.categoryRepository.error().observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)

            Log.d(HomeFragment.TAG, it)
        })

        categoryViewModel.wishListRepository.error().observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)

            Log.d(HomeFragment.TAG, it)
        })

        categoryViewModel.mErrorServer.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)

            Log.d(HomeFragment.TAG, it)
        })
    }

    private fun observeAddToWishlistEvent() {
        categoryViewModel.mAddWishListLiveData.observe(viewLifecycleOwner, Observer {

            AppUtil.showToastySuccess(context, R.string.add_to_wishlist)

        })
    }

    private fun observeStartLogin() {
        categoryViewModel.startLoginEvent.observe(this, Observer {
            context?.let {
                ContextCompat.startActivity(
                    it, Intent(context, LoginActivity::class.java), null
                )
            }
        })
    }

    private fun observeShowHint() {
        categoryViewModel.showHint.observe(this, Observer {
            binding.hintContainer.visibility = it
        })
    }

    private fun observeProgressBar() {
        categoryViewModel.categoryRepository.progressBarVisibility().observe(viewLifecycleOwner,
            Observer {
                binding.progressBar.visibility = it
                if (it == View.VISIBLE) {
                    (activity as MainActivity).binding.progressBar.visibility = it
                    (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
                } else {
                    (activity as MainActivity).binding.progressBar.visibility = it
                    (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
                }
            })
    }

    private fun observeBadgeNumber() {
        categoryViewModel.badgeNumber.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }

    private fun observeProductList() {
        categoryViewModel.productList.observe(viewLifecycleOwner, Observer {
            listingAdapter.onListingItemClick = this
            products = it.products as MutableList<Product>
            switchVeganVisiability = true
            switchOrganicVisiability = true
            switchGlutenFreeVisiability = true

//            if (skipDuplicateProductListObserver){
            if (categoryViewModel.currentPage == 0) {


//              listingAdapter.productList = it.products as MutableList<Product>
                listingAdapter.reset(binding.recyclerViewProduct.scrollState)
                listingAdapter.refresh(it.products as MutableList<Product>)
            } else {
                listingAdapter.refresh(it.products as MutableList<Product>)
            }
//            }

//            skipDuplicateProductListObserver=true

            if (urlLink.isNotEmpty()) {
                if (urlLink.contains("http")) {
                    urlLink = urlLink.substringBefore("/c").substringAfterLast("/")
                    binding.textViewCategoryName.text =
                        URLDecoder.decode(urlLink, "UTF-8").replace("-", " ")
                } else {
                    binding.textViewCategoryName.text =
                        ""
                }

            }
//            val categoryFilterFacets: MutableList<Facet> = mutableListOf()
//            categoryFilterAdapter = CategoryFilterAdapter()
//            categoryFilterAdapter?.breadCrumbsList = it.breadcrumbs
//            binding.recyclerViewFilter.adapter = categoryFilterAdapter
//            categoryFilterAdapter.refresh(it.facets ?: listOf())
//            it.facets?.forEach { facet ->
//                if (facet.name == "Vegan" || facet.name == "Organic" || facet.name == "Gluten Free") {
//                    val isVisible = facet.values?.any { value -> value.name == "true" }
//                    if (isVisible == true) {
//                        categoryFilterFacets.add(facet)
//                    }
//                }
//            }
//            categoryFilterAdapter?.refresh(categoryFilterFacets)
//            categoryFilterAdapter?.onFilterChecked = this
//            categoryFilterAdapter?.onFilterUnChecked = this


            totalResults = it.pagination?.totalResults ?: 0

            breadCrumbList = it.breadcrumbs
            facetList =
                it.facets?.filterNot { facet ->
                    (facet.name == "Vegan" || facet.name == "Organic" || facet.name == "Gluten Free")
                } ?: ArrayList()

//            facetSwitch.clear()
            it.facets?.forEach {
                if (it.name == "Vegan" || it.name == "Gluten Free" || it.name == "Organic") {
                    if (it.name == "Vegan") {
                        it.values?.forEachIndexed { index, value ->
                            if (value.name == "true") {
                                binding.switchVegan.visibility = View.VISIBLE
                                switchVeganVisiability = false
                            } else {
                                if (switchVeganVisiability)
                                    binding.switchVegan.visibility = View.GONE
                            }
                        }
                    }

                    if (it.name == "Gluten Free") {
                        it.values?.forEachIndexed { index, value ->
                            if (value.name == "true") {
                                binding.switchGlutenFree.visibility = View.VISIBLE
                                switchGlutenFreeVisiability = false
                            } else {
                                if (switchGlutenFreeVisiability)
                                    binding.switchGlutenFree.visibility = View.GONE
                            }
                        }
                    }
                    if (it.name == "Organic") {
                        it.values?.forEachIndexed { index, value ->
                            if (value.name == "true") {
                                binding.switchOrganic.visibility = View.VISIBLE
                                switchOrganicVisiability = false
                            } else {
                                if (switchOrganicVisiability)
                                    binding.switchOrganic.visibility = View.GONE
                            }
                        }
                    }
//                    facetSwitch.add(it.name ?: "")
                }
            }


//            if (facetSwitch.contains("Vegan")) {
//
//                binding.switchVegan.visibility = View.VISIBLE
//            } else {
//                binding.switchVegan.visibility = View.GONE
//            }
//
//            if (facetSwitch.contains("Gluten Free")) {
//                binding.switchGlutenFree.visibility = View.VISIBLE
//            } else {
//                binding.switchGlutenFree.visibility = View.GONE
//            }
//
//
//            if (facetSwitch.contains("Organic")) {
//                binding.switchOrganic.visibility = View.VISIBLE
//            } else {
//                binding.switchVegan.visibility = View.GONE
//            }

            sortList = it.sorts ?: ArrayList()
            if (sortSelected.isNullOrEmpty()) {
                if (it.sorts.isNullOrEmpty().not()) {
                    sortSelected = it.sorts?.get(0)?.code ?: ""
                    categoryViewModel.sort = it.sorts?.get(0)?.code ?: ""
//                    filterQuery = sortSelected
                }
            } else {
                categoryViewModel.sort = sortSelected
//                filterQuery = sortSelected
            }



            lastPage = it.pagination?.totalPages ?: 0
            listingAdapter.totalResult = it.pagination?.totalResults ?: 0
//            binding.buttonSortBy.isEnabled = true
//            binding.buttonFilter.isEnabled = true
//            binding.buttonOrganic.isEnabled = true
        })
    }


    override fun onClick(position: Int) {
        binding.picker.smoothScrollToPosition(wrapper.getClosestPosition(position))
    }

    override fun onStart() {
        super.onStart()
        binding.picker.addOnItemChangedListener(this)

    }

    private var lastCalledTime: Long = 0


    override fun onCurrentItemChanged(p0: CategoryAdapter.ViewHolder?, p1: Int) {
        categorySelected = p0
//        categoryViewModel.startLoginEvent2.skip(2).subscribe {
//
//        }
//        if (discreteItemsLoadedOnTheFirstTime && filterQuery.isNotEmpty()) {
//            discreteItemsLoadedOnTheFirstTime = false
//        } else {
        if (p0?.itemBinding
                ?.category?.id != this.categoryId
        ) {
            filterQuery = ""
        }


//        }
        binding.textViewCategoryName.text = p0?.itemBinding?.category?.name
        listingAdapter.reset(binding.recyclerViewProduct.scrollState)
        categoryFilterAdapter?.clear()
//        query = query.plus("category:${p0?.itemBinding?.category?.id}")
        this.categoryId = p0?.itemBinding?.category?.id ?: ""

        categoryViewModel.currentPage = 0

        // bug fixes to force it call only one time on fragment instansiate
        if (SystemClock.elapsedRealtime() - lastCalledTime < MARGIN_TIME) {
            return
        } else {
            listingAdapter.onListingItemClick = null
            if (getQuery() == ":") {
                filterQuery = query.plus(getQuery()) + "relevance"

                categoryViewModel.getProductList(
                    this.categoryId,
                    categoryViewModel.currentPage,

                    query.plus(getQuery()) + "relevance", true, "listing"
                )
            } else {
                filterQuery = query.plus(getQuery())
                categoryViewModel.getProductList(
                    this.categoryId,
                    categoryViewModel.currentPage,

                    query.plus(getQuery()), true, "listing"
                )
            }

        }
        lastCalledTime = SystemClock.elapsedRealtime()
        if (binding.switchOrganic.isChecked) {
            skipOrganicOnCheckedListener = false
            binding.switchOrganic.isChecked = false
        }
        if (binding.switchGlutenFree.isChecked) {
            skipGlutenFreeOnCheckedListener = false
            binding.switchGlutenFree.isChecked = false

        }
        if (binding.switchVegan.isChecked) {
            skipVeganOnCheckedListener = false
            binding.switchVegan.isChecked = false

        }
//        subCategoryAdapter.subCategoryList = p0?.itemBinding?.category?.subcategories
    }

//    private fun buttonClicks() {
//        binding.buttonSortBy.setOnClickListener {
//            val sortDialogFragment = SortDialogFragment(sortList)
//            sortDialogFragment.show(childFragmentManager, "")
//            sortDialogFragment.onClick = this
//        }
//
//        binding.buttonFilter.setOnClickListener {
//            findNavController().navigate(
//                R.id.action_navigate_to_filter,
//                bundleOf(Constants.INTENT_KEY.FACETS to facetList)
//            )
//
//
//        }
//    }

    override fun onClick(code: String) {


        listingAdapter.reset(binding.recyclerViewProduct.scrollState)
        categoryViewModel.currentPage = 0
        listingAdapter.onListingItemClick = null
        sortSelected = code
        categoryViewModel.sort = sortSelected
//        var filterReplaceFirst2 = getQuery().replaceFirst(':', '$')
//        filterReplaceFirst2.replaceRange(
//            IntRange(
//                filterReplaceFirst2.indexOfFirst { it == '$' },
//                getQuery().length - 1
//            ), sortSelected
//        )
        var query = ""

        if (getQuery().first() == ':') {
            var dummyQuery = getQuery().replaceFirst(':', '%')
            if (!dummyQuery.contains(':')) {
                query = dummyQuery.replaceRange(IntRange(0, dummyQuery.length - 1), code)
            } else {
                query =
                    dummyQuery.replaceRange(IntRange(0, dummyQuery.indexOfFirst { it == ':' } - 1),
                        code)
            }
        } else {
            var dummyQuery = getQuery()
            if (!dummyQuery.contains(':')) {
                query = dummyQuery.replaceRange(IntRange(0, dummyQuery.length - 1), code)
            } else {
                query =
                    dummyQuery.replaceRange(IntRange(0, dummyQuery.indexOfFirst { it == ':' } - 1),
                        code)
            }
        }

//        filterQuery = code
        filterQuery = ":$query"
//        filterQuery=filterQuery.replace()
        categoryViewModel.getProductList(
            this.categoryId,
            categoryViewModel.currentPage,
            filterQuery,
            true, "listing"
        )
    }

    override fun onListingItemClick(key: String, productCode: String) {
        if (key.equals("onAllList")) {

//            categoryViewModel.productList.value=null
//            skipDuplicateProductListObserver=false
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(
                    Constants.INTENT_KEY.PRODUCT_CODE to productCode,
                    "categoryId" to categoryId
                )
            )
        } else if (key.equals("onWishList")) {
            categoryViewModel.addToWishList(productCode)
        } else if (key.equals("onRemoveWishList")) {
            categoryViewModel.removeFromWishList(productCode)
        }
    }

    private fun observeAddToCart() {
        categoryViewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.added_to_cart_message)
        })
    }

    private fun observeSpanCount() {
        categoryViewModel.spanCountLiveData.observe(viewLifecycleOwner, Observer {
            layoutManager?.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return when (binding.recyclerViewProduct.adapter?.getItemViewType(
                        position
                    )) {
                        listingAdapter.VIEW_TYPE_NORMAL -> 1
                        listingAdapter.VIEW_TYPE_LOADING -> it
                        else -> 1
                    }
                }
            }
            this.mSpanCount = it
            listingAdapter.spanCount = it
            TransitionManager.beginDelayedTransition(binding.recyclerViewProduct)
            layoutManager.spanCount = it

        })
    }

    private fun observeBottomGroupLayoutVisibility() {
        categoryViewModel.bottomGroupLayoutVisibility.observe(viewLifecycleOwner, Observer {
            listingAdapter.bottomGroupLayoutVisibility = it
        })


//         categoryViewModel.spanCountMutableLive.observe(viewLifecycleOwner, Observer {
//            listingAdapter.spanCount = it
//        })


    }

    private fun getQuery(): String {
        return if (filterQuery.isNotEmpty()) {

            if (isComeFromFilter) {
                "$filterQuery"
            } else {
                "$filterQuery"
//                var filterReplaceFirst = filterQuery.replaceFirst(':', '$')
//                filterReplaceFirst =  filterReplaceFirst.substring(
//                    filterReplaceFirst.indexOf(':'),
//                    filterQuery.length - 1
//                )
//
//                "$filterReplaceFirst"
            }

        } else {
//            if (binding.switchOrganic.isChecked) {
//                return ":relevance:category:$categoryId".plus(organic)
//            } else {
//                return ":relevance:category:$categoryId"
//            }
            ":$sortSelected"
        }
    }

    fun changeLayoutManager(spanCount: Int) {
        categoryViewModel.spanCount = spanCount
        layoutManager?.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return when (binding.recyclerViewProduct.adapter?.getItemViewType(
                    position
                )) {
                    listingAdapter.VIEW_TYPE_NORMAL -> 1
                    listingAdapter.VIEW_TYPE_LOADING -> spanCount
                    else -> 1
                }
            }
        }

    }

    var filterQueryHashMap = HashMap<String, String>()
    override fun onFilterChecked(facet: Facet?) {
        //TODO add hashmap here <facetName, value>
        var query = facet?.values?.get(0)?.query?.query?.value?.replace("false", "true")
        filterQueryHashMap.put(facet?.name ?: "", query ?: "")
        Log.d(
            TAG,
            "onFilterChecked: getQuery -> ${getQuery()} query -> ${query} \n filterQuery -> ${filterQuery}"
        )
//        filterQuery = getQuery().plus(query)
        filterQuery = query ?: ""
        Log.d(TAG, "onFilterChecked: query -> ${query}")
        Log.d(TAG, "onFilterChecked: filterQuery -> ${filterQuery}")
        Log.d(TAG, "onFilterChecked: getQuery -> ${getQuery()}")
        categoryViewModel.currentPage = 0
        listingAdapter.onListingItemClick = null
        categoryViewModel.getProductList(
            this.categoryId,
            categoryViewModel.currentPage,
            getQuery(),
            true, "listing"
        )

    }

    override fun onFilterUnChecked(breadCrumb: BreadCrumb?) {
        breadCrumb?.removeQuery?.query?.value?.let {
            filterQuery = it
            categoryViewModel.currentPage = 0
            listingAdapter.onListingItemClick = null
            categoryViewModel.getProductList(
                this.categoryId,
                categoryViewModel.currentPage,
                getQuery(), true, "listing"
            )
        }
//        filterQuery = ":relevance:category:$categoryId"

    }

}

