package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ProductDiscounts(
    @SerializedName("currencyIso")
    val currencyIso: String = "",
    @SerializedName("formattedValue")
    val formattedValue: String = "",
    @SerializedName("maxQuantity")
    val maxQuantity: Int = 0,
    @SerializedName("minQuantity")
    val minQuantity: Int = 0,
    @SerializedName("priceType")
    val priceType: String = "",
    @SerializedName("value")
    val value: Double = 0.0
)