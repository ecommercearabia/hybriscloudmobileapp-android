package com.erabia.foodcrowd.android.common.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSlotResult implements Parcelable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("timeSlotData")
    @Expose
    private List<TimeSlotData> timeSlotData = null;

    public TimeSlotResult(){}
    protected TimeSlotResult(Parcel in) {
        date = in.readString();
        timeSlotData = in.createTypedArrayList(TimeSlotData.CREATOR);
    }

    public static final Creator<TimeSlotResult> CREATOR = new Creator<TimeSlotResult>() {
        @Override
        public TimeSlotResult createFromParcel(Parcel in) {
            return new TimeSlotResult(in);
        }

        @Override
        public TimeSlotResult[] newArray(int size) {
            return new TimeSlotResult[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<TimeSlotData> getTimeSlotData() {
        return timeSlotData;
    }

    public void setTimeSlotData(List<TimeSlotData> timeSlotData) {
        this.timeSlotData = timeSlotData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(date);
        parcel.writeTypedList(timeSlotData);
    }
}
