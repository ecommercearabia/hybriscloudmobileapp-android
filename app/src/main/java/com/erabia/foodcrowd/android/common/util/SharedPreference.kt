package com.erabia.foodcrowd.android.common.util

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.erabia.foodcrowd.android.common.MyApplication

class SharedPreference {
    private lateinit var sharedPreference: SharedPreferences

    init {
        sharedPreference =
            PreferenceManager.getDefaultSharedPreferences(MyApplication.getInstance())

    }

    companion object {
        private var instance: SharedPreference? = null
        private const val REFRESH_TOKEN = "REFRESH_TOKEN"
        private const val USER_TOKEN = "USER_TOKEN"
        private const val FULL_NAME: String = "FULL_NAME"
        private const val LOGIN_FLAG = "LOGIN_FLAG"
        private const val FACEBOOK_LOGIN_FLAG = "FACEBOOK_LOGIN_FLAG"
        private const val IS_REMEMBER_ME = "IS_REMEMBER_ME"
        private const val LOGIN_EMAIL = "LOGIN_EMAIL"
        private const val PREVIOUS_LOGIN_EMAIL = "PREVIOUS_LOGIN_EMAIL"
        private const val PREVIOUS_EDIT_LOGIN_EMAIL = "PREVIOUS_EDIT_LOGIN_EMAIL"
        private const val LOGIN_EMAIL_FINGERPRINT = "LOGIN_EMAIL_FINGERPRINT"
        private const val SHIPMENT_TYPE = "SHIPMENT_TYPE"
        private const val SIGNATURE_ID = "SIGNATURE_ID"
        private const val LOGIN_TOKEN = "LOGIN_TOKEN"
        private const val CART_ID = "CART_ID"
        private const val SOCIAL_ID = "SOCIAL_ID"
        private const val SOCIAL_EMAIL_ID = "SOCIAL_EMAIL_ID"
        private const val SOCIAL_USER_NAME = "SOCIAL_USER_NAME"
        private const val APP_TOKEN = "APP_TOKEN"
        private const val GUID = "GUID"
        private const val IS_HINT_SHOWN = "IS_HINT_SHOWN"
        private const val IS_FINGERPRINT_ENABLED = "IS_FINGERPRINT_ENABLED"
        private const val IS_LOGIN_FIRST_TIME = "IS_LOGIN_FIRST_TIME"
        private const val FIREBASE_TOKEN = "FIREBASE_TOKEN"
        private const val pickup_In_Store_Enabled = "pickupInStoreEnabled"
        private const val nationality_Enabled = "nationalityEnabled"
        private const val VOUCHER_ID = "VOUCHER_ID"
        private const val REFERRAL_CODE = "REFERRAL_CODE"
        private const val store_credit_limit = "store_credit_limit"


        @Synchronized
        fun getInstance(): SharedPreference {
            if (instance == null) {
                instance = SharedPreference()
            }
            return instance as SharedPreference
        }
    }


    fun isFacebookLoginFlag(): Boolean {
        return sharedPreference.getBoolean(FACEBOOK_LOGIN_FLAG, false)
    }

    fun isRememberMeLoginFlag(): Boolean {
        return sharedPreference.getBoolean(IS_REMEMBER_ME, false)
    }


    fun setFacebookLoginFlag(isFacebookLogIn: Boolean) {
        sharedPreference.edit().putBoolean(FACEBOOK_LOGIN_FLAG, isFacebookLogIn).apply()
    }

    fun setLOGIN_User(loginUser: String) {
        sharedPreference.edit().putString(LOGIN_FLAG, loginUser).apply()
    }

    fun setLOGIN_EMAIL(loginEmail: String) {
        sharedPreference.edit().putString(LOGIN_EMAIL, loginEmail).apply()
    }


    fun setPreviousLoginEmail(loginEmail: String) {
        sharedPreference.edit().putString(PREVIOUS_LOGIN_EMAIL, loginEmail).apply()
    }

    fun setPreviousFromEditTextLoginEmail(loginEmail: String) {
        sharedPreference.edit().putString(PREVIOUS_EDIT_LOGIN_EMAIL, loginEmail).apply()
    }


    fun setLOGIN_EMAIL_FINGERPRINT(loginEmail: String) {
        sharedPreference.edit().putString(LOGIN_EMAIL_FINGERPRINT, loginEmail).apply()
    }

    fun setShipmentType(type: String) {
        sharedPreference.edit().putString(SHIPMENT_TYPE, type).apply()
    }

    fun getShipmentType(): String? {
        return sharedPreference.getString(SHIPMENT_TYPE, "")
    }

    fun setVoucherID(type: String) {
        sharedPreference.edit().putString(VOUCHER_ID, type).apply()
    }

    fun getVoucherID(): String? {
        return sharedPreference.getString(VOUCHER_ID, "")
    }

    fun setStoreCreditLimit(type: String) {
        sharedPreference.edit().putString(store_credit_limit, type).apply()
    }

    fun getStoreCreditLimit(): String? {
        return sharedPreference.getString(store_credit_limit, "")
    }

    fun setReferralCode(type: String) {
        sharedPreference.edit().putString(REFERRAL_CODE, type).apply()
    }

    fun getReferralCode(): String? {
        return sharedPreference.getString(REFERRAL_CODE, "")
    }

    fun setSIGNATURE_ID(signatureId: String) {
        sharedPreference.edit().putString(SIGNATURE_ID, signatureId).apply()
    }

    fun setFIREBASE_TOKEN(firebaseToken: String) {
        sharedPreference.edit().putString(FIREBASE_TOKEN, firebaseToken).apply()
    }


    fun setFingerPrintEnabled(isEnabled: Boolean) {
        sharedPreference.edit().putBoolean(IS_FINGERPRINT_ENABLED, isEnabled).apply()
    }


    fun isFingerPrintEnabled(): Boolean {
        return sharedPreference.getBoolean(IS_FINGERPRINT_ENABLED, false)
    }


    fun setIsLoginFirstTime(isEnabled: Boolean) {
        sharedPreference.edit().putBoolean(IS_LOGIN_FIRST_TIME, isEnabled).apply()
    }

    fun setPickupInStoreEnabled(isEnabled: Boolean) {
        sharedPreference.edit().putBoolean(pickup_In_Store_Enabled, isEnabled).apply()
    }
    fun getPickupInStoreEnabled(): Boolean {
        return sharedPreference.getBoolean(pickup_In_Store_Enabled, false)
    }

    fun setNationalityEnabled(isEnabled: Boolean) {
        sharedPreference.edit().putBoolean(nationality_Enabled, isEnabled).apply()
    }
    fun getNationalityEnabled(): Boolean {
        return sharedPreference.getBoolean(nationality_Enabled, false)
    }


    fun isLoginFirstTime(): Boolean {
        return sharedPreference.getBoolean(IS_LOGIN_FIRST_TIME, true)
    }


    @SuppressLint("ApplySharedPref")
    fun setLOGIN_TOKEN(loginToken: String) {
        sharedPreference.edit().putString(LOGIN_TOKEN, loginToken).commit()
    }


    @SuppressLint("ApplySharedPref")
    fun setApp_TOKEN(appToken: String?) {
        sharedPreference.edit().putString(APP_TOKEN, appToken).commit()
    }

    @SuppressLint("ApplySharedPref")
    fun setUser_TOKEN(appToken: String?) {
        sharedPreference.edit().putString(USER_TOKEN, appToken).commit()
    }


    @SuppressLint("ApplySharedPref")
    fun setEXPIRES_IN_TOKEN(expiresIn: Int) {
        sharedPreference.edit().putInt(USER_TOKEN, expiresIn).commit()
    }

    fun setFullName(fullName: String) {
        sharedPreference.edit().putString(FULL_NAME, fullName).apply()
    }

    @SuppressLint("ApplySharedPref")
    fun setCART_ID(cartId: String) {
        sharedPreference.edit().putString(CART_ID, cartId).commit()
    }


    @SuppressLint("ApplySharedPref")
    fun setRefreshToken(refreshToken: String?) {
        sharedPreference.edit().putString(REFRESH_TOKEN, refreshToken).commit()
    }

    @SuppressLint("ApplySharedPref")
    fun setRememberMeLoginFlag(isRemember: Boolean) {
        sharedPreference.edit().putBoolean(IS_REMEMBER_ME, isRemember).apply()

    }


    fun getLoginEmail(): String? {
        return sharedPreference.getString(LOGIN_EMAIL, "")
    }


    fun getPreviousLoginEmail(): String? {
        return sharedPreference.getString(PREVIOUS_LOGIN_EMAIL, "")
    }


    fun getPreviousEditLoginEmail(): String? {
        return sharedPreference.getString(PREVIOUS_EDIT_LOGIN_EMAIL, "")
    }


    fun getLoginEmailFingerPrint(): String? {
        return sharedPreference.getString(LOGIN_EMAIL_FINGERPRINT, "")
    }



    fun getSIGNATURE_ID(): String? {
        return sharedPreference.getString(SIGNATURE_ID, "")
    }

    fun getFIREBASE_TOKEN(): String? {
        return sharedPreference.getString(FIREBASE_TOKEN, "")
    }

    fun getFullName(): String? {
        return sharedPreference.getString(FULL_NAME, "")
    }


    fun getLoginToken(): String? {
        return sharedPreference.getString(LOGIN_TOKEN, "")
    }

    fun getAppToken(): String? {
        return sharedPreference.getString(APP_TOKEN, "")
    }


    fun getCartId(): String? {
        return sharedPreference.getString(CART_ID, "")
    }

    fun getRefreshToken(): String? {
        return sharedPreference.getString(REFRESH_TOKEN, "")
    }

    fun getUserToken(): String? {
        return sharedPreference.getString(USER_TOKEN, null)
    }


    fun getLOGIN_User(): String? {
        return sharedPreference.getString(LOGIN_FLAG, null)
    }

    fun setSocialId(socialId: String) {
        sharedPreference.edit().putString(SOCIAL_ID, socialId).apply()
    }

    fun getSocialId(): String? {
        return sharedPreference.getString(SOCIAL_ID, "")
    }

    fun setSocialEmailId(socialEmailId: String) {
        sharedPreference.edit().putString(SOCIAL_EMAIL_ID, socialEmailId).apply()
    }

    fun getSocialEmailId(): String? {
        return sharedPreference.getString(SOCIAL_EMAIL_ID, "")
    }

    fun setSocialUserName(socialUserName: String) {
        sharedPreference.edit().putString(SOCIAL_USER_NAME, socialUserName).apply()
    }

    fun getSocialUserName(): String? {
        return sharedPreference.getString(SOCIAL_USER_NAME, "")
    }

    @SuppressLint("ApplySharedPref")
    fun setGUID(guid: String?) {
        sharedPreference.edit().putString(GUID, guid).commit()
    }

    fun setIsHintShown(isShown: Boolean) {
        sharedPreference.edit().putBoolean(IS_HINT_SHOWN, isShown).apply()
    }

    fun getGUID(): String? {
        return sharedPreference.getString(GUID, null)
    }

    fun getIsHintShown(): Boolean {
        return sharedPreference.getBoolean(IS_HINT_SHOWN, false)
    }


    fun clearSP() {
//        sharedPreference.edit().clear().commit()
        sharedPreference.edit().remove(REFRESH_TOKEN).commit()
        sharedPreference.edit().remove(USER_TOKEN).commit()
        sharedPreference.edit().remove(FULL_NAME).commit()
        sharedPreference.edit().remove(LOGIN_FLAG).commit()
        sharedPreference.edit().remove(FACEBOOK_LOGIN_FLAG).commit()
        sharedPreference.edit().remove(IS_REMEMBER_ME).commit()
        sharedPreference.edit().remove(LOGIN_EMAIL).commit()
        sharedPreference.edit().remove(LOGIN_TOKEN).commit()
        sharedPreference.edit().remove(CART_ID).commit()
        sharedPreference.edit().remove(SOCIAL_ID).commit()
        sharedPreference.edit().remove(SOCIAL_EMAIL_ID).commit()
        sharedPreference.edit().remove(SOCIAL_USER_NAME).commit()
        sharedPreference.edit().remove(APP_TOKEN).commit()
        sharedPreference.edit().remove(GUID).commit()
        sharedPreference.edit().remove(IS_HINT_SHOWN).commit()

    }

    fun clearSocialSP() {
        sharedPreference.edit().remove(SOCIAL_EMAIL_ID).commit()
        sharedPreference.edit().remove(SOCIAL_ID).commit()
        sharedPreference.edit().remove(SOCIAL_USER_NAME).commit()
    }


}