package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class DeliveryPointOfService(
    @SerializedName("address")
    val address: Address = Address(),
    @SerializedName("description")
    val description: String = "",
    @SerializedName("displayName")
    val displayName: String = "",
    @SerializedName("distanceKm")
    val distanceKm: Int = 0,
    @SerializedName("features")
    val features: Features = Features(),
    @SerializedName("formattedDistance")
    val formattedDistance: String = "",
    @SerializedName("geoPoint")
    val geoPoint: GeoPoint = GeoPoint(),
    @SerializedName("mapIcon")
    val mapIcon: MapIcon = MapIcon(),
    @SerializedName("name")
    val name: String = "",
    @SerializedName("openingHours")
    val openingHours: OpeningHours = OpeningHours(),
    @SerializedName("storeContent")
    val storeContent: String = "",
    @SerializedName("storeImages")
    val storeImages: List<StoreImage> = listOf(),
    @SerializedName("url")
    val url: String = "",
    @SerializedName("warehouseCodes")
    val warehouseCodes: List<String> = listOf()
)