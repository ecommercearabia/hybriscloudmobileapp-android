package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Error(
    @SerializedName("message")
    val message: String = "",
    @SerializedName("type")
    val type: String = ""
)