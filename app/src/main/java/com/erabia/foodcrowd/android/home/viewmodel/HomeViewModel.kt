package com.erabia.foodcrowd.android.home.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.lifecycle.*
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.hideRefreshLayout
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.ContentSlots
import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class HomeViewModel(
    homeRepository: HomeRepository,
    cartRepository: CartRepository,
    wishListRepository: WishListRepository
) : ProductItemViewModel(homeRepository, cartRepository, wishListRepository) {
    companion object {
        private const val TAG: String = "HomeViewModel"
    }

    var contentSlots: LiveData<ContentSlots> =
        homeRepository?.homeLiveData?.let { Transformations.map(it) { it?.contentSlots } } as LiveData<ContentSlots>
    val hideRefreshLoaderEvent: SingleLiveEvent<Boolean> = homeRepository.hideRefreshLayout()
    val progressBarVisibility: SingleLiveEvent<Int> = homeRepository.progressBarVisibility()
    val error: SingleLiveEvent<String> = homeRepository.error()
    val itemLiveData: MutableLiveData<Component> = homeRepository.itemLiveData
    private val bannerListSubject = homeRepository.bannerListSubject
    private val tabListSubject = homeRepository.tabListSubject

//    var updateSignatureLiveData: MutableLiveData<String> = MutableLiveData<String>()

    private val bannerSizeSubject = homeRepository.bannerSizeSubject
    val addToCartSuccess = cartRepository.addToCartSuccess
    val bannerList: MutableLiveData<MutableList<Component?>?> by lazy { MutableLiveData<MutableList<Component?>?>() }
    val navigateBannerToDetails: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val navigateBannerToListing: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val tabList: MutableLiveData<MutableList<Component?>?> by lazy { MutableLiveData<MutableList<Component?>?>() }
    val bannerSize: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    private val compositeDisposable = CompositeDisposable()
    var quantity: Int = 1
    val addToWishlistEvent: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val loadingVisibility: MutableLiveData<Int> = wishListRepository.loadingVisibility
    val mErrorServer: SingleLiveEvent<String> = wishListRepository.mErrorServer

    //    var mGetWishListLiveData: LiveData<GetWishListResponse> = wishListRepository.mGetWishListLiveData
    var mAddWishListLiveData: MutableLiveData<Void> = wishListRepository.mAddWishListLiveData

    init {

        compositeDisposable.add(tabListSubject.subscribe({
            tabList.value = it
        }, {
            Log.d(TAG, it.message ?: "")
        }))

        compositeDisposable.add(bannerListSubject.subscribe({
            bannerList.value = it
        }, {
            Log.d(TAG, it.message ?: "")
        }))



        compositeDisposable.add(bannerSizeSubject.subscribe({
            bannerSize.value = it
        }, {
            Log.d(TAG, it.message ?: "")
        }))
    }

    fun bannersNavegate(urlLink: String) {


        if (urlLink?.isNotEmpty() == true && urlLink != "{}" && urlLink != "null") {
            when {
                urlLink?.get(urlLink.lastIndexOf("/") - 1).toString() == "p" -> {
                    //Details
                    navigateBannerToDetails.postValue(urlLink?.substringAfterLast("p/"))
                }
                urlLink?.get(urlLink.lastIndexOf("/") - 1).toString() == "c" -> {
                    //list
                    navigateBannerToListing.postValue(urlLink)
                }
            }
        }
//        urlLink.charAt(urlLink.lastIndexOf("/"))
    }
//
//    fun loadCart(userId: String, cartId: String, fields: String) {
//        cartRepository.loadCart(userId, cartId, fields)
//    }

    fun loadHome(): LiveData<Home> = runBlocking {
        return@runBlocking homeRepository.loadHome(Constants.API_KEY.PAGE_ID).apply {
            Transformations.map(this) {
                this@HomeViewModel.hideRefreshLoaderEvent.postValue(true)
            }
        }
    }

//    fun getCart(userId: String, cartId: String, fields: String) {
//        if (SharedPreference.getInstance().getUserToken()
//                ?.isNotEmpty() == true || SharedPreference.getInstance().getGUID()
//                ?.isNotEmpty() == true
//        ) {
//            loadCart("", "", Constants.FIELDS_FULL)
//        } else {
//            createCart(
//                SharedPreference.getInstance().getUserToken()?.let { "current" }
//                ?: run { "anonymous" },
//                "", "", Constants.FIELDS_FULL
//            )
//        }
//    }


    fun getAllComponents(component: Component) {
        viewModelScope.launch {
            homeRepository.getAllComponents(component)
        }
    }

    fun getBanners(component: Component): Single<MutableList<Component?>?>? = runBlocking {
        return@runBlocking homeRepository.getBanners(component)
    }

    fun getTabs(component: Component): Single<MutableList<Component?>?>? = runBlocking {
        return@runBlocking homeRepository.getTabs(component)
    }

    fun setFirebaseTokeb(
        userId: String,
        mobileToken: String
    ) {
        homeRepository.setFirebaseToken(userId, mobileToken)
    }



    fun onRefresh() {
        loadHome()
    }
//    fun getProduct(id: String): LiveData<Product> {
//        return homeRepository.getProduct(id)
//    }

    fun quantityTextChanged(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                try {
                    this@HomeViewModel.quantity = s.toString().toInt()
                } catch (e: Exception) {
                    Log.d(TAG, e.message ?: "")
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }


    class Factory(
        val homeRepository: HomeRepository,
        val cartRepository: CartRepository,
        val wishListRepository: WishListRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return HomeViewModel(homeRepository, cartRepository, wishListRepository) as T
        }
    }

}