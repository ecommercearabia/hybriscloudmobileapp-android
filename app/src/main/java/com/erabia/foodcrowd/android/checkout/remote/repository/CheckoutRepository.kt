package com.erabia.foodcrowd.android.checkout.remote.repository

import android.annotation.SuppressLint
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.checkout.model.*
import com.erabia.foodcrowd.android.checkout.model.PaymentInfo
import com.erabia.foodcrowd.android.checkout.remote.service.CheckoutService
import com.erabia.foodcrowd.android.checkout.util.CheckoutItemManager
import com.erabia.foodcrowd.android.checkout.view.CheckoutFragment
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.repository.Repository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import java.lang.Exception
import java.util.concurrent.TimeUnit
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.newaddress.model.ZipModelAddressResponse
import com.erabia.foodcrowd.android.personaldetails.remote.repository.PersonalDetailsRepository
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.functions.Function5
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class CheckoutRepository(
    private val checkoutService: CheckoutService,
    private val cartService: CartService
) : Repository {
    companion object {
        const val TAG = "CheckoutRepository"
    }

    var prePaymentMode = ""
    var prePaymentModeFlag = true
    val compositeDisposable = CompositeDisposable()
    private val cartRepository = CartRepository(cartService)
    val getTimeSlotLiveData: MutableLiveData<TimeSlot> by lazy { MutableLiveData<TimeSlot>() }
    val paymentInfoLiveData: MutableLiveData<PaymentInfo> by lazy { MutableLiveData<PaymentInfo>() }
    val getDeliveryModeTypesLiveData: MutableLiveData<DeliveryTypes> by lazy { MutableLiveData<DeliveryTypes>() }
    val getPaymentMode: MutableLiveData<PaymentModes> by lazy { MutableLiveData<PaymentModes>() }
    val preSelectPaymentMode: MutableLiveData<PaymentMode> by lazy { MutableLiveData<PaymentMode>() }
    val preSelectStoreCreditMode: MutableLiveData<StoreCreditMode> by lazy { MutableLiveData<StoreCreditMode>() }
    val setDeliveryTypes: MutableLiveData<setDeliveryTypesResponse> by lazy { MutableLiveData<setDeliveryTypesResponse>() }
    val setPaymentMode: MutableLiveData<SetPaymentModeResponse> by lazy { MutableLiveData<SetPaymentModeResponse>() }
    val getStoreCreditMode: MutableLiveData<StoreCreditModes> by lazy { MutableLiveData<StoreCreditModes>() }
    val setStoreCreditMode: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val storeCreditAmount: MutableLiveData<StoreCreditAmount> by lazy { MutableLiveData<StoreCreditAmount>() }
    val storeCreditVisibility: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val setTimeSlotLiveData: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val cartLiveData: MutableLiveData<Cart> by lazy { MutableLiveData<Cart>() }
    val enableCheckoutButtonLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val footerVisibilityLiveDate: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val placeOrderLiveData: MutableLiveData<PlaceOrderResponse> by lazy { MutableLiveData<PlaceOrderResponse>() }
    var isTermsChecked: Boolean = false
    val setDeliveryModeSuccessEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val hasPaymentMethodSetToCart: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val hasTimeSlotSetToCart: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
    val badgeNumber = cartRepository.badgeNumber
    val setPaymentDetailsSuccessEvent: BehaviorSubject<PaymentDetailsResponse> =
        BehaviorSubject.create()
    val appliedVoucher: MutableLiveData<List<AppliedVoucher>> by lazy { MutableLiveData<List<AppliedVoucher>>() }
    val clearPromoCodeText: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val applyVoucherSuccessLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val deleteVoucherSuccessLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val getBankOfferSuccess: SingleLiveEvent<BankOfferResponse> by lazy { SingleLiveEvent<BankOfferResponse>() }
    val getBankOfferError: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val enableStoreCreditLimit: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }


    var cart: Cart? = null
    fun loadPaymentMode(userId: String, cartId: String, fields: String): LiveData<PaymentModes> {
        //TODO remove fake params
        compositeDisposable.add(
            checkoutService?.getPaymentModes("current", "current", fields).get()
                .subscribe(this, {
                    getPaymentMode.value = it.body()
                    try {
                        if ((it.body()?.paymentModes?.size
                                ?: 0) == 1 && it.body()?.paymentModes?.get(0)?.code == "continue"
                        ) {
                            preSelectPaymentMode.value = it.body()?.paymentModes?.get(0)
                            if (cart?.paymentMode?.code != "continue") {
                                setPaymentMode(
                                    Constants.CURRENT,
                                    Constants.CURRENT,
                                    "continue",
                                    Constants.FIELDS_FULL
                                )
                            }
                        }
                    } catch (e: Exception) {
                        Log.d(TAG, e.message ?: "")
                    }
                })
        )
        return getPaymentMode
    }

    fun loadDeliveryModeTypes(
        userId: String,
        cartId: String,
        fields: String
    ): LiveData<DeliveryTypes> {
        compositeDisposable.add(
            checkoutService?.getDeliveryTypes("current", "current", "FULL").get()
                .subscribe(this, {
                    getDeliveryModeTypesLiveData.value = it.body()
                })
        )
        return getDeliveryModeTypesLiveData
    }

    @SuppressLint("CheckResult")
    fun loadAppConfig() {
        checkoutService.getAppConfig("FULL").get().subscribe(
            this, onSuccess_200 = {
                enableStoreCreditLimit.postValue(it.body()?.storeCreditConfiguration?.enableCheckTotalPriceWithStoreCreditLimit)
                SharedPreference.getInstance().setStoreCreditLimit(
                    it.body()?.storeCreditConfiguration?.totalPriceWithStoreCreditLimit.toString()
                        ?: ""
                )
            }, onError_400 = {
                Log.d("loadAppConfig error", it.toString())
            })
    }

//    fun setPaymentMode2(
//        userId: String,
//        cartId: String,
//        paymentModeCode: String,
//        fields: String
//    ): MutableLiveData<SetPaymentModeResponse> {
//        compositeDisposable.add(
//            checkoutService.setPaymentModes(
//                "current",
//                "current",
//                paymentModeCode,
//                fields
//            ).get()
//                .subscribe(this, onSuccess_200 = {
//                    setPaymentMode.value = it.body()
//                    loadCart("", "", Constants.FIELDS_FULL)
//
//                }, onSuccess_201 = {
//                    setPaymentMode.value = it.body()
//                    loadCart("", "", Constants.FIELDS_FULL)
//                })
//        )
//        return setPaymentMode
//    }

//    fun setPaymentMode(
//        userId: String,
//        cartId: String,
//        paymentModeCode: String,
//        fields: String
//    ) {
//        compositeDisposable.add(
//            checkoutService.setPaymentModes(
//                "current",
//                "current",
//                paymentModeCode,
//                fields
//            ).get()
//                .subscribe(this, onSuccess_200 = {
//                    setPaymentMode.value = it.body()
//                    loadCart("", "", Constants.FIELDS_FULL)
//
//                }, onSuccess_201 = {
//                    setPaymentMode.value = it.body()
//                    loadCart("", "", Constants.FIELDS_FULL)
//                })
//        )
//
//    }

    @SuppressLint("CheckResult")
    fun setDeliveryTypes(
        userId: String,
        cartId: String,
        deliveryTypeCode: String,
        fields: String
    ) {
        checkoutService.setDeliveryTypes(
            "current",
            "current",
            deliveryTypeCode,
            fields
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    setDeliveryTypes.postValue(it.body())
                    cartService.getCart("current", "current", "FULL")
                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
            }
        }.get().doOnSubscribe {
            this.progressBarVisibility().value = View.VISIBLE
        }.doFinally {

        }.subscribe({
//            getCartResponse(it)
            this.progressBarVisibility().value = View.GONE

        }, {
            Log.d(com.erabia.foodcrowd.android.common.extension.TAG, it.message ?: "")
            if (it.message?.contains("Unable to resolve host") == true) {
                this.error().postValue("Please check your internet connection")
                this.progressBarVisibility().value = View.GONE
                this.hideRefreshLayout().value = true
            }
        })
    }

    @SuppressLint("CheckResult")
    fun setPaymentMode(
        userId: String,
        cartId: String,
        paymentModeCode: String,
        fields: String
    ) {
        checkoutService.setPaymentModes(
            "current",
            "current",
            paymentModeCode,
            fields
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    setPaymentMode.postValue(it.body())
                    cartService.getCart("current", "current", "FULL")
                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
            }
        }.get().doOnSubscribe {
            this.progressBarVisibility().value = View.VISIBLE
        }.doFinally {

        }.subscribe({
            getCartResponse(it)

        }, {
            Log.d(com.erabia.foodcrowd.android.common.extension.TAG, it.message ?: "")
            if (it.message?.contains("Unable to resolve host") == true) {
                this.error().postValue("Please check your internet connection")
                this.progressBarVisibility().value = View.GONE
                this.hideRefreshLayout().value = true
            }

        })


    }

    fun loadStoreCreditMode(
        userId: String,
        cartId: String,
        fields: String
    ): LiveData<StoreCreditModes> {
        //TODO remove fake params
        compositeDisposable.add(
            checkoutService?.getStoreCreditModes("current", "current", fields).get()
                .subscribe(this, {
                    getStoreCreditMode.value = it.body()
                })
        )
        return getStoreCreditMode
    }

    //    fun setStoreCreditMode1(
//        userId: String,
//        cartId: String,
//        storeCreditModeCode: String,
//        amount: String,
//        fields: String
//    ) {
//        compositeDisposable.add(
//            checkoutService.setStoreCreditModes(
//                "current",
//                "current",
//                storeCreditModeCode,
//                amount,
//                fields
//            ).get()
//                .subscribe(this, onSuccess_200 = {
//                    setStoreCreditMode.value = true
//                    loadCart("", "", Constants.FIELDS_FULL)
//                }, onSuccess_201 = {
//                    setStoreCreditMode.value = true
//                    loadCart("", "", Constants.FIELDS_FULL)
//                    loadPaymentMode(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
//                })
//        )
//
//    }
    var paymentModes: List<PaymentMode?>? = listOf()

    @SuppressLint("CheckResult")
    fun setStoreCreditMode(
        userId: String,
        cartId: String,
        storeCreditModeCode: String,
        amount: String,
        fields: String,
        lang: String
    ) {
        checkoutService.setStoreCreditModes(
            "current",
            "current",
            storeCreditModeCode,
            amount,
            fields,
            lang
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    setStoreCreditMode.postValue(true)
                    checkoutService.getPaymentModes("current", "current", fields).flatMap {
                        getPaymentMode.postValue(it.body())
                        try {
                            paymentModes = it.body()?.paymentModes
                            if ((it.body()?.paymentModes?.size
                                    ?: 0) == 1 && it.body()?.paymentModes?.get(0)?.code == "continue"
                            ) {
                                preSelectPaymentMode.postValue(
                                    it.body()?.paymentModes?.get(
                                        0
                                    )
                                )

                            }

                        } catch (e: Exception) {
                            Log.d(TAG, e.message ?: "")
                        }
                        cartService.getCart("current", "current", "FULL")

                    }
                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
            }
        }.get().doOnSubscribe {
            this.progressBarVisibility().value = View.VISIBLE
        }.doOnTerminate {

        }.subscribe({
            getCartResponse(it)
        },
            {
                Log.d(com.erabia.foodcrowd.android.common.extension.TAG, it.message ?: "")
                if (it.message?.contains("Unable to resolve host") == true) {
                    this.error().postValue("Please check your internet connection")
                    this.progressBarVisibility().value = View.GONE
                    this.hideRefreshLayout().value = true
                }
            })

    }

    fun setPaymentWithOutGetCart(paymentModeCode: String) {
        checkoutService.setPaymentModes(
            "current",
            "current",
            paymentModeCode,
            "FULL"
        ).get()
            .subscribe(this, {
                setPaymentMode.postValue(it.body())

            })

    }


    fun loadTimeSlot(userId: String, cartId: String, fields: String): LiveData<TimeSlot> {

        compositeDisposable.add(
            checkoutService?.getTimeSlot("current", "current", "FULL").get()
                .subscribe(this, {
                    getTimeSlotLiveData.value = it.body()
//                    loadCart("", "", Constants.FIELDS_FULL)
                })
        )
        return getTimeSlotLiveData
    }


//    fun setTimeSlot2(
//        userId: String,
//        cartId: String,
//        fields: String,
//        timeSlotRequest: TimeSlotRequest
//    ): LiveData<Boolean> {
//        compositeDisposable.add(
//            checkoutService.setTimeSlot("current", "current", "FULL", timeSlotRequest)
//                .get().subscribe(this,
//                    onSuccess_200 = {
//                        setTimeSlotLiveData.value = true
//                        loadCart("", "", Constants.FIELDS_FULL)
//
//                    }, onSuccess_201 = {
//                        setTimeSlotLiveData.value = true
//                        loadCart("", "", Constants.FIELDS_FULL)
//
//                    })
//        )
//        return setTimeSlotLiveData
//    }


    @SuppressLint("CheckResult")
    fun setTimeSlot(
        userId: String,
        cartId: String,
        fields: String,
        timeSlotRequest: TimeSlotRequest
    ) {
        checkoutService.setTimeSlot("current", "current", "FULL", timeSlotRequest)
            .flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        setTimeSlotLiveData.postValue(true)
                        cartService.getCart("current", cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                }
            }.get().subscribe(this, onSuccess_200 = {
//                getCartResponse(it)
            }
            )

    }

    fun loadCart(userId: String, cartId: String, fields: String): LiveData<Cart> {
        //TODO remove fake params
        compositeDisposable.add(
            cartService.getCart("current", "current", "FULL").get()
//                .delaySubscription(1000, TimeUnit.MILLISECONDS)
                .subscribe(this, {
                    this.cart = it.body()
                    cartLiveData.value = it.body()
                    appliedVoucher.value = it.body()?.appliedVouchers
                    val paymentMode = it.body()?.paymentMode
                    val timeSlotInfoData = it.body()?.timeSlotInfoDataData
                    if (paymentMode != null) {
                        if (paymentMode.code == "card") {
                            loadPaymentInfo(
                                Constants.CURRENT,
                                Constants.CURRENT,
                                Constants.FIELDS_FULL
                            )
                        }
                        preSelectPaymentMode.value = paymentMode
                        hasPaymentMethodSetToCart.value = true
//                setPaymentMode(
//                    Constants.CURRENT,
//                    Constants.CURRENT,
//                    paymentMode.code,
//                    Constants.FIELDS_FULL
//                )
                    }
                    if (timeSlotInfoData != null) {
                        hasTimeSlotSetToCart.value = true
                    }

                    if (CheckoutItemManager.isCartReadyToCheckout() && isTermsChecked) {
                        enableCheckoutButtonLiveData.postValue(true)
                    }
                    footerVisibilityLiveDate.value = View.VISIBLE

                })
        )
        return cartLiveData
    }

//    fun deleteVoucher2(userId: String, cartId: String, voucherId: String) {
//        cartService.deleteVoucher(userId, cartId, voucherId.toUpperCase()).get().subscribe(this, {
//            deleteVoucherSuccessLiveData.value = true
//            clearPromoCodeText.postValue(true)
//            loadCart(userId, cartId, Constants.FIELDS_FULL)
//        })
//    }
//

    @SuppressLint("CheckResult")
    fun deleteVoucher(
        userId: String = "current", cartId: String = "current", voucherId: String
    ) {
        cartService.deleteVoucher(userId, cartId, voucherId.toUpperCase())
            .flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        deleteVoucherSuccessLiveData.postValue(true)
                        clearPromoCodeText.postValue(true)
                        cartService.getCart(userId, cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                }
            }.get().subscribe(this, onSuccess_200 = {
                getCartResponse(it)
            }
            )

    }

//    fun applyVoucher2(userId: String, cartId: String, voucherId: String) {
//        cartService.applyVoucher(userId, cartId, voucherId).get().subscribe(this, {
//            applyVoucherSuccessLiveData.value = true
//            clearPromoCodeText.postValue(true)
//            loadCart(userId, cartId, Constants.FIELDS_FULL)
//        })
//    }

    @SuppressLint("CheckResult")
    fun applyVoucher(
        userId: String = "current", cartId: String = "current", voucherId: String
    ) {
        cartService.applyVoucher(userId, cartId, voucherId)
            .flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        applyVoucherSuccessLiveData.postValue(true)
                        clearPromoCodeText.postValue(true)
                        cartService.getCart(userId, cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                }
            }.get().subscribe(this, onSuccess_200 = {
                getCartResponse(it)
            }
            )

    }

    fun loadStoreCreditAmount(userId: String, fields: String): MutableLiveData<StoreCreditAmount> {
        compositeDisposable.add(
            checkoutService.getStoreCreditAmount(userId, fields).get().subscribe(this, {
                this.storeCreditAmount.value = it.body()
                if ((it.body()?.value ?: 0.0) == 0.0) {
                    storeCreditVisibility.value = View.GONE
                } else {
                    storeCreditVisibility.value = View.VISIBLE
                }
            })
        )
        return storeCreditAmount
    }

    fun loadPaymentInfo(
        userId: String,
        cartId: String,
        fields: String
    ): MutableLiveData<PaymentInfo> {

        compositeDisposable.add(
            checkoutService.getPaymentInfo(userId, cartId, fields)
                .get().doOnSubscribe {
//                    this.progressBarVisibility().value=View.VISIBLE
                }.doOnTerminate {

                }.doFinally {
                    Handler(Looper.getMainLooper()).postDelayed({
                        this.progressBarVisibility().value = View.GONE
                    }, 1000)
                }
                .subscribe({
                    paymentInfoLiveData.value = it.body()

                }, {
                    Log.d(TAG, it.message ?: "")
                    if (it.message?.contains("Unable to resolve host") == true) {
                        this.error().postValue("Please check your internet connection")
                        this.progressBarVisibility().value = View.GONE
                        this.hideRefreshLayout().value = true
                    }
                })

        )

        return paymentInfoLiveData
    }

    fun placeOrder(userId: String, cartId: String, fields: String) {
        var lang = LocaleHelper.getLanguage(MyApplication.getContext()!!)
        compositeDisposable.add(
            checkoutService.placeOrder("current", "current", "FULL", lang.toString())
                .get().subscribe(this, onSuccess_200 = {}, onSuccess_201 = {
                    placeOrderLiveData.value = it.body()
                    badgeNumber.value = 0
                })
        )
    }

    fun setDeliveryMode(userId: String, cartId: String, deliveryModeId: String) {
        compositeDisposable.add(
            checkoutService.setDeliveryMode(userId, cartId, deliveryModeId)
                .get().subscribe(this, onSuccess_200 = {
                    setDeliveryModeSuccessEvent.value = true
                }, onSuccess_201 = {
                    setDeliveryModeSuccessEvent.value = true
                })
        )
    }

//    @SuppressLint("CheckResult")
//    fun setPaymentDetails(
//        userId: String,
//        cartId: String,
//        fields: String,
//        body: PaymentDetailsRequest
//    ) {
//        checkoutService.setPaymentDetails(userId, cartId, fields, body)
//            .get().subscribe(this, onSuccess_200 = {
//                placeOrder(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
//            },onSuccess_201 = {
//                placeOrder(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
//            }, doOnSubscribe = {})
//    }
//
//
//

    @SuppressLint("CheckResult")
    fun setPaymentDetails(
        userId: String,
        cartId: String,
        fields: String,
        data: String
    ) {
        compositeDisposable.add(
            checkoutService.setPaymentDetails(userId, cartId, fields, data)
                .get().subscribe(this, onSuccess_200 = {
                    placeOrder(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
                }, onSuccess_201 = {
                    placeOrder(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
                })
        )
    }

    @SuppressLint("CheckResult")
    fun getAllCheckoutData(userId: String, cartId: String, deliveryModeId: String) {
        checkoutService.setDeliveryMode(
            userId,
            cartId,
            deliveryModeId
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    val getBankOfferObservable = cartService.getBankOffers(
                        "FULL"
                    ).map<ZipModelCheckoutAllData.SuccessBankOffer> {
                        ZipModelCheckoutAllData.SuccessBankOffer(it)
                    }

                    val getTimeSlotObservable = checkoutService.getTimeSlot(
                        userId, cartId, "FULL"
                    ).map<ZipModelCheckoutAllData.SuccessGetTimeSlot> {
                        ZipModelCheckoutAllData.SuccessGetTimeSlot(it)
                    }

                    val getStoreCreditAmountObservable = checkoutService.getStoreCreditAmount(
                        userId, "FULL"
                    ).map<ZipModelCheckoutAllData.SuccessStoreCreditAmount> {
                        ZipModelCheckoutAllData.SuccessStoreCreditAmount(it)
                    }

                    val getStoreCreditModesObservable = checkoutService.getStoreCreditModes(
                        "current", "current", "FULL"
                    ).map<ZipModelCheckoutAllData.SuccessStoreCreditModes> {
                        ZipModelCheckoutAllData.SuccessStoreCreditModes(it)
                    }


                    val getPaymentModesObservable = checkoutService.getPaymentModes(
                        "current", "current", "FULL"
                    ).map<ZipModelCheckoutAllData.SuccessPaymentModes> {
                        ZipModelCheckoutAllData.SuccessPaymentModes(it)
                    }
                    Observable.zip(
                        getBankOfferObservable,
                        getTimeSlotObservable,
                        getStoreCreditAmountObservable,
                        getStoreCreditModesObservable,
                        getPaymentModesObservable,
                        Function5 { t1: ZipModelCheckoutAllData.SuccessBankOffer,
                                    t5: ZipModelCheckoutAllData.SuccessGetTimeSlot,
                                    t3: ZipModelCheckoutAllData.SuccessStoreCreditAmount,
                                    t4: ZipModelCheckoutAllData.SuccessStoreCreditModes,
                                    t2: ZipModelCheckoutAllData.SuccessPaymentModes ->
                            val pairModel = MultiComponent(t1, t2, t3, t4, t5)
                            pairModel
                        }).flatMap {
                        when (it.first.data.code()) {
                            200, 201, 202 -> {
                                getBankOfferSuccess.postValue(it.first.data.body())
                            }
                            400, 401, 403 -> {
                                getBankOfferError.postValue(getResponseErrorMessage(it.first.data))
                                throw (RuntimeException(it.first.data.message()))
                            }
                            else -> {
                                getBankOfferError.postValue(getResponseErrorMessage(it.first.data))
                                throw (RuntimeException(it.first.data.message()))
                            }
                        }


                        when (it.second.data.code()) {
                            200, 201, 202 -> {
                                getPaymentMode.postValue(it.second.data.body())
                                try {
                                    if ((it.second.data.body()?.paymentModes?.size
                                            ?: 0) == 1 && it.second.data.body()?.paymentModes?.get(0)?.code == "continue"
                                    ) {
                                        preSelectPaymentMode.postValue(
                                            it.second.data.body()?.paymentModes?.get(
                                                0
                                            )
                                        )
                                        if (cart?.paymentMode?.code != "continue") {
                                            setPaymentMode(
                                                Constants.CURRENT,
                                                Constants.CURRENT,
                                                "continue",
                                                Constants.FIELDS_FULL
                                            )
                                        }
                                    }
                                } catch (e: Exception) {
                                    Log.d(TAG, e.message ?: "")
                                }
                            }
                            400, 401, 403 -> {
                                error().postValue(getResponseErrorMessage(it.second.data))
                                throw (RuntimeException(it.second.data.message()))
                            }
                            else -> {
                                error().postValue(getResponseErrorMessage(it.second.data))

                                throw (RuntimeException(it.second.data.message()))
                            }
                        }


                        when (it.third.data.code()) {
                            200, 201, 202 -> {
                                this.storeCreditAmount.postValue(it.third.data.body())
                                if ((it.third.data.body()?.value ?: 0.0) == 0.0) {
                                    storeCreditVisibility.postValue(View.GONE)
                                } else {
                                    storeCreditVisibility.postValue(View.VISIBLE)
                                }
                            }
                            400, 401, 403 -> {
                                error().postValue(getResponseErrorMessage(it.third.data))
                                throw (RuntimeException(it.third.data.message()))
                            }
                            else -> {
                                error().postValue(getResponseErrorMessage(it.third.data))

                                throw (RuntimeException(it.third.data.message()))
                            }
                        }

                        when (it.fourth.data.code()) {
                            200, 201, 202 -> {
                                getStoreCreditMode.postValue(it.fourth.data.body())
                            }
                            400, 401, 403 -> {
                                error().postValue(getResponseErrorMessage(it.fourth.data))
                                throw (RuntimeException(it.fourth.data.message()))
                            }
                            else -> {
                                error().postValue(getResponseErrorMessage(it.fourth.data))

                                throw (RuntimeException(it.fourth.data.message()))
                            }
                        }
                        when (it.five.data.code()) {
                            200, 201, 202 -> {
                                getTimeSlotLiveData.postValue(it.five.data.body())

                                cartService.getCart("current", "current", "FULL")
                            }
                            400, 401, 403 -> {
                                error().postValue(getResponseErrorMessage(it.five.data))
                                throw (RuntimeException(it.five.data.message()))
                            }
                            else -> {
                                error().postValue(getResponseErrorMessage(it.five.data))
                                throw (RuntimeException(it.five.data.message()))
                            }
                        }
                    }

                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }.get().subscribe(this, onSuccess_200 = {
            getCartResponse(it)
//            if (cart?.paymentMode?.code != "continue") {
//                setPaymentMode(
//                    Constants.CURRENT,
//                    Constants.CURRENT,
//                    cart?.paymentMode?.code ?: "",
//                    Constants.FIELDS_FULL
//                )
//            }

        }, onSuccess_201 = {
            getCartResponse(it)
//            if (cart?.paymentMode?.code != "continue") {
//                setPaymentMode(
//                    Constants.CURRENT,
//                    Constants.CURRENT,
//                    cart?.paymentMode?.code ?: "",
//                    Constants.FIELDS_FULL
//                )
//            }
        })

    }


    fun getCartResponse(cart: Response<Cart>) {
        this.cart = cart.body()
        cartLiveData.value = cart.body()
        appliedVoucher.value = cart.body()?.appliedVouchers
        val paymentMode = cart.body()?.paymentMode
        if (cart.body()?.storeCreditMode != null) {

            val storeCredit = cart.body()?.storeCreditMode

            preSelectStoreCreditMode.value = storeCredit
        }
        val timeSlotInfoData = cart.body()?.timeSlotInfoDataData
        if (paymentMode != null) {
            if (paymentMode.code == "card") {
                loadPaymentInfo(
                    Constants.CURRENT,
                    Constants.CURRENT,
                    Constants.FIELDS_FULL
                )
            } else {
                Handler(Looper.getMainLooper()).postDelayed({
                    this.progressBarVisibility().value = View.GONE
                }, 1000)
            }
            preSelectPaymentMode.value = paymentMode

            hasPaymentMethodSetToCart.value = true
//                setPaymentMode(
//                    Constants.CURRENT,
//                    Constants.CURRENT,
//                    paymentMode.code,
//                    Constants.FIELDS_FULL
//                )
        } else {
            Handler(Looper.getMainLooper()).postDelayed({
                this.progressBarVisibility().value = View.GONE
            }, 1000)
        }
        if (timeSlotInfoData != null) {
            hasTimeSlotSetToCart.value = true
        }

        if (CheckoutItemManager.isCartReadyToCheckout() && isTermsChecked) {
            enableCheckoutButtonLiveData.postValue(true)
        }
        footerVisibilityLiveDate.value = View.VISIBLE

    }

    fun dispose() {
        compositeDisposable.dispose()
    }

}