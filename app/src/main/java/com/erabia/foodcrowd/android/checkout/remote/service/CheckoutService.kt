package com.erabia.foodcrowd.android.checkout.remote.service

import com.erabia.foodcrowd.android.checkout.model.*
import com.erabia.foodcrowd.android.checkout.model.PaymentInfo
import com.erabia.foodcrowd.android.common.model.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.Query

interface CheckoutService {
    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/deliverytypes")
    fun getDeliveryTypes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<DeliveryTypes>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/deliverytype")
    fun setDeliveryTypes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("deliveryTypeCode") deliveryTypeCode: String,
        @Query("fields") fields: String
    ): Observable<Response<setDeliveryTypesResponse>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/paymentmodes")
    fun getPaymentModes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<PaymentModes>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/paymentmodes")
    fun setPaymentModes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("paymentModeCode") paymentModeCode: String,
        @Query("fields") fields: String
    ): Observable<Response<SetPaymentModeResponse>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/storecreditmodes")
    fun getStoreCreditModes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<StoreCreditModes>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/storecreditmodes")
    fun setStoreCreditModes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("storeCreditModeTypeCode") storeCreditModeTypeCode: String,
        @Query("amount") amount: String,
        @Query("fields") fields: String,
        @Query("lang") lang: String
    ): Observable<Response<Unit>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/timeSlot")
    fun getTimeSlot(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<TimeSlot>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/timeSlot")
    fun setTimeSlot(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String,
        @Body body: TimeSlotRequest
    ): Observable<Response<Unit>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/orders")
    fun placeOrder(
        @Path("userId") userId: String,
        @Query("cartId") cartId: String,
        @Query("fields") fields: String,
        @Query("lang") lang: String
    ): Observable<Response<PlaceOrderResponse>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/deliverymode")
    fun setDeliveryMode(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("deliveryModeId") deliveryModeId: String
    ): Observable<Response<Unit>>

    //
//    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/paymentdetails")
//    fun setPaymentDetails(
//        @Path("userId") userId: String,
//        @Path("cartId") cartId: String,
//        @Query("fields") fields: String,
//        @Body body: PaymentDetailsRequest
//    ): Observable<Response<PaymentDetailsResponse>>
//
//
    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/payment/transaction")
    fun setPaymentDetails(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String,
        @Query("data") data: String

    ): Observable<Response<PaymentDetailsResponse>>


    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/payment/info")
    fun getPaymentInfo(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String

    ): Observable<Response<PaymentInfo>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/storecredit/amount")
    fun getStoreCreditAmount(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<StoreCreditAmount>>

    @GET("rest/v2/foodcrowd-ae/config")
    fun getAppConfig(
        @Query("fields") fields: String
    ): Observable<Response<SiteConfig>>

}