package com.erabia.foodcrowd.android.referralcode.model


import com.google.gson.annotations.SerializedName

data class ReferralCodeResponse(

    @SerializedName("code")
    var code: String? = "",
    @SerializedName("valid")
    var valid: Boolean? = false,
    @SerializedName("newAppliedAmount")
    var newAppliedAmount: Double? = 0.0,
    @SerializedName("percentage")
    var percentage: Double? = 0.0,
    @SerializedName("totalAmount")
    var totalAmount: Double? = 0.0,
    @SerializedName("histories")
    var histories: List<Histories?> = listOf()


) {

    data class Histories(
        @SerializedName("amount")
        var amount: Double? = 0.0,
        @SerializedName("creationDate")
        var deactivationDate: String? = "",
        @SerializedName("customer")
        var historie: Customer = Customer()

    ) {

        data class Customer(
            @SerializedName("name")
            var firstName: String? = ""

        )
    }
}