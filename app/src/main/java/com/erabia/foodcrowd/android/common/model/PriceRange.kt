package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class PriceRange(
    @SerializedName("maxPrice")
    @TypeConverters(DataConverter::class)
    val maxPrice: MaxPrice = MaxPrice(),
    @SerializedName("minPrice")
    @TypeConverters(DataConverter::class)
    val minPrice: MinPrice = MinPrice()
)