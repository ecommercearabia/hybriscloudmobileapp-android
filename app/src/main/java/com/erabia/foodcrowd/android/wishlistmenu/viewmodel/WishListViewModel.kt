package com.erabia.foodcrowd.android.wishlistmenu.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.AddToCartRequest
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class WishListViewModel(val mWishListRepository: WishListRepository,val homeRepository: HomeRepository)  : ViewModel() {

    companion object {
        private const val TAG: String = "WishListViewModel"
    }
//var quantity=1
    var mGetWishListLiveData: LiveData<GetWishListResponse> = mWishListRepository.mGetWishListLiveData
    var mDeleteWishItemLiveData: LiveData<Void> = mWishListRepository.mDeleteWishItemLiveData
//    var mAddWishListLiveData: MutableLiveData<Void> = mWishListRepository.mAddWishListLiveData
    val loadingVisibility: MutableLiveData<Int> = mWishListRepository.loadingVisibility
    val mErrorEvent: SingleLiveEvent<String> = mWishListRepository.error()
    val mErrorServer: SingleLiveEvent<String> = mWishListRepository.mErrorServer
    private val compositeDisposable = CompositeDisposable()
    val addToCartSuccess = homeRepository.addToCartSuccess
    val createCartSuccess: BehaviorSubject<Cart> = BehaviorSubject.create()
    val progressBarVisibility: SingleLiveEvent<Int> = homeRepository.progressBarVisibility()


    fun getWishList(){
        mWishListRepository.getWishList()
    }

    fun deleteWishItem(productCode:String){
        mWishListRepository.deleteWishItem(productCode)
    }


//
//    fun plusFuncation(){
//        if (quantity < product?.stock?.stockLevel ?: 0) {
//
//            quantity++
//        }
//    }
//
//     fun minusFuncation(){
//         if (quantity > 1) {
//             quantity--
//         }
//    }






    fun onAddToCartClick(product: Product, quantity: Int = 1) {
        val addToCartRequest = AddToCartRequest(product = product, quantity = quantity)
        if (SharedPreference.getInstance().getUserToken() == null) {
            if (SharedPreference.getInstance().getGUID() == null) {

                compositeDisposable.add(homeRepository.createCartSuccess.subscribe({
                    SharedPreference.getInstance().setGUID(it?.guid ?: "")
                    homeRepository.addToCart(
                        Constants.ANONYMOUS,
                        SharedPreference.getInstance().getGUID() ?: "",
                        addToCartRequest,
                        Constants.FIELDS_FULL
                    )
                }, {
                    Log.d(WishListViewModel.TAG, it.message ?: "")
                }))
                createCart("anonymous", null, null, Constants.FIELDS_FULL)
            } else {
                homeRepository.addToCart(
                    Constants.ANONYMOUS,
                    SharedPreference.getInstance().getGUID() ?: "",
                    addToCartRequest,
                    Constants.FIELDS_FULL
                )
            }

        } else {
            homeRepository.addToCart(
                Constants.CURRENT,
                Constants.CURRENT,
                addToCartRequest,
                Constants.FIELDS_FULL
            )
        }
    }

    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String
    ): BehaviorSubject<Cart> {
        return homeRepository.createCart(userId, oldCartId, toMergeCartGuid, fields)
    }






    class Factory(
        val mWishListRepository: WishListRepository,
        val homeRepository: HomeRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return WishListViewModel(mWishListRepository,homeRepository) as T
        }
    }
}