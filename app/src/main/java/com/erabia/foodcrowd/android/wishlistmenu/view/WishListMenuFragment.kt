package com.erabia.foodcrowd.android.wishlistmenu.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.db.DataBaseManager
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentWishListMenuBinding
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.wishlistmenu.adapter.MyWishListAdapter
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService
import com.erabia.foodcrowd.android.wishlistmenu.viewmodel.WishListViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.*

class WishListMenuFragment : Fragment() {
    //    var myWishList: List<String> = emptyList()
    private var mMyWishListAdapter: MyWishListAdapter? = null
    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: WishListViewModel
    lateinit var binding: FragmentWishListMenuBinding
    var myWishList: List<GetWishListResponse.WishlistEntry?> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val homeService = RequestManager.getClient().create(HomeService::class.java)
        val cartService = RequestManager.getClient().create(CartService::class.java)
        val dataBase = DataBaseManager.getInstance(requireContext())

        val factory = WishListViewModel.Factory(
            WishListRepository(
                RequestManager.getClient().create(WishListService::class.java)
            ),
            HomeRepository(
                homeService
                , cartService, dataBase.homeDao()
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(WishListViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_wish_list_menu, container, false)
        binding.lifecycleOwner = this
        binding.mWishListViewModel = mViewModel
//        binding.orderDetails =  mDetailsResponse
        mViewModel.getWishList()

        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerGetWishList()
        observerError()
        observeAddToCart()
        observeProgressBar()

        binding.mContinueShoppingWishListButton.clicks().filterRapidClicks().subscribe {

            (activity as MainActivity).bottomNavigationView.selectedItemId = R.id.home

        }.addTo(compositeDisposable)

        binding.mAddAllButton.clicks().filterRapidClicks().subscribe {

            AppUtil.showToastyWarning(context ,R.string.coming_soon)
        }.addTo(compositeDisposable)

    }

    private fun observeAddToCart() {
        mViewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context ,R.string.added_to_cart_message)
            val mFirebaseAnalytics =  FirebaseAnalytics.getInstance(requireContext())

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.entry.product?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.entry.product?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.entry.product?.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }


            val addToCartParams = Bundle()

            it.entry.product?.price?.currencyIso?.let { it1 -> addToCartParams.putString(
                FirebaseAnalytics.Param.CURRENCY, it1 ?: "") }
            it.entry.product?.price?.value?.let { it1 -> addToCartParams.putDouble(FirebaseAnalytics.Param.VALUE, it1 ?: 0.0) }

            addToCartParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                arrayOf(productBundle)
            )

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, addToCartParams)

        })
    }

    private fun observeProgressBar() {
        mViewModel?.progressBarVisibility?.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })

    }
    fun observerGetWishList() {
        mViewModel.mGetWishListLiveData.observe(viewLifecycleOwner, Observer {
            myWishList = it.wishlistEntries
            if (myWishList.isEmpty()) {
                binding.mWishListGroup.visibility = View.GONE
                binding.mNoWishGroup.visibility = View.VISIBLE
            } else {
                binding.mWishListGroup.visibility = View.VISIBLE
                binding.mNoWishGroup.visibility = View.GONE
                mMyWishListAdapter =
                    MyWishListAdapter(
                        myWishList, mViewModel
                    )
                mMyWishListAdapter?.setHasStableIds(true)
                binding?.mMyWishListRecycleView?.adapter = mMyWishListAdapter
            }


        })
    }


    fun observerError() {
        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {

            AppUtil.showToastyError(context , it)

        })

        mViewModel.mErrorServer.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)

        })
    }

//    fun observerDeleteItem() {
//        mViewModel.mDeleteWishItemLiveData.observe(viewLifecycleOwner, Observer {
//
//
//        })
//
//
//    }


    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}