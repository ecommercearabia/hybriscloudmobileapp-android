package com.erabia.foodcrowd.android.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.model.AppliedVoucher
import com.erabia.foodcrowd.android.databinding.RowPromoCodeBinding

class PromoCodeAdapter(val cartViewModel: CheckoutViewModel) :
    RecyclerView.Adapter<PromoCodeAdapter.ViewHolder>() {


    lateinit var binding: RowPromoCodeBinding
    var promoCodeList: List<AppliedVoucher>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_promo_code,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return promoCodeList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.voucherModel = promoCodeList?.get(position)
        holder.itemBinding.viewModel = cartViewModel
    }

    override fun getItemId(position: Int): Long {
        return promoCodeList?.get(position)?.code.hashCode().toLong()
    }

    class ViewHolder(var itemBinding: RowPromoCodeBinding) :
        RecyclerView.ViewHolder(itemBinding.root)


}