package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

@Entity
data class ContentSlot(
    @SerializedName("components")
//    @TypeConverters(DataConverter::class)
    @Ignore
    val components: Components = Components(),
    @Ignore
    @SerializedName("name")
    val name: String = "",
    @Ignore
    @SerializedName("position")
    val position: String = "",
    @Ignore
    @SerializedName("slotId")
    val slotId: String = "",
    @Ignore
    @SerializedName("slotShared")
    val slotShared: Boolean = false,
    @Ignore
    @SerializedName("slotUuid")
    val slotUuid: String = ""
)