package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class DeliveryAddress(
    @SerializedName("cellphone")
    val cellphone: String = "",
    @SerializedName("companyName")
    val companyName: String = "",
    @SerializedName("country")
    val country: Country = Country(),
    @SerializedName("defaultAddress")
    val defaultAddress: Boolean = false,
    @SerializedName("district")
    val district: String = "",
    @SerializedName("email")
    val email: String = "",
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("formattedAddress")
    val formattedAddress: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("line1")
    val line1: String = "",
    @SerializedName("line2")
    val line2: String = "",
    @SerializedName("phone")
    val phone: String = "",
    @SerializedName("postalCode")
    val postalCode: String = "",
    @SerializedName("region")
    val region: Region = Region(),
    @SerializedName("shippingAddress")
    val shippingAddress: Boolean = false,
    @SerializedName("title")
    val title: String = "",
    @SerializedName("titleCode")
    val titleCode: String = "",
    @SerializedName("town")
    val town: String = "",
    @SerializedName("visibleInAddressBook")
    val visibleInAddressBook: Boolean = false
)