package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ProductBanner(
    @SerializedName("altText")
    val altText: String = "",
    @SerializedName("format")
    val format: String = "",
    @SerializedName("galleryIndex")
    val galleryIndex: Int = 0,
    @SerializedName("imageType")
    val imageType: String = "",
    @SerializedName("url")
    val url: String = ""
)