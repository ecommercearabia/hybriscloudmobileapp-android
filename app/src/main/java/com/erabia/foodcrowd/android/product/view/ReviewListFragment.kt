package com.erabia.foodcrowd.android.product.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentReviewListBinding
import com.erabia.foodcrowd.android.product.adapter.ProductImageBannerAdapter
import com.erabia.foodcrowd.android.product.adapter.ReviewListAdapter
import com.erabia.foodcrowd.android.product.di.DaggerIProductComponent
import com.erabia.foodcrowd.android.product.di.IProductComponent
import com.erabia.foodcrowd.android.product.di.ProductModule
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel
import javax.inject.Inject

class ReviewListFragment : Fragment() {
    private var selectedImagePosition: Int = 0
    var imagesList: List<Image>? = listOf()
    lateinit var mViewModel: ProductViewModel
    lateinit var binding: FragmentReviewListBinding
    private var mReviewListAdapter: ReviewListAdapter? = null
    lateinit var daggerComponent: IProductComponent
    private var productCode: String = ""
    lateinit var imageBannerAdapter: ProductImageBannerAdapter

    @Inject
    lateinit var factory: ProductViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        daggerComponent =
            DaggerIProductComponent.builder().productModule(context?.let { ProductModule(it) })
                .build()
        daggerComponent.inject(this)
        productCode = ""
        arguments?.let { productCode = it.getString(Constants.INTENT_KEY.PRODUCT_CODE) ?: "" }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_review_list, container, false)
        mViewModel = ViewModelProvider(this, factory).get(ProductViewModel::class.java)
        binding.viewModel = mViewModel
//        mViewModel.getProductDetails(productCode)
        mViewModel.getReviewDetails(productCode)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageBannerAdapter = ProductImageBannerAdapter(this, mViewModel)
        binding.viewPagerBanner.adapter = imageBannerAdapter
        binding.dots.attachViewPager(binding.viewPagerBanner)
        observeGetReviewList()
        observeProduct()
        observePreSelectedZoomImagePosition()
        observeNavigateToZoom()
        observeProgressBar()
        observeError()

    }

    private fun observeProgressBar() {
        mViewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = it
        })
    }


    private fun observeError() {

        mViewModel.error.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })

    }

    private fun observeGetReviewList() {


        mViewModel.productReviewLiveData.observe(viewLifecycleOwner, Observer {

            if (!it.reviews.isNullOrEmpty()) {
                binding.mReviewsGroup.visibility = View.VISIBLE
                binding.mNotFoundTextView.visibility = View.GONE
                mReviewListAdapter =
                    ReviewListAdapter(
                        it.reviews, mViewModel
                    )
                mReviewListAdapter?.setHasStableIds(true)
                binding?.mReviewRecyclerView?.adapter = mReviewListAdapter

            } else {
                binding.mReviewsGroup.visibility = View.GONE
                binding.mNotFoundTextView.visibility = View.VISIBLE
            }

        })
    }

    private fun observeProduct() {


        mViewModel.mProductLiveData.observe(viewLifecycleOwner, Observer {
            imageBannerAdapter = ProductImageBannerAdapter(this, mViewModel)
            binding.viewPagerBanner.adapter = imageBannerAdapter
            imageBannerAdapter.imagesList = it.images?.filter { it.format == "zoom" }
            binding.dots.attachViewPager(binding.viewPagerBanner)
            imagesList = it.images?.filter { it.format == "zoom" }
        })


    }

    private fun observeNavigateToZoom() {
        mViewModel.navigateToZoomEvent.observe(viewLifecycleOwner, Observer {
            findNavController()
                .navigate(
                    R.id.action_details_to_zoomFragment,
                    bundleOf(
                        "url" to it,
                        "imageList" to imagesList,
                        "selectedPosition" to selectedImagePosition
                    )
                )
        })

    }

    private fun observePreSelectedZoomImagePosition() {
        mViewModel.preSelectedZoomImagePosition.observe(viewLifecycleOwner, Observer {
            selectedImagePosition = it
        })

    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

}