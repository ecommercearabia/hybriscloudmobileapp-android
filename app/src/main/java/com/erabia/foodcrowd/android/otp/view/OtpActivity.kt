package com.erabia.foodcrowd.android.otp.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityOtpBinding
import com.erabia.foodcrowd.android.home.remote.service.LoginService
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.otp.remote.repository.OtpRepository
import com.erabia.foodcrowd.android.otp.remote.service.OtpService
import com.erabia.foodcrowd.android.otp.viewmodel.OtpVIewModel
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import com.jakewharton.rxbinding3.view.clicks
import com.poovam.pinedittextfield.PinField
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class OtpActivity : AppCompatActivity() {
    private lateinit var viewModel: OtpVIewModel
    private lateinit var binding: ActivityOtpBinding
    var compositeDisposable = CompositeDisposable()

    var mCode: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp)
        val mobile_number = intent.getStringExtra("mobile_number") ?: ""
        val countryisoCode = intent.getStringExtra("countryisoCode") ?: ""
        var first_name = intent.getStringExtra("first_name") ?: ""
        var last_name = intent.getStringExtra("last_name") ?: ""
        var email = intent.getStringExtra("email") ?: ""
        var password = intent.getStringExtra("password") ?: ""
        var mTitle = intent.getStringExtra("title") ?: ""
        var mNationality = intent.getStringExtra("nationality") ?: ""
        var mReferralCode = intent.getStringExtra("referral_code")
        val factory = OtpVIewModel.Factory(
            OtpRepository(RequestManager.getClient().create(OtpService::class.java)),
            SignUpRepository( RequestManager.getClient().create(SignUpService::class.java)),
            LoginRepository( RequestManager.getClient().create(LoginService::class.java)
            ,  RequestManager.getClient().create(CartService::class.java))
        )
        viewModel = ViewModelProvider(this, factory).get(OtpVIewModel::class.java)
        binding.lifecycleOwner = this
        binding.mOtpVIewModel = viewModel

        viewModel.sendOtp(countryisoCode, mobile_number)

        mVerifyButton.clicks().filterRapidClicks().subscribe {
            mCode = mPinEditText.text.toString()
            viewModel.verifyOtp(mCode, countryisoCode, mobile_number)

        }.addTo(compositeDisposable)

        mSignUpTextView.clicks().filterRapidClicks().subscribe {
            viewModel.sendOtp(countryisoCode, mobile_number)
        }.addTo(compositeDisposable)

        mBackImageView.clicks().filterRapidClicks().subscribe {
            finish()

        }.addTo(compositeDisposable)

        viewModel.mVerifyOtpLiveData.observe(this, Observer {
            viewModel.doRegister(first_name,last_name,password,mTitle,mNationality,email,countryisoCode, mobile_number , mReferralCode ?:"")
        })
        viewModel.mErrorOtpEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)

        })
        viewModel.mErrorSignUpEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)

        })
        viewModel.mErrorLoginEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)
        })

        viewModel.mRegisterLiveData.observe(this, Observer {
            AppUtil.showToastySuccess( this , R.string.success)
            viewModel.doLogin(email,password)
        })

        viewModel.mLoginLiveData.observe(this, Observer {
            Log.e("mmmmmmmmm", it.refreshToken ?: "")
            SharedPreference.getInstance().setRefreshToken(it.refreshToken ?: "")
            SharedPreference.getInstance().setUser_TOKEN(it.accessToken ?: "")
            SharedPreference.getInstance().setApp_TOKEN(it.accessToken ?: "")
//            SharedPreference.getInstance().setLOGIN_User(email)
            SharedPreference.getInstance().setLOGIN_EMAIL(email)
            SharedPreference.getInstance().setLOGIN_EMAIL_FINGERPRINT(email)
            if(intent.getStringExtra("referral_code").isNullOrEmpty()){
                startActivityForResult(Intent(this, ThanksActivity::class.java),99)
            }
            else {
                startActivityForResult(Intent(this, ThanksReferral::class.java),99)

            }

        })

        val listener = object : PinField.OnTextCompleteListener {
            override fun onTextComplete(enteredText: String): Boolean {

                if (enteredText.length == 4) {
                    mVerifyButton.isEnabled = true
                }

                return@onTextComplete true
            }
        }
        mPinEditText.onTextCompleteListener = listener


        mPinEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

                if (s.toString().length < 4) {
                    mVerifyButton.isEnabled = false
                }
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }
    private fun observerRegistration() {


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==99&&resultCode== Activity.RESULT_OK){
            setResult(Activity.RESULT_OK)
            finish()

        }
    }
    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}