package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class Classification(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("features")
    @TypeConverters(DataConverter::class)
    val features: List<Feature> = listOf(),
    @SerializedName("name")
    val name: String = ""


)