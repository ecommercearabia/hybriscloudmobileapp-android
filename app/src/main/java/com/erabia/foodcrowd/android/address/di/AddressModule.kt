package com.erabia.foodcrowd.android.address.di

import com.erabia.foodcrowd.android.address.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.address.remote.service.AddressService
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import dagger.Module
import dagger.Provides

@Module
class AddressModule {

    @Provides
    fun addressRepository() =
        AddressRepository(RequestManager.getClient().create(AddressService::class.java))



    @Provides
    fun signUpRepository() =
        SignUpRepository(RequestManager.getClient().create(SignUpService::class.java))



    @Provides
    fun addressViewModel() = AddressViewModel()


}