package com.erabia.foodcrowd.android.storecredit.remote.service

import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface StoreCreditService {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/storecredit/amount")
    fun getAvailableBalance(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<AvailableBalanceResponse>>



    @GET("rest/v2/foodcrowd-ae/users/{userId}/storecredit/history")
    fun getStoreHistory(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<StoreCreditListResponse>>



}