package com.erabia.foodcrowd.android.home.viewmodel

import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.home.remote.repository.HighlightedProductRepository

class HighLightedProductsViewModel(
    private val highlightedProductRepository: HighlightedProductRepository
) {
    val productLiveData: MutableLiveData<Product> = highlightedProductRepository.productLiveData
    suspend fun getProduct(id: String) {
        highlightedProductRepository.getProduct(id)
    }
}
