package com.erabia.foodcrowd.android.newaddress.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.newaddress.model.*
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse

class EditAddressViewModel(val mAddressRepository: AddressRepository) : ViewModel() {
    val loadingVisibility: MutableLiveData<Int> = mAddressRepository.loadingVisibility
    var mCountryListLiveData: LiveData<List<CountryResponse.Country>> = mAddressRepository.mCountryListLiveData
    val mMobileCountryListLiveData: LiveData<List<MobileCountryResponse.Country>> =
        mAddressRepository.mMobileCountryListLiveData
    val mCityListLiveData: LiveData<CityListResponse> = mAddressRepository.mCityListLiveData
    val mAreaListLiveData: LiveData<AreaListResponse> = mAddressRepository.mAreaListLiveData

    val mTitleListLiveData: LiveData<TitleResponse> = mAddressRepository.mTitleListLiveData
    val mObserverEndLiveData: LiveData<Int> = mAddressRepository.mObserverEndLiveData

    val mErrorEvent: LiveData<String> = mAddressRepository.mErrorEvent

    val updateAddressEvent: LiveData<Int> = mAddressRepository.updateAddressEvent

    val mErrorAreaAndCreateEvent: SingleLiveEvent<String> = mAddressRepository.error()
    val resetAndHideAdapter: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    //Start textWatcher after we set street name from previous fragment
    val startPlaceTextWatcher: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    var flagArea = true
    var mTitleCode = ""
    var mTitleName = ""
    var mCitySelection = CityListResponse.City()
    var mCountrySelection = CountryResponse.Country()
    val mAreaSelection: MutableLiveData<AreaListResponse.Area> by lazy { MutableLiveData<AreaListResponse.Area>() }
    val areaListForPickerLiveData = MutableLiveData<AreaListResponse?>()
    var mMobileCodeSelection = MobileCountryResponse.Country()
    var mMobileNumber: String = ""
    var mLineOne: String = ""
    var mLineTwo: String = ""
    var mAddressName: String = ""
    var mFirstName: String = ""
    var mLastName: String = ""
    var mNearestLand: String = ""
    var mStreetNumber: String = ""
    var mBuildingNumber: String = ""
    var mApartmentNumber: String = ""
    var mAddressId: String = ""
    var mDefaultAddressChecked: Boolean = false
    var mNavigateToMap = SingleLiveEvent<Int>()

    var mAreaCodeFromPreviousFragment = ""
    lateinit var address: Address

    val mEnableButton: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }


    fun getAllDataSpinner() {
        mAddressRepository.getAllDataSpinnerForEditAddress()
    }

    fun setAddressFromPreviousFragment(address: Address) {
        this.address = address
        mAreaCodeFromPreviousFragment = address.area?.code ?: ""
        mAddressName = address.addressName ?: ""
        mFirstName = address.firstName ?: ""
        mLastName = address.lastName ?: ""
        mLineOne = address.line1 ?: ""
        mLineTwo = address.line2 ?: ""
        mNearestLand = address.nearestLandmark ?: ""
        mStreetNumber = address.streetName ?: ""
        mBuildingNumber = address.buildingName ?: ""
        mApartmentNumber = address.apartmentNumber ?: ""
        mMobileNumber = AppUtil.getMobileNumberWithoutIsoCode(address.mobileNumber ?: "")
        mAddressId = address.id ?: ""
//        emptyText()
        startPlaceTextWatcher.value = true
    }

    fun setAddressMap(lineOne: String, addressID: String) {
        mLineOne = lineOne ?: ""
        mAddressId = addressID ?: ""
        flagArea = false
//        emptyText()
    }


    fun onSelectItemTitle(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        mTitleCode =
            (pos?.let { it1 -> parent?.adapter?.getItem(it1) } as TitleResponse.Title).code.toString()
        mTitleName =
            (pos.let { it1 -> parent?.adapter?.getItem(it1) } as TitleResponse.Title).name.toString()
    }


    fun onSelectItemCountry(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        mCountrySelection =
            (pos?.let { it1 -> parent?.adapter?.getItem(it1) } as CountryResponse.Country)

        if (!mCountrySelection.isocode.isNullOrEmpty())
            mCountrySelection.isocode?.let { mAddressRepository.getCityListAndArea(it) }
    }


    fun onSelectItemCity(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        // if there is bug time this should be change //
        mCitySelection = parent?.adapter?.getItem(pos) as CityListResponse.City

        if (!mCitySelection.code.isNullOrEmpty())
            mCitySelection.code?.let {
                mCountrySelection.isocode?.let { it1 ->
                    mAddressRepository.getArea(
                        it1, it
                    )
                }
            }
    }

//    fun onSelectItemArea(
//        parent: AdapterView<*>?,
//        spinner: Spinner?,
//        pos: Int,
//        id: Long
//    ) {
//
//        if (!(pos?.let { it1 -> parent?.adapter?.getItem(it1) } as AreaListResponse.Area).code.isNullOrEmpty()) {
//            if (flagArea) {
//                val pos =
//                    mAreaListLiveData.value?.areas?.indexOfFirst { area ->
//                        area?.code == mAreaCodeFromPreviousFragment
//                    }
//                if (pos != -1) {
//                    spinner?.setSelection(pos ?: 0)
//                    mAreaSelection = parent?.adapter?.getItem(pos ?: 0) as AreaListResponse.Area
//
//                    flagArea = false
//                }
//            } else {
//                mAreaSelection = parent?.adapter?.getItem(pos) as AreaListResponse.Area
//            }
//        }
//    }

    fun onSelectMobileNumber(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        mMobileCodeSelection = parent?.adapter?.getItem(pos) as MobileCountryResponse.Country
    }

    fun onCheckedChange(button: CompoundButton?, check: Boolean) {
        Log.d("Z1D1", "onCheckedChange: $check")
        mDefaultAddressChecked = check
    }

    fun emptyText(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (mFirstName.isNotEmpty()
                    && mLastName.isNotEmpty()
                    && mMobileNumber.isNotEmpty()
//                    && mLineOne.isNotEmpty()
                    && mAddressName.isNotEmpty()
                    && mNearestLand.isNotEmpty()
                    && mStreetNumber.isNotEmpty()
//                    && mBuildingNumber.isNotEmpty()
                    && mApartmentNumber.isNotEmpty()
                ) {
                    mEnableButton.postValue(true)

                } else {
                    mEnableButton.postValue(false)

                }
            }


            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }

    fun updateAddress() {
        var address = AddressRequest()

        address.addressName = mAddressName
        address.apartmentNumber = mApartmentNumber
//        address.buildingName = mBuildingNumber
        address.defaultAddress = mDefaultAddressChecked
        address.firstName = mFirstName
        address.lastName = mLastName
        address.line1 = mStreetNumber
//        address.line2 = mLineTwo
        address.mobileNumber = mMobileNumber
        address.nearestLandmark = mNearestLand
        address.streetName = mStreetNumber
        address.title = mTitleName
        address.titleCode = mTitleCode

        var area = AddressRequest.Area()
        area.code = mAreaSelection.value?.code
        area.name = mAreaSelection.value?.name

        address.city?.code = mCitySelection.code
        address.city?.name = mCitySelection.name
        address.city?.areas = mCitySelection.areas

        address.mobileCountry?.isdcode = mMobileCodeSelection.isdcode
        address.mobileCountry?.isocode = mMobileCodeSelection.isocode
        address.mobileCountry?.name = mMobileCodeSelection.name

        var country = AddressRequest.Country()
        country.name = mCountrySelection.name
        country.isocode = mCountrySelection.isocode
        country.isdcode = mCountrySelection.isdcode

        address.country = country
        address.city = address.city
        address.area = area
        address.mobileCountry = address.mobileCountry

        mAddressRepository.updateAddress(mAddressId, address)
    }

    fun navigateToMap() {
        mNavigateToMap.postValue(1)
    }

    fun onContainerLayoutClick() {
        resetAndHideAdapter.postValue(true)
    }

    class Factory(
        val mAddressRepository: AddressRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return EditAddressViewModel(mAddressRepository) as T
        }
    }

    fun areaListForPicker() {
        areaListForPickerLiveData.postValue(mAreaListLiveData.value)
    }

}