package com.erabia.foodcrowd.android.product.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.PagerAdapter
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.databinding.BannersDetailsRowItemBinding
import com.erabia.foodcrowd.android.databinding.BannersRowItemBinding
import com.erabia.foodcrowd.android.product.view.ProductDetailsFragment
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel

class ProductImageBannerAdapter(val fragment: Fragment, val viewModel: ProductViewModel) :
    PagerAdapter() {
    private lateinit var context: Context

    companion object {
        const val TAG = "ProductImageBanner"
    }

    //in order to show offline cached image , we have to pass size of list to instansiate items
    //inside the pager adapter, then glide will display the  cached images
    var size: Int = 0
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var imagesList: List<Image>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun getCount(): Int {
        if (imagesList != null && imagesList?.count() != null)
            return imagesList?.count()!!
        else return 0
//        return size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        context = container.context
        var image: Image? = null
        val itemBinding: BannersDetailsRowItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.banners_details_row_item, container, false
        )
        itemBinding.viewModel = viewModel
        try {
            image = imagesList?.get(position) ?: Image()
        } catch (e: IndexOutOfBoundsException) {
            Log.d(TAG, e.message ?: "")
        }
        itemBinding.url = image?.url
        container.addView(itemBinding.root)
        return itemBinding.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}