package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class AddToCartResponse(
    @SerializedName("deliveryModeChanged")
    val deliveryModeChanged: Boolean = false,
    @SerializedName("entry")
    val entry: Entry = Entry(),
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("quantityAdded")
    val quantityAdded: Int = 0,
    @SerializedName("statusCode")
    val statusCode: String = "",
    @SerializedName("statusMessage")
    val statusMessage: String = ""
)