package com.erabia.foodcrowd.android.verificationcode.remote.repository

import android.util.Log
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.SendEmailOtp
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.service.LoginWithOtpService
import com.erabia.foodcrowd.android.home.remote.service.VerificationService
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository

class VerificationRepository(
    private val verificationService: VerificationService,
    private val cartService: CartService
) :
    Repository {
    val mLoginLiveData: SingleLiveEvent<TokenModel> by lazy { SingleLiveEvent<TokenModel>() }
    private var guid: String = ""

    companion object {
        const val TAG: String = "LoginWithOtpRepository"
    }

    fun verify(code: String, email: String, name: String) {
        verificationService.verify(code, email, name).get()
            .subscribe(this, {
                when (it.code()) {
                    200 -> {
                        mLoginLiveData.value = it.body()
                        if (SharedPreference.getInstance().getGUID()?.isNotEmpty() == true) {

                            cartService.getCart(
                                Constants.CURRENT,
                                Constants.CURRENT,
                                "FULL"
                            ).get().subscribe(this,
                                {
                                    this.guid = it.body()?.guid ?: ""
                                    cartService.createCart(
                                        Constants.CURRENT,
                                        SharedPreference.getInstance().getGUID(),
                                        guid,
                                        Constants.FIELDS_FULL
                                    ).get().subscribe({
                                        Log.d(LoginRepository.TAG, "cart created")
                                    }, {
                                        Log.d(LoginRepository.TAG, it.message ?: "")
                                    })
                                }, onError_400 = {
                                    val errorResponse = getResponseErrorMessage(it)
                                    if (
                                        errorResponse.contains("No cart created yet.") ||
                                        errorResponse.contains("Cart not found.")
                                    ) {
                                        cartService.createCart(
                                            Constants.CURRENT,
                                            "",
                                            "",
                                            Constants.FIELDS_FULL
                                        ).get().subscribe({
                                            Log.d(LoginRepository.TAG, "cart created")
                                        }, {
                                            Log.d(LoginRepository.TAG, it.message ?: "")
                                        })
                                    }
                                })
                        }
                    }
                }
            }, onSuccess_201 = {
                mLoginLiveData.value = it.body()
            })
    }


}
