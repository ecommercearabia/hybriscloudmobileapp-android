package com.erabia.foodcrowd.android.cart.remote.repository

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response


class CartRepository(private val cartService: CartService) : Repository {
    companion object {
        const val TAG = "CartRepository"
    }

    val deleteVoucherSuccessLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val applyVoucherSuccessLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val clearPromoCodeText: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val deleteEntrySuccessLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val appliedVoucher: MutableLiveData<List<AppliedVoucher>> by lazy { MutableLiveData<List<AppliedVoucher>>() }
    val cartLiveData: MutableLiveData<Cart> by lazy { MutableLiveData<Cart>() }
    val receivedPromotionVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val potentialPromotionVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val receivedPromotionMessage: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val potentialPromotionMessageList: MutableLiveData<List<PotentialOrderPromotion>> by lazy { MutableLiveData<List<PotentialOrderPromotion>>() }
    val cartVaildationLiveData: SingleLiveEvent<CartVaildationResponse> by lazy { SingleLiveEvent<CartVaildationResponse>() }
    val createCartSuccess: BehaviorSubject<Cart> = BehaviorSubject.create()
    var emptyCartVisibility: SingleLiveEvent<Int> = SingleLiveEvent()
    val checkoutButtonVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val footerVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val promoCodeVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val addToCartSuccess: SingleLiveEvent<AddToCartResponse> by lazy { SingleLiveEvent<AddToCartResponse>() }
    val getBankOfferSuccess: SingleLiveEvent<BankOfferResponse> by lazy { SingleLiveEvent<BankOfferResponse>() }
    val getShipmentTypesCONFIGSuccessfully: SingleLiveEvent<SiteConfig> by lazy { SingleLiveEvent<SiteConfig>() }
    val getBankOfferError: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val badgeNumber: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val loadingVisibility: SingleLiveEvent<Int> = SingleLiveEvent()
    val shipmentTypesLiveData: SingleLiveEvent<ShipmentTypes> by lazy { SingleLiveEvent<ShipmentTypes>() }
    val updateShipmentTypesLiveData: SingleLiveEvent<UpdateShipmentTypes> by lazy { SingleLiveEvent<UpdateShipmentTypes>() }
    val selectedShipmentTypesLiveData: SingleLiveEvent<SupportedShipmentType> by lazy { SingleLiveEvent<SupportedShipmentType>() }
    var code2 = ""
    var code = ""

    // send type to put api
    @SuppressLint("CheckResult")
    fun updateShipmentTypes(userId: String, cartId: String) {
        code = SharedPreference.getInstance().getShipmentType() ?: ""
        code2 = if (code == "0") {
            "DELIVERY"
        } else {
            "PICKUP_IN_STORE"
        }
        cartService.updateShipmentTypes(userId, "FULL", code2)
            .get()
            .subscribe({
                when (it.code()) {
                    200, 201 -> {
                        updateShipmentTypesLiveData.value = it.body()
                        // delete voucher if PICKUP_IN_STORE
                        if (code2 == "PICKUP_IN_STORE"){
                            var voucher =  SharedPreference.getInstance().getVoucherID()
                            if (voucher != null && voucher != ""){
                                deleteVoucherShipment(userId, cartId, voucher)
                            }
                        }
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d("CartRepo", it.toString())
                })
    }

    @SuppressLint("CheckResult")
    fun loadAppConfig() {
        cartService.getAppConfig("FULL").get().subscribe(this, onSuccess_200 = {
            getShipmentTypesCONFIGSuccessfully.value = it.body()
            var pickupFlag = it.body()?.pickupInStoreEnabled
            if (pickupFlag != null) {
                SharedPreference.getInstance().setPickupInStoreEnabled(pickupFlag)
            }
        }, onError_400 = {
            getBankOfferError.postValue(getResponseErrorMessage(it))
        })
    }

    // get shipment type
    @SuppressLint("CheckResult")
    fun loadCartShipmentTypes(userId: String, cartId: String): LiveData<ShipmentTypes> {
        cartService.getShipmentTypes(userId, cartId, "FULL").get()
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201 -> {
                            shipmentTypesLiveData.value = it.body()
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d("car repository", it.toString())
                }
            )
        return shipmentTypesLiveData
    }

    // check if there is any preselected
    @SuppressLint("CheckResult")
    fun loadSelectedShipmentType(userId: String, cartId: String): LiveData<SupportedShipmentType> {
        cartService.getSelectedShipmentType(userId, cartId, "FULL").get()
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201 -> {
                            selectedShipmentTypesLiveData.value = it.body()

                            if (it.body()?.code != null || it.body()?.code != "") {
                                if (it?.body()?.code == "PICKUP_IN_STORE") {
                                    SharedPreference.getInstance().setShipmentType("1")
                                } else {
                                    SharedPreference.getInstance().setShipmentType("0")
                                }
                            }
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d("SelectedShipment ", it.toString())
                }
            )
        return selectedShipmentTypesLiveData
    }

    @SuppressLint("CheckResult")
    fun loadCart(userId: String, cartId: String, fields: String): LiveData<Cart> {
        cartService.getCart(userId, cartId, "FULL").get().subscribe(this, {
            loadSelectedShipmentType(userId, cartId)
//            loadCartShipmentTypes(userId)
            SharedPreference.getInstance().setCART_ID(it.body()?.code ?: "")
            SharedPreference.getInstance().setGUID(it.body()?.guid ?: "")
            cartLiveData.value = it.body()
            if ((it.body()?.entries?.size ?: 0) == 0) {
                emptyCartVisibility.value = View.VISIBLE
                checkoutButtonVisibility.value = View.GONE
                footerVisibility.value = View.GONE
                promoCodeVisibility.value = View.GONE
                badgeNumber.value = 0

            } else {
                badgeNumber.value = it.body()?.entries?.size
                emptyCartVisibility.value = View.GONE
                checkoutButtonVisibility.value = View.VISIBLE
                footerVisibility.value = View.VISIBLE
                promoCodeVisibility.value = View.VISIBLE

                appliedVoucher.value = it.body()?.appliedVouchers

                if ((it.body()?.appliedOrderPromotions?.size ?: 0) > 0 &&
                    it.body()?.appliedOrderPromotions?.get(0)?.description?.isNotEmpty() == true
                ) {
                    receivedPromotionVisibility.value = View.VISIBLE
                    receivedPromotionMessage.value =
                        it.body()?.appliedOrderPromotions?.get(0)?.description
                } else {
                    receivedPromotionVisibility.value = View.GONE
                }



                if ((it.body()?.potentialOrderPromotions?.size ?: 0) > 0 &&
                    it.body()?.potentialOrderPromotions?.get(0)?.description?.isNotEmpty() == true
                ) {
                    potentialPromotionVisibility.value = View.VISIBLE
                    potentialPromotionMessageList.value =
                        it.body()?.potentialOrderPromotions
                } else {
                    potentialPromotionVisibility.value = View.GONE
                }


            }
        }, onError_400 = {
            val errorResponse = getResponseErrorMessage(it)
            if (
                errorResponse.contains("No cart created yet.") ||
                errorResponse.contains("Cart not found.")
            ) {
                emptyCartVisibility.value = View.VISIBLE
                checkoutButtonVisibility.value = View.GONE

                createCart(
                    userId,
                    SharedPreference.getInstance().getGUID(),
                    null,
                    "FULL"
                ).subscribe({
                    cartService?.getCart(userId, cartId, "FULL").get().subscribe(this, {
                        cartLiveData.value = it.body()
                    }, onError_400 = {

                    })
                }, {
                    Log.d(TAG, it.message ?: "")
                    if (it.message?.contains("Unable to resolve host") == true) {
                        this.error().postValue("Please check your internet connection")
                        this.progressBarVisibility().value = View.GONE
                        this.hideRefreshLayout().value = true
                    }
                })

            }
        })
        return cartLiveData
    }


    @SuppressLint("CheckResult")
    fun doValidation(userId: String, cartId: String) {
        cartService.doValidation(userId, cartId, "FULL" , LocaleHelper.getLanguage(MyApplication.getContext()!!).toString() )
            .get().subscribe(this, {
                cartVaildationLiveData.value = it.body()
            },
                onError_400 = {
                    error().postValue(getResponseErrorMessage(it))
//                    loadCart(userId, cartId, Constants.FIELDS_FULL)
                }
            )
    }

//    fun deleteEntry2(userId: String, cartId: String, entry: Entry?, fields: String) {
//        cartService.deleteEntry(
//            userId,
//            cartId,
//            entry?.entryNumber ?: -1,
//            fields
//        ).get().subscribe(this, {
//            deleteEntrySuccessLiveData.value = true
//        })
//    }

    @SuppressLint("CheckResult")
    fun deleteEntry(userId: String, cartId: String, entry: Entry?, fields: String) {
        cartService.deleteEntry(
            userId,
            cartId,
            entry?.entryNumber ?: -1,
            fields
        ).flatMap {

            when (it.code()) {
                200, 201, 202 -> {
                    val mFirebaseAnalytics =
                        FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                    var productsBundle = arrayOf<Bundle?>()

                    val productBundle = Bundle().apply {
                        putString(
                            FirebaseAnalytics.Param.ITEM_ID,
                            entry?.entryNumber.toString()
                        )
                        putString(FirebaseAnalytics.Param.ITEM_NAME, entry?.product?.name)
                        entry?.basePrice?.value?.let { it1 ->
                            putDouble(
                                FirebaseAnalytics.Param.PRICE,
                                it1 ?: 0.0
                            )
                        }

                    }
                    productsBundle = append(productsBundle, productBundle)

                    val removeFromCartParams = Bundle()
                    removeFromCartParams.putString(
                        FirebaseAnalytics.Param.CURRENCY,
                        entry?.basePrice?.currencyIso ?: ""
                    )
                    entry?.totalPrice?.value.let { it1 ->
                        removeFromCartParams.putDouble(
                            FirebaseAnalytics.Param.VALUE,
                            it1!! ?: 0.0
                        )
                    }
                    removeFromCartParams.putParcelableArray(
                        FirebaseAnalytics.Param.ITEMS,
                        productsBundle
                    )

                    mFirebaseAnalytics.logEvent(
                        FirebaseAnalytics.Event.REMOVE_FROM_CART,
                        removeFromCartParams
                    )

                    deleteEntrySuccessLiveData.postValue(true)
                    cartService.getCart(userId, cartId, "FULL")


                }
                400, 401, 402, 403 -> {

                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }.get().doOnTerminate {


        }
            .doOnSubscribe {
                this.progressBarVisibility().value = View.VISIBLE

            }.doFinally {
                Handler(Looper.getMainLooper()).postDelayed({
                    this.progressBarVisibility().value = View.GONE

                }, 300)
            }
            .subscribe({
                getCartResponse(it, userId, cartId)
            }, {
                Log.d(TAG, it.message ?: "")
                if (it.message?.contains("Unable to resolve host") == true) {
                    this.error().postValue("Please check your internet connection")
                    this.progressBarVisibility().value = View.GONE
                    this.hideRefreshLayout().value = true
                }
            })
    }

//
//    fun deleteVoucher2(userId: String, cartId: String, voucherId: String) {
//        cartService.deleteVoucher(userId, cartId, voucherId).get().subscribe(this, {
//            deleteVoucherSuccessLiveData.value = true
//            loadCart(userId, cartId, Constants.FIELDS_FULL)
//        })
//    }

    @SuppressLint("CheckResult")
    fun deleteVoucherShipment(
        userId: String = "current", cartId: String = "current", voucherId: String
    ) {
        cartService.deleteVoucher(userId, cartId, voucherId.toUpperCase())
            .flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        clearPromoCodeText.postValue(true)
                        cartService.getCart(userId, cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                }
            }.get().subscribe(this, onSuccess_200 = {
                getCartResponse(it, userId, cartId)
            }
            )
    }


    @SuppressLint("CheckResult")
    fun deleteVoucher(userId: String, cartId: String, voucherId: String) {
        cartService.deleteVoucher(userId, cartId, voucherId)
            .flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        deleteVoucherSuccessLiveData.value = true
                        cartService.getCart(userId, cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        throw (RuntimeException("Something wrong happened"))

                    }
                }
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(this, {
                getCartResponse(it, userId, cartId)
            }
            )
    }
//
//    fun applyVoucher2(userId: String, cartId: String, voucherId: String) {
//        cartService.applyVoucher(userId, cartId, voucherId).get().subscribe(this, {
//            applyVoucherSuccessLiveData.value = true
//            clearPromoCodeText.postValue(true)
//            loadCart(userId, cartId, Constants.FIELDS_FULL)
//        })
//    }
//

    @SuppressLint("CheckResult")
    fun applyVoucher(userId: String, cartId: String, voucherId: String) {
        cartService.applyVoucher(userId, cartId, voucherId).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    applyVoucherSuccessLiveData.value = true
                    clearPromoCodeText.postValue(true)
                    cartService.getCart(userId, cartId, "FULL")
                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }.get().subscribe(this, {
            getCartResponse(it, userId, cartId)

        }
        )
    }

    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String,
        isAddToCart: Boolean = false,
        cartId: String = "",
        entry: AddToCartRequest? = null
    ): BehaviorSubject<Cart> {
        cartService.createCart(userId, oldCartId, toMergeCartGuid, fields).get()
            .subscribe(this, onSuccess_200 = {
                SharedPreference.getInstance().setCART_ID(it.body()?.code ?: "")
                createCartSuccess.onNext(it.body() ?: Cart())
                if (isAddToCart) {

                    if (entry != null) {
                        cartService?.addToCart(
                            userId,
                            cartId,
                            entry,
                            fields,
                            Constants.API_KEY.ANDROID
                        )
                            .get().subscribe(this, {
                                addToCartSuccess.value = it.body()
                            }, onError_400 = {

                            })
                    }
//
                }
            }, onSuccess_201 = {
                createCartSuccess.onNext(it.body() ?: Cart())
            })
        return createCartSuccess
    }

    /**
     * Request getCart after addToCart successfully to set cart badge number as cartResponse
     * entries number
     */
    fun addToCart2(userId: String, cartId: String, entry: AddToCartRequest, fields: String) {
        entry.product?.discount = null
        cartService.addToCart(userId, cartId, entry, fields, Constants.API_KEY.ANDROID).get()
            .subscribe(this, onSuccess_200 = {
                addToCartSuccess.value = it.body()
                loadCart(userId, cartId, fields)
            }, onError_400 = {
                if (getResponseErrorMessage(it).contains("No cart created yet.")) {
                    createCart(userId, null, null, "FULL")
                    cartService?.addToCart(userId, cartId, entry, fields, Constants.API_KEY.ANDROID)
                        .get().subscribe(this, {
                            addToCartSuccess.value = it.body()
                        }, onError_400 = {

                        })
                }
            })
    }

    @SuppressLint("CheckResult")
    fun addToCart(userId: String, cartId: String, entry: AddToCartRequest, fields: String) {
        entry.product?.discount = null
        cartService.addToCart(userId, cartId, entry, fields, Constants.API_KEY.ANDROID).get()
            .doOnSubscribe {
                this.progressBarVisibility().value = View.VISIBLE
            }.doOnTerminate {

            }.subscribe({
                when (it.code()) {
                    200, 201, 202 -> {
                        addToCartSuccess.value = it.body()
                        loadCart(userId, cartId, fields)
                    }
                    400, 401, 402 -> {
                        if (getResponseErrorMessage(it).contains("No cart created yet.")) {
                            createCart(userId, null, null, "FULL", true, cartId, entry)

                        }

                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }

                }
            },
                {
                    Log.d(TAG, it.message ?: "")
                    if (it.message?.contains("Unable to resolve host") == true) {
                        this.error().postValue("Please check your internet connection")
                        this.progressBarVisibility().value = View.GONE
                        this.hideRefreshLayout().value = true
                    }
                })
    }


    fun getBankOffer() {
        cartService.getBankOffers("FULL").get().subscribe(this, onSuccess_200 = {
            getBankOfferSuccess.value = it.body()
        }, onError_400 = {
            getBankOfferError.postValue(getResponseErrorMessage(it))
        })
    }


//    fun updateQuantity2(userId: String, cartId: String, entry: Entry, fields: String) {
//        entry.product?.discount = null
//        entry.entryNumber?.let {
//            cartService.updateQuantity(userId, cartId, it, entry, fields)
//                .get().subscribe(this, onSuccess_200 = {
//                    loadCart(userId, cartId, fields)
//                })
//        }
//    }
//


    @SuppressLint("CheckResult")
    fun updateQuantity(userId: String, cartId: String, entry: Entry, fields: String) {
        entry.product?.discount = null
        entry.entryNumber?.let {
            cartService.updateQuantity(userId, cartId, it, entry, fields).flatMap {
                when (it.code()) {
                    200, 201, 202 -> {
                        cartService.getCart(userId, cartId, "FULL")
                    }
                    400, 401, 402, 403 -> {
                        error().postValue(getResponseErrorMessage(it))
                        throw (RuntimeException("Something wrong happened"))
                    }
                    else -> {
                        throw (RuntimeException("Something wrong happened"))

                    }
                }
            }.get().subscribe(this, onSuccess_200 = {
                getCartResponse(it, userId, cartId)
            }
            )
        }
    }


    fun getCartResponse(response: Response<Cart>, userId: String, cartId: String) {
        when (response.code()) {
            200, 201, 202 -> {

                SharedPreference.getInstance().setCART_ID(response.body()?.code ?: "")
                SharedPreference.getInstance().setGUID(response.body()?.guid ?: "")

                cartLiveData.value = response.body()
                if ((response.body()?.entries?.size ?: 0) == 0) {
                    emptyCartVisibility.value = View.VISIBLE
                    checkoutButtonVisibility.value = View.GONE
                    footerVisibility.value = View.GONE
                    promoCodeVisibility.value = View.GONE
                    badgeNumber.value = 0

                } else {
                    badgeNumber.value = response.body()?.entries?.size
                    emptyCartVisibility.value = View.GONE
                    checkoutButtonVisibility.value = View.VISIBLE
                    footerVisibility.value = View.VISIBLE
                    promoCodeVisibility.value = View.VISIBLE

                    appliedVoucher.value = response.body()?.appliedVouchers

                    if ((response.body()?.appliedOrderPromotions?.size ?: 0) > 0 &&
                        response.body()?.appliedOrderPromotions?.get(0)?.description?.isNotEmpty() == true
                    ) {
                        receivedPromotionVisibility.value = View.VISIBLE
                        receivedPromotionMessage.value =
                            response.body()?.appliedOrderPromotions?.get(0)?.description
                    } else {
                        receivedPromotionVisibility.value = View.GONE
                    }



                    if ((response.body()?.potentialOrderPromotions?.size ?: 0) > 0 &&
                        response.body()?.potentialOrderPromotions?.get(0)?.description?.isNotEmpty() == true
                    ) {
                        potentialPromotionVisibility.value = View.VISIBLE
                        potentialPromotionMessageList.value =
                            response.body()?.potentialOrderPromotions
                    } else {
                        potentialPromotionVisibility.value = View.GONE
                    }


                }
            }

            400, 401 -> {
                val errorResponse = getResponseErrorMessage(response)
                if (
                    errorResponse.contains("No cart created yet.") ||
                    errorResponse.contains("Cart not found.")
                ) {
                    emptyCartVisibility.value = View.VISIBLE
                    checkoutButtonVisibility.value = View.GONE

                    createCart(
                        userId,
                        SharedPreference.getInstance().getGUID(),
                        null,
                        "FULL"
                    ).subscribe({
                        cartService?.getCart(userId, cartId, "FULL").get()
                            .subscribe(this, {
                                cartLiveData.value = it.body()
                            }, onError_400 = {

                            })
                    }, {
                        Log.d(TAG, it.message ?: "")
                        if (it.message?.contains("Unable to resolve host") == true) {
                            this.error().postValue("Please check your internet connection")
                            this.progressBarVisibility().value = View.GONE
                            this.hideRefreshLayout().value = true
                        }
                    })

                }
            }
            500, 501, 502 -> {


            }
        }
    }

    fun append(arr: Array<Bundle?>, element: Bundle): Array<Bundle?> {
        val array = arrayOfNulls<Bundle>(arr.size + 1)
        System.arraycopy(arr, 0, array, 0, arr.size)
        array[arr.size] = element
        return array
    }
}