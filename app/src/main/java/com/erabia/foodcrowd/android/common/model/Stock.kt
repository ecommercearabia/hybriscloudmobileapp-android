package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Stock(
    @SerializedName("stockLevel")
    val stockLevel: Int = 0,
    @SerializedName("stockLevelStatus")
    val stockLevelStatus: String = ""
)