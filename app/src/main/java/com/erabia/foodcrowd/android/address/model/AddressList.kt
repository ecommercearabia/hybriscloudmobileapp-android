package com.erabia.foodcrowd.android.address.model

import com.erabia.foodcrowd.android.common.model.Address
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class AddressList(
    @SerializedName("addresses")
     var addresses: List<Address>? = listOf()
)