package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Country(
    @SerializedName("isdcode")
    var isdcode: String? = null,
    @SerializedName("isocode")
    var isocode: String? = null,
    @SerializedName("name")
    var name: String? = null
) {
    fun getCountryName(countryList: List<Country>): List<String> {
        var countryNameList: MutableList<String> = ArrayList()
        for (i in 0 until countryList.size) {
            countryNameList.add(countryList.get(i).name ?: "")
        }
        return countryNameList
    }

    fun findCountryByName(countryList: List<Country>?, name: String): String? {
        return countryList?.filter { it.name == name }?.firstOrNull()?.isocode
    }
}


