package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class VariantMatrix(
    @SerializedName("elements")
    val elements: List<Any> = listOf(),
    @SerializedName("isLeaf")
    val isLeaf: Boolean = false,
    @SerializedName("parentVariantCategory")
    val parentVariantCategory: ParentVariantCategory = ParentVariantCategory(),
    @SerializedName("variantOption")
    val variantOption: VariantOption = VariantOption(),
    @SerializedName("variantValueCategory")
    val variantValueCategory: VariantValueCategory = VariantValueCategory()
)