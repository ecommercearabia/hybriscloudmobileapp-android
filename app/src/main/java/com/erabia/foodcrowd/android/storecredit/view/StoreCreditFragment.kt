package com.erabia.foodcrowd.android.storecredit.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
//import com.erabia.foodcrowd.android.checkout.remote.repository.StoreCreditLimitRepository
//import com.erabia.foodcrowd.android.checkout.remote.service.StoreCreditLimitService
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentMyOrderBinding
import com.erabia.foodcrowd.android.databinding.FragmentStoreCreditBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.storecredit.adapter.StoreCreditAdapter
import com.erabia.foodcrowd.android.storecredit.remote.repository.StoreCreditRepository
import com.erabia.foodcrowd.android.storecredit.remote.service.StoreCreditService
import com.erabia.foodcrowd.android.storecredit.viewmodel.StoreCreditViewModel
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_store_credit.view.*

class StoreCreditFragment : Fragment() {
    var storeCreditList: List<String> = listOf("s", "s")
    private var mStoreCreditAdapter: StoreCreditAdapter? = null

    var compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: StoreCreditViewModel
    lateinit var binding: FragmentStoreCreditBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = StoreCreditViewModel.Factory(
            StoreCreditRepository(
                RequestManager.getClient().create(StoreCreditService::class.java)
            )
//            StoreCreditLimitRepository( RequestManager.getClient().create(StoreCreditLimitService::class.java))
        )
        mViewModel = ViewModelProvider(this, factory).get(StoreCreditViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_store_credit, container, false)
        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel
//        binding.orderDetails =  mDetailsResponse

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerStoreCreditList()
    }

    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }

    fun observerStoreCreditList(){
        mViewModel.mStoreCreditListLiveData.observe(viewLifecycleOwner, Observer {
            mStoreCreditAdapter =
                StoreCreditAdapter(
                    it?.storeCreditHistroy
                )
            mStoreCreditAdapter?.setHasStableIds(true)
            binding.mStoreCreditRecyclerView?.adapter = mStoreCreditAdapter


        })

        mViewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(context , it)

        })
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title=""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title=""
    }
}