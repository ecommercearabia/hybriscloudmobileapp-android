package com.erabia.foodcrowd.android.whatexpress.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.databinding.FragmentWhatIsExpressBinding
import com.erabia.foodcrowd.android.main.MainActivity

class WhatIsExpressFragment : Fragment() {
    lateinit var binding: FragmentWhatIsExpressBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        (activity as MainActivity).supportActionBar?.title = ""
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_what_is_express, container, false)

        if(MyApplication.getContext()?.let { LocaleHelper.getLanguage(it).equals("en") } == true){
            binding.imageViewWhatIsExpress.background = context?.getDrawable(R.drawable.express_img)
        } else {
            binding.imageViewWhatIsExpress.background = context?.getDrawable(R.drawable.express_image_ar)
        }
        return binding.root
    }
}