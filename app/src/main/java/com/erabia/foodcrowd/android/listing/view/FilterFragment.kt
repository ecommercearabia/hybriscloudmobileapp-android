package com.erabia.foodcrowd.android.listing.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Value
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.ItemDivider
import com.erabia.foodcrowd.android.databinding.FragmentFilterBinding
import com.erabia.foodcrowd.android.listing.adapter.FilterAdapter
import com.erabia.foodcrowd.android.listing.adapter.FilterAdapter2
import com.erabia.foodcrowd.android.listing.adapter.IFilterCheckCallBack
import com.erabia.foodcrowd.android.listing.di.DaggerIListingComponent
import com.erabia.foodcrowd.android.listing.di.IListingComponent
import com.erabia.foodcrowd.android.listing.model.Listing
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import kotlinx.android.synthetic.main.filter_row.view.*
import javax.inject.Inject


class FilterFragment : Fragment(), FilterAdapter.IOnChecked, IFilterCheckCallBack {

    //    private var filterQuery: String = ""
    private var categoryId: String = ""
    private var sortSelected: String = ""
    lateinit var binding: FragmentFilterBinding

    //    lateinit var filterAdapter: FilterAdapter
    private var product: Listing = Listing()


    val daggerComponent: IListingComponent by lazy {
        DaggerIListingComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)
    }

    lateinit var listingViewModel: ListingViewModel

    @Inject
    lateinit var factory: ListingViewModel.Factory


    private val concatAdapterConfig = ConcatAdapter.Config.Builder()
        .setIsolateViewTypes(true)
        .setStableIdMode(ConcatAdapter.Config.StableIdMode.ISOLATED_STABLE_IDS)
        .build()


    private var filterAdapters: List<FilterAdapter2>? = null
    private var queryFilter: String = ""

    private var facetsList: List<Facet?>? = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listingViewModel = ViewModelProvider(this, factory).get(ListingViewModel::class.java)

        facetsList = arguments.let {
            it?.get(Constants.INTENT_KEY.FACETS) as List<Facet?>?
        }
        categoryId = arguments.let { it?.get("categoryid").toString() }
//        filterQuery = arguments.let { it?.get("query").toString() }
        sortSelected = arguments.let { it?.get("sort_selected").toString() }

//        filterAdapter = FilterAdapter(filterQuery, sortSelected)
//        filterAdapter.filterList =
//            arguments.let { it?.get(Constants.INTENT_KEY.FACETS) as List<Facet> }
//
//        filterAdapter.onChecked = this
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_filter, container, false)
//        filterAdapter.reset()
//        context?.let { ItemDivider(it) }?.let { binding.recyclerViewFilter.addItemDecoration(it) }
//        binding.recyclerViewFilter.adapter = filterAdapter
        buttonsClicks()
        initRecycleViewAdapters()
        observeErrorObserver()
        listingViewModel.productList.observe(viewLifecycleOwner, Observer {
            handleSuccessFilterProductList(it)
        })
        observeProgressBar()
        return binding.root
    }

    private fun buttonsClicks() {
        binding.buttonDone.setOnClickListener {
            if (!queryFilter.isNullOrEmpty()) {
                setFragmentResult(
                    Constants.REQUEST_FILTER_KEY,
                    bundleOf(Constants.FILTER_KEY to queryFilter)
                )
                findNavController().popBackStack()
            } else {


                AppUtil.showToastyWarning(context ,R.string.please_select_filter_criteria)

            }

        }


        binding.buttonRest.setOnClickListener {
            setFragmentResult(
                Constants.REQUEST_FILTER_KEY,
                bundleOf(Constants.FILTER_KEY to sortSelected)
            )
            findNavController().popBackStack()
        }


    }

    private fun observeProgressBar() {
        listingViewModel.progressBar.observe(viewLifecycleOwner,
            Observer {
                if (it == View.VISIBLE) {
                    (activity as MainActivity).binding.progressBar.visibility = it
                    (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
                } else {
                    (activity as MainActivity).binding.progressBar.visibility = it
                    (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
                }
            })
    }


    private fun observeErrorObserver() {
        listingViewModel.errorObserver.observe(viewLifecycleOwner,
            Observer {
                AppUtil.showToastyError(context , it)

            })
    }


    private fun handleSuccessFilterProductList(response: Listing?) {
        response?.let { it ->
            product = it
            facetsList = it.facets?.filterNot { facet ->
                (facet.name == "Vegan" || facet.name == "Organic" || facet.name == "Gluten Free")
            } ?: ArrayList()

//            breadCrumbList = it.breadCrumbList
//            if (breadCrumbList?.isNotEmpty() == true) {
//                binding.mDeleteFilterTitleTextView.visibility = View.VISIBLE
//                deleteFilterAdapter.breadCrumbList = breadCrumbList
//
//            } else {
//                binding.mDeleteFilterTitleTextView.visibility = View.GONE
//                deleteFilterAdapter.breadCrumbList = breadCrumbList
//            }

            filterAdapters = null
            filterAdapters = facetsList?.map { _ ->
                FilterAdapter2(filerCheckListener = this)
            }
            filterAdapters?.forEachIndexed { index, itemsExpandableAdapter ->
                if (index < facetsList?.size ?: 0) {
                    itemsExpandableAdapter.setHasStableIds(true)
                    itemsExpandableAdapter.filterAndBreadCrumbList = facetsList?.get(index)
                    facetsList?.get(index)?.values?.forEach { valueFacetListDomain ->
                        if (valueFacetListDomain?.selected == true) {
                            itemsExpandableAdapter.isExpanded = true
                        }
                    }
                }
            }

            val concatAdapter = ConcatAdapter(concatAdapterConfig, filterAdapters ?: emptyList())
            with(binding.recyclerViewFilter) {
                layoutManager = LinearLayoutManager(context)
                itemAnimator =
                    ExpandableItemAnimator()
                adapter = concatAdapter
            }
        }
    }

    private fun initRecycleViewAdapters() {
//        binding.mDeleteFilterRecycleView.adapter = deleteFilterAdapter


        filterAdapters = facetsList?.map { _ ->
            FilterAdapter2(filerCheckListener = this)
        }
        filterAdapters?.forEachIndexed { index, itemsExpandableAdapter ->
            itemsExpandableAdapter.setHasStableIds(true)
            itemsExpandableAdapter.filterAndBreadCrumbList = facetsList?.get(index)

//            if (breadCrumbList?.isNotEmpty() == true) {
//                binding.mDeleteFilterTitleTextView.visibility = View.VISIBLE
//                deleteFilterAdapter.breadCrumbList = breadCrumbList
//
//                facetsList?.get(index)?.value?.forEach {
//                    if (it?.selected == true) {
//                        itemsExpandableAdapter.isExpanded = true
//                    }
//                }
//            } else {
//                binding.mDeleteFilterTitleTextView.visibility = View.GONE
//                itemsExpandableAdapter.isExpanded = index == 0
//            }
        }

        val concatAdapter = ConcatAdapter(concatAdapterConfig, filterAdapters ?: emptyList())
        with(binding.recyclerViewFilter) {
            layoutManager = LinearLayoutManager(context)
            itemAnimator =
                ExpandableItemAnimator()
            adapter = concatAdapter
        }
    }


    override fun onChecked(filterQuery: String) {
//        this.filterQuery = filterQuery
        Log.d("filter_query", filterQuery)
        listingViewModel.filter?.filter(filterQuery)
//        listingViewModel.getProductList(
//            0,
//            filterQuery,"")

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                findNavController().popBackStack()
            }

            else -> {
                super.onOptionsItemSelected(item)
            }
        }
        return true
    }

    override fun onCheckedFilter(valueFacetsList: Value?) {
        queryFilter = valueFacetsList?.query?.query?.value ?: ""
        listingViewModel.sort = sortSelected
        listingViewModel.getProductList(
            categoryId,
            0,
            queryFilter,
            true,
            "listing"
        )
    }

}