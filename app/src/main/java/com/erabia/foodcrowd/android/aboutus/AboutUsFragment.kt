package com.erabia.foodcrowd.android.aboutus

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.aboutus.remote.AboutUsRepository
import com.erabia.foodcrowd.android.aboutus.remote.AboutUsService
import com.erabia.foodcrowd.android.aboutus.viewModel.AboutUsViewModel
import com.erabia.foodcrowd.android.databinding.FragmentAboutUsBinding
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.disposables.CompositeDisposable
import androidx.lifecycle.Observer
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.main.MainActivity


class AboutUsFragment : Fragment() {

    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: AboutUsViewModel
    lateinit var binding: FragmentAboutUsBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = AboutUsViewModel.Factory(
                AboutUsRepository(
                        RequestManager.getClient().create(AboutUsService::class.java)
                )
        )
        mViewModel = ViewModelProvider(this, factory).get(AboutUsViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding =
                DataBindingUtil.inflate(inflater, R.layout.fragment_about_us, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = mViewModel

        return binding.root

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.mAboutUsResponseLiveData.observe(viewLifecycleOwner, Observer {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                binding.mHtmlTextView.text = Html.fromHtml(
                        it.content ?: "",
                        Html.FROM_HTML_MODE_COMPACT
                )
            } else {
                binding.mHtmlTextView.text = Html.fromHtml(it.content ?: "")
            }
        })

        loadingVisibilityObserver()
        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })

        }

    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }
    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title=""
        mViewModel.getAboutUs()

    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title=""
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}