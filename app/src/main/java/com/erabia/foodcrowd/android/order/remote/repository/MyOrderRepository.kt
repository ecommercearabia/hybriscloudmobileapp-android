package com.erabia.foodcrowd.android.order.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import io.reactivex.rxkotlin.subscribeBy

class MyOrderRepository(val myOrderService: MyOrderService) : Repository {
    companion object {
        const val TAG: String = "MyOrderRepository"
    }

    val mOrderListLiveData: SingleLiveEvent<OrderHistoryListResponse> by lazy { SingleLiveEvent<OrderHistoryListResponse>() }
    val mItemDetails: SingleLiveEvent<OrderDetailsResponse> by lazy { SingleLiveEvent<OrderDetailsResponse>() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun loadOrder(){
        myOrderService.getOrder(
            "current", 0,
            100,"FULL"
        ).get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE}
            .doOnTerminate { this.progressBarVisibility().value= View.GONE}
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200->{
                            mOrderListLiveData.value = it.body()

                        }
                        201->{
                            mOrderListLiveData.value = it.body()

                        }
                        400->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }


    @SuppressLint("CheckResult")
    fun getItemDetails(mCode: String){
        myOrderService.getDetailsOrder(
            "current", mCode,
            "FULL"
        ).get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE}
            .doOnTerminate { this.progressBarVisibility().value= View.GONE}
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200->{
                            mItemDetails.value = it.body()

                        }
                        201->{
                            mItemDetails.value = it.body()

                        }
                        400->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }



}