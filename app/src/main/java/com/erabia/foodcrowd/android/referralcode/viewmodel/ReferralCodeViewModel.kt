package com.erabia.foodcrowd.android.referralcode.viewmodel


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.personaldetails.remote.repository.PersonalDetailsRepository
import com.erabia.foodcrowd.android.personaldetails.viewmodel.PersonalDetailsViewModel
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeModel
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeResponse
import com.erabia.foodcrowd.android.referralcode.remote.repository.ReferralCodeRepository

class ReferralCodeViewModel(val referralCodeRepository: ReferralCodeRepository) :
    ViewModel() {

    val mReferralCode: SingleLiveEvent<ReferralCodeResponse> = referralCodeRepository.mReferralCode
    val mReferralCodeLiveData: SingleLiveEvent<ReferralCodeResponse> =
        referralCodeRepository.mReferralCodeLiveData
    val shareViaEmail: SingleLiveEvent<Boolean> = referralCodeRepository.shareViaEmail
    val shareClickEvent: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val shareClick: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val loadingVisibility: MutableLiveData<Int> = referralCodeRepository.loadingVisibility
    var emails: String = ""


    fun getReferralCode() {
        referralCodeRepository.getReferralCode()
    }


    fun getConfig() {
        referralCodeRepository.loadAppConfig()
    }

    fun doGenerate() {
        referralCodeRepository.generateReferralCode()
    }

    fun onShareClick(referralCode: String) {
        shareClick.postValue(referralCode)

    }

    fun onShareViaEmail() {
        if (emails != "" ) {
            referralCodeRepository.sendReferralSubscriptionEmails(emails)
        }

    }

    class Factory(
        val referralCodeRepository: ReferralCodeRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ReferralCodeViewModel(referralCodeRepository) as T
        }
    }
}