package com.erabia.foodcrowd.android.newaddress.remote.service

import com.erabia.foodcrowd.android.address.model.CityList
import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.model.Area
import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.network.AuthInterceptor
import com.erabia.foodcrowd.android.newaddress.model.*
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.google.gson.Gson
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface AddressService {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/addresses")
     fun getAddressList(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<AddressList>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/addresses")
    fun createAddress(
        @Path("userId") userId: String,
        @Body body: AddressRequest
    ): Observable<Response<Address>>


    @PUT("rest/v2/foodcrowd-ae/users/{userId}/addresses/{addressId}")
    fun updateAddress(
        @Path("userId") userId: String,
        @Path("addressId") addressId: String,
        @Body body: AddressRequest
    ): Observable<Response<Void>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/addresses/delivery")
    fun setDeliveryAddress(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("addressId") addressId: String
    ): Observable<Response<Void>>


    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/addresses/{addressId}")
    fun deleteAddress(
        @Path("userId") userId: String,
        @Path("addressId") addressId: String
    ): Observable<Response<Void>>


    @GET("rest/v2/foodcrowd-ae/users/{userId}")
    fun getCustomerProfile(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<PersonalDetailsModel>>


    @GET("rest/v2/foodcrowd-ae/countries")
    fun getMobileCountryList(
        @Query("fields") fields: String,
        @Query("type") type: String
    ): Observable<Response<Any>>



    @GET("rest/v2/foodcrowd-ae/countries/{countyIsoCode}/cities")
    fun getCities(
        @Path("countyIsoCode") countyIsoCode: String,
        @Query("fields") fields: String
    ): Observable<Response<CityListResponse>>

      @GET("rest/v2/foodcrowd-ae/countries/{countyIsoCode}/cities/{cityCode}/areas")
    fun getArea(
        @Path("countyIsoCode") countyIsoCode: String,
        @Path("cityCode") cityCode: String,
        @Query("fields") fields: String
    ): Observable<Response<AreaListResponse>>

    @GET("rest/v2/foodcrowd-ae/titles")
    fun getLocalizedTitle(
        @Query("fields") fields: String
    ): Observable<Response<TitleResponse>>

}