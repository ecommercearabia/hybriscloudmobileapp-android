package com.erabia.foodcrowd.android.forgetpassword.remote.repositry

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.forgetpassword.model.ResetPasswordPostData
import com.erabia.foodcrowd.android.forgetpassword.model.ResetPasswordResponse
import com.erabia.foodcrowd.android.forgetpassword.remote.service.ForgetPasswordService
import com.erabia.foodcrowd.android.signup.model.RegisterResponse

import io.reactivex.rxkotlin.subscribeBy

class ForgetPasswordRepository(val forgetPasswordService: ForgetPasswordService) :
    Repository {
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mGetTokenLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }

    companion object {
        const val TAG: String = "ForgetRepository"
    }

    @SuppressLint("CheckResult")
    fun getToken(mEmail: String) {
        forgetPasswordService.generateToken(
            mEmail
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mGetTokenLiveData.value = it.body().toString()
                        }
                        201 -> {
                            mGetTokenLiveData.value = it.body().toString()
                        }
                        202 -> {
                            mGetTokenLiveData.value = it.body().toString()
                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
    }


}
