package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class DeliveryOrderGroup(
    @SerializedName("deliveryAddress")
    val deliveryAddress: DeliveryAddress = DeliveryAddress(),
    @SerializedName("entries")
    val entries: List<Entry> = listOf(),
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("totalPriceWithTax")
    val totalPriceWithTax: TotalPriceWithTax = TotalPriceWithTax()
)