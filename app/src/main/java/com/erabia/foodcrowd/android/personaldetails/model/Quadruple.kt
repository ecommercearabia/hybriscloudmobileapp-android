package com.erabia.foodcrowd.android.personaldetails.model

data class Quadruple(
    val first: ZipModelResponse.SuccessPersonal,
    val second: ZipModelResponse.SuccessTitle,
    val third: ZipModelResponse.SuccessCountry,
    val fourth: ZipModelResponse.SuccessNationality
)