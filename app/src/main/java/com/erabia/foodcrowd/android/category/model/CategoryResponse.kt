package com.erabia.foodcrowd.android.category.model

import com.google.gson.annotations.SerializedName


data class CategoryResponse(
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("category")
    var category: Category? = null,
    @SerializedName("id")
    var id: String? = null,
    @SerializedName("lastModified")
    var lastModified: String? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("url")
    var url: String? = null,
    @SerializedName("subcategories")
    var subcategories: List<Subcategory>? = null
)