package com.erabia.foodcrowd.android.order.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.MyOrderRowItemBinding
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel


class MyOrderAdapter(
    val myOrderList: List<OrderHistoryListResponse.Order?>,
    val mMyOrderViewModel: MyOrderViewModel
) :
    RecyclerView.Adapter<ViewHolder>() {
    private var context: Context? = null

    lateinit var mMyOrderRowItemBinding: MyOrderRowItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.my_order_row_item, parent, false)

        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        mMyOrderRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.my_order_row_item, parent, false)
        return ViewHolder(
            mMyOrderRowItemBinding
        )

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return myOrderList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mMyOrderRowItemBinding.orders = myOrderList[position]
        holder.mMyOrderRowItemBinding.mOrderViewModel = mMyOrderViewModel
        holder.mMyOrderRowItemBinding.postion = position
        holder.mMyOrderRowItemBinding.mDatePlacedValueTextView.text =
            AppUtil.formatDate(
                "yyyy-MM-dd'T'HH:mm:ssZ",
                "MMM dd, yyyy HH:mm a",
                myOrderList?.get(position)?.placed
            )
//        holder.mMyOrderRowItemBinding.mViewOrderButton.setOnClickListener {
//            context?.startActivity(Intent(context,OrderActivity::class.java))
//        }
    }

}

class ViewHolder(val mMyOrderRowItemBinding: MyOrderRowItemBinding) :
    RecyclerView.ViewHolder(mMyOrderRowItemBinding.root)

