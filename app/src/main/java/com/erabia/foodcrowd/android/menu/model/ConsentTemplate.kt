package com.erabia.foodcrowd.android.menu.model


import com.google.gson.annotations.SerializedName

data class ConsentTemplate(
    @SerializedName("id")
    val id: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("version")
    val version: String? = null,
    @SerializedName("currentConsent")
    val currentConsent: CurrentConsent? = null

)

data class ConsentTemplateList(
    @SerializedName("consentTemplates")
    val ConsentTemplate: ArrayList<ConsentTemplate>? = arrayListOf()
)

data class CurrentConsent(
    @SerializedName("code")
    val code: String? = null,
    @SerializedName("consentGivenDate")
    val consentGivenDate: String? = null,
    @SerializedName("consentWithdrawnDate")
    val consentWithdrawnDate: String? = null
)