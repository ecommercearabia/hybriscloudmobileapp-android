package com.erabia.foodcrowd.android.address.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.adapter.AddressAdapter
import com.erabia.foodcrowd.android.address.adapter.IOnClick
import com.erabia.foodcrowd.android.address.di.DaggerIAddressComponent
import com.erabia.foodcrowd.android.address.di.IAddressComponent
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.databinding.FragmentAddressBinding

class AddressFragment : Fragment(), IOnClick {


    val daggerComponent: IAddressComponent by lazy {
        DaggerIAddressComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)

    }

    val addressViewModel: AddressViewModel by activityViewModels()
    lateinit var addressAdapter: AddressAdapter
    lateinit var binding: FragmentAddressBinding
    var bundel = Bundle()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addressAdapter = AddressAdapter(addressViewModel, findNavController())
        addressAdapter.setHasStableIds(true)
        addressAdapter.onClick = this

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_address, container, false)
        arguments?.let {
            bundel = it
            addressViewModel.bundle = it
        }
        Log.d("bundel", arguments?.let { it.getString(Constants.COME_FROM) } ?: "")
        Log.d("bundel", addressViewModel.bundle.getString(Constants.COME_FROM) ?: "")
        addressViewModel.initialization(daggerComponent.getAddressRepo(),daggerComponent.getSignUpRepo())
        binding.addressVM = addressViewModel

        addressAdapter.cameFrom = bundel.getString(Constants.COME_FROM)
        binding.recyclerViewDeliveryAddress.adapter = addressAdapter
        addressViewModel.getAddressList()

        observeProgressBar()
        observeEmptyAddress()
        observeAddressList()
        observeAddAddress()
        observeNavigation()
        observeAddAddressButtonVisibility()
        observeCheckoutNavigation()
        observeAddressDetails()
        return binding.root
    }

    private fun observeAddressDetails() {
        addressViewModel.updateAction.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                it,
                bundleOf(
                    Constants.INTENT_KEY.ADDRESS_ID to bundel.getString(Constants.INTENT_KEY.ADDRESS_ID),
                    Constants.COME_FROM to bundel.getString(Constants.COME_FROM)
                )
            )
        })
    }

    private fun observeCheckoutNavigation() {
        addressViewModel.addressRepository.navigateFromAddressToCheckout.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().navigate(it)
            })
    }


    private fun observeProgressBar() {
        addressViewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = it
        })
    }

    private fun observeAddAddressButtonVisibility() {
        addressViewModel.addAddressButtonVisibility.observe(viewLifecycleOwner, Observer {
            binding.buttonAddAddress.visibility = it
        })
    }

    private fun observeEmptyAddress() {
        addressViewModel.emptyAddressVisibility.observe(viewLifecycleOwner, Observer {
            binding.constraintLayoutEmptyAddress.visibility = it

        })
    }

    private fun observeAddressList() {
        addressViewModel.addressList.observe(viewLifecycleOwner, Observer {
            addressAdapter.addressList = it.addresses
        })
    }

    private fun observeAddAddress() {
        addressViewModel.action.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                it ?: 0,
                bundleOf(Constants.COME_FROM to bundel.getString(Constants.COME_FROM))
            )

        })
    }

    private fun observeNavigation() {
        addressViewModel.addressRepository.navigateFromCartToAddress.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().navigate(
                    it,
                    bundleOf(Constants.COME_FROM to Constants.INTENT_KEY.CART),
                    NavOptions.Builder().setPopUpTo(R.id.cart, false).build()
                )
            })
    }


    override fun onClick(address: Address?) {
        if (bundel.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CART)
        ) {
            addressViewModel.setDeliveryAddress(address ?: Address())
//            findNavController().navigate(R.id.action_navigate_to_checkout)
        } else if (bundel.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CHECKOUT)
        ) {
            addressViewModel.setDeliveryAddress(address ?: Address())
            findNavController().popBackStack()
        } else {
            addressViewModel.setBillingAddress(address ?: Address())
            findNavController().popBackStack()
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
}