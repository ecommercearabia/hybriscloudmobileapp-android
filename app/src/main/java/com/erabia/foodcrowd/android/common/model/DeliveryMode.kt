package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class DeliveryMode(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("deliveryCost")
    val deliveryCost: DeliveryCost = DeliveryCost(),
    @SerializedName("description")
    val description: String = "",
    @SerializedName("name")
    val name: String = ""
)