package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Value(
    @SerializedName("count")
    val count: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("query")
    val query: Query? = null,
    @SerializedName("selected")
    val selected: Boolean? = null

)