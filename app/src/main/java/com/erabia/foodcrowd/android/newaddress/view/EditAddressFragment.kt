package com.erabia.foodcrowd.android.newaddress.view

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.databinding.FragmentEditAddressBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.newaddress.adapter.PlacesAutoCompleteAdapter
import com.erabia.foodcrowd.android.newaddress.model.AreaListResponse
import com.erabia.foodcrowd.android.newaddress.model.CityListResponse
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.newaddress.remote.service.AddressService
import com.erabia.foodcrowd.android.newaddress.viewmodel.EditAddressViewModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import io.reactivex.disposables.CompositeDisposable

class EditAddressFragment : Fragment(), PlacesAutoCompleteAdapter.HasPlacesNotifiedClickListener {
    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: EditAddressViewModel
    lateinit var binding: FragmentEditAddressBinding
    var address: Address? = null
    var geoAddress: android.location.Address? = null
    lateinit var mComeFrom: String
    lateinit var addressID: String
    var dummyTitleList: List<TitleResponse.Title> = listOf(TitleResponse.Title("", "Title"))
    var dummyCountryList: List<CountryResponse.Country> =
        listOf(CountryResponse.Country("", "", "Country"))
    var dummyCityList: List<CityListResponse.City> =
        listOf(CityListResponse.City(emptyList(), "", "City"))
    var dummyAreaList: List<AreaListResponse.Area> = listOf(AreaListResponse.Area("", "Area"))
    private var mAutoCompleteAdapter: PlacesAutoCompleteAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = EditAddressViewModel.Factory(
            AddressRepository(
                RequestManager.getClient().create(AddressService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(EditAddressViewModel::class.java)

        mComeFrom = arguments?.get("COME_FROM").toString()

        if (arguments?.get("address") != null) {
            address = arguments?.get("address") as Address
            addressID = address!!.id
        }
        mViewModel.getAllDataSpinner()
        if (address != null) {
            address?.let { mViewModel.setAddressFromPreviousFragment(it) }

        } else {
            geoAddress = arguments?.get("geo") as android.location.Address
            addressID = arguments?.get("addressId").toString()
            geoAddress?.let { it ->
                it.getAddressLine(0)?.let { mViewModel.setAddressMap(it, addressID) }
            }
        }
        // Initialize the SDK
        context?.let { Places.initialize(it, Constants.PLACE_API_KE) }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_edit_address, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = mViewModel

        mAutoCompleteAdapter = PlacesAutoCompleteAdapter(requireContext())

        initAutoCompletePlace()
        initKeyboardEnterClickButton()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeUpdateAddress()
        observeContainerLayoutClickEvent()
        observerLastLiveData()
        observerEnableButton()
        observeNavigateToMap()
        observerError()
        observeStartPlaceTextWatcher()

        var titleArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyTitleList
                )
            }

        var countryArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyCountryList
                )
            }
        var cityArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyCityList
                )
            }
        var areaArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyAreaList
                )
            }

        binding.mTitleSpinner.adapter = titleArrayAdapter
        binding.mCountrySpinner.adapter = countryArrayAdapter
        binding.mCitySpinner.adapter = cityArrayAdapter
//        binding.mAreaSpinner.adapter = areaArrayAdapter
        observeAreaSelected()
        observeListAreaSelected()
        observeListArea()
        loadingVisibilityObserver()
    }
    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }
    private fun observeAreaSelected() {
        mViewModel.mAreaSelection.observe(viewLifecycleOwner, Observer {
            binding.editTextArea.setText(it.name ?: "")
        })
    }

    private fun observeListArea() {
        mViewModel.mAreaListLiveData.observe(viewLifecycleOwner,
            Observer<AreaListResponse> {
                val pos = it.areas?.indexOfFirst { area ->
                    area?.code == mViewModel.mAreaCodeFromPreviousFragment
                }
                if (pos != -1) {
                    val area = it.areas?.get(pos ?: 0)
                    mViewModel.mAreaSelection.postValue(
                        AreaListResponse.Area(
                            area?.code,
                            area?.name
                        )
                    )
                } else {
                    val area = it.areas?.get(0)
                    mViewModel.mAreaSelection.postValue(
                        AreaListResponse.Area(
                            area?.code,
                            area?.name
                        )
                    )
                }

            })
    }

    private fun observeListAreaSelected() {
        mViewModel.areaListForPickerLiveData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val list = arrayListOf<AreaListResponse.Area>()
                it.areas?.forEach { area ->
                    if (area != null)
                        list.add(area)
                }
                DialogData.getAreaDialog(context, null, mViewModel, list)
            }
        })
    }

    private fun initAutoCompletePlace() {
        binding.placesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        mAutoCompleteAdapter?.clickListener = object :
            PlacesAutoCompleteAdapter.ClickListener {
            override fun click(place: String?) {
                binding.streetNamePlaceSearch.setText(place)
                binding.placesRecyclerView.visibility = View.GONE
            }

        }
        mAutoCompleteAdapter?.hasPlacesNotifiedClickListener = this
        binding.placesRecyclerView.adapter = mAutoCompleteAdapter
        mAutoCompleteAdapter?.notifyDataSetChanged()

    }

    private fun initKeyboardEnterClickButton() {
        binding.streetNamePlaceSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                mAutoCompleteAdapter?.filter?.filter(null)
                binding.placesRecyclerView.visibility = View.GONE
            }
            false
        }
    }

    private fun initStreetNameTextWatcher() {
        binding.streetNamePlaceSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d(AddAddressFragment.TAG, "onTextChanged: ")
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString() != "") {
                    mAutoCompleteAdapter?.filter?.filter(s.toString())
                    if (binding.placesRecyclerView.visibility == View.GONE) {
                        binding.placesRecyclerView.visibility = View.VISIBLE
                    }
                } else {
                    if (binding.placesRecyclerView.visibility == View.VISIBLE) {
                        binding.placesRecyclerView.visibility = View.VISIBLE
                        binding.placesRecyclerView.visibility = View.GONE
                    }
                }
            }

        })

    }

    private fun observeNavigateToMap() {
        mViewModel.mNavigateToMap.observe(
            viewLifecycleOwner,
            Observer {
//                val action =
//                    EditAddressFragmentDirections.actionEditAddressFragmentToMapAddressFragment2(
//                        "edit",
//                        mComeFrom, addressID
//                    )
//
//                findNavController().navigate(action)
            })
    }

    private fun observeStartPlaceTextWatcher() {
        mViewModel.startPlaceTextWatcher.observe(viewLifecycleOwner, Observer {
            initStreetNameTextWatcher()
        })
    }

    private fun observeUpdateAddress() {
        mViewModel.updateAddressEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context , it)

            findNavController().popBackStack()
        })
    }

    fun observerEnableButton() {
        mViewModel.mEnableButton.observe(viewLifecycleOwner, Observer {
            binding.mEditAddressButton.isEnabled = it
        })
    }

    fun observerError() {

        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })

        mViewModel.mErrorAreaAndCreateEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })
    }

    private fun observerLastLiveData() {

        mViewModel.mObserverEndLiveData.observe(viewLifecycleOwner, Observer {

//            mViewModel.emptyText()
            if (address != null) {
                val posTitle =
                    mViewModel.mTitleListLiveData.value?.titles?.indexOfFirst { title ->
                        title.code == address?.titleCode
                    }
                binding.mTitleSpinner.setSelection(posTitle ?: 0)

                val posCountry =
                    mViewModel.mCountryListLiveData.value?.indexOfFirst { country ->
                        country.name == address?.country?.name
                    }
                binding.mCountrySpinner.setSelection(posCountry ?: 0)

                val posCity =
                    mViewModel.mCityListLiveData.value?.cities?.indexOfFirst { city ->
                        city?.name == address?.city?.name
                    }
                binding.mCitySpinner.setSelection(posCity ?: 0)


                val posMobileCode =
                    mViewModel.mMobileCountryListLiveData.value?.indexOfFirst { country ->
                        country?.isdcode == address?.country?.isdcode
                    }
                binding.mCodeMobileSpinner.setSelection(posMobileCode ?: 0)

                binding.mAddress = address
            }
            address = null
        })
    }

    private fun observeContainerLayoutClickEvent() {
        mViewModel.resetAndHideAdapter.observe(viewLifecycleOwner, Observer {
            mAutoCompleteAdapter?.filter?.filter(null)
            binding.placesRecyclerView.visibility = View.GONE
        })
    }

    override fun hasPlacesNotified(hasNotified: Boolean) {
        if (hasNotified) {
            Log.d(AddAddressFragment.TAG, "hasItemsShown: $hasNotified")
            Log.d(
                AddAddressFragment.TAG,
                "hasItemsShown: recyclerView ${binding.placesRecyclerView.height}"
            )
            binding.scrollView.smoothScrollTo(0, binding.mStreetNameTextInputLayout.top)
        }
    }


}