package com.erabia.foodcrowd.android.email.remote.service

import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.TokenModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ChangeEmailService {

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/login")
    fun changeEmail(
        @Path("userId") userId: String,
        @Query("newLogin") newLogin: String,
        @Query("password") password: String
    ): Observable<Response<Void>>

    @POST("rest/v2/foodcrowd-ae/login/token")
    fun loginEmailMobile(
        @Query("identifier") identifier: String,
        @Query("password") password: String,
        @Header("clientId") clientId: String,
        @Header("clientSecret") clientSecret: String
    ): Observable<Response<TokenModel>>

    @PATCH("rest/v2/foodcrowd-ae/users/{userId}/mobile-token")
    fun setFirebaseToken(
        @Path("userId") userId: String,
        @Query("mobileToken") mobileToken: String
    ): Observable<Response<Product>>


    @POST("rest/v2/foodcrowd-ae/users/{userId}/signature")
    fun setSignature(@Path("userId") userId: String,@Query("signatureId") signatureId:String): Observable<Response<Void>>



}