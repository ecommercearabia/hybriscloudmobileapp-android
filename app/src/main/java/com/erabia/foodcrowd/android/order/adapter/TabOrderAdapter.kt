package com.erabia.foodcrowd.android.order.adapter

import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.view.OrderDetailsFragment
import com.erabia.foodcrowd.android.order.view.OrderItemFragment

class TabOrderAdapter (private val mOrderDetailsResponse: OrderDetailsResponse, fm: FragmentManager, internal var totalTabs: Int) :  FragmentPagerAdapter(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    // this is for fragment tabs
    @NonNull
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                //  val homeFragment: HomeFragment = HomeFragment()
                return OrderItemFragment(mOrderDetailsResponse)
            }
            1->{
                return OrderDetailsFragment(mOrderDetailsResponse)
            }
            else-> {
                throw IllegalStateException("position $position is invalid for this viewpager")
            }
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}
