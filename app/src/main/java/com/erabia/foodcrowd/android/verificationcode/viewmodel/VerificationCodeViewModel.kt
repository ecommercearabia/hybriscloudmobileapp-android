package com.erabia.foodcrowd.android.verificationcode.viewmodel

import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.loginwithotp.remote.repository.LoginWithOtpRepository
import com.erabia.foodcrowd.android.verificationcode.remote.repository.VerificationRepository

class VerificationCodeViewModel(
    private val verificationRepository: VerificationRepository,
    private val loginWithOtpRepository: LoginWithOtpRepository
) :
    ViewModel() {

    companion object {
        const val TAG = "VerificationCodeViewModel"
    }

    var email = ""
    var verificationCode1 = ""
    var verificationCode2 = ""
    var verificationCode3 = ""
    var verificationCode4 = ""
    val progressBarVisibility: SingleLiveEvent<Int> = verificationRepository.progressBarVisibility()
    val error: SingleLiveEvent<String> = verificationRepository.error()
    var mLoginLiveData: LiveData<TokenModel> = verificationRepository.mLoginLiveData
    val closeKeyboardEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val sendOtpSuccessEvent = loginWithOtpRepository.sendEmailSuccessEvent


    fun onVerifyClick() {
        if (isAllFieldsFilled()) {
            verify()
        } else {
            error.postValue(MyApplication.getContext()?.getString(R.string.please_fill_all_field))
        }
    }

    fun verify() {
        if (isAllFieldsFilled()) {
            val code = verificationCode1.plus(verificationCode2).plus(verificationCode3)
                .plus(verificationCode4)
            verificationRepository.verify(code, email, "")
            closeKeyboardEvent.value = true
        }

    }

    fun sendOtpCode() {
        loginWithOtpRepository.loginWithOtp(email)
    }

    fun onEditTextVerificationCode1Changed(
        s: CharSequence, start: Int, before: Int,
        count: Int
    ) {
        verificationCode1 = s.toString()
        verify()
    }

    fun onEditTextVerificationCode2Changed(
        s: CharSequence, start: Int, before: Int,
        count: Int
    ) {
        verificationCode2 = s.toString()
        verify()
    }

    fun onEditTextVerificationCode3Changed(
        s: CharSequence, start: Int, before: Int,
        count: Int
    ) {
        verificationCode3 = s.toString()
        verify()
    }

    fun onEditTextVerificationCode4Changed(
        s: CharSequence, start: Int, before: Int,
        count: Int
    ) {
        verificationCode4 = s.toString()
        verify()
    }


    private fun isAllFieldsFilled(): Boolean {
        return verificationCode1.length == 1 &&
                verificationCode2.length == 1 &&
                verificationCode3.length == 1 &&
                verificationCode4.length == 1
    }

    class Factory(
        private val verificationRepository: VerificationRepository,
        private val loginWithOtpRepository: LoginWithOtpRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return VerificationCodeViewModel(verificationRepository, loginWithOtpRepository) as T
        }

    }
}