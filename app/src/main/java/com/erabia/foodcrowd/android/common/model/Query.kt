package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Query(
    @SerializedName("query")
    val query: QueryValue? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("value")
    val value: String? = null
)