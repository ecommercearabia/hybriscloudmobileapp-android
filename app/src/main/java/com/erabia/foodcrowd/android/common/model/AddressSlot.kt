package com.erabia.foodcrowd.android.common.model

import java.util.*

class AddressSlot {
    var pos: String? = null
    var deliverySlotCode: String? = null
    var deliveryDate: String? = null
    var startTime: String? = null
    var endTime: String? = null
    var dayOfWeek: String? = null
    var selected: Boolean? = null
    var expired: Boolean? = null
    var available: Boolean? = null

    constructor(
        pos: String?,
        deliverySlotCode: String?,
        deliveryDate: String?,
        startTime: String?,
        endTime: String?,
        dayOfWeek: String?,
        isSelected: Boolean?,
        isExpired: Boolean?,
        isAvailable: Boolean?
    ) {
        this.pos = pos
        this.deliverySlotCode = deliverySlotCode
        this.deliveryDate = deliveryDate
        this.startTime = startTime
        this.endTime = endTime
        this.dayOfWeek = dayOfWeek
        selected = isSelected
        expired = isExpired
        available = isAvailable
    }

    constructor() {}

    companion object {
        @JvmStatic
        val dummyAddressSlot: List<AddressSlot>
            get() {
                val addressSlotList: MutableList<AddressSlot> =
                    ArrayList()
                val addressSlot1 = AddressSlot()
                addressSlot1.available = true
                addressSlot1.expired = false
                addressSlot1.startTime = "09:00"
                addressSlot1.endTime = "11:00"
                addressSlot1.deliveryDate = "7-1-2018"
                addressSlot1.dayOfWeek = "Saturday"
                val addressSlot2 = AddressSlot()
                addressSlot2.available = false
                addressSlot2.expired = true
                addressSlot2.startTime = "12:00"
                addressSlot2.endTime = "04:00"
                addressSlot2.deliveryDate = "8-1-2018"
                addressSlot2.dayOfWeek = "Sunday"
                val addressSlot3 = AddressSlot()
                addressSlot3.available = true
                addressSlot3.expired = false
                addressSlot3.startTime = "08:00"
                addressSlot3.endTime = "03:00"
                addressSlot3.deliveryDate = "9-1-2018"
                addressSlot3.dayOfWeek = "Monday"
                addressSlotList.add(addressSlot1)
                addressSlotList.add(addressSlot2)
                addressSlotList.add(addressSlot3)
                return addressSlotList
            }
    }
}