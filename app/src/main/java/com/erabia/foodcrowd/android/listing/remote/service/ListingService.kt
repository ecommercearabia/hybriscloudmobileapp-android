package com.erabia.foodcrowd.android.listing.remote.service

import com.erabia.foodcrowd.android.listing.model.Listing
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface ListingService {

    @GET("rest/v2/foodcrowd-ae/products/search/{categoryID}")
    fun getProductList(
        @Path("categoryID") categoryID: String,
        @Query("currentPage") currentPage: Int,
        @Query("pageSize") pageSize: Int,
        @Query("query") query: String,
        @Query("fields") fields: String,
        @Query("sort") sort:String
    ): Observable<Response<Listing>>

}