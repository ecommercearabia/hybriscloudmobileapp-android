package com.erabia.foodcrowd.android.loyaltypoint.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.LoyalityPointRowItemBinding
import com.erabia.foodcrowd.android.loyaltypoint.viewmodel.LoyaltyPointViewModel
import com.erabia.foodcrowd.android.loyaltypoint.model.LoyaltyPointData
import com.erabia.foodcrowd.android.loyaltypoint.model.LoyaltyPointHistroy

class LoyaltyPointAdapter (
    val loyaltyPointViewModel: LoyaltyPointViewModel
) : RecyclerView.Adapter<LoyaltyPointAdapter.ViewHolder>() {

    private val TAG: String = "LoyaltyPointAdapter"
    private var context: Context? = null

    var loyaltyPointDataList: LoyaltyPointData? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var loyaltyPointAmountDataList: String? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var loyalityPointRowItemBinding: LoyalityPointRowItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        loyalityPointRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.loyality_point_row_item, parent, false)
        return ViewHolder(loyalityPointRowItemBinding)
    }

    override fun getItemCount(): Int {
        return loyaltyPointDataList?.loyaltyPointHistroy?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val loyaltyPointData = loyaltyPointDataList?.loyaltyPointHistroy?.get(position)
        holder.itemBinding.mDatePlacedValueTextView.text =
            AppUtil.formatDate(
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "MMM dd, yyyy HH:mm a",
                loyaltyPointDataList?.loyaltyPointHistroy?.get(position)?.dateOfPurchase
        )
        holder.bind(loyaltyPointData)
    }

    inner class ViewHolder(val itemBinding: LoyalityPointRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        @SuppressLint("CheckResult")

        fun bind(loyaltyPointHistroy: LoyaltyPointHistroy?) {
            itemBinding.viewModel = loyaltyPointViewModel
            itemBinding.customerModel = loyaltyPointHistroy
//            itemBinding.buttonDelete.setSafeOnClickListener {
//                customerPaymentOption?.customerCardId?.let { it1 ->
//                    manageCardsViewModel.deleteCard(
//                        it1
//                    )
//                }
//            }
        }
    }
}