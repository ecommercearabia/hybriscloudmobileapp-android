package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class SendEmailOtp(
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("status")
    val status: Boolean? = null
)