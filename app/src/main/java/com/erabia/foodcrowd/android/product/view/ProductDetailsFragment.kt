package com.erabia.foodcrowd.android.product.view

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DiscountPriceManager
import com.erabia.foodcrowd.android.common.util.ImageLoader
import com.erabia.foodcrowd.android.databinding.FragmentProductDetailsBinding
import com.erabia.foodcrowd.android.home.adapter.HomeListingAdapter
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.product.adapter.ProductImageBannerAdapter
import com.erabia.foodcrowd.android.product.di.DaggerIProductComponent
import com.erabia.foodcrowd.android.product.di.IProductComponent
import com.erabia.foodcrowd.android.product.di.ProductModule
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.android.synthetic.main.cart_row_item.view.*
import javax.inject.Inject


class ProductDetailsFragment : Fragment() {

    private var selectedImagePosition: Int = 0
    private var productCode: String = ""
    private var categoryId: String = ""
    lateinit var binding: FragmentProductDetailsBinding
    lateinit var daggerComponent: IProductComponent
    var imagesList: List<Image>? = listOf()
    var product: Product? = null

    @Inject
    lateinit var factory: ProductViewModel.Factory

    @Inject
    lateinit var highLightedProductsViewModel: HighLightedProductsViewModel
    lateinit var imageBannerAdapter: ProductImageBannerAdapter
    lateinit var viewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).toolbar.navigationIcon =
            ContextCompat.getDrawable((activity as MainActivity), R.drawable.ic_arrow_back_24dp)
        setHasOptionsMenu(true)

        daggerComponent =
            DaggerIProductComponent.builder().productModule(context?.let { ProductModule(it) })
                .build()
        daggerComponent.inject(this)

        productCode = ""

        arguments?.let { productCode = it.getString(Constants.INTENT_KEY.PRODUCT_CODE) ?: "" }
//        categoryId = arguments?.getString("categoryId") ?: ""

    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).toolbar.navigationIcon =
            ContextCompat.getDrawable((activity as MainActivity), R.drawable.ic_arrow_back_24dp)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
//                setFragmentResult(
//                    "fromDetails",
//                    bundleOf("categoryId" to categoryId)
//                )
                this.findNavController().popBackStack()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)
        viewModel = ViewModelProvider(this, factory).get(ProductViewModel::class.java)

        binding.viewModel = viewModel
        viewModel.getProductDetails(productCode)

//        (activity as MainActivity).toolbar.setNavigationOnClickListener {
//            this.findNavController().popBackStack()
//
//            findNavController().popBackStack()
//        }

//        requireActivity().onBackPressedDispatcher.addCallback(
//            viewLifecycleOwner,
//            object : OnBackPressedCallback(true) {
//                override fun handleOnBackPressed() {
//                    setFragmentResult(
//                        "fromDetails",
//                        bundleOf("categoryId" to categoryId)
//
//                    )
//                    findNavController().popBackStack()
//                    // if you want onBackPressed() to be called as normal afterwards
//
//                }
//            }
//        )
        observeProduct()
        observeProductReference()
        observeBadgeNumber()
        observeYouMayAlsoLikeLabel()
        observeError()
        observeProgressBarCart()
        observeErrorCart()
        observeProgressBar()
        observeQuantityTextValue()
        observeAddToCartSuccess()
        observeProductInfoVisibility()
        observeReviewVisibility()
        observeNutritionalVisibility()
        observeNavigateToProductDetails()
        observePreSelectedZoomImagePosition()
        observeNavigateToZoom()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).toolbar.navigationIcon =
            ContextCompat.getDrawable((activity as MainActivity), R.drawable.ic_arrow_back_24dp)
        binding.mSeeMoreReviewTextView.setOnClickListener {
            findNavController().navigate(
                R.id.action_details_to_reviewListFragment,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to productCode)
            )
        }


        binding.mWriteReviewButton.setOnClickListener {
            findNavController().navigate(
                R.id.action_details_to_addReviewFragment,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to productCode)
            )
        }

        binding.buttonAddToCart.setSafeOnClickListener {
            product?.let { it1 -> viewModel.onAddToCartClick(it1) }
        }


        binding.helpIconDetails.setSafeOnClickListener {
            AlertDialog.Builder(requireContext())
                .setMessage(getString(R.string.express_delivery_tooltip))
                .setPositiveButton(getString(R.string.learn_more),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        findNavController().navigate(
                            R.id.action_detailsScreen_to_whatIsExpressFragment2
                        )
                        dialogInterface.dismiss()
                    })
                .setNegativeButton(getString(R.string.close),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                .create()
                .show()

//            SimpleTooltip.Builder(context)
//                .anchorView(binding?.helpIconDetails)
//                .text(getString(R.string.express_delivery_tooltip))
//                .textColor(R.color.deliveryDayPreviousBtnColor)
//                .gravity(Gravity.TOP)
//                .animated(false)
//                .transparentOverlay(false)
//                .ignoreOverlay(true)
//                .backgroundColor(resources.getColor(R.color.base_bk))
//                .arrowColor(resources.getColor(R.color.base_bk))
//                .onDismissListener { onExpressDeliveryDetailClick() }
//                .dismissOnInsideTouch(true)
//                .build()
//                .show()
        }

    }

    @SuppressLint("ResourceType")
    private fun onExpressDeliveryDetailClick() {
        findNavController().navigate(R.id.action_detailsScreen_to_whatIsExpressFragment2)
    }

    private fun observeProduct() {
        viewModel.product.observe(viewLifecycleOwner, Observer {
            binding.product = it
            product = it

            val mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }

            val viewItemParams = Bundle()
            it.price?.currencyIso?.let { it1 ->
                viewItemParams.putString(
                    FirebaseAnalytics.Param.CURRENCY,
                    it1 ?: ""
                )
            }
            it.price?.value?.let { it1 ->
                viewItemParams.putDouble(
                    FirebaseAnalytics.Param.VALUE,
                    it1 ?: 0.0
                )
            }
            viewItemParams.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(productBundle))

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, viewItemParams)

            if (product?.stock?.stockLevel ?: 0 > 0 && product?.stock?.stockLevelStatus != "outOfStock") {
                binding.buttonAddToCart.visibility = View.VISIBLE
                binding.clQuantity.visibility = View.VISIBLE
            } else {
                binding.buttonAddToCart.visibility = View.GONE
                binding.clQuantity.visibility = View.GONE
            }

            try {
                binding.textViewNote.text =
                    " - ${it.baseOptions?.get(0)?.selected?.variantOptionQualifiers?.get(0)?.value}"

                val discountPriceManager = DiscountPriceManager(it)
                discountPriceManager.setupPriceView(
                    binding.textViewProductPrice,
                    binding.textViewProductOldPrice,
                    binding.textViewDiscount
                )

            } catch (e: Exception) {
                Log.d(HomeListingAdapter.TAG, e.message ?: "")
            }
            try {
//                val countryOfOrigin =
//                    it.classifications?.get(0)?.features?.get(2)?.featureValues?.get(0)?.value
               var countryFlag = if (it?.countryOfOrigin == null) {
                    it.countryOfOriginIsocode.toString()
                } else{
                    it.countryOfOrigin.toString()
                }
                ImageLoader().setImageFlag(binding.imageViewFlag, countryFlag)
                binding.textViewOrigin.text = countryFlag
            } catch (e: Exception) {
                Log.d(HomeListingAdapter.TAG, e.message ?: "")
            }
            imageBannerAdapter = ProductImageBannerAdapter(this, viewModel)
            binding.viewPagerBanner.adapter = imageBannerAdapter

            imagesList = it.images?.filter { it.format == "zoom" && it.imageType == "GALLERY" }
//            viewModel.imagesList.value=it.images?.filter { it.format == "zoom" }
            imageBannerAdapter.imagesList = imagesList
            binding.dots.attachViewPager(binding.viewPagerBanner)
            var builder2 = StringBuilder()
            for (s1 in it.classifications?.get(0)?.features?.get(0)?.featureValues!!) {
                builder2.append(s1.value + " ")
            }
//            binding.textViewStorage.text =
//                it.classifications?.get(0)?.features?.get(0)?.name + " " + builder2.toString() ?: ""
            val storageInstruction =
                it.classifications?.get(0)?.features?.find { it.name == "Storage Instructions" }
            if (storageInstruction != null) {
                binding.textViewStorage.visibility = View.VISIBLE
                var textStorage = ""
                storageInstruction?.featureValues?.forEach {
                    if (it.value.isNotEmpty()) {
                        if (textStorage.count() > 0) {
                            textStorage = textStorage.plus(", ")
                        }
                        textStorage = textStorage.plus(it.value)
                    }
                }
                binding.textViewStorage.text = "Storage Instructions : $textStorage" ?: ""
            } else {
                binding.textViewStorage.visibility = View.INVISIBLE
            }

            if (it.organic == true) {
                binding.mOrganicAppCompatImageView.visibility = View.VISIBLE

            } else {
                binding.mOrganicAppCompatImageView.visibility = View.GONE

            }

            if (it.vegan == true) {
                binding.mVeganAppCompatImageView.visibility = View.VISIBLE

            } else {
                binding.mVeganAppCompatImageView.visibility = View.GONE

            }


            if (it.glutenFree == true) {
                binding.mGlutenFreeAppCompatImageView.visibility = View.VISIBLE

            } else {
                binding.mGlutenFreeAppCompatImageView.visibility = View.GONE
            }

            if (it.chilled == true) {
                binding.mChilledImageView.visibility = View.VISIBLE
            } else {
                binding.mChilledImageView.visibility = View.GONE
            }

            if (it.dry == true) {
                binding.mDryImageView.visibility = View.VISIBLE
            } else {
                binding.mDryImageView.visibility = View.GONE
            }

            if (it.frozen == true) {
                binding.mFrozenImageView.visibility = View.VISIBLE
            } else {
                binding.mFrozenImageView.visibility = View.GONE
            }

            if (it.express == true) {
                binding.expressDelivery.visibility = View.VISIBLE
                binding.expressDeliveryText.visibility = View.GONE
                binding.helpIconDetails.visibility = View.VISIBLE
            } else {
                binding.expressDelivery.visibility = View.GONE
                binding.expressDeliveryText.visibility = View.GONE
                binding.helpIconDetails.visibility = View.GONE
            }

            if (it.nutritionFacts.isNullOrEmpty()) {
                binding.textViewNutritionalFacts.visibility = View.GONE
                binding.imageViewNutritionalArrow.visibility = View.GONE
                binding.textViewNutritionalFactsValue.visibility = View.GONE
                binding.mNutritionalGroup.visibility = View.GONE
                binding.view2.visibility = View.GONE
            }

                if (it.unitOfMeasureDescription.isNullOrEmpty().not()) {
                binding.textViewMeasureDescription.visibility = View.VISIBLE
            } else {
                binding.textViewMeasureDescription.visibility = View.GONE
            }

            if (it.summary.isNullOrEmpty().not()) {
                binding.textViewSummary.visibility = View.VISIBLE
            } else {
                binding.textViewSummary.visibility = View.GONE
            }

            if ((it.stock?.stockLevel ?: 0) <= 0) {
                binding.imageViewInStockIcon.visibility = View.GONE
                binding.textViewInStock.text = getString(R.string.out_of_stock)
            } else {
                binding.imageViewInStockIcon.visibility = View.VISIBLE
                binding.textViewInStock.text = getString(R.string.in_stock)
            }

            if (it.potentialPromotions.isNullOrEmpty()) {
                binding.textViewProductPotentialPromotions.visibility = View.GONE
            } else {
                if (it.potentialPromotions?.get(0)?.description == null) {
                    binding.textViewProductPotentialPromotions.visibility = View.GONE
                } else {
                    binding.textViewProductPotentialPromotions.text =
                        it.potentialPromotions?.get(0)?.description
                }
            }
            prepareClassification(it)
        })
    }

    private fun observeProductReference() {
        viewModel.productReferenceLiveData.observe(viewLifecycleOwner, Observer {
            prepareListingAdapter(highLightedProductsViewModel, binding,
                it?.map { productReference -> productReference.target.code } as MutableList<String>)
        })
    }

    private fun observeBadgeNumber() {
        viewModel.badgeNumber.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }

    private fun prepareListingAdapter(
        itemViewModel: HighLightedProductsViewModel,
        binding: FragmentProductDetailsBinding,
        productCodeList: MutableList<String>
    ) {
        val listingAdapter = HomeListingAdapter(itemViewModel, viewModel, false, -1)
        listingAdapter.setHasStableIds(true)
        listingAdapter.productList =
            productCodeList.map { Product().apply { code = it } }.toMutableList()
        binding.recyclerViewRecentlyProduct.itemAnimator = null

        binding.recyclerViewRecentlyProduct.adapter = listingAdapter
        observeHighlightedProductViewModel(
            highLightedProductsViewModel,
            binding.recyclerViewRecentlyProduct,
            listingAdapter
        )


    }

    private fun observeYouMayAlsoLikeLabel() {
        viewModel.youMayAlsoLike.observe(viewLifecycleOwner, Observer {
            binding.textViewRecentlyProduct.visibility = it
        })
    }

    private fun prepareClassification(product: Product?) {
//        if (product?.classifications!=null){
//            val classificationAdapter = ClassificationAdapter()
//            binding.recyclerViewProductInfo.adapter = classificationAdapter
//            classificationAdapter.classificationList = product?.classifications
//        }

    }

    private fun observeProgressBar() {
        viewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }

        })
    }

    private fun observeError() {
        viewModel.error.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)
            Log.d(HomeFragment.TAG, it)
        })
    }

    private fun observeProgressBarCart() {
        viewModel.progressBarVisibilityCart.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }

        })
    }

    private fun observeErrorCart() {
        viewModel.errorCart.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)
            Log.d(HomeFragment.TAG, it)
        })
    }

    private fun observeQuantityTextValue() {
        viewModel.quantityTextValue.observe(viewLifecycleOwner, Observer {
            binding.textViewQuantity.text = it
        })
    }

    private fun observeAddToCartSuccess() {
        viewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.added_to_cart_message)

            val mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.entry.product?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.entry.product?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.entry.product?.price?.value?.let { it1 ->
                    putDouble(
                        FirebaseAnalytics.Param.PRICE,
                        it1 ?: 0.0
                    )
                }
            }


            val addToCartParams = Bundle()

            it.entry.product?.price?.currencyIso?.let { it1 ->
                addToCartParams.putString(
                    FirebaseAnalytics.Param.CURRENCY, it1 ?: ""
                )
            }
            it.entry.product?.price?.value?.let { it1 ->
                addToCartParams.putDouble(
                    FirebaseAnalytics.Param.VALUE,
                    it1 ?: 0.0
                )
            }

            addToCartParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                arrayOf(productBundle)
            )

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, addToCartParams)

        })
    }

    private fun observeProductInfoVisibility() {
        viewModel.productInfoVisibility.observe(viewLifecycleOwner, Observer {
            binding.groupProductInformation.visibility = it
            if (it == View.GONE) {
                binding.imageViewArrow.setImageResource(R.drawable.ic_arrow_right_24)
            } else {
                binding.imageViewArrow.setImageResource(R.drawable.ic_arrow_down)
            }
        })
    }


    private fun observeReviewVisibility() {
        viewModel.reviewVisibility.observe(viewLifecycleOwner, Observer {
            binding.mReviewGroup.visibility = it
            if (it == View.GONE) {
                binding.imageViewReviewArrow.setImageResource(R.drawable.ic_arrow_right_24)
            } else {
                binding.imageViewReviewArrow.setImageResource(R.drawable.ic_arrow_down)
            }
        })
    }

    private fun observeNutritionalVisibility() {
        viewModel.nutritionalVisibility.observe(viewLifecycleOwner, Observer {
            binding.mNutritionalGroup.visibility = it
            if (it == View.GONE) {
                binding.imageViewNutritionalArrow.setImageResource(R.drawable.ic_arrow_right_24)
            } else {
                binding.imageViewNutritionalArrow.setImageResource(R.drawable.ic_arrow_down)
            }
        })
    }


    private fun observeHighlightedProductViewModel(
        highLightedProductsViewModel: HighLightedProductsViewModel,
        recyclerView: RecyclerView,
        homeListingAdapter: HomeListingAdapter
    ) {
        highLightedProductsViewModel.productLiveData.observe(
            viewLifecycleOwner, Observer {
                //notifyItemChanged() should called on ui thread , it will crashed if its called on
                //background thread
                recyclerView.post {
                    homeListingAdapter.updateItem(it)
                }
            })
    }

    private fun observeNavigateToProductDetails() {
        viewModel.navigateToProductDetails.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to it?.code)
            )
        })

    }

    private fun observeNavigateToZoom() {
        viewModel.navigateToZoomEvent.observe(viewLifecycleOwner, Observer {
            findNavController()
                .navigate(
                    R.id.action_details_to_zoomFragment,
                    bundleOf(
                        "url" to it,
                        "imageList" to imagesList,
                        "selectedPosition" to selectedImagePosition
                    )
                )
        })

    }

    private fun observePreSelectedZoomImagePosition() {
        viewModel.preSelectedZoomImagePosition.observe(viewLifecycleOwner, Observer {
            selectedImagePosition = it
        })

    }

    override fun onDestroy() {
        setFragmentResult(
            "SEARCH_FRAGMENT",
            bundleOf("SearchKey" to "search")
        )
        super.onDestroy()
    }
}