package com.erabia.foodcrowd.android.newaddress.model


import com.google.gson.annotations.SerializedName

data class AreaListResponse(
    @SerializedName("areas")
    var areas: List<Area?>? = listOf()
) {
    data class Area(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) {
        override fun toString(): String {
            return name ?: ""
        }
    }
}