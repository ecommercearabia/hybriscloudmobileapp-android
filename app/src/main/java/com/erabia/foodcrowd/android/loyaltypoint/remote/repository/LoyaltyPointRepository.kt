package com.erabia.foodcrowd.android.loyaltypoint.remote.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.loyaltypoint.remote.service.ILoyaltyPoint
import com.erabia.foodcrowd.android.loyaltypoint.model.LoyaltyPointData

class LoyaltyPointRepository (val iLoyaltyPoint: ILoyaltyPoint) : Repository {
    companion object {
        const val TAG: String = "LoyaltyPointRepository"
    }

    val loyaltyPointLiveData: MutableLiveData<LoyaltyPointData> by lazy { MutableLiveData<LoyaltyPointData>() }
    val loyaltyPointAmountLiveData: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    fun loadLoyaltyPoint(
        userId: String
    ): LiveData<LoyaltyPointData> {
        iLoyaltyPoint.getLoyaltyPointHistory(
            "current",
            Constants.FIELDS_FULL
        ).get()
            .subscribe(this,
                onSuccess_200 = {
                    loyaltyPointLiveData.value = it?.body()
                },
                doOnSubscribe = {
                }
            )
        return loyaltyPointLiveData
    }

    fun loadLoyaltyPointAmount(
        userId: String
    ): LiveData<String> {
        iLoyaltyPoint.getLoyaltyPoint(
            "current",
            Constants.FIELDS_FULL
        ).get()
            .subscribe(this,
                onSuccess_200 = {
                    loyaltyPointAmountLiveData.value = it?.body()
                },
                doOnSubscribe = {
                }
            )
        return loyaltyPointAmountLiveData
    }
}