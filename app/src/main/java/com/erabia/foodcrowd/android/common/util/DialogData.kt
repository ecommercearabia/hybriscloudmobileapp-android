package com.erabia.foodcrowd.android.common.util

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.adapter.AreaNameAdapter
import com.erabia.foodcrowd.android.common.adapter.CountryCodeAdapter
import com.erabia.foodcrowd.android.common.adapter.PersonalDetailsCountryCodeAdapter
import com.erabia.foodcrowd.android.common.adapter.RegisterCountryCodeAdapter
import com.erabia.foodcrowd.android.newaddress.model.AreaListResponse.Area
import com.erabia.foodcrowd.android.common.model.City
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.newaddress.model.AreaListResponse
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddAddressViewModel
import com.erabia.foodcrowd.android.newaddress.viewmodel.EditAddressViewModel
import com.erabia.foodcrowd.android.personaldetails.viewmodel.PersonalDetailsViewModel
import com.erabia.foodcrowd.android.referralcode.viewmodel.ReferralCodeViewModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.viewmodel.SignUpViewModel


class DialogData {
    companion object {
        fun getCountryDialog(
            context: Context?,
            addressViewModel: AddressViewModel,
            CountryList: List<Country>
        ) {
            var position: Int = 0
            val list = Country().getCountryName(CountryList)
            var label: String = ""
            label = context?.getString(R.string.country_label) ?: ""

            val builder = AlertDialog.Builder(context)
//            if (parentDialogViewModel is RightDropDownViewModel) {
//                label = parentDialogViewModel.label
//            } else {
//                label = context?.getString(R.string.code) ?: ""
//            }
            builder.setItems(list.toTypedArray(), object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    position = which
                    addressViewModel.selectedCountry?.postValue(CountryList[position])
                }
            })

            builder.setTitle(label)
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, which ->
                    addressViewModel.selectedCountry?.postValue(CountryList[position])
                })
            builder.setNegativeButton(
                R.string.cancel,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.cancel()
                })

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        fun getCityDialog(
            context: Context?,
            addressViewModel: AddressViewModel,
            CityList: List<City>
        ) {
            var position: Int = 0
            val list = City().getCityName(CityList)
            var label: String = ""
            label = context?.getString(R.string.city) ?: ""

            val builder = AlertDialog.Builder(context)

            builder.setItems(list.toTypedArray(), object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    position = which
                    addressViewModel.selectedCity.postValue(CityList[position])
                }
            })

            builder.setTitle(label)
                .setPositiveButton(R.string.ok, DialogInterface.OnClickListener { dialog, which ->
                    addressViewModel.selectedCity.postValue(CityList[position])
                })
            builder.setNegativeButton(
                R.string.cancel,
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.cancel()
                })

            val dialog: AlertDialog = builder.create()
            dialog.show()
        }

        fun getCountryCode(
            context: Context?,
            countryList: List<Country>,
            addressViewModel: AddressViewModel
        ): AlertDialog {
            val view = LayoutInflater.from(context).inflate(
                R.layout.dailog_country_search,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)
            val searchView = view.findViewById<SearchView>(R.id.autocomplete_search)
            val recyclerView =
                view.findViewById<RecyclerView>(R.id.recycler_view_country)
            searchView.onActionViewExpanded()

            var adapter =
                CountryCodeAdapter(countryList.sortedBy { it.isocode }, addressViewModel)
            adapter.setHasStableIds(true)
            recyclerView.adapter = adapter

            searchView
                .setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        adapter.filter.filter(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        adapter.filter.filter(newText)
                        return false
                    }

                })


            view.findViewById<Button>(R.id.button_ok).setOnClickListener(View.OnClickListener {
                addressViewModel.mobileSelectedCountry.postValue(addressViewModel.countryCode)
                builder.dismiss()

            })
            view.findViewById<Button>(R.id.button_cancel).setOnClickListener(View.OnClickListener {
                builder.dismiss()
            })
            builder.show()
            return builder
        }

        fun getAreaDialog(
            context: Context?,
            addressViewModel: AddAddressViewModel?,
            editAddressViewModel: EditAddressViewModel?,
            areaList: List<AreaListResponse.Area>
        ):AlertDialog {

            val view = LayoutInflater.from(context).inflate(
                R.layout.dailog_country_search,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)
            val searchView = view.findViewById<SearchView>(R.id.autocomplete_search)
            val textViewTitle = view.findViewById<TextView>(R.id.textView_title)
            val recyclerView =
                view.findViewById<RecyclerView>(R.id.recycler_view_country)
//            searchView.onActionViewExpanded()
//            searchView.onActionViewExpanded()

            if (areaList.size <= 1) {
                searchView.visibility = View.INVISIBLE
                textViewTitle.visibility = View.VISIBLE
            } else {
                searchView.visibility = View.VISIBLE
                textViewTitle.visibility = View.INVISIBLE
            }

            var adapter =
                AreaNameAdapter(areaList.sortedBy { it.code })
            adapter.setHasStableIds(true)
            recyclerView.adapter = adapter

            searchView
                .setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        adapter.filter.filter(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        adapter.filter.filter(newText)
                        return false
                    }

                })

            adapter.onAreaClickListener =
                object : AreaNameAdapter.OnAreaClickListener {
                    override fun onAreaClick(area: Area) {
                        if(addressViewModel != null) {
                            addressViewModel.mAreaSelection.postValue(area)
                        }else {
                            editAddressViewModel!!.mAreaSelection.postValue(area)
                        }
                        builder.dismiss()
                    }

                }

            view.findViewById<Button>(R.id.button_cancel).setOnClickListener(View.OnClickListener {
                builder.dismiss()
            })
            builder.show()
            return builder
        }

        fun getShareVia(
            context: Context?,
            referralCodeViewModel: ReferralCodeViewModel,
            referralCode: String
        )
                : AlertDialog {
            val view = LayoutInflater.from(context).inflate(
                R.layout.dailog_share_via,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)


            view.findViewById<Button>(R.id.send_via_email_button)
                .setOnClickListener(View.OnClickListener {
                    getShareViaEmail(context, referralCodeViewModel)
                    builder.dismiss()
                })

            view.findViewById<Button>(R.id.send_via_social_media_button)
                .setOnClickListener(View.OnClickListener {
                    referralCodeViewModel.shareClickEvent.postValue(referralCode)
                    builder.dismiss()
                })

            builder.show()
            return builder
        }

        fun getShareViaEmail(
            context: Context?,
            referralCodeViewModel: ReferralCodeViewModel
        )
                : AlertDialog {
            val view = LayoutInflater.from(context).inflate(
                R.layout.send_referral_code_via_email,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)


            view.findViewById<EditText>(R.id.mEmailsEditText).doAfterTextChanged {
                if (view.findViewById<EditText>(R.id.mEmailsEditText).text.isNotEmpty()) {
                    view.findViewById<Button>(R.id.send_button).isEnabled = true
                } else {
                    view.findViewById<Button>(R.id.send_button).isEnabled = false
                }
            }

            view.findViewById<Button>(R.id.send_button).setOnClickListener(View.OnClickListener {
                referralCodeViewModel.emails =
                    view.findViewById<EditText>(R.id.mEmailsEditText).text.toString()
                referralCodeViewModel.onShareViaEmail()
                builder.dismiss()
            })

            view.findViewById<Button>(R.id.cancel_button).setOnClickListener(View.OnClickListener {
                builder.dismiss()
            })
            builder.show()
            return builder
        }

        fun getRegisterCountryCode(
            context: Context?,
            countryList: List<CountryResponse.Country>,
            signUpViewModel: SignUpViewModel
        ): AlertDialog {
            val view = LayoutInflater.from(context).inflate(
                R.layout.dailog_country_search,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)
            val searchView = view.findViewById<SearchView>(R.id.autocomplete_search)
            val textViewTitle = view.findViewById<TextView>(R.id.textView_title)
            val recyclerView =
                view.findViewById<RecyclerView>(R.id.recycler_view_country)
            searchView.onActionViewExpanded()

            if (countryList.size <= 1) {
                searchView.visibility = View.INVISIBLE
                textViewTitle.visibility = View.VISIBLE
            } else {
                searchView.visibility = View.VISIBLE
                textViewTitle.visibility = View.INVISIBLE
            }

            var adapter =
                RegisterCountryCodeAdapter(countryList.sortedBy { it.isocode }, signUpViewModel)
            adapter.setHasStableIds(true)
            recyclerView.adapter = adapter

            searchView
                .setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        adapter.filter.filter(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        adapter.filter.filter(newText)
                        return false
                    }

                })

            adapter.onCountryCodeClickListener =
                object : RegisterCountryCodeAdapter.OnCountryCodeClickListener {
                    override fun onCountryCodeClick(country: CountryResponse.Country) {
                        signUpViewModel.selectedCountry.postValue(country)
                        builder.dismiss()
                    }

                }

//            view.findViewById<Button>(R.id.button_ok).setOnClickListener(View.OnClickListener {
//                signUpViewModel.selectedCountry.postValue(signUpViewModel.countryCode)
//                builder.dismiss()
//
//            })
            view.findViewById<Button>(R.id.button_cancel).setOnClickListener(View.OnClickListener {
                builder.dismiss()
            })
            builder.show()
            return builder
        }


        fun getPersonalDetailsCountryCode(
            context: Context?,
            countryList: List<CountryResponse.Country>,
            mPersonalDetailsViewModel: PersonalDetailsViewModel
        ): AlertDialog {
            val view = LayoutInflater.from(context).inflate(
                R.layout.dailog_country_search,
                null
            ) as ConstraintLayout
            val builder = AlertDialog.Builder(context).create()
            builder.setView(view)
            val searchView = view.findViewById<SearchView>(R.id.autocomplete_search)
            val recyclerView =
                view.findViewById<RecyclerView>(R.id.recycler_view_country)
            searchView.onActionViewExpanded()

            var adapter =
                PersonalDetailsCountryCodeAdapter(
                    countryList.sortedBy { it.isocode },
                    mPersonalDetailsViewModel
                )
            adapter.setHasStableIds(true)
            recyclerView.adapter = adapter

            searchView
                .setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        adapter.filter.filter(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {
                        adapter.filter.filter(newText)
                        return false
                    }

                })


            view.findViewById<Button>(R.id.button_ok).visibility = View.VISIBLE
            view.findViewById<Button>(R.id.button_ok).setOnClickListener(View.OnClickListener {
                mPersonalDetailsViewModel.selectedCountry.postValue(mPersonalDetailsViewModel.countryCode)
                builder.dismiss()

            })
            view.findViewById<Button>(R.id.button_cancel).setOnClickListener(View.OnClickListener {
                builder.dismiss()
            })
            builder.show()
            return builder
        }


    }


}