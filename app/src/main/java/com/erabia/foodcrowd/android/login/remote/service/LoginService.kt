package com.erabia.foodcrowd.android.home.remote.service

import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.network.AuthInterceptor
import com.google.gson.Gson
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface LoginService {
    @POST("/authorizationserver/oauth/token")
    fun getOauth(
        @Query("grant_type") grant_type: String,
        @Query("username") username: String,
        @Query("password") password: String,
        @Query("scope") scope: String,
        @Query("client_id") client_id: String,
        @Query("client_secret") client_secret: String
    ): Observable<Response<TokenModel>>


    @POST("rest/v2/foodcrowd-ae/login/token")
    fun loginEmailMobile(
        @Query("identifier") identifier: String,
        @Query("password") password: String,
        @Header("clientId") clientId: String,
        @Header("clientSecret") clientSecret: String
    ): Observable<Response<TokenModel>>

    @POST("rest/v2/foodcrowd-ae/signature/auth")
    fun doFingerPrintLogin(
        @Query("signatureId") signatureId: String,
        @Query("email") email: String
    ): Observable<Response<TokenModel>>


    @PATCH("rest/v2/foodcrowd-ae/users/{userId}/mobile-token")
    fun setFirebaseToken(
        @Path("userId") userId: String,
        @Query("mobileToken") mobileToken: String
    ): Observable<Response<Product>>

}