package com.erabia.foodcrowd.android.newaddress.model


import com.google.gson.annotations.SerializedName

data class CityListResponse(
    @SerializedName("cities")
    var cities: List<City?>? = listOf()
) {
    data class City(
        @SerializedName("areas")
        var areas: List<Area?>? = listOf(),
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) {
        data class Area(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        override fun toString(): String {
            return name ?: ""
        }
    }
}