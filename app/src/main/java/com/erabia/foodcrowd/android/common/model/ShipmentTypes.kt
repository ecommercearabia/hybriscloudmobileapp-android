package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class ShipmentTypes(
    @SerializedName("supportedShipmentTypes")
    var supportedShipmentTypes:  List<SupportedShipmentType>? =listOf()
)

data class SupportedShipmentType(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("name")
    val name: String = ""
){
}