package com.erabia.foodcrowd.android.newaddress.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.newaddress.model.PlaceAutocomplete
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.*
import java.util.*
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import kotlin.collections.ArrayList


class PlacesAutoCompleteAdapter(val context: Context) :
    RecyclerView.Adapter<PlacesAutoCompleteAdapter.ViewHolder>(), Filterable {
    private var mResultList: MutableList<PlaceAutocomplete>? = mutableListOf()
    private val placesClient: PlacesClient = Places.createClient(context)
    var clickListener: ClickListener? = null
    var hasPlacesNotifiedClickListener: HasPlacesNotifiedClickListener? = null

    companion object {
        private const val TAG = "PlacesAutoAdapter"
    }

    interface ClickListener {
        fun click(place: String?)
    }

    interface HasPlacesNotifiedClickListener {
        fun hasPlacesNotified(hasNotified: Boolean)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val convertView: View =
            layoutInflater.inflate(R.layout.place_row_item, viewGroup, false)
        return ViewHolder(convertView)
    }

    override fun onBindViewHolder(@NonNull holder: ViewHolder, i: Int) {
        holder.address.text = mResultList?.get(i)?.address
        holder.area.text = mResultList?.get(i)?.area
    }

    override fun getItemCount(): Int {
        return mResultList?.size ?: 0
    }

    /**
     * Returns the filter for the current set of autocomplete results.
     */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val results = FilterResults()
                if (constraint != null) {
                    if (constraint.toString() != "") {
                        mResultList = getPredictions(constraint)
                        results.values = mResultList
                        results.count = mResultList?.size ?: 0
                    } else {
                        mResultList = mutableListOf()
                    }
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged()
                    hasPlacesNotifiedClickListener?.hasPlacesNotified(true)
                } else {
                    notifyDataSetChanged()
                    hasPlacesNotifiedClickListener?.hasPlacesNotified(false)
                    // The API did not return any results, invalidate the data set.
                    //notifyDataSetInvalidated();
                }
            }

        }
    }

    fun getItem(position: Int): PlaceAutocomplete {
        return mResultList?.get(position) ?: PlaceAutocomplete()
    }

    inner class ViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val address: TextView = itemView.findViewById(R.id.textView_address)
        val area: TextView = itemView.findViewById(R.id.textView_area)
        val mRow: ViewGroup = itemView.findViewById(R.id.layout_place)


        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val item = mResultList?.get(adapterPosition)
            if (v.id == R.id.layout_place) {
                val placeId = item?.placeId.toString()
                val placeFields: List<Place.Field> = Arrays.asList(
                    Place.Field.ID,
                    Place.Field.NAME,
                    Place.Field.LAT_LNG,
                    Place.Field.ADDRESS
                )
                val request = FetchPlaceRequest.builder(placeId, placeFields).build()
                placesClient.fetchPlace(request)
                    .addOnSuccessListener { response ->

                        val place: Place = response.place
                        clickListener!!.click(address.text.toString())
                    }.addOnFailureListener { exception ->
                        if (exception is ApiException) {
                            Toast.makeText(context, exception.message + "", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
            }
        }

    }

    private fun getPredictions(constraint: CharSequence): ArrayList<PlaceAutocomplete> {
        val resultList: ArrayList<PlaceAutocomplete> = ArrayList()

        // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
        // and once again when the user makes a selection (for example when calling fetchPlace()).
        val token = AutocompleteSessionToken.newInstance()

        // Use the builder to create a FindAutocompletePredictionsRequest.
        val request =
            FindAutocompletePredictionsRequest.builder() // Call either setLocationBias() OR setLocationRestriction().
                //.setLocationBias(bounds)
                .setCountry("AE")
//                .setTypeFilter(TypeFilter.ESTABLISHMENT)
                .setSessionToken(token)
                .setQuery(constraint.toString())
                .build()
        val autocompletePredictions: Task<FindAutocompletePredictionsResponse> =
            placesClient.findAutocompletePredictions(request)

        // This method should have been called off the main UI thread. Block and wait for at most
        // 60s for a result from the API.
        try {
            Tasks.await(autocompletePredictions, 60, TimeUnit.SECONDS)
        } catch (e: ExecutionException) {
            Log.d(TAG, "getPredictions: ${e.message}")
            e.printStackTrace()
        } catch (e: InterruptedException) {
            Log.d(TAG, "getPredictions: ${e.message}")
            e.printStackTrace()
        } catch (e: TimeoutException) {
            Log.d(TAG, "getPredictions: ${e.message}")
            e.printStackTrace()
        }
        return if (autocompletePredictions.isSuccessful) {
            val findAutocompletePredictionsResponse: FindAutocompletePredictionsResponse =
                autocompletePredictions.result
            for (prediction in findAutocompletePredictionsResponse.autocompletePredictions) {
                Log.i(TAG, prediction.placeId)
                resultList.add(
                    PlaceAutocomplete(
                        prediction.placeId,
                        prediction.getPrimaryText(null).toString(),
                        prediction.getFullText(null).toString()
                    )
                )
            }
            resultList
        } else {
            resultList
        }
    }

    fun clear() {
        mResultList = mutableListOf()
    }
}