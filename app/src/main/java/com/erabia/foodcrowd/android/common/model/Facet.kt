package com.erabia.foodcrowd.android.common.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Facet(

    @SerializedName("category")
    val category: Boolean? = null,
    @SerializedName("multiSelect")
    val multiSelect: Boolean? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("priority")
    val priority: Int? = null,
    @SerializedName("values")
    val values: List<Value>? = null,
    @SerializedName("visible")
    val visible: Boolean? = null
):Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readString(),
        parcel.readValue(Int::class.java.classLoader) as? Int,
        TODO("values"),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {

    }

    override fun describeContents(): Int {
      return 0
    }

    companion object CREATOR : Parcelable.Creator<Facet> {
        override fun createFromParcel(parcel: Parcel): Facet {
            return Facet(parcel)
        }

        override fun newArray(size: Int): Array<Facet?> {
            return arrayOfNulls(size)
        }
    }
}