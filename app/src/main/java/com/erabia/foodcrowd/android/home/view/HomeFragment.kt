package com.erabia.foodcrowd.android.home.view

//import com.erabia.foodcrowd.android.databinding.FragmentHomeBinding

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.db.DataBaseManager
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentHomeBinding
import com.erabia.foodcrowd.android.home.adapter.HomeAdapter
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
import com.erabia.foodcrowd.android.login.view.LoginActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.disposables.CompositeDisposable


class HomeFragment : Fragment() {
    val compositeDisposable = CompositeDisposable()

    companion object {
        fun newInstance() = HomeFragment()
        const val TAG = "HomeFragment"
    }

    private var homeAdapter: HomeAdapter? = null
    private lateinit var binding: FragmentHomeBinding
    private lateinit var viewModel: HomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // TODO: add viewmodel facotry
        val dataBase = DataBaseManager.getInstance(requireContext())
        val homeService = RequestManager.getClient().create(HomeService::class.java)
        val cartService = RequestManager.getClient().create(CartService::class.java)
        val factory = HomeViewModel.Factory(
            HomeRepository(
                homeService,
                cartService,
                dataBase.homeDao()
            ),
            CartRepository(cartService),
            WishListRepository(RequestManager.getClient().create(WishListService::class.java))
        )
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)


//            val biometricManager = context?.let { BiometricManager.from(it) }

//        if (!SharedPreference.getInstance().getLoginEmail().isNullOrEmpty()){
//            when (biometricManager?.canAuthenticate(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)) {
//
//                BiometricManager.BIOMETRIC_SUCCESS -> {
//                    if (SharedPreference.getInstance().isFingerPrintEnabled())
//                        alertDialog(
//                            "are you need to use FingerPrint ID to login?",
//                            "FingerPrint ID"
//                        )
//                    Log.d("MY_APP_TAG", "App can authenticate using biometrics.")
//
//                }
//                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
//                    Log.e("MY_APP_TAG", "No biometric features available on this device.")
//                    SharedPreference.getInstance().setFingerPrintEnabled(false)
//
//                }
//                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
//                    Log.e("MY_APP_TAG", "Biometric features are currently unavailable.")
//                    SharedPreference.getInstance().setFingerPrintEnabled(false)
//
//                }
//                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
//                    // Prompts the user to create credentials that your app accepts.
//                    SharedPreference.getInstance().setFingerPrintEnabled(false)
//                    Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED")
//                }
//                else -> {
//                    // Check if we're running on Android 6.0 (M) or higher
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        //Fingerprint API only available on from Android 6.0 (M)
//                        val fingerprintManager =
//                            requireContext().getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
//                        if (!fingerprintManager.isHardwareDetected) {
//                            SharedPreference.getInstance().setFingerPrintEnabled(false)
//                        } else if (!fingerprintManager.hasEnrolledFingerprints()) {
//                            SharedPreference.getInstance().setFingerPrintEnabled(false)
//                            // User hasn't enrolled any fingerprints to authenticate with
//                        } else {
//
//
//
//                            if (!SharedPreference.getInstance().isFingerPrintEnabled())
//                                alertDialog(
//                                    "are you need to use FingerPrint ID to login?",
//                                    "FingerPrint ID"
//                                )
//                            // Everything is ready for fingerprint authentication
//                        }
//                    }
//
//                }
//            }
//
//        }


//        SharedPreference.getInstance().setUser_TOKEN("ztXAofz1jIOx2Uuo3iooN_3w1Nw")


        if (!this::binding.isInitialized) {
            viewModel.getCart("", "", Constants.FIELDS_FULL)
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
            binding.viewModel = viewModel
            homeAdapter =
                HomeAdapter(this, viewModel, findNavController(), homeService, dataBase.homeDao())
            binding.recyclerView.adapter = homeAdapter
            binding.swipeRefreshLayout.setOnRefreshListener { viewModel.loadHome() }
            viewModel.loadHome()

        }

        observeItemBanner()
        observeHideRefreshLoaderEvent()
        observeBannerListSubject()
        observeTabListSubject()
        observeBannerSizeSubject()
        observeSlots()
        observeAddToCart()
        observeBadgeNumber()
        observeNavigateToProductDetails()
        observeNavigateBannerToDetails()
        observeNavigateBannerToListing()
        observeProgressBar()
        observeError()
        observeAddToWishlistEvent()
        observeStartLogin()


        if (SharedPreference.getInstance().getLoginEmail() != "") {
            if(SharedPreference.getInstance().getFIREBASE_TOKEN() != ""){
                viewModel.setFirebaseTokeb("current" ,
                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString() )
            }
        }


        return binding.root
    }

    private fun observeBannerListSubject() {
        viewModel.bannerList.observe(viewLifecycleOwner, Observer {
            homeAdapter?.bannersList = it
            Log.d(TAG, "Banner list : ${it.toString()}")
        })
    }


    private fun observeTabListSubject() {
        viewModel.tabList.observe(viewLifecycleOwner, Observer {
            homeAdapter?.tabList = it
            Log.d(TAG, "Banner list : ${it.toString()}")
        })
    }

    fun isBioMerticAvailbale(): Boolean {
        val pm: PackageManager? = context?.packageManager;
        return context?.packageManager?.hasSystemFeature(PackageManager.FEATURE_FACE) == true
//    pm?.hasSystemFeature(PackageManager.FEATURE_FACE) == true

//    pm?.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT) == true
    }

    private fun observeBannerSizeSubject() {
        viewModel.bannerSize.observe(viewLifecycleOwner, Observer {
            homeAdapter?.bannersSize = it
        })
    }

//    @SuppressLint("HardwareIds")
//    private fun alertDialog(message: String, title: String) {
//        val dialog: AlertDialog.Builder? = context?.let { AlertDialog.Builder(it) }
//        dialog?.setMessage(message)
//        dialog?.setTitle(title)
//        dialog?.setPositiveButton(
//            getString(R.string.yes)
//        ) { _, _ ->
//            val deviceId = SharedPreference.getInstance().getSIGNATURE_ID()
//            viewModel.updateSignature(deviceId ?: "")
//        }
//        dialog?.setNegativeButton(
//            getString(R.string.close)
//        ) { _, _ ->
//            dialog.create().dismiss()
//        }
//        val alertDialog: AlertDialog? = dialog?.create()
//        alertDialog?.show()
//
//    }

    //    private fun observeAddToWishlistEvent() {
//        viewModel.addToWishlistEvent.observe(this, Observer {
//            Toast.makeText(context, R.string.add_to_wishlist, Toast.LENGTH_SHORT).show()
//        })
//    }
    private fun observeAddToWishlistEvent() {
        viewModel.mAddWishListLiveData.observe(viewLifecycleOwner, Observer {
//            Toast.makeText(context, R.string.add_to_wishlist, Toast.LENGTH_SHORT).view.setBackgroundColor(
//                Color.parseColor("#F6AE2D")).show()

            AppUtil.showToastySuccess(  context , R.string.add_to_wishlist)
        })
    }

    private fun observeStartLogin() {
        viewModel.startLoginEvent.observe(this, Observer {
            context?.let {
                ContextCompat.startActivity(
                    it, Intent(context, LoginActivity::class.java), null
                )
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()

    }

    private fun observeProgressBar() {
        viewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            binding.progressBar.visibility = it
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })

    }


    private fun observeError() {
        viewModel.error.observe(viewLifecycleOwner, Observer {
            if (!it.contains("No cart created yet.") &&
                !it.contains("Cart not found.") && !it.contains("Cart is not anonymous") && !it.contains("Something wrong happened")
            ) {
                AppUtil.showToastyError(context , it)
            }


            Log.d(TAG, it)
        })

        viewModel.mErrorServer.observe(viewLifecycleOwner, Observer {
            if (!it.contains("No cart created yet.") ||
                !it.contains("Cart not found.")
            ) {
                AppUtil.showToastyError(context ,it)
            }
            Log.d(TAG, it)
        })
    }


    private fun observeSlots() {
        viewModel.contentSlots?.observe(viewLifecycleOwner, Observer {
//            binding.textViewData.text = it?.toString()
            homeAdapter?.homeList =
                Component.parseToComponent(it?.contentSlot?.filter { it.slotId != "FooterPaymentLinksSlot" }
                    ?.sortedBy { it.slotId }
                )
            Log.d(TAG, it?.toString() ?: "")
        })
    }

    private fun observeItemBanner() {
        viewModel.itemLiveData.observe(viewLifecycleOwner, Observer {

        })
    }

    private fun observeHideRefreshLoaderEvent() {
        viewModel.hideRefreshLoaderEvent.observe(viewLifecycleOwner, Observer {
            binding.swipeRefreshLayout.isRefreshing = false
        })
    }

    private fun observeAddToCart() {
        viewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
//            Toast.makeText(context, R.string.added_to_cart_message, Toast.LENGTH_LONG).show()
            AppUtil.showToastySuccess(  context , R.string.added_to_cart_message)

            val mFirebaseAnalytics =  FirebaseAnalytics.getInstance(requireContext())

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.entry.product?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.entry.product?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.entry.product?.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }


            val addToCartParams = Bundle()

            it.entry.product?.price?.currencyIso?.let { it1 -> addToCartParams.putString(FirebaseAnalytics.Param.CURRENCY, it1 ?: "") }
            it.entry.product?.price?.value?.let { it1 -> addToCartParams.putDouble(FirebaseAnalytics.Param.VALUE, it1 ?: 0.0) }

            addToCartParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                arrayOf(productBundle)
            )


            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, addToCartParams)
        })
    }

    private fun observeBadgeNumber() {
        viewModel.badgeNumber.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }

    private fun observeNavigateToProductDetails() {
        viewModel.navigateToProductDetails.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to it?.code)
            )
        })
    }


    private fun observeNavigateBannerToDetails() {
        viewModel.navigateBannerToDetails.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to it)
            )
        })
    }

    private fun observeNavigateBannerToListing() {
        viewModel.navigateBannerToListing.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_category,
                bundleOf(Constants.API_KEY.CATEGORY_ID to it)
            )
        })
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.toolbar_home_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        findNavController().navigate(R.id.action_navigate_to_search)
        return super.onOptionsItemSelected(item)
    }

}
