package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Principal(
    @SerializedName("currency")
    val currency: Currency = Currency(),
    @SerializedName("customerId")
    val customerId: String = "",
    @SerializedName("deactivationDate")
    val deactivationDate: String = "",
    @SerializedName("defaultAddress")
    val defaultAddress: DefaultAddress = DefaultAddress(),
    @SerializedName("displayUid")
    val displayUid: String = "",
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("language")
    val language: Language = Language(),
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("title")
    val title: String = "",
    @SerializedName("titleCode")
    val titleCode: String = "",
    @SerializedName("uid")
    val uid: String = ""
)