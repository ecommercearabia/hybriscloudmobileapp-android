package com.erabia.foodcrowd.android.address.model

import com.erabia.foodcrowd.android.common.model.Country
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class CountryList(
    @SerializedName("countries")
    val countries: List<Country>? = listOf()
)