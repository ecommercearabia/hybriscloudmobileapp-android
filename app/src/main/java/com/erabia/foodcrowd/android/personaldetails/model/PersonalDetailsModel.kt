package com.erabia.foodcrowd.android.personaldetails.model
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PersonalDetailsModel(
    @SerializedName("currency")
    var currency: Currency? = Currency(),
    @SerializedName("customerId")
    var customerId: String? = "",
    @SerializedName("deactivationDate")
    var deactivationDate: String? = "",
    @SerializedName("defaultAddress")
    var defaultAddress: DefaultAddress? = DefaultAddress(),
    @SerializedName("nationality")
    var nationality: Nationality? = Nationality(),
    @SerializedName("displayUid")
    var displayUid: String? = "",
    @SerializedName("firstName")
    var firstName: String? = "",
    @SerializedName("language")
    var language: Language? = Language(),
    @SerializedName("lastName")
    var lastName: String? = "",
    @SerializedName("mobileCountry")
    var mobileCountry: MobileCountry? = MobileCountry(),
    @SerializedName("mobileNumber")
    var mobileNumber: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("titleCode")
    var titleCode: String? = "",
    @SerializedName("uid")
    var uid: String? = ""
): Parcelable {
    @Parcelize
    data class Currency(
        @SerializedName("active")
        var active: Boolean? = false,
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("symbol")
        var symbol: String? = ""
    ): Parcelable
    @Parcelize
    data class DefaultAddress(
        @SerializedName("addressName")
        var addressName: String? = "",
        @SerializedName("area")
        var area: Area? = Area(),
        @SerializedName("cellphone")
        var cellphone: String? = "",
        @SerializedName("city")
        var city: City? = City(),
        @SerializedName("companyName")
        var companyName: String? = "",
        @SerializedName("country")
        var country: Country? = Country(),
        @SerializedName("defaultAddress")
        var defaultAddress: Boolean? = false,
        @SerializedName("district")
        var district: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("firstName")
        var firstName: String? = "",
        @SerializedName("formattedAddress")
        var formattedAddress: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("lastName")
        var lastName: String? = "",
        @SerializedName("line1")
        var line1: String? = "",
        @SerializedName("line2")
        var line2: String? = "",
        @SerializedName("mobileCountry")
        var mobileCountry: MobileCountry? = MobileCountry(),
        @SerializedName("mobileNumber")
        var mobileNumber: String? = "",
        @SerializedName("nearestLandmark")
        var nearestLandmark: String? = "",
        @SerializedName("phone")
        var phone: String? = "",
        @SerializedName("postalCode")
        var postalCode: String? = "",
        @SerializedName("region")
        var region: Region? = Region(),
        @SerializedName("shippingAddress")
        var shippingAddress: Boolean? = false,
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("titleCode")
        var titleCode: String? = "",
        @SerializedName("town")
        var town: String? = "",
        @SerializedName("visibleInAddressBook")
        var visibleInAddressBook: Boolean? = false
    ) : Parcelable{
        @Parcelize
        data class Area(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ): Parcelable

        @Parcelize
        data class City(
            @SerializedName("areas")
            var areas: List<Area?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) : Parcelable{
            @Parcelize
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ): Parcelable
        }
        @Parcelize
        data class Country(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ): Parcelable
        @Parcelize
        data class MobileCountry(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ): Parcelable

        @Parcelize
        data class Region(
            @SerializedName("countryIso")
            var countryIso: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("isocodeShort")
            var isocodeShort: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ): Parcelable
    }

    @Parcelize
    data class Language(
        @SerializedName("active")
        var active: Boolean? = false,
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("nativeName")
        var nativeName: String? = ""
    ): Parcelable

    @Parcelize
    data class MobileCountry(
        @SerializedName("isdcode")
        var isdcode: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ): Parcelable

    @Parcelize
    data class Nationality(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ): Parcelable
}