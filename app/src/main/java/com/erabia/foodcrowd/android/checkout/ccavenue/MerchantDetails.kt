package com.erabia.foodcrowd.android.checkout.ccavenue

import android.os.Parcel
import android.os.Parcelable

class MerchantDetails(
    var access_code: String? = null,
    var merchant_id: String? = null,
    var currency: String? = null,
    var order_id: String? = null,
    var amount: String? = null,
    var redirect_url: String? = null,
    var cancel_url: String? = null,
    var rsa_url: String? = null,
    var customer_id: String? = null,
    var add1: String? = null,
    var add2: String? = null,
    var add3: String? = null,
    var add4: String? = null,
    var add5: String? = null,
    var promo_code: String? = null,
    var show_addr: Boolean? = null,
    var CCAvenue_promo: Boolean? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean,
        parcel.readValue(Boolean::class.java.classLoader) as? Boolean
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(access_code)
        parcel.writeString(merchant_id)
        parcel.writeString(currency)
        parcel.writeString(order_id)
        parcel.writeString(amount)
        parcel.writeString(redirect_url)
        parcel.writeString(cancel_url)
        parcel.writeString(rsa_url)
        parcel.writeString(customer_id)
        parcel.writeString(add1)
        parcel.writeString(add2)
        parcel.writeString(add3)
        parcel.writeString(add4)
        parcel.writeString(add5)
        parcel.writeString(promo_code)
        parcel.writeValue(show_addr)
        parcel.writeValue(CCAvenue_promo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MerchantDetails> {
        override fun createFromParcel(parcel: Parcel): MerchantDetails {
            return MerchantDetails(parcel)
        }

        override fun newArray(size: Int): Array<MerchantDetails?> {
            return arrayOfNulls(size)
        }
    }

}