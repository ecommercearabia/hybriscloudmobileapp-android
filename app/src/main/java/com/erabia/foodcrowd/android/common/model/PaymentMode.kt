package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PaymentMode(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("description")
    val description: String = "",
    @SerializedName("name")
    val name: String = ""
)