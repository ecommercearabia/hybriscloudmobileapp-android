package com.erabia.foodcrowd.android.email.view

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.email.viewmodel.ChangeEmailViewModel
import com.erabia.foodcrowd.android.databinding.FragmentChangeEmailBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository
import com.erabia.foodcrowd.android.password.remote.service.ChangePasswordService
import io.reactivex.disposables.CompositeDisposable

class ChangeEmailFragment : Fragment() {
    var compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: SharedViewModel
    lateinit var binding: FragmentChangeEmailBinding
    var mEmailEditText: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = SharedViewModel.Factory(
            ChangePasswordRepository(
                RequestManager.getClient().create(
                    ChangePasswordService::class.java
                )
            ),
            ChangeEmailRepository(
                RequestManager.getClient().create(
                    ChangeEmailService::class.java
                ),  RequestManager.getClient().create(CartService::class.java)
            )
        )

        mViewModel = ViewModelProvider(this, factory).get(SharedViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_change_email, container, false)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_change_email, container, false)
        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel


        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerValidation()
        observerChangePassword()
        observerLogin()
        loadingVisibilityObserver()
    }


    fun observerValidation() {
        mViewModel.mErrorNewEmailValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mEmailChangeEmailEditText.error = it

        })

        mViewModel.mErrorConfrimEmailValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mConfirmEmailChangeEmailEditText.error = it

        })
        mViewModel.mErrorPasswordEmailValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mPasswordEmailChangeEmailEditText.error = it

        })
    }

    fun observerChangePassword() {
        mViewModel.mChangeEmailLiveData.observe(viewLifecycleOwner, Observer {

            SharedPreference.getInstance().setLOGIN_TOKEN("")
            SharedPreference.getInstance().setLOGIN_EMAIL("")
            SharedPreference.getInstance().setRefreshToken("")
            SharedPreference.getInstance().setUser_TOKEN("")
            SharedPreference.getInstance().setApp_TOKEN("")
//            SharedPreference.getInstance().setLOGIN_User("")
            SharedPreference.getInstance().clearSP()
            mViewModel.dataToShare.value="logout"

        })

        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)


        })

    }

    fun observerLogin() {
        mViewModel.mLoginLiveData.observe(viewLifecycleOwner, Observer {

            AppUtil.showToastySuccess(  context ,R.string.email_changed)

            Log.e("mohamedrefreshToken", it.refreshToken ?: "")
            SharedPreference.getInstance().setRefreshToken(it.refreshToken ?: "")
            SharedPreference.getInstance().setUser_TOKEN(it.accessToken ?: "")
            SharedPreference.getInstance().setApp_TOKEN(it.accessToken ?: "")
//            SharedPreference.getInstance().setLOGIN_User(mEmailLoginEditText.text.toString())
            SharedPreference.getInstance().setPreviousLoginEmail(
                SharedPreference.getInstance().getLoginEmailFingerPrint() ?: ""
            )
//            SharedPreference.getInstance().setPreviousFromEditTextLoginEmail(mEmailEditText)
            SharedPreference.getInstance().setLOGIN_EMAIL_FINGERPRINT( binding.mEmailChangeEmailEditText.text.toString())
            SharedPreference.getInstance().setLOGIN_EMAIL( binding.mEmailChangeEmailEditText.text.toString())

            activity?.onBackPressed()


        })

        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(requireActivity() , it)
        })



    }


    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibilityEmail.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }


    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
}