package com.erabia.foodcrowd.android.checkout.adapter

import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.model.StoreCreditMode
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.databinding.RowStoreCreditModeBinding

class StoreCreditAdapter(val viewModel: CheckoutViewModel) :
    RecyclerView.Adapter<StoreCreditAdapter.MyViewHolder>() {
    private var storeCreditModeSelected: StoreCreditMode? = null
    var binding: RowStoreCreditModeBinding? = null
    var showBillingAddressSection = false

    //preSelect paymentMode from cart repsonse if exist
    var preSelectedStoreCredit: String = ""
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var storeCreditModeList: List<StoreCreditMode>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var selectedPosition = -1

    var storeCreditCode: String? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_store_credit_mode,
            parent,
            false
        )
        return MyViewHolder(binding!!)
    }

    override fun getItemCount(): Int {
        return storeCreditModeList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val storeCreditMode = storeCreditModeList?.get(position)
        holder.itemBinding.viewModel = viewModel
        //PreSelected from cart api response
        if (preSelectedStoreCredit == storeCreditMode?.storeCreditModeType?.code) {
            holder.itemBinding.radioButton.isChecked = true
            viewModel.selectedStoreCreditMode = storeCreditModeList?.get(position)

            selectedPosition = position
        }

        holder.itemBinding.storeCreditMode = storeCreditMode

        holder.itemBinding.radioButton.isChecked = selectedPosition == position

//        if (storeCreditModeSelected?.storeCreditModeType?.code == "REDEEM_SPECIFIC_AMOUNT") {
//            holder.itemBinding.editTextAmount.visibility = View.VISIBLE
//        } else {
//            holder.itemBinding.editTextAmount.visibility = View.GONE
//
//        }

        holder.itemBinding.radioButton.setOnClickListener {
            var amount = ""
            if (storeCreditModeList?.get(position)?.storeCreditModeType?.code == "REDEEM_SPECIFIC_AMOUNT") {
                amount = holder.itemBinding.editTextAmount.text.toString()
                holder.itemBinding.editTextAmount.visibility = View.VISIBLE
            } else {
                holder.itemBinding.editTextAmount.visibility = View.GONE

            }
            viewModel.selectedStoreCreditMode = storeCreditModeList?.get(position)

            selectedPosition = position
            storeCreditModeSelected = storeCreditModeList?.get(position)
            preSelectedStoreCredit = ""
            storeCreditCode = ""

            notifyDataSetChanged()

        }


    }

    override fun getItemId(position: Int): Long {
        return storeCreditModeList?.get(position)?.storeCreditModeType.hashCode().toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    class MyViewHolder(var itemBinding: RowStoreCreditModeBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

}