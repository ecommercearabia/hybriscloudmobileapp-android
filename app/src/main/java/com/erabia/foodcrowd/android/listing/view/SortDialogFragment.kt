package com.erabia.foodcrowd.android.listing.view

import android.os.Bundle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.adapter.CategoryAdapter
import com.erabia.foodcrowd.android.common.model.Sort
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import kotlinx.android.synthetic.main.fragment_sort_dialog_list_dialog.view.*
import kotlinx.android.synthetic.main.fragment_sort_dialog_list_dialog_item.view.*

class SortDialogFragment(var list: List<Sort>) : BottomSheetDialogFragment() {

    lateinit var layoutView: View
    var onClick: IOnClick? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        layoutView = inflater.inflate(R.layout.fragment_sort_dialog_list_dialog, container, false)
        var itemAdapter = ItemAdapter()
        layoutView.list.adapter = itemAdapter
        itemAdapter.sortList = list
        layoutView.button_close.setOnClickListener(View.OnClickListener {
            this.dismiss()
        })
        return layoutView
    }

    private inner class ViewHolder internal constructor(
        inflater: LayoutInflater,
        parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        inflater.inflate(
            R.layout.fragment_sort_dialog_list_dialog_item,
            parent,
            false
        )
    )

    private inner class ItemAdapter : RecyclerView.Adapter<ViewHolder>() {
        var sortList: List<Sort>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(LayoutInflater.from(parent.context), parent)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.itemView.text_view_sort_name.text = sortList?.get(position)?.name
            holder.itemView.text_view_sort_name.setOnClickListener {
                holder.itemView.text_view_sort_name.setBackgroundColor(resources.getColor(R.color.divider_color))
                onClick?.onClick(sortList?.get(position)?.code ?: "")
                this@SortDialogFragment.dismiss()
                notifyDataSetChanged()
            }
        }

        override fun getItemCount(): Int {
            return sortList?.size ?: 0
        }
    }

    interface IOnClick {
        fun onClick(code: String)
    }
}