package com.erabia.foodcrowd.android.password.remote.service

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ChangePasswordService {

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/password")
    fun changePassword(
        @Path("userId") userId: String,
        @Query("new") newPassword: String,
        @Query("old") oldPassword: String
    ): Observable<Response<Void>>

}