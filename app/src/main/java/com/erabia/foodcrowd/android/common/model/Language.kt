package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Language(
    @SerializedName("active")
    val active: Boolean = false,
    @SerializedName("isocode")
    val isocode: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("nativeName")
    val nativeName: String = ""
)