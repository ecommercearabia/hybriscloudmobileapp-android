package com.erabia.foodcrowd.android.order.di

import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.view.MyAllOrderActivity
import dagger.Component

@Component(modules = [MyOrderModule::class])
interface IMyOrderComponent {

    fun inject (myOrderActivity: MyAllOrderActivity)
    fun getMyOrderRepo(): MyOrderRepository
}
