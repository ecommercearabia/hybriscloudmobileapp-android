package com.erabia.foodcrowd.android.common

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.navigation.NavDeepLinkBuilder
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.splash.SplashActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.Exception

class MyFirebaseMessagingService : FirebaseMessagingService() {
    companion object {
        const val TAG = "MyFirebaseMessagingService"
    }

    // [START receive_message]
    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("TAG ", "From: ${remoteMessage.from}")
        var notificationType = ""
        var productId = ""
        var title = "SmartBuy"
        var body = ""
        Log.d(TAG, "onMessageReceived: getFrom" + remoteMessage.from)
        Log.d(TAG, "onMessageReceived: getNotification" + remoteMessage.notification)
        Log.d(TAG, "onMessageReceived: getData" + remoteMessage.data)
        Log.d(TAG, "onMessageReceived: getMessageType" + remoteMessage.messageType)
        Log.d(TAG, "onMessageReceived: getTo" + remoteMessage.to)
        Log.d(TAG, "onMessageReceived: getCollapseKey" + remoteMessage.collapseKey)

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            try {

                Log.d("TAG", "Message data payload: ${remoteMessage.data}")
                val dataParams = remoteMessage.data
                val notification = remoteMessage.notification
//                val jsonObject = JSONObject(params)
//                val dataObject = JSONObject(jsonObject.get("data") as String)
//                val notificationObject = JSONObject(jsonObject.get("notification") as String)

                notificationType = dataParams.get("type").toString()
                productId = dataParams.get("id").toString()
                title = notification?.title.toString()
                body = notification?.body.toString()
            } catch (e: Exception) {
                Log.d(TAG, "onMessageReceived: ${e.message}")
            }

            sendNotification(body, title, notificationType, productId)
//            remoteMessage.data.get("body")?.let { body ->
//                remoteMessage.data.get("title")
//                    ?.let { title -> sendNotification(body, title, notificationType, productId) }
//            }

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob()
            } else {
                // Handle message within 10 seconds
                handleNow()
            }
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    /**
     * Called if the FCM registration token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the
     * FCM registration token is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String) {
        Log.d("Refreshed token ", "Refreshed token: $token")

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // FCM registration token to your app server.
        sendRegistrationToServer(token)
    }
    // [END on_new_token]


    private fun scheduleJob() {
        // [START dispatch_job]
        val work = OneTimeWorkRequest.Builder(MyWorker::class.java).build()
        WorkManager.getInstance().beginWith(work).enqueue()
        // [END dispatch_job]
    }

    private fun handleNow() {
        Log.d("TAG ", "Short lived task is done.")
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM registration token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private fun sendRegistrationToServer(token: String?) {
        //  Implement this method to send token to your app server.
        Log.d("sendRegistration ", "sendRegistrationTokenToServer($token)")
    }

    private fun sendNotification(
        messageBody: String,
        messageTitle: String,
        notificationType: String = "",
        id: String = ""
    ) {
        val bundle = Bundle()
        var destination = R.id.details
        when (notificationType) {
            "product" -> {
                destination = R.id.details
                bundle.putString(Constants.INTENT_KEY.PRODUCT_CODE, id)
            }
//            "brand" -> {
//                destination = R.id.listing
//                bundle.putString(Constants.API_KEY.CATEGORY_ID, id)
//            }
            "listing", "category" -> {
                destination = R.id.details
                bundle.putString(Constants.API_KEY.CATEGORY_ID, id)
            }
        }

        val pendingIntent = NavDeepLinkBuilder(applicationContext)
            .setComponentName(MainActivity::class.java)
            .setGraph(R.navigation.home)
            .setDestination(destination)
            .setArguments(bundle)
            .createPendingIntent()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val intent = Intent(this, SplashScreenActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            val pendingIntent = PendingIntent.getActivity(
//                this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT
//            )
//            val bundle = Bundle()
//            bundle.putString(Constants.INTENT.PRODUCT_ID, productId)
//
//            val pendingIntent = NavDeepLinkBuilder(this)
//                .setGraph(R.navigation.home)
//                .setDestination(R.id.action_navigate_to_details)
//                .setArguments(bundle)
//                .createPendingIntent()
            val channelId = "app_channel"
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            // Since android Oreo notification channel is needed.

            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(1251, notificationBuilder.build())

        } else {
//            val intent = Intent(this, SplashScreenActivity::class.java)
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//            var pendingIntent: PendingIntent? = null
//
//            pendingIntent =
//                PendingIntent.getActivity(this, 1251, intent, PendingIntent.FLAG_ONE_SHOT)

            val defaultSoundUri: Uri =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder =
                NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(messageTitle)
                    .setContentText(messageBody)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(1251, notificationBuilder.build())

        }

    }


}