package com.erabia.foodcrowd.android.product.remote.service


import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.product.model.AddReviewPostData
import com.erabia.foodcrowd.android.product.model.AddReviewResponse
import com.erabia.foodcrowd.android.product.model.ReviewsResponse
import com.erabia.foodcrowd.android.signup.model.RegisterPostData
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ProductService {

    @GET("rest/v2/foodcrowd-ae/products/{productCode}")
    fun getProductDetails(
        @Path("productCode") productCode: String,
        @Query("fields") fields: String
    ): Observable<Response<Product>>

      @GET("rest/v2/foodcrowd-ae/products/{productCode}/reviews")
    fun getReviewDetails(
        @Path("productCode") productCode: String,
        @Query("fields") fields: String,
        @Query("maxCount") maxCount: Int
    ): Observable<Response<ReviewsResponse>>


    @POST("rest/v2/foodcrowd-ae/products/{productCode}/reviews")
    fun addReview(
        @Path("productCode") productCode: String,
        @Query("fields") fields:String,
        @Body body: AddReviewPostData
    ): Observable<Response<AddReviewResponse>>

}