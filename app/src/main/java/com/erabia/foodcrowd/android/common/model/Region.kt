package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Region(
    @SerializedName("countryIso")
    var countryIso: String = "",
    @SerializedName("isocode")
    var isocode: String = "",
    @SerializedName("isocodeShort")
    var isocodeShort: String = "",
    @SerializedName("name")
    var name: String = ""
)