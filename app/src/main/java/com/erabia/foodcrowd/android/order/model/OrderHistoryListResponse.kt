package com.erabia.foodcrowd.android.order.model


import com.google.gson.annotations.SerializedName

data class OrderHistoryListResponse(
    @SerializedName("orders")
    var orders: List<Order?> = listOf(),
    @SerializedName("pagination")
    var pagination: Pagination? = Pagination(),
    @SerializedName("sorts")
    var sorts: List<Sort?>? = listOf()
) {
    data class Order(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("guid")
        var guid: String? = "",
        @SerializedName("placed")
        var placed: String? = "",
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("statusDisplay")
        var statusDisplay: String? = "",
        @SerializedName("total")
        var total: Total? = Total()
    ) {
        data class Total(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        )
    }

    data class Pagination(
        @SerializedName("currentPage")
        var currentPage: Int? = 0,
        @SerializedName("pageSize")
        var pageSize: Int? = 0,
        @SerializedName("sort")
        var sort: String? = "",
        @SerializedName("totalPages")
        var totalPages: Int? = 0,
        @SerializedName("totalResults")
        var totalResults: Int? = 0
    )

    data class Sort(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("selected")
        var selected: Boolean? = false
    )
}