package com.erabia.foodcrowd.android.menu.viewmodel

import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.menu.model.PreparingMenuModel

class MenuItemViewModel(
    var preparingMenuModel: PreparingMenuModel,
    var sharedViewModel: SharedViewModel
) {

    fun onItemClick() {
        if (preparingMenuModel.navAction == null && preparingMenuModel.getActivity() == null
            && preparingMenuModel.custom.isNullOrEmpty() && !preparingMenuModel.isConsent
        ) {
            onLogoutClick()
        } else {
            if (preparingMenuModel.navAction == null && preparingMenuModel.custom.isNullOrEmpty() &&
                preparingMenuModel.getActivity() != null
            ) {
                sharedViewModel.activitySelectedItem.postValue(preparingMenuModel)
            }
            if (preparingMenuModel.getActivity() == null && preparingMenuModel.custom.isNullOrEmpty() &&
                preparingMenuModel.navAction != null
            ) {
                sharedViewModel.selectedItem.postValue(preparingMenuModel)
            }
            if (preparingMenuModel.navAction == null && preparingMenuModel.getActivity() == null
                && preparingMenuModel.custom?.isNotEmpty() ?: false
            ) {
                sharedViewModel.customSelectedItem.postValue(preparingMenuModel)
            }
        }
    }

    fun onLogoutClick() {
//        SharedPreference.getInstance().setLOGIN_FLAG(false)
        SharedPreference.getInstance().setLOGIN_TOKEN("")
        SharedPreference.getInstance().setLOGIN_EMAIL("")
        SharedPreference.getInstance().setRefreshToken("")
        SharedPreference.getInstance().setUser_TOKEN("")
        SharedPreference.getInstance().setApp_TOKEN("")
        SharedPreference.getInstance().setRememberMeLoginFlag(false)
        SharedPreference.getInstance().setIsLoginFirstTime(true)
//        SharedPreference.getInstance().setLOGIN_User("")
        SharedPreference.getInstance().clearSP()

        sharedViewModel.logoutItem.value=true
    }

}