package com.erabia.foodcrowd.android.checkout.model


import com.google.gson.annotations.SerializedName

data class Start(
    @SerializedName("hour")
    val hour: Int = 0,
    @SerializedName("minute")
    val minute: Int = 0,
    @SerializedName("nano")
    val nano: Int = 0,
    @SerializedName("second")
    val second: Int = 0
)