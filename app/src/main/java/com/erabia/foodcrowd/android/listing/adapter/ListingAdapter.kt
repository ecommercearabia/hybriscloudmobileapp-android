package com.erabia.foodcrowd.android.listing.adapter

import android.os.Build
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Discount
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.DiscountPriceManager
import com.erabia.foodcrowd.android.common.util.ImageLoader
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ListingRowBinding
import com.erabia.foodcrowd.android.databinding.RowProgressBinding
import com.erabia.foodcrowd.android.home.adapter.HomeListingAdapter
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import java.lang.Exception

class ListingAdapter(val listingViewModel: ListingViewModel?) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        const val TAG = "ListingAdapter"
    }

    lateinit var itemBinding: ViewDataBinding
    val VIEW_TYPE_LOADING = 0
    val VIEW_TYPE_NORMAL = 1

    var totalResult = 0
    var onListingItemClick: IListingItemClick? = null
    private var productList: MutableList<Product> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var bottomGroupLayoutVisibility = View.VISIBLE
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    var spanCount: Int? = 2
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            VIEW_TYPE_NORMAL -> {
                itemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.listing_row,
                    parent,
                    false
                )

                return NormalViewHolder(itemBinding as ListingRowBinding)
            }
            VIEW_TYPE_LOADING -> {
                itemBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.row_progress,
                    parent,
                    false
                )
                return ProgressbarViewHolder(itemBinding as RowProgressBinding)
            }
            else -> throw IllegalArgumentException("No Type")

        }

    }

    override fun getItemViewType(position: Int): Int {
        Log.d("pos_vt", position.toString())
        Log.d(
            TAG,
            "position : $position ---- listSize : ${productList?.size} ---- viewType : ${if (position == productList?.size) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL}"
        )
        return if (position == productList?.size) VIEW_TYPE_LOADING else VIEW_TYPE_NORMAL
    }

    override fun getItemCount(): Int {
        Log.d("pos_ic", productList?.size.toString())
        return if (productList?.size == null) 0 else productList?.size?.plus(1) ?: 0

    }

    override fun getItemId(position: Int): Long {
        Log.d("pos_ii", position.toString())
        Log.d(
            TAG, "itemId : ${
                if (position > productList?.size ?: 0) productList?.get(position)?.code.hashCode()
                    .toLong() else 0
            }"
        )
//        return if (position > productList?.size ?: 0) productList?.get(position)?.code.hashCode()
//            .toLong() else 0
        return position.toLong()

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d(TAG, "itemCount : $itemCount")
        try {
            Log.d(TAG, "onBindViewHolder : $position product : ${productList?.get(position)}")

        } catch (e: Exception) {

        }
        when (getItemViewType(position)) {
            VIEW_TYPE_NORMAL -> {
                val product = productList?.get(position)
                var quantity = 1
                holder as NormalViewHolder
                holder.itemBinding.textViewQuantity.text = quantity.toString()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    holder.itemBinding.textViewProductName.text = Html.fromHtml(
                        product?.name ?: "",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                } else {
                    holder.itemBinding.textViewProductName.text =
                        Html.fromHtml(product?.name ?: "")
                }

                val discountPriceManager = DiscountPriceManager(product)
                discountPriceManager.setupPriceView(
                    holder.itemBinding.textViewPrice,
                    holder.itemBinding.textViewOldPrice,
                    holder.itemBinding.textViewDiscount,
                    holder.itemBinding.textViewDiscountSmall,
                    spanCount
                )

                try {
                    holder.itemBinding.url =
                        productList.get(position).images?.firstOrNull() { it.format == "zoom" }?.url

                } catch (e: Exception) {
                    Log.d(HomeListingAdapter.TAG, e.message ?: "")
                }

                try {
                    holder.itemBinding.textViewNote.text =
                        " / ${product?.unitOfMeasure ?: "Unit"}"
                } catch (e: Exception) {
                    Log.d(HomeListingAdapter.TAG, e.message ?: "")
                }
                try {
                    ImageLoader().setImageFlag(
                        holder.itemBinding.imageViewFlag,
                        product?.countryOfOriginIsocode ?: "UAE"
                    )
                    holder.itemBinding.textViewOrigin.text = product?.countryOfOrigin ?: "Origin"
                } catch (e: Exception) {
                    Log.d(HomeListingAdapter.TAG, e.message ?: "")
                }

                if ((productList?.get(position)?.stock?.stockLevel ?: 0) > 0) {
                    holder.itemBinding.mOutOfStockTextView.visibility =
                        View.GONE
                    holder.itemBinding.imageViewAddToCart.visibility =
                        View.VISIBLE
                    holder.itemBinding.clQuantity.visibility = View.VISIBLE
                } else {

                    holder.itemBinding.mOutOfStockTextView.visibility =
                        View.VISIBLE
                    holder.itemBinding.imageViewAddToCart.visibility =
                        View.GONE
                    holder.itemBinding.clQuantity.visibility = View.GONE
                }

                // Classification Tags Listing / category
                if (productList?.get(position)?.chilled == true) {
                    holder.itemBinding.mChilledImageView.visibility = View.VISIBLE
                } else {
                    holder.itemBinding.mChilledImageView.visibility = View.GONE
                }

                if (productList?.get(position)?.dry == true) {
                    holder.itemBinding.mDryImageView.visibility = View.VISIBLE
                } else {
                    holder.itemBinding.mDryImageView.visibility = View.GONE
                }

                if (productList?.get(position)?.frozen == true) {
                    holder.itemBinding.mFrozenImageView.visibility = View.VISIBLE
                } else {
                    holder.itemBinding.mFrozenImageView.visibility = View.GONE
                }

                if (productList?.get(position)?.express == true) {
                    holder.itemBinding.expressDelivery.visibility = View.VISIBLE
                } else {
                    holder.itemBinding.expressDelivery.visibility = View.INVISIBLE
                }

                if (productList?.get(position)?.inWishlist == true) {
                    holder.itemBinding.imageViewWishlistSelectedCat.visibility = View.VISIBLE
                    holder.itemBinding.imageViewWishlist.visibility = View.VISIBLE
                } else {
                    holder.itemBinding.imageViewWishlistSelectedCat.visibility = View.GONE
                    holder.itemBinding.imageViewWishlist.visibility = View.VISIBLE
                }

                holder.itemBinding.product = productList?.get(position)

                holder.itemBinding.imageViewAdd.setOnClickListener {
                    quantity += 1
                    holder.itemBinding.textViewQuantity.text =
                        quantity.toString()
                }
                holder.itemBinding.imageViewMinus.setOnClickListener {
                    if (quantity > 1) {
                        quantity -= 1
                        holder.itemBinding.textViewQuantity.text =
                            quantity.toString()
                    } else {
                        quantity = 1
                        holder.itemBinding.textViewQuantity.text =
                            quantity.toString()
                    }


                }


                (holder as NormalViewHolder).itemBinding.imageViewId.setOnClickListener(View.OnClickListener {
                    onListingItemClick?.onListingItemClick(
                        "onAllList",
                        productList?.get(position)?.code ?: ""
                    )
                })

                // change icon from category / search by clicking
                (holder as NormalViewHolder).itemBinding.imageViewWishlist.setOnClickListener(View.OnClickListener {
                    var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""
                    if (userLogin.isNotEmpty()) {
                        holder.itemBinding.imageViewWishlist.visibility = View.VISIBLE
                        holder.itemBinding.imageViewWishlistSelectedCat.visibility = View.VISIBLE
                    }
                    onListingItemClick?.onListingItemClick(
                        "onWishList",
                        productList?.get(position)?.code ?: ""
                    )
                })

                (holder as NormalViewHolder).itemBinding.imageViewWishlistSelectedCat.setOnClickListener(
                    View.OnClickListener {
                        holder.itemBinding.imageViewWishlist.visibility = View.VISIBLE
                        holder.itemBinding.imageViewWishlistSelectedCat.visibility = View.GONE
                        onListingItemClick?.onListingItemClick(
                            "onRemoveWishList",
                            productList?.get(position)?.code ?: ""
                        )
                    })

                (holder as NormalViewHolder).itemBinding.imageViewAddToCart.setOnClickListener(View.OnClickListener {
                    productList?.get(position)
                        ?.let { it1 -> listingViewModel?.onAddToCartClick(it1, quantity) }
                })

                holder.itemBinding.bottomLayout.visibility = bottomGroupLayoutVisibility
                holder.itemBinding.imageViewSmile.visibility = bottomGroupLayoutVisibility

//                if (spanCount == 4) {
//                    holder.itemBinding.textViewDiscountSmall.visibility = bottomGroupLayoutVisibility
//                } else {
//                    holder.itemBinding.textViewDiscount.visibility = bottomGroupLayoutVisibility
//                }
//                holder.itemBinding.textViewDiscount.visibility = bottomGroupLayoutVisibility
//                if (bottomGroupLayoutVisibility == View.VISIBLE)
//                    holder.itemBinding.textViewDiscountSmall.visibility = View.GONE
//                else
//                    holder.itemBinding.textViewDiscountSmall.visibility = View.VISIBLE

            }
//            VIEW_TYPE_LOADING -> {
//                if (productList?.size == totalResult)
//                    (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
//                        View.GONE
//                else
//                    (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
//                        View.VISIBLE
//
//            }


            VIEW_TYPE_LOADING -> {
                when {
                    itemCount == 1 -> {
                        (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
                            View.GONE

                    }
                    productList?.size == totalResult -> {
                        (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
                            View.GONE
                    }
                    itemCount - 1 == totalResult -> {
                        (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
                            View.GONE
                    }
                    else -> {
                        (holder as ProgressbarViewHolder).itemBinding.progressbar.visibility =
                            View.VISIBLE

                    }
                }
            }
        }

    }

    fun reset(scrollState: Int = RecyclerView.SCROLL_STATE_IDLE) {
        Log.d(TAG, "scrollState -> $scrollState (outside)")
        productList.clear()
        notifyDataSetChanged()

        //fix crash when recyclerView still scrolling and setData
        if (scrollState == RecyclerView.SCROLL_STATE_DRAGGING || scrollState == RecyclerView.SCROLL_STATE_SETTLING) {
            notifyDataSetChanged()
            Log.d(TAG, "scrollState -> $scrollState")
        }
    }


    fun refresh(productList: MutableList<Product>) {
        this.productList.addAll(productList)
        notifyDataSetChanged()
    }

    class NormalViewHolder(val itemBinding: ListingRowBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    class ProgressbarViewHolder(val itemBinding: RowProgressBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    interface IListingItemClick {
        fun onListingItemClick(key: String, productCode: String)
    }
}
