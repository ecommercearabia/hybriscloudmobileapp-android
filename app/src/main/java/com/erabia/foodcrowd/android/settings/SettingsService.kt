package com.erabia.foodcrowd.android.settings

import com.erabia.foodcrowd.android.common.model.TokenModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface SettingsService {

    @POST("rest/v2/foodcrowd-ae/users/{userId}/signature")
    fun setSignature(@Path("userId") userId: String,@Query("signatureId") signatureId:String): Observable<Response<Void>>



}