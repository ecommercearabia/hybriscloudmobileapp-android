package com.erabia.foodcrowd.android.checkout.ccavenue

import android.os.Parcel
import android.os.Parcelable

data class CCAvenueShippingAddress(
    var name: String? = null,
    var address: String? = null,
    var city: String? = null,
    var state: String? = null,
    var country: String? = null,
    var telephone: String? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(country)
        parcel.writeString(telephone)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CCAvenueShippingAddress> {
        override fun createFromParcel(parcel: Parcel): CCAvenueShippingAddress {
            return CCAvenueShippingAddress(parcel)
        }

        override fun newArray(size: Int): Array<CCAvenueShippingAddress?> {
            return arrayOfNulls(size)
        }
    }
}