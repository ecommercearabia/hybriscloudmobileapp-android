package com.erabia.foodcrowd.android.menu.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.SubMenuRowBinding
import com.erabia.foodcrowd.android.menu.model.PreparingMenuModel
import com.erabia.foodcrowd.android.menu.viewmodel.ConsentViewModel
import com.erabia.foodcrowd.android.menu.viewmodel.MenuItemViewModel
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.sub_menu_row.view.*


class SubMenuRecyclerViewAdapter(
    var sharedViewModel: SharedViewModel , var consentViewModel: ConsentViewModel


) :
    RecyclerView.Adapter<SubMenuHeaderViewHolder>() {
    private var context: Context? = null
    lateinit var list: List<PreparingMenuModel>
    lateinit var binding: SubMenuRowBinding



    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: SubMenuHeaderViewHolder, position: Int) {



        binding.mMenuItemViewModel = MenuItemViewModel(list[position], sharedViewModel)
        holder.itemView.text_view_menu.text = list[position].getText()
        if (list[position].getImage() != 0) {
            holder.itemView.image_view_menu.setImageResource(list[position].getImage())
            holder.itemView.image_view_menu.visibility = View.VISIBLE
        }
        if (list?.get(position).getText() == context?.getString(R.string.logout)) {
            context?.resources?.getColor(R.color.holo_red_dark)?.let {
                holder.itemView.text_view_menu.setTextColor(
                    it
                )
            }
        }
//        holder.itemView.setOnClickListener(View.OnClickListener {
//            holder.itemView.context.startActivity(
//                Intent(holder.itemView.context, list[position].getActivity()!!::class.java)
//            )
//        })


        if ( list[position].getText() == context?.getString(R.string.consent_management) ) {
            holder.itemView.consentRecyclerView.visibility = View.VISIBLE

            consentViewModel.consentList.observe(context as LifecycleOwner, Observer {

                var adapter: ConsentRecyclerViewAdapter =
                ConsentRecyclerViewAdapter(
                    sharedViewModel , consentViewModel
                )
            holder.itemView.consentRecyclerView.adapter = adapter
                adapter.setMenu(it.ConsentTemplate!!)
            })

        }
    }

    fun setMenu(list: List<PreparingMenuModel>) {
        this.list = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubMenuHeaderViewHolder {

        context = parent.context
        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.sub_menu_row,
                parent,
                false
            )
        return SubMenuHeaderViewHolder(
            binding.root
        )

    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class SubMenuHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {


}
