package com.erabia.foodcrowd.android.order.view

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.ActivityOrderBinding
import com.erabia.foodcrowd.android.order.adapter.TabOrderAdapter
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.google.android.material.tabs.TabLayout
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.activity_order.toolbar

class OrderActivity : AppCompatActivity() {
    lateinit var mViewModel: MyOrderViewModel
    var compositeDisposable = CompositeDisposable()
    lateinit var binding: ActivityOrderBinding
    var mOrderDetailsResponse=OrderDetailsResponse()
    lateinit var mOrderCode:String
    lateinit var date_placed:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order)


        val factory = MyOrderViewModel.Factory(
            MyOrderRepository(
                RequestManager.getClient().create(MyOrderService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(MyOrderViewModel::class.java)

        binding.lifecycleOwner = this
        binding.mMyOrderViewModel = mViewModel

        setSupportActionBar(toolbar)
        toolbar.title = ""
        toolbar.setNavigationOnClickListener {
            finish()
        }

        mOrderCode = intent.getStringExtra("code") ?: ""
        date_placed = intent.getStringExtra("date_placed") ?: ""
        mViewModel.getItemDetails(mOrderCode)

        itemDetailsObserver()
        observeProgressBar()


    }
    private fun observeProgressBar() {
        mViewModel.progressBarVisibility.observe(this, Observer {
            if (it == View.VISIBLE) {
                binding.progressBar.visibility = it
                binding.progressBar.setFreezAndVisiable(this)
            } else {
                binding.progressBar.visibility = it
                binding.progressBar.hideFreezAndVisiableProgress(this)
            }
        })
    }

    fun itemDetailsObserver() {


        mViewModel.mItemDetails.observe(this, Observer {

            mOrderDetailsResponse=it
            initTab()
            Log.e("mohamed","mohamed")
        })
    }

    fun initTab() {
        tabLayout?.newTab()?.setText("Item")?.let { tabLayout.addTab(it) }
        tabLayout?.newTab()?.setText("Details")?.let { tabLayout.addTab(it) }
        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL

        val adapter =
            TabOrderAdapter(
                mOrderDetailsResponse,
                supportFragmentManager,
                tabLayout!!.tabCount
            )
        viewPager.adapter = adapter

        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }


}