package com.erabia.foodcrowd.android.otp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.otp.model.SendOtpResponse
import com.erabia.foodcrowd.android.otp.remote.repository.OtpRepository
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.viewmodel.SignUpViewModel

class OtpVIewModel(val mOtpRepository: OtpRepository, val mSignUpRepository: SignUpRepository, val mLoginRepository: LoginRepository) :
    ViewModel() {


    val loadingVisibility: MutableLiveData<Int> = mOtpRepository.loadingVisibility
    var mSendOtpLiveData: LiveData<SendOtpResponse> = mOtpRepository.mSendOtpLiveData
    var mUpdateProfileLiveData: LiveData<Void> =  mOtpRepository.mUpdateProfileLiveData
    var mVerifyOtpLiveData: LiveData<SendOtpResponse> = mOtpRepository.mVerifyOtpLiveData
    val mErrorOtpEvent: SingleLiveEvent<String> = mOtpRepository.error()
    val mErrorSignUpEvent: SingleLiveEvent<String> = mSignUpRepository.error()
    val mErrorLoginEvent: SingleLiveEvent<String> = mLoginRepository.error()
    var mRegisterLiveData: LiveData<RegisterResponse> = mSignUpRepository.mRegisterLiveData
    var mLoginLiveData: LiveData<TokenModel>  = mLoginRepository.mLoginLiveData

    fun sendOtp(IsoCode: String, mMobileNumber: String) {
        mOtpRepository.sendOtp(IsoCode, mMobileNumber)
    }

    fun verifyOtp(code: String, IsoCode: String, mMobileNumber: String) {
        mOtpRepository.verifyOtp(code, IsoCode, mMobileNumber)
    }

    fun updateProfile(p : PersonalDetailsModel) {
        mOtpRepository.updateProfile(p)
    }

    fun doRegister(
        mFirstName: String,
        mLastName: String,
        mPassword: String,
        mTitlePostion: String,
        mNationality : String,
        email: String,
        IsoCode: String,
        mMobileNumber: String,
        mReferralCode: String
    ) {
        mSignUpRepository.doRegister(
            mFirstName,
            mLastName,
            mPassword,
            mTitlePostion,
            mNationality,
            email,
            IsoCode,
            mMobileNumber,
            mReferralCode
        )
    }

    fun doLogin(email:String,password: String) {
        mLoginRepository.doLogin(email, password)
    }


    class Factory(
        val mOtpRepository: OtpRepository,
        val mSignUpRepository: SignUpRepository,
        val mLoginRepository: LoginRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return OtpVIewModel(mOtpRepository, mSignUpRepository,mLoginRepository) as T
        }
    }
}