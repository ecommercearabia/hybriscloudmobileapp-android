package com.erabia.foodcrowd.android.checkout.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.CompoundButton
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.BuildConfig
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.checkout.model.*
import com.erabia.foodcrowd.android.checkout.model.PaymentInfo
import com.erabia.foodcrowd.android.checkout.remote.repository.CheckoutRepository
import com.erabia.foodcrowd.android.checkout.util.CheckoutItemManager
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import mumbai.dev.sdkdubai.external.MerchantDetails
import mumbai.dev.sdkdubai.external.ShippingAddress
import mumbai.dev.sdkdubai.external.StandardInstructions
import mumbai.dev.sdkdubai.external.BillingAddress

class CheckoutViewModel(
    private val checkoutRepository: CheckoutRepository,
    private val cartRepository: CartRepository
) : ViewModel(),
    CheckoutItemManager.OnButtonCheckoutEnabled {
    private var redeemStoreCreditAmount: String = ""
    var selectedStoreCreditMode: StoreCreditMode? = null
    var ccAvenueStandardInstruction: StandardInstructions? = null
    var ccAvenueBillingAddress: BillingAddress? = null
    var ccAvenueShippingAddress: ShippingAddress? = null
    var ccAvenueMerchantDetails: MerchantDetails? = null
    private var enableCheckoutButton: Boolean = false
    val getPaymentModeLiveData: MutableLiveData<PaymentModes> = checkoutRepository.getPaymentMode
    val deliveryModeTypesList: MutableLiveData<DeliveryTypes> = checkoutRepository.getDeliveryModeTypesLiveData
    val setDeliveryTypes: MutableLiveData<setDeliveryTypesResponse> = checkoutRepository.setDeliveryTypes
    val getStoreCreditModeLiveData: MutableLiveData<StoreCreditModes> =
        checkoutRepository.getStoreCreditMode
    val setPaymentModeLiveData: MutableLiveData<SetPaymentModeResponse> =
        checkoutRepository.setPaymentMode
    val setStoreCreditModeLiveData: MutableLiveData<Boolean> =
        checkoutRepository.setStoreCreditMode
    val paymentInfoLiveData: MutableLiveData<PaymentInfo> = checkoutRepository.paymentInfoLiveData
    val getTimeSlotLiveData: MutableLiveData<TimeSlot> = checkoutRepository.getTimeSlotLiveData
    val setTimeSlotLiveData: MutableLiveData<Boolean> = checkoutRepository.setTimeSlotLiveData
    val storeCreditAmount: MutableLiveData<StoreCreditAmount> = checkoutRepository.storeCreditAmount
    val storeCreditVisibility: MutableLiveData<Int> = checkoutRepository.storeCreditVisibility
    val preSelectPaymentMode: MutableLiveData<PaymentMode> = checkoutRepository.preSelectPaymentMode
    val preSelectStoreCreditMode: MutableLiveData<StoreCreditMode> = checkoutRepository.preSelectStoreCreditMode
    val cart: LiveData<Cart> = checkoutRepository.cartLiveData
    val getBankOfferSuccess = checkoutRepository.getBankOfferSuccess
    val appliedVoucher = checkoutRepository.appliedVoucher
    val placeOrderLiveData: LiveData<PlaceOrderResponse> = checkoutRepository.placeOrderLiveData
    val setDeliveryModeSuccessEvent: LiveData<Boolean> =
        checkoutRepository.setDeliveryModeSuccessEvent
    val progressBarVisibility: SingleLiveEvent<Int> = checkoutRepository.progressBarVisibility()
    val error: SingleLiveEvent<String> = checkoutRepository.error()
    var isTermsChecked = checkoutRepository.isTermsChecked
        set(value) {
            field = value
            checkoutRepository.isTermsChecked = value
        }


    var selectedStoreCreditModeFromGetCart: String = ""

    val enableCheckoutButtonLiveData = checkoutRepository.enableCheckoutButtonLiveData
    val cartData = checkoutRepository.cart
    val changeDeliveryAddressAction: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val navigateToPaymentOption: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    var isBillingAddress = false
    var billingAddress: Address? = null
    val hasPaymentMethodSetToCart: MutableLiveData<Boolean> =
        checkoutRepository.hasPaymentMethodSetToCart
    val hasTimeSlotSetToCart: MutableLiveData<Boolean> =
        checkoutRepository.hasTimeSlotSetToCart
    var useDeliveryAddressSwitchButton = MutableLiveData<Boolean>(true)
    val selectBillingAddressAction: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val badgeNumber = checkoutRepository.badgeNumber
    val footerVisibilityLiveDate = checkoutRepository.footerVisibilityLiveDate
    var promoCode: String = ""
    val clearPromoCodeText = checkoutRepository.clearPromoCodeText
    val getBankOfferError = checkoutRepository.getBankOfferError
    val enableStoreCreditLimit = checkoutRepository.enableStoreCreditLimit
    val applyVoucherSuccess: SingleLiveEvent<Boolean> =
        checkoutRepository.applyVoucherSuccessLiveData
    val deleteVoucherSuccess: SingleLiveEvent<Boolean> =
        checkoutRepository.deleteVoucherSuccessLiveData

    var isTimeSlotValid: Boolean = false


    var mCartId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
        Constants.CURRENT
    } else {
        SharedPreference.getInstance().getGUID() ?: ""
    }
    var mUserId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
        Constants.CURRENT
    } else {
        Constants.ANONYMOUS
    }

    val validationSuccess: SingleLiveEvent<CartVaildationResponse> =
        cartRepository.cartVaildationLiveData

    val mErrorValidationMessage: SingleLiveEvent<String> = SingleLiveEvent()

    init {
        CheckoutItemManager.onButtonCheckoutEnabled = this
//        checkoutRepository.setDeliveryMode("current", "current", "fc-ae")
    }

    fun loadPaymentMode(userId: String, cartId: String, fields: String): LiveData<PaymentModes> {
        return checkoutRepository.loadPaymentMode(userId, cartId, fields)
    }

    fun getConfig (){
        return checkoutRepository.loadAppConfig()
    }


    fun loadDeliveryModeTypesVM(userId: String, cartId: String, fields: String): LiveData<DeliveryTypes> {
        return checkoutRepository.loadDeliveryModeTypes(userId, cartId, fields)
    }

    fun loadStoreCreditMode(
        userId: String,
        cartId: String,
        fields: String
    ): LiveData<StoreCreditModes> {
        return checkoutRepository.loadStoreCreditMode(userId, cartId, fields)
    }


    fun loadTimeSlot(userId: String, cartId: String, fields: String): LiveData<TimeSlot> {
        return checkoutRepository.loadTimeSlot(userId, cartId, fields)
    }

    fun getBankOffer() {
        cartRepository.getBankOffer()
    }

    fun setTimeSlot(
        userId: String,
        cartId: String,
        fields: String,
        timeSlotRequest: TimeSlotRequest
    ) {
        checkoutRepository.setTimeSlot(userId, cartId, fields, timeSlotRequest)
    }

    fun setPaymentMode(
        userId: String,
        cartId: String,
        paymentModeCode: String,
        fields: String
    ) {
        checkoutRepository.setPaymentMode(userId, cartId, paymentModeCode, fields)
    }

    fun setDeliveryTypes(
        userId: String,
        cartId: String,
        deliveryTypeCode: String,
        fields: String
    ) {
        checkoutRepository.setDeliveryTypes(userId, cartId, deliveryTypeCode, fields)
    }

    fun setStoreCreditMode(
        userId: String,
        cartId: String,
        storeCreditModeCode: String,
        amount: String,
        fields: String,
        lang: String
    ) {
        checkoutRepository.setStoreCreditMode(
            userId,
            cartId,
            storeCreditModeCode,
            amount,
            fields,
            lang
        )
    }

    fun loadCart(userId: String, cartId: String, fields: String): LiveData<Cart> {
        return checkoutRepository.loadCart("", "", Constants.FIELDS_FULL)
    }

    fun loadStoreCreditAmount(userId: String, fields: String): LiveData<StoreCreditAmount> {
        return checkoutRepository.loadStoreCreditAmount(userId, fields)
    }

    fun loadPaymentInfo(
        userId: String,
        cartId: String,
        fields: String
    ): MutableLiveData<PaymentInfo> {
        return checkoutRepository.loadPaymentInfo(userId, cartId, fields)
    }

    fun setDeliveryMode(userId: String, cartId: String, deliveryModeId: String) {
        checkoutRepository.setDeliveryMode(userId, cartId, deliveryModeId)
    }

    fun getAllCheckoutData(userId: String, cartId: String, deliveryModeId: String) {
        checkoutRepository.getAllCheckoutData(userId, cartId, deliveryModeId)
    }


    //    fun setPaymentDetails(
//        holder: String,
//        expiryMonth: String,
//        expiryYear: String,
//        bin: String,
//        brand: String,
//        cardNumber: String
//    ) {
//        val params = HashMap<String, String>()
//        params["userId"] = SharedPreference.getInstance().getLoginEmail() ?: ""
//        params["cartId"] = "current"
//
//        val paymentDetailsModel = PaymentDetailsRequest()
//        val cardTypeModel = CardType()
//        cardTypeModel.code = "visa"
//        paymentDetailsModel.accountHolderName = holder
//        paymentDetailsModel.cardType = cardTypeModel
//        paymentDetailsModel.expiryMonth = expiryMonth
//        paymentDetailsModel.expiryYear = expiryYear
//        paymentDetailsModel.id = bin
//        paymentDetailsModel.cardNumber = cardNumber
//        paymentDetailsModel.saved = false
//        paymentDetailsModel.defaultPayment = false
//        paymentDetailsModel.issueNumber = ""
//        paymentDetailsModel.startMonth = ""
//        paymentDetailsModel.startYear = ""
//        paymentDetailsModel.subscriptionId = ""
//
//        var billingAddressModel = Address()
//        // when user select use same delivery address
//        if (isBillingAddress) {
//            billingAddressModel = billingAddress ?: Address()
//
//        } else {
//            billingAddressModel = cart?.value?.deliveryAddress ?: Address()
//
//        }
//        billingAddressModel.town = billingAddressModel.city?.name ?: "town"
//        billingAddressModel.postalCode = "12345"
//        paymentDetailsModel.billingAddress = billingAddressModel
//
//        checkoutRepository.setPaymentDetails(
//            Constants.CURRENT,
//            Constants.CURRENT,
//            Constants.FIELDS_FULL,
//            paymentDetailsModel
//        )
//    }
//
//
//
//
    fun setPaymentDetails(
        trackingData: String
    ) {
        checkoutRepository.setPaymentDetails(
            Constants.CURRENT,
            Constants.CURRENT,
            Constants.FIELDS_FULL,
            trackingData
        )
    }


    fun checked(isChecked: Boolean) {
        isTermsChecked = isChecked
        enableCheckoutButtonLiveData.value =
            isTermsChecked && CheckoutItemManager.isCartReadyToCheckout() && isTimeSlotValid
    }

    fun onCheckoutClick() {
        if (cart.value?.cartTimeSlot?.showTimeSlot == false) {
            hasTimeSlotSetToCart.value = true
            isTimeSlotValid = true
        }

        if (CheckoutItemManager.isCartReadyToCheckout()) {
            if (isTimeSlotValid) {
                if (isTermsChecked) {
                    val paymentDetailsRequest = PaymentDetailsRequest()
                    if (cart.value?.paymentMode?.code == "card") {
                        prepareMerchantDetails()
                    } else {
                        checkoutRepository.placeOrder("", "", "")
                    }
                } else {
                    AppUtil.showToastyWarning(MyApplication.getContext(), R.string.accept_tems_and_condition_message)
                }
            } else {
                AppUtil.showToastyWarning(MyApplication.getContext(), R.string.choose_valid_delivery_time)
            }
        } else {
            AppUtil.showToastyError(MyApplication.getContext(), CheckoutItemManager.errorMessage)
        }
    }

    fun onCheckoutValidation() {
        // navigateTo.postValue(R.id.action_navigate_to_checkout)
        if (validationSuccess.value?.valid ?: false) {
            onCheckoutClick()
        } else {
            var mErrorMessage: String = ""

            var builder2 = StringBuilder()
            for (s in validationSuccess.value?.errors!!) {
                builder2.append(s + "\n")
            }
            mErrorMessage = builder2.toString()
            mErrorValidationMessage.postValue(mErrorMessage)
        }


    }

    fun onValidationClick() {
        if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
            cartRepository.doValidation(SharedPreference.getInstance().getUserToken()
                ?.let { "current" }
                ?: run { "anonymous" },
                SharedPreference.getInstance().getUserToken()?.let { "current" }
                    ?: run { SharedPreference.getInstance().getGUID() ?: "" })
        } else {
            AppUtil.showToastyError(MyApplication.getContext(), "Please login")
        }


    }

    private fun prepareMerchantDetails() {
        val paymentInfo = paymentInfoLiveData.value
        ccAvenueMerchantDetails = MerchantDetails()


        if (BuildConfig.FLAVOR.equals("stg")) {
//            ccAvenueMerchantDetails?.env_type = "LIVE"
            ccAvenueMerchantDetails?.redirect_url =
                "https://ccavenue.erabia.io/ccavResponseHandler.php"
            ccAvenueMerchantDetails?.cancel_url =
                "https://ccavenue.erabia.io/ccavResponseHandler.php"
            ccAvenueMerchantDetails?.rsa_url = "https://ccavenue.erabia.io/GetRSA.php"

        } else if (BuildConfig.FLAVOR.equals("prod")) {

//            ccAvenueMerchantDetails?.env_type = "LIVE"
            ccAvenueMerchantDetails?.redirect_url =
                "https://ccavenue.erabia.io/ccavResponseHandlerProd.php"
            ccAvenueMerchantDetails?.cancel_url =
                "https://ccavenue.erabia.io/ccavResponseHandlerProd.php"
            ccAvenueMerchantDetails?.rsa_url = "https://ccavenue.erabia.io/GetRSAProd.php"
        }

        ccAvenueMerchantDetails?.access_code = paymentInfo?.accessCode
        ccAvenueMerchantDetails?.merchant_id = paymentInfo?.merchantId


        ccAvenueMerchantDetails?.currency = paymentInfo?.currency
        ccAvenueMerchantDetails?.order_id = paymentInfo?.orderId
        ccAvenueMerchantDetails?.amount = paymentInfo?.amount

        ccAvenueMerchantDetails?.customer_id = paymentInfo?.customerIdentifier
        ccAvenueMerchantDetails?.add1 = paymentInfo?.deliveryAddress
        ccAvenueMerchantDetails?.promo_code = paymentInfo?.promoCode
        ccAvenueMerchantDetails?.isShow_addr = true
        ccAvenueMerchantDetails?.isCCAvenue_promo = paymentInfo?.promoCode.isNullOrEmpty().not()




        ccAvenueShippingAddress = ShippingAddress()
        ccAvenueShippingAddress?.name = paymentInfo?.deliveryName
        ccAvenueShippingAddress?.address = paymentInfo?.deliveryAddress
        ccAvenueShippingAddress?.city = paymentInfo?.deliveryCity
        ccAvenueShippingAddress?.state = paymentInfo?.deliveryState
        ccAvenueShippingAddress?.country = paymentInfo?.deliveryCountry
        ccAvenueShippingAddress?.telephone = paymentInfo?.deliveryTel

        ccAvenueStandardInstruction = StandardInstructions()
        ccAvenueStandardInstruction?.si_type = ""


        ccAvenueBillingAddress = BillingAddress()

        ccAvenueBillingAddress?.name = paymentInfo?.deliveryName
        ccAvenueBillingAddress?.address = paymentInfo?.deliveryAddress
        ccAvenueBillingAddress?.city = paymentInfo?.deliveryCity
        ccAvenueBillingAddress?.state = paymentInfo?.deliveryState
        ccAvenueBillingAddress?.country = paymentInfo?.deliveryCountry
        ccAvenueBillingAddress?.telephone = paymentInfo?.deliveryTel
        ccAvenueBillingAddress?.email = paymentInfo?.customerIdentifier

        navigateToPaymentOption.postValue(true)

    }

    override fun onEnabled() {
        this.enableCheckoutButton = true
        if (isTermsChecked) {
            enableCheckoutButtonLiveData.postValue(true)
        }
        Log.d("CheckoutViewModel", "Enabled")
    }

    fun onChangeShippingAddressClick() {
        changeDeliveryAddressAction.postValue(R.id.action_navigate_to_address)
    }

    fun deleteVoucher(voucherId: String) {
        SharedPreference.getInstance().setVoucherID("");
        checkoutRepository.deleteVoucher(mUserId, mCartId, voucherId)
    }

    fun applyVoucher() {
        if (promoCode.isEmpty()) {
            error.postValue("Promo code required")
        } else {
            SharedPreference.getInstance().setVoucherID(promoCode)
            checkoutRepository.applyVoucher(mUserId, mCartId, promoCode.toUpperCase())
        }
    }

    fun checkedSwitch(): CompoundButton.OnCheckedChangeListener {
        return object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                if (!isChecked) {
                    selectBillingAddressAction.postValue(R.id.action_navigate_to_address)
                    isBillingAddress = true
                    useDeliveryAddressSwitchButton.postValue(false)
                } else {
                    useDeliveryAddressSwitchButton.postValue(true)
                    isBillingAddress = false
                    //   addressViewModel.billingAddress.postValue(null)
                    //   addressViewModel.popEventInCheckout.postValue(false)

                }

            }

        }
    }

    fun amountText(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isEmpty()) redeemStoreCreditAmount =
                    "0.0" else redeemStoreCreditAmount =
                    s.toString()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }

    fun onStoreCreditButtonClick() {
//        selectedStoreCreditMode
        LocaleHelper.getLanguage(MyApplication.getContext()!!)?.let {
            setStoreCreditMode(
                "", "",
                selectedStoreCreditMode?.storeCreditModeType?.code ?: "",
                redeemStoreCreditAmount,
                Constants.FIELDS_FULL,
                it

            )
        }
    }

    override fun onCleared() {
        super.onCleared()
        checkoutRepository.dispose()
    }

    class Factory(
        private val checkoutRepository: CheckoutRepository,
        private val cartRepository: CartRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CheckoutViewModel(checkoutRepository, cartRepository) as T
        }
    }
}