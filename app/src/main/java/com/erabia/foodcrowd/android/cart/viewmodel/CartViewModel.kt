package com.erabia.foodcrowd.android.cart.viewmodel

import android.app.Activity
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.*
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import io.reactivex.disposables.CompositeDisposable


class CartViewModel(
    private val cartRepository: CartRepository
) : ViewModel() {
    companion object {
        const val TAG = "CartViewModel"
    }
    var shipmentCode = ""
    val expressDeliveryLiveEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val deliveryCostVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val cashOnDeliveryVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val showLoginDialog: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val continueShoppingClickEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val validationLoadShipmentType: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val shipmentTypesRadioGroup: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val navigateTo: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val navigateToDetails: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val receivedPromotionVisibility: SingleLiveEvent<Int> =
        cartRepository.receivedPromotionVisibility
    val potentialPromotionVisibility: SingleLiveEvent<Int> =
        cartRepository.potentialPromotionVisibility
    val receivedPromotionMessage: MutableLiveData<String> = cartRepository.receivedPromotionMessage
    val potentialPromotionMessageList: MutableLiveData<List<PotentialOrderPromotion>> =
        cartRepository.potentialPromotionMessageList
    val shipmentTypesList: SingleLiveEvent<ShipmentTypes> = cartRepository.shipmentTypesLiveData
    val updateShipmentTypesList: SingleLiveEvent<UpdateShipmentTypes> = cartRepository.updateShipmentTypesLiveData
    val footerVisibility: SingleLiveEvent<Int>? = cartRepository.footerVisibility
    val promoCodeVisibility: SingleLiveEvent<Int>? = cartRepository.promoCodeVisibility
    var badgeNumber: SingleLiveEvent<Int> = cartRepository.badgeNumber
    //    var cart: Cart? = null
    var activity: SingleLiveEvent<Activity> = SingleLiveEvent()
    var loginActivity: SingleLiveEvent<Activity> = SingleLiveEvent()
    var emptyCartVisibility: SingleLiveEvent<Int> = cartRepository.emptyCartVisibility
    val checkoutButtonVisibility: SingleLiveEvent<Int> = cartRepository.checkoutButtonVisibility
    val progressBar: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    private val compositeDisposable = CompositeDisposable()
    val cart: LiveData<Cart> = cartRepository.cartLiveData
    val hideRefreshLoaderEvent: SingleLiveEvent<Boolean> = cartRepository.hideRefreshLayout()
    val progressBarVisibility: SingleLiveEvent<Int> = cartRepository.progressBarVisibility()
    val loadingVisibility: SingleLiveEvent<Int> = cartRepository.loadingVisibility
    val error: SingleLiveEvent<String> = cartRepository.error()
    val deleteEntrySuccess: SingleLiveEvent<Boolean> = cartRepository.deleteEntrySuccessLiveData
    val applyVoucherSuccess: SingleLiveEvent<Boolean> = cartRepository.applyVoucherSuccessLiveData
    val deleteVoucherSuccess: SingleLiveEvent<Boolean> = cartRepository.deleteVoucherSuccessLiveData
    val validationSuccess: SingleLiveEvent<CartVaildationResponse> = cartRepository.cartVaildationLiveData
    val mErrorValidationMessage: SingleLiveEvent<String> = SingleLiveEvent()
    val openMapObserver: SingleLiveEvent<String> = SingleLiveEvent()
    val appliedVoucher = cartRepository.appliedVoucher
    val getBankOfferSuccess = cartRepository.getBankOfferSuccess
    val getShipmentTypesCONFIGSuccess = cartRepository.getShipmentTypesCONFIGSuccessfully
    val getBankOfferError = cartRepository.getBankOfferError
    val clearPromoCodeText = cartRepository.clearPromoCodeText
    var promoCode: String = ""
    var mCartId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
        Constants.CURRENT
    } else {
        SharedPreference.getInstance().getGUID() ?: ""
    }
    var mUserId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
        Constants.CURRENT
    } else {
        Constants.ANONYMOUS
    }


//    init {
//        getBankOffer()
//    }

    fun loadCart(userId: String, cartId: String, fields: String): LiveData<Cart> {
        this.mCartId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
            Constants.CURRENT
        } else {
                SharedPreference.getInstance().getGUID() ?: ""
        }
        this.mUserId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
            Constants.CURRENT
        } else {
            Constants.ANONYMOUS
        }
        return cartRepository.loadCart(this.mUserId, this.mCartId, "")
            .apply {
                Transformations.map(this) {
                    this@CartViewModel.hideRefreshLoaderEvent.postValue(true)
                }
            }
    }

    fun getShipmentType() : LiveData<ShipmentTypes>{
        return cartRepository.loadCartShipmentTypes(this.mUserId, this.mCartId)
    }

    fun getConfig() {
        return cartRepository.loadAppConfig()
    }

    fun getCart(userId: String, cartId: String, fields: String) {
        if (SharedPreference.getInstance().getUserToken()
                ?.isNotEmpty() == true || SharedPreference.getInstance().getGUID()
                ?.isNotEmpty() == true
        ) {
            loadCart("", "", Constants.FIELDS_FULL)
        } else {
            createCart(SharedPreference.getInstance().getUserToken()?.let { "current" }
                ?: run { "anonymous" },
                "", "", Constants.FIELDS_FULL
            )
        }
    }


    fun getBankOffer() {
        cartRepository.getBankOffer()
    }


    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String
    ) {
        compositeDisposable.add(cartRepository.createCartSuccess.subscribe({
            SharedPreference.getInstance().setGUID(it?.guid ?: "")
            loadCart("", "", Constants.FIELDS_FULL)
        }, {
            Log.d(TAG, it.message ?: "")
        }))
        cartRepository.createCart(userId, oldCartId, toMergeCartGuid, fields)
    }

    fun onRefresh() {
        loadCart("", "", "")
//        getBankOffer()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun loadShipmnetSuccess(){
        validationLoadShipmentType.postValue(true)
    }

    fun onCheckoutClick() {
        // navigateTo.postValue(R.id.action_navigate_to_checkout)
        if (validationSuccess.value?.valid ?: false) {
            if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {

                shipmentCode = SharedPreference.getInstance().getShipmentType() ?: ""
                if (shipmentCode == "1") {
                    navigateTo.postValue(Constants.NavigationAction.CHECKOUT)
                } else {
                    if (cart?.value?.deliveryAddress?.id.isNullOrEmpty())
                        navigateTo.postValue(Constants.NavigationAction.ADDRESS)
                    else
                        navigateTo.postValue(Constants.NavigationAction.ADDRESS)
                }

            } else {
                navigateTo.postValue(100)
                AppUtil.showToastyError(MyApplication.getContext(), "Please login")
            }
        } else {
            var mErrorMessage: String = ""

            var builder2 = StringBuilder()
            for (s in validationSuccess.value?.errors!!) {
                builder2.append(s + "\n")
            }
            mErrorMessage = builder2.toString()
            mErrorValidationMessage.postValue(mErrorMessage)
        }
    }

    // change shipment selected
    fun onShipmentSelected() {
        if ( mUserId !="anonymous" )
        cartRepository.updateShipmentTypes(mUserId, mCartId)
    }

    fun onValidationClick() {
        if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
//            onShipmentSelected()
            cartRepository.doValidation(
                SharedPreference.getInstance().getUserToken()
                    ?.let { "current" }
                    ?: run { "anonymous" },
                SharedPreference.getInstance().getUserToken()?.let { "current" }
                    ?: run { SharedPreference.getInstance().getGUID() ?: "" })
        } else {
            navigateTo.postValue(100)
            AppUtil.showToastyError(MyApplication.getContext(), "Please login")
        }
    }

    fun openMap(){
        var latitude = cart.value?.pickupOrderGroups?.get(0)?.deliveryPointOfService?.geoPoint?.latitude
        var longitude = cart.value?.pickupOrderGroups?.get(0)?.deliveryPointOfService?.geoPoint?.longitude
        Log.d("teem lat long ", "$latitude , $longitude")
        openMapObserver.postValue("$latitude , $longitude")
    }

    fun onProductClick(code: String) {
        navigateToDetails.postValue(code)
    }

    fun setShipmentTypesRadioGroup() {
        shipmentTypesRadioGroup.postValue(true)
    }

    fun onContinueShoppingClick() {
        continueShoppingClickEvent.postValue(true)
//        activity.postValue(MainActivity())
    }

    fun onExpressDeliveryIcon() {
        expressDeliveryLiveEvent.postValue(true)
    }

    fun onQuantityIncreased(entry: Entry) {
        var newQuantity: Int = entry.quantity?.plus(1) ?: 0
        entry.quantity = newQuantity
        if (newQuantity <= entry.product?.stock?.stockLevel ?: 0) {
            cartRepository.updateQuantity(
                mUserId,
                mCartId,
                entry,
                Constants.FIELDS_FULL
            )
        } else {
//            Toast.makeText(
//                MyApplication.getContext(),
//                R.string.out_of_stock,
//                Toast.LENGTH_SHORT
//            ).show()


            AppUtil.showToastyWarning(MyApplication.getContext(), R.string.out_of_stock)

        }
    }

    fun onQuantityDecreased(entry: Entry) {
        var newQuantity: Int = entry.quantity?.minus(1) ?: 0
        entry.quantity = newQuantity
        if (newQuantity >= 1) {
            cartRepository.updateQuantity(
                mUserId,
                mCartId,
                entry,
                Constants.FIELDS_FULL
            )
        }
    }

    fun deleteEntry(entry: Entry) {
        cartRepository.deleteEntry(mUserId, mCartId, entry, Constants.FIELDS_FULL)
    }

    fun deleteVoucher(voucherId: String) {
        cartRepository.deleteVoucher(mUserId, mCartId, voucherId)
    }

    fun applyVoucher() {
        if (promoCode.isEmpty()) {
            error.postValue("Promo code required")
        } else {
            cartRepository.applyVoucher(mUserId, mCartId, promoCode.toUpperCase())
        }
    }


    class Factory(
        private val cartRepository: CartRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CartViewModel(cartRepository) as T
        }
    }
}