package com.erabia.foodcrowd.android.login.remote.repository

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.remote.service.LoginService
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.rxkotlin.subscribeBy

class LoginRepository(
    private val loginService: LoginService,
    private val cartService: CartService
) :
    Repository {
    private var guid: String = ""
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mLoginLiveData: SingleLiveEvent<TokenModel> by lazy { SingleLiveEvent<TokenModel>() }
    val mNavigateToOtpLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    companion object {
        const val TAG: String = "LoginRepository"
    }

    @SuppressLint("CheckResult")
    fun doLogin(mUserName: String, mPassword: String) {
//        loginService.getOauth(
//            "password",
//            mUserName,
//            mPassword,
//            "",
//            "occ_mobile",
//            "Erabia@123"
//        )
        loginService.loginEmailMobile(
            mUserName,
            mPassword,
            "mobile_android",
            "secret"
        )
            .get()
            .doOnSubscribe {
                loadingVisibility.value = View.VISIBLE
            }
            .doOnTerminate {
                loadingVisibility.value = View.GONE
            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mLoginLiveData.value = it.body()
                            if (SharedPreference.getInstance().getGUID()?.isNotEmpty() == true) {

                                val mFirebaseAnalytics =
                                    FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                                val params = Bundle()

                                params.putString(FirebaseAnalytics.Param.METHOD, "Login")
                                mFirebaseAnalytics.logEvent(
                                    FirebaseAnalytics.Event.LOGIN,
                                    params
                                )
                                cartService.getCart(
                                    Constants.CURRENT,
                                    Constants.CURRENT,
                                    "FULL"
                                ).get().subscribe(this,
                                    {
                                        this.guid = it.body()?.guid ?: ""

                                        cartService.createCart(
                                            Constants.CURRENT,
                                            SharedPreference.getInstance().getGUID(),
                                            this.guid,
                                            Constants.FIELDS_FULL
                                        ).get().subscribe({
                                            Log.d(TAG, "cart created")
                                        }, {
                                            Log.d(TAG, it.message ?: "")
                                        })

                                    }, onError_400 = {
                                        val errorResponse = getResponseErrorMessage(it)
                                        if (
                                            errorResponse.contains("No cart created yet.") ||
                                            errorResponse.contains("Cart not found.")
                                        ) {
                                            cartService.createCart(
                                                Constants.CURRENT,
                                                SharedPreference.getInstance().getGUID(),
                                                "",
                                                Constants.FIELDS_FULL
                                            ).get().subscribe({
                                                Log.d(TAG, "cart created")
                                            }, {
                                                Log.d(TAG, it.message ?: "")
                                            })
                                        }
                                    })


                            }


                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }

                        }
                        201 -> {
                            mLoginLiveData.value = it.body()
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }
                        401 -> {
                            error().postValue(MyApplication.getContext()!!.resources.getString(R.string.invalid_user_name_password))
                        }
                        else -> {
                            error().postValue(MyApplication.getContext()!!.resources.getString(R.string.invalid_user_name_password))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                })

    }


    @SuppressLint("CheckResult")
    fun doFingerPrintLogin(signatureId: String, email: String) {
        loginService.doFingerPrintLogin(
            signatureId, email
        ).get()
            .doOnSubscribe {
                loadingVisibility.value = View.VISIBLE
            }
            .doOnTerminate {
                loadingVisibility.value = View.GONE
            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mLoginLiveData.value = it.body()
                            if (SharedPreference.getInstance().getGUID()?.isNotEmpty() == true) {

                                val mFirebaseAnalytics =
                                    FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                                val params = Bundle()

                                params.putString(FirebaseAnalytics.Param.METHOD, "Login With Face Id")
                                mFirebaseAnalytics.logEvent(
                                    FirebaseAnalytics.Event.LOGIN,
                                    params
                                )
                                cartService.getCart(
                                    Constants.CURRENT,
                                    Constants.CURRENT,
                                    "FULL"
                                ).get().subscribe(this,
                                    {
                                        this.guid = it.body()?.guid ?: ""

                                        cartService.createCart(
                                            Constants.CURRENT,
                                            SharedPreference.getInstance().getGUID(),
                                            this.guid,
                                            Constants.FIELDS_FULL
                                        ).get().subscribe({
                                            Log.d(TAG, "cart created")
                                        }, {
                                            Log.d(TAG, it.message ?: "")
                                        })

                                    }, onError_400 = {
                                        val errorResponse = getResponseErrorMessage(it)
                                        if (
                                            errorResponse.contains("No cart created yet.") ||
                                            errorResponse.contains("Cart not found.")
                                        ) {
                                            cartService.createCart(
                                                Constants.CURRENT,
                                                "",
                                                "",
                                                Constants.FIELDS_FULL
                                            ).get().subscribe({
                                                Log.d(TAG, "cart created")
                                            }, {
                                                Log.d(TAG, it.message ?: "")
                                            })
                                        }
                                    })


                            }
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }
                        201 -> {
                            mLoginLiveData.value = it.body()
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }
                        401 -> {
                            error().postValue("Invalid user credential")
                        }
                        else -> {
                            error().postValue("Invalid user credential")
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                })

    }

    @SuppressLint("CheckResult")
    fun setFirebaseToken(
        userId: String,
        mobileToken: String
    ) {
        loginService.setFirebaseToken(userId , mobileToken).get().subscribe(
            {
                when (it.code()) {
                    200 -> {

                    }
                    401 -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
            {
                Log.d(HomeRepository.TAG, " message: ${it.message}")
                Log.d(HomeRepository.TAG, " cause: ${it.cause}")

            }
        )
    }


    fun doLoginWithOtp() {
        mNavigateToOtpLiveData.postValue(true)
    }

}
