package com.erabia.foodcrowd.android.cart.di

import com.erabia.foodcrowd.android.cart.view.CartFragment
import dagger.Component

@Component(modules = [CartModule::class])
interface CartComponent {
    fun inject(cartFragment: CartFragment)
}