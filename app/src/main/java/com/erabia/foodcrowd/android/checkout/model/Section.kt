package com.erabia.foodcrowd.android.checkout.model

data class Section(
    var itemType: CheckoutItemType? = null,
    var hasSetToCart: Boolean = false,
    var errorMessage: String = ""
) {
}