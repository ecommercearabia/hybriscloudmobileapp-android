package com.erabia.foodcrowd.android.common.util

import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.Group
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.erabia.foodcrowd.android.common.customview.VerificationTextWatcher
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.newaddress.adapter.AddressAdapter
import com.erabia.foodcrowd.android.newaddress.model.AddressList
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddressListViewModel
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.google.android.material.tabs.TabLayout


internal object BindingAdapter {


    @androidx.databinding.BindingAdapter("textChangedListener")
    @JvmStatic
    fun bindTextWatcher(editText: EditText?, textWatcher: TextWatcher?) {
        editText?.addTextChangedListener(textWatcher)
    }

    @androidx.databinding.BindingAdapter("searchViewChangedListener")
    @JvmStatic
    fun bindSearchViewWatcher(
        searchView: SearchView?,
        textWatcher: SearchView.OnQueryTextListener?
    ) {
        searchView?.setOnQueryTextListener(textWatcher)
    }

    @androidx.databinding.BindingAdapter("textViewChangedListener")
    @JvmStatic
    fun bindTextView(textView: TextView, textWatcher: TextWatcher?) {
        textView.addTextChangedListener(textWatcher)
    }

    @androidx.databinding.BindingAdapter("onCheckedChangeListener")
    @JvmStatic
    fun bindRadioGroup(
        radioGroup: RadioGroup,
        changeListener: RadioGroup.OnCheckedChangeListener?
    ) {
        radioGroup.setOnCheckedChangeListener(changeListener)
    }

    @androidx.databinding.BindingAdapter("onTabSelectedListener")
    @JvmStatic
    fun bindTabLayout(
        tabLayout: TabLayout,
        listener: TabLayout.OnTabSelectedListener
    ) {
        tabLayout.addOnTabSelectedListener(listener)
    }

    @androidx.databinding.BindingAdapter("cardBackground")
    @JvmStatic
    fun bindCardViewBackground(cardView: CardView, background: Int) {
        cardView.setBackgroundResource(background)
    }

    @androidx.databinding.BindingAdapter("switchChangedListener")
    @JvmStatic
    fun bindSwitchView(
        switchView: Switch?,
        onCheckedChangeListener: CompoundButton.OnCheckedChangeListener?
    ) {
        switchView?.setOnCheckedChangeListener(onCheckedChangeListener)
    }


    @androidx.databinding.BindingAdapter("imageResource")
    @JvmStatic
    fun bindBannerImageResource(imageView: ImageView, url: String?) {

        ImageLoader().loadImage(url, imageView)
    }

    @androidx.databinding.BindingAdapter("imageBitmapResource")
    @JvmStatic
    fun bindBitmapImageResource(imageView: ImageView, url: String?) {

        ImageLoader().loadToBitmapImage(url, imageView)
    }


    @androidx.databinding.BindingAdapter("imageResourceWithPlaceHolder")
    @JvmStatic
    fun bindImageResource(imageView: ImageView, url: String?) {
        ImageLoader().loadImageWithPlaceHolder(url, imageView)
    }

    @androidx.databinding.BindingAdapter("svgImageResourceWithPlaceHolder")
    @JvmStatic
    fun bindSvgImageResource(imageView: ImageView, url: String?) {
        ImageLoader().loadSvgImageWithPlaceHolder(url, imageView)
    }

    @androidx.databinding.BindingAdapter("setAdapter")
    @JvmStatic
    fun bindRecyclerViewAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?) {
        if (adapter != null) {
            if (!adapter.hasObservers()) {
                adapter.setHasStableIds(true)
            }
        }
        recyclerView.setHasFixedSize(true)
//        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = adapter
    }

    @androidx.databinding.BindingAdapter("appendData", "viewModel", "from")
    @JvmStatic
    fun appendData(
        view: RecyclerView,
        addressList: AddressList?,
        addressListViewModel: AddressListViewModel,
        mFromCome: String
    ) {
        try {
//            val instance = adapter
            view.adapter = addressList?.let { AddressAdapter(it, addressListViewModel, mFromCome) }
        } catch (e: Exception) {

        }
    }


    @androidx.databinding.BindingAdapter("visibilityNotFound")
    @JvmStatic
    fun visibilityNotFound(
        view: Group, addressList: AddressList?
    ) {
        try {
            if (!addressList?.addresses.isNullOrEmpty()) {
                view.visibility = View.GONE
            } else {
                view.visibility = View.VISIBLE

            }
        } catch (e: Exception) {
            Log.e("exeption", e.message ?: "")
        }
    }


    @androidx.databinding.BindingAdapter("visibilityFound")
    @JvmStatic
    fun visibilityFound(
        view: Group, addressList: AddressList?
    ) {
        try {
            if (!addressList?.addresses.isNullOrEmpty()) {
                view.visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            Log.e("exeption", e.message ?: "")

        }
    }


    @androidx.databinding.BindingAdapter("setPagerAdapter")
    @JvmStatic
    fun bindPagerAdapter(viewPager: ViewPager?, pagerAdapter: PagerAdapter?) {
        if (viewPager != null)
            viewPager.adapter = pagerAdapter
    }

    @androidx.databinding.BindingAdapter("setImage")
    @JvmStatic
    fun loadImage(view: ImageView, imageUrl: String?) {
        Glide.with(view.context)
            .load(imageUrl).apply(RequestOptions().circleCrop())
            .into(view)
    }
//    @androidx.databinding.BindingAdapter("setCategoryProductPagerAdapter")
//    @JvmStatic
//    fun bindPagerAdapter(viewPager: ViewPager?, pagerAdapter: CategoryProductPagerAdapter?) {
//        if (viewPager != null)
//            viewPager.adapter = pagerAdapter
//    }


//    @androidx.databinding.BindingAdapter(
//        value = ["selectedValue", "selectedValueAttrChanged"],
//        requireAll = false
//    )
//    @JvmStatic
//    fun bindSpinnerData(
//        pAppCompatSpinner: Spinner,
//        newSelectedValue: Int?,
//        newTextAttrChanged: InverseBindingListener
//    ) {
//        pAppCompatSpinner.onItemSelectedListener = object : OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>?,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                newTextAttrChanged.onChange()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {}
//        }
//        if (newSelectedValue != null) {
//            val pos =
//                (pAppCompatSpinner.adapter as ArrayAdapter<String?>).getPosition(
//                    newSelectedValue.toString()
//                )
//            pAppCompatSpinner.setSelection(pos, true)
//        }
//    }

    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    fun captureSelectedValue(pAppCompatSpinner: Spinner): Int? {
        return pAppCompatSpinner.selectedItem as Int
    }

    @androidx.databinding.BindingAdapter("viewVisibility")
    @JvmStatic
    fun setVisibility(view: View, visibility: Int) {
        view.visibility = visibility
    }

    @androidx.databinding.BindingAdapter("imageViewRes")
    @JvmStatic
    fun setImageResource(view: View, src: Int) {
        (view as ImageView).setImageResource(src)
    }

    @androidx.databinding.BindingAdapter("selectedValue")
    @JvmStatic // add this line !!
    fun Spinner.setSelectedValue(selectedValue: Any?) {
        setSpinnerValue(selectedValue)
    }

    @JvmStatic // add this line !!
    @androidx.databinding.BindingAdapter("entries")
    fun Spinner.setEntries(entries: List<Any>?) {
        setSpinnerEntries(entries)
    }


    @JvmStatic // add this line !!
    @androidx.databinding.BindingAdapter("entries2")
    fun Spinner.setEntries(entries: MutableLiveData<TitleResponse>?) {
        setSpinnerEntries(entries?.value?.titles)
    }

    @androidx.databinding.BindingAdapter("onItemSelected")
    fun Spinner.setOnItemSelectedListener(itemSelectedListener: ItemSelectedListener?) {
        setSpinnerItemSelectedListener(itemSelectedListener)
    }

    @androidx.databinding.BindingAdapter("newValue")
    fun Spinner.setNewValue(newValue: Any?) {
        setSpinnerValue(newValue)
    }

    @androidx.databinding.BindingAdapter("restValue")
    fun Spinner.restValue(restValue: Boolean) {
        if (restValue) {
            restValue()

        }
    }


    @androidx.databinding.BindingAdapter("selectedValueAttrChanged")
    @JvmStatic // add this line !!
    fun Spinner.setInverseBindingListener(inverseBindingListener: InverseBindingListener?) {
        setSpinnerInverseBindingListener(inverseBindingListener)
    }

    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    fun Spinner.getSelectedValue(): Any? {
        return getSpinnerValue()

    }

    @JvmStatic
    @androidx.databinding.BindingAdapter("mutableVisibility")
    fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
        val parentActivity: AppCompatActivity? = view.getParentActivity()
        if (parentActivity != null && visibility != null) {
            visibility.observe(parentActivity, Observer { value ->
                view.visibility = value ?: View.VISIBLE
            })
        }
    }

    @JvmStatic
    @androidx.databinding.BindingAdapter(value = [ "editTextNext" , "editTextPrevious"])
    fun verificationTextWatcher(
        editText: EditText?,
        editTextNext: EditText,
        editTextPrevious: EditText
    ) {
        editText?.addTextChangedListener(VerificationTextWatcher(editTextNext, editTextPrevious))
    }


//    @androidx.databinding.BindingAdapter("imageFlag")
//    @JvmStatic
//    fun setImageFlag(view: View?, country: String?) {
//        var mCountry: String? = country?.toLowerCase()
//        if(mCountry != null) {
//            when (country?.toLowerCase()) {
//                "rsa" -> mCountry = "South Africa"
//                "uae" -> mCountry = "ae"
//                "Netherland" -> mCountry = "NLD"
//            }
//            (view as? ImageView)?.setImageResource(World.getFlagOf(mCountry))
//        }
//    }
//
//    @androidx.databinding.BindingAdapter("ListingPagination")
//    @JvmStatic
//    fun bindListingPagination(view: RecyclerView, viewModel: ListingViewModel) {
//        Paginator(
//            recyclerView = view,
//            isLoading = { viewModel.isLoading },
//            loadMore = { viewModel.load(it.toString()) },
//            isLastPage = { viewModel.isLastItem(it) }
//        ).apply {
//            currentPage = 0
//        }
//    }
}