package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PaymentInfo(
    @SerializedName("accountHolderName")
    val accountHolderName: String = "",
    @SerializedName("billingAddress")
    val billingAddress: BillingAddress = BillingAddress(),
    @SerializedName("cardNumber")
    val cardNumber: String = "",
    @SerializedName("cardType")
    val cardType: CardType = CardType(),
    @SerializedName("defaultPayment")
    val defaultPayment: Boolean = false,
    @SerializedName("expiryMonth")
    val expiryMonth: String = "",
    @SerializedName("expiryYear")
    val expiryYear: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("issueNumber")
    val issueNumber: String = "",
    @SerializedName("saved")
    val saved: Boolean = false,
    @SerializedName("startMonth")
    val startMonth: String = "",
    @SerializedName("startYear")
    val startYear: String = "",
    @SerializedName("subscriptionId")
    val subscriptionId: String = ""
)