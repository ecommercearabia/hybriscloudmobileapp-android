package com.erabia.foodcrowd.android.checkout.model

data class StoreCreditModes(
    val storeCreditModes: List<StoreCreditMode>? = null
)