package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.SerializedName

class DeliveryTypes {
    @SerializedName("deliveryModeTypes")
    var deliveryModeTypes: List<DeliveryModeType> = listOf()
}

data class DeliveryModeType(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("name")
    val name: String = ""
)

data class setDeliveryTypesResponse(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("deliveryType")
    val deliveryType: String = "",
    @SerializedName("cartTimeSlot")
    val cartTimeSlot: CartExpressTimeSlot = CartExpressTimeSlot()

    )