package com.erabia.foodcrowd.android.category.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Category(
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("image")
    var image: Image? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("plpPicture")
    var plpPicture: PlpPicture? = null,
    @SerializedName("plpPictureResponsive")
    var plpPictureResponsive: PlpPictureResponsive? = null,
    @SerializedName("url")
    var url: String? = null
)