package com.erabia.foodcrowd.android.menu.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ConsentRowBinding
import com.erabia.foodcrowd.android.menu.model.ConsentTemplate
import com.erabia.foodcrowd.android.menu.viewmodel.ConsentViewModel

import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.consent_row.view.*

class ConsentRecyclerViewAdapter(
    var sharedViewModel: SharedViewModel, var consentViewModel: ConsentViewModel
) :
    RecyclerView.Adapter<ConsentViewHolder>() {
    private var context: Context? = null
    var list: ArrayList<ConsentTemplate> = arrayListOf()
    lateinit var binding: ConsentRowBinding


    override fun onBindViewHolder(holder: ConsentViewHolder, position: Int) {

        if (list.size - 1 == position) {
            holder.itemView.line.visibility = View.GONE
        }

        holder.itemView.text_view_menu.text = list[position].name
        holder.itemView.text_view_description_.text = list[position].description
        if (list[position].currentConsent == null || list[position].currentConsent?.consentWithdrawnDate != null) {
            holder.itemView.switch_consent.isChecked = false
        } else {
            holder.itemView.switch_consent.isChecked = true
        }

        holder.itemView.switch_consent.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.d("checked", "onBindViewHolder:$isChecked ${list[position].id} ")


            if (isChecked) {
                consentViewModel.giveConsent(
                    "current",
                    list[position].id ?: "", list[position].version ?: ""
                )
            } else {
                consentViewModel.deleteConsent(
                    "current",
                    list[position].currentConsent?.code ?: ""
                )
            }

        }


    }

    @SuppressLint("NotifyDataSetChanged")
    fun setMenu(list: ArrayList<ConsentTemplate>) {
        this.list.clear()
        this.list.addAll(list)
        this.notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConsentViewHolder {
        binding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.consent_row,
                parent,
                false
            )
        return ConsentViewHolder(
            binding.root
        )

    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ConsentViewHolder(view: View) : RecyclerView.ViewHolder(view) {


}
