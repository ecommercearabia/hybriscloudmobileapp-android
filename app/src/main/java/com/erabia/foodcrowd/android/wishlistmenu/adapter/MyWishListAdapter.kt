package com.erabia.foodcrowd.android.wishlistmenu.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.databinding.MyOrderRowItemBinding
import com.erabia.foodcrowd.android.databinding.MyWishListRowItemBinding
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import com.erabia.foodcrowd.android.wishlistmenu.viewmodel.WishListViewModel


class MyWishListAdapter(
    val myWishList: List<GetWishListResponse.WishlistEntry?>,
    val mWishListViewModel: WishListViewModel
) :
    RecyclerView.Adapter<ViewHolder>() {
    private var context: Context? = null
    var quantity: Int = 1

    lateinit var mMyWishListRowItemBinding: MyWishListRowItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.my_wish_list_row_item, parent, false)
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        mMyWishListRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.my_wish_list_row_item, parent, false)
        return ViewHolder(
            mMyWishListRowItemBinding
        )

    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return myWishList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mMyWishListRowItemBinding.wishlistEntry = myWishList[position]
        holder.mMyWishListRowItemBinding.mWishListViewModel = mWishListViewModel
        holder.mMyWishListRowItemBinding.postion = position
        var product = myWishList.get(position)?.product
        quantity = 1
        holder.mMyWishListRowItemBinding.imageViewQuantityDecrease.setOnClickListener {

            if (quantity > 1) {
                quantity--
                holder.mMyWishListRowItemBinding.textViewProductQuantity.text = quantity.toString()

            }
        }

        holder.mMyWishListRowItemBinding.imageViewQuantityIncrease.setOnClickListener {
            if (quantity < product?.stock?.stockLevel ?: 0) {
                quantity++
                holder.mMyWishListRowItemBinding.textViewProductQuantity.text = quantity.toString()
            }
        }

        holder.mMyWishListRowItemBinding.mAddToCartButton.setSafeOnClickListener {

            product?.let { mWishListViewModel?.onAddToCartClick(it, quantity) }

        }

        holder.mMyWishListRowItemBinding.mDeleteWishListButoon.setSafeOnClickListener {


            mWishListViewModel.deleteWishItem(myWishList[position]?.product?.code ?: "")

        }


        if ((product?.stock?.stockLevel ?: 0) > 0) {
            holder.mMyWishListRowItemBinding.clQuantityContainer.visibility = View.VISIBLE
            holder.mMyWishListRowItemBinding.mOutOfStockTextView.visibility = View.GONE
            holder.mMyWishListRowItemBinding.mAddToCartButton.visibility = View.VISIBLE
        } else {
            holder.mMyWishListRowItemBinding.mOutOfStockTextView.visibility = View.VISIBLE
            holder.mMyWishListRowItemBinding.clQuantityContainer.visibility = View.GONE
            holder.mMyWishListRowItemBinding.mAddToCartButton.visibility = View.GONE
        }


    }

}

class ViewHolder(val mMyWishListRowItemBinding: MyWishListRowItemBinding) :
    RecyclerView.ViewHolder(mMyWishListRowItemBinding.root)

