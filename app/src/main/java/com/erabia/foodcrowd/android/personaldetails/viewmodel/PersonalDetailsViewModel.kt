package com.erabia.foodcrowd.android.personaldetails.viewmodel

import android.app.Application
import android.opengl.Visibility
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.personaldetails.remote.repository.PersonalDetailsRepository
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import com.erabia.foodcrowd.android.storecredit.remote.repository.StoreCreditRepository
import okhttp3.internal.filterList

class PersonalDetailsViewModel(val mPersonalDetailsRepository: PersonalDetailsRepository) :
    ViewModel() {
    var mIndexTitle: Int = 0
    var mIndexNationality: Int = 0
    var skipTitle: Boolean = true
    var skipNationality: Boolean = true

    var mTitleListLiveData: LiveData<TitleResponse> = mPersonalDetailsRepository.mTitleListLiveData
    var mNationalitiesListLiveData: LiveData<NationalitiesResponse> = mPersonalDetailsRepository.mNationalitiesListLiveData
    var country: LiveData<CountryResponse> = mPersonalDetailsRepository.mCountryListLiveData
    var mPersonalDetailsModelLiveData: LiveData<PersonalDetailsModel> =
        mPersonalDetailsRepository.mPersonalDetailsModelLiveData

    //    var mNationalitiesPersonalDetailsModelLiveData: LiveData<NationalitiesResponse> =
//        mPersonalDetailsRepository.mNationalitiesListLiveData
    val loadingVisibility: MutableLiveData<Int> = mPersonalDetailsRepository.loadingVisibility
    var mUpdateProfileLiveData: LiveData<Void> = mPersonalDetailsRepository.mUpdateProfileLiveData
    val mSendOtp:  SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    val mErrorEvent: SingleLiveEvent<String> = mPersonalDetailsRepository.error()
    var mAvailableBalance = ""
    var onViewOrderClickObserver: MutableLiveData<Int> = MutableLiveData()
    var mTitleCode: MutableLiveData<String> = MutableLiveData()
    var mNationalityCodePersonal: MutableLiveData<String> = MutableLiveData()
    var mNationalityNamePersonal: MutableLiveData<String> = MutableLiveData()
    var countryCodeList = MutableLiveData<CountryResponse>()
    var countryCode: CountryResponse.Country? = null
    val selectedCountry: MutableLiveData<CountryResponse.Country> by lazy { MutableLiveData<CountryResponse.Country>() }
    var codeNumber = ""
    var mBirthDate = ""
    var mErrorFirstNameValidationMessage: MutableLiveData<Boolean> = MutableLiveData()
    var mErrorLastNameValidationMessage: MutableLiveData<Boolean> = MutableLiveData()
    var mErrorBirthDateValidationMessage: MutableLiveData<Boolean> = MutableLiveData()
    var mErrorMobileNumberValidationMessage: MutableLiveData<Boolean> = MutableLiveData()
    val mEnableButton: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    var mErrorSubscribeSmsValidationMessage: MutableLiveData<Boolean> = MutableLiveData()
    var mSubscribeSmsChecked: Boolean = false
    var enableButton : Boolean = false
    var enableButtonTitle : Boolean = false
    var enableButtonNationality : Boolean = false



    lateinit var mBranchSelected: String

    init {
        getTitleAndPersonalDetailsBalance()
//        getNationalitiesPersonalDetailsBalance()
        mErrorFirstNameValidationMessage.value = false
        mErrorLastNameValidationMessage.value = false
        mErrorBirthDateValidationMessage.value = false
        mErrorMobileNumberValidationMessage.value = false
    }

    private fun getTitleAndPersonalDetailsBalance() {
        mPersonalDetailsRepository.getTitleAndPersonalDetailsBalance()
    }

    fun getNationalitiesPersonalDetailsBalance() {
//        mPersonalDetailsRepository.getNationalities()
    }

    fun getNationalitiesConfig() {
        mPersonalDetailsRepository.getNationalityConfig()
    }

//    fun doUpdate() {
//        Log.e("aaasssssas", mPersonalDetailsModelLiveData.value?.firstName.toString())
//        mPersonalDetailsRepository.updateProfile()
//    }

    fun doUpdate() {
        Log.e("aaasssssas", mPersonalDetailsModelLiveData.value?.firstName.toString())
        mSendOtp.postValue(true)
    }



    fun firstTimeCountrySelected(mCode: String) {
        codeNumber = mCode
    }

    fun onSelectNationalitiesItem(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        if (!mPersonalDetailsModelLiveData.value?.nationality?.code.isNullOrEmpty() && skipNationality) {
            skipNationality = false
            for (i in 0 until (mNationalitiesListLiveData?.value?.nationalities?.size ?: 0) step 1) {
                if (mNationalitiesListLiveData.value?.nationalities?.get(i)?.code.equals(mPersonalDetailsModelLiveData.value?.nationality?.code) ) {
                    spinner?.setSelection(i)
                    mNationalityCodePersonal.value = mNationalitiesListLiveData.value?.nationalities?.get(i)?.code ?: ""
                    mPersonalDetailsModelLiveData.value?.nationality?.code = mNationalityCodePersonal.value
                    mPersonalDetailsModelLiveData.value?.nationality?.name = mNationalitiesListLiveData.value?.nationalities?.get(i)?.name ?: ""
                    enableButtonNationality = true
                    enableButton()
                }
            }
        }else {

            if (pos == 0) {
                spinner?.setSelection(pos)
                enableButtonNationality = false
                enableButton()
                mNationalityCodePersonal.value = ""

            } else {
                skipNationality = false
                enableButtonNationality = true
                enableButton()
                spinner?.setSelection(pos)
                mNationalityCodePersonal.value =
                    mNationalitiesListLiveData.value?.nationalities?.get(pos)?.code ?: ""
                mPersonalDetailsModelLiveData.value?.nationality?.code =
                    mNationalityCodePersonal.value
                mPersonalDetailsModelLiveData.value?.nationality?.name =
                    mNationalitiesListLiveData.value?.nationalities?.get(pos)?.name ?: ""
            }
        }
    }

    fun onSelectItem(
        parent: AdapterView<*>?,
        spinner: Spinner?,
        pos: Int,
        id: Long
    ) {
        if (!mPersonalDetailsModelLiveData.value?.titleCode.isNullOrEmpty() && skipTitle) {
            skipTitle = false
            for (i in 0 until (mTitleListLiveData?.value?.titles?.size ?: 0) step 1) {
                if (mTitleListLiveData.value?.titles?.get(i)?.code.equals(mPersonalDetailsModelLiveData.value?.titleCode)
              ) {
                    spinner?.setSelection(i)
                    mTitleCode.value = mTitleListLiveData.value?.titles?.get(i)?.code ?: ""
                    mPersonalDetailsModelLiveData.value?.titleCode = mTitleCode.value
                    mPersonalDetailsModelLiveData.value?.title = mTitleListLiveData.value?.titles?.get(i)?.name ?: ""
                }
            }
            enableButtonTitle = true
            enableButton()
        }else{
            if(pos == 0){
                spinner?.setSelection(pos)
                enableButtonTitle = false
                enableButton()
                mTitleCode.value = ""

            }
            else {
                skipTitle = false
                enableButtonTitle = true
                enableButton()
                spinner?.setSelection(pos)
                mTitleCode.value = mTitleListLiveData.value?.titles?.get(pos)?.code ?: ""
                mPersonalDetailsModelLiveData.value?.titleCode = mTitleCode.value
                mPersonalDetailsModelLiveData.value?.title =
                    mTitleListLiveData.value?.titles?.get(pos)?.name ?: ""
            }
        }
    }

    fun countryCode() {
        Log.e("asdzxc", country.value?.countries?.get(0)?.isdcode.toString())
        codeNumber = mPersonalDetailsModelLiveData.value?.defaultAddress?.mobileCountry?.isdcode ?: ""
        countryCodeList.postValue(country.value)
    }

    fun enableButton(){
        if(enableButton && enableButtonNationality && enableButtonTitle){
            mEnableButton.postValue(true)
        }
        else {
            mEnableButton.postValue(false)
        }
    }
    fun emptyText(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                enableButton = (mPersonalDetailsModelLiveData.value?.firstName.toString().isNotEmpty()
                        && mPersonalDetailsModelLiveData.value?.lastName.toString().isNotEmpty() &&
                        mPersonalDetailsModelLiveData.value?.mobileNumber.toString()
                            .isNotEmpty())

                enableButton()
            }


            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }

    class Factory(
        val mPersonalDetailsRepository: PersonalDetailsRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return PersonalDetailsViewModel(mPersonalDetailsRepository) as T
        }
    }
}