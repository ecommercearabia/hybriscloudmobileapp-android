package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class Feature(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("comparable")
    val comparable: Boolean = false,
    @SerializedName("featureValues")
    @TypeConverters(DataConverter::class)
    val featureValues: List<FeatureValue> = listOf(),
    @SerializedName("name")
    val name: String = "",
    @SerializedName("range")
    val range: Boolean = false
)