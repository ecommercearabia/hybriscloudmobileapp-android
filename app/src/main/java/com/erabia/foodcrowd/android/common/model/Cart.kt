package com.erabia.foodcrowd.android.common.model


import android.os.Parcel
import android.os.Parcelable
import com.erabia.foodcrowd.android.checkout.model.StoreCreditMode
import com.google.gson.annotations.SerializedName

data class Cart(
    @SerializedName("appliedOrderPromotions")
    val appliedOrderPromotions: List<AppliedOrderPromotion> = listOf(),
    @SerializedName("appliedProductPromotions")
    val appliedProductPromotions: List<AppliedProductPromotion> = listOf(),
    @SerializedName("appliedVouchers")
    val appliedVouchers: List<AppliedVoucher> = listOf(),
    @SerializedName("calculated")
    val calculated: Boolean = false,
    @SerializedName("cartTimeSlot")
    val cartTimeSlot: CartExpressTimeSlot = CartExpressTimeSlot(),
    @SerializedName("code")
    val code: String? = "",
    @SerializedName("created")
    val created: String? = "",
    @SerializedName("deliveryAddress")
    val deliveryAddress: Address = Address(),
    @SerializedName("deliveryCost")
    val deliveryCost: DeliveryCost = DeliveryCost(),
    @SerializedName("deliveryItemsQuantity")
    val deliveryItemsQuantity: Int = 0,
    @SerializedName("deliveryMode")
    val deliveryMode: DeliveryMode = DeliveryMode(),
    @SerializedName("deliveryOrderGroups")
    val deliveryOrderGroups: List<DeliveryOrderGroup> = listOf(),
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("entries")
    val entries: List<Entry>? = listOf(),
    @SerializedName("expirationTime")
    val expirationTime: String? = "",
    @SerializedName("guid")
    val guid: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("net")
    val net: Boolean = false,
    @SerializedName("orderDiscounts")
    val orderDiscounts: OrderDiscounts = OrderDiscounts(),
    @SerializedName("paymentInfo")
    val paymentInfo: PaymentInfo = PaymentInfo(),
    @SerializedName("paymentMode")
    val paymentMode: PaymentMode? = null,
    @SerializedName("pickupItemsQuantity")
    val pickupItemsQuantity: Int = 0,
    @SerializedName("pickupOrderGroups")
    val pickupOrderGroups: List<PickupOrderGroup> = listOf(),
    @SerializedName("potentialOrderPromotions")
    val potentialOrderPromotions: List<PotentialOrderPromotion> = listOf(),
    @SerializedName("potentialProductPromotions")
    val potentialProductPromotions: List<PotentialProductPromotion> = listOf(),
    @SerializedName("productDiscounts")
    val productDiscounts: ProductDiscounts = ProductDiscounts(),

    @SerializedName("storeCreditMode")
    val storeCreditMode: StoreCreditMode = StoreCreditMode(),


    @SerializedName("saveTime")
    val saveTime: String? = "",
    @SerializedName("savedBy")
    val savedBy: SavedBy? = SavedBy(),
    @SerializedName("site")
    val site: String? = "",
    @SerializedName("store")
    val store: String? = "",
    @SerializedName("subTotal")
    val subTotal: SubTotal = SubTotal(),
    @SerializedName("subTotalBeforeSavingPrice")
    val subTotalBeforeSavingPrice: SubTotalBeforeSavingPrice = SubTotalBeforeSavingPrice(),
    @SerializedName("totalDiscounts")
    val totalDiscounts: TotalDiscounts = TotalDiscounts(),
    @SerializedName("totalItems")
    val totalItems: Int = 0,
    @SerializedName("totalPrice")
    val totalPrice: TotalPrice = TotalPrice(),
    @SerializedName("totalPriceWithTax")
    val totalPriceWithTax: TotalPriceWithTax = TotalPriceWithTax(),
    @SerializedName("totalTax")
    val totalTax: TotalTax = TotalTax(),
    @SerializedName("totalUnitCount")
    val totalUnitCount: Int = 0,
    @SerializedName("user")
    val user: User = User(),
    @SerializedName("timeSlotInfoData")
    var timeSlotInfoDataData: TimeSlotInfoData? = null,
    @SerializedName("storeCreditAmount")
    val storeCreditAmount: StoreCreditAmount = StoreCreditAmount(),
    @SerializedName("deliveryStatusDisplay")
    val deliveryStatusDisplay: String? = "",
    @SerializedName("deliveryStatus")
    val deliveryStatus: String? = "",
    @SerializedName("deliveryType")
    val deliveryType: String? = "",
    @SerializedName("express")
    val express: Boolean = false
    ) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("appliedOrderPromotions"),
        TODO("appliedProductPromotions"),
        TODO("appliedVouchers"),
        parcel.readByte() != 0.toByte(),
        TODO("CartExpressTimeSlot"),
        parcel.readString(),
        parcel.readString(),
        TODO("deliveryAddress"),
        TODO("deliveryCost"),
        parcel.readInt(),
        TODO("deliveryMode"),
        TODO("deliveryOrderGroups"),
        parcel.readString(),
        parcel.createTypedArrayList(Entry),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        TODO("orderDiscounts"),
        TODO("paymentInfo"),
        TODO("paymentMode"),
        parcel.readInt(),
        TODO("pickupOrderGroups"),
        TODO("potentialOrderPromotions"),
        TODO("potentialProductPromotions"),
        TODO("productDiscounts"),
        TODO("storeCreditMode"),
        parcel.readString(),
        TODO("savedBy"),
        parcel.readString(),
        parcel.readString(),
        TODO("subTotal"),
        TODO("subTotalBeforeSavingPrice"),
        TODO("totalDiscounts"),
        parcel.readInt(),
        TODO("totalPrice"),
        TODO("totalPriceWithTax"),
        TODO("totalTax"),
        parcel.readInt(),
        TODO("user"),
        TODO("timeSlotInfoDataData"),
        TODO("storeCreditAmount"),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel?, p1: Int) {
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<Cart> {
        override fun createFromParcel(parcel: Parcel): Cart {
            return Cart(parcel)
        }

        override fun newArray(size: Int): Array<Cart?> {
            return arrayOfNulls(size)
        }
    }
}