package com.erabia.foodcrowd.android.login.view

import android.app.Activity
import android.app.Dialog
import android.app.KeyguardManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.security.keystore.KeyProperties
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityLoginBinding
import com.erabia.foodcrowd.android.forgetpassword.view.ForgetPasswordActivity
import com.erabia.foodcrowd.android.home.remote.service.LoginService
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.login.viewmodel.LoginViewModel
import com.erabia.foodcrowd.android.loginwithotp.view.LoginWithOtpActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.signup.view.SignUpActivity
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.util.concurrent.Executor


class LoginActivity : AppCompatActivity(), BiometricManager.LoginBiometricCallBack {

    var mEmailEditText: String = ""

    val compositeDisposable = CompositeDisposable()
    private lateinit var mViewModel: LoginViewModel
    var isPattrenAndFingerIsEnabled = false
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)

        val factory = LoginViewModel.Factory(
            LoginRepository(
                RequestManager.getClient().create(LoginService::class.java),
                RequestManager.getClient().create(CartService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(LoginViewModel::class.java)
        binding.lifecycleOwner = this
        binding.mLoginViewModel = mViewModel
        BiometricManager.onLoginBiometricResult = this

        val km = getSystemService(KEYGUARD_SERVICE) as KeyguardManager


        if (SharedPreference.getInstance().isFingerPrintEnabled()) {
            if (km.isKeyguardSecure) {
                binding.mLoginFingerButton.visibility = View.VISIBLE
                binding.mTouchIdTextView.visibility = View.VISIBLE
                isPattrenAndFingerIsEnabled = true
            } else {
                binding.mLoginFingerButton.visibility = View.VISIBLE
                binding.mTouchIdTextView.visibility = View.VISIBLE
                isPattrenAndFingerIsEnabled = false
            }
        } else {
            binding.mLoginFingerButton.visibility = View.GONE
            binding.mTouchIdTextView.visibility = View.GONE
        }


        observerValidation()
        observerLogin()
        observerNavigateToLoginOtp()
        binding.mSignUpTextView.clicks().filterRapidClicks().subscribe {
            startActivityForResult(Intent(this, SignUpActivity::class.java), 99)
        }.addTo(compositeDisposable)


        binding.mLoginButton.clicks().filterRapidClicks().subscribe {
            mEmailEditText = binding.mEmailLoginEditText.text.toString()
            mViewModel.doLogin()
        }.addTo(compositeDisposable)

        binding.mForgetPasswordTextView.clicks().filterRapidClicks().subscribe {
            startActivityForResult(Intent(this, ForgetPasswordActivity::class.java), 11)
        }.addTo(compositeDisposable)


        mViewModel.onBackImageObserver.observe(this, Observer {
            finish()
        })

        if (SharedPreference.getInstance()
                .isRememberMeLoginFlag() && !SharedPreference.getInstance().getLoginEmail()
                .isNullOrEmpty()
        ) {
            binding.mRememberMeCheckBox.isChecked = true
            binding.mEmailLoginEditText.setText(SharedPreference.getInstance().getLoginEmail())
        }



        binding.mLoginFingerButton.setOnClickListener {
            isPattrenAndFingerIsEnabled = km.isKeyguardSecure
            if (isPattrenAndFingerIsEnabled) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {

                    val authIntent = km.createConfirmDeviceCredentialIntent(
                        getString(R.string.enter_biometrice_title),
                        getString(R.string.enter_biometric_subtitle)
                    )
                    startActivityForResult(authIntent, 100)
                } else {

                    val biometricManager = BiometricManager(this)
                    biometricManager.makeLoginBiometric(this)
                }

            } else {


                AppUtil.showToastyWarning(this , R.string.fingerprint_disabled)

            }

        }
    }


    fun observerValidation() {
        mViewModel.mErrorEmailValidationMessage.observe(this, Observer {
            binding.mEmailLoginEditText.error = it
        })

        mViewModel.mErrorPasswordValidationMessage.observe(this, Observer {
            binding.mPasswordLoginEditTExt.error = it
        })

    }

    //    basic openid
    fun observerLogin() {
        mViewModel.mLoginLiveData.observe(this, Observer {

            SharedPreference.getInstance()
                .setRememberMeLoginFlag(binding.mRememberMeCheckBox.isChecked)
            Log.e("mohamedrefreshToken", it.refreshToken ?: "")
            SharedPreference.getInstance().setRefreshToken(it.refreshToken ?: "")
            SharedPreference.getInstance().setUser_TOKEN(it.accessToken ?: "")
            SharedPreference.getInstance().setApp_TOKEN(it.accessToken ?: "")
//            SharedPreference.getInstance().setLOGIN_User(mEmailLoginEditText.text.toString())
            SharedPreference.getInstance().setPreviousLoginEmail(
                SharedPreference.getInstance().getLoginEmailFingerPrint() ?: ""
            )
//            SharedPreference.getInstance().setPreviousFromEditTextLoginEmail(mEmailEditText)
            SharedPreference.getInstance().setLOGIN_EMAIL_FINGERPRINT(mEmailEditText)
            SharedPreference.getInstance().setLOGIN_EMAIL(mEmailEditText)


            AppUtil.showToastySuccess(  this , R.string.success_login)

            setResult(Activity.RESULT_OK)
            finish()
        })

        mViewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)
        })


    }

    private fun observerNavigateToLoginOtp() {
        mViewModel.mNavigateToLoginOtpLiveData.observe(this, Observer {
            val intent = Intent(this, LoginWithOtpActivity::class.java)
            intent.addFlags(intent.flags or Intent.FLAG_ACTIVITY_NO_HISTORY)
            startActivity(intent)
            finish()
        })

        mViewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            setResult(Activity.RESULT_OK)
            finish()
        }

        if (requestCode == 100) {
            if (!SharedPreference.getInstance().getSIGNATURE_ID().isNullOrEmpty()) {
                mEmailEditText = SharedPreference.getInstance().getLoginEmailFingerPrint() ?: ""
                mViewModel.doFingerPrintLogin(mEmailEditText)
            }
        }


        if (requestCode == 11 && resultCode == Activity.RESULT_OK) {
            binding.mLoginFingerButton.visibility = View.GONE
            binding.mTouchIdTextView.visibility = View.GONE

        }


    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onLoginBiometricCallBack(mEmail: String) {
        mEmailEditText = mEmail
        mViewModel.doFingerPrintLogin(mEmailEditText)
    }


}