package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class FutureStock(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("formattedDate")
    val formattedDate: String = "",
    @SerializedName("stock")
    val stock: Stock = Stock()
)