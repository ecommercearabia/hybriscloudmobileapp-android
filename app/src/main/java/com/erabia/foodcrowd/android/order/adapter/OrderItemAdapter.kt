package com.erabia.foodcrowd.android.order.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.MyOrderItemListRowItemBinding
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel


class OrderItemAdapter(val myOrderItemList: List<OrderDetailsResponse.Entry?>,val mMyOrderViewModel: MyOrderViewModel) :
    RecyclerView.Adapter<MyViewHolder>() {
    private var context: Context? = null

    lateinit var mMyOrderItemListRowItemBinding: MyOrderItemListRowItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//        val view = LayoutInflater.from(parent.context).inflate(R.layout.my_order_item_list_row_item, parent, false)

        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        mMyOrderItemListRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.my_order_item_list_row_item, parent, false)
        return MyViewHolder(mMyOrderItemListRowItemBinding)


    }

    override fun getItemCount(): Int {
        return myOrderItemList.size
    }
    override fun getItemViewType(position: Int): Int {
        return position
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.mMyOrderItemListRowItemBinding.mEntry= myOrderItemList?.get(position)
        holder.mMyOrderItemListRowItemBinding.mOrderViewModel= mMyOrderViewModel
        holder.mMyOrderItemListRowItemBinding.postion= position
//        holder.mMyOrderItemListRowItemBinding.mWishitemImageView.setImageURI(myOrderItemList?.get(position)?.product?.categories?.get(0)?.image.url)
//        if (!myOrderItemList?.get(position)?.product?.images.isNullOrEmpty()){
//            var imageLoader=ImageLoader()
//            imageLoader.loadImage(myOrderItemList?.get(position)?.product?.images?.get(0)?.url,holder.mMyOrderItemListRowItemBinding.mWishitemImageView)
//        }
//        else{
//            mMyOrderItemListRowItemBinding.mWishitemImageView.setImageResource(R.drawable.ic_image_placeholder)
//        }

    }

}

class MyViewHolder(val mMyOrderItemListRowItemBinding: MyOrderItemListRowItemBinding) : RecyclerView.ViewHolder(mMyOrderItemListRowItemBinding.root)

