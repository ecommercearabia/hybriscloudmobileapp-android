package com.erabia.foodcrowd.android.home.adapter

import android.content.Context
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.viewpager.widget.PagerAdapter
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.databinding.BannersRowItemBinding
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel


class BannerAdapter(val navController: NavController, val homeViewModel: HomeViewModel) : PagerAdapter() {

    private lateinit var context: Context

    companion object {
        const val TAG = "BannerAdapter"
    }

    //in order to show offline cached image , we have to pass size of list to instansiate items
    //inside the pager adapter, then glide will display the  cached images
    var size: Int = 0
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var componentList: List<Component>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }



    override fun getCount(): Int {
        if (componentList != null && componentList?.count() != null)
            return componentList?.count()!!
        else return 0
//        return size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        context = container.context

        var component: Component? = null



        val itemBinding: BannersRowItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.banners_row_item, container, false
        )
        try {
            component = componentList?.get(position) ?: Component()
        } catch (e: IndexOutOfBoundsException) {
            Log.d(TAG, e.message ?: "")
        }

        itemBinding.url = component?.media?.mobile?.url

        itemBinding.imageViewBanner.setOnClickListener {
            homeViewModel.bannersNavegate(componentList?.get(position)?.urlLink.toString())
        }

        container.addView(itemBinding.root)
        return itemBinding.root
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}