package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Promotion(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("couldFireMessages")
    val couldFireMessages: List<String> = listOf(),
    @SerializedName("description")
    val description: String = "",
    @SerializedName("enabled")
    val enabled: Boolean = false,
    @SerializedName("endDate")
    val endDate: String = "",
    @SerializedName("firedMessages")
    val firedMessages: List<String> = listOf(),
    @SerializedName("priority")
    val priority: Int = 0,
    @SerializedName("productBanner")
    val productBanner: ProductBanner = ProductBanner(),
    @SerializedName("promotionGroup")
    val promotionGroup: String = "",
    @SerializedName("promotionType")
    val promotionType: String = "",
    @SerializedName("restrictions")
    val restrictions: List<Restriction> = listOf(),
    @SerializedName("startDate")
    val startDate: String = "",
    @SerializedName("title")
    val title: String = ""
)