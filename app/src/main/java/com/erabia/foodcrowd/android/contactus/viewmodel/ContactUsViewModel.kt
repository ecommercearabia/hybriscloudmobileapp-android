package com.erabia.foodcrowd.android.contactus.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse
import com.erabia.foodcrowd.android.contactus.remote.ContactUsRepository
import com.erabia.foodcrowd.android.contactus.remote.ContactUsService

class ContactUsViewModel(val mContactUsRepository: ContactUsRepository) : ViewModel() {

    var mContactUsResponseLiveData: LiveData<ContactUsResponse> =
        mContactUsRepository.mContactUsResponseLiveData
    val loadingVisibility: MutableLiveData<Int> = mContactUsRepository.loadingVisibility
    val mErrorEvent: SingleLiveEvent<String> = mContactUsRepository.error()

    fun getContactUs() {
        mContactUsRepository.getContactUsDetails()
    }

    class Factory(
        val mContactUsRepository: ContactUsRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ContactUsViewModel(mContactUsRepository) as T
        }

    }
}