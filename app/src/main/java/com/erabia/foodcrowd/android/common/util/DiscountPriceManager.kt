package com.erabia.foodcrowd.android.common.util

import android.view.View
import android.widget.TextView
import com.erabia.foodcrowd.android.common.model.Product

class DiscountPriceManager(val product: Product) {

    fun setupPriceView(
        textViewPrice: TextView,
        textViewOldPrice: TextView,
        textViewDiscountPercentage: TextView? = null,
        textViewDiscountPercentageSmall: TextView? = null,
        spanCount: Int? = 1
    ) {
        if (product.discount?.discountPrice?.formattedValue?.isNotEmpty() == true) {
            textViewOldPrice.visibility = View.VISIBLE
            if (spanCount==4){
                textViewDiscountPercentageSmall?.visibility = View.VISIBLE
                textViewDiscountPercentage?.visibility = View.INVISIBLE
                textViewDiscountPercentageSmall?.text = "-".plus(product.discount?.percentage?.toInt().toString()).plus("%")

            }else{
                textViewDiscountPercentage?.visibility = View.VISIBLE
                textViewDiscountPercentageSmall?.visibility = View.INVISIBLE
                textViewDiscountPercentage?.text = "-".plus(product.discount?.percentage?.toInt().toString()).plus("%")

            }
            textViewOldPrice.text = product.price?.formattedValue ?: ""
            textViewPrice.text =
                product.discount?.discountPrice?.formattedValue ?: ""
        } else {
            textViewDiscountPercentage?.visibility = View.INVISIBLE
            textViewDiscountPercentage?.visibility = View.INVISIBLE
            textViewOldPrice.visibility = View.GONE
            textViewPrice.text = product.price?.formattedValue ?: ""
        }
    }
}