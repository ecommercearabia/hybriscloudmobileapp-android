package com.erabia.foodcrowd.android.common

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.res.Configuration
import android.os.Build
import com.blongho.country_data.World
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.facebook.stetho.Stetho
import java.util.*

class MyApplication : Application() {
    companion object {


        private var mContext: Context? = null
        private var instance: MyApplication? = null
        private var mActivity: Activity? = null

        @Synchronized
        fun getInstance(): MyApplication? {
            return instance
        }

        fun getContext(): Context? {
            return mContext
        }

        fun setActivityContext(activity: Activity) {
            mActivity = activity
        }

        fun getActivity(): Activity? {
            return mActivity

        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        Stetho.initializeWithDefaults(this)
        mContext = this
        LocaleHelper.updateResources(
            mContext,
            LocaleHelper.getLanguage(mContext as MyApplication)
        )
        World.init(applicationContext);

    }

    protected override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleHelper.setLocale(base))
        // super.attachBaseContext(base)

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        LocaleHelper.setLocale(this)

    }
    fun changeLocale(lang:String?) {
        val mLocale = Locale(lang)
        val config = baseContext.resources
            .configuration
        Locale.setDefault(mLocale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(mLocale)
        } else config.locale = mLocale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }


}