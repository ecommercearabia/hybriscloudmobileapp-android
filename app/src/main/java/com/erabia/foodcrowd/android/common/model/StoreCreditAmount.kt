package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class StoreCreditAmount(
    @SerializedName("currencyIso")
    val currencyIso: String = "",
    @SerializedName("formattedValue")
    val formattedValue: String = "",
    @SerializedName("priceType")
    val priceType: String = "",
    @SerializedName("value")
    val value: Double = 0.0
)