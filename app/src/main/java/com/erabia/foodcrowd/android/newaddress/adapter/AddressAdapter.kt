package com.erabia.foodcrowd.android.newaddress.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddressListViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.ItemAddressRowBinding
import com.erabia.foodcrowd.android.databinding.RowAddressBinding
import com.erabia.foodcrowd.android.newaddress.model.AddressList
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse


class AddressAdapter(
    val addressList: AddressList,
    var addressListViewModel: AddressListViewModel,
    var mFromCome: String
) :
    RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {
    private var context: Context? = null

    lateinit var binding: ItemAddressRowBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        this.context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_address_row,
            parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return addressList.addresses?.size ?: 0
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val addressModel = addressList.addresses?.get(position)
        holder.view.addressModel = addressModel
        holder.view.addressVM = addressListViewModel
        holder.view.fromCome = mFromCome
        holder.view.imageViewFav.setImageResource(
            if (addressModel?.defaultAddress == true) {
                R.drawable.ic_star_defualt_address
            } else {
                R.drawable.ic_star_gray
            }
        )

        holder.view.textViewPhoneNumber.text =
            "${addressModel?.mobileCountry?.isdcode}-${
                AppUtil.getMobileNumberWithoutIsoCode(
                    addressModel?.mobileNumber ?: ""
                )
            }"

    }

    class MyViewHolder(val view: ItemAddressRowBinding) : RecyclerView.ViewHolder(view.root)

    override fun getItemId(position: Int): Long {
        return addressList.addresses?.get(position)?.id?.toLong() ?: 0
    }


}
