package com.erabia.foodcrowd.android.personaldetails.view

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.setSpinnerEntries
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentPersonalDetailsBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.personaldetails.remote.repository.PersonalDetailsRepository
import com.erabia.foodcrowd.android.personaldetails.remote.service.PersonalDetailsService
import com.erabia.foodcrowd.android.personaldetails.viewmodel.PersonalDetailsViewModel
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import java.text.SimpleDateFormat
import java.util.*


class PersonalDetailsFragment : Fragment() {
    var compositeDisposable = CompositeDisposable()
    val myCalendar: Calendar = Calendar.getInstance()
    lateinit var mViewModel: PersonalDetailsViewModel
    lateinit var binding: FragmentPersonalDetailsBinding
    var mIsoCode: String = ""
    var fromFragment: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = PersonalDetailsViewModel.Factory(
            PersonalDetailsRepository(
                RequestManager.getClient().create(PersonalDetailsService::class.java)
            )
        )

        mViewModel = ViewModelProvider(this, factory).get(PersonalDetailsViewModel::class.java)
        fromFragment = arguments?.getString("fromFragment") ?: ""

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_personal_details, container, false)
        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel



        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clicksView()
        observerEnableButton()
        observerCountryCodeSuccess()
        observerPersonalDetailsSuccess()
        observerNationalitiesSuccess()
        observerTitlesSuccess()
        observerTitlesSuccess1()
        observerNationalitySuccess1()

//        observerUpdateProfileSuccess()
        loadingVisibilityObserver()
        observerUpdateProfileSuccess()

//
//        mViewModel.country.observe(viewLifecycleOwner, Observer {
//
//        })

    }


//    private fun observerUpdateProfileSuccess() {
//
//        mViewModel.mUpdateProfileLiveData.observe(viewLifecycleOwner, Observer {
//
//            AppUtil.showToastySuccess(context, R.string.success)
//
//        })
//
//    }

    private fun observerUpdateProfileSuccess() {

        mViewModel.mSendOtp.observe(viewLifecycleOwner, Observer {

            var t = mViewModel.mPersonalDetailsModelLiveData.value
            if(it)
                findNavController().navigate(
                    R.id.action_navigate_to_Otp,
                    bundleOf("personalDetailsModel" to t,
                        "fromFragment" to fromFragment
                    )
                )

        })

    }

    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }


    private fun observerTitlesSuccess() {

        mViewModel.mTitleListLiveData.observe(viewLifecycleOwner, Observer {

            var titlesList = it.titles
            titlesList?.add(0, TitleResponse.Title("",getString(R.string.title_label)))
            binding.mTitlePersonalSpinner.setSpinnerEntries(titlesList)

        })}

    private fun observerTitlesSuccess1() {

        mViewModel.mTitleCode.observe(viewLifecycleOwner, Observer {

            if(it.isNullOrEmpty()) {
                binding.mTitlePersonalSpinner1.error = ""
            }
            else {
                binding.mTitlePersonalSpinner1.visibility = View.GONE
            }


        })}

    private fun observerNationalitySuccess1() {

        mViewModel.mNationalityCodePersonal.observe(viewLifecycleOwner, Observer {

            if(it.isNullOrEmpty()) {
                binding.mNationalitiesSpinner1.error = ""
            }
            else {
                binding.mNationalitiesSpinner1.visibility = View.GONE
            }


        })}

    private fun observerNationalitiesSuccess() {

        mViewModel.mNationalitiesListLiveData.observe(viewLifecycleOwner, Observer {

            var nationalitiesList = it.nationalities
            nationalitiesList?.add(0, NationalitiesResponse.Nationalities("",getString(R.string.nationality)))
            binding.mNationalitiesSpinner.setSpinnerEntries(nationalitiesList)
        })}

    private fun observerPersonalDetailsSuccess() {

        mViewModel.mPersonalDetailsModelLiveData.observe(viewLifecycleOwner, Observer {
            val personalDetails = it
            personalDetails.defaultAddress?.mobileNumber =
                AppUtil.getMobileNumberWithoutIsoCode(
                    personalDetails.defaultAddress?.mobileNumber ?: ""
                )
            binding.mPersonalDetailsModel = personalDetails
            mViewModel.firstTimeCountrySelected(it?.defaultAddress?.mobileCountry?.isdcode ?: "")


            var countryCode: String? = it.mobileCountry?.isdcode
            if (countryCode.equals(countryCode?.length?.let { it1 ->
                    it.mobileNumber?.length?.minus(it1)?.let { it2 ->
                        it.mobileNumber?.dropLast(
                            it2
                        )
                    }
                })) {
                var mobileNumber = countryCode?.length?.let { it1 -> it.mobileNumber?.drop(it1) }
                binding.textViewMobileNumber.setText(mobileNumber)
            }
        })

        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context, it)
        })


        mViewModel.getNationalitiesConfig()
        if (SharedPreference.getInstance().getNationalityEnabled()) {
            mViewModel.getNationalitiesPersonalDetailsBalance()
        } else {
            binding.mNationalitiesSpinner.visibility = View.GONE
            binding.mNationalitiesTextView.visibility = View.GONE
        }
    }

    private fun observerCountryCodeSuccess() {
        mViewModel.countryCodeList.observe(viewLifecycleOwner, Observer {
            it.countries?.let { it1 ->
                DialogData.getPersonalDetailsCountryCode(
                    context,
                    it1,
                    mViewModel
                )
            }
        })

        mViewModel.selectedCountry.observe(viewLifecycleOwner, Observer {
            binding?.mCodeTextEditText?.setText(it.isdcode)
            mIsoCode = it.isocode.toString()
            mViewModel.mPersonalDetailsModelLiveData.value?.mobileCountry?.isocode = mIsoCode
        })

    }

    private fun observerEnableButton() {
        mViewModel.mEnableButton.observe(viewLifecycleOwner, Observer {
            binding.mUpdateProfileButton.isEnabled = it
        })


    }

    private fun clicksView() {

//        val date =
//            OnDateSetListener { view1, year, monthOfYear, dayOfMonth ->
//                myCalendar[Calendar.YEAR] = year
//                myCalendar[Calendar.MONTH] = monthOfYear
//                myCalendar[Calendar.DAY_OF_MONTH] = dayOfMonth
//                updateLabel()
//            }
//
//        binding.mBirthDateEditText.clicks().subscribe {
//            DatePickerDialog(
//                requireContext(), date, myCalendar[Calendar.YEAR], myCalendar[Calendar.MONTH],
//                myCalendar[Calendar.DAY_OF_MONTH]
//            ).show()
//        }.addTo(compositeDisposable)

        binding.mFemaleButton.clicks().subscribe {
            binding.mFemaleButton.setStrokeWidthResource(R.dimen.stroke_width)
            binding.mFemaleButton.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )

            binding.mMaleButton.setStrokeWidthResource(R.dimen.non_stroke_width)
            binding.mMaleButton.setTextColor(ContextCompat.getColor(requireContext(), R.color.gray))

        }.addTo(compositeDisposable)

        binding.mMaleButton.clicks().subscribe {
            binding.mMaleButton.setStrokeWidthResource(R.dimen.stroke_width)
            binding.mMaleButton.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.colorPrimary
                )
            )
            binding.mFemaleButton.setStrokeWidthResource(R.dimen.non_stroke_width)
            binding.mFemaleButton.setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    R.color.gray
                )
            )

        }.addTo(compositeDisposable)


    }

//    private fun updateLabel() {
//        val myFormat = "MM/dd/yy" //In which you need put here
//        val sdf = SimpleDateFormat(myFormat, Locale.US)
//        binding.mBirthDateEditText.setText(sdf.format(myCalendar.time))
//    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
}