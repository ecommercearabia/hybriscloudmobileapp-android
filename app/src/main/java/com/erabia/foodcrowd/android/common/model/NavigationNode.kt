package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

//@Entity
data class NavigationNode(
    @SerializedName("children")
    @TypeConverters(DataConverter::class)
    val children: List<Children> = listOf(),
    @SerializedName("entries")
    @TypeConverters(DataConverter::class)
    val entries: List<Entry> = listOf(),
    @SerializedName("name")
    val name: String = "",
    @SerializedName("uid")
    val uid: String = "",
    @SerializedName("uuid")
    val uuid: String = ""
)