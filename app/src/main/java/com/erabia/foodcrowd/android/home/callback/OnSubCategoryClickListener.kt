package com.erabia.foodcrowd.android.home.callback

import com.erabia.foodcrowd.android.common.model.Component

interface OnSubCategoryClickListener {
    fun onSubCategoryClick(subCategory: Component)
}
