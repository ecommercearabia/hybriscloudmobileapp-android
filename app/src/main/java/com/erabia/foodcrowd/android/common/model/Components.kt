package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Components")
class Components() {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @SerializedName("component")
    @TypeConverters(DataConverter::class)
    var component: List<Component> = listOf()
}