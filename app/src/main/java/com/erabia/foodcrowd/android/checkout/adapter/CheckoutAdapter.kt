package com.erabia.foodcrowd.android.checkout.adapter

import android.content.Context
import android.text.InputFilter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.navigation.NavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.adapter.BankOfferAdapter
import com.erabia.foodcrowd.android.cart.adapter.PromoCodeAdapter
import com.erabia.foodcrowd.android.checkout.model.CheckoutItemType
import com.erabia.foodcrowd.android.checkout.model.StoreCreditMode
import com.erabia.foodcrowd.android.checkout.model.StoreCreditModes
import com.erabia.foodcrowd.android.checkout.model.TimeSlot
import com.erabia.foodcrowd.android.checkout.util.CheckoutItemManager
import com.erabia.foodcrowd.android.checkout.view.CheckoutFragmentDirections
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.DELIVERY_TIME
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.MY_ITEMS
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.ORDER_SUMMARY
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.PAYMENT_METHOD
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.PROMO_CODE
import com.erabia.foodcrowd.android.common.Constants.CHECKOUT_SECTION.SHIPPING_ADDRESS
import com.erabia.foodcrowd.android.common.customview.DeliveryDateTimeView
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.*
import com.jakewharton.rxbinding3.view.clicks
import java.util.concurrent.TimeUnit


class CheckoutAdapter(
    val viewModel: CheckoutViewModel,
    val onTimeSlotListener: DeliveryDateTimeView.OnTimeSlotListener,
    val navController: NavController

) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    init {
//        Log.v("paymentAdapterRef", "init")
////        viewModel.loadTimeSlot("", "", "")
//    }

    var preSelectPaymentMode: PaymentMode? = null
        set(value) {
            field = value
            paymentAdapter?.preSelectedPaymentMode = value?.code ?: ""
        }


    var preSelectedStoreCredit: StoreCreditMode? = null
        set(value) {
            field = value
            storeCreditAdapter?.preSelectedStoreCredit = value?.storeCreditModeType?.code ?: ""
        }


    var storeCreditVisibility: Int = View.GONE
        set(value) {
            field = value
            paymentMethodBinding?.groupStoreCredit?.visibility = value
        }
    private var itemsAdapter: ItemsAdapter? = null
    private var promoCodeAdapter: PromoCodeAdapter? = null
    private var bankOfferAdapter: BankOfferAdapter? = null
    private var isClearPromoCode: Boolean = false
    private var bankOfferError: Boolean = false
    private var isPaymentLoadedFirstTime: Boolean = true
    private var hasDeliveryItemManagerInitiated = false
    private var deliveryAdapter: DeliveryAdapter? = DeliveryAdapter(viewModel)
    private var paymentAdapter: PaymentAdapter? = PaymentAdapter(viewModel)
    private var storeCreditAdapter: StoreCreditAdapter? = StoreCreditAdapter(viewModel)
    private var deliveryTimeBinding: CheckoutDeliveryTimeRowItemBinding? = null
    private var paymentMethodBinding: CheckoutPaymentMethodRowItemBinding? = null
    private var orderSummaryBinding: CheckoutOrderSummaryRowItemBinding? = null
    private var shippingAddressBinding: CheckoutShippingAddressRowItemBinding? = null
    private var promoCodeSectionRowItemBinding: PromoCodeSectionRowItemBinding? = null
    private var isPaymentReadyToExpand: Boolean = false
    var isStoreCreditLimit: Boolean = false

    var vouchersList: List<AppliedVoucher>? = null
        set(value) {
            field = value
            promoCodeAdapter?.promoCodeList = value
        }
    var isClearPromoCodeText: Boolean = false
        set(value) {
            field = value
            isClearPromoCode = value
        }

    var observeBankOfferError: Boolean = false
        set(value) {
            field = value
            bankOfferError = value
        }

    var timeSlot: TimeSlot? = null
        set(value) {
            field = value
            if (value?.timeSlotDays != null) {
                deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.setTimeSlot(timeSlot?.timeSlotDays)
            }
        }
    var paymentModes: PaymentModes? = null
        set(value) {
            field = value
            paymentAdapter?.paymentModeList = value?.paymentModes
        }

    var deliveryType: String = ""
    var paymentModesRes: setDeliveryTypesResponse? = null
        set(value) {
            field = value
            deliveryType  = value?.deliveryType.toString()
        }

    var deliveryTypes: DeliveryTypes? = null
        set(value) {
            field = value
            deliveryAdapter?.deliveryModeTypeList = value?.deliveryModeTypes
        }

    var storeCreditModes: StoreCreditModes? = null
        set(value) {
            field = value
            storeCreditAdapter?.storeCreditModeList = value?.storeCreditModes
        }

    var storeCreditAmount: StoreCreditAmount? = null
        set(value) {
            field = value
            paymentMethodBinding?.storeCreditAmount = value
        }

    var bankOffer: BankOfferResponse? = null
        set(value) {
            field = value
            bankOfferAdapter?.mBankOfferList = value?.bankOffers
        }


    var cart: Cart? = null
        set(value) {
            field = value
//            setupTimeSlotInfoIfExist(value)
            orderSummaryBinding?.cart = value
            if (cart?.timeSlotInfoDataData?.date?.isNotEmpty() == true) {
                deliveryTimeBinding?.textViewDeliveryTimeDetails?.text =
                    "${cart?.timeSlotInfoDataData?.date} ${cart?.timeSlotInfoDataData?.day} ${cart?.timeSlotInfoDataData?.start}"
                deliveryTimeBinding?.deliveryModeTypesText?.visibility= View.GONE
                deliveryTimeBinding?.recyclerDeliveryModeTypes?.visibility= View.GONE
            }
            paymentMethodBinding?.storeCreditAmount = cart?.storeCreditAmount
//            if (value != null) {
//                addressId = cart?.deliveryAddress?.id ?: ""
//            }
        }
    private var setupTimeSlotInfoFirstTime = true
    private fun setupTimeSlotInfoIfExist(cart: Cart?) {
        if (cart?.timeSlotInfoDataData?.date?.isNotEmpty() == true
            && setupTimeSlotInfoFirstTime
        ) {
            setupTimeSlotInfoFirstTime = false
            deliveryTimeItemManager?.hasDetails = true
            deliveryTimeItemManager?.isHeaderViewExpanded = true
//            deliveryTimeItemManager?.enableNextSection()
            deliveryTimeBinding?.textViewDeliveryTimeDetails?.text =
                "${cart?.timeSlotInfoDataData?.date} ${cart?.timeSlotInfoDataData?.day} ${cart?.timeSlotInfoDataData?.start}"
            isPaymentReadyToExpand = true

        }
    }

    var itemTypeList: List<CheckoutItemType>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var context: Context? = null
    var addressId: String = ""


    private var itemBinding: ViewDataBinding? = null
    private var viewHolder: RecyclerView.ViewHolder? = null

    var paymentMethodItemManager: CheckoutItemManager? =
        CheckoutItemManager(itemType = CheckoutItemType.PAYMENT_METHOD)

    var deliveryTimeItemManager: CheckoutItemManager? =
        CheckoutItemManager(itemType = CheckoutItemType.DELIVERY_TIME)

    init {
        bankOfferAdapter?.setHasStableIds(true)
        itemsAdapter?.setHasStableIds(true)
        paymentAdapter?.setHasStableIds(true)
        storeCreditAdapter?.setHasStableIds(true)
        promoCodeAdapter?.setHasStableIds(true)


        Log.d("", "")
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(context)
        when (viewType) {
            DELIVERY_TIME -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.checkout_delivery_time_row_item,
                    parent,
                    false
                )
                viewHolder =
                    DeliveryTimeViewHolder((itemBinding as? CheckoutDeliveryTimeRowItemBinding?)!!)
            }
            PROMO_CODE -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.promo_code_section_row_item,
                    parent,
                    false
                )

                bankOfferAdapter = BankOfferAdapter()
                promoCodeAdapter = PromoCodeAdapter(viewModel)

                viewHolder =
                    PromoCodeViewHolder((itemBinding as? PromoCodeSectionRowItemBinding?)!!)


            }


            PAYMENT_METHOD -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.checkout_payment_method_row_item,
                    parent,
                    false
                )
                viewHolder =
                    PaymentMethodViewHolder((itemBinding as? CheckoutPaymentMethodRowItemBinding?)!!)
            }

            ORDER_SUMMARY -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.checkout_order_summary_row_item,
                    parent,
                    false
                )

                viewHolder =
                    OrderSummaryViewHolder((itemBinding as? CheckoutOrderSummaryRowItemBinding?)!!)
            }

            SHIPPING_ADDRESS -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.checkout_shipping_address_row_item,
                    parent,
                    false
                )
                viewHolder =
                    ShippingAddressViewHolder((itemBinding as? CheckoutShippingAddressRowItemBinding?)!!)
            }

            MY_ITEMS -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.checkout_my_items_row_item,
                    parent,
                    false
                )
                itemsAdapter = ItemsAdapter(2)

                viewHolder = MyItemsViewHolder((itemBinding as? CheckoutMyItemsRowItemBinding?)!!)
            }
        }
        return viewHolder!!
    }


    override fun getItemCount(): Int {
        return itemTypeList?.size ?: 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            DELIVERY_TIME -> {
                deliveryTimeBinding = (holder as DeliveryTimeViewHolder).itemBinding
                context?.let { holder.bind(deliveryTimeBinding!!, it) }

                deliveryTimeBinding?.recyclerDeliveryModeTypes?.adapter = deliveryAdapter

            }

            PROMO_CODE -> {
                promoCodeSectionRowItemBinding = (holder as PromoCodeViewHolder).itemBinding
                promoCodeSectionRowItemBinding?.checkoutViewModel = viewModel
                (holder as PromoCodeViewHolder).bind(
                    bankOffer?.bankOffers ?: listOf(),
                    vouchersList ?: listOf()
                )

            }
            PAYMENT_METHOD -> {
                val paymentMethodBinding = (holder as PaymentMethodViewHolder).itemBinding
                paymentMethodBinding.viewModel = viewModel
                paymentMethodBinding?.storeCreditAmount = storeCreditAmount
                paymentMethodBinding.groupStoreCredit.visibility = storeCreditVisibility

                if(isStoreCreditLimit){
                    paymentMethodBinding?.textViewStoreCreditLimit?.visibility=View.VISIBLE
                    paymentMethodBinding?.textStoreLimitTitle.text =
                        context?.resources?.getString(R.string.toreCreditLimit_part1) + " " +
                                SharedPreference.getInstance().getStoreCreditLimit() + " " +
                                context?.resources?.getString(R.string.toreCreditLimit_part2)
                } else{
                    paymentMethodBinding?.textViewStoreCreditLimit?.visibility=View.GONE
                }

//                paymentAdapter = PaymentAdapter(viewModel)

//                val itemAnimator = paymentMethodBinding?.recyclerViewPaymentMethod?.itemAnimator
//                if (itemAnimator is SimpleItemAnimator) {
//                    itemAnimator.supportsChangeAnimations = false
//                }
//                val itemAnimator2 = paymentMethodBinding?.recyclerViewStoreCreditMode?.itemAnimator
//                if (itemAnimator2 is SimpleItemAnimator) {
//                    itemAnimator2.supportsChangeAnimations = false
//                }

                paymentMethodBinding.recyclerViewPaymentMethod.itemAnimator = null
                paymentMethodBinding.recyclerViewStoreCreditMode.itemAnimator = null
                paymentMethodBinding.recyclerViewPaymentMethod.adapter = paymentAdapter
                paymentMethodBinding.recyclerViewStoreCreditMode.adapter = storeCreditAdapter
                context?.let { holder.bind(it) }
            }
            ORDER_SUMMARY -> {
                orderSummaryBinding = (holder as OrderSummaryViewHolder).itemBinding
                orderSummaryBinding?.cart = cart
                var shipmentType = SharedPreference.getInstance().getShipmentType();
                if (shipmentType == "1") {
                    orderSummaryBinding?.textViewDeliveryLabel?.visibility = View.GONE
                    orderSummaryBinding?.textViewDelivery?.visibility = View.GONE
                }
            }
            SHIPPING_ADDRESS -> {
                var code = SharedPreference.getInstance().getShipmentType() ?: ""
                if (code == "0") {
                    shippingAddressBinding = (holder as ShippingAddressViewHolder).itemBinding
                    shippingAddressBinding?.address = cart?.deliveryAddress
                    shippingAddressBinding?.textViewMobileNumber?.text =
                        "${cart?.deliveryAddress?.mobileCountry?.isdcode}-${
                            AppUtil.getMobileNumberWithoutIsoCode(
                                cart?.deliveryAddress?.mobileNumber ?: ""
                            )
                        }"
//                shippingAddressBinding.textViewStreetName.text
                    shippingAddressBinding?.checkoutVM = viewModel
                } else {
                    shippingAddressBinding = (holder as ShippingAddressViewHolder).itemBinding
                    shippingAddressBinding?.clShippingAddress?.visibility = View.GONE
                    shippingAddressBinding?.imageViewStart?.visibility = View.GONE
                    shippingAddressBinding?.textViewShippingAddressLabel?.visibility = View.GONE
                    shippingAddressBinding?.constrainLayoutShippingAddress?.visibility = View.GONE
                }
            }
            MY_ITEMS -> {
                (holder as MyItemsViewHolder).bind(cart?.entries ?: listOf())
                (holder as MyItemsViewHolder).itemBinding.textViewAllOrderItems.setOnClickListener {


                    val action = cart?.let { it1 ->
                        CheckoutFragmentDirections.actionCheckoutScreenToAllOrderCheckoutFragment(
                            it1, null
                        )
                    }
////            findNavController().navigate(FragmentDirection,bundle)
                    (holder as MyItemsViewHolder).itemBinding.root.post {
                        action?.let { it1 -> navController.navigate(it1) }
                    }

                }

            }
        }
    }

    //    fun switchPaymentMethod(){
//        paymentAdapter.notifyDataSetChanged()
//    }
    override fun getItemViewType(position: Int): Int {
        return when (itemTypeList?.get(position)) {
            CheckoutItemType.DELIVERY_TIME -> DELIVERY_TIME
            CheckoutItemType.PROMO_CODE -> PROMO_CODE
            CheckoutItemType.PAYMENT_METHOD -> PAYMENT_METHOD
            CheckoutItemType.ORDER_SUMMARY -> ORDER_SUMMARY
            CheckoutItemType.SHIPPING_ADDRESS -> SHIPPING_ADDRESS
            CheckoutItemType.MY_ITEMS -> MY_ITEMS
            else -> -1
        }
    }

    fun showBillingAddressSection(b: Boolean) {
        paymentAdapter?.showBillingAddressSection(b)
    }


    inner class DeliveryTimeViewHolder(val itemBinding: CheckoutDeliveryTimeRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root), CheckoutItemManager.OnExpandedListener {
        var context: Context? = null

        init {
            itemBinding.dateTimeViewDeliveryTimeSlot?.setListener(onTimeSlotListener)
        }

        fun bind(binding: CheckoutDeliveryTimeRowItemBinding, context: Context) {
            this.context = context
//            if (deliveryTimeItemManager == null) {
            deliveryTimeItemManager = context.let { mContext ->
                CheckoutItemManager(
                    mContext,
                    itemBinding.headerViewDeliveryTime,
                    itemBinding.textViewDeliveryTimeDetails,
                    itemBinding.deliveryTimeContainer,
                    CheckoutItemType.DELIVERY_TIME
                )
//                }
            }
            deliveryTimeItemManager?.onExpandedListener = this
            setupTimeSlotInfoIfExist(cart)
            if (timeSlot?.timeSlotDays != null) {
                deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.setTimeSlot(timeSlot?.timeSlotDays)
                checkTime()
            }
            deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.setSelectedTimeSlot(
                cart?.timeSlotInfoDataData?.date,
                cart?.timeSlotInfoDataData?.periodCode
            )
            if(cart?.express == true){
                Log.d("teem adapter", "true")
                deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.visibility = View.GONE
                deliveryTimeBinding?.textViewCheckoutExpressDeliveryText?.visibility = View.GONE
                deliveryTimeBinding?.deliveryModeTypesText?.visibility = View.VISIBLE
                deliveryTimeBinding?.recyclerDeliveryModeTypes?.visibility = View.VISIBLE
            } else{
                Log.d("teem adapter", "false")
                deliveryTimeBinding?.deliveryModeTypesText?.visibility = View.GONE
                deliveryTimeBinding?.recyclerDeliveryModeTypes?.visibility = View.GONE

                if (cart?.cartTimeSlot?.showTimeSlot == true) {
                    if (cart?.cartTimeSlot?.messages?.size!! > 0) {
                        deliveryTimeBinding?.textViewCheckoutExpressDeliveryText?.text =
                            cart?.cartTimeSlot?.messages?.get(0)
                    } else {
                        deliveryTimeBinding?.textViewCheckoutExpressDeliveryText?.visibility = View.GONE
                    }
                    deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.visibility = View.VISIBLE
                } else {
                    if (cart?.cartTimeSlot?.messages?.size!! > 0) {
                        deliveryTimeBinding?.textViewCheckoutExpressDeliveryText?.text =
                            cart?.cartTimeSlot?.messages?.get(0)
                    } else {
                        deliveryTimeBinding?.textViewCheckoutExpressDeliveryText?.visibility = View.GONE
                    }
                    deliveryTimeBinding?.dateTimeViewDeliveryTimeSlot?.visibility = View.GONE
                }
            }


            // init first section only one time

//            if (hasDeliveryItemManagerInitiated && cart?.timeSlotInfoDataData == null) {
//                hasDeliveryItemManagerInitiated = true
//                deliveryTimeItemManager?.expand()
//            }

            if (cart?.timeSlotInfoDataData == null) {
                hasDeliveryItemManagerInitiated = true
                deliveryTimeItemManager?.expand()
            }
        }

        private fun checkTime() {
            for (timeSlotDay in timeSlot?.timeSlotDays!!) {
                if (timeSlotDay.date == cart?.timeSlotInfoDataData?.date ?: "") {
                    for (period in timeSlotDay.periods) {
                        if (period.end == cart?.timeSlotInfoDataData?.end ?: "" && period.enabled) {
                            viewModel.isTimeSlotValid = true
                        }

                    }
                }
            }
        }


        override fun onExpanded() {
//            viewModel.loadTimeSlot("", "", "")
            //TODO  this flag should set on api success 200
//            deliveryTimeItemManager?.isHeaderViewExpanded = true
            deliveryTimeItemManager?.isLoaded = true
//            deliveryTimeItemManager?.enableNextSection()
        }


    }

    inner class PaymentMethodViewHolder(val itemBinding: CheckoutPaymentMethodRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root), CheckoutItemManager.OnExpandedListener {
        var context: Context? = null
        fun bind(context: Context) {
            this.context = context
//            itemBinding.buttonConfirm.setSafeOnClickListener {
//                viewModel.onStoreCreditButtonClick()
//            }

            itemBinding.buttonConfirm.clicks().throttleFirst(2000, TimeUnit.MILLISECONDS)
                .subscribe {
                    viewModel.onStoreCreditButtonClick()
                }


//            if (paymentMethodItemManager == null) {
//                paymentMethodItemManager = context.let { mContext ->
//                    CheckoutItemManager(
//                        context = mContext,
//                        headerView = itemBinding.headerViewPaymentMethod,
//                        detailsContainer = itemBinding.paymentMethodContainer,
//                        itemType = CheckoutItemType.PAYMENT_METHOD
//                    )
//                }
//            }

            paymentMethodItemManager?.setDetailsManager(
                context = context,
                headerView = itemBinding.headerViewPaymentMethod,
                detailsContainer = itemBinding.paymentMethodContainer,
                itemType = CheckoutItemType.PAYMENT_METHOD
            )
            paymentMethodItemManager?.onExpandedListener = this
            paymentMethodItemManager?.expand()

//            if (isPaymentReadyToExpand && isPaymentLoadedFirstTime) {
//                isPaymentLoadedFirstTime = false
//                paymentMethodItemManager?.expand()
//            }
        }

        override fun onExpanded() {
//            viewModel.loadPaymentMode("", "", Constants.FIELDS_FULL)
//            viewModel.loadStoreCreditMode("", "", Constants.FIELDS_FULL)
            paymentMethodItemManager?.isLoaded = true
        }
    }

    inner class OrderSummaryViewHolder(val itemBinding: CheckoutOrderSummaryRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    inner class ShippingAddressViewHolder(val itemBinding: CheckoutShippingAddressRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

    inner class MyItemsViewHolder(val itemBinding: CheckoutMyItemsRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        init {
            itemBinding.recyclerViewItems.adapter = itemsAdapter
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            itemBinding.recyclerViewItems.layoutManager = layoutManager
            itemBinding.recyclerViewItems.isNestedScrollingEnabled = false
            itemBinding.recyclerViewItems.setHasFixedSize(false)

        }


        fun bind(entryList: List<Entry>) {
            itemsAdapter?.entryList = entryList

        }
    }

    inner class PromoCodeViewHolder(val itemBinding: PromoCodeSectionRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        init {

            itemBinding.recyclerViewBankOffer.adapter = bankOfferAdapter
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            itemBinding.recyclerViewBankOffer.layoutManager = layoutManager
            itemBinding.recyclerViewBankOffer.isNestedScrollingEnabled = false
            itemBinding.recyclerViewBankOffer.setHasFixedSize(false)

            itemBinding.recyclerViewPromoCodes.adapter = promoCodeAdapter
            var shipmentType = SharedPreference.getInstance().getShipmentType();
            if (shipmentType == "1") {
                itemBinding.textViewHavePromoCode.visibility = View.GONE
                itemBinding.editTextPromoCode.visibility = View.GONE
                itemBinding.buttonApplay.visibility = View.GONE
                itemBinding.recyclerViewPromoCodes.visibility = View.GONE
            }

        }

        fun bind(
            entryList: List<BankOfferResponse.BankOffer?>,
            vouchersList: List<AppliedVoucher>?
        ) {
            bankOfferAdapter?.mBankOfferList = entryList
            promoCodeAdapter?.promoCodeList = vouchersList
            if (isClearPromoCode) {
                itemBinding?.editTextPromoCode?.text?.clear()
            }
            itemBinding?.editTextPromoCode?.filters = arrayOf<InputFilter>(InputFilter.AllCaps())



            if (bankOfferError) {
                itemBinding?.textViewBankOffer?.visibility = View.GONE
                itemBinding?.recyclerViewBankOffer?.visibility = View.GONE
            }


        }

    }

}