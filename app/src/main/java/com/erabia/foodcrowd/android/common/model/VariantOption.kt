package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class VariantOption(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("priceData")
    val priceData: PriceData = PriceData(),
    @SerializedName("stock")
    val stock: Stock = Stock(),
    @SerializedName("url")
    val url: String = "",
    @SerializedName("variantOptionQualifiers")
    val variantOptionQualifiers: List<VariantOptionQualifier> = listOf()
)