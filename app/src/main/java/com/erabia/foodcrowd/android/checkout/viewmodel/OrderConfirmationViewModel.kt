package com.erabia.foodcrowd.android.checkout.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.util.SharedPreference

class OrderConfirmationViewModel : ViewModel() {


    val seeAllClickEvent: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val continueShoppingEvent: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }

    fun onSeeAllClick() {
        seeAllClickEvent.postValue(0)
    }



    fun onContinueShoppingClick() {

        continueShoppingEvent.postValue(R.id.home)
    }

}