package com.erabia.foodcrowd.android.checkout.model


import com.google.gson.annotations.SerializedName

data class Period(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("enabled")
    val enabled: Boolean = false,
    @SerializedName("end")
    val end: String = "",
    @SerializedName("intervalFormattedValue")
    val intervalFormattedValue: String = "",
    @SerializedName("start")
    val start: String = ""
)