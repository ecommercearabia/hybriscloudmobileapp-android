package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Restriction(
    @SerializedName("description")
    val description: String = "",
    @SerializedName("restrictionType")
    val restrictionType: String = ""
)