package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class AddToCartRequest(
    @SerializedName("basePrice")
    val basePrice: BasePrice? = null,
    @SerializedName("cancellableQuantity")
    val cancellableQuantity: Int? = null,
    @SerializedName("cancelledItemsPrice")
    val cancelledItemsPrice: CancelledItemsPrice? = null,
    @SerializedName("configurationInfos")
    val configurationInfos: List<ConfigurationInfo>? = null,
    @SerializedName("deliveryMode")
    val deliveryMode: DeliveryMode? = null,
    @SerializedName("deliveryPointOfService")
    val deliveryPointOfService: DeliveryPointOfService? = null,
    @SerializedName("entryNumber")
    val entryNumber: Int? = null,
    @SerializedName("product")
    val product: Product? = null,
    @SerializedName("quantity")
    var quantity: Int? = null,
    @SerializedName("quantityAllocated")
    val quantityAllocated: Int? = null,
    @SerializedName("quantityCancelled")
    val quantityCancelled: Int? = null,
    @SerializedName("quantityPending")
    val quantityPending: Int? = null,
    @SerializedName("quantityReturned")
    val quantityReturned: Int? = null,
    @SerializedName("quantityShipped")
    val quantityShipped: Int? = null,
    @SerializedName("quantityUnallocated")
    val quantityUnallocated: Int? = null,
    @SerializedName("returnableQuantity")
    val returnableQuantity: Int? = null,
    @SerializedName("returnedItemsPrice")
    val returnedItemsPrice: ReturnedItemsPrice? = null,
    @SerializedName("totalPrice")
    val totalPrice: TotalPrice? = null,
    @SerializedName("updateable")
    val updateable: Boolean? = null,
    @SerializedName("url")
    val url: String? = null
)