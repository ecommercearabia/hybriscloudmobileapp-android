package com.erabia.foodcrowd.android.category.model

import com.google.gson.annotations.SerializedName

data class CatalogVersion(
    @SerializedName("id")
     val id: String,
    @SerializedName("url")
     val url: String,
    @SerializedName("categories")
     val categories: List<CategoryResponse>? = listOf()
)

