package com.erabia.foodcrowd.android.address.view

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.di.DaggerIAddressComponent
import com.erabia.foodcrowd.android.address.di.IAddressComponent
import com.erabia.foodcrowd.android.address.model.CityList
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.setSpinnerEntries
import com.erabia.foodcrowd.android.common.model.Area
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.databinding.FragmentAddAddressBinding
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import kotlinx.android.synthetic.main.fragment_add_address.*


class AddAddressFragment : Fragment() {
    var mTitleCode: String = ""

    val daggerComponent: IAddressComponent by lazy {
        DaggerIAddressComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)

    }

    var skipTitle: Boolean = true
    var mIndexTitle: Int = 0
    lateinit var mTitleSelected: String

    var titleCodeFromPersonalDetails: String = ""
    var titleCodeFromAddressDetails: String = ""
    var titles: List<TitleResponse.Title>? = listOf()
    val addressViewModel: AddressViewModel by activityViewModels()
    lateinit var binding: FragmentAddAddressBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addressViewModel.initialization(
            daggerComponent.getAddressRepo(),
            daggerComponent.getSignUpRepo()
        )
        arguments?.let { addressViewModel.bundle = it }
        //  Log.d("bundel", arguments?.let { it.getString(Constants.INTENT_KEY.ADDRESS_ID) })
        Log.d(
            "bundel",
            addressViewModel.bundle.getString(Constants.COME_FROM) + addressViewModel.bundle.getString(
                Constants.INTENT_KEY.ADDRESS_ID
            )
        )
        addressViewModel.clearData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_address, container, false)
        binding.addressVM = addressViewModel


        binding.editTextFirstName.requestFocus()
        observeProgressBar()
        observeCountry()
        observeSelectedCountry()
        observeCountryCode()
        observeSelectedCountryCode()
        observeCity()
        observeSelectedCity()
        observeArea()
        observeSelectedArea()
        observeGeoAddress()
        observeSaveButtonEnable()
        observeAddAddress()
        observeNavigation()
        observeError()
        observeAddressDetails()
        observeUpdateAddress()
        observeMap()
        observeBillingAddress()
        observePersonalDetails()
        binding.mTitleSpinner.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {

                override fun onItemSelected(
                    parent: AdapterView<*>, view: View,
                    position: Int, id: Long
                ) {
                    if (addressViewModel.bundle.getString(
                            Constants.INTENT_KEY.ADDRESS_ID
                        ).isNullOrBlank()
                    ) {
                        if (skipTitle) {
                            for (i in 0 until (titles?.size ?: 0) step 1) {

                                if (titles?.get(i)?.code.equals(titleCodeFromPersonalDetails)) {
                                    mIndexTitle = i
                                    addressViewModel.mTitleCode.value =
                                        titles?.get(i)?.code.toString()
                                    skipTitle = false
                                    if (!skipTitle) {
                                        mTitleSpinner.setSelection(mIndexTitle)
                                    }
                                } else {
                                }
                            }
                        } else {
                            addressViewModel.mTitleCode.value =
                                titles?.get(position)?.code.toString()
                        }

                    } else {
                        if (skipTitle) {
                            for (i in 0 until (titles?.size ?: 0) step 1) {

                                if (titles?.get(i)?.code.equals(titleCodeFromAddressDetails)) {
                                    mIndexTitle = i
                                    addressViewModel.mTitleCode.value =
                                        titles?.get(i)?.code.toString()
                                    skipTitle = false
                                    if (!skipTitle) {
                                        mTitleSpinner.setSelection(mIndexTitle)
                                    }
                                } else {
                                }
                            }
                        } else {
                            addressViewModel.mTitleCode.value =
                                titles?.get(position)?.code.toString()
                        }
                    }


                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                }

            }
        addressViewModel.mTitleCode.observe(viewLifecycleOwner, Observer {
            mTitleCode = it
        })

        addressViewModel.mTitleListLiveData.observe(viewLifecycleOwner, Observer {
            titles = it.titles
            binding.mTitleSpinner.setSpinnerEntries(titles)

        })

        return binding.root
    }

    private fun observeSelectedArea() {
        addressViewModel.selectedArea.observe(viewLifecycleOwner, Observer {
            binding.editTextArea.setText(it.name)
        })
    }

    private fun observeArea() {
//        addressViewModel.areaList.observe(viewLifecycleOwner, Observer {
//            DialogData.getAreaDialog(context, addressViewModel, it)
//        })
    }

    private fun observeSelectedCity() {
        addressViewModel.selectedCity.observe(viewLifecycleOwner, Observer {
            binding.editTextCity.setText(it.name)
        })
    }

    private fun observeCity() {
        addressViewModel.cityList.observe(viewLifecycleOwner, Observer {
            it.cities?.let { it1 -> DialogData.getCityDialog(context, addressViewModel, it1) }
        })
    }

    private fun observeMap() {
        addressViewModel.action.observe(this, Observer {
            findNavController().navigate(
                it,
                bundleOf(
                    Constants.COME_FROM to Constants.INTENT_KEY.ADDRESS_FORM,
                    Constants.INTENT_KEY.ADDRESS_ID to addressViewModel.bundle.getString(Constants.INTENT_KEY.ADDRESS_ID)
                )
            )
        })
    }


    private fun observeUpdateAddress() {
        addressViewModel.addressRepository.updateAddressEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context , it)

            findNavController().popBackStack()
        })
    }

    private fun observeAddressDetails() {
        addressViewModel.addressDetails.observe(viewLifecycleOwner, Observer {
            binding.editTextFirstName.setText(it.firstName)
            binding.editTextLastName.setText(it.lastName)
            binding.editTextCountry.setText(it.country?.name)
            binding.editTextCity.setText(it.city?.name)
            binding.editTextArea.setText(it.area?.name)
            binding.editTextPostCode.setText(it.postalCode)
            binding.editTextNearestLandmark.setText(it.nearestLandmark)
            binding.editTextAddressOne.setText(it.line1)
            binding.editTextAddressTow.setText(it.line2)
            binding.editTextCode.setText(it.mobileCountry?.isdcode)
            binding.editTextMobileNum.setText(it.mobileNumber)
            addressViewModel.city = addressViewModel.addressRepository.getCityList(
                it.country?.isocode ?: ""
            ) as SingleLiveEvent<CityList>
            titleCodeFromAddressDetails = it.titleCode.toString()
        })
    }


    private fun observeProgressBar() {
        addressViewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            binding.progressBarAddress.visibility = it
        })
    }


    private fun observeError() {
        addressViewModel.addressRepository.error().observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)

        })
    }

    private fun observeAddAddress() {
        addressViewModel.addressRepository.mAddAdressLiveEvent.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().popBackStack()
            })
    }

    private fun observeBillingAddress() {
        addressViewModel.addressRepository.billingAddressLiveEvent.observe(
            viewLifecycleOwner,
            Observer {
                addressViewModel.setBillingAddress(it)
                findNavController().navigateUp()
                activity?.onBackPressed()

            })
    }

    private fun observeNavigation() {
        addressViewModel.addressRepository.navigateFromAddressToCheckout.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().navigate(it)
            })
    }


    private fun observeSaveButtonEnable() {
        addressViewModel.saveButtonEnable.observe(viewLifecycleOwner, Observer {
            binding.buttonSave.isEnabled = it
        })
    }

    private fun observeSelectedCountryCode() {
        addressViewModel.mobileSelectedCountry.observe(viewLifecycleOwner, Observer {
            binding.editTextCode.setText(it.isdcode)
        })
    }

    private fun observeCountryCode() {
        addressViewModel.mobileCountryCodeList.observe(viewLifecycleOwner, Observer {

//            binding.editTextCode.setText(it.countries?.get(0)?.isdcode.toString())

//            if (it.countries?.size?:0 >1 ){
//                binding.editTextCode.isEnabled=true
            it.countries?.let { it1 -> DialogData.getCountryCode(context, it1, addressViewModel) }
//            }
//            else{
//                binding.editTextCode.isEnabled=false
//
//            }
        })

        addressViewModel.mobilecountry.observe(viewLifecycleOwner, Observer {

            binding.editTextCode.isEnabled = it.countries?.size ?: 0 > 1
            binding.editTextCode.setTextColor(Color.parseColor("#323232"))
            binding.editTextCode.setText(it.countries?.get(0)?.isdcode.toString())
//            addressViewModel.mobileCodeNumber = it.countries?.get(0)?.isocode.toString()

        })


    }


    private fun observePersonalDetails() {
        addressViewModel.personalDetails.observe(viewLifecycleOwner, Observer {

            binding.editTextFirstName.setText(it.firstName)
            binding.editTextLastName.setText(it.lastName)
            titleCodeFromPersonalDetails = it.titleCode.toString()

        })
    }


    private fun observeSelectedCountry() {
        addressViewModel.selectedCountry.observe(viewLifecycleOwner, Observer {
            binding.editTextCountry.setText(it.name)
            addressViewModel.city =
                it.isocode?.let { it1 -> addressViewModel.addressRepository.getCityList(it1) } as SingleLiveEvent<CityList>
        })


    }

    private fun observeCountry() {
        addressViewModel.countryNameList.observe(viewLifecycleOwner, Observer {
            it.countries?.let { it1 -> DialogData.getCountryDialog(context, addressViewModel, it1) }
        })
    }


    private fun observeGeoAddress() {
        addressViewModel.geoAddress.observe(this, Observer {
            it.let {
                  it.adminArea?.let { addressViewModel.area = it }
                //  it.locality?.let { addressViewModel.cityName = it }
                it.getAddressLine(0)?.let { addressViewModel.addressLineOne = it }
                it.postalCode?.let { addressViewModel.postCode = it }
            }

        })
    }


}