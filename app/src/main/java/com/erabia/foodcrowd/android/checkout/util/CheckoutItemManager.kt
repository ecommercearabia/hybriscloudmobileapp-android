package com.erabia.foodcrowd.android.checkout.util

import android.content.Context
import android.view.View
import com.erabia.foodcrowd.android.checkout.model.CheckoutItemType
import com.erabia.foodcrowd.android.checkout.model.Section

/**
 * Manage each item in the checkout
 *
 * @param headerView the headerView for each Item that contains title and icon
 * @param shortDetails the details View is a summary  after finish from the item process (ex after api)
 * @param detailsContainer this view open on title clicked , it should has actions to finish process for current item
 *
 * @author haytham ayyash
 */
class CheckoutItemManager(
    var context: Context? = null,
    var headerView: View? = null,
    var shortDetails: View? = null,
    var detailsContainer: View? = null,
    var itemType: CheckoutItemType = CheckoutItemType.NONE
) {

    fun setDetailsManager(
        context: Context? = null,
        headerView: View? = null,
        shortDetails: View? = null,
        detailsContainer: View? = null,
        itemType: CheckoutItemType
    ) {
        this.context=context
        this.headerView=headerView
        this.detailsContainer=detailsContainer
        this.itemType=itemType
    }

    companion object {
        var errorMessage: String = "Please complete required actions"
        const val TAG = "CheckoutItemManager"
        var itemList = listOf<Section>(
            Section(CheckoutItemType.DELIVERY_TIME, false, "Please Choose Delivery Time"),
            Section(CheckoutItemType.PAYMENT_METHOD, false, "Please Choose Payment Mode")
        )

        fun reset() {
            itemList = listOf<Section>(
                Section(CheckoutItemType.DELIVERY_TIME, false, "Please Choose Delivery Time"),
                Section(CheckoutItemType.PAYMENT_METHOD, false, "Please Choose Payment Mode")
            )
        }

//        fun isItemEnable(itemType: CheckoutItemType): Boolean {
//            return itemList.first { it.itemType == itemType }.isEnabled
//        }

        fun hasItemSetToCart(itemType: CheckoutItemType): Boolean {
            return itemList.first { it.itemType == itemType }.hasSetToCart
        }

        var onButtonCheckoutEnabled: OnButtonCheckoutEnabled? = null

        fun getSection(itemType: CheckoutItemType) =
            itemList.first { it.itemType == itemType }

        fun isCartReadyToCheckout(): Boolean {
            for (section in itemList) {
                if (!section.hasSetToCart) {
                    errorMessage = section.errorMessage
                    return false
                }
            }
            return true
        }
    }

    init {
        headerView?.setOnClickListener {
//            if (isSectionEnabled(itemType)) {
            onHeaderClick()
//            } else {
//                Toast.makeText(context, getSection().errorMessage, Toast.LENGTH_LONG)
//                    .let { it.showToastyError(it, context).show() }
//
//            }
        }
    }


    /**
     *  we have to set this attr to true when GET method loaded, in order to call GET Api only once
     *   onExpand view
     */
    var isLoaded = false

    /**
     * change to true when the section successfully finished to enable and expand next section
     */
//    var isFinished = false
//        set(value) {
//            field = value
//            enableNextSection()
//            preExpandSectionCallback?.preExpandSection()
//        }

    private var isExpanded = false

    var onExpandedListener: OnExpandedListener? = null

    var preExpandSectionCallback: CheckoutItemManager? = null

    var hasDetails: Boolean = false

    var containerVisibility: Int = View.VISIBLE
        set(value) {
            field = value
            detailsContainer?.visibility = value
        }
    var shortDetailsVisibility: Int = View.GONE
        set(value) {
            field = value
            hasDetails = true
            shortDetails?.visibility = value
        }

    var isHeaderViewExpanded = false
        set(value) {
            field = value
//            isHeaderViewExpandedLiveData.postValue(value)
            isExpanded = value
            if (value) {
                containerVisibility = View.VISIBLE
                shortDetailsVisibility = View.GONE
            } else if (hasDetails) {
                containerVisibility = View.GONE
                shortDetailsVisibility = View.VISIBLE
            } else {
                containerVisibility = View.GONE
                shortDetailsVisibility = View.GONE
            }
        }

    /**
     * this method called onHeaderClick when headerView expanded and hasn't details hasDetails = false
     * and isLoaded = false , We can use this method for api call
     */
    fun callApiOnExpanded() {
        onExpandedListener?.onExpanded()
    }

    /**
     * performed always onclick
     */
    fun onClick() {}

    fun onHeaderClick() {
        onClick()
        if (!isExpanded) {
            //become expanded
            expandView()
        } else {
            //become non expanded
            nonExpandView()
        }
    }

    private fun nonExpandView() {
        isExpanded = false
        isHeaderViewExpanded = false
        detailsContainer?.visibility = containerVisibility
        shortDetails?.visibility = shortDetailsVisibility
//        containerVisibilityLiveData.postValue(containerVisibility)
//        detailsVisibilityLiveData.postValue(detailsVisibility)
    }

    private fun expandView() {
        isExpanded = true
        if (shortDetailsVisibility == View.VISIBLE) {
            shortDetailsVisibility = View.GONE
            containerVisibility = View.VISIBLE
            if (!isLoaded) {
                callApiOnExpanded()
            }
        } else {
            if (hasDetails) {
                shortDetailsVisibility = View.VISIBLE
                containerVisibility = View.GONE
            } else {
                shortDetailsVisibility = View.GONE
                containerVisibility = View.VISIBLE
                callApiOnExpanded()
            }
        }
//        containerVisibilityLiveData.postValue(containerVisibility)
//        detailsVisibilityLiveData.postValue(detailsVisibility)
    }

//    @Deprecated(
//        "No need to enable sections sequentially inside checkout",
//        level = DeprecationLevel.WARNING
//    )
//    fun enableNextSection(): Section? {
//        try {
//            val currentSection = getSection()
//            val nextSection = itemList[itemList.indexOf(currentSection) + 1]
//            nextSection.isEnabled = true
//            if (nextSection.itemType == CheckoutItemType.CHECKOUT_BUTTON) {
//                onButtonCheckoutEnabled?.onEnabled()
//            }
//            return nextSection
//        } catch (e: Exception) {
//            Log.d(TAG, e.message ?: "")
//            return null
//        }
//    }

//    fun enableAndExpandNextSection() {
//        val nextSection = enableNextSection()
//        preExpandSectionCallback?.preExpandSection()
//    }

//    private fun isSectionEnabled(itemType: CheckoutItemType): Boolean {
//        val currentSection = getSection()
//        return currentSection.isEnabled
//    }

    fun getSection() = itemList.first { it.itemType == itemType }

    fun expand() {
        if (shortDetailsVisibility == View.GONE) {
            shortDetailsVisibility = View.GONE
            containerVisibility = View.VISIBLE
            if (!isLoaded) {
                callApiOnExpanded()
            }
        }
    }

    interface OnExpandedListener {
        fun onExpanded()
    }

    interface OnButtonCheckoutEnabled {
        fun onEnabled()
    }
    /**
     * expand section when previous section is done
     */
//    interface PreExpandSectionCallback {
//        fun preExpandSection()
//    }
}