package com.erabia.foodcrowd.android.search.viewmodel

import android.os.Handler
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.BreadCrumb
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository

class SearchViewModel(
     val listingRepository: ListingRepository,
    val cartRepository: CartRepository,
    val wishListRepository: WishListRepository
) :
    ListingViewModel(listingRepository, cartRepository) {
    private var query: String = ""
    val itemsVisibility: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val searchImageViewVisibility: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    var mAddWishListLiveData: MutableLiveData<Void> = wishListRepository.mAddWishListLiveData
    val progressBarVisibilityListing: SingleLiveEvent<Int> =
        listingRepository.progressBarVisibility()
    val progressBarVisibilityCart: SingleLiveEvent<Int> = cartRepository.progressBarVisibility()
    val progressBarVisibilityWishlist: SingleLiveEvent<Int> =
        wishListRepository.progressBarVisibility()
    val startLoginEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val loadingVisibility: MutableLiveData<Int> = wishListRepository.loadingVisibility
    val toggleBreadCrumbList: MutableLiveData<List<BreadCrumb>> =
        listingRepository.toggleBreadCrumbList
    val toggleFacetList: MutableLiveData<List<Facet>> = listingRepository.toggleFacetList
    var badgeNumber: SingleLiveEvent<Int> = cartRepository.badgeNumber
    val handler = Handler()
    var runnable: Runnable? = null
    var categoryId: String? = ""
    override fun getProductList(currentPage: Int) {
        super.sort = ""
        super.getProductList(categoryId!!,currentPage, query,true,"search")
    }

    fun addToWishList(productId: String) {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""
        if (userLogin.isNotEmpty()) {
            wishListRepository.getAndAddWishList(productId)
        } else {
            startLoginEvent.postValue(true)
        }
    }
    // remove from wishlist
    fun removeFromWishList(productId: String) {
        wishListRepository.getWishListToDelete(productId)
    }

//TODO implement it with coroutines instead of handler
    fun searchText(): SearchView.OnQueryTextListener {
        return object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(s: String?): Boolean {
                runnable?.let { handler.removeCallbacks(it) }

                runnable = Runnable {
                    resetAdapter.postValue(true)
                    currentPage = 0
                    this@SearchViewModel.query = s ?: ""
                    if (s?.isEmpty() == true) {
                        itemsVisibility.postValue(View.GONE)
                        searchImageViewVisibility.postValue(View.VISIBLE)
                        toggleFacetList.value = listOf()
                    } else {
                        searchImageViewVisibility.postValue(View.GONE)
                        itemsVisibility.postValue(View.VISIBLE)
                    }

                    listingRepository.loadProduct(categoryId!!,0, s ?: "", "",true,"search")

                }
                runnable?.let { handler.postDelayed(it, 600) }
                return false
            }
        }
    }


    @Suppress("UNCHECKED_CAST")
    class Factory(
        private val listingRepository: ListingRepository,
        private val cartRepository: CartRepository,
        private val wishListRepository: WishListRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SearchViewModel(listingRepository, cartRepository, wishListRepository) as T
        }
    }
}