package com.erabia.foodcrowd.android.category.di

import com.erabia.foodcrowd.android.category.remote.repository.CategoryRepository
import com.erabia.foodcrowd.android.category.view.CategoryFragment
import com.erabia.foodcrowd.android.listing.di.ListingModule
import dagger.Component

@Component(modules = [CategoryModule::class , ListingModule::class])
interface ICategoryComponent {

    fun inject (categoryFragment: CategoryFragment)
}