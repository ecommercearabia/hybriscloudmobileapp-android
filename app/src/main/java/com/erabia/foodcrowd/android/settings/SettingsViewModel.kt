package com.erabia.foodcrowd.android.settings

import android.app.Activity
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.menu.model.MenuModel
import com.erabia.foodcrowd.android.menu.model.PreparingMenuModel
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository

class SettingsViewModel(
    val mSettingsRepository: SettingsRepository
) : ViewModel() {

    val loadingVisibility: MutableLiveData<Int> = mSettingsRepository.loadingVisibility
    var updateSignatureLiveData = mSettingsRepository.updateSignatureLiveData

    val mErrorEvent: SingleLiveEvent<String> = mSettingsRepository.error()


    fun updateSignature(deviceId: String) {
        mSettingsRepository.updateSignature(deviceId)
    }


    class Factory(
        val mSettingsRepository: SettingsRepository

    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SettingsViewModel(
                mSettingsRepository
            ) as T
        }
    }
}