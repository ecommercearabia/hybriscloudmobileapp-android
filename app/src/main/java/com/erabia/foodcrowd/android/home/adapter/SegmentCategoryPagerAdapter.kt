package com.erabia.foodcrowd.android.home.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.repository.HighlightedProductRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.home.view.SegmentCategoryFragment
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
import com.erabia.foodcrowd.android.home.viewmodel.SegmentCategoryFragmentViewModel

class SegmentCategoryPagerAdapter(
    val context: Context,
    val homeFragment: HomeFragment?,
    val homeViewModel: HomeViewModel,
    fragmentManager: FragmentManager,
    val homeService: HomeService,
    val homeDao: HomeDao
) : FragmentStatePagerAdapter(
    fragmentManager,
    BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT
) {
    private var isFirstInit: Boolean = true

    /***
     * this custom setter is perform only one time to save fragment state in recyclerView
     */
    var componentList: List<Component>? = null
        set(value) {
            field = value
//            if (isFirstInit) {
            for (i in 0 until (componentList?.size ?: 0)) {
                segmentCategoryFragmentViewModel =
                    SegmentCategoryFragmentViewModel(
                        homeViewModel,
                        homeFragment?.findNavController(),
                        HighLightedProductsViewModel(
                            HighlightedProductRepository(
                                homeService,
                                homeDao
                            )
                        )
                    )
                //TODO add a correct id
                segmentCategoryFragmentList.add(
                    i, SegmentCategoryFragment(
                        homeViewModel,
                        segmentCategoryFragmentViewModel!!,
                            "",
                        homeService,
                        homeDao,
                        componentList?.get(i)
                    )
                )
                isFirstInit = false
                notifyDataSetChanged()
            }
//            }
        }
    var segmentCategoryFragmentViewModel: SegmentCategoryFragmentViewModel? = null
    var segmentCategoryFragmentList = mutableListOf<SegmentCategoryFragment>()

    override fun getItem(position: Int): Fragment {
        if (segmentCategoryFragmentList.size > 0)
            return segmentCategoryFragmentList.get(position)
        else return Fragment()
    }

    override fun getCount(): Int {
        return componentList?.size ?: 0
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return componentList?.get(position)?.name ?: ""
    }

}