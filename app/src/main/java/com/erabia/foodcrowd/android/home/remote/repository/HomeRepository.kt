package com.erabia.foodcrowd.android.home.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.subjects.BehaviorSubject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import retrofit2.Response

class HomeRepository(
    private val homeService: HomeService,
    private val cartService: CartService,
    private val homeDao: HomeDao?
) :
    Repository {

    companion object {
        const val TAG: String = "HomeRepository"
    }

    private val cartRepository = CartRepository(cartService)
    val homeLiveData: MutableLiveData<Home> by lazy { MutableLiveData<Home>() }
    var bannerListSubject: BehaviorSubject<MutableList<Component?>?> = BehaviorSubject.create()
    var tabListSubject: BehaviorSubject<MutableList<Component?>?> = BehaviorSubject.create()
    var bannerSizeSubject: BehaviorSubject<Int> = BehaviorSubject.create()
    val itemLiveData: MutableLiveData<Component> by lazy { MutableLiveData<Component>() }
    val addToCartSuccess: SingleLiveEvent<AddToCartResponse> by lazy { SingleLiveEvent<AddToCartResponse>() }
    val createCartSuccess: BehaviorSubject<Cart> = BehaviorSubject.create()
    val badgeNumber = cartRepository.badgeNumber

    @SuppressLint("CheckResult")
    suspend fun loadHome(pageId: String): LiveData<Home> {
//        progressBarVisibility().value = (View.VISIBLE)
        var home: Home? = homeDao?.getHome()
        homeLiveData?.value = home
        homeService.getHome(pageId).get().subscribe(this, {
            home = it.body()
            homeLiveData?.value = home
            CoroutineScope(IO).launch {
                home?.let { home ->
                    try {
                        homeDao?.deleteHome()
                        homeDao?.deleteNotExistProducts(
                            home.contentSlots.contentSlot.get(0).components.component.get(
                                0
                            ).productCodes.split(" ")
                        )
                        homeDao?.insertHome(home)
                    } catch (e: Exception) {
                        Log.d(TAG, e.message ?: "")
                    }
                }
            }
        })
        return homeLiveData
    }



    fun loadCart(userId: String, cartId: String, fields: String) {
        cartRepository.loadCart(userId, cartId, fields)
    }

    /**
     * insure that the observer before emits data, because there is multiple data will emits in this method
     */
    suspend fun getAllComponents(component: Component?) {
        component?.banners?.split(" ")?.forEach {
            getComponent(it)
        }
    }

    @SuppressLint("CheckResult")
    suspend fun getComponent(componentId: String): LiveData<Component> {
//        val liveData = MutableLiveData<Component>()
//        var comopnent: Component? = comopnentDao?.getComponent()
        var comopnent: Component?
//        liveData.value = comopnent
        homeService.getComponent(componentId).get().subscribe(
            {
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200 -> {
                        comopnent = it.body()
                        itemLiveData.value = comopnent
                        CoroutineScope(IO).launch {
                            comopnent?.let {
//                                comopnentDao?.insertComponent(it)
                            }
                        }
                    }
                    401 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
            {
                Log.d(TAG, " message: ${it.message}")
                Log.d(TAG, " cause: ${it.cause}")

            }
        )
        return itemLiveData
    }


    @SuppressLint("CheckResult")
    suspend fun getBanners(mComponent: Component): Single<MutableList<Component?>?>? {
        val observables = mutableListOf<Observable<Response<Component>>>()
        var banners: MutableList<Component?>? = homeDao?.getBanners() as? MutableList<Component?>
        banners?.let {
            bannerListSubject.onNext(it) }
//        Log.d(TAG, "local banners: ${banners.toString()}")
//        banners?.let { bannerListSubject.onNext(it) }
        val bannersId = mComponent.banners.split(" ")
        bannerSizeSubject.onNext(bannersId.size)
        bannersId.forEach {
            observables.add(homeService.getComponent(it).get())
        }

        val bannerList = Observable.merge(observables).map { it.body()?.apply { componentType = Component.BANNERS } }.toList()
        bannerList.subscribe(
            {
                banners = it
                CoroutineScope(IO).launch {
                    banners?.let {
                        homeDao?.deleteBanners()
                        homeDao?.insertBanners(banners as List<Component>)
                    }
                }
                bannerListSubject.onNext(banners as MutableList<Component?>)
            }, {
                Log.d(TAG, "getAllComponentApi ${it.message}")
            })
        return bannerList
    }

    @SuppressLint("CheckResult")
    suspend fun getTabs(mComponent: Component): Single<MutableList<Component?>?>? {
        val observables = mutableListOf<Observable<Response<Component>>>()
        var tabs: MutableList<Component?>? =
            homeDao?.getTabs() as? MutableList<Component?>
        tabs?.let { tabListSubject.onNext(it) }
//        Log.d(TAG, "local tabs: ${tabs.toString()}")
//        tabs?.let { tabListSubject.onNext(it) }
        val tabsId = mComponent.productCarouselComponents.split(" ")
        tabsId.forEach {
            observables.add(homeService.getComponent(it).get())
        }
        val tabList =
            Observable.merge(observables).map { it.body()?.apply { componentType = Component.TAB } }
                .toList()
        tabList.subscribe({
            tabs = it
            CoroutineScope(IO).launch {
                homeDao?.deleteTabs()
                homeDao?.insertTabs(tabs as List<Component>)
            }
            tabListSubject.onNext(tabs as MutableList<Component?>)
        }, {
            Log.d(TAG, "getAllComponentApi ${it.message}")
        })
        return tabList
    }

    fun addToCart(userId: String, cartId: String, entry: AddToCartRequest, fields: String) {
        cartRepository.addToCart(userId, cartId, entry, fields)

//        cartService.addToCart(userId, cartId, entry, fields).get().subscribe(this, onSuccess_200 = {
//            addToCartSuccess.value = it.body()
//        },onError_400 = {
//            if (getResponseErrorMessage(it).contains("No cart created yet.")){
//                createCart(userId,null,null,"FULL")
//                cartService?.addToCart(userId, cartId, entry,fields).get().subscribe(this, {
//                    addToCartSuccess.value = it.body()
//                })
//            }
//        })
    }

    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String
    ): BehaviorSubject<Cart> {
        cartService.createCart(userId, oldCartId, toMergeCartGuid, fields).get()
            .subscribe(this, onSuccess_200 = {
                createCartSuccess.onNext(it.body() ?: Cart())
            }, onSuccess_201 = {
                createCartSuccess.onNext(it.body() ?: Cart())
            })
        return createCartSuccess
    }

    @SuppressLint("CheckResult")
    fun setFirebaseToken(
        userId: String,
        mobileToken: String
    ) {
       homeService.setFirebaseToken(userId , mobileToken).get().subscribe(
           {
               when (it.code()) {
                   200 -> {

                   }
                   401 -> {
//                       error().postValue(getResponseErrorMessage(it))
                   }
                   else -> {
//                       error().postValue(getResponseErrorMessage(it))
                   }
               }
           },
           {
               Log.d(TAG, " message: ${it.message}")
               Log.d(TAG, " cause: ${it.cause}")

           }
       )
    }


}
