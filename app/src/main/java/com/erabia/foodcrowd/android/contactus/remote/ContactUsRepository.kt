package com.erabia.foodcrowd.android.contactus.remote

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.checkout.model.*
import com.erabia.foodcrowd.android.checkout.model.PaymentInfo
import com.erabia.foodcrowd.android.checkout.remote.service.CheckoutService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response
import java.lang.Exception

class ContactUsRepository(
    private val contactUsService: ContactUsService

) : Repository {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mContactUsResponseLiveData: SingleLiveEvent<ContactUsResponse> by lazy { SingleLiveEvent<ContactUsResponse>() }

    companion object {
        const val TAG = "ContactUsRepository"
    }

    @SuppressLint("CheckResult")
    fun getContactUsDetails() {
        contactUsService.getContactUsDetails(
            "ContactUsTextParagraph"
            , "FULL"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201, 202 -> {
                            mContactUsResponseLiveData.value = it.body()
                        }

                        400 -> {
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
    }

}