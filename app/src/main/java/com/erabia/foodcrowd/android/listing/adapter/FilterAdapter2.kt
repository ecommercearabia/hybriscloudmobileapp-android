package com.erabia.foodcrowd.android.listing.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Value
import kotlinx.android.synthetic.main.item_filter_content.view.*
import kotlinx.android.synthetic.main.item_filter_header.view.*
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

class FilterAdapter2(
    private var filerCheckListener: IFilterCheckCallBack
) :
    RecyclerView.Adapter<FilterAdapter2.ViewHolder>() {

    var filterAndBreadCrumbList: Facet? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    companion object {
        private const val VIEW_TYPE_ITEM = 1
        private const val VIEW_TYPE_HEADER = 2
        private const val IC_EXPANDED_ROTATION_DEG = 0F
        private const val IC_COLLAPSED_ROTATION_DEG = 180F
    }

    var isExpanded: Boolean by Delegates.observable(false) { _: KProperty<*>, _: Boolean, newExpandedValue: Boolean ->
        if (newExpandedValue) {
            filterAndBreadCrumbList?.values?.let {

                notifyItemRangeInserted(1, it.size)

            }
            //To update the header expand icon
            notifyItemChanged(0)

        } else {
            filterAndBreadCrumbList?.values?.let {

                notifyItemRangeRemoved(1, it.size)
            }
            // To update the header expand icon
            notifyItemChanged(0)
        }

    }

    private val onHeaderClickListener = View.OnClickListener {
        isExpanded = !isExpanded
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) VIEW_TYPE_HEADER else VIEW_TYPE_ITEM
    }

    override fun getItemCount(): Int {

        return if (isExpanded) {
            (filterAndBreadCrumbList?.values?.count() ?: 0) + 1
        } else {
            if (filterAndBreadCrumbList != null) {
                1
            } else {
                0
            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            VIEW_TYPE_HEADER -> ViewHolder.HeaderVH(
                inflater.inflate(
                    R.layout.item_filter_header,
                    parent,
                    false
                )
            )
            else -> ViewHolder.ItemVH(
                inflater.inflate(
                    R.layout.item_filter_content,
                    parent,
                    false
                )
            )
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder.ItemVH -> {


                holder.bind(
                    filterAndBreadCrumbList?.values?.get(position - 1),
                    filerCheckListener
                )
            }
            is ViewHolder.HeaderVH -> {
                holder.bind(
                    filterAndBreadCrumbList?.name.toString(),
                    isExpanded,
                    onHeaderClickListener
                )
            }
        }
    }


    override fun getItemId(position: Int): Long {
//        return filterAndBreadCrumbList?.value?.get(position -1)?.name?.hashCode()?.toLong() ?: 0
        return position.toLong()
    }

    sealed class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        class ItemVH(itemView: View) : ViewHolder(itemView) {

            fun bind(
                breadCrumb: Value?,
                filterCheck: IFilterCheckCallBack
            ) {

                itemView.mTitleValueCheckBox.isChecked = breadCrumb?.selected ?: false

                itemView.apply {
                    itemView.mTitleValueCheckBox.text = breadCrumb?.name
                }

                itemView.mTitleValueCheckBox.setOnClickListener {
                    filterCheck.onCheckedFilter(
                        breadCrumb
                    )
                }

            }
        }

        class HeaderVH(itemView: View) : ViewHolder(itemView) {
            private val tvTitle = itemView.mTitleFilterTextView
            internal val icExpand = itemView.mExpendImageView

            fun bind(
                content: String,
                expanded: Boolean,
                onClickListener: View.OnClickListener
            ) {
                tvTitle.text = content
                icExpand.rotation =
                    if (expanded)
                        IC_EXPANDED_ROTATION_DEG
                    else
                        IC_COLLAPSED_ROTATION_DEG
                itemView.setOnClickListener(onClickListener)
            }
        }
    }
}


