package com.erabia.foodcrowd.android.common.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.home.db.HomeDao

@Database(entities = [Home::class, Product::class, Component::class], version = 14)
abstract class DataBaseManager : RoomDatabase() {
    companion object {
        private var instance: DataBaseManager? = null

        @Synchronized
        fun getInstance(context: Context): DataBaseManager {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    DataBaseManager::class.java, "crowdFood.db"
                ).fallbackToDestructiveMigration().build()
            }
            return instance as DataBaseManager
        }
    }

    abstract fun homeDao(): HomeDao


}