package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import com.google.gson.annotations.SerializedName

data class Tablet(
    @SerializedName("altText")
    val altText: String = "",
    @SerializedName("catalogId")
    val catalogId: String = "",
    @SerializedName("code")
    val code: String = "",
    @SerializedName("downloadUrl")
    val downloadUrl: String = "",
    @SerializedName("mime")
    val mime: String = "",
    @SerializedName("url")
    val url: String = ""
)