package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class BaseOption(
    @SerializedName("options")
    @TypeConverters(DataConverter::class)
    val options: List<Option> = listOf(),
    @SerializedName("selected")
    val selected: Selected = Selected(),
    @SerializedName("variantType")
    val variantType: String = ""
)