package com.erabia.foodcrowd.android.common.model
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateShipmentTypes {
   @SerializedName("cartModifications")
   @Expose
   var cartModifications: List<CartModification>? = listOf()
}

 data class CartModification(
    @SerializedName("entry")
    @Expose
    var entry: ShipmentEntry? = ShipmentEntry(),
    @SerializedName("quantity")
    @Expose
    var quantity: Int? = 0,
    @SerializedName("quantityAdded")
    @Expose
    var quantityAdded: Int? = 0,
    @SerializedName("statusCode")
    @Expose
    var statusCode: String? = ""
)

 data class ShipmentEntry(
    @SerializedName("basePrice")
    @Expose
    var basePrice: ShipmentBasePrice? = ShipmentBasePrice(),
    @SerializedName("configurationInfos")
    @Expose
    var configurationInfos: List<Any>? = listOf(),
    @SerializedName("deliveryPointOfService")
    @Expose
    var deliveryPointOfService: ShipmentDeliveryPointOfService? = ShipmentDeliveryPointOfService(),
    @SerializedName("entryNumber")
    @Expose
    var entryNumber: Int? = 0,
    @SerializedName("product")
    @Expose
    var product: ShipmentProduct? = ShipmentProduct(),
    @SerializedName("quantity")
    @Expose
    var quantity: Int? = 0,
    @SerializedName("totalPrice")
    @Expose
    var totalPrice: ShipmentTotalPrice? = ShipmentTotalPrice(),
    @SerializedName("updateable")
    @Expose
    var updateable: Boolean? = false
)

 data class ShipmentBasePrice(
    @SerializedName("currencyIso")
    @Expose
    var currencyIso: String? = "",
    @SerializedName("formattedValue")
    @Expose
    var formattedValue: String? = "",
    @SerializedName("priceType")
    @Expose
    var priceType: String? = "",
    @SerializedName("value")
    @Expose
    var value: Double? = 0.0
)

 data class ShipmentDeliveryPointOfService(
    @SerializedName("address")
    @Expose
    var address: ShipmentAddress? = ShipmentAddress(),
    @SerializedName("description")
    @Expose
    var description: String? = "",
    @SerializedName("displayName")
    @Expose
    var displayName: String? = "",
    @SerializedName("features")
    @Expose
    var features: ShipmentFeatures? = ShipmentFeatures(),
    @SerializedName("geoPoint")
    @Expose
    var geoPoint: ShipmentGeoPoint? = ShipmentGeoPoint(),
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("openingHours")
    @Expose
    var openingHours: ShipmentOpeningHours? = ShipmentOpeningHours(),
    @SerializedName("storeContent")
    @Expose
    var storeContent: String? = "",
    @SerializedName("storeImages")
    @Expose
    var storeImages: List<Any>? = listOf()
)

 data class ShipmentProduct(
    @SerializedName("availableForPickup")
    @Expose
    var availableForPickup: Boolean? = false,
    @SerializedName("nutritionFacts")
    var nutritionFacts: String? = "",
    @SerializedName("baseOptions")
    @Expose
    var baseOptions: List<ShipmentBaseOption>? = listOf(),
    @SerializedName("baseProduct")
    @Expose
    var baseProduct: String? = "",
    @SerializedName("categories")
    @Expose
    var categories: List<ShipmentCategory>? = listOf(),
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("configurable")
    @Expose
    var configurable: Boolean? = false,
    @SerializedName("countryOfOrigin")
    @Expose
    var countryOfOrigin: String? = "",
    @SerializedName("countryOfOriginIsocode")
    @Expose
    var countryOfOriginIsocode: String? = "",
    @SerializedName("glutenFree")
    @Expose
    var glutenFree: Boolean? = false,
    @SerializedName("images")
    @Expose
    var images: List<ImageX>? = listOf(),
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("organic")
    @Expose
    var organic: Boolean? = false,
    @SerializedName("purchasable")
    @Expose
    var purchasable: Boolean? = false,
    @SerializedName("stock")
    @Expose
    var stock: ShipmentSecStock? = ShipmentSecStock(),
    @SerializedName("unitOfMeasure")
    @Expose
    var unitOfMeasure: String? = "",
    @SerializedName("url")
    @Expose
    var url: String? = "",
    @SerializedName("vegan")
    @Expose
    var vegan: Boolean? = false
)

 data class ShipmentTotalPrice(
    @SerializedName("currencyIso")
    @Expose
    var currencyIso: String? = "",
    @SerializedName("formattedValue")
    @Expose
    var formattedValue: String? = "",
    @SerializedName("priceType")
    @Expose
    var priceType: String? = "",
    @SerializedName("value")
    @Expose
    var value: Double? = 0.0
)

 data class ShipmentAddress(
    @SerializedName("area")
    @Expose
    var area: ShipmentArea? = ShipmentArea(),
    @SerializedName("buildingName")
    @Expose
    var buildingName: String? = "",
    @SerializedName("city")
    @Expose
    var city: ShipmentCity? = ShipmentCity(),
    @SerializedName("country")
    @Expose
    var country: ShipmentCountry? = ShipmentCountry(),
    @SerializedName("defaultAddress")
    @Expose
    var defaultAddress: Boolean? = false,
    @SerializedName("formattedAddress")
    @Expose
    var formattedAddress: String? = "",
    @SerializedName("id")
    @Expose
    var id: String? = "",
    @SerializedName("line1")
    @Expose
    var line1: String? = "",
    @SerializedName("line2")
    @Expose
    var line2: String? = "",
    @SerializedName("phone")
    @Expose
    var phone: String? = "",
    @SerializedName("shippingAddress")
    @Expose
    var shippingAddress: Boolean? = false,
    @SerializedName("title")
    @Expose
    var title: String? = "",
    @SerializedName("titleCode")
    @Expose
    var titleCode: String? = "",
    @SerializedName("town")
    @Expose
    var town: String? = "",
    @SerializedName("visibleInAddressBook")
    @Expose
    var visibleInAddressBook: Boolean? = false
)

 data class ShipmentFeatures(
    @SerializedName("entry")
    @Expose
    var entry: List<EntryX>? = listOf()
)

 data class ShipmentGeoPoint(
    @SerializedName("latitude")
    @Expose
    var latitude: Double? = 0.0,
    @SerializedName("longitude")
    @Expose
    var longitude: Double? = 0.0
)

 data class ShipmentOpeningHours(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("specialDayOpeningList")
    @Expose
    var specialDayOpeningList: List<Any>? = listOf(),
    @SerializedName("weekDayOpeningList")
    @Expose
    var weekDayOpeningList: List<ShipmentWeekDayOpening>? = listOf()
)

 data class ShipmentArea(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = ""
)

 data class ShipmentCity(
    @SerializedName("areas")
    @Expose
    var areas: List<AreaX>? = listOf(),
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = ""
)

 data class ShipmentCountry(
    @SerializedName("isocode")
    @Expose
    var isocode: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = ""
)

 data class AreaX(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = ""
)

 data class EntryX(
    @SerializedName("key")
    @Expose
    var key: String? = ""
)

 data class ShipmentWeekDayOpening(
    @SerializedName("closed")
    @Expose
    var closed: Boolean? = false,
    @SerializedName("closingTime")
    @Expose
    var closingTime: ShipmentClosingTime? = ShipmentClosingTime(),
    @SerializedName("openingTime")
    @Expose
    var openingTime: ShipmentOpeningTime? = ShipmentOpeningTime(),
    @SerializedName("weekDay")
    @Expose
    var weekDay: String? = ""
)

 data class ShipmentClosingTime(
    @SerializedName("formattedHour")
    @Expose
    var formattedHour: String? = "",
    @SerializedName("hour")
    @Expose
    var hour: Int? = 0,
    @SerializedName("minute")
    @Expose
    var minute: Int? = 0
)

 data class ShipmentOpeningTime(
    @SerializedName("formattedHour")
    @Expose
    var formattedHour: String? = "",
    @SerializedName("hour")
    @Expose
    var hour: Int? = 0,
    @SerializedName("minute")
    @Expose
    var minute: Int? = 0
)

 data class ShipmentBaseOption(
    @SerializedName("selected")
    @Expose
    var selected: ShipmentSelected? = ShipmentSelected(),
    @SerializedName("variantType")
    @Expose
    var variantType: String? = ""
)

 data class ShipmentCategory(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("image")
    @Expose
    var image: ShipmentImage? = ShipmentImage(),
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("url")
    @Expose
    var url: String? = ""
)

 data class ImageX(
    @SerializedName("altText")
    @Expose
    var altText: String? = "",
    @SerializedName("format")
    @Expose
    var format: String? = "",
    @SerializedName("imageType")
    @Expose
    var imageType: String? = "",
    @SerializedName("url")
    @Expose
    var url: String? = ""
)

 data class ShipmentSecStock(
    @SerializedName("stockLevel")
    @Expose
    var stockLevel: Int? = 0,
    @SerializedName("stockLevelStatus")
    @Expose
    var stockLevelStatus: String? = ""
)

 data class ShipmentSelected(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("priceData")
    @Expose
    var priceData: ShipmentPriceData? = ShipmentPriceData(),
    @SerializedName("stock")
    @Expose
    var stock: ShipmentStock? = ShipmentStock(),
    @SerializedName("url")
    @Expose
    var url: String? = "",
    @SerializedName("variantOptionQualifiers")
    @Expose
    var variantOptionQualifiers: List<ShipmentVariantOptionQualifier>? = listOf()
)

 data class ShipmentPriceData(
    @SerializedName("currencyIso")
    @Expose
    var currencyIso: String? = "",
    @SerializedName("formattedValue")
    @Expose
    var formattedValue: String? = "",
    @SerializedName("priceType")
    @Expose
    var priceType: String? = "",
    @SerializedName("value")
    @Expose
    var value: Double? = 0.0
)

 data class ShipmentStock(
    @SerializedName("stockLevel")
    @Expose
    var stockLevel: Int? = 0,
    @SerializedName("stockLevelStatus")
    @Expose
    var stockLevelStatus: String? = ""
)

 data class ShipmentVariantOptionQualifier(
    @SerializedName("name")
    @Expose
    var name: String? = "",
    @SerializedName("qualifier")
    @Expose
    var qualifier: String? = "",
    @SerializedName("value")
    @Expose
    var value: String? = ""
)

 data class ShipmentImage(
    @SerializedName("format")
    @Expose
    var format: String? = "",
    @SerializedName("url")
    @Expose
    var url: String? = ""
)