package com.erabia.foodcrowd.android.listing.di

import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.view.FilterFragment
import com.erabia.foodcrowd.android.listing.view.ListingFragment
import dagger.Component

@Component(modules = [ListingModule::class])
interface IListingComponent {

    fun inject(listFragment: ListingFragment)

    fun inject(filterFragment: FilterFragment)

    fun getListingRepository(): ListingRepository
}