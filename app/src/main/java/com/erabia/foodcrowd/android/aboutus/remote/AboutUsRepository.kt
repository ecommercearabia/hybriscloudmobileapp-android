package com.erabia.foodcrowd.android.aboutus.remote

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse
import io.reactivex.rxkotlin.subscribeBy


class AboutUsRepository(
        private val aboutUsService: AboutUsService

) : Repository {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mAboutUsResponseLiveData: SingleLiveEvent<ContactUsResponse> by lazy { SingleLiveEvent<ContactUsResponse>() }

    companion object {
        const val TAG = "AboutUsRepository"
    }

    @SuppressLint("CheckResult")
    fun getAboutUsDetails() {
        aboutUsService.getAboutUsServiceDetails(
                "AboutUsTextParagraph"
                , "FULL"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
                .doOnTerminate { loadingVisibility.value = View.GONE }
                .subscribeBy(
                        onNext =
                        {
                            when (it.code()) {
                                200, 201, 202 -> {
                                    mAboutUsResponseLiveData.value = it.body()
                                }

                                400 -> {
                                    error().postValue(getResponseErrorMessage(it))

                                }
                                else -> {
                                    error().postValue(getResponseErrorMessage(it))
                                }
                            }
                        }
                        ,
                        onError =
                        {
                            Log.d(TAG, it.toString())
                        }
                )
    }

}