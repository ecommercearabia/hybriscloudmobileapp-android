package com.erabia.foodcrowd.android.category.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.category.model.CategoryResponse
import com.erabia.foodcrowd.android.category.model.Subcategory
import com.erabia.foodcrowd.android.category.remote.service.CategoryService
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.repository.Repository

class CategoryRepository(val categoryService: CategoryService) : Repository {
    companion object {
        const val TAG: String = "CategoryRepository"
    }


    @SuppressLint("CheckResult")
    fun loadCategory(): LiveData<CategoryResponse> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = MutableLiveData<CategoryResponse>()
        categoryService.getCategory(
            "foodcrowdProductCatalog", "Online",
            "1"
        ).get()
            .subscribe(
                {
                    liveData.value = it.body()
                    progressBarVisibility().value = (View.GONE)
                }
                ,
                {
                    Log.d(CategoryRepository.TAG, it.toString())
                }
            )
        return liveData
    }

    @SuppressLint("CheckResult")
    fun loadSubCategory(id: String): LiveData<Subcategory> {
        progressBarVisibility().value = (View.VISIBLE)
        var liveData = MutableLiveData<Subcategory>()
        categoryService.getSubCategory(
            "foodcrowdProductCatalog", "Online", id).get()
            .subscribe(
                {
                    liveData.value = it.body()
                    error().postValue("test error messages")
                    progressBarVisibility().value = (View.GONE)
                }
                ,
                {
                    Log.d(CategoryRepository.TAG, it.toString())
                }
            )
        return liveData
    }
}