package com.erabia.foodcrowd.android.home.remote.repository

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HighlightedProductRepository(
    private val homeService: HomeService,
    private val homeDao: HomeDao
) : Repository {
    val productLiveData: MutableLiveData<Product> by lazy { MutableLiveData<Product>() }

    @SuppressLint("CheckResult")
    suspend fun getProduct(id: String): LiveData<Product> {
        var product: Product? = homeDao?.getProduct(id)
        productLiveData.value = product
        homeService.getProduct(id, Constants.FIELDS_FULL).get().subscribe(this, {
            product = it.body()
            productLiveData.value = product
            CoroutineScope(Dispatchers.IO).launch { product?.let { homeDao?.insertProduct(it) } }
        }, doOnSubscribe = {}, onError_400 = {} )
        return productLiveData
    }
}