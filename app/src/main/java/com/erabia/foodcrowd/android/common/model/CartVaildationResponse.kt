package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class CartVaildationResponse(
    @SerializedName("errors")
    var errors: List<String?>? = listOf(),
    @SerializedName("valid")
    var valid: Boolean? = false
)