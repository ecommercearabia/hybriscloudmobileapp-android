package com.erabia.foodcrowd.android.checkout.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.clickWithDebounce
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.common.model.PaymentMode
import com.erabia.foodcrowd.android.databinding.RowPaymentMethodBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class PaymentAdapter(val viewModel: CheckoutViewModel) :
    RecyclerView.Adapter<PaymentAdapter.MyViewHolder>() {
    private var paymentModeSelected: PaymentMode? = null
    var binding: RowPaymentMethodBinding? = null
    var showBillingAddressSection = false

    //preSelect paymentMode from cart repsonse if exist
    var preSelectedPaymentMode: String = ""
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var paymentModeList: List<PaymentMode>? = null
        set(value) {
            field = value
            selectedPosition = -1
            notifyDataSetChanged()
        }
    lateinit var context: Context
    var selectedPosition = -1
        set(value) {
            field = value
            if (paymentModeList?.size ?: 0 > 0) {
//                paymentItemViewModel?.paymentMethod =
//                    paymentModeList?.get(selectedPosition)?.code ?: ""
//                parentViewModel?.paymentMethod = paymentModeList?.get(selectedPosition)?.code ?: ""
//                paymentItemViewModel?.isHeaderViewExpanded = true
            }
//            notifyDataSetChanged()
        }
    var paymentCode: String? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_payment_method,
            parent,
            false
        )
        return MyViewHolder(binding!!)
    }

    override fun getItemCount(): Int {
        return paymentModeList?.size ?: 0
    }

    @SuppressLint("CheckResult")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val paymentMode = paymentModeList?.get(position)
        holder.itemBinding.checkoutVM = viewModel
        //PreSelected from cart api response
        if (preSelectedPaymentMode == paymentMode?.code) {
            holder.itemBinding.radioButton.isChecked = preSelectedPaymentMode == paymentMode?.code
            selectedPosition = position

//            notifyDataSetChanged()
        }

        if (paymentModeList?.size == 1) {

        }
        if (position == 0) {
            holder.itemBinding.separatorLine.visibility = View.INVISIBLE
        }


        //TODO remove this condition
//        if (paymentMode?.code == "card")
//            holder.itemBinding.radioButton.isEnabled = false

        holder.itemBinding.paymentMode = paymentMode

        holder.itemBinding.radioButton.isChecked = selectedPosition == position

//        if (paymentModeSelected?.code == "card") {
//            viewModel.loadPaymentInfo(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
//        }

        if (paymentMode?.code == "cod") {
            holder.itemBinding.imageView.background = context.getDrawable(R.drawable.money)
        }
        if (paymentMode?.code == "ccod") {
            holder.itemBinding.imageView.background = context.getDrawable(R.drawable.ccod)
        }
        if (paymentMode?.code == "card") {
            holder.itemBinding.imageView.background = context.getDrawable(R.drawable.card)
//            holder.itemBinding?.billingAddressContainer?.visibility = View.VISIBLE
//                parentViewModel?.billingAddress = billingAddressModel
//            holder.itemBinding.billingAddressContainer.visibility =
//                if (showBillingAddressSection) View.VISIBLE else View.GONE
//
//            holder.itemBinding.groupSwitch.visibility = View.VISIBLE
//            Log.v("billingAddress - onbind", holder.itemBinding.billingAddressContainer.toString())
        }


        //      holder.itemBinding.switchUseSameDeliveryAddress.setOnCheckedChangeListener { compoundButton, isChecked ->
//            if (!isChecked) {
//                //TODO navigate to address form
//                viewModel.isBillingAddress = true
//            } else {
//                viewModel.isBillingAddress = false
//            }
//        }

        holder.itemBinding.radioButton.setOnClickListener {
//                Handler(Looper.getMainLooper()).postDelayed({
//                    //Do something after 100ms
//                }, 1000)
            viewModel.setPaymentMode(
                "", "",
                paymentModeList?.get(position)?.code ?: "",
                Constants.FIELDS_FULL
            )
            selectedPosition = position
            paymentModeSelected = paymentModeList?.get(position)
            preSelectedPaymentMode = ""
            paymentCode = ""
            notifyDataSetChanged()

        }


    }

    fun resetData() {
        binding?.radioButton?.isChecked = false
    }

    override fun getItemId(position: Int): Long {
        return paymentModeList?.get(position)?.code.hashCode().toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun showBillingAddressSection(b: Boolean) {
        showBillingAddressSection = b
        notifyDataSetChanged()
//        binding?.billingAddressContainer?.visibility = if (b) View.VISIBLE else View.GONE
    }

    class MyViewHolder(var itemBinding: RowPaymentMethodBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

}