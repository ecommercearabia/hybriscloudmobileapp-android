package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class VariantValueCategory(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("sequence")
    val sequence: Int = 0,
    @SerializedName("superCategories")
    val superCategories: List<SuperCategory> = listOf()
)