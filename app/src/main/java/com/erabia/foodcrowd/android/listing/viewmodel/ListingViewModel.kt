package com.erabia.foodcrowd.android.listing.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.AddToCartRequest
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.listing.model.Listing
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

open class ListingViewModel(
    private val listingRepository: ListingRepository,
    private val cartRepository: CartRepository
) : ViewModel() {

    companion object {
        private const val TAG: String = "ListingViewModel"
    }

    var currentPage = 0
    var productList = listingRepository.productListLiveData
    var productListSearch: MutableLiveData<Listing> = listingRepository.productListSearchLiveData
    var progressBar: SingleLiveEvent<Int> = listingRepository.progressBarVisibility()
    var errorObserver: SingleLiveEvent<String> = listingRepository.error()
    var filterQuery = ""
    var mQuantityAdd: Int = 0
    var mQuantityMinus: Int = 0
    var mSumQuantity: Int = 0
    var query2: String = ""
    var sort: String = ""
    var filter: IFilter? = null
    private val compositeDisposable = CompositeDisposable()
    val resetAdapter: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    val addToCartSuccess = cartRepository.addToCartSuccess

    fun getProductList(
        category: String,
        currentPage: Int,
        query: String,
//        sort: String,
        isShowLoading: Boolean,
        fromFragment: String
    ) {
        if (currentPage == 0) {
            resetAdapter.postValue(true)
        }
        val finalQuery = if (query.contains("::")) {
            query.replace("::", ":")
        }else{
            query
        }

//        productList =
        listingRepository.loadProduct(
            category,
            currentPage,
            finalQuery,
            sort,
            isShowLoading,
            fromFragment
        )// as SingleLiveEvent<Listing>

    }


    interface IFilter {
        fun filter(query: String)
    }


    fun getQueryFromCategory(): String {
        return query2
    }


    fun addQuantity() {
        mSumQuantity += 1
    }

    fun minusQuantity() {
        if (mSumQuantity > 0) {
            mSumQuantity -= 1
        } else {
            mSumQuantity = 0

        }

    }


    fun onAddToCartClick(product: Product, quantity: Int) {
        val addToCartRequest = AddToCartRequest(product = product, quantity = quantity)
        if (SharedPreference.getInstance().getUserToken() == null) {
            if (SharedPreference.getInstance().getGUID() == null) {

                compositeDisposable.add(cartRepository.createCartSuccess.subscribe({
                    SharedPreference.getInstance().setGUID(it?.guid ?: "")
                    cartRepository.addToCart(
                        Constants.ANONYMOUS,
                        SharedPreference.getInstance().getGUID() ?: "",
                        addToCartRequest,
                        Constants.FIELDS_FULL
                    )
                }, {
                    Log.d(TAG, it.message ?: "")
                }))
                createCart("anonymous", null, null, Constants.FIELDS_FULL)
            } else {
                cartRepository.addToCart(
                    Constants.ANONYMOUS,
                    SharedPreference.getInstance().getGUID() ?: "",
                    addToCartRequest,
                    Constants.FIELDS_FULL
                )
            }

        } else {
            cartRepository.addToCart(
                Constants.CURRENT,
                Constants.CURRENT,
                addToCartRequest,
                Constants.FIELDS_FULL
            )
        }
    }

    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String
    ): BehaviorSubject<Cart> {
        return cartRepository.createCart(userId, oldCartId, toMergeCartGuid, fields)
    }

    class Factory(
        private val listingRepository: ListingRepository,
        private val cartRepository: CartRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ListingViewModel(listingRepository, cartRepository) as T
        }
    }

    open fun getProductList(currentPage: Int) {}
}