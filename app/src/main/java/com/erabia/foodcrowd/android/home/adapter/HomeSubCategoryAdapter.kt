package com.erabia.foodcrowd.android.home.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.databinding.HomeSubCategoryRowItemBinding
import com.erabia.foodcrowd.android.home.callback.OnSubCategoryClickListener

class HomeSubCategoryAdapter() :
    RecyclerView.Adapter<HomeSubCategoryAdapter.ViewHolder>() {
    private var context: Context? = null
    var onSubCategoryClickListener: OnSubCategoryClickListener? = null
    private var rowIndex: Int = -1


    var componentList: List<Component>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)

        val itemBinding: HomeSubCategoryRowItemBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.home_sub_category_row_item,
            parent,
            false
        )
        return ViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return componentList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(componentList?.get(position), position)
    }

    inner class ViewHolder(val binding: HomeSubCategoryRowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var component: Component? = null
        var mPosition = 0


        fun bindData(
            component: Component?,
            position: Int
        ) {
            this.component = component
            binding.component = component
            this.mPosition = position
//            drawItem()
        }

//        fun drawItem() {
//            if (rowIndex == mPosition) {
//                binding?.textViewSubCategory?.visibility = View.VISIBLE
//            } else {
//                binding.textViewSubCategory.visibility = View.INVISIBLE
//            }
//        }

        fun currentPosition(holder: ViewHolder?, position: Int) {
            rowIndex = position
        }

    }

//    fun currentItemChanged(holder: ViewHolder?, position: Int) {
//        rowIndex = position
//    }

}