package com.erabia.foodcrowd.android.common.model
import com.google.gson.annotations.SerializedName

data class CartExpressTimeSlot(
    @SerializedName("messages")
    val messages: List<String> = listOf(),
    @SerializedName("showTimeSlot")
    val showTimeSlot: Boolean = false
)