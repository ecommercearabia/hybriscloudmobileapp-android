package com.erabia.foodcrowd.android.email.remote.repositry

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.google.firebase.analytics.FirebaseAnalytics

import io.reactivex.rxkotlin.subscribeBy

class ChangeEmailRepository(val changeEmailService: ChangeEmailService,
                            val cartService: CartService
) :
    Repository {
    private var guid: String = ""

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mChangeEmailLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val updateSignatureLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val mLoginLiveData: SingleLiveEvent<TokenModel> by lazy { SingleLiveEvent<TokenModel>() }


    companion object {
        const val TAG: String = "ForgetRepository"
    }


    @SuppressLint("CheckResult")
    fun changeEmail(mNewEmail: String,mPassword: String) {
        changeEmailService.changeEmail(
            "current",mNewEmail,mPassword
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mChangeEmailLiveData.value = it.body().toString()
                            doLogin(mNewEmail,mPassword)
                        }
                        201 -> {
                            mChangeEmailLiveData.value = it.body().toString()
                            doLogin(mNewEmail,mPassword)
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                            loadingVisibility.value = View.GONE
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                            loadingVisibility.value = View.GONE
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun updateSignature(signatureId: String){
        changeEmailService.setSignature(Constants.CURRENT, signatureId).get()
            .subscribe(this, onSuccess_200 = {
                updateSignatureLiveData.value = "Success"
            }, onSuccess_201 =
            {
                updateSignatureLiveData.value = "Success"
            })
    }

    @SuppressLint("CheckResult")
    fun doLogin(mUserName: String, mPassword: String) {
//        loginService.getOauth(
//            "password",
//            mUserName,
//            mPassword,
//            "",
//            "occ_mobile",
//            "Erabia@123"
//        )
        changeEmailService.loginEmailMobile(
            mUserName,
            mPassword,
            "mobile_android",
            "secret"
        )
            .get()
            .doOnSubscribe {
                loadingVisibility.value = View.VISIBLE
            }
            .doOnTerminate {
                loadingVisibility.value = View.GONE
            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mLoginLiveData.value = it.body()
                            loadingVisibility.value = View.GONE
                            if (SharedPreference.getInstance().getGUID()?.isNotEmpty() == true) {

                                val mFirebaseAnalytics =
                                    FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                                val params = Bundle()

                                params.putString(FirebaseAnalytics.Param.METHOD, "Login")
                                mFirebaseAnalytics.logEvent(
                                    FirebaseAnalytics.Event.LOGIN,
                                    params
                                )
                                cartService.getCart(
                                    Constants.CURRENT,
                                    Constants.CURRENT,
                                    "FULL"
                                ).get().subscribe(this,
                                    {
                                        this.guid = it.body()?.guid ?: ""

                                        cartService.createCart(
                                            Constants.CURRENT,
                                            SharedPreference.getInstance().getGUID(),
                                            this.guid,
                                            Constants.FIELDS_FULL
                                        ).get().subscribe({
                                            Log.d(LoginRepository.TAG, "cart created")
                                        }, {
                                            Log.d(LoginRepository.TAG, it.message ?: "")
                                        })

                                    }, onError_400 = {
                                        val errorResponse = getResponseErrorMessage(it)
                                        if (
                                            errorResponse.contains("No cart created yet.") ||
                                            errorResponse.contains("Cart not found.")
                                        ) {
                                            cartService.createCart(
                                                Constants.CURRENT,
                                                SharedPreference.getInstance().getGUID(),
                                                "",
                                                Constants.FIELDS_FULL
                                            ).get().subscribe({
                                                Log.d(LoginRepository.TAG, "cart created")
                                            }, {
                                                Log.d(LoginRepository.TAG, it.message ?: "")
                                            })
                                        }
                                    })


                            }


                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }

                        }
                        201 -> {
                            loadingVisibility.value = View.GONE
                            mLoginLiveData.value = it.body()
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }
                        401 -> {
                            loadingVisibility.value = View.GONE
                            error().postValue(MyApplication.getContext()!!.resources.getString(R.string.invalid_user_name_password))
                        }
                        else -> {
                            loadingVisibility.value = View.GONE
                            error().postValue(MyApplication.getContext()!!.resources.getString(R.string.invalid_user_name_password))
                        }
                    }
                },
                onError =
                {
                    Log.d(LoginRepository.TAG, it.toString())
                    loadingVisibility.value = View.GONE
                })

    }
    @SuppressLint("CheckResult")
    fun setFirebaseToken(
        userId: String,
        mobileToken: String
    ) {
        changeEmailService.setFirebaseToken(userId , mobileToken).get().subscribe(
            {
                when (it.code()) {
                    200 -> {

                    }
                    401 -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
            {
                Log.d(HomeRepository.TAG, " message: ${it.message}")
                Log.d(HomeRepository.TAG, " cause: ${it.cause}")

            }
        )
    }



}
