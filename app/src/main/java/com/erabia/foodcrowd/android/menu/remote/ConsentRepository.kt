package com.erabia.foodcrowd.android.menu.remote

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.menu.model.ConsentTemplateList

class ConsentRepository(private val consentService: ConsentService) : Repository {

    var consentTemplateListLiveData = MutableLiveData<ConsentTemplateList>()


    @SuppressLint("CheckResult")
    fun giveConsent(userId: String, consentTemplateId: String, consentTemplateVersion: String) {
        consentService.giveConsent(userId, consentTemplateId, consentTemplateVersion)
            .get().subscribe(this, onSuccess_200 = {
                getConsents(userId)
            },
                onError_400 = {
                    error().postValue(getResponseErrorMessage(it))
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getConsents(userId: String) {
        progressBarVisibility().value = (View.VISIBLE)
        consentService.getConsents(userId, "FULL")
            .get().subscribe(this,
                onSuccess_200 = {
                    Log.d("TAG", "getConsents: ${it.body()}")
                    consentTemplateListLiveData.value = it.body() ?: ConsentTemplateList()
                    progressBarVisibility().value = (View.GONE)
                },

                onError_400 = {
                    error().postValue(getResponseErrorMessage(it))
                }
            )
    }

    @SuppressLint("CheckResult")
    fun deleteConsent(userId: String, consentCode: String) {
        consentService.deleteConsent(userId, consentCode)
            .get().subscribe(this, onSuccess_200 = {
                getConsents(userId)
            },
                onError_400 = {
                    error().postValue(getResponseErrorMessage(it))
                }
            )
    }

}