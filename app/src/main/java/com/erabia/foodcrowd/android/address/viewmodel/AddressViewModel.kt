package com.erabia.foodcrowd.android.address.viewmodel

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.CompoundButton
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.model.AddressList
import com.erabia.foodcrowd.android.address.model.CityList
import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.address.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.checkout.model.CheckoutItemType
import com.erabia.foodcrowd.android.checkout.util.CheckoutItemManager
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import javax.inject.Inject


class AddressViewModel @Inject constructor() : ViewModel() {


    lateinit var addressRepository: AddressRepository
    lateinit var signUpRepository: SignUpRepository
    var firstName = ""
    var lastName = ""
    var countryName = ""
    var mobileCountryName = ""
    var cityName = ""
    var codeNumber = ""
    var mobileNum = ""
    var addressLineOne = ""
    var addressLineTow = ""
    var addressName = ""
    var area = ""
    var postCode = ""
    var nearstLandMark = ""
    var outOfRang = true
    var countryCode: Country? = null
    var bundle = Bundle()
    val saveButtonEnable: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val selectedCountry: MutableLiveData<Country> by lazy { MutableLiveData<Country>() }
    val selectedCity: MutableLiveData<City> by lazy { MutableLiveData<City>() }
    val selectedArea: MutableLiveData<Area> by lazy { MutableLiveData<Area>() }
    val mobileSelectedCountry: MutableLiveData<Country> by lazy { MutableLiveData<Country>() }
    val billingAddress: SingleLiveEvent<Address> by lazy { SingleLiveEvent<Address>() }

    lateinit var progressBarVisibility: SingleLiveEvent<Int>
    var emptyAddressVisibility = SingleLiveEvent<Int>()
    var addAddressButtonVisibility = SingleLiveEvent<Int>()
    var countryNameList = SingleLiveEvent<CountryList>()
    var mobileCountryCodeList = SingleLiveEvent<CountryList>()

    var cityList = SingleLiveEvent<CityList>()
    var areaList = SingleLiveEvent<List<Area>>()
    var addressList = MutableLiveData<AddressList>()
    var action = SingleLiveEvent<Int>()
    var updateAction = SingleLiveEvent<Int>()
    var popEvent = SingleLiveEvent<Boolean>()
    var outOfRangeObserver = SingleLiveEvent<Boolean>()
    var geoAddress = SingleLiveEvent<android.location.Address>()
    var country = SingleLiveEvent<CountryList>()
    var personalDetails = SingleLiveEvent<PersonalDetailsModel>()
    var city = SingleLiveEvent<CityList>()
    var mobilecountry = SingleLiveEvent<CountryList>()
    var mTitleListLiveData: MutableLiveData<TitleResponse> = MutableLiveData<TitleResponse>()
    var addressDetails = MutableLiveData<Address>()
    var mobileCodeNumber = ""

    var message = MutableLiveData<Int>()
    var mTitleCode: SingleLiveEvent<String> = SingleLiveEvent()

    var mDefaultAddressChecked: MutableLiveData<Boolean> = MutableLiveData()


    fun initialization(addressRepository: AddressRepository, signUpRepository: SignUpRepository) {
        this.addressRepository = addressRepository
        this.signUpRepository = signUpRepository
        mTitleListLiveData = signUpRepository.getTitle() as MutableLiveData<TitleResponse>

        country = addressRepository.getShippingCountryList() as SingleLiveEvent<CountryList>
        mobileCodeNumber = mobilecountry.value?.countries?.get(0)?.isdcode?:""

        mobilecountry = addressRepository.getMobileCountryList() as SingleLiveEvent<CountryList>

        progressBarVisibility = addressRepository.progressBarVisibility()
        addressRepository.getAddressList(
            bundle.getString(Constants.COME_FROM) ?: ""
        ) as MutableLiveData<AddressList>

    }

    fun getPersonalDetailsList(){
        personalDetails = addressRepository.getPersonalDetailsList() as SingleLiveEvent<PersonalDetailsModel>

    }

    fun onCheckedChange(button: CompoundButton?, check: Boolean) {
        Log.d("Z1D1", "onCheckedChange: $check")
        mDefaultAddressChecked.value = check
    }

    fun getAddressList() {
        addressList = addressRepository.addressList
        emptyAddressVisibility = addressRepository.emptyAddressVisibility
        addAddressButtonVisibility = addressRepository.addAddressButtonVisibility
    }

    fun createAddress() {
        var address = Address()
        var country = Country()
        country.name = countryName
        country.isocode =
            Country().findCountryByName(this.country.value?.countries, countryName).toString()
        var mobileCountry = MobileCountry()
        mobileCountry.isdcode = mobileCodeNumber
        mobileCountry.isocode =
            MobileCountry().findCountryByCode(this.mobilecountry.value?.countries, mobileCodeNumber)
                .toString()
        var area = Area()
        area.name = this.area
        area.code = Area().findAreaByName(areaList.value, this.area)

        var city = City()
        city.name = cityName
        city.code = City().findCityByName(this.city.value?.cities, cityName)

        address.firstName = firstName
        address.lastName = lastName
        address.mobileNumber = mobileNum
        address.line1 = addressLineOne
        address.line2 = addressLineTow
        address.country = country
        address.city = city
        address.area = area
        address.postalCode = postCode
        address.mobileCountry = mobileCountry
        address.titleCode = mTitleCode.value
        address.addressName = addressName
        address.nearestLandmark = nearstLandMark
        address.defaultAddress = mDefaultAddressChecked.value

        addressRepository.createAddress(address, bundle.getString(Constants.COME_FROM) ?: "")
    }

    fun updateAddress(addressId: String) {
        var address = Address()
        var country = Country()
        country.name = countryName
        country.isocode =
            Country().findCountryByName(this.country.value?.countries, countryName).toString()
        var mobileCountry = MobileCountry()
        mobileCountry.isdcode = mobileCodeNumber
        mobileCountry.isocode =
            MobileCountry().findCountryByCode(this.mobilecountry.value?.countries, mobileCodeNumber)
                .toString()
        var area = Area()
        area.name = this.area
        area.code = Area().findAreaByName(areaList.value, this.area)

        var city = City()
        city.name = cityName
        city.code = City().findCityByName(this.city.value?.cities, cityName)

        address.firstName = firstName
        address.lastName = lastName
        address.mobileNumber = mobileNum
        address.line1 = addressLineOne
        address.line2 = addressLineTow
        address.country = country
        address.city = city
        address.area = area
        address.postalCode = postCode
        address.mobileCountry = mobileCountry
        address.addressName = addressName
        address.titleCode = mTitleCode.value
        address.nearestLandmark = nearstLandMark
        address.defaultAddress = mDefaultAddressChecked.value

        addressRepository.updateAddress(addressId, address)
    }

    fun deleteAddress(addressId: String) {
        addressRepository.deleteAddress(addressId, bundle.getString(Constants.COME_FROM) ?: "")
    }

    fun clearData() {
        if (bundle.getString(Constants.INTENT_KEY.ADDRESS_ID).isNullOrEmpty()) {
            firstName = ""
            lastName = ""
            countryName = ""
            mobileCountryName = ""
            cityName = ""
            codeNumber = ""
            mobileNum = ""
            addressLineOne = ""
            addressLineTow = ""
            area = ""
            postCode = ""
            mobileCodeNumber = ""
            nearstLandMark = ""
            getPersonalDetailsList()
        } else {
            addressDetails =
                bundle.getString(Constants.INTENT_KEY.ADDRESS_ID)?.let {
                    addressRepository.getAddress(it)
                } as MutableLiveData<Address>
        }

    }

    fun getAddress(addressId: String) {
        bundle.putString(Constants.INTENT_KEY.ADDRESS_ID, addressId)
        updateAction.postValue(R.id.action_navigate_to_editAddress)
    }

    fun emptyText(): TextWatcher {
        return object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (firstName.isNotEmpty() && countryName.isNotEmpty() && mobileCodeNumber.isNotEmpty() &&
                    mobileNum.isNotEmpty() && addressLineOne.isNotEmpty() && cityName.isNotEmpty()
                    && lastName.isNotEmpty()
                ) {
                    saveButtonEnable.postValue(true)
                } else {
                    saveButtonEnable.postValue(false)
                }
            }


            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        }
    }


    fun setDeliveryAddress(address: Address) {
        addressRepository.setCartDeliveryAddress(address)
    }

    fun setBillingAddress(address: Address) {
        billingAddress.postValue(address)
    }

    fun shippingCountryName() {
        countryNameList.postValue(country.value)
    }

    fun MobileCountryCode() {
        mobileCountryCodeList.postValue(mobilecountry.value)
    }





    fun cityNameList() {
        if (city.value != null)
            cityList.postValue(city.value)
        else

        AppUtil.showToastyWarning(MyApplication.getContext() ,R.string.please_select_country)
    }

    fun areaNameList() {
        if (city.value?.cities?.isNotEmpty() ?: false)
            areaList.postValue(City().areaList(city.value?.cities, cityName))
        else
            AppUtil.showToastyWarning(MyApplication.getContext() ,R.string.please_select_city)
    }

    fun navigateToMap() {
        action.postValue(R.id.action_navigate_to_map_screen)
    }

    fun onAddNewAddressClick() {
        bundle.putString(Constants.INTENT_KEY.ADDRESS_ID, "")
        action.postValue(R.id.action_navigate_to_addAddress)
    }

    fun onConfirmAddressClick() {
        if (outOfRang) {
            if (bundle.getString(Constants.COME_FROM).equals(Constants.INTENT_KEY.ADDRESS_FORM)) {
                popEvent.postValue(true)
            } else {
                action.postValue(R.id.action_navigate_to_address_screen)
            }
        }
        else{
            outOfRangeObserver.value=true
        }

    }

    fun setGeoAddressData(geoAddressData: android.location.Address) {
        geoAddress.postValue(geoAddressData)
    }

    fun onSaveButtonClick() {
        Log.d(
            "bundel",
            bundle.getString(Constants.COME_FROM) + bundle.getString(Constants.INTENT_KEY.ADDRESS_ID)
        )
        if (bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.MENU) ||
            bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.ADDRESS_FORM)
        ) {
            if (bundle.getString(Constants.INTENT_KEY.ADDRESS_ID).isNullOrEmpty())
                createAddress()
            else
                bundle.getString(Constants.INTENT_KEY.ADDRESS_ID)?.let { updateAddress(it) }

        } else if (bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CART) || bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CHECKOUT) || bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CHECKOUT_BILLING)
        ) {
            if (bundle.getString(Constants.INTENT_KEY.ADDRESS_ID).isNullOrEmpty())
                createAddress()
            else
                bundle.getString(Constants.INTENT_KEY.ADDRESS_ID)?.let { updateAddress(it) }
        }
    }


    /* comments and unused
    // 1
        if (listOf(addressList.value).isNullOrEmpty()) {
            Log.d("test", "if")
            if (bundle.getString(Constants.COME_FROM).equals(Constants.INTENT_KEY.CART)) {
                navigateFromCartToAddress.postValue(R.id.action_navigate_to_addAddress)
            } else {
                //   emptyAddressVisibility.postValue(View.VISIBLE)
                addAddressButtonVisibility.postValue(View.VISIBLE)
            }
        } else {
            Log.d("test", "else")
            //  emptyAddressVisibility.postValue(View.GONE)
            addAddressButtonVisibility.postValue(View.VISIBLE)
        }

        // 2
        fun navigateToCheckout() {
        navigateFromCartToAddress.postValue(R.id.action_addressScreen_to_checkoutScreen)
    }
    //3
       fun saveAddress() {
        if (bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CART) || bundle.getString(Constants.COME_FROM)
                .equals(Constants.INTENT_KEY.CHECKOUT)
        ) {

//            setDeliveryAddress(addAddressLiveData.value!!)
            addAddressLiveData.value.let { it?.let { it1 -> setDeliveryAddress(it1) } }
        } else {
            popEvent.postValue(true)
        }
    }

    //4
     val popEventInCheckout: MutableLiveData<Boolean> by lazy { MutableLiveData<Boolean>() }
     val navigateFromCartToAddress: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
     var addAddressLiveData: LiveData<Address> = SingleLiveEvent()
     var mDeliveryAddressLiveData: LiveData<Void> = SingleLiveEvent()
     var popEvent: SingleLiveEvent<Boolean> = SingleLiveEvent()
     var countryCodeList = MutableLiveData<CountryList>()
     var mobileCountryNameList = MutableLiveData<CountryList>()
     val navigateFromAddressToCheckout: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }


     */

}