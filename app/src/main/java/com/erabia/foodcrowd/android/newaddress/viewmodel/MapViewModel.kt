package com.erabia.foodcrowd.android.newaddress.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent

class MapViewModel : ViewModel(){

    var outOfRang = true
    var outOfRangeObserver = SingleLiveEvent<Boolean>()

    fun onConfirmAddressClick() {
        outOfRangeObserver.value = outOfRang
    }

    class Factory : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MapViewModel() as T
        }

    }
}