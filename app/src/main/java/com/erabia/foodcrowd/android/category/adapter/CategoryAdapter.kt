package com.erabia.foodcrowd.android.category.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.model.Subcategory
import com.erabia.foodcrowd.android.category.viewmodel.CategoryViewModel
import com.erabia.foodcrowd.android.databinding.CategoryRowBinding


class CategoryAdapter(var categoryViewModel: CategoryViewModel) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    lateinit var itemBinding: CategoryRowBinding
    var onClick: IOnClick? = null
    var categoryList: List<Subcategory>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        itemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.category_row, parent, false
        )

        return ViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return categoryList?.size ?: 0

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.category = categoryList?.get(position)
        holder.itemBinding.categoryVM = categoryViewModel
        holder.itemBinding.imageViewCategory.setOnClickListener(View.OnClickListener {
            onClick?.onClick(position)
        })
    }


    override fun getItemId(position: Int): Long {
        return categoryList?.get(position)?.id?.toLong() ?: 0

    }

    class ViewHolder(val itemBinding: CategoryRowBinding) : RecyclerView.ViewHolder(itemBinding.root)

    interface IOnClick {
        fun onClick(position: Int)
    }
}