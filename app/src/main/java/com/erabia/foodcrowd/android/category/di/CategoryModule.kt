package com.erabia.foodcrowd.android.category.di

import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.category.remote.repository.CategoryRepository
import com.erabia.foodcrowd.android.category.remote.service.CategoryService
import com.erabia.foodcrowd.android.category.viewmodel.CategoryViewModel
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.remote.service.ListingService
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService
import dagger.Module
import dagger.Provides

@Module
class CategoryModule {

    @Provides
    fun categoryRepository() =
        CategoryRepository(RequestManager.getClient().create(CategoryService::class.java))

//    @Provides
//    fun listingRepository() =
//        ListingRepository(RequestManager.getClient().create(ListingService::class.java))
//
    @Provides
    fun cartRepository() =
        WishListRepository(RequestManager.getClient().create(WishListService::class.java))


    @Provides
    fun factory(
        categoryRepository: CategoryRepository,
        listingRepository: ListingRepository,
        cartRepository: CartRepository,
        wishListRepository: WishListRepository
    ) = CategoryViewModel.Factory(categoryRepository, listingRepository, cartRepository,wishListRepository)
}