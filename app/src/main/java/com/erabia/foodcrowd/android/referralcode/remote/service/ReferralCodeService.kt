package com.erabia.foodcrowd.android.referralcode.remote.service


import com.erabia.foodcrowd.android.common.model.SiteConfig
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeModel
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query


interface ReferralCodeService {

    @POST("rest/v2/foodcrowd-ae/users/{userId}/referral-code/generate")
    fun generateReferralCode(@Path("userId") userId: String): Observable<Response<ReferralCodeResponse>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/referral-code")
    fun getReferralCode(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<ReferralCodeResponse>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/subscribe")
    fun sendReferralSubscriptionEmails(
        @Path("userId") userId: String,
        @Query("inSeperateEmails") inSeperateEmails: Boolean,
        @Query("emails") emails: String
    ): Observable<Response<ReferralCodeModel>>


    @GET("rest/v2/foodcrowd-ae/config")
    fun getAppConfig(
        @Query("fields") fields: String
    ): Observable<Response<SiteConfig>>
}