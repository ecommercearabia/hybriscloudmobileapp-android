package com.erabia.foodcrowd.android.checkout.model

import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.common.model.BankOfferResponse
import com.erabia.foodcrowd.android.common.model.PaymentModes
import com.erabia.foodcrowd.android.common.model.StoreCreditAmount
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import retrofit2.Response

sealed class ZipModelCheckoutAllData {

    data class SuccessBankOffer(val data: Response<BankOfferResponse>) : ZipModelCheckoutAllData()
    data class SuccessGetTimeSlot(val data: Response<TimeSlot>) : ZipModelCheckoutAllData()
    data class SuccessStoreCreditAmount(val data: Response<StoreCreditAmount>) : ZipModelCheckoutAllData()
    data class SuccessStoreCreditModes(val data: Response<StoreCreditModes>) : ZipModelCheckoutAllData()
    data class SuccessPaymentModes(val data: Response<PaymentModes>) : ZipModelCheckoutAllData()
    data class Error(val t: Throwable) : ZipModelCheckoutAllData()
}