package com.erabia.foodcrowd.android.checkout.model


import com.google.gson.annotations.SerializedName

data class TimeSlotRequest(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("day")
    val day: String = "",
    @SerializedName("end")
    val end: String = "",
    @SerializedName("periodCode")
    val periodCode: String = "",
    @SerializedName("start")
    val start: String = ""
)