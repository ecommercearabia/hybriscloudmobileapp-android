package com.erabia.foodcrowd.android.checkout.view

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.SimpleItemAnimator
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.di.DaggerIAddressComponent
import com.erabia.foodcrowd.android.address.di.IAddressComponent
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.checkout.adapter.CheckoutAdapter
import com.erabia.foodcrowd.android.checkout.ccavenue.CCAvenueState
import com.erabia.foodcrowd.android.checkout.model.CheckoutItemType
import com.erabia.foodcrowd.android.checkout.model.TimeSlotRequest
import com.erabia.foodcrowd.android.checkout.remote.repository.CheckoutRepository
import com.erabia.foodcrowd.android.checkout.remote.service.CheckoutService
import com.erabia.foodcrowd.android.checkout.util.CheckoutItemManager
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.customview.DeliveryDateTimeView
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentCheckoutBinding
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.signup.view.TermsAndPrivacyWebViewActivity
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import kotlinx.android.synthetic.main.checkout_delivery_time_row_item.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mumbai.dev.sdkdubai.PaymentOptions
import mumbai.dev.sdkdubai.external.CustomModel



class CheckoutFragment : Fragment(), DeliveryDateTimeView.OnTimeSlotListener,
    CustomModel.OnCustomStateListener {
    companion object {
        const val TAG = "CheckoutFragment"
    }

    var loadDataFirstTime = true
    var shipmentCode = ""
    private var adapter: CheckoutAdapter? = null
    var binding: FragmentCheckoutBinding? = null
    var viewModel: CheckoutViewModel? = null
    var isRequestGetCartWhenNavegateBetweenScreen = false


    val daggerComponent: IAddressComponent by lazy {
        DaggerIAddressComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)

    }

//    val addressViewModel: AddAddressViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val checkoutService = RequestManager.getClient().create(CheckoutService::class.java)
        val cartService = RequestManager.getClient().create(CartService::class.java)

        val factory = CheckoutViewModel.Factory(
            CheckoutRepository(
                checkoutService, cartService
            ),
            CartRepository(cartService)
        )
        viewModel = ViewModelProvider(this, factory).get(CheckoutViewModel::class.java)
        shipmentCode = SharedPreference.getInstance().getShipmentType() ?: ""
        if (shipmentCode == "0") {
            viewModel?.getAllCheckoutData("current", "current", "fc-ae")
        } else {
            viewModel?.getAllCheckoutData("current", "current", "pickup")
        }
//        viewModel?.getBankOffer()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_checkout, container, false)
        binding?.viewModel = viewModel
        adapter = viewModel?.let { CheckoutAdapter(viewModel!!, this, findNavController()) }
        adapter?.setHasStableIds(true)
        val itemAnimator = binding?.recyclerViewCheckoutStep2?.itemAnimator
        if (itemAnimator is SimpleItemAnimator) {
            itemAnimator.supportsChangeAnimations = false
        }
        CheckoutItemManager.reset()
//        setFragmentResultListener("fromSeeMore") { _, bundle ->
//            // use any type that can be put in a Bundle is supported
//            isFromSeeMore = bundle.get("from") as Boolean
//            adapter?.deliveryTimeItemManager?.isLoaded = true
//            adapter?.paymentMethodItemManager?.isLoaded = true
//            binding?.switchTermsAndConditionLabel?.isChecked = false
//            viewModel?.isTermsChecked = false
//        }


        lifecycleScope.launch {

            if (!loadDataFirstTime) {
                isRequestGetCartWhenNavegateBetweenScreen = true
                adapter?.deliveryTimeItemManager?.isLoaded = true
                adapter?.paymentMethodItemManager?.isLoaded = true
                loadDataFirstTime = false
                viewModel?.isTermsChecked = false
                delay(200)
                binding?.switchTermsAndConditionLabel?.isChecked = false
                viewModel?.checked(binding?.switchTermsAndConditionLabel?.isChecked ?: false)

            } else {
                loadDataFirstTime = false
            }
        }

        binding?.switchTermsAndConditionLabel?.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel?.checked(isChecked)

        }
        //teem checkout
//            val termsString = resources.getString(R.string.terms_and_condition_message)
//            Toast.makeText(context,Html.fromHtml(termsString),Toast.LENGTH_LONG).show()
//            mTearmsTextView?.setText(Html.fromHtml(termsString))  ;

        val redString = context?.resources?.getString(R.string.terms_and_condition_message)
//            Toast.makeText(context,Html.fromHtml(redString),Toast.LENGTH_LONG).show()

        binding?.mTearmsTextView?.text = Html.fromHtml(redString)

        makeLinks(
            Pair(resources.getString(R.string.terms_condition), View.OnClickListener {
                val intent = Intent(context, TermsAndPrivacyWebViewActivity::class.java)
                intent.putExtra("url", getString(R.string.terms_url))
                startActivity(intent)
            })
        )

        binding?.recyclerViewCheckoutStep2?.adapter = adapter
//        adapter?.itemTypeList = listOf(
//            CheckoutItemType.DELIVERY_TIME,
//            CheckoutItemType.PAYMENT_METHOD,
//            CheckoutItemType.ORDER_SUMMARY,
//            CheckoutItemType.SHIPPING_ADDRESS,
//            CheckoutItemType.MY_ITEMS
//        )

        adapter?.itemTypeList = listOf()
        CustomModel.getInstance().setListener(this)
        observeError()
        observeProgressBar()
        observeGetTimeSlot()
        observeSetTimeSlot()
        observeTimeSlotMethodNextSection()
        observeEnablePaymentMethodNextSection()
        observeGetPaymentMode()
        observeGetPaymentModeRes()
        observeGetDeliveryTypes()
        observeSetPaymentMode()
        observeSetStoreCreditMode()
        observeGetStoreCreditMode()
        observeGetStoreCreditAmount()
        observeStoreCreditVisibility()
        observePreSelectPaymentMode()
        observePreSelectStoreCredit()
        observeGetCart()
        observeBankOfferSuccess()
        observeBankOfferError()
        observeFooterVisibility()
        observeEnableCheckoutButton()
        observePlaceOrder()
        observeUseDeliveryAddressSwitchButton()
        observeSetDeliveryMode()
        observeNavigateToPaymentOption()
        observeChangeDeliveryAddress()
        observeBillingAddressSelection()
        observeBadgeNumber()
        observeAppliedVoucher()
        observeClearPromoCodeText()
        observeApplyVoucherSuccess()
        observeDeleteVoucherSuccess()
//        observeBillingAddress()
//        viewModel?.loadCart("", "", Constants.FIELDS_FULL)
        observeValidationSuccess()
        observeValidationError()
        getAppConfig()
        getDeliveryModeTypes()
//        handleDeliveryModeTypes()
        observeEnableStoreCreditLimit()
        return binding?.root
    }

    fun getAppConfig(){
        viewModel?.getConfig()
    }

    fun getDeliveryModeTypes(){
        viewModel?.loadDeliveryModeTypesVM("", "", Constants.FIELDS_FULL)
    }

//    fun handleDeliveryModeTypes(){
//        viewModel?.deliveryModeTypesList?.observe(viewLifecycleOwner, Observer {
//            it.deliveryModeTypes?.forEachIndexed { index, DeliveryModeType ->
//                val radioButton = RadioButton(requireContext())
//                radioButton.isChecked = true
//                radioButton.id = index
//                radioButton.text = DeliveryModeType.name ?: ""
//
//////                binding?.recyclerViewCheckoutStep2?.deliveryModeTypesRadioGroup?.addView(radioButton)
////                binding?.recyclerViewCheckoutStep2?.recycler_delivery_mode_types?.addView(radioButton)
////                binding?.recyclerViewCheckoutStep2?.deliveryModeTypesRadioGroup?.addView(radioButton)
////                binding?.fragmentCheckout?.deliveryModeTypesRadioGroup?.addView(radioButton)
//                binding?.recyclerViewCheckoutStep2?.deliveryModeTypesText?.text = "trrrrrrr"
//
//            }
//        })
//    }

    fun makeLinks(links: Pair<String, View.OnClickListener>) {
        val spannableString = SpannableString(binding?.mTearmsTextView?.text)
        var startIndexOfLink = -1
//        for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun updateDrawState(textPaint: TextPaint) {
                // use this to change the link color
                textPaint.color = textPaint.linkColor
                // toggle below value to enable/disable
                // the underline shown below the clickable text
                textPaint.isUnderlineText = true
            }

            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                links.second.onClick(view)
            }
        }
        startIndexOfLink =
            binding?.mTearmsTextView?.text.toString().indexOf(links.first, startIndexOfLink + 1)
//          if(startIndexOfLink == -1)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + links.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
//        }
        binding?.mTearmsTextView?.movementMethod =
            LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
        binding?.mTearmsTextView?.setText(spannableString, TextView.BufferType.SPANNABLE)
    }


//    private fun observeBillingAddress() {
//        addressViewModel.billingAddress.observe(viewLifecycleOwner, Observer {
//            viewModel?.billingAddress = it
//        })
//    }

    private fun observeBadgeNumber() {
        viewModel?.badgeNumber?.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }


    private fun observeBillingAddressSelection() {
        viewModel?.selectBillingAddressAction?.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                it,
                bundleOf(Constants.COME_FROM to Constants.INTENT_KEY.CHECKOUT_BILLING)
            )
        })
    }

    private fun observeProgressBar() {
        viewModel?.progressBarVisibility?.observe(viewLifecycleOwner, Observer {
//            binding?.progressBar?.visibility = it
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }

    private fun observeError() {
        viewModel?.error?.observe(viewLifecycleOwner, Observer { it ->
            AppUtil.showToastyError(context, it)

            (activity as MainActivity).binding.progressBar.visibility = View.GONE
            (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            Log.d(HomeFragment.TAG, it)
        })
    }


    private fun observeGetTimeSlot() {
        viewModel?.getTimeSlotLiveData?.observe(viewLifecycleOwner, Observer {
//            checkoutAdapter?.deliveryTimeItemManager?.
//            if (!isRequestGetCartWhenNavegateBetweenScreen)
//                viewModel?.loadCart("", "", Constants.FIELDS_FULL)

            adapter?.timeSlot = it
        })
    }

    private fun observeSetTimeSlot() {
        viewModel?.setTimeSlotLiveData?.observe(viewLifecycleOwner, Observer {
//            adapter?.deliveryTimeItemManager?.hasDetails = true
//            (adapter?.deliveryTimeItemManager?.shortDetails as TextView).text = true
//            adapter?.deliveryTimeItemManager?.enableNextSection()
            adapter?.deliveryTimeItemManager?.isHeaderViewExpanded = false
            adapter?.paymentMethodItemManager?.let { itemManager ->
                itemManager.expand()
            }
        })
    }

    private fun observeEnablePaymentMethodNextSection() {
        viewModel?.hasPaymentMethodSetToCart?.observe(viewLifecycleOwner, Observer {
            adapter?.paymentMethodItemManager?.getSection()?.hasSetToCart = it
        })
    }

    private fun observeTimeSlotMethodNextSection() {
        viewModel?.hasTimeSlotSetToCart?.observe(viewLifecycleOwner, Observer {
            adapter?.deliveryTimeItemManager?.getSection()?.hasSetToCart = it

        })
    }

    private fun observeGetPaymentMode() {
        viewModel?.getPaymentModeLiveData?.observe(viewLifecycleOwner, Observer {
            adapter?.paymentModes = it
        })
    }

    private fun observeGetPaymentModeRes() {
        viewModel?.setDeliveryTypes?.observe(viewLifecycleOwner, Observer {
            if(it.deliveryType == "EXPRESS"){
                Log.d("teem res", "EXPRESS")
                binding?.recyclerViewCheckoutStep2?.date_time_view_delivery_time_slot?.visibility = View.GONE
                binding?.recyclerViewCheckoutStep2?.textViewCheckout_expressDeliveryText?.visibility = View.VISIBLE
                if (it?.cartTimeSlot?.messages?.size!! > 0) {
                    binding?.recyclerViewCheckoutStep2?.textViewCheckout_expressDeliveryText?.text = it?.cartTimeSlot?.messages?.get(0)
                }
                binding?.recyclerViewCheckoutStep2?.recycler_delivery_mode_types?.visibility = View.GONE
                binding?.recyclerViewCheckoutStep2?.deliveryModeTypesText?.visibility = View.GONE
            } else{
                Log.d("teem res", "normaaaal")
                binding?.recyclerViewCheckoutStep2?.date_time_view_delivery_time_slot?.visibility = View.VISIBLE
                binding?.recyclerViewCheckoutStep2?.textViewCheckout_expressDeliveryText?.visibility = View.GONE
                binding?.recyclerViewCheckoutStep2?.recycler_delivery_mode_types?.visibility = View.GONE
                binding?.recyclerViewCheckoutStep2?.deliveryModeTypesText?.visibility = View.GONE
            }
        })
    }

    private fun observeGetDeliveryTypes() {
        viewModel?.deliveryModeTypesList?.observe(viewLifecycleOwner, Observer {
            adapter?.deliveryTypes = it
        })
    }

    private fun observeGetStoreCreditMode() {
        viewModel?.getStoreCreditModeLiveData?.observe(viewLifecycleOwner, Observer {
            adapter?.storeCreditModes = it
        })
    }

    private fun observeGetStoreCreditAmount() {
        viewModel?.storeCreditAmount?.observe(viewLifecycleOwner, Observer {
            adapter?.storeCreditAmount = it
        })
    }

    private fun observeStoreCreditVisibility() {
        viewModel?.storeCreditVisibility?.observe(viewLifecycleOwner, Observer {
            adapter?.storeCreditVisibility = it
        })
    }

    private fun observePreSelectPaymentMode() {
        viewModel?.preSelectPaymentMode?.observe(viewLifecycleOwner, Observer {
            adapter?.preSelectPaymentMode = it
        })
    }

    private fun observePreSelectStoreCredit() {
        viewModel?.preSelectStoreCreditMode?.observe(viewLifecycleOwner, Observer {
            adapter?.preSelectedStoreCredit = it
//            viewModel?.selectedStoreCreditModeFromGetCart = it?.storeCreditModeType?.code ?: ""
        })
    }


    private fun observeSetPaymentMode() {
        viewModel?.setPaymentModeLiveData?.observe(viewLifecycleOwner, Observer {
//            adapter?.paymentMethodItemManager?.enableNextSection()
        })
    }

    private fun observeSetStoreCreditMode() {
        viewModel?.setStoreCreditModeLiveData?.observe(viewLifecycleOwner, Observer {


//            viewModel?.loadCart(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
//            viewModel?.loadPaymentMode(Constants.CURRENT, Constants.CURRENT, Constants.FIELDS_FULL)
        })
    }

    private fun observeGetCart() {
        viewModel?.cart?.observe(viewLifecycleOwner, Observer {
            val mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

            var productsBundle = arrayOf<Bundle?>()

            for (i in 0 until (it?.entries?.size ?: 0)) {
                val product = it?.entries?.get(i)?.product

                val productBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product?.code ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product?.name ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                    it?.entries?.get(i)?.quantity?.let { it1 ->
                        putInt(
                            FirebaseAnalytics.Param.QUANTITY,
                            it1
                        )
                    }
                    product?.price?.value?.let { it1 ->
                        putDouble(
                            FirebaseAnalytics.Param.PRICE,
                            it1
                        )
                    }
                }
                if (it?.entries?.size != 0) {
                    productsBundle = append(productsBundle, productBundle)
                }

            }

            var coupons = ""
            for (i in 0 until (it?.appliedVouchers?.size ?: 0)) {
                coupons += it?.appliedVouchers?.get(i)?.code ?: ""
                if (i < (it?.appliedVouchers?.size ?: 0)) {
                    coupons += ", "
                }
            }

            val beginCheckoutParams = Bundle()

            beginCheckoutParams.putString(
                FirebaseAnalytics.Param.COUPON,
                coupons
            )

            beginCheckoutParams.putString(
                FirebaseAnalytics.Param.CURRENCY,
                it?.totalPriceWithTax?.currencyIso ?: ""
            )
            it?.totalPriceWithTax?.value?.let { it1 ->
                beginCheckoutParams.putDouble(
                    FirebaseAnalytics.Param.VALUE,
                    it1
                )
            }
            beginCheckoutParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                productsBundle
            )

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, beginCheckoutParams)


            adapter?.cart = it
            adapter?.itemTypeList = listOf(
                CheckoutItemType.DELIVERY_TIME,
                CheckoutItemType.PROMO_CODE,
                CheckoutItemType.PAYMENT_METHOD,
                CheckoutItemType.ORDER_SUMMARY,
                CheckoutItemType.SHIPPING_ADDRESS,
                CheckoutItemType.MY_ITEMS
            )
        })
    }

    private fun observeBankOfferSuccess() {
        viewModel?.getBankOfferSuccess?.observe(viewLifecycleOwner, Observer {
            adapter?.bankOffer = it
        })

    }

    private fun observeBankOfferError() {
        viewModel?.getBankOfferError?.observe(viewLifecycleOwner, Observer {
            adapter?.observeBankOfferError = true
            AppUtil.showToastyError(context, it)
            (activity as MainActivity).binding.progressBar.visibility = View.GONE
            (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
        })
    }

    private fun observeEnableStoreCreditLimit() {
        viewModel?.enableStoreCreditLimit?.observe(viewLifecycleOwner, Observer {
            adapter?.isStoreCreditLimit = it
        })
    }

    private fun observeAppliedVoucher() {
        viewModel?.appliedVoucher?.observe(viewLifecycleOwner, Observer {
            adapter?.vouchersList = it
        })
    }


    private fun observeClearPromoCodeText() {
        viewModel?.clearPromoCodeText?.observe(viewLifecycleOwner, Observer {
            adapter?.isClearPromoCodeText = it
        })
    }

    private fun observeFooterVisibility() {
        viewModel?.footerVisibilityLiveDate?.observe(viewLifecycleOwner, Observer {
            if (
                binding?.switchTermsAndConditionLabel?.visibility != View.VISIBLE ||
                binding?.mTearmsTextView?.visibility != View.VISIBLE ||
                binding?.buttonCheckout?.visibility != View.VISIBLE
            ) {
                binding?.switchTermsAndConditionLabel?.visibility = View.VISIBLE
                binding?.mTearmsTextView?.visibility = View.VISIBLE
                binding?.buttonCheckout?.visibility = View.VISIBLE
            }

        })
    }

    private fun observeEnableCheckoutButton() {
        viewModel?.enableCheckoutButtonLiveData?.observe(viewLifecycleOwner, Observer {
            if (it) {
                binding?.buttonCheckout?.alpha = 1.0f
            } else {
                binding?.buttonCheckout?.alpha = 0.5f
            }
        })
    }

    private fun observePlaceOrder() {
        viewModel?.placeOrderLiveData?.observe(viewLifecycleOwner, Observer { it ->

//            val firebaseAnalytics = context?.let { it1 -> FirebaseAnalytics.getInstance(it1) }
//            val bundle = Bundle()
//            bundle.putString(Param.TRANSACTION_ID, it.code ?: "")
//            bundle.putString(Param.CURRENCY, it.totalPriceWithTax?.currencyIso ?: "")
//            bundle.putDouble(Param.VALUE, it.totalPriceWithTax?.value ?: 0.0)
//            bundle.putDouble(Param.SHIPPING, it.deliveryCost?.value ?: 0.0)
//            bundle.putDouble(Param.TAX, it.totalTax?.value ?: 0.0)
//            bundle.putParcelableArrayList(Param.ITEMS, it.entries as? ArrayList<out Parcelable?>?)
//            firebaseAnalytics?.logEvent(Event.PURCHASE, bundle)

//            bundle.putParcelable("order_data", it)
            val action = CheckoutFragmentDirections.actionNavigateToOrderConfirmation(it)
////            findNavController().navigate(FragmentDirection,bundle)
            findNavController().navigate(action)
            AppUtil.showToastySuccess(context, R.string.Success)

        })
    }

    private fun observeSetDeliveryMode() {
        viewModel?.setDeliveryModeSuccessEvent?.observe(viewLifecycleOwner, Observer {
//            viewModel?.loadCart("", "", Constants.FIELDS_FULL)
//            viewModel?.loadTimeSlot("", "", "")
//            viewModel?.loadStoreCreditAmount(Constants.CURRENT, Constants.FIELDS_FULL)
        })
    }

    private fun observeUseDeliveryAddressSwitchButton() {
        viewModel?.useDeliveryAddressSwitchButton?.observe(viewLifecycleOwner, Observer {
            if (it) {
                adapter?.showBillingAddressSection(false)
            } else {
                adapter?.showBillingAddressSection(true)
            }
        })
    }

    private fun observeNavigateToPaymentOption() {
        viewModel?.navigateToPaymentOption?.observe(viewLifecycleOwner, Observer {
            val i = Intent(context, PaymentOptions::class.java)
            //Intent i =new Intent(MainActivity.this,PaymentOptions.class);
            // Intent i =new Intent(MainActivity.this,PaymentDetails.class);
            i.putExtra("merchant", viewModel?.ccAvenueMerchantDetails)
            i.putExtra("billing", viewModel?.ccAvenueBillingAddress)
            i.putExtra("shipping", viewModel?.ccAvenueShippingAddress)
            i.putExtra("standard instructions", viewModel?.ccAvenueStandardInstruction)
            Log.d(
                TAG,
                "observeNavigateToPaymentOption - current Thread : ${Thread.currentThread().name}"
            )
            startActivity(i)
        })
    }

    override fun onTimeSlotClickListener(slot: TimeSlotRequest?) {
        viewModel?.setTimeSlot(
            SharedPreference.getInstance().getLoginEmail() ?: "",
            SharedPreference.getInstance().getCartId() ?: "",
            Constants.FIELDS_FULL,
            slot ?: TimeSlotRequest()
        )
    }

    private fun observeChangeDeliveryAddress() {
        viewModel?.changeDeliveryAddressAction?.observe(this, Observer {
            findNavController().navigate(
                it,
                bundleOf(Constants.COME_FROM to Constants.INTENT_KEY.CHECKOUT)
            )
        })
    }

    private fun observeValidationSuccess() {
        viewModel?.validationSuccess?.observe(viewLifecycleOwner, Observer {
            viewModel?.onCheckoutValidation()
        })
    }

    private fun observeValidationError() {
        viewModel?.mErrorValidationMessage?.observe(viewLifecycleOwner, Observer {
            AlertDialog.Builder(requireContext())
                .setTitle(getString(R.string.title_label))
                .setMessage(it)
                .setPositiveButton("ok",
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                .create()
                .show()
        })
    }

    override fun stateChanged() {

        Log.d(TAG, "stateChanged - current Thread : ${Thread.currentThread().name}")

        if (!CustomModel.getInstance().state.equals("Transaction Closed") && !CustomModel.getInstance().state.equals("Transaction Cancelled by User")) {

            val json = CustomModel.getInstance().state
            val gson = Gson()
            val paymentState: CCAvenueState = gson.fromJson(json, CCAvenueState::class.java)
            //    if (paymentState.order_status == "Success") {
//                viewModel?.setPaymentDetails("name", "12", "20", "123", "brand", "4111111111111111")
            Handler(Looper.getMainLooper()).post {
                viewModel?.setPaymentDetails(paymentState.tracking_id)
            }
            // } else {
            //     Toast.makeText(context, R.string.failed, Toast.LENGTH_LONG).show()
            // }
            Log.d(TAG, "paymentState ${paymentState}")
        } else {
            AppUtil.showToastyWarning(context, R.string.canceled)

        }


    }

    private fun observeApplyVoucherSuccess() {
        viewModel?.applyVoucherSuccess?.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.done)
        })
    }

    private fun observeDeleteVoucherSuccess() {
        viewModel?.deleteVoucherSuccess?.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.done)
        })
    }

    fun append(arr: Array<Bundle?>, element: Bundle): Array<Bundle?> {
        val array = arrayOfNulls<Bundle>(arr.size + 1)
        System.arraycopy(arr, 0, array, 0, arr.size)
        array[arr.size] = element
        return array
    }
}