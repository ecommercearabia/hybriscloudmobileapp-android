package com.erabia.foodcrowd.android.home.remote.service

import com.erabia.foodcrowd.android.common.model.SendEmailOtp
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface LoginWithOtpService {
    @POST("rest/v2/foodcrowd-ae/otp/send/login")
    fun sendOtp(
        @Query("email") email: String
    ): Observable<Response<SendEmailOtp>>

}