package com.erabia.foodcrowd.android.home.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.util.Log
import android.util.SparseIntArray
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.PageSwitcher
import com.erabia.foodcrowd.android.databinding.BannerTypeRowItemBinding
import com.erabia.foodcrowd.android.databinding.BannersTypeRowItemBinding
import com.erabia.foodcrowd.android.databinding.HighlightedProductRowItemContainerBinding
import com.erabia.foodcrowd.android.databinding.TabTypeRowItemBinding
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.repository.HighlightedProductRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import kotlin.collections.set


class HomeAdapter(
    val fragment: Fragment,
    val homeViewModel: HomeViewModel,
    val navController: NavController,
    val homeService: HomeService,
    val homeDao: HomeDao
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    DiscreteScrollView.OnItemChangedListener<HomeSubCategoryAdapter.ViewHolder> {
    companion object {
        const val TAG = "HomeAdapter"
    }

    private var pageSwitcher: PageSwitcher? = null
    private var tabBinding: TabTypeRowItemBinding? = null
    private var subCategoryAdapter: HomeSubCategoryAdapter? = null
    private var component: Component? = null
    private var bannersItemBinding: BannersTypeRowItemBinding? = null
    private var bannerAdapter: BannerAdapter? = null

    @Suppress("UNCHECKED_CAST")
    var bannersSize: Int = 0
        set(value) {
            field = value
//            bannerAdapter?.size = value
        }
    var bannersList: MutableList<Component?>? = null
        set(value) {
            field = value
            bannerAdapter?.componentList = value as? List<Component>
            totalPages = value?.size ?: 0
            pageSwitcher?.totalPages = value?.count()
            pageSwitcher?.execute(3)
        }
    var tabList: MutableList<Component?>? = null
        set(value) {
            field = value
//            segmentCategoryPagerAdapter?.componentList = value as? List<Component>
            subCategoryAdapter?.componentList = value as? List<Component>
        }
    var homeFragment: HomeFragment? = null
    private var isFirstBannerSetter: Boolean = true
    var homeList: List<Component>? = null
        set(value) {
            field = value
            Log.d(TAG, homeList.toString())
            notifyDataSetChanged()
        }
    var itemBinding: ViewDataBinding? = null
    private var page = 0
    private var totalPages: Int = 0
    private val BANNERS = 0
    private val HIGHLIGHTED_PRODUCTS = 1
    private val BANNER = 2

    //    private val HIGHLIGHTED_PRODUCTS_GRID = 3
    private val TAB = 4
    private var viewHolder: RecyclerView.ViewHolder? = null
    private var mViewHolder: ViewHolder? = null
    private var context: Context? = null
    private var segmentCategoryPagerAdapter: SegmentCategoryPagerAdapter? = null
    var bannerCurrentPage = 0

//    private lateinit var categoryProductPagerAdapter: CategoryProductPagerAdapter
//    private lateinit var segmentCategoryPagerAdapter: SegmentCategoryPagerAdapter


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        when (viewType) {
            BANNERS -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.banners_type_row_item,
                    parent,
                    false
                )
                val bannersBinding = itemBinding as BannersTypeRowItemBinding
                bannerAdapter = BannerAdapter(navController, homeViewModel)
                bannerAdapter?.saveState()
                viewHolder = ViewHolder(itemBinding!!)
            }

            BANNER -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.banner_type_row_item,
                    parent,
                    false
                )
                viewHolder = ViewHolder(itemBinding!!)
            }
            HIGHLIGHTED_PRODUCTS -> {

                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.highlighted_product_row_item_container,
                    parent,
                    false
                )
                viewHolder = ViewHolder(itemBinding!!)
            }
            TAB -> {
                itemBinding = DataBindingUtil.inflate(
                    layoutInflater,
                    R.layout.tab_type_row_item,
                    parent,
                    false
                )

                viewHolder = ViewHolder(itemBinding!!)
            }

            else -> {
                itemBinding =
                    DataBindingUtil.inflate(
                        layoutInflater,
                        R.layout.view_not_supported_row_item,
                        parent,
                        false
                    )
                viewHolder = ViewHolder(itemBinding!!)
            }
        }
        return viewHolder!!
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (homeList != null) {
//            val home = homeList?.get(position)
//            try {
//                component = home?.components?.component?.get(0)
//            } catch (e: Exception) {
//                Log.d(TAG, e.message)
//            }
            component = homeList?.get(position)
            mViewHolder = holder as ViewHolder
            Log.d("bannerStatusBind", "position : $position \n")
            Log.d(TAG, "position : $position typeCode ${homeList?.get(position)?.typeCode}")
            when (holder.itemViewType) {
                BANNERS -> {
                    bannersItemBinding = mViewHolder?.itemBinding as BannersTypeRowItemBinding
                    bannersItemBinding?.viewPager?.adapter = bannerAdapter
                    bannersItemBinding?.dots?.attachViewPager(bannersItemBinding?.viewPager)
                    pageSwitcher = context?.let { context ->
                        bannersItemBinding?.viewPager?.let { viewPager ->
                            PageSwitcher(
                                context,
                                viewPager
                            )
                        }
                    }
                    homeViewModel.getBanners(component ?: Component())
//                    bannersItemBinding?.viewPager?.setOnTouchListener(
//                        object : OnTouchListener {
//                            private var moved = false
//                            override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
//                                if (motionEvent.action == MotionEvent.ACTION_DOWN) {
//                                    moved = false
//                                }
//                                if (motionEvent.action == MotionEvent.ACTION_MOVE) {
//                                    moved = true
//                                }
//                                if (motionEvent.action == MotionEvent.ACTION_UP) {
//                                    if (!moved) {
//                                        view.performClick()
//                                    }
//                                }
//                                return false
//                            }
//                        }
//                    )
                    // then you can simply use the standard onClickListener ...

                    // then you can simply use the standard onClickListener ...
//                    bannersItemBinding?.viewPager?.setOnClickListener(
//                        object : View.OnClickListener {
//                            override fun onClick(view: View?) {
//                                Log.i("asdasdas", "Dayum!")
//                            }
//                        }
//                    )
//                    bannersItemBinding?.viewPager?.getChildAt(bannersItemBinding?.viewPager?.currentItem?:0)?.setOnClickListener {
//                        Toast.makeText(context, R.string.add_to_wishlist, Toast.LENGTH_SHORT).show()
//                    }
//                    bannersItemBinding?.viewPager?.addOnPageChangeListener(object :
//                        ViewPager.OnPageChangeListener {
//                        override fun onPageScrollStateChanged(state: Int) {
//
//                        }
//
//                        override fun onPageScrolled(
//                            position: Int,
//                            positionOffset: Float,
//                            positionOffsetPixels: Int
//                        ) {
//                        }
//
//                        override fun onPageSelected(position: Int) {
////                            homeList?.get(position)?.urlLink.toString().substringAfterLast("p")
//
//                        }
//
//                    })

                }


                BANNER -> {
                    val bannerRowItemBinding = mViewHolder?.itemBinding as BannerTypeRowItemBinding
                    bannerRowItemBinding.imageViewBanner.setImageDrawable(null)
                    if (component != null) {
                        bannerRowItemBinding.url = component?.media?.mobile?.url
                        var urlLink = component?.urlLink.toString() ?: ""
                        var productCodesBanner =
                            component?.productCodes?.split(" ")?.toMutableList() ?: mutableListOf()
                        bannerRowItemBinding.imageViewBanner.setOnClickListener {
                            var urlLink = urlLink

                            homeViewModel.bannersNavegate(urlLink)

                        }
                    }

                }
                HIGHLIGHTED_PRODUCTS -> {
                    val highLightedProductViewHolder =
                        mViewHolder!!.itemBinding as HighlightedProductRowItemContainerBinding
                    highLightedProductViewHolder.textViewHeader.text = component?.title ?: ""
                    val highlightedProductRepository =
                        HighlightedProductRepository(homeService, homeDao)
                    val highLightedProductsViewModel =
                        HighLightedProductsViewModel(highlightedProductRepository)

                    val key = mViewHolder?.layoutPosition
                    val state = scrollStates[key]


                    Handler().postDelayed({
                        if (state != null) {
                            highLightedProductViewHolder.recyclerViewCategory.layoutManager?.onRestoreInstanceState(
                                state
                            )

                        } else {
                            highLightedProductViewHolder.recyclerViewCategory.layoutManager?.scrollToPosition(
                                0
                            )

                        }
                    }, 50)

                    prepareListingAdapter(
                        position,
                        highLightedProductsViewModel,
                        highLightedProductViewHolder,
                        component?.productCodes?.split(" ")?.toMutableList() ?: mutableListOf(),
                        mViewHolder!!
                    )

                }
                TAB -> {
                    tabBinding = mViewHolder!!.itemBinding as TabTypeRowItemBinding
                    tabBinding?.picker?.addOnItemChangedListener(this)

                    segmentCategoryPagerAdapter = SegmentCategoryPagerAdapter(
                        context!!,
                        homeFragment,
                        homeViewModel,
                        fragment.parentFragmentManager,
                        homeService,
                        homeDao
                    )
                    val highLightedProductsViewModel = HighLightedProductsViewModel(
                        HighlightedProductRepository(homeService, homeDao)
                    )
                    val listingAdapter =
                        HomeListingAdapter(highLightedProductsViewModel, homeViewModel, false)
                    subCategoryAdapter = HomeSubCategoryAdapter()
                    val wrapper: InfiniteScrollAdapter<*> = InfiniteScrollAdapter.wrap(
                        subCategoryAdapter!!
                    )

                    listingAdapter.setHasStableIds(true)
                    tabBinding?.recyclerViewHighlightedProduct?.isFocusable = false
                    tabBinding?.recyclerViewHighlightedProduct?.adapter = listingAdapter
//        observeCategoryProduct()
                    tabBinding?.picker?.adapter = wrapper
                    tabBinding?.picker?.setItemTransformer(
                        ScaleTransformer.Builder()
                            .setMaxScale(1.05f)
                            .setMinScale(0.75f)
                            .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                            .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                            .build()
                    )
                    homeViewModel.getTabs(component ?: Component())
                }
            }
        }
    }


    private fun observeHighlightedProductViewModel(
        highLightedProductsViewModel: HighLightedProductsViewModel,
        recyclerView: RecyclerView,
        homeListingAdapter: HomeListingAdapter
    ) {
        highLightedProductsViewModel.productLiveData.observe(
            fragment.viewLifecycleOwner,
            androidx.lifecycle.Observer { product ->
                //notifyItemChanged() should called on ui thread , it will crashed if its called on
                //background thread
                recyclerView.post {
                    homeListingAdapter.updateItem(product)
                }


            })


//        highLightedProductsViewModel.errorProductLiveData.observe(
//            fragment.viewLifecycleOwner,
//            androidx.lifecycle.Observer { id ->
//                Toast.makeText(context, id, Toast.LENGTH_LONG).show()
////                homeListingAdapter.deleteNullItem(id)
//                recyclerView.post {
//                    homeListingAdapter.deleteNullItem(id)
//                }
//
//                //notifyItemChanged() should called on ui thread , it will crashed if its called on
//                //background thread
//
//            })


    }

    private fun observeProductPositionViewModel(
        recyclerView: RecyclerView,
        homeViewModel: HomeViewModel,
        position: Int
    ) {
        homeViewModel.navigatePosition.observe(
            fragment.viewLifecycleOwner,
            androidx.lifecycle.Observer {
                //notifyItemChanged() should called on ui thread , it will crashed if its called on
                //background thread
//                Handler().postDelayed({
//
//                    val lastSeenFirstPosition: Int = listPosition.get(position, 0)
//                    if (lastSeenFirstPosition >= 0) {
//                        viewHolder.layoutManager.scrollToPositionWithOffset(
//                            lastSeenFirstPosition,
//                            0
//                        )
//                    }
//
//                    recyclerView.smoothScrollToPosition(
//                        recyclerView.findViewHolderForAdapterPosition(
//                            it
//                        )?.adapterPosition ?: 0
//                    )
//
//
//                }, 50)


            })
    }


    var selectedHomeListPos = SparseIntArray()
    lateinit var mListState: Parcelable
    var mBundleRecyclerViewState = Bundle()

    //    var homeListAdapterArrayList = HashMap<Int, HomeListingAdapter>()
    val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

    //    var homeListAdapterLayoutManagerArrayList = HashMap<Int, RecyclerView.LayoutManager>()
    private fun prepareListingAdapter(
        position: Int,
        itemViewModel: HighLightedProductsViewModel,
        binding: HighlightedProductRowItemContainerBinding,
        productCodeList: MutableList<String>,
        viewHolder: RecyclerView.ViewHolder
    ) {

//        if (!selectedHomeListPos.get(position, false)) {
        val listingAdapter = HomeListingAdapter(itemViewModel, homeViewModel, false, -1)
//        homeListAdapterArrayList[position] = listingAdapter

//        observeProductPositionViewModel(binding.recyclerViewCategory, homeViewModel, position)

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)

//            homeListAdapterLayoutManagerArrayList[position] = layoutManager

        binding.recyclerViewCategory.layoutManager =
            layoutManager
        mListState = binding.recyclerViewCategory.layoutManager?.onSaveInstanceState()!!
        listingAdapter.setHasStableIds(true)

        mBundleRecyclerViewState.putParcelable("KEY_RECYCLER_STATE$position", mListState);

//            selectedHomeListPos.put(position, true)
//        }


//        Handler().postDelayed({
//            mListState = mBundleRecyclerViewState.getParcelable("KEY_RECYCLER_STATE$position")!!
//            binding.recyclerViewCategory.layoutManager?.onRestoreInstanceState(mListState)
//        }, 50)

        Log.e(TAG, "prepareListingAdapter: position=   $position")
        listingAdapter.productList =
            productCodeList.map { Product().apply { code = it } }.toMutableList()
        binding.recyclerViewCategory.itemAnimator = null

        binding.recyclerViewCategory.adapter = listingAdapter
        observeHighlightedProductViewModel(
            itemViewModel,
            binding.recyclerViewCategory,
            listingAdapter
        )

//        binding.recyclerViewCategory.post({ binding.recyclerViewCategory.setScrollX(scrollXState[viewHolder.getAdapterPosition()]) })

    }


    val scrollStates = mutableMapOf<Int, Parcelable?>()

    override fun onViewRecycled(viewHolder: RecyclerView.ViewHolder) {
        mViewHolder = viewHolder as ViewHolder
        when (mViewHolder?.itemViewType) {

            HIGHLIGHTED_PRODUCTS -> {
                val key = viewHolder.layoutPosition
                scrollStates[key] =
                    (mViewHolder?.itemBinding as HighlightedProductRowItemContainerBinding).recyclerViewCategory.layoutManager?.onSaveInstanceState()

            }

        }


//        mViewHolder = viewHolder as ViewHolder
//        scrollXState[viewHolder.adapterPosition] =
//            (viewHolder.itemBinding as HighlightedProductRowItemContainerBinding).recyclerViewCategory.getScrollX()

        // Store position
//        val position = viewHolder.adapterPosition
//        val firstVisiblePosition: Int = layoutManager.findFirstVisibleItemPosition()
//        selectedHomeListPos.put(position, firstVisiblePosition)
        super.onViewRecycled(viewHolder)
    }


    private fun prepareListingAdapter(
        productCodeList: MutableList<String>?
    ) {
        val itemViewModel =
            HighLightedProductsViewModel(HighlightedProductRepository(homeService, homeDao))
        val listingAdapter = HomeListingAdapter(itemViewModel, homeViewModel, false, -1)
        listingAdapter.setHasStableIds(true)
        listingAdapter.productList =
            productCodeList?.map { Product().apply { code = it } }?.toMutableList()
        tabBinding?.recyclerViewHighlightedProduct?.itemAnimator = null

        tabBinding?.recyclerViewHighlightedProduct?.adapter = listingAdapter
        tabBinding?.recyclerViewHighlightedProduct?.let {
            observeHighlightedProductViewModel(
                itemViewModel,
                it,
                listingAdapter
            )
        }
    }

    override fun getItemCount(): Int {
        return if (homeList != null)
            homeList?.size ?: 0
        else 0
    }


    override fun getItemViewType(position: Int): Int {
        try {
            return when (homeList?.get(position)?.typeCode) {
                Constants.HYBRIS_TYPE.BANNERS -> BANNERS
                Constants.HYBRIS_TYPE.HIGHLIGHTED_PRODUCTS -> HIGHLIGHTED_PRODUCTS
                Constants.HYBRIS_TYPE.BANNER -> BANNER
                Constants.HYBRIS_TYPE.TAB -> TAB
                else -> -1
            }
        } catch (e: Exception) {
            Log.d(TAG, e.message ?:"")
            return -1
        }
    }

    class ViewHolder(val itemBinding: ViewDataBinding) : RecyclerView.ViewHolder(itemBinding.root)

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onCurrentItemChanged(
        viewHolder: HomeSubCategoryAdapter.ViewHolder?,
        position: Int
    ) {
        prepareListingAdapter(viewHolder?.component?.productCodes?.split(" ") as? MutableList<String>)
    }

}
