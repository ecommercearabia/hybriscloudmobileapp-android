package com.erabia.foodcrowd.android.common.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubTotalBeforeSavingPrice(
    @SerializedName("currencyIso")
    val currencyIso: String = "",
    @SerializedName("formattedValue")
    val formattedValue: String = "",
    @SerializedName("maxQuantity")
    val maxQuantity: Double = 0.0,
    @SerializedName("minQuantity")
    val minQuantity: Double = 0.0,
    @SerializedName("priceType")
    val priceType: String = "",
    @SerializedName("value")
    val value: Double = 0.0
): Parcelable