package com.erabia.foodcrowd.android.product.viewmodel

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.AddToCartRequest
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.ProductReference
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.viewmodel.ProductItemViewModel
import com.erabia.foodcrowd.android.product.model.AddReviewPostData
import com.erabia.foodcrowd.android.product.model.AddReviewResponse
import com.erabia.foodcrowd.android.product.model.ReviewsResponse
import com.erabia.foodcrowd.android.product.remote.repository.ProductRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import io.reactivex.disposables.CompositeDisposable

class ProductViewModel(
    homeRepository: HomeRepository,
    wishListRepository: WishListRepository,
    private val productRepository: ProductRepository,
    cartRepository: CartRepository
) : ProductItemViewModel(homeRepository, cartRepository, wishListRepository) {

    companion object {
        const val TAG = "ProductViewModel"
    }

    val preSelectedZoomImagePosition: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val navigateToZoomEvent: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val addToWishlist: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    var reviewTitle = ""
    var reviewName = ""
    var reviewCommit = ""

    val loadingVisibility: MutableLiveData<Int> = productRepository.loadingVisibility

    val productReferenceLiveData: MutableLiveData<List<ProductReference>> =
        productRepository.productReferenceLiveData
    val youMayAlsoLike: SingleLiveEvent<Int> = productRepository.youMayAlsoLikeVisibility
    val quantityTextValue: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    var product = MutableLiveData<Product>()
    var productReviewLiveData: SingleLiveEvent<ReviewsResponse> =
        productRepository.productReviewLiveData
    var addReviewResponseLiveData: SingleLiveEvent<AddReviewResponse> =
        productRepository.addReviewResponseLiveData
    var mProductLiveData: SingleLiveEvent<Product> = productRepository.mProductLiveData
    var mErrorTitleValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorRateValidationMessage: MutableLiveData<String> = MutableLiveData()


    private val compositeDisposable = CompositeDisposable()
    var quantity: Int = 1
    val error = productRepository.error()
    val progressBarVisibility: SingleLiveEvent<Int> = productRepository.progressBarVisibility()
    val progressBarVisibilityCart: SingleLiveEvent<Int> = cartRepository.progressBarVisibility()
    val errorCart = productRepository.error()
    val addToCartSuccess = cartRepository.addToCartSuccess
    val productInfoVisibility: SingleLiveEvent<Int> by lazy {
        SingleLiveEvent<Int>().apply {
            value = View.GONE
        }
    }
    val reviewVisibility: SingleLiveEvent<Int> by lazy {
        SingleLiveEvent<Int>().apply {
            value = View.GONE
        }
    }
    val nutritionalVisibility: SingleLiveEvent<Int> by lazy {
        SingleLiveEvent<Int>().apply {
            value = View.GONE
        }
    }

    fun getProductDetails(productCode: String) {
        product = productRepository.loadProduct(productCode) as MutableLiveData<Product>

//         mDescribtion=product.value?.description

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            mDescribtion= Html.fromHtml(product.value?.description, Html.FROM_HTML_MODE_COMPACT)
//                .toString()
//        } else {
//            mDescribtion= Html.fromHtml(product.value?.description).toString()
//        }

    }

    fun getReviewDetails(productCode: String) {
        productRepository.getProductAndReview(productCode)
    }

    fun addReview(productCode: String, rate: Double) {
        val addReviewPostData = AddReviewPostData()
        if (reviewTitle.isEmpty()) {
            mErrorTitleValidationMessage.value = "Please fill the blank"
        } else if (reviewCommit.isEmpty()) {
            mErrorTitleValidationMessage.value = "Please fill the blank"
        } else if (rate.equals(0.0)) {
            mErrorRateValidationMessage.value = "Please Rate the product"
        }



        if (reviewTitle.isNotEmpty() && reviewCommit.isNotEmpty() && !rate.equals(0.0)) {
            addReviewPostData.alias = reviewName
            addReviewPostData.headline = reviewTitle
            addReviewPostData.comment = reviewCommit
            addReviewPostData.rating = rate

            productRepository.addReview(productCode, addReviewPostData)
        }
    }


    fun increaseQuantity() {
        if (quantity < product?.value?.stock?.stockLevel ?: 0) {
            quantity++
            quantityTextValue.postValue(quantity.toString())
        } else {


            AppUtil.showToastyWarning(MyApplication.getContext(), R.string.out_of_stock)

        }
    }

    fun decreaseQuantity() {
        if (quantity > 1) {
            quantity--
            quantityTextValue.postValue(quantity.toString())
        }

    }

    fun onAddToCartClick(product: Product) {
        val addToCartRequest = AddToCartRequest(product = product, quantity = quantity)
        if (SharedPreference.getInstance().getUserToken() == null) {
            if (SharedPreference.getInstance().getGUID() == null) {

                compositeDisposable.add(cartRepository.createCartSuccess.subscribe({
                    SharedPreference.getInstance().setGUID(it?.guid ?: "")
                    cartRepository.addToCart(
                        Constants.ANONYMOUS,
                        SharedPreference.getInstance().getGUID() ?: "",
                        addToCartRequest,
                        Constants.FIELDS_FULL
                    )
                }, {
                    Log.d(TAG, it.message ?: "")
                }))
                cartRepository.createCart("anonymous", null, null, Constants.FIELDS_FULL)
            } else {
                cartRepository.addToCart(
                    Constants.ANONYMOUS,
                    SharedPreference.getInstance().getGUID() ?: "",
                    addToCartRequest,
                    Constants.FIELDS_FULL
                )
            }

        } else {
            cartRepository.addToCart(
                Constants.CURRENT,
                Constants.CURRENT,
                addToCartRequest,
                Constants.FIELDS_FULL
            )
        }
    }

    fun onProductInfoClick() {
        if (productInfoVisibility.value == View.GONE) {
            productInfoVisibility.postValue(View.VISIBLE)
        } else {
            productInfoVisibility.postValue(View.GONE)
        }
    }

    fun onReviewClick() {
        if (reviewVisibility.value == View.GONE) {
            reviewVisibility.postValue(View.VISIBLE)
        } else {
            reviewVisibility.postValue(View.GONE)
        }
    }
    fun onNutritionalClick() {
        if (nutritionalVisibility.value == View.GONE) {
            nutritionalVisibility.postValue(View.VISIBLE)
        } else {
            nutritionalVisibility.postValue(View.GONE)
        }
    }

    fun onImageBannerClick(url: String) {
        preSelectedZoomImagePosition.postValue(getSelectedZoomImagePosition(product.value, url))
        navigateToZoomEvent.postValue(url)
    }

    fun getSelectedZoomImagePosition(product: Product?, url: String): Int {
        val images: List<String?>? = product?.images?.filter { it.format == "zoom" }?.map { it.url }
        Log.d(TAG, "position ${images?.indexOf(url)}")
        return images?.indexOf(url) ?: 0
    }

    class Factory(
        val homeRepository: HomeRepository,
        val wishListRepository: WishListRepository,
        val productRepository: ProductRepository,
        val cartRepository: CartRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ProductViewModel(
                homeRepository,
                wishListRepository,
                productRepository,
                cartRepository
            ) as T
        }
    }

}