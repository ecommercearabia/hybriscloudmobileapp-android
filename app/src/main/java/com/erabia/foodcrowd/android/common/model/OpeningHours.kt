package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class OpeningHours(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("specialDayOpeningList")
    val specialDayOpeningList: List<SpecialDayOpening> = listOf(),
    @SerializedName("weekDayOpeningList")
    val weekDayOpeningList: List<WeekDayOpening> = listOf()
)