package com.erabia.foodcrowd.android.loyaltypoint.model
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class LoyaltyPointData(
    @SerializedName("loyaltyPointHistroy")
    @Expose
    var loyaltyPointHistroy: List<LoyaltyPointHistroy>? = listOf()
)

data class LoyaltyPointHistroy(
    @SerializedName("actionType")
    @Expose
    var actionType: ActionType? = ActionType(),
    @SerializedName("balancePoints")
    @Expose
    var balancePoints: Double? = 0.0,
    @SerializedName("convertLimitLoyaltyPoints")
    @Expose
    var convertLimitLoyaltyPoints: Double? = 0.0,
    @SerializedName("dateOfPurchase")
    @Expose
    var dateOfPurchase: String? = "",
    @SerializedName("loyaltyPointToStoreCreditConversion")
    @Expose
    var loyaltyPointToStoreCreditConversion: Double? = 0.0,
    @SerializedName("orderCode")
    @Expose
    var orderCode: String? = "",
    @SerializedName("points")
    @Expose
    var points: Double? = 0.0,
    @SerializedName("productTOLoyaltyPointsConversion")
    @Expose
    var productTOLoyaltyPointsConversion: Double? = 0.0
)

data class ActionType(
    @SerializedName("code")
    @Expose
    var code: String? = "",
    @SerializedName("name")
    @Expose
    var name: String? = ""
)