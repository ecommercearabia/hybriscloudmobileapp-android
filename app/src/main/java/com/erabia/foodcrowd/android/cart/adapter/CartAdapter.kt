package com.erabia.foodcrowd.android.cart.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.common.model.Entry
import com.erabia.foodcrowd.android.databinding.CartRowItemBinding
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.cart_row_item.view.*
import org.reactivestreams.Subscriber
import java.util.*
import java.util.concurrent.TimeUnit

class CartAdapter(val cartViewModel: CartViewModel?) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    private val TAG: String = "CartAdapter"
    private var context: Context? = null
    var entryList: MutableList<Entry>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var deleteCart: IDeleteCart

    interface IDeleteCart {
        fun deleteCart(entry: Entry?)
    }

    lateinit var cartRowItemBinding: CartRowItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        cartRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.cart_row_item, parent, false)
        return ViewHolder(cartRowItemBinding)
    }

    override fun getItemCount(): Int {
        return entryList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = entryList?.get(position)
        val product = entry?.product
        holder.bind(entry)

    }


    override fun getItemId(position: Int): Long {
        return entryList?.get(position)?.product?.code.hashCode().toLong()
    }

    fun refreshData(entryList: List<Entry>?) {
        entryList?.let {
            this.entryList?.addAll(it)
        }
        notifyDataSetChanged()
    }

    inner class ViewHolder(val itemBinding: CartRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        @SuppressLint("CheckResult")
        fun bind(entry: Entry?) {
            itemBinding.viewModel = cartViewModel
            itemBinding.entry = entry
            itemBinding.product = entry?.product
//

            itemBinding.buttonDelete.setSafeOnClickListener {
                entry?.let { it1 -> cartViewModel?.deleteEntry(it1) }

            }

//            itemBinding.buttonDelete.clicks().debounce(300, TimeUnit.MILLISECONDS).subscribe {
//
//            }
//
//            itemBinding.buttonDelete.clicks().debounce(300, TimeUnit.MILLISECONDS).subscribe(object :Subscriber) {
//
//
//            }
//            itemBinding.buttonDelete.clickWithDebounce{
//                entry?.let { it1 -> cartViewModel?.deleteEntry(it1) }
//            }

//            itemBinding.buttonDelete.clicks().filterRapidClicks().subscribe {
//                entry?.let { it1 -> cartViewModel?.deleteEntry(it1) }
//            }

            if (itemBinding.product?.discount != null) {
                itemBinding.textViewSaving.visibility = View.VISIBLE
                itemBinding.textViewSaving.text = "Saved " + itemBinding.product?.discount!!.percentage + "% Discount"
            }

            itemView.text_view_product_quantity.text = entry?.quantity.toString()
            if ((entry?.product?.stock?.stockLevel ?: 0) > 0) {
                itemBinding.clQuantityContainer.visibility = View.VISIBLE
                itemBinding.textViewOutOfStock.visibility = View.GONE
            } else {
                itemBinding.clQuantityContainer.visibility = View.GONE
                itemBinding.textViewOutOfStock.visibility = View.VISIBLE
            }

            if(entry?.product?.express == true){
                itemView.expressDeliveryCart.visibility = View.VISIBLE
                itemView.helpIconCart.visibility = View.VISIBLE
            } else {
                itemView.expressDeliveryCart.visibility = View.GONE
                itemView.helpIconCart.visibility = View.GONE
            }
        }
    }


}