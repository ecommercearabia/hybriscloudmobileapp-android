package com.erabia.foodcrowd.android.otp.model


import com.google.gson.annotations.SerializedName

data class SendOtpResponse(
    @SerializedName("description")
    val description: String? = "",
    @SerializedName("status")
    val status: Boolean? = false
)