package com.erabia.foodcrowd.android.signup.remote.repository

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.signup.model.*
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.rxkotlin.subscribeBy

class SignUpRepository(val signUpService: SignUpService) :
    Repository {
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mRegisterLiveData: SingleLiveEvent<RegisterResponse> by lazy { SingleLiveEvent<RegisterResponse>() }
    val mCountryListLiveData: SingleLiveEvent<CountryResponse> by lazy { SingleLiveEvent<CountryResponse>() }
    val liveData: SingleLiveEvent<TitleResponse> by lazy { SingleLiveEvent<TitleResponse>() }
    val nationalitiesLiveData: SingleLiveEvent<NationalitiesResponse> by lazy { SingleLiveEvent<NationalitiesResponse>() }
    val nationalitiesConfigLiveData: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    var countryCodeLiveData = SingleLiveEvent<String>()
    var isoCodeLiveData = SingleLiveEvent<String>()

    companion object {
        const val TAG: String = "SignUpRepository"
    }

    @SuppressLint("CheckResult")
    fun getTitle(): LiveData<TitleResponse> {
        signUpService.getLocalizedTitle(
            "FULL"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            liveData.value = it.body()
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
        return liveData
    }

    @SuppressLint("CheckResult")
    fun getNationalityConfig() {
        signUpService.getAppConfig("FULL").get().subscribe(this,
            onSuccess_200 = {
                nationalitiesConfigLiveData.value = it.body()?.registrationConfiguration?.nationalityConfigurations?.enabled!!
                SharedPreference.getInstance().setReferralCode(it.body()?.registrationConfiguration?.referralCodeConfigurations?.senderRewardAmount.toString() ?: "")


            }, onError_400 = {
                Log.d(TAG, it.toString())
            })
    }

    @SuppressLint("CheckResult")
    fun getNationalities(): LiveData<NationalitiesResponse> {
        signUpService.getNationalities().get()
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            nationalitiesLiveData.value = it.body()
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
        return nationalitiesLiveData
    }

    @SuppressLint("CheckResult")
    fun getCountry(): LiveData<CountryResponse> {
        val liveData = MutableLiveData<CountryResponse>()
        signUpService.getCountry(
            "FULL", "MOBILE"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            liveData.value = it.body()
                            if (it?.body()?.countries?.size == 1) {
                                it.body()?.countries?.get(0)?.isdcode?.let {
                                    countryCodeLiveData.value = it
                                }
                                it.body()?.countries?.get(0)?.isocode?.let {
                                    isoCodeLiveData.value = it
                                }
                            }
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
        return liveData
    }


    @SuppressLint("CheckResult")
    fun doRegister(
        mFirstName: String,
        mLastName: String,
        mPassword: String,
        mTitleCode: String,
        mNationality: String,
        mUid: String,
        IsoCode: String,
        mMobileNumber: String,
        mReferralCode: String
    ) {

        val registerPostData = RegisterPostData()
        registerPostData.firstName = mFirstName
        registerPostData.lastName = mLastName
        registerPostData.password = mPassword
        registerPostData.titleCode = mTitleCode
        registerPostData.mobileCountryCode = IsoCode
        registerPostData.mobileNumber = mMobileNumber
        registerPostData.uid = mUid
        registerPostData.nationality = mNationality
        registerPostData.referralCode = mReferralCode

        signUpService.register(
            registerPostData,
            "FULL"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mRegisterLiveData.value = it.body()
                            val mFirebaseAnalytics =
                                FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                            val params = Bundle()

                            params.putString(FirebaseAnalytics.Param.METHOD, "SignUp")
                            mFirebaseAnalytics.logEvent(
                                FirebaseAnalytics.Event.LOGIN,
                                params
                            )
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }
                        201 -> {
                            mRegisterLiveData.value = it.body()
                            if (SharedPreference.getInstance().getFIREBASE_TOKEN() != "") {
                                setFirebaseToken(
                                    "current",
                                    SharedPreference.getInstance().getFIREBASE_TOKEN().toString()
                                )
                            }
                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )

    }
    @SuppressLint("CheckResult")
    fun setFirebaseToken(
        userId: String,
        mobileToken: String
    ) {
        signUpService.setFirebaseToken(userId, mobileToken).get().subscribe(
            {
                when (it.code()) {
                    200 -> {

                    }
                    401 -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
//                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
            {
                Log.d(HomeRepository.TAG, " message: ${it.message}")
                Log.d(HomeRepository.TAG, " cause: ${it.cause}")

            }
        )
    }


}
