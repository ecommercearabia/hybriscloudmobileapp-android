package com.erabia.foodcrowd.android.wishlistmenu.remote.service

import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface WishListService {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/wishlists/default")
    fun getCustomerWishList(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<GetWishListResponse>>


    @PUT("rest/v2/foodcrowd-ae/users/{userId}/wishlists/{wishlistPK}/{productCode}/")
    fun addWishList(
        @Path("userId") userId: String,
        @Path("productCode") productCode: String,
        @Path("wishlistPK") wishlistPK: String
    ): Observable<Response<Void>>

     @DELETE("rest/v2/foodcrowd-ae/users/{userId}/wishlists/{wishlistPK}/{productCode}/")
    fun deleteWishItem(
        @Path("userId") userId: String,
        @Path("productCode") productCode: String,
        @Path("wishlistPK") wishlistPK: String
    ): Observable<Response<Void>>



}