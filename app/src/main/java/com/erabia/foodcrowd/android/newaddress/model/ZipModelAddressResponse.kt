package com.erabia.foodcrowd.android.newaddress.model

import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import retrofit2.Response

sealed class ZipModelAddressResponse {

    data class SuccessTitle(val data: Response<TitleResponse>) : ZipModelAddressResponse()
    data class SuccessCountry(val data: Response<Any>) : ZipModelAddressResponse()
    data class SuccessPersonalDetails(val data: Response<PersonalDetailsModel>) : ZipModelAddressResponse()
    data class Error(val t: Throwable) : ZipModelAddressResponse()
}