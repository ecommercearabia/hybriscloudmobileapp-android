package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Review(
    @SerializedName("alias")
    val alias: String = "",
    @SerializedName("comment")
    val comment: String = "",
    @SerializedName("date")
    val date: String = "",
    @SerializedName("headline")
    val headline: String = "",
    @SerializedName("id")
    val id: String = "",
    @SerializedName("principal")
    val principal: Principal = Principal(),
    @SerializedName("rating")
    val rating: Int = 0
)