package com.erabia.foodcrowd.android.verificationcode.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityVerificationCodeBinding
import com.erabia.foodcrowd.android.home.remote.service.LoginWithOtpService
import com.erabia.foodcrowd.android.home.remote.service.VerificationService
import com.erabia.foodcrowd.android.loginwithotp.remote.repository.LoginWithOtpRepository
import com.erabia.foodcrowd.android.loginwithotp.view.LoginWithOtpActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.verificationcode.remote.repository.VerificationRepository
import com.erabia.foodcrowd.android.verificationcode.viewmodel.VerificationCodeViewModel
import kotlinx.android.synthetic.main.activity_login.*


class VerificationCodeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityVerificationCodeBinding
    private lateinit var mViewModel: VerificationCodeViewModel
    var email = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_verification_code)
        val factory = VerificationCodeViewModel.Factory(
            VerificationRepository(
                RequestManager.getClient().create(VerificationService::class.java),
                RequestManager.getClient().create(CartService::class.java)
            ),
            LoginWithOtpRepository(
                RequestManager.getClient().create(LoginWithOtpService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(VerificationCodeViewModel::class.java)
        email = intent.getStringExtra(Constants.INTENT_KEY.EMAIL) ?: ""
        mViewModel.email = email
        binding.viewModel = mViewModel
        initView()
        observeProgressBar()
        observeSendOtp()
        observeVerifySuccessEvent()
        observeCloseKeyboardEvent()
        observerLogin()
        observeError()
    }

    private fun observeVerifySuccessEvent() {
        //TODO  save variable like token into sharedPreferences
    }


    private fun observerLogin() {
        mViewModel.mLoginLiveData.observe(this, Observer {

            Log.e("mohamedrefreshToken", it.refreshToken ?: "")
            SharedPreference.getInstance().setRefreshToken(it.refreshToken ?: "")
            SharedPreference.getInstance().setUser_TOKEN(it.accessToken ?: "")
            SharedPreference.getInstance().setApp_TOKEN(it.accessToken ?: "")
//            SharedPreference.getInstance().setLOGIN_User(mEmailLoginEditText.text.toString())
            AppUtil.showToastySuccess(  this, R.string.success_login)
            SharedPreference.getInstance().setPreviousLoginEmail(
                SharedPreference.getInstance().getLoginEmailFingerPrint() ?: ""
            )
//            SharedPreference.getInstance().setPreviousFromEditTextLoginEmail(mEmailEditText)
            SharedPreference.getInstance().setLOGIN_EMAIL_FINGERPRINT(email)
            SharedPreference.getInstance().setLOGIN_EMAIL(email)

            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        })
    }

    private fun observeError() {
        mViewModel.error.observe(this, Observer {
            AppUtil.showToastyError(this , it)
            Log.d(LoginWithOtpActivity.TAG, it)
        })
    }

    private fun observeCloseKeyboardEvent() {
        mViewModel.closeKeyboardEvent.observe(this, Observer {
            AppUtil.closeKeyboard()
        })
    }


    private fun observeSendOtp() {
        mViewModel.sendOtpSuccessEvent.observe(this, Observer {
            Toast.makeText(this, it.description, Toast.LENGTH_SHORT).show()
        })
    }

    private fun observeProgressBar() {
        mViewModel.progressBarVisibility.observe(this, Observer {
            binding.progressBar1.visibility = it
        })
    }

    private fun initView() {
        binding.editTextVerificationCode1.requestFocus()
        AppUtil.showKeyboard()
    }
}