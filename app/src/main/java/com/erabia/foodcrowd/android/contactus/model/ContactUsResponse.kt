package com.erabia.foodcrowd.android.contactus.model


import com.google.gson.annotations.SerializedName

data class ContactUsResponse(
    @SerializedName("container")
    var container: String? = "",
    @SerializedName("content")
    var content: String? = "",
    @SerializedName("modifiedTime")
    var modifiedTime: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("typeCode")
    var typeCode: String? = "",
    @SerializedName("uid")
    var uid: String? = "",
    @SerializedName("uuid")
    var uuid: String? = ""
)