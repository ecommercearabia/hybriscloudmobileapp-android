package com.erabia.foodcrowd.android.common.customview

import android.app.Activity
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.constraintlayout.widget.ConstraintLayout
import com.erabia.foodcrowd.android.R

/**
 * Created by Mohammad Al-Junaidi on 28,July,2021
 */
class CustomProgressBar : ConstraintLayout {
//    var isHide = true

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.custom_progress_dialog, this, true)
//       var progress_bar = findViewById(R.id.progress_bar)
    }


    fun setFreezAndVisiable(activity: Activity) {
        activity.window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )

//        Handler(Looper.getMainLooper()).postDelayed({
//            hideFreezAndVisiableProgress(activity)
//            this.visibility = View.GONE
//        }, 6000)
    }


    fun hideFreezAndVisiableProgress(activity: Activity) {

        activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }



}