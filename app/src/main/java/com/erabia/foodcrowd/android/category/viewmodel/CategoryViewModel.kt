package com.erabia.foodcrowd.android.category.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.category.model.CategoryResponse
import com.erabia.foodcrowd.android.category.model.Subcategory
import com.erabia.foodcrowd.android.category.remote.repository.CategoryRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import io.reactivex.subjects.PublishSubject

class CategoryViewModel(
    val categoryRepository: CategoryRepository,
    listingRepository: ListingRepository,
    cartRepository: CartRepository,
    val wishListRepository: WishListRepository
) : ListingViewModel(listingRepository, cartRepository) {

    val showHint: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val spanCountLiveData: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    val bottomGroupLayoutVisibility: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    var categoryList = MutableLiveData<CategoryResponse?>()
    var subCategoryList = MutableLiveData<Subcategory>()

    var action = MutableLiveData<Int>()
    var categoryId = SingleLiveEvent<String>()
    val startLoginEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val startLoginEvent2: PublishSubject<Boolean> = PublishSubject.create()
    val mErrorServer: SingleLiveEvent<String> = wishListRepository.mErrorServer
    val loadingVisibility: MutableLiveData<Int> = wishListRepository.loadingVisibility
    val badgeNumber: SingleLiveEvent<Int> = cartRepository.badgeNumber

    //    var mGetWishListLiveData: LiveData<GetWishListResponse> = wishListRepository.mGetWishListLiveData
    var mAddWishListLiveData: MutableLiveData<Void> = wishListRepository.mAddWishListLiveData
    var spanCount = 2
        set(value) {
            field = value
            spanCountLiveData.value = value
            if (value >= 3) {
                bottomGroupLayoutVisibility.value = View.GONE
            } else {
                bottomGroupLayoutVisibility.value = View.VISIBLE
            }
        }

    init {
        if (!SharedPreference.getInstance().getIsHintShown()) {
            SharedPreference.getInstance().setIsHintShown(true)
            showHint.value = View.VISIBLE
        }
    }


    fun getCategoryList() {
        categoryList =
            categoryRepository.loadCategory() as MutableLiveData<CategoryResponse?>
    }

    fun onSubCategoryClick(id: String) {
        categoryId.postValue(id)
        // action.postValue(R.id.action_navigate_to_listing)
    }

    fun addToWishList(productId: String) {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""
        if (userLogin.isNotEmpty()) {
            wishListRepository.getAndAddWishList(productId)
        } else {
            startLoginEvent.postValue(true)
        }
    }

    // remove from wishlist
    fun removeFromWishList(productId: String) {
        wishListRepository.getWishListToDelete(productId)
    }

    fun onCloseHintClick() {
        showHint.value = View.GONE
    }

    class Factory(
        private val categoryRepository: CategoryRepository,
        private val listingRepository: ListingRepository,
        private val cartRepository: CartRepository,
        private val wishListRepository: WishListRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return CategoryViewModel(
                categoryRepository,
                listingRepository,
                cartRepository,
                wishListRepository
            ) as T
        }
    }
}