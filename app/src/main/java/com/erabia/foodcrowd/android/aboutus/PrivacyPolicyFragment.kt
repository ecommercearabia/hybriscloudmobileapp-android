package com.erabia.foodcrowd.android.aboutus

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.main.MainActivity
import kotlinx.android.synthetic.main.fragment_privacy_policy.*
import java.util.*

class PrivacyPolicyFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_privacy_policy, container, false)

        return view

    }


    private inner class MyWebView : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            val url: String = request?.url.toString();
            view?.loadUrl(url)
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progress_bar?.visibility = View.GONE

            webView?.loadUrl(
                "javascript:(function() { " +
                        "var head = document.getElementsByTagName('header')[0].style.display='none'; " +
                        "var head = document.getElementsByTagName('footer')[0].style.display='none'; " +
                        "})()"
            );
//            layoutView.web_view_about_us.loadUrl("javascript:document.getElementsByClassName('content-page-design').style.display ='none';");

        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        when (LocaleHelper.getSummary(context as MainActivity)) {
            Constants.ENGLISH -> {
                MyApplication.getInstance()?.changeLocale("en");
                changeLocale("en")
            }
            Constants.ARABIC -> {
                MyApplication.getInstance()?.changeLocale("ar");
                changeLocale("ar")
            }
        }
        progress_bar?.visibility = View.VISIBLE

        webView.webViewClient = MyWebView()
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK

        webView.loadUrl(getString(R.string.privacy_url))
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    fun changeLocale(lang: String?) {
        val mLocale = Locale(lang)
        val config = this.resources
            ?.configuration
        Locale.setDefault(mLocale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config?.setLocale(mLocale)
        } else config?.locale = mLocale
        this?.resources?.updateConfiguration(config, this?.resources?.displayMetrics)
    }
}