package com.erabia.foodcrowd.android.splash

import android.annotation.SuppressLint
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import androidx.core.content.ContentProviderCompat.requireContext
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.main.MainActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging

class SplashActivity : AppCompatActivity() {
    companion object {
        const val TAG = "SplashActivity"
    }

    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val deviceId = Settings.Secure.getString(
            contentResolver, Settings.Secure.ANDROID_ID
        )

        SharedPreference.getInstance().setSIGNATURE_ID(deviceId)

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("FireBase ", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result

            // Log and toast
//            val msg = getString(R.string.update, token)
            Log.d("FireBase ", token.toString())
//            Toast.makeText(baseContext, token.toString(), Toast.LENGTH_SHORT).show()
        })
        mHandler.postDelayed(mRunnable, mDelay)

    }


    private val mDelay: Long = 2000;
    private var mHandler = Handler()

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {

            val intent = Intent(this, MainActivity::class.java)
            try {
//                        val notification: java.util.HashMap<*, *>? =
//                            Gson().fromJson(
//                                getIntent().extras?.get("data").toString(),
//                                HashMap::class.java
//                            )
                intent.putExtra(
                    Constants.INTENT.NOTIFICATION_TYPE,
                    getIntent()?.extras?.get("type")?.toString()
                )
                intent.putExtra(
                    Constants.INTENT_KEY.PRODUCT_CODE,
                    getIntent()?.extras?.get("id")?.toString()
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)

                Log.d(TAG, "run: after putExtra type : ${getIntent()?.extras?.get("type")}")
                Log.d(TAG, "run: after putExtra id : ${getIntent()?.extras?.get("id")}")
                intent.putExtra("testKey", "testValue")
            } catch (e: Exception) {
                Log.d(TAG, "run: ${e.message}")
            }

            startActivity(intent)
            finish()
        }
    }
}