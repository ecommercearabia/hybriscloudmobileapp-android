package com.erabia.foodcrowd.android.signup.model

import com.google.gson.annotations.SerializedName

data class NationalitiesResponse(
    @SerializedName("nationalities")
    val nationalities: ArrayList<Nationalities>? = arrayListOf()
) {
    data class Nationalities(
        @SerializedName("code")
        val code: String? = "",
        @SerializedName("name")
        val name: String? = ""

    ){
        override fun toString(): String {
            return name ?: ""
        }
    }
}