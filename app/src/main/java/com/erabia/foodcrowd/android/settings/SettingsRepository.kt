package com.erabia.foodcrowd.android.settings

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository

import io.reactivex.rxkotlin.subscribeBy

class SettingsRepository(val settingsService: SettingsService) :
    Repository {
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val updateSignatureLiveData: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    companion object {
        const val TAG: String = "ForgetRepository"
    }


    @SuppressLint("CheckResult")
    fun updateSignature(signatureId: String) {
        settingsService.setSignature(Constants.CURRENT, signatureId).get()
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            updateSignatureLiveData.value = "Success"
                        }
                        201 -> {
                            updateSignatureLiveData.value = "Success"
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
    }


}
