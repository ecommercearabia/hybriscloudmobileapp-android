package com.erabia.foodcrowd.android.common.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TimeSlotData implements Parcelable {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("selected")
    @Expose
    private Boolean selected;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("valid")
    @Expose
    private Boolean valid;

    protected TimeSlotData(Parcel in) {
        date = in.readString();
        byte tmpSelected = in.readByte();
        selected = tmpSelected == 0 ? null : tmpSelected == 1;
        time = in.readString();
        byte tmpValid = in.readByte();
        valid = tmpValid == 0 ? null : tmpValid == 1;
    }

    public static final Creator<TimeSlotData> CREATOR = new Creator<TimeSlotData>() {
        @Override
        public TimeSlotData createFromParcel(Parcel in) {
            return new TimeSlotData(in);
        }

        @Override
        public TimeSlotData[] newArray(int size) {
            return new TimeSlotData[size];
        }
    };

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(date);
        parcel.writeByte((byte) (selected == null ? 0 : selected ? 1 : 2));
        parcel.writeString(time);
        parcel.writeByte((byte) (valid == null ? 0 : valid ? 1 : 2));
    }
}
