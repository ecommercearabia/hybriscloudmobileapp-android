package com.erabia.foodcrowd.android.menu.remote

import com.erabia.foodcrowd.android.menu.model.ConsentTemplate
import com.erabia.foodcrowd.android.menu.model.ConsentTemplateList
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ConsentService {
    @GET("rest/v2/foodcrowd-ae/users/{userId}/consenttemplates")
    fun getConsents(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<ConsentTemplateList>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/consents")
    fun giveConsent(
        @Path("userId") userId: String,
        @Query("consentTemplateId") consentTemplateId: String,
        @Query("consentTemplateVersion") consentTemplateVersion: String

    ): Observable<Response<ConsentTemplate>>

    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/consents/{consentCode}")
    fun deleteConsent(
        @Path("userId") userId: String,
        @Path("consentCode") consentCode: String
    ): Observable<Response<Unit>>
}