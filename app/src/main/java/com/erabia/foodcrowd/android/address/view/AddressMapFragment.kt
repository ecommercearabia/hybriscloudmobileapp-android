package com.erabia.foodcrowd.android.address.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.di.DaggerIAddressComponent
import com.erabia.foodcrowd.android.address.di.IAddressComponent
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentAddressMapBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import java.util.*

class AddressMapFragment : Fragment() {

    val daggerComponent: IAddressComponent by lazy {
        DaggerIAddressComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)

    }

    val addressViewModel: AddressViewModel by activityViewModels()
    private lateinit var binding: FragmentAddressMapBinding
    lateinit var mMap: GoogleMap
    val MY_PERMISSIONS_REQUEST_LOCATION = 99
    lateinit var fusedLocationClient: FusedLocationProviderClient
    var lat: Double = 0.0
    var lang: Double = 0.0
    var mContext: Context? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = context
    }

    private val callback = OnMapReadyCallback { googleMap ->
        mMap = googleMap

        googleMap.uiSettings.isMyLocationButtonEnabled = true
        if (checkLocationPermission()) {

            getLocationData(googleMap)
        }

        googleMap.setOnCameraIdleListener(GoogleMap.OnCameraIdleListener {
            lat = googleMap.cameraPosition.target.latitude
            lang = googleMap.cameraPosition.target.longitude
            setGeoAddress(lat, lang, googleMap)
           // binding.imageViewPin.visibility = View.GONE
        })
        googleMap.setOnCameraMoveListener(GoogleMap.OnCameraMoveListener {
            binding.imageViewPin.visibility = View.VISIBLE
        })


    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_address_map, container, false)
        addressViewModel.initialization(
            daggerComponent.getAddressRepo(),
            daggerComponent.getSignUpRepo()
        )
        fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(activity as MainActivity)
        binding.addressVM = addressViewModel
        arguments?.let { addressViewModel.bundle = it }
        Log.d("bundel", arguments?.let { it.getString(Constants.COME_FROM) } ?:"")
        addressViewModel.bundle.getString(Constants.COME_FROM)?.let { Log.d("bundel", it) }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?

        mapFragment?.getMapAsync(callback)
        observeConfirmAddress()
        observePopEvent()
    }


    private fun observePopEvent() {
        addressViewModel.popEvent.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            findNavController().popBackStack()
        })
        addressViewModel.outOfRangeObserver.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {

                AppUtil.showToastyWarning(context ,R.string.out_of_the_range)

            })

    }

    private fun observeConfirmAddress() {
        addressViewModel.action.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(it ?: 0)

        })
    }

    fun checkLocationPermission(): Boolean {
        return if (ActivityCompat.checkSelfPermission(
                mContext as MainActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                mContext as MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) { // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    mContext as MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) { // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                AlertDialog.Builder(mContext as MainActivity)
                    .setTitle("Set your location")
                    .setMessage("location permission")
                    .setPositiveButton("ok",
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            //Prompt the user once explanation has been shown
                            requestPermissions(

                                arrayOf(
                                    Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION
                                ),
                                MY_PERMISSIONS_REQUEST_LOCATION
                            )
                        })
                    .create()
                    .show()
            } else { // No explanation needed, we can request the permission.
                requestPermissions(
                    arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    MY_PERMISSIONS_REQUEST_LOCATION
                )
            }
            false
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_LOCATION -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(
                            mContext as MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {

                        AppUtil.showToastyWarning(context ,R.string.permission_granted)
                        getLocationData(mMap)

                    }
                } else { // permission denied, boo! Disable the
// functionality that depends on this permission.
                }
                return
            }
        }
    }


    fun setGeoAddress(lat: Double, lang: Double, mMap: GoogleMap) {
        val geo = Geocoder(activity?.getApplicationContext(), Locale.getDefault())
        val addresses: List<Address> = geo.getFromLocation(lat, lang, 1)
        if (addresses.isEmpty()) {
            AppUtil.showToastyWarning(context ,R.string.waiting_for_location)
            addressViewModel.outOfRang = false

        } else {
            if (addresses.size > 0) {

                val adminArea = addresses.get(0).adminArea ?: ""
                if (adminArea.equals("Abu Dhabi")) {
                    addressViewModel.setGeoAddressData(addresses.get(0))
                    addressViewModel.outOfRang = true

                } else {
                    addressViewModel.outOfRang = false

                }
                Log.d(
                    "Map_location",
                    addresses[0].getFeatureName().toString() + ", " + addresses[0].getAddressLine(
                        0
                    ) + ", " + addresses[0].getAdminArea() + ", " + addresses[0].getCountryName()
                )

            }

        }
    }

    @SuppressLint("MissingPermission")
    fun getLocationData(mMap: GoogleMap) {


        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                val geo = Geocoder(activity?.getApplicationContext(), Locale.getDefault())
                val addresses: List<Address> =
                    geo.getFromLocation(location.latitude, location.longitude, 1)

                if (addresses.isEmpty()) {
                    AppUtil.showToastyWarning(context ,R.string.waiting_for_location)
                    addressViewModel.outOfRang = false
                } else {
                    if (addresses.size > 0) {

                        val adminArea = addresses.get(0).adminArea ?: ""
                        if (adminArea.equals("Abu Dhabi")) {
                            val currentLatLng = LatLng(location.latitude, location.longitude)
                            mMap.animateCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    currentLatLng,
                                    15f
                                )
                            )
                            addressViewModel.outOfRang = true

                        } else {
                            val currentLatLng = LatLng(24.466667, 54.366669)
                            mMap.animateCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    currentLatLng,
                                    15f
                                )
                            )
                            addressViewModel.outOfRang = false

                        }

                    }

                }


            }
        }
    }
}