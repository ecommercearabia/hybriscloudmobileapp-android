package com.erabia.foodcrowd.android.common.util

import android.content.Context
import android.os.Handler
import androidx.viewpager.widget.ViewPager


class PageSwitcher(val context: Context, val viewPager: ViewPager) {
    var bannerCurrentPage = 0
    var totalPages: Int? = 0

    fun execute(seconds: Int) {
        val handler = Handler()
        val runnable = Runnable {
            if (bannerCurrentPage >= ((totalPages ?: 0)) ?: 0) {
                bannerCurrentPage = 0
            } else {
                bannerCurrentPage += 1
            }
            viewPager?.currentItem = bannerCurrentPage

        }

        viewPager?.addOnPageChangeListener(object :
            ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                bannerCurrentPage = position
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 2000);

            }

        })
        handler.postDelayed(runnable, 2000);


    }

}