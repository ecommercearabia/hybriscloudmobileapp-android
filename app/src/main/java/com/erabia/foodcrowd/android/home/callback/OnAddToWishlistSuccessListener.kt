package com.erabia.foodcrowd.android.home.callback

interface OnAddToWishlistSuccessListener {
    fun onAddToWishlistSuccess()
}