package com.erabia.foodcrowd.android.personaldetails.remote.service

import com.erabia.foodcrowd.android.common.model.SetPaymentModeResponse
import com.erabia.foodcrowd.android.common.model.SiteConfig
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface PersonalDetailsService {

    @GET("rest/v2/foodcrowd-ae/users/{userId}")
    fun getCustomerProfile(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<PersonalDetailsModel>>


    @GET("rest/v2/foodcrowd-ae/titles")
    fun getLocalizedTitle(
        @Query("fields") fields: String
    ): Observable<Response<TitleResponse>>

    @GET("rest/v2/foodcrowd-ae/nationalities")
    fun getNationalities(
    ): Observable<Response<NationalitiesResponse>>

    @GET("rest/v2/foodcrowd-ae/config")
    fun getAppConfig(
        @Query("fields") fields: String
    ): Observable<Response<SiteConfig>>

    @GET("rest/v2/foodcrowd-ae/countries")
    fun getCountry(
        @Query("fields") fields: String,
        @Query("type") type: String
    ): Observable<Response<CountryResponse>>


    @PUT("rest/v2/foodcrowd-ae/users/{userId}")
    fun updateProfile(
        @Path("userId") userId: String,
        @Query("lang") lang: String,
        @Body mPersonalDetailsModel: PersonalDetailsModel?
    ): Observable<Response<Void>>

}