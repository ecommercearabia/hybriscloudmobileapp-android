//package com.erabia.foodcrowd.android.home.view
//
//import android.os.Bundle
//import androidx.fragment.app.Fragment
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.databinding.DataBindingUtil
//import com.erabia.foodcrowd.android.R
//import com.erabia.foodcrowd.android.home.callback.OnAddToWishlistSuccessListener
//import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
//import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel
//import com.google.android.material.bottomsheet.BottomSheetDialogFragment
//import com.erabia.foodcrowd.android.databinding.FragmentAddToWishlistBottomSheetBinding as Binding
//
//class AddToWishlistBottomSheet : BottomSheetDialogFragment() {
//    var onAddToWishlistSuccessListener: OnAddToWishlistSuccessListener? = null
//
//    private lateinit var productId: String
////    lateinit var productItemViewModel: ProductViewModel
////    var variantItemAdapter: VariantItemAdapter? = null
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        productId = arguments?.getString("Product_Id") ?: ""
//
//    }
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        // Inflate the layout for this fragment
////        return inflater.inflate(R.layout.fragment_add_to_wishlist_bottom_sheet, container, false)
//        val binding = DataBindingUtil.inflate<Binding>(
//            LayoutInflater.from(context),
//            R.layout.fragment_add_to_wishlist_bottom_sheet,
//            container,
//            false
//        )
////        binding.viewModel = productItemViewModel
//
//        return binding.root
//
//    }
//
//}