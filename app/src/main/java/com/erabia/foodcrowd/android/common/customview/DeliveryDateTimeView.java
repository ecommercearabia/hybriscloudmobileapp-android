package com.erabia.foodcrowd.android.common.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.erabia.foodcrowd.android.R;
import com.erabia.foodcrowd.android.checkout.model.AddressTimeSlot;
import com.erabia.foodcrowd.android.checkout.model.Period;
import com.erabia.foodcrowd.android.checkout.model.TimeSlotDay;
import com.erabia.foodcrowd.android.checkout.model.TimeSlotRequest;
import com.erabia.foodcrowd.android.common.adapter.DeliveryDayAdapter;
import com.erabia.foodcrowd.android.common.adapter.DeliveryTimeAdapter;

import org.jetbrains.annotations.Nullable;

import java.util.List;


/**
 * Created by dev-10 on 10/5/17.dd
 */

public class DeliveryDateTimeView extends RelativeLayout implements DeliveryDayAdapter.OnDayClickListener,
        DeliveryTimeAdapter.OnTimeClickListener {

    private NonScrollGridView timeNonScrollGridView;
    private List<AddressTimeSlot> checkOutDeliveryDayList;
    private int currentIndex = 0;
    private int timeTextColor;
    private RecyclerView dateRecyclerView;
    private RecyclerView timeListView;
    private List<TimeSlotDay> timeSlotList;
    private OnTimeSlotListener onTimeSlotListener;
    private TimeSlotDay timeSlotDay;
    private DeliveryDayAdapter deliveryDayAdapter;
    private DeliveryTimeAdapter deliveryTimeAdapter;

    public interface OnTimeSlotListener {
        void onTimeSlotClickListener(TimeSlotRequest slot);
    }


    public void setListener(OnTimeSlotListener onTimeSlotListener) {
        this.onTimeSlotListener = onTimeSlotListener;
    }

    public DeliveryDateTimeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.erabiaTime,
                0, 0);
        int previousColor = typedArray.getColor(R.styleable.erabiaTime_previousColor,
                ContextCompat.getColor(context, R.color.deliveryDayPreviousBtnColor));
        int nextColor = typedArray.getColor(R.styleable.erabiaTime_nextColor,
                ContextCompat.getColor(context, R.color.deliveryDayNextBtnColor));
        timeTextColor = typedArray.getColor(R.styleable.erabiaTime_timeTextColor,
                ContextCompat.getColor(context, R.color.deliveryTimeTextColor));
        int columnsNumber = typedArray.getInteger(R.styleable.erabiaTime_columnsNumber, 2);
        typedArray.recycle();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.merg_date_time, this, true);
        timeNonScrollGridView = findViewById(R.id.non_scroll_view_time_grid);
        timeNonScrollGridView.setNumColumns(columnsNumber);
        dateRecyclerView = findViewById(R.id.recycler_view_day);
        timeListView = findViewById(R.id.recycler_view_time);
    }

    public void setTimeSlot(List<TimeSlotDay> timeSlotList) {
        this.checkOutDeliveryDayList = checkOutDeliveryDayList;
        this.timeSlotList = timeSlotList;
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        dateRecyclerView.setLayoutManager(linearLayoutManager);
        deliveryDayAdapter = new DeliveryDayAdapter(getContext(), timeSlotList);
        deliveryDayAdapter.setListener(DeliveryDateTimeView.this);
        dateRecyclerView.setAdapter(deliveryDayAdapter);

        final LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false);
        timeListView.setLayoutManager(linearLayoutManager2);
        if (timeSlotList != null && !timeSlotList.isEmpty()) {
            deliveryTimeAdapter = new DeliveryTimeAdapter();
            deliveryTimeAdapter.setSlotList(this.timeSlotList.get(0).getPeriods());
            deliveryTimeAdapter.setListener(this);
            timeListView.setAdapter(deliveryTimeAdapter);
        }
    }

    public void setSelectedTimeSlot(String date, String periodCode) {
        if (deliveryDayAdapter != null) {
            deliveryDayAdapter.setSelectedDate(date);
        }
        if (deliveryTimeAdapter != null) {
            deliveryTimeAdapter.setSelectedPeriodCode(periodCode);
        }
    }

    @Override
    public void onDayClickListener(int position) {
//        DeliveryTimeAdapter deliveryTimeAdapter = new DeliveryTimeAdapter();
        if (deliveryTimeAdapter != null) {
            deliveryTimeAdapter.setPreSelectedPosition(-1);
            deliveryTimeAdapter.setSlotList(this.timeSlotList.get(position).getPeriods());
            deliveryTimeAdapter.setListener(this);
            timeListView.setAdapter(deliveryTimeAdapter);
            this.timeSlotDay = this.timeSlotList.get(position);
        }
    }

    @Override
    public void onTimeClickListener(@Nullable Period period) {
        if (onTimeSlotListener != null && timeSlotDay != null && period != null) {
            TimeSlotRequest slot = new TimeSlotRequest(
                    timeSlotDay.getDate(),
                    timeSlotDay.getDay(),
                    period.getEnd(),
                    period.getCode(),
                    period.getStart()
            );
            onTimeSlotListener.onTimeSlotClickListener(slot);
        }

    }
}