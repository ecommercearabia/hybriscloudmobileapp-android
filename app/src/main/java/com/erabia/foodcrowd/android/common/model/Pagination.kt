package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class Pagination(
    @SerializedName("currentPage")
    val currentPage: Int? = null,
    @SerializedName("pageSize")
    val pageSize: Int? = null,
    @SerializedName("sort")
    val sort: String? = null,
    @SerializedName("totalPages")
    val totalPages: Int? = null,
    @SerializedName("totalResults")
    val totalResults: Int? = null
)