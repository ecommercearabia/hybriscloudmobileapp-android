package com.erabia.foodcrowd.android.login.view

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.util.Log
import android.view.Window
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.SharedPreference
import java.util.concurrent.Executor

/**
 * Created by Mohammad Al-Junaidi on 22,March,2021
 */
class BiometricManager(context: Context) {
    private lateinit var executor: Executor
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var promptInfo: BiometricPrompt.PromptInfo
    var mEmailEditText: String = ""

    companion object {
        var onBiometricResult: BiometricCallBack? = null
        var onLoginBiometricResult: LoginBiometricCallBack? = null

    }

    val mContext = context
//    val mIsFirstTimeShow = isFirstTimeShow

    interface BiometricCallBack {
        fun onBiometricCallBack(message: String)
    }


    interface LoginBiometricCallBack {
        fun onLoginBiometricCallBack(mEmail: String)
    }


    fun getStatusBiometric() {
        val biometricManager = mContext.let { BiometricManager.from(it) }

        when (biometricManager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)) {

            BiometricManager.BIOMETRIC_SUCCESS -> {
                if (!SharedPreference.getInstance().isFingerPrintEnabled())
                    showUpdateSignatureAlertDialog(
                        mContext.getString(R.string.alert_biometric_message),
                        mContext.getString(R.string.biometric_credential)
                    )
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.")

            }
            BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                Log.e("MY_APP_TAG", "No biometric features available on this device.")
                SharedPreference.getInstance().setFingerPrintEnabled(false)

            }
            BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.")
                SharedPreference.getInstance().setFingerPrintEnabled(false)
            }
            BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                // Prompts the user to create credentials that your app accepts.
                if (!SharedPreference.getInstance().isFingerPrintEnabled())
                    showUpdateSignatureAlertDialog(
                        mContext.getString(R.string.alert_biometric_message),
                        mContext.getString(R.string.biometric_credential)
                    )
                Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED")
            }
            else -> {
                // Check if we're running on Android 6.0 (M) or higher
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //Fingerprint API only available on from Android 6.0 (M)
                    val fingerprintManager =
                        mContext.getSystemService(Context.FINGERPRINT_SERVICE) as FingerprintManager
                    if (!fingerprintManager.isHardwareDetected) {
                        SharedPreference.getInstance().setFingerPrintEnabled(false)
                    } else if (!fingerprintManager.hasEnrolledFingerprints()) {
//                            SharedPreference.getInstance().setFingerPrintEnabled(false)
                        if (!SharedPreference.getInstance().isFingerPrintEnabled())
                            showUpdateSignatureAlertDialog(
                                mContext.getString(R.string.alert_biometric_message),
                                mContext.getString(R.string.biometric_credential)
                            )
                        // User hasn't enrolled any fingerprints to authenticate with
                    } else {


                        if (!SharedPreference.getInstance().isFingerPrintEnabled())
                            showUpdateSignatureAlertDialog(
                                mContext.getString(R.string.alert_biometric_message),
                                mContext.getString(R.string.biometric_credential)
                            )
                        // Everything is ready for fingerprint authentication
                    }
                }

            }
        }


    }

    @SuppressLint("HardwareIds")
    private fun showUpdateSignatureAlertDialog(message: String, title: String) {

        if (SharedPreference.getInstance().isLoginFirstTime()) {
            val dialog: AlertDialog.Builder? = mContext.let { AlertDialog.Builder(it) }
            dialog?.setMessage(message)
            dialog?.setCancelable(false)
            dialog?.setTitle(title)
            dialog?.setPositiveButton(
                mContext.getString(R.string.yes)
            ) { _, _ ->
                SharedPreference.getInstance().setIsLoginFirstTime(false)
                onBiometricResult?.onBiometricCallBack("Success")

//            val deviceId = SharedPreference.getInstance().getSIGNATURE_ID()
//            viewModel.updateSignature(deviceId ?: "")
            }
            dialog?.setNegativeButton(
                mContext.getString(R.string.close)
            ) { _, _ ->
                dialog.create().dismiss()
                SharedPreference.getInstance().setIsLoginFirstTime(false)
            }
            val alertDialog: AlertDialog? = dialog?.create()
            alertDialog?.show()
        }
    }


    fun makeLoginBiometric(fragment: FragmentActivity) {
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {

            executor = ContextCompat.getMainExecutor(mContext)
            biometricPrompt = BiometricPrompt(fragment, executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(
                        errorCode: Int,
                        errString: CharSequence
                    ) {
                        super.onAuthenticationError(errorCode, errString)
//                        Toast.makeText(
//                            mContext,
//                            "Authentication error: $errString", Toast.LENGTH_SHORT
//                        )
//                            .show()
                    }

                    override fun onAuthenticationSucceeded(
                        result: BiometricPrompt.AuthenticationResult
                    ) {
                        super.onAuthenticationSucceeded(result)
                        showEnterEmailDialog()
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()
//                        Toast.makeText(
//                            mContext, "Authentication failed",
//                            Toast.LENGTH_SHORT
//                        ).show()
                    }
                })

            promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle(mContext.getString(R.string.enter_biometrice_title))
                .setSubtitle(mContext.getString(R.string.enter_biometric_subtitle))
                .setDeviceCredentialAllowed(true)
                .build()
            biometricPrompt.authenticate(promptInfo)

        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.Q) {
            executor = ContextCompat.getMainExecutor(mContext)
            biometricPrompt = BiometricPrompt(fragment, executor,
                object : BiometricPrompt.AuthenticationCallback() {
                    override fun onAuthenticationError(
                        errorCode: Int,
                        errString: CharSequence
                    ) {
                        super.onAuthenticationError(errorCode, errString)

                    }

                    override fun onAuthenticationSucceeded(
                        result: BiometricPrompt.AuthenticationResult
                    ) {
                        super.onAuthenticationSucceeded(result)


                        showEnterEmailDialog()
                    }

                    override fun onAuthenticationFailed() {
                        super.onAuthenticationFailed()

                    }
                })

            promptInfo = BiometricPrompt.PromptInfo.Builder()
                .setTitle(mContext.getString(R.string.enter_biometrice_title))
                .setSubtitle(mContext.getString(R.string.enter_biometric_subtitle))
//                .setNegativeButtonText("Use account password")
                .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
                .build()
            biometricPrompt.authenticate(promptInfo)
        }
    }


    fun showEnterEmailDialog() {
        if (!SharedPreference.getInstance().getSIGNATURE_ID().isNullOrEmpty()) {
            mEmailEditText = SharedPreference.getInstance().getLoginEmailFingerPrint() ?: ""

            onLoginBiometricResult?.onLoginBiometricCallBack(mEmailEditText)
//                mViewModel.doFingerPrintLogin(mEmailEditText)
        }

//        val dialog = Dialog(mContext, R.style.CustomLoginDialog)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setCancelable(false)
//        dialog.setContentView(R.layout.enter_email_dialog)
////        val mLoginEmailDialogTextInputEditText = dialog.findViewById(R.id.mLoginEmailDialogTextInputEditText) as TextInputEditText
//
//        val mLoginEmailDialogButton =
//            dialog.findViewById(R.id.mLoginEmailDialogButton) as AppCompatTextView
//        val mCancelEmailDialogButton =
//            dialog.findViewById(R.id.mCancelEmailDialogButton) as AppCompatTextView
//
//
//        val mEmailDialogEditText =
//            dialog.findViewById(R.id.mLoginEmailDialogTextInputEditText) as AppCompatEditText
//
//
//
//        mLoginEmailDialogButton.setOnClickListener {
//
//            if (!SharedPreference.getInstance().getSIGNATURE_ID().isNullOrEmpty()) {
//                mEmailEditText = mEmailDialogEditText.text.toString()
//
//                onLoginBiometricResult?.onLoginBiometricCallBack(mEmailEditText)
////                mViewModel.doFingerPrintLogin(mEmailEditText)
//            }
//
//
//        }
//        mCancelEmailDialogButton.setOnClickListener { dialog.dismiss() }
//        dialog.show()
    }


}