package com.erabia.foodcrowd.android.category.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class PlpPictureResponsive(
    @SerializedName("format")
    var format: String? = null,
    @SerializedName("url")
    var url: String? = null
)