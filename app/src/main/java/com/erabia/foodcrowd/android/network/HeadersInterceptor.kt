package com.erabia.foodcrowd.android.network

import android.util.Log
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class HeadersInterceptor : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        var request = chain.request()
        // HttpUrl url = request.url().newBuilder().addQueryParameter("lang", "en").build();
        //TODO Remove static email after finish login api


        if (chain.request().method.equals("GET")){
            if (LocaleHelper.getLanguage(MyApplication.getContext()!!).equals("ar")){
                val url = request.url.newBuilder().addQueryParameter("lang", "ar").build()
                request = request
                    .newBuilder()
                    .url(url = url).addHeader(Constants.API_KEY.APP_ID, "Android")
                    .addHeader(Constants.API_KEY.APP_VERSION, "100")
                    .header(Constants.API_KEY.AUTHORIZATION, "Bearer " + (SharedPreference.getInstance().getUserToken() ?: SharedPreference.getInstance().getAppToken()))
                    .build()
            } else {
                val url = request.url.newBuilder().addQueryParameter("lang", "en").build()
                request = request
                    .newBuilder()
                    .url(url = url).addHeader(Constants.API_KEY.APP_ID, "Android")
                    .addHeader(Constants.API_KEY.APP_VERSION, "100")
                    .header(Constants.API_KEY.AUTHORIZATION, "Bearer " + (SharedPreference.getInstance().getUserToken() ?: SharedPreference.getInstance().getAppToken()))
                    .build()
            }
        }else{
            request = request
                .newBuilder()
                .addHeader(Constants.API_KEY.APP_ID, "Android")
                .addHeader(Constants.API_KEY.APP_VERSION, "100")
                .header(Constants.API_KEY.AUTHORIZATION, "Bearer " + (SharedPreference.getInstance().getUserToken() ?: SharedPreference.getInstance().getAppToken()))
                .build()
        }

//        var mToken:String
//        if (SharedPreference.getInstance().getRefreshToken().toString().isNotEmpty()){
//        mToken = SharedPreference.getInstance().getRefreshToken() ?: ""
//
//        }
//        else{
//            mToken=SharedPreference.getInstance().getAppToken() ?: ""
//        }
    //TODO remove static token
    /*    request.newBuilder().addHeader(Constants.API_KEY.APP_ID, "Android")
            .addHeader(Constants.API_KEY.APP_VERSION, "100")
            .header(Constants.API_KEY.AUTHORIZATION, "Bearer " + (SharedPreference.getInstance().getUserToken() ?: SharedPreference.getInstance().getAppToken()))
         //   .header(Constants.API_KEY.AUTHORIZATION, "Bearer " + "040a21e6-9cad-4724-be9d-eb89bcbbe002")*/
//            .addHeader(Constants.API_KEY.ENV, BuildConfig.ENV)
//            .addHeader(Constants.API_KEY.USER_ID, SharedPreference.getInstance().getLoginEmail())
//            .addHeader(Constants.API_KEY.TOKEN, SharedPreference.getInstance().getLoginToken())
//            .addHeader(
//                Constants.API_KEY.LANG,
//                LocaleHelper.getLanguage(MyApplication.getContext()!!)
//            )
        return chain.proceed(request)
    }
}

