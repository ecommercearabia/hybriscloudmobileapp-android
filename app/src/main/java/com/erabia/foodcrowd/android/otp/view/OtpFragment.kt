package com.erabia.foodcrowd.android.otp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.AppUtil

import com.erabia.foodcrowd.android.databinding.ActivityOtpBinding
import com.erabia.foodcrowd.android.databinding.FragmentOtpBinding
import com.erabia.foodcrowd.android.home.remote.service.LoginService
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddAddressViewModel
import com.erabia.foodcrowd.android.otp.remote.repository.OtpRepository
import com.erabia.foodcrowd.android.otp.remote.service.OtpService
import com.erabia.foodcrowd.android.otp.viewmodel.OtpVIewModel
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_otp.*


class OtpFragment : Fragment() {

    private lateinit var viewModel: OtpVIewModel
    private lateinit var binding: FragmentOtpBinding
    var compositeDisposable = CompositeDisposable()
    var mCode: String = ""
    var fromFragment: String = ""




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        fromFragment = arguments?.getString("fromFragment") ?: ""


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding =
        DataBindingUtil.inflate(inflater, R.layout.fragment_otp, container, false)
        val personalDetailsModel = arguments?.get("personalDetailsModel") as PersonalDetailsModel

        val factory = OtpVIewModel.Factory(
            OtpRepository(RequestManager.getClient().create(OtpService::class.java)),
            SignUpRepository( RequestManager.getClient().create(SignUpService::class.java)),
            LoginRepository( RequestManager.getClient().create(LoginService::class.java)
                ,  RequestManager.getClient().create(CartService::class.java))
        )
        viewModel = ViewModelProvider(this, factory).get(OtpVIewModel::class.java)

        binding.lifecycleOwner = this
        binding.mOtpVIewModel = viewModel

        viewModel.sendOtp(personalDetailsModel.mobileCountry?.isocode ?: "", personalDetailsModel.mobileNumber ?: "")


        binding.mVerifyButton.clicks().filterRapidClicks().subscribe {
            mCode = mPinEditText.text.toString()
            viewModel.verifyOtp(mCode, personalDetailsModel.mobileCountry?.isocode ?: "", personalDetailsModel.mobileNumber ?: "")

        }.addTo(compositeDisposable)

        binding.mSignUpTextView.clicks().filterRapidClicks().subscribe {
            viewModel.sendOtp(personalDetailsModel.mobileCountry?.isocode ?: "", personalDetailsModel.mobileNumber ?: "")
        }.addTo(compositeDisposable)

        binding.mBackImageView.clicks().filterRapidClicks().subscribe {
            findNavController().popBackStack()

        }.addTo(compositeDisposable)


        viewModel.mErrorOtpEvent.observe(this, Observer {
            AppUtil.showToastyError(requireContext() , it)

        })

        viewModel.mSendOtpLiveData.observe(viewLifecycleOwner, Observer {
            binding.mVerifyButton.isEnabled = true

        })
        viewModel.mErrorSignUpEvent.observe(this, Observer {
            AppUtil.showToastyError(requireContext() , it)

        })
        viewModel.mErrorLoginEvent.observe(this, Observer {
            AppUtil.showToastyError(requireContext() , it)
        })


        viewModel.mUpdateProfileLiveData.observe(viewLifecycleOwner, Observer {

//            AppUtil.showToastySuccess(context, R.string.success)
            if(!fromFragment.isNullOrEmpty()) {
                findNavController().navigate(
                    R.id.action_navigate_to_cart
                )
            }
            else {
                findNavController().popBackStack(findNavController().graph.startDestination, false)
            }

        })

        viewModel.mVerifyOtpLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.updateProfile(personalDetailsModel)
        })




        return binding.root
    }
}

