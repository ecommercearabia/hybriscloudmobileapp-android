package com.erabia.foodcrowd.android.managecards.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.FragmentManageSavedCardsBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.managecards.adapter.ManageCardsAdapter
import com.erabia.foodcrowd.android.managecards.remote.repository.ManageCardsRepository
import com.erabia.foodcrowd.android.managecards.remote.service.IManageCards
import com.erabia.foodcrowd.android.managecards.viewmodel.ManageCardsViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import androidx.lifecycle.Observer

class ManageCardsFragment : Fragment() {

    lateinit var binding: FragmentManageSavedCardsBinding
    lateinit var manageCardsViewModel: ManageCardsViewModel
    private var manageCardsAdapter: ManageCardsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val factory = ManageCardsViewModel.Factory(
            ManageCardsRepository(
                RequestManager.getClient().create(IManageCards::class.java)
            )
        )

        manageCardsViewModel =
            ViewModelProvider(this, factory).get(ManageCardsViewModel::class.java)
        manageCardsViewModel.getSavedCardsList()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).toolbar.title = ""

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_manage_saved_cards,
            container,
            false
        )
        observeCardsList()
        observeerrorResponse()
        observeProgressBar()
        initView()
        return binding.root
    }



    private fun initView() {
        manageCardsAdapter = ManageCardsAdapter(manageCardsViewModel)
        manageCardsAdapter?.setHasStableIds(true)
        binding?.recyclerViewCard?.adapter = manageCardsAdapter
    }

    private fun observeCardsList() {
        manageCardsViewModel?.cardList.observe(viewLifecycleOwner, Observer {
            manageCardsAdapter?.manageCardDataList = it
            if (it.customerPaymentOptions?.isEmpty() == true) {
                binding.recyclerViewCard.visibility = View.GONE
                binding.mManagedCardEmptyTextView.visibility = View.VISIBLE
                binding.appManagedCardImageView.visibility = View.VISIBLE
            } else {
                binding.recyclerViewCard.visibility = View.VISIBLE
                binding.mManagedCardEmptyTextView.visibility = View.GONE
                binding.appManagedCardImageView.visibility = View.GONE
            }
        })
    }
    private fun observeProgressBar() {
        manageCardsViewModel.progressBarVisibility.observe(this, Observer {
            if (it == View.VISIBLE) {
                binding.progressBar.visibility = it
                binding.progressBar.setFreezAndVisiable(requireActivity())
            } else {
                binding.progressBar.visibility = it
                binding.progressBar.hideFreezAndVisiableProgress(requireActivity())
            }
        })
    }

    private fun observeerrorResponse() {
        manageCardsViewModel?.errorResponseObserve.observe(viewLifecycleOwner, Observer {
        binding.mManagedCardEmptyTextView.text = it
        })
    }
}