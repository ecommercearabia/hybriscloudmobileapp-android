package com.erabia.foodcrowd.android.common.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.databinding.RowCountryCodeBinding

class CountryCodeAdapter(
    var countryList: List<Country>,
    var parentViewModel: AddressViewModel
) :
    RecyclerView.Adapter<CountryCodeAdapter.MyViewHolder>(), Filterable {


    lateinit var binding: RowCountryCodeBinding
    var countryFilterList: ArrayList<Country>
    var selectedItem = 0

    init {
        countryFilterList = java.util.ArrayList(countryList)
        parentViewModel.countryCode = countryList?.get(0)
    }

    override fun getItemCount(): Int {
        return countryFilterList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.view.country = countryFilterList.get(position)
//        holder.view.imageViewCountryFlag.setImageResource(
//            World.getFlagOf(
//                countryFilterList.get(
//                    position
//                ).isocode
//            )
//        )
        holder.view.clCounrty.setOnClickListener(View.OnClickListener {
            selectedItem = position
            parentViewModel.countryCode = countryFilterList.get(position)
            notifyDataSetChanged()
        })
        if (selectedItem == position) {
            holder.view.clCounrty.setBackgroundColor(holder.itemView.context.resources.getColor(R.color.light_gray))

        } else {
            holder.view.clCounrty.setBackgroundColor(0)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_country_code,
            parent,
            false
        )
        return MyViewHolder(binding)
    }


    class MyViewHolder(var view: RowCountryCodeBinding) : RecyclerView.ViewHolder(view.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    countryFilterList = java.util.ArrayList(countryList)
                } else {
                    val resultList = ArrayList<Country>()
                    for (row in countryList) {
                        if ((row.name?: "").contains(charSearch, true)) {
                            resultList.add(row)
                        }
                    }
                    countryFilterList = resultList

                }
                val filterResults = FilterResults()
                filterResults.values = countryFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                countryFilterList = results?.values as ArrayList<Country>
                // countryFilterList = Arrays.asList(results?.values) as ArrayList<Country>
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return countryList.get(position).isocode.hashCode()?.toLong()
    }


}