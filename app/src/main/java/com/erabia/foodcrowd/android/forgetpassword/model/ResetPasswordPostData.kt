package com.erabia.foodcrowd.android.forgetpassword.model


import com.google.gson.annotations.SerializedName

data class ResetPasswordPostData(
    @SerializedName("newPassword")
    var newPassword: String? = "",
    @SerializedName("token")
    var token: String? = ""
)