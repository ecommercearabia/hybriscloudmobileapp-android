package com.erabia.foodcrowd.android.checkout.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.adapter.ItemsAdapter
import com.erabia.foodcrowd.android.checkout.adapter.ItemsConfirmationAdapter
import com.erabia.foodcrowd.android.checkout.model.PlaceOrderResponse
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.main.MainActivity
import kotlinx.android.synthetic.main.fragment_all_order_checkout.view.*


class AllOrderCheckoutFragment : Fragment() {
    private var itemsAdapter: ItemsAdapter? = null
    var cart: Cart? = null
    var orderData: PlaceOrderResponse? = null
    lateinit var callback: OnBackPressedCallback
    var mFromCheckOut: Boolean? = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        callback =
//            object : OnBackPressedCallback(true /* enabled by default */) {
//                override fun handleOnBackPressed() {
//
//                    setFragmentResult(
//                        "fromSeeMore",
//                        bundleOf(
//                            "from" to true
//                        )
//                    )
//                    view?.post {
//                        findNavController().navigateUp()
//                    }
//                }
//            }
//        requireActivity().onBackPressedDispatcher.addCallback(requireActivity(), callback)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_all_order_checkout, container, false)

        cart = arguments?.let { AllOrderCheckoutFragmentArgs.fromBundle(it).cart }
        orderData = arguments?.let { AllOrderCheckoutFragmentArgs.fromBundle(it).orderData }


//        (activity as MainActivity).toolbar.setNavigationOnClickListener {
//                setFragmentResult(
//                    "fromSeeMore",
//                    bundleOf(
//                        "from" to true
//                    )
//                )
//            view?.post {
//                findNavController().navigateUp()
//            }
//
//        }
        if (cart != null) {
            itemsAdapter = ItemsAdapter(cart?.entries?.size ?: 0)

            view.recycler_view_items.adapter = itemsAdapter
            view.recycler_view_items.isNestedScrollingEnabled = false
            view.recycler_view_items.setHasFixedSize(false)

            itemsAdapter?.entryList = cart?.entries ?: listOf()

        } else {

            val itemsConfirmationAdapter = ItemsConfirmationAdapter(orderData?.entries?.size ?: 0)
            view.recycler_view_items.adapter = itemsConfirmationAdapter
            view.recycler_view_items.isNestedScrollingEnabled = false
            view.recycler_view_items.setHasFixedSize(false)
            itemsConfirmationAdapter.entryList = orderData?.entries
        }

        return view
    }

//
//    override fun onDestroy() {
//        super.onDestroy()
//        callback.isEnabled = false
//        callback.remove()
//    }


//    override fun onStop() {
//        super.onStop()
//        setFragmentResult(
//            "fromSeeMore",
//            bundleOf(
//                "from" to true
//            )
//        )
//    }


}