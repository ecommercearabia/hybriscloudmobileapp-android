package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




data class CurrentQuery (
    @SerializedName("query")
    val query: Query? = null,
    @SerializedName("url")
    val url: String? = null
)