package com.erabia.foodcrowd.android.checkout.model


import android.os.Parcelable
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.model.AppliedValue
import com.erabia.foodcrowd.android.common.model.Currency
import com.erabia.foodcrowd.android.common.model.Price
import com.erabia.foodcrowd.android.common.model.SubTotalBeforeSavingPrice
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlaceOrderResponse(
//    @SerializedName("appliedOrderPromotions")
//    var appliedOrderPromotions: List<Any?>? = listOf(),
//    @SerializedName("appliedProductPromotions")
//    var appliedProductPromotions: List<Any?>? = listOf(),
    @SerializedName("appliedVouchers")
    var appliedVouchers: List<AppliedVoucher?>? = listOf(),
    @SerializedName("calculated")
    var calculated: Boolean? = false,
    @SerializedName("code")
    var code: String? = "",
    @SerializedName("created")
    var created: String? = "",
    @SerializedName("deliveryAddress")
    var deliveryAddress: DeliveryAddress? = DeliveryAddress(),
    @SerializedName("deliveryCost")
    var deliveryCost: DeliveryCost? = DeliveryCost(),
    @SerializedName("deliveryItemsQuantity")
    var deliveryItemsQuantity: Int? = 0,
    @SerializedName("deliveryMode")
    var deliveryMode: DeliveryMode? = DeliveryMode(),
    @SerializedName("deliveryOrderGroups")
    var deliveryOrderGroups: List<DeliveryOrderGroup?>? = listOf(),
    @SerializedName("entries")
    var entries: List<Entry?>? = listOf(),
    @SerializedName("guestCustomer")
    var guestCustomer: Boolean? = false,
    @SerializedName("guid")
    var guid: String? = "",
    @SerializedName("net")
    var net: Boolean? = false,
    @SerializedName("orderDiscounts")
    var orderDiscounts: OrderDiscounts? = OrderDiscounts(),
    @SerializedName("paymentMode")
    var paymentMode: PaymentMode? = PaymentMode(),
    @SerializedName("pickupItemsQuantity")
    var pickupItemsQuantity: Int? = 0,
//    @SerializedName("pickupOrderGroups")
//    var pickupOrderGroups: List<Any?>? = listOf(),
    @SerializedName("productDiscounts")
    var productDiscounts: ProductDiscounts? = ProductDiscounts(),
    @SerializedName("site")
    var site: String? = "",
    @SerializedName("statusDisplay")
    var statusDisplay: String? = "",
    @SerializedName("store")
    var store: String? = "",
    @SerializedName("storeCreditAmount")
    var storeCreditAmount: StoreCreditAmount? = StoreCreditAmount(),
    @SerializedName("storeCreditAmountSelected")
    var storeCreditAmountSelected: Double? = 0.0,
    @SerializedName("subTotal")
    var subTotal: SubTotal? = SubTotal(),
    @SerializedName("subTotalBeforeSavingPrice")
    val subTotalBeforeSavingPrice: SubTotalBeforeSavingPrice = SubTotalBeforeSavingPrice(),
    @SerializedName("timeSlotInfoData")
    var timeSlotInfoData: TimeSlotInfoData? = TimeSlotInfoData(),
    @SerializedName("totalDiscounts")
    var totalDiscounts: TotalDiscounts? = TotalDiscounts(),
    @SerializedName("totalItems")
    var totalItems: Int? = 0,
    @SerializedName("totalPrice")
    var totalPrice: TotalPrice? = TotalPrice(),
    @SerializedName("totalPriceWithTax")
    var totalPriceWithTax: TotalPriceWithTax? = TotalPriceWithTax(),
    @SerializedName("totalTax")
    var totalTax: TotalTax? = TotalTax(),
    @SerializedName("type")
    var type: String? = "",
    @SerializedName("unconsignedEntries")
    var unconsignedEntries: List<UnconsignedEntry?>? = listOf(),
    @SerializedName("user")
    var user: User? = User()
):Parcelable {

    @Parcelize
    data class DeliveryAddress(
        @SerializedName("addressName")
        var addressName: String? = "",
        @SerializedName("country")
        var country: Country? = Country(),
        @SerializedName("defaultAddress")
        var defaultAddress: Boolean? = false,
        @SerializedName("firstName")
        var firstName: String? = "",
        @SerializedName("formattedAddress")
        var formattedAddress: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("lastName")
        var lastName: String? = "",
        @SerializedName("line1")
        var line1: String? = "",
        @SerializedName("line2")
        var line2: String? = "",
        @SerializedName("mobileCountry")
        var mobileCountry: MobileCountry? = MobileCountry(),
        @SerializedName("mobileNumber")
        var mobileNumber: String? = "",
        @SerializedName("nearestLandmark")
        var nearestLandmark: String? = "",
        @SerializedName("shippingAddress")
        var shippingAddress: Boolean? = false,
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("titleCode")
        var titleCode: String? = "",
        @SerializedName("town")
        var town: String? = "",
        @SerializedName("visibleInAddressBook")
        var visibleInAddressBook: Boolean? = false
    ) :Parcelable {

        @Parcelize
        data class Country(
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ):Parcelable

        @Parcelize
        data class MobileCountry(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ):Parcelable
    }

    @Parcelize
    data class DeliveryCost(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ):Parcelable

    @Parcelize
    data class DeliveryMode(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("deliveryCost")
        var deliveryCost: DeliveryCost? = DeliveryCost(),
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) :Parcelable{

        @Parcelize
        data class DeliveryCost(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ):Parcelable
    }

    @Parcelize
    data class DeliveryOrderGroup(
        @SerializedName("entries")
        var entries: List<Entry?>? = listOf(),
        @SerializedName("totalPriceWithTax")
        var totalPriceWithTax: TotalPriceWithTax? = TotalPriceWithTax()
    ) :Parcelable {
        @Parcelize
        data class Entry(
            @SerializedName("basePrice")
            var basePrice: BasePrice? = BasePrice(),
//            @SerializedName("configurationInfos")
//            var configurationInfos: List<Any?>? = listOf(),
            @SerializedName("entryNumber")
            var entryNumber: Int? = 0,
            @SerializedName("product")
            var product: Product? = Product(),
            @SerializedName("quantity")
            var quantity: Int? = 0,
            @SerializedName("totalPrice")
            var totalPrice: TotalPrice? = TotalPrice(),
            @SerializedName("updateable")
            var updateable: Boolean? = false
        ):Parcelable {
            @Parcelize
            data class BasePrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double? = 0.0
            ) :Parcelable

            @Parcelize
            data class Product(
                @SerializedName("availableForPickup")
                var availableForPickup: Boolean? = false,
                @SerializedName("nutritionFacts")
                var nutritionFacts: String? = "",
                @SerializedName("baseOptions")
                var baseOptions: List<BaseOption?>? = listOf(),
                @SerializedName("baseProduct")
                var baseProduct: String? = "",
                @SerializedName("categories")
                var categories: List<Category?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("configurable")
                var configurable: Boolean? = false,
                @SerializedName("countryOfOrigin")
                var countryOfOrigin: String? = "",
                @SerializedName("countryOfOriginIsocode")
                var countryOfOriginIsocode: String? = "",
                @SerializedName("images")
                var images: List<Image?>? = listOf(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("purchasable")
                var purchasable: Boolean? = false,
                @SerializedName("stock")
                var stock: Stock? = Stock(),
                @SerializedName("unitOfMeasure")
                var unitOfMeasure: String? = "",
                @SerializedName("url")
                var url: String? = ""
            ):Parcelable {
                @Parcelize
                data class BaseOption(
                    @SerializedName("selected")
                    var selected: Selected? = Selected(),
                    @SerializedName("variantType")
                    var variantType: String? = ""
                ):Parcelable {
                    @Parcelize
                    data class Selected(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ):Parcelable {

                        @Parcelize
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double? = 0.0
                        ):Parcelable

                        @Parcelize
                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Int? = 0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        ):Parcelable

                        @Parcelize
                        data class VariantOptionQualifier(
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ):Parcelable
                    }
                }

                @Parcelize
                data class Category(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("image")
                    var image: Image? = Image(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                ) :Parcelable{

                    @Parcelize
                    data class Image(
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    ):Parcelable
                }

                @Parcelize
                data class Image(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                ):Parcelable

                @Parcelize
                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Int? = 0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                ):Parcelable
            }

            @Parcelize
            data class TotalPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double? = 0.0
            ):Parcelable
        }

        @Parcelize
        data class TotalPriceWithTax(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ):Parcelable
    }
    @Parcelize
    data class AppliedVoucher(
        @SerializedName("code")
        val code: String = "",
        @SerializedName("description")
        val description: String = "",
        @SerializedName("freeShipping")
        val freeShipping: Boolean = false,
        @SerializedName("name")
        val name: String = "",
        @SerializedName("value")
        val value: Double = 0.0,
        @SerializedName("valueFormatted")
        val valueFormatted: String = "",
        @SerializedName("valueString")
        val valueString: String = "",
        @SerializedName("voucherCode")
        val voucherCode: String = ""
    ): Parcelable
    @Parcelize
    data class Entry(
        @SerializedName("basePrice")
        var basePrice: BasePrice? = BasePrice(),
//        @SerializedName("configurationInfos")
//        var configurationInfos: List<Any?>? = listOf(),
        @SerializedName("entryNumber")
        var entryNumber: Int? = 0,
        @SerializedName("product")
        var product: Product? = Product(),
        @SerializedName("quantity")
        var quantity: Int? = 0,
        @SerializedName("totalPrice")
        var totalPrice: TotalPrice? = TotalPrice(),
        @SerializedName("updateable")
        var updateable: Boolean? = false
    ) : Parcelable {
        @Parcelize
        data class BasePrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ) :  Parcelable

        @Parcelize
        data class Product(
            @SerializedName("availableForPickup")
            var availableForPickup: Boolean? = false,
            @SerializedName("nutritionFacts")
            var nutritionFacts: String? = "",

            @SerializedName("baseOptions")
            var baseOptions: List<BaseOption?>? = listOf(),
            @SerializedName("baseProduct")
            var baseProduct: String? = "",
            @SerializedName("categories")
            var categories: List<Category?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("configurable")
            var configurable: Boolean? = false,
            @SerializedName("countryOfOrigin")
            var countryOfOrigin: String? = "",
            @SerializedName("countryOfOriginIsocode")
            var countryOfOriginIsocode: String? = "",
            @SerializedName("images")
            var images: List<Image?>? = listOf(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("purchasable")
            var purchasable: Boolean? = false,
            @SerializedName("stock")
            var stock: Stock? = Stock(),
            @SerializedName("unitOfMeasure")
            var unitOfMeasure: String? = "",
            @SerializedName("url")
            var url: String? = ""
        ) :  Parcelable {
            @Parcelize
            data class BaseOption(
                @SerializedName("selected")
                var selected: Selected? = Selected(),
                @SerializedName("variantType")
                var variantType: String? = ""
            ) :  Parcelable {
                @Parcelize
                data class Selected(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) :  Parcelable{
                    @Parcelize
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double? = 0.0
                    ) : Parcelable

                    @Parcelize
                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Int? = 0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )  : Parcelable
                    @Parcelize
                    data class VariantOptionQualifier(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) :  Parcelable
                }
            }
            @Parcelize
            data class Category(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("image")
                var image: Image? = Image(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("url")
                var url: String? = ""
            ) :  Parcelable {
                @Parcelize
                data class Image(
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                ) :  Parcelable
            }
            @Parcelize
            data class Image(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            ) :  Parcelable

            @Parcelize
            data class Stock(
                @SerializedName("stockLevel")
                var stockLevel: Int? = 0,
                @SerializedName("stockLevelStatus")
                var stockLevelStatus: String? = ""
            ) :  Parcelable
        }

        @Parcelize
        data class TotalPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ) :  Parcelable
    }

    @Parcelize
    data class OrderDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class PaymentMode(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) :Parcelable

    @Parcelize
    data class ProductDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class StoreCreditAmount(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Int? = 0
    ) :Parcelable

    @Parcelize
    data class SubTotal(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class TimeSlotInfoData(
        @SerializedName("date")
        var date: String? = "",
        @SerializedName("day")
        var day: String? = "",
        @SerializedName("end")
        var end: String? = "",
        @SerializedName("periodCode")
        var periodCode: String? = "",
        @SerializedName("start")
        var start: String? = ""
    ) :Parcelable

    @Parcelize
    data class TotalDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class TotalPrice(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class TotalPriceWithTax(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) :Parcelable

    @Parcelize
    data class TotalTax(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double? = 0.0
    ) : Parcelable

    @Parcelize
    data class UnconsignedEntry(
        @SerializedName("basePrice")
        var basePrice: BasePrice? = BasePrice(),
//        @SerializedName("configurationInfos")
//        var configurationInfos: List<Any?>? = listOf(),
        @SerializedName("entryNumber")
        var entryNumber: Int? = 0,
        @SerializedName("product")
        var product: Product? = Product(),
        @SerializedName("quantity")
        var quantity: Int? = 0,
        @SerializedName("totalPrice")
        var totalPrice: TotalPrice? = TotalPrice(),
        @SerializedName("updateable")
        var updateable: Boolean? = false
    ) :Parcelable {

        @Parcelize
        data class BasePrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ) :Parcelable

        @Parcelize
        data class Product(
            @SerializedName("availableForPickup")
            var availableForPickup: Boolean? = false,
            @SerializedName("nutritionFacts")
            var nutritionFacts: String? = "",

            @SerializedName("baseOptions")
            var baseOptions: List<BaseOption?>? = listOf(),
            @SerializedName("baseProduct")
            var baseProduct: String? = "",
            @SerializedName("categories")
            var categories: List<Category?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("configurable")
            var configurable: Boolean? = false,
            @SerializedName("countryOfOrigin")
            var countryOfOrigin: String? = "",
            @SerializedName("countryOfOriginIsocode")
            var countryOfOriginIsocode: String? = "",
            @SerializedName("images")
            var images: List<Image?>? = listOf(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("purchasable")
            var purchasable: Boolean? = false,
            @SerializedName("stock")
            var stock: Stock? = Stock(),
            @SerializedName("unitOfMeasure")
            var unitOfMeasure: String? = "",
            @SerializedName("url")
            var url: String? = ""
        ) : Parcelable{

            @Parcelize
            data class BaseOption(
                @SerializedName("selected")
                var selected: Selected? = Selected(),
                @SerializedName("variantType")
                var variantType: String? = ""
            ) : Parcelable {

                @Parcelize
                data class Selected(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) : Parcelable {
                    @Parcelize
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double? = 0.0
                    ) : Parcelable

                    @Parcelize
                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Int? = 0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    ) : Parcelable

                    @Parcelize
                    data class VariantOptionQualifier(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) : Parcelable
                }
            }

            @Parcelize
            data class Category(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("image")
                var image: Image? = Image(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("url")
                var url: String? = ""
            ) : Parcelable {
                @Parcelize
                data class Image(
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                ) :Parcelable
            }

            @Parcelize
            data class Image(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            ) :Parcelable

            @Parcelize
            data class Stock(
                @SerializedName("stockLevel")
                var stockLevel: Int? = 0,
                @SerializedName("stockLevelStatus")
                var stockLevelStatus: String? = ""
            ) : Parcelable
        }
        @Parcelize
        data class TotalPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        ) : Parcelable
    }

    @Parcelize
    data class User(
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("uid")
        var uid: String? = ""
    ) : Parcelable

}
