package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ConfigurationInfo(
    @SerializedName("configurationLabel")
    val configurationLabel: String = "",
    @SerializedName("configurationValue")
    val configurationValue: String = "",
    @SerializedName("configuratorType")
    val configuratorType: String = "",
    @SerializedName("status")
    val status: String = ""
)