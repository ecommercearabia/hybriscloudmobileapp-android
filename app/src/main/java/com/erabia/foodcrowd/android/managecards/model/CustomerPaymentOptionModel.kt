package com.erabia.foodcrowd.android.managecards.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class CustomerPaymentOption(
    @SerializedName("customerCardId")
    @Expose
    var customerCardId: String? = "",
    @SerializedName("customerCardLabel")
    @Expose
    var customerCardLabel: String? = "",
    @SerializedName("customerCardName")
    @Expose
    var customerCardName: String? = "",
    @SerializedName("customerCardNo")
    @Expose
    var customerCardNo: String? = "",
    @SerializedName("customerCardType")
    @Expose
    var customerCardType: String? = "",
    @SerializedName("customerEmail")
    @Expose
    var customerEmail: String? = "",
    @SerializedName("customerPayoptType")
    @Expose
    var customerPayoptType: String? = ""
)