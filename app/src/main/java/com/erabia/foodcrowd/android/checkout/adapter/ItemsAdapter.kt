package com.erabia.foodcrowd.android.checkout.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.model.PlaceOrderResponse
import com.erabia.foodcrowd.android.common.model.Entry
import com.erabia.foodcrowd.android.databinding.RowItemFinishBinding

/**
 * @param numOfItems is number of items will show in recycler view,
 * if this param is null recyclerView will show all items
 */
class ItemsAdapter(var numOfItems: Int?) :
    RecyclerView.Adapter<ItemsAdapter.MyViewHolder>() {

    private var context: Context? = null
    lateinit var binding: RowItemFinishBinding
    var entryList: List<Entry>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_item_finish,
            parent,
            false
        )

        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return when {
            numOfItems == null -> entryList?.size ?: 0
            numOfItems ?: 0 > entryList?.size ?: 0 -> entryList?.size ?: 0
            else -> numOfItems ?: 0
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (numOfItems == null || numOfItems ?: 0 > entryList?.size ?: 0) {
            if (position == (entryList?.size ?: 0) - 1)
                holder.binding.separatorLine.visibility = View.GONE
        } else {
            if (position == (numOfItems ?: 0) - 1)
                holder.binding.separatorLine.visibility = View.GONE
        }
        holder.binding.entry = entryList?.get(position)
        holder.binding.product = entryList?.get(position)?.product
    }

    override fun getItemId(position: Int): Long {
        return entryList?.get(position)?.product?.code.hashCode().toLong()
    }
    class MyViewHolder(var binding: RowItemFinishBinding) : RecyclerView.ViewHolder(binding.root)

}