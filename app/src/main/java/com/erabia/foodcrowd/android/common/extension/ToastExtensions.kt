package com.erabia.foodcrowd.android.common.extension

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.erabia.foodcrowd.android.R
import es.dmoral.toasty.Toasty

fun Toast.showToastySuccess(toast: Toast, ctx: Context?): Toast {
    toast.view?.background =
        ctx?.let { it1 -> ContextCompat.getDrawable(it1, R.drawable.success_toast) }
    val v = toast.view?.findViewById<View>(android.R.id.message) as TextView
    v.setTextColor(Color.WHITE)
    toast.view
    return toast
}

fun Toast.showToastyError(toast: Toast, ctx: Context?): Toast {
    toast.view?.background =
        ctx?.let { it1 -> ContextCompat.getDrawable(it1, R.drawable.error_toast) }
    val v = toast.view?.findViewById<View>(android.R.id.message) as TextView
    v.setTextColor(Color.WHITE)
    toast.view
    return toast
}


fun Toast.showToastyWarning(toast: Toast, ctx: Context?): Toast {
    toast.view?.background =
        ctx?.let { it1 -> ContextCompat.getDrawable(it1, R.drawable.warnang_toast) }
    val v = toast.view?.findViewById<View>(android.R.id.message) as TextView
    v.setTextColor(Color.WHITE)
    toast.view
    return toast
}

