package com.erabia.foodcrowd.android.signup.model


import com.google.gson.annotations.SerializedName

data class CountryResponse(
    @SerializedName("countries")
    var countries: ArrayList<Country> = arrayListOf()
)
{

    data class Country(
        @SerializedName("isdcode")
        var isdcode: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ){
        override fun toString(): String {
            return name ?: ""
        }

    }




}