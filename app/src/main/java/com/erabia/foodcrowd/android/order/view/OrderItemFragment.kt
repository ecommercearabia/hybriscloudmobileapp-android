package com.erabia.foodcrowd.android.order.view

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.ActivityOrderBinding
import com.erabia.foodcrowd.android.databinding.FragmentOrderItemBinding
import com.erabia.foodcrowd.android.order.adapter.OrderItemAdapter
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.disposables.CompositeDisposable

class OrderItemFragment(mOrderDetailsResponse:OrderDetailsResponse) : Fragment() {
    var mDetailsResponse=mOrderDetailsResponse
    var myOrderItemList: List<String> = listOf("","","","","","","","")
    private var mOrderItemAdapter: OrderItemAdapter? = null
    lateinit var mViewModel: MyOrderViewModel
    var compositeDisposable = CompositeDisposable()
    lateinit var binding: FragmentOrderItemBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = MyOrderViewModel.Factory(
            MyOrderRepository(
                RequestManager.getClient().create(MyOrderService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(MyOrderViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        // val view= inflater.inflate(R.layout.fragment_order_item, container, false)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_item, container, false)
        binding.lifecycleOwner = this
        binding.mMyOrderViewModel = mViewModel
        binding.orderDetails=mDetailsResponse

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        mViewModel.getItemDetails(mCode)


//        mViewModel.mItemDetails.observe(viewLifecycleOwner, Observer {


            mOrderItemAdapter =
                OrderItemAdapter(
                    mDetailsResponse.entries,mViewModel
                )
            mOrderItemAdapter?.setHasStableIds(true)

            binding?.mOrderItemRecyclerView?.adapter = mOrderItemAdapter

            Log.e("mohamed","mohamed")
//        })
        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)

        })
    }

}