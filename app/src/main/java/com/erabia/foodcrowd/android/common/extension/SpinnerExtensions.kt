package com.erabia.foodcrowd.android.common.extension

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.databinding.InverseBindingListener
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.R


/**
 * set spinner entries
 */
fun Spinner.setSpinnerEntries(entries: List<Any>?) {
    if (entries != null) {
        val arrayAdapter =
            ArrayAdapter(context, R.layout.item_spinner, R.id.mSpinnerTextView, entries)
        adapter = arrayAdapter
        tag = -1
    }
}

/**
 * set spinner onItemSelectedListener listener
 */
fun Spinner.setSpinnerItemSelectedListener(listener: ItemSelectedListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (tag != position) {
                    listener.onItemSelected(parent.getItemAtPosition(position))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}

/**
 * set spinner onItemSelectedListener listener
 */
fun Spinner.setSpinnerInverseBindingListener(listener: InverseBindingListener?) {
    if (listener == null) {
        onItemSelectedListener = null
    } else {
        onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (tag != position) {
                    listener.onChange()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }
}


/**
 * set spinner value
 */
fun Spinner.setSpinnerValue(value: Any?) {
    if (adapter != null && value != null) {
        val position = (adapter as ArrayAdapter<Any?>).getPosition(value)
        setSelection(position, false)
        tag = position
    }
}


/**
 * set spinner value
 */
fun Spinner.restValue() {
    if (adapter != null) {
        setSelection(0, false)
    }
}

/**
 * get spinner value
 */
fun Spinner.getSpinnerValue(): Any? {
    return selectedItem
}


interface ItemSelectedListener {
    fun onItemSelected(item: Any?)
}


