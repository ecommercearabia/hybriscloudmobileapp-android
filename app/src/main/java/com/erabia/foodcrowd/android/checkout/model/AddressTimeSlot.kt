package com.erabia.foodcrowd.android.checkout.model

import com.erabia.foodcrowd.android.common.model.AddressSlot
import com.erabia.foodcrowd.android.common.model.AddressSlot.Companion.dummyAddressSlot
import java.util.*

class AddressTimeSlot(
    var date: String,
    val addressSlotList: List<AddressSlot>
) {

    companion object {
        val dummyAddressTimeSlot: List<AddressTimeSlot>
            get() {
                val addressSlotList: MutableList<AddressTimeSlot> =
                    ArrayList()
                val addressSlot1 =
                    AddressTimeSlot("data", dummyAddressSlot)
                val addressSlot2 =
                    AddressTimeSlot("data1", dummyAddressSlot)
                val addressSlot3 =
                    AddressTimeSlot("data1", dummyAddressSlot)
                addressSlotList.add(addressSlot1)
                addressSlotList.add(addressSlot2)
                addressSlotList.add(addressSlot3)
                return addressSlotList
            }
    }

}