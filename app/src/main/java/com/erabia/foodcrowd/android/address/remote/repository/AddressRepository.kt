package com.erabia.foodcrowd.android.address.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.model.AddressList
import com.erabia.foodcrowd.android.address.model.CityList
import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.address.remote.service.AddressService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import es.dmoral.toasty.Toasty
import io.reactivex.rxkotlin.subscribeBy

class AddressRepository(val addressService: AddressService) : Repository {
    companion object {
        const val TAG: String = "AddressRepository"
    }

    val mAddAdressLiveEvent: SingleLiveEvent<Address> by lazy { SingleLiveEvent<Address>() }
    val billingAddressLiveEvent: SingleLiveEvent<Address> by lazy { SingleLiveEvent<Address>() }
    val mTitleLiveEvent: SingleLiveEvent<TitleResponse> by lazy { SingleLiveEvent<TitleResponse>() }
    val navigateFromAddressToCheckout: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val emptyAddressVisibility: SingleLiveEvent<Int> = SingleLiveEvent<Int>()
    val addAddressButtonVisibility: SingleLiveEvent<Int> = SingleLiveEvent()
    val navigateFromCartToAddress: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val updateAddressEvent: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val addressList = SingleLiveEvent<AddressList>()

    @SuppressLint("CheckResult")
    fun getTitle():LiveData<TitleResponse>{
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = SingleLiveEvent<TitleResponse>()

        addressService.getLocalizedTitle(
            "FULL"
        ).get()
            .subscribeBy(
                onNext =
                {
                    progressBarVisibility().value = (View.GONE)
                    when (it.code()) {
                        200 -> {
                            liveData.postValue(it.body())
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )

        return liveData
    }


    @SuppressLint("CheckResult")
    fun createAddress(address: Address, comeFrom: String) {
        progressBarVisibility().value = (View.VISIBLE)
        addressService.createAddress("current", address)
            .get()
            .subscribe({
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200, 201 -> {
                        if (comeFrom.equals(Constants.INTENT_KEY.CART) || comeFrom.equals(
                                Constants.INTENT_KEY.CHECKOUT
                            )
                        ) {
                            it.body()?.let { it1 -> setCartDeliveryAddress(it1) }
                        }
                        else if (comeFrom.equals(Constants.INTENT_KEY.MENU)) {
                            getAddressList(comeFrom)
                            mAddAdressLiveEvent.postValue(it.body())
                        } else {
                            billingAddressLiveEvent.postValue(it.body())
                        }

                    }

                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }


            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

    }

    @SuppressLint("CheckResult")
    fun getShippingCountryList(): LiveData<CountryList> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = SingleLiveEvent<CountryList>()
        addressService.getCountryList(Constants.FIELDS_FULL, "SHIPPING")
            .get()
            .subscribe({
                liveData.value = it.body()
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }




    @SuppressLint("CheckResult")
    fun getPersonalDetailsList(): LiveData<PersonalDetailsModel> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = SingleLiveEvent<PersonalDetailsModel>()
        addressService.getCustomerProfile("current", "FULL")
            .get()
            .subscribe({
                liveData.value = it.body()
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }




    @SuppressLint("CheckResult")
    fun getMobileCountryList(): LiveData<CountryList> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = SingleLiveEvent<CountryList>()
        addressService.getCountryList(Constants.FIELDS_FULL, "MOBILE")
            .get()
            .subscribe({
                liveData.value = it.body()
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }

    @SuppressLint("CheckResult")
    fun getAddressList(comeFrom: String): LiveData<AddressList> {
        progressBarVisibility().value = (View.VISIBLE)
        var liveData = MutableLiveData<AddressList>()
        addressService.getAddressList(
            "current", Constants.FIELDS_FULL
        )
            .get()
            .subscribe({
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200 -> {
                        if (it.body()?.addresses.isNullOrEmpty()) {
                            if (comeFrom.equals(Constants.INTENT_KEY.CART)) {
                                navigateFromCartToAddress.postValue(R.id.action_navigate_to_addAddress)
                            } else {
                                emptyAddressVisibility.value = View.VISIBLE
                                addAddressButtonVisibility.value = View.VISIBLE
                                addressList.value = it.body()
                            }
                        } else {
                            emptyAddressVisibility.value = View.GONE
                            addAddressButtonVisibility.value = View.VISIBLE
                            addressList.value = it.body()
                        }

                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }

    @SuppressLint("CheckResult")
    fun setCartDeliveryAddress(address: Address): LiveData<Int> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = MutableLiveData<Int>()
        addressService.setDeliveryAddress("current", "current", address.id)
            .get()
            .subscribe({
                when (it.code()) {
                    200, 201 -> {
//                        navigateFromAddressToCheckout.postValue(R.id.action_navigate_to_checkout)

                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }

    @SuppressLint("CheckResult")
    fun deleteAddress(addressId: String, comeFrom: String) {
        progressBarVisibility().value = (View.VISIBLE)
        var liveData = MutableLiveData<AddressList>()
        addressService.deleteAddress(
            "current", addressId
        )
            .get()
            .subscribe({
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200 -> {
                        AppUtil.showToastySuccess( MyApplication.getContext() ,R.string.delete_address)

                        getAddressList(comeFrom)
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })


    }

    @SuppressLint("CheckResult")
    fun getAddress(addressId: String): LiveData<Address> {
        progressBarVisibility().value = (View.VISIBLE)
        var liveData = MutableLiveData<Address>()
        addressService.getAddressDetails(
            "current", addressId, Constants.FIELDS_FULL
        )
            .get()
            .subscribe({
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200 -> {
                        liveData.value = it.body()
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })
        return liveData

    }

    @SuppressLint("CheckResult")
    fun updateAddress(addressId: String, address: Address) {
        progressBarVisibility().value = (View.VISIBLE)
        var liveData = MutableLiveData<Int>()
        addressService.updateAddress(
            "current", addressId, address
        )
            .get()
            .subscribe({
                progressBarVisibility().value = (View.GONE)
                when (it.code()) {
                    200 -> {
                        updateAddressEvent.postValue(R.string.update_address)
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })
    }


    @SuppressLint("CheckResult")
    fun getCityList(isoCode: String): LiveData<CityList> {
        progressBarVisibility().value = (View.VISIBLE)
        val liveData = SingleLiveEvent<CityList>()
        addressService.getCities(isoCode, Constants.FIELDS_FULL)
            .get()
            .subscribe({
                liveData.value = it.body()
                progressBarVisibility().value = (View.GONE)
            },
                {
                    Log.d(AddressRepository.TAG, it.toString())
                })

        return liveData
    }


}