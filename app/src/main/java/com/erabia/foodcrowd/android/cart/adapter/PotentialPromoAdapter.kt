package com.erabia.foodcrowd.android.cart.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView


import com.erabia.foodcrowd.android.R

import com.erabia.foodcrowd.android.common.model.PotentialOrderPromotion

import com.erabia.foodcrowd.android.databinding.RowPotentialPromtionBinding

class PotentialPromoAdapter :
    RecyclerView.Adapter<PotentialPromoAdapter.ViewHolder>() {
    private var context: Context? = null
    var mPotentialList: MutableList<PotentialOrderPromotion>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var itemBinding: RowPotentialPromtionBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        itemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.row_potential_promtion, parent, false)
        return ViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return mPotentialList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = mPotentialList?.get(position)
//        val product = entry?.product
        entry?.let { holder.bind(it) }

    }


    inner class ViewHolder(val itemBinding: RowPotentialPromtionBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(entry: PotentialOrderPromotion) {
            itemBinding.potentialPromotion = entry
//            itemBinding.textViewPotentialPromotionValue.text=entry.description
//            itemView.text_view_product_quantity.text = entry?.quantity.toString()
        }
    }


}