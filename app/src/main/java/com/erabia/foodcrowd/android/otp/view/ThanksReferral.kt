package com.erabia.foodcrowd.android.otp.view

import android.annotation.SuppressLint
import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityOtpBinding
import com.erabia.foodcrowd.android.databinding.ActivityThanksReferralBinding
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_thanks.*

class ThanksReferral : AppCompatActivity() {

        var compositeDisposable = CompositeDisposable()

       private lateinit var binding: ActivityThanksReferralBinding


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_thanks_referral)


            binding.mStartShoppingButton.clicks().filterRapidClicks().subscribe {
                setResult(Activity.RESULT_OK)
                finish()
            }.addTo(compositeDisposable)
            binding.mBackImageView.clicks().filterRapidClicks().subscribe {
                setResult(Activity.RESULT_OK)
                finish()
            }.addTo(compositeDisposable)

        binding.textView10.text = resources?.getString(R.string.remember_to_use_the) + " " + SharedPreference.getInstance().getReferralCode() + " "+ resources?.getString(R.string.added_in_your_store_credit)

        }



        override fun onDestroy() {
            super.onDestroy()
            compositeDisposable.dispose()
        }
    }


