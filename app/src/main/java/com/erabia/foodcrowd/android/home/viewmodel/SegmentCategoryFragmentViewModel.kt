package com.erabia.foodcrowd.android.home.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.home.adapter.HomeListingAdapter
import io.reactivex.disposables.Disposable

class SegmentCategoryFragmentViewModel(
    val homeViewModel: HomeViewModel,
    val navController: NavController?,
    val highLightedProductsViewModel: HighLightedProductsViewModel
) : ViewModel() {
    val highlightedProductAdapter = HomeListingAdapter(highLightedProductsViewModel, homeViewModel)

    //        ListingAdapter(
//            homeViewModel,
//            homeViewModel.addToCartUseCase,
//            homeViewModel.addToWishlistUseCase,
//            navController,
//            withPagination = false
//        )
    var disposable: Disposable? = null

    //    val category: MutableLiveData<List<Locale.Category>> by lazy { MutableLiveData<List<Locale.Category>>() }
    val component: MutableLiveData<List<Component>> by lazy { MutableLiveData<List<Component>>() }

//    var categoryProductLiveData: MutableLiveData<CategoryProduct>? = MutableLiveData()
//        get() {
//            return if (field == null) {
//                field = MutableLiveData()
//                field
//            } else
//                field
//        }

//    fun loadCategoryProduct(
//        categoryCode: String,
//        currentPage: String,
//        pageSize: String,
//        sort: String,
//        query: String
//    ) {
//        progressBar().postValue(View.VISIBLE)
//        var categoryProduct: CategoryProduct
//        val params: HashMap<String, String> = HashMap()
//        params[Constants.API_KEY.CATEGORY_CODE] = categoryCode
//        params[Constants.API_KEY.CURRENT_PAGE] = currentPage
//        params[Constants.API_KEY.PAGE_SIZE] = pageSize
//        params[Constants.API_KEY.SORT] = sort
//        params[Constants.API_KEY.QUERY] = query
//        params[Constants.API_KEY.FIELDS] = "FULL"
//        disposable = listingUseCase.setParams(params).get().subscribe({
//            progressBar().postValue(View.GONE)
//            when (it.code()) {
//                200 -> {
//                    if (it.body() != null) {
//                        categoryProduct = it.body()!!
//                        categoryProductLiveData?.postValue(categoryProduct.getData()!!)
//                    }
//                }
//                400, 401 -> {
//                    networkError().postValue("Some error happened")
//                }
//                else -> {
//                    networkError().postValue("Some error happened")
//
//                }
//            }
//        }, {
//            progressBar().postValue(View.GONE)
//            networkError().postValue("Some error happened")
//        })
//    }

    @SuppressLint("CheckResult")
//    fun loadCategory(categoryId: String) {
//        progressBar().postValue(View.VISIBLE)
//        val params: HashMap<String, String> = HashMap()
//        params[Constants.API_KEY.CATALOG_ID] = BuildConfig.CATALOG_ID_VALUE
//        params[Constants.API_KEY.CATEGORY_ID] = categoryId
//        params[Constants.API_KEY.CATEGORY_FIELDS] = Constants.FIELDS_FULL
//        CategoryUseCase().setParams(params).get().subscribe({
//            progressBar().postValue(View.GONE)
//            when (it.code()) {
//                200 -> {
//                    progressBar().postValue(View.GONE)
//                    val categoryData: List<SubCategoryOne>? =
//                        it.body()?.getData()?.subcategories?.filter { it.urlImage != null }
//                    category.postValue(categoryData)
//                }
//                201 -> {
//
//                }
//                401 -> {
//                    networkError().postValue("Response Error")
//                }
//
//                400 -> {
//                    networkError().postValue("Response Error")
//                }
//                else -> {
//                    networkError().postValue("Response Error")
//
//                }
//            }
//        }, {
//            networkError().postValue("Response Error")
//            Log.v("ApiError", it.message)
//        })
//    }

    override fun onCleared() {
        super.onCleared()
        if (disposable != null && !disposable!!.isDisposed)
            disposable!!.dispose()
    }
}