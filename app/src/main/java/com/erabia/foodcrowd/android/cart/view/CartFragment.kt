package com.erabia.foodcrowd.android.cart.view


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.adapter.BankOfferAdapter
import com.erabia.foodcrowd.android.cart.adapter.CartAdapter
import com.erabia.foodcrowd.android.cart.adapter.PotentialPromoAdapter
import com.erabia.foodcrowd.android.cart.adapter.PromoCodeAdapter
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.db.DataBaseManager
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.model.Entry
import com.erabia.foodcrowd.android.common.model.PotentialOrderPromotion
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentCartBinding
import com.erabia.foodcrowd.android.home.view.HomeFragment
import com.erabia.foodcrowd.android.login.view.LoginActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*
import com.erabia.foodcrowd.android.common.util.SharedPreference
import android.view.Gravity
import com.erabia.foodcrowd.android.common.MyApplication
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import androidx.navigation.NavController
import kotlinx.android.synthetic.main.cart_row_item.view.*


class CartFragment : Fragment() {

    init {
//        DaggerCartComponent.builder().build().inject(this)

    }

    private var promoCodeAdapter: PromoCodeAdapter? = null
    private var potentialPromotionAdapter: PotentialPromoAdapter? = null
    private var cartAdapter: CartAdapter? = null
    private var bankOfferAdapter: BankOfferAdapter? = null
    private var cart: Cart? = null
    private var navController: NavController? = null

    //    @Inject
    var viewModel: CartViewModel? = null

    var binding: FragmentCartBinding? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false)
        val dataBase = DataBaseManager.getInstance(requireContext())
        val cartService = RequestManager.getClient().create(CartService::class.java)
        val factory = CartViewModel.Factory(
            CartRepository(
                cartService
            )
        )
//        binding?.editTextPromoCode?.filters = arrayOf<InputFilter>(AllCaps())
//        SharedPreference.getInstance().setUser_TOKEN("ztXAofz1jIOx2Uuo3iooN_3w1Nw")
//        SharedPreference.getInstance().setRefreshToken()

        viewModel = ViewModelProvider(this, factory).get(CartViewModel::class.java)

//        val navHostFragment =
//            (context as AppCompatActivity).supportFragmentManager.findFragmentById(R.id.nav_host_container)

        binding?.cartViewModel = viewModel

//        promoCodeAdapter = viewModel?.let { PromoCodeAdapter(viewModel!!) }
        potentialPromotionAdapter = PotentialPromoAdapter()
//        binding?.recyclerViewPromoCodes?.adapter = promoCodeAdapter
        setHasOptionsMenu(true)
        initView()
        observeCart()
//        observeReceivedPromotionMessage()
        observePotentialPromotionMessage()
//        observeReceivedPromotionVisibility()
//        observePotentialPromotionVisibility()
        observeAppliedVoucher()
//        observeBankOfferSuccess()
//        observeBankOfferError()
        observeHideRefreshLoaderEvent()
        observeError()
//        observeDeleteEntrySuccess()
        observeValidationSuccess()
        observeLoadShipmentType()
        observeValidationError()
        observeNavigateTo()
        observeNavigateToDetails()
        observeCheckoutButtonVisibility()
        observeExpressDeliveryClickEvent()
        observeContinueShoppingClickEvent()
        observeShipmentTypesRadioGroupClickEvent()
        observeFooterVisibility()
//        observePromoCodeVisibility()
//        observeClearPromoCodeText()
        observeDeleteVoucherSuccess()
        observeApplyVoucherSuccess()
        observeEmptyCartVisibility()
        observeProgressBar()
        observeBadgeNumber()
        maps()
//        viewModel?.getCart("", "", "")
//        viewModel?.getBankOffer()
        handleShipmentTypes()
        getShipmentTypes()
        handleShipmentTypeUpdated()
//        observeShipmentTypesCONFIG()
        return binding?.root
    }

    override fun onResume() {
        super.onResume()
        viewModel?.getCart("", "", "")
    }

    private fun initView() {
        cartAdapter = CartAdapter(viewModel)
        cartAdapter?.setHasStableIds(true)
        binding?.recyclerViewCartItem?.adapter = cartAdapter

        bankOfferAdapter = BankOfferAdapter()
        bankOfferAdapter?.setHasStableIds(true)
//        binding?.recyclerViewBankOffer?.adapter = bankOfferAdapter


        potentialPromotionAdapter = PotentialPromoAdapter()
        potentialPromotionAdapter?.setHasStableIds(true)
//        binding?.recyclerViewPotentialPromotionValue?.adapter = potentialPromotionAdapter


    }

    fun maps() {
        viewModel?.openMapObserver?.observe(viewLifecycleOwner, Observer {
            val gmmIntentUri =
                Uri.parse("google.navigation:q=$it")
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(mapIntent)
        })
    }

    private fun observeCart() {
        viewModel?.cart?.observe(viewLifecycleOwner, Observer {
            binding?.cart = it
            cartAdapter?.entryList = it?.entries as? MutableList<Entry>?
            handleShipmentTypes()
            if (binding?.cart?.pickupOrderGroups?.size!! > 0) {
                binding?.pickupStore?.visibility = View.VISIBLE
                binding?.textStoreLocationLabel?.text = binding?.cart?.pickupOrderGroups?.get(0)?.deliveryPointOfService?.address?.formattedAddress
            } else {
                binding?.pickupStore?.visibility = View.GONE
            }

            val mFirebaseAnalytics =
                FirebaseAnalytics.getInstance(requireContext())

            var productsBundle = arrayOf<Bundle?>()

            for (i in 0 until (it?.entries?.size ?: 0)) {
                val product = it?.entries?.get(i)?.product

                val productBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product?.code ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product?.name ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                    it?.entries?.get(i)?.quantity?.let { it1 ->
                        putInt(
                            FirebaseAnalytics.Param.QUANTITY,
                            it1
                        )
                    }
                    product?.price?.value?.let { it1 ->
                        putDouble(
                            FirebaseAnalytics.Param.PRICE,
                            it1
                        )
                    }
                }
                productsBundle = append(productsBundle, productBundle)

            }
            val viewCartParams = Bundle()
            viewCartParams.putString(
                FirebaseAnalytics.Param.CURRENCY,
                it?.totalPriceWithTax?.currencyIso ?: ""
            )
            it?.totalPriceWithTax?.value.let { it1 ->
                viewCartParams.putDouble(
                    FirebaseAnalytics.Param.VALUE,
                    it1 ?: 0.0
                )
            }
            viewCartParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                productsBundle
            )

            mFirebaseAnalytics.logEvent(
                FirebaseAnalytics.Event.VIEW_CART,
                viewCartParams
            )
        })
    }

    // show shipment type
    // why!
    private fun handleShipmentTypes() {
        viewModel?.shipmentTypesList?.observe(viewLifecycleOwner, Observer {
            it.supportedShipmentTypes?.forEachIndexed { index, supportedShipmentType ->
                val radioButton = RadioButton(requireContext())

                var shipmentCode = SharedPreference.getInstance().getShipmentType() ?: ""
                Log.d("teem shipmentCode ", shipmentCode)
                if (shipmentCode == "1") {
                    if (supportedShipmentType.code == "PICKUP_IN_STORE") {
                        radioButton.isChecked = true
                        SharedPreference.getInstance().setShipmentType("1")
                        binding?.pickupStore?.visibility = View.VISIBLE
                        binding?.textViewDeliveryLabel?.visibility = View.GONE
                        binding?.textViewDelivery?.visibility = View.GONE
                    }
                } else {
                    if (supportedShipmentType.code == "DELIVERY") {
                        radioButton.isChecked = true
                        SharedPreference.getInstance().setShipmentType("0")
                        binding?.pickupStore?.visibility = View.GONE
                        binding?.textViewDeliveryLabel?.visibility = View.VISIBLE
                        binding?.textViewDelivery?.visibility = View.VISIBLE
                    }
                }
                radioButton.id = index
                radioButton.text = supportedShipmentType.name ?: ""
                binding?.mShipmentStatusRadioGroup?.addView(radioButton)
            }
        })
    }

//    private fun observeReceivedPromotionMessage() {
//                viewModel?.receivedPromotionMessage?.observe(viewLifecycleOwner, Observer {
//                    binding?.textViewReceivedPromotionValue?.text = it
//                })
//    }

    private fun observePotentialPromotionMessage() {
        viewModel?.potentialPromotionMessageList?.observe(viewLifecycleOwner, Observer {
            potentialPromotionAdapter?.mPotentialList = it as MutableList<PotentialOrderPromotion>?
        })
    }


//    private fun observeReceivedPromotionVisibility() {
//        viewModel?.receivedPromotionVisibility?.observe(viewLifecycleOwner, Observer {
//            binding?.textViewReceivedPromotionValue?.visibility = it
//            binding?.textViewReceivedPromotionLabel?.visibility = it
//        })
//    }

    private fun observeBadgeNumber() {
        viewModel?.badgeNumber?.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }

//    private fun observePotentialPromotionVisibility() {
//        viewModel?.potentialPromotionVisibility?.observe(viewLifecycleOwner, Observer {
//            binding?.recyclerViewPotentialPromotionValue?.visibility = it
//            binding?.textViewPotentialPromotionLabel?.visibility = it
//        })
//    }


    private fun observeAppliedVoucher() {
        viewModel?.appliedVoucher?.observe(viewLifecycleOwner, Observer {
            promoCodeAdapter?.promoCodeList = it
        })
    }


//    private fun observeShipmentTypesCONFIG() {
//        viewModel?.getShipmentTypesCONFIGSuccess?.observe(viewLifecycleOwner, Observer {
//            if (SharedPreference.getInstance().getPickupInStoreEnabled()) {
//                viewModel?.getShipmentType()
//            }
//        })
//    }

    private fun getShipmentTypes() {
        viewModel?.getConfig()
        if (SharedPreference.getInstance().getPickupInStoreEnabled()) {
            viewModel?.getShipmentType()
        }
    }

//    private fun observeBankOfferSuccess() {
//        viewModel?.getBankOfferSuccess?.observe(viewLifecycleOwner, Observer {
//            binding?.textViewBankOffer?.visibility = View.VISIBLE
//            binding?.recyclerViewBankOffer?.visibility = View.VISIBLE
//            bankOfferAdapter?.mBankOfferList =
//                it?.bankOffers as? MutableList<BankOfferResponse.BankOffer>?
//        })
//    }
//
//    private fun observeBankOfferError() {
//        viewModel?.getBankOfferError?.observe(viewLifecycleOwner, Observer {
//            binding?.textViewBankOffer?.visibility = View.GONE
//            binding?.recyclerViewBankOffer?.visibility = View.GONE
//            Toast.makeText(context, it, Toast.LENGTH_LONG)
//                .let { it.showToastyError(it, context).show() }
//        })
//    }


    private fun observeHideRefreshLoaderEvent() {
        viewModel?.hideRefreshLoaderEvent?.observe(viewLifecycleOwner, Observer {
            binding?.layoutPullToRefresh?.isRefreshing = false
        })
    }

    private fun observeProgressBar() {
        viewModel?.progressBarVisibility?.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })

    }

    private fun observeError() {
        viewModel?.error?.observe(viewLifecycleOwner, Observer {
            if (!it.contains("No cart created yet.") &&
                !it.contains("Cart not found.") && !it.contains("Cart is not anonymous")
            ) {
                AppUtil.showToastyError(context, it)
            }

            Log.d(HomeFragment.TAG, it)
        })
    }

//    private fun observeDeleteEntrySuccess() {
//        viewModel?.deleteEntrySuccess?.observe(viewLifecycleOwner, Observer {
//            viewModel?.loadCart(Constants.FIELDS_FULL, "", "")
//        })
//    }

    private fun observeValidationSuccess() {
        viewModel?.validationSuccess?.observe(viewLifecycleOwner, Observer {
            viewModel?.onCheckoutClick()
        })
    }

    private fun observeLoadShipmentType() {
        viewModel?.validationLoadShipmentType?.observe(viewLifecycleOwner, Observer {
            viewModel?.loadShipmnetSuccess()
        })
    }


    // missing title or nationality alert
    private fun observeValidationError() {
        viewModel?.mErrorValidationMessage?.observe(viewLifecycleOwner, Observer {

            if(it.contains("Please update your profile") || it.contains("ملف التعريف الخاص بك") ) {
                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.warning))
                    .setMessage(it)
                    .setPositiveButton(getString(R.string.update_my_Profile),
                        DialogInterface.OnClickListener { dialogInterface, i ->

                            findNavController().navigate(
                                R.id.action_cart_to_personalDetailsFragment2,
                                bundleOf("fromFragment" to "cart")
                            )

                            dialogInterface.dismiss()
                        })
                    .create()
                    .show()
            }

            else {
                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.warning))
                    .setMessage(it)
                    .setPositiveButton(getString(R.string.ok),
                        DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                        })
                    .create()
                    .show()

            }





        })
    }


    private fun observeNavigateTo() {
        viewModel?.navigateTo?.observe(viewLifecycleOwner, Observer {
            if (it == 100) {
                startActivity(Intent(context, LoginActivity::class.java))
            } else {
                findNavController().navigate(
                    it,
                    bundleOf(Constants.COME_FROM to Constants.INTENT_KEY.CART)
                )

            }
        })
    }

    private fun observeNavigateToDetails() {
        viewModel?.navigateToDetails?.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to it)
            )
        })
    }


    private fun observeEmptyCartVisibility() {
        viewModel?.emptyCartVisibility?.observe(viewLifecycleOwner, Observer {
            binding?.constraintLayoutEmptyCart?.visibility = it
        })
    }

    private fun observeCheckoutButtonVisibility() {
        viewModel?.checkoutButtonVisibility?.observe(viewLifecycleOwner, Observer {
            binding?.buttonCartCheckOut?.visibility = it
            binding?.buttonContinueShoppingWithItems?.visibility = it
        })
    }

    private fun observeExpressDeliveryClickEvent() {
        viewModel?.expressDeliveryLiveEvent?.observe(viewLifecycleOwner, Observer {

            AlertDialog.Builder(requireContext())
                .setMessage(getString(R.string.express_delivery_tooltip))
                .setPositiveButton(getString(R.string.learn_more),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        findNavController().navigate(
                            R.id.action_cart_to_whatIsExpressFragment
                        )
                        dialogInterface.dismiss()
                    })
                .setNegativeButton(getString(R.string.close),
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                .create()
                .show()

//            SimpleTooltip.Builder(context)
//                .anchorView(binding?.recyclerViewCartItem?.helpIconCart)
//                .text(getString(R.string.express_delivery_tooltip))
//                .textColor(R.color.deliveryDayPreviousBtnColor)
//                .gravity(Gravity.TOP)
//                .animated(false)
//                .transparentOverlay(false)
//                .ignoreOverlay(true)
//                .backgroundColor(resources.getColor(R.color.base_bk))
//                .arrowColor(resources.getColor(R.color.base_bk))
//                .onDismissListener { onExpressDeliveryClick() }
//                .dismissOnInsideTouch(true)
//                .build()
//                .show()
        })
    }

    @SuppressLint("ResourceType")
    private fun onExpressDeliveryClick() {
        findNavController().navigate(R.id.action_cart_to_whatIsExpressFragment)
    }

    private fun observeContinueShoppingClickEvent() {
        viewModel?.continueShoppingClickEvent?.observe(viewLifecycleOwner, Observer {
            (activity as MainActivity).bottomNavigationView.selectedItemId = R.id.home
        })
    }

    private fun handleShipmentTypeUpdated() {
        viewModel?.updateShipmentTypesList?.observe(viewLifecycleOwner, Observer {
            if (SharedPreference.getInstance().getShipmentType() == "1") {
                binding?.pickupStore?.visibility = View.VISIBLE
                binding?.textStoreLocationLabel?.text =
                    it?.cartModifications?.get(0)?.entry?.deliveryPointOfService?.address?.formattedAddress
            } else {
                binding?.pickupStore?.visibility = View.GONE
            }
        })
    }

    // change shipment type radio
    private fun observeShipmentTypesRadioGroupClickEvent() {
        viewModel?.shipmentTypesRadioGroup?.observe(viewLifecycleOwner, Observer {
            binding?.mShipmentStatusRadioGroup?.setOnCheckedChangeListener { group, checkedId ->
                val radioButton: View = group.findViewById(checkedId)
                val index: Int = group.indexOfChild(radioButton)

                val bundle = Bundle()
                bundle.putInt("shipmentIndex", index)
                val type = bundle!!.getInt("shipmentIndex")
                SharedPreference.getInstance().setShipmentType(type.toString())

                Log.d("teem type", type.toString())
                if (type == 1 && binding?.cart?.pickupOrderGroups?.size!! > 0) {
                    binding?.pickupStore?.visibility = View.VISIBLE
                    binding?.textStoreLocationLabel?.text =
                        binding?.cart?.pickupOrderGroups?.get(0)?.deliveryPointOfService?.address?.formattedAddress
                    binding?.textViewDeliveryLabel?.visibility = View.GONE
                    binding?.textViewDelivery?.visibility = View.GONE
                } else {
                    binding?.pickupStore?.visibility = View.GONE
                    binding?.textViewDeliveryLabel?.visibility = View.VISIBLE
                    binding?.textViewDelivery?.visibility = View.VISIBLE
                }
                viewModel?.onShipmentSelected()
            }
        })
    }

    private fun observeFooterVisibility() {
        viewModel?.footerVisibility?.observe(this, Observer {
            binding?.footerLayout?.visibility = it
        })
    }

//    private fun observePromoCodeVisibility() {
//        viewModel?.promoCodeVisibility?.observe(this, Observer {
//            binding?.constraintLayoutPromoCode?.visibility = it
//        })
//    }

//    private fun observeClearPromoCodeText() {
//        viewModel?.clearPromoCodeText?.observe(viewLifecycleOwner, Observer {
//            binding?.editTextPromoCode?.text?.clear()
//        })
//    }

    private fun observeApplyVoucherSuccess() {
        viewModel?.applyVoucherSuccess?.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.done)
        })
    }

    private fun observeDeleteVoucherSuccess() {
        viewModel?.deleteVoucherSuccess?.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(context, R.string.done)

        })
    }

    fun append(arr: Array<Bundle?>, element: Bundle): Array<Bundle?> {
        val array = arrayOfNulls<Bundle>(arr.size + 1)
        System.arraycopy(arr, 0, array, 0, arr.size)
        array[arr.size] = element
        return array
    }
}

