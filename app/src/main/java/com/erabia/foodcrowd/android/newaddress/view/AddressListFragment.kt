package com.erabia.foodcrowd.android.newaddress.view

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentAddressListBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.newaddress.remote.service.AddressService
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddressListViewModel
import io.reactivex.disposables.CompositeDisposable

class AddressListFragment : Fragment() {
    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: AddressListViewModel
    lateinit var binding: FragmentAddressListBinding
    var mComeFrom = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        mComeFrom = arguments?.getString(Constants.COME_FROM).toString()

        val factory = AddressListViewModel.Factory(
            AddressRepository(
                RequestManager.getClient().create(AddressService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(AddressListViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_address_list, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = mViewModel
        binding.fromCom = mComeFrom
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        mViewModel.getAddressList()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.swipeRefreshLayout.setOnRefreshListener { mViewModel.getAddressList() }
        observerAddressList()
        observerError()
        observerDeleteAddress()
        addAddressClickObserver()
        editAddressClickObserver()
        itemAddressClickObserver()
        loadingVisibilityObserver()

    }

    fun observerAddressList() {
        mViewModel.mAddressListLiveData.observe(viewLifecycleOwner, Observer {
            binding.swipeRefreshLayout.isRefreshing = false
            binding.addressList = it

            binding.isEmpty = it.addresses.isNullOrEmpty()
        })
    }

    fun addAddressClickObserver() {
        mViewModel.addAddressLiveData.observe(viewLifecycleOwner, Observer {
            var action =
                AddressListFragmentDirections.actionAddressListFragmentToMapAddressFragment(
                    mComeFrom,
                    "add"
                )
            findNavController().navigate(action)
        })
    }


    fun itemAddressClickObserver() {
        mViewModel.mNavigateFromAddressToCheckout.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(it)
        })
    }

    fun loadingVisibilityObserver() {
        mViewModel?.progressBarVisibility?.observe(viewLifecycleOwner, Observer {
//            binding?.progressBar?.visibility = it
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }

    fun editAddressClickObserver() {
        mViewModel.editAddressLiveData.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(
                R.id.action_addressListFragment_to_editAddressFragment,
                bundleOf("address" to it, "COME_FROM" to mComeFrom)
            )
        })
    }

    fun observerDeleteAddress() {
        mViewModel.mDeleteAddressLiveData.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context , R.string.address_deleted)
        })
    }

    fun observerError() {
        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            binding.swipeRefreshLayout.isRefreshing = false

            AppUtil.showToastyError(context , it)
        })
    }


}