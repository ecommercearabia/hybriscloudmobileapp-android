package com.erabia.foodcrowd.android.common.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.model.TimeSlotDay
import com.erabia.foodcrowd.android.databinding.DeliveryDayRowItemBinding

class DeliveryDayAdapter(val context: Context, val timeSlotList: List<TimeSlotDay>) :
    RecyclerView.Adapter<DeliveryDayAdapter.ViewHolder>() {
    private var selectedPosition = -1
        set(value) {
            field = value
            if (value != -1) {
                onDayClickListener?.onDayClickListener(selectedPosition)
            }
            notifyDataSetChanged()
        }
    var selectedDate: String? = null
        set(value) {
            field = value
            val selectedTimeSlot = timeSlotList.singleOrNull() {
                it.date == value
            }
            selectedPosition = timeSlotList.indexOf(selectedTimeSlot)
        }
    var onDayClickListener: OnDayClickListener? = null
    private val deliveryDayRowItemBinding: DeliveryDayRowItemBinding? = null

    interface OnDayClickListener {
        fun onDayClickListener(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding: DeliveryDayRowItemBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.delivery_day_row_item,
                parent,
                false
            )
        return ViewHolder(itemBinding)
    }

    override fun getItemCount(): Int {
        return timeSlotList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val timeSlot: TimeSlotDay = timeSlotList[position]
        val dayDate: String = timeSlot.date
//        val splitStr: Array<String?> = dayDate.split(" ").toTypedArray()
        var day = timeSlot.day
        var date = timeSlot.date

        holder.itemBinding.textViewDeliveryDate.text = date
        holder.itemBinding.cardViewDeliveryDay.background = context.resources.getDrawable(
            R.drawable.address_background,
            null
        )
        holder.itemBinding.textViewDeliveryDay.text = day
        holder.itemBinding.cardViewDeliveryDay.setOnClickListener {
            selectedPosition = position
        }
        if (selectedPosition == position) {
            holder.itemBinding.cardViewDeliveryDay.background = context.resources.getDrawable(
                R.drawable.address_background,
                null
            )
        } else {
            holder.itemBinding.cardViewDeliveryDay.background = context.resources.getDrawable(
                R.drawable.address_background_non_defualt,
                null
            )
            holder.itemBinding.textViewDeliveryDay.setTextColor(context.resources.getColor(R.color.blackText))
        }
    }

    fun setListener(onDayClickListener: OnDayClickListener?) {
        this.onDayClickListener = onDayClickListener
    }

    class ViewHolder(val itemBinding: DeliveryDayRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

}