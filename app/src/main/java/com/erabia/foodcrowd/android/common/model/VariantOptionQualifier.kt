package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class VariantOptionQualifier(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("qualifier")
    val qualifier: String = "",
    @SerializedName("value")
    val value: String = ""
)