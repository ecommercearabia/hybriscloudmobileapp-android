package com.erabia.foodcrowd.android.wishlistmenu.model
import com.erabia.foodcrowd.android.common.model.Product
import com.google.gson.annotations.SerializedName
data class GetWishListResponse(
    @SerializedName("description")
    var description: String? = "",
    @SerializedName("name")
    var name: String? = "",
    @SerializedName("pk")
    var pk: String? = "",
    @SerializedName("size")
    var size: Double? = 0.0,
    @SerializedName("wishlistEntries")
    var wishlistEntries: List<WishlistEntry?> = listOf()
) {
    data class WishlistEntry(
        @SerializedName("addedDate")
        var addedDate: String? = "",
        @SerializedName("comment")
        var comment: String? = "",
        @SerializedName("desired")
        var desired: Double? = 0.0,
        @SerializedName("product")
        var product: Product? = Product(),
        @SerializedName("received")
        var received: Double? = 0.0,
        @SerializedName("wishlistPK")
        var wishlistPK: String? = ""
    ) {
//        data class Product(
//            @SerializedName("availableForPickup")
//            var availableForPickup: Boolean? = false,
//            @SerializedName("averageRating")
//            var averageRating: Double? = 0.0,
//            @SerializedName("baseOptions")
//            var baseOptions: List<BaseOption?>? = listOf(),
//            @SerializedName("baseProduct")
//            var baseProduct: String? = "",
//            @SerializedName("categories")
//            var categories: List<Category?>? = listOf(),
//            @SerializedName("classifications")
//            var classifications: List<Classification?>? = listOf(),
//            @SerializedName("code")
//            var code: String? = "",
//            @SerializedName("configurable")
//            var configurable: Boolean? = false,
//            @SerializedName("configuratorType")
//            var configuratorType: String? = "",
//            @SerializedName("countryOfOrigin")
//            var countryOfOrigin: String? = "",
//            @SerializedName("countryOfOriginIsocode")
//            var countryOfOriginIsocode: String? = "",
//            @SerializedName("description")
//            var description: String? = "",
//            @SerializedName("futureStocks")
//            var futureStocks: List<FutureStock?>? = listOf(),
//            @SerializedName("images")
//            var images: List<Image?>? = listOf(),
//            @SerializedName("manufacturer")
//            var manufacturer: String? = "",
//            @SerializedName("multidimensional")
//            var multidimensional: Boolean? = false,
//            @SerializedName("name")
//            var name: String? = "",
//            @SerializedName("numberOfReviews")
//            var numberOfReviews: Double? = 0.0,
//            @SerializedName("potentialPromotions")
//            var potentialPromotions: List<PotentialPromotion?>? = listOf(),
//            @SerializedName("price")
//            var price: Price? = Price(),
//            @SerializedName("priceRange")
//            var priceRange: PriceRange? = PriceRange(),
//            @SerializedName("productReferences")
//            var productReferences: List<ProductReference?>? = listOf(),
//            @SerializedName("purchasable")
//            var purchasable: Boolean? = false,
//            @SerializedName("reviews")
//            var reviews: List<Review?>? = listOf(),
//            @SerializedName("stock")
//            var stock: Stock? = Stock(),
//            @SerializedName("summary")
//            var summary: String? = "",
//            @SerializedName("tags")
//            var tags: List<String?>? = listOf(),
//            @SerializedName("unitOfMeasure")
//            var unitOfMeasure: String? = "",
//            @SerializedName("unitOfMeasureDescription")
//            var unitOfMeasureDescription: String? = "",
//            @SerializedName("url")
//            var url: String? = "",
//            @SerializedName("variantMatrix")
//            var variantMatrix: List<VariantMatrix?>? = listOf(),
//            @SerializedName("variantOptions")
//            var variantOptions: List<VariantOption?>? = listOf(),
//            @SerializedName("variantType")
//            var variantType: String? = "",
//            @SerializedName("volumePrices")
//            var volumePrices: List<VolumePrice?>? = listOf(),
//            @SerializedName("volumePricesFlag")
//            var volumePricesFlag: Boolean? = false
//        )
//        {
//            data class BaseOption(
//                @SerializedName("options")
//                var options: List<Option?>? = listOf(),
//                @SerializedName("selected")
//                var selected: Selected? = Selected(),
//                @SerializedName("variantType")
//                var variantType: String? = ""
//            ) {
//                data class Option(
//                    @SerializedName("code")
//                    var code: String? = "",
//                    @SerializedName("priceData")
//                    var priceData: PriceData? = PriceData(),
//                    @SerializedName("stock")
//                    var stock: Stock? = Stock(),
//                    @SerializedName("url")
//                    var url: String? = "",
//                    @SerializedName("variantOptionQualifiers")
//                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
//                ) {
//                    data class PriceData(
//                        @SerializedName("currencyIso")
//                        var currencyIso: String? = "",
//                        @SerializedName("formattedValue")
//                        var formattedValue: String? = "",
//                        @SerializedName("maxQuantity")
//                        var maxQuantity: Double? = 0.0,
//                        @SerializedName("minQuantity")
//                        var minQuantity: Double? = 0.0,
//                        @SerializedName("priceType")
//                        var priceType: String? = "",
//                        @SerializedName("value")
//                        var value: Double? = 0.0
//                    )
//
//                    data class Stock(
//                        @SerializedName("stockLevel")
//                        var stockLevel: Double? = 0.0,
//                        @SerializedName("stockLevelStatus")
//                        var stockLevelStatus: String? = ""
//                    )
//
//                    data class VariantOptionQualifier(
//                        @SerializedName("image")
//                        var image: Image? = Image(),
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("qualifier")
//                        var qualifier: String? = "",
//                        @SerializedName("value")
//                        var value: String? = ""
//                    ) {
//                        data class Image(
//                            @SerializedName("altText")
//                            var altText: String? = "",
//                            @SerializedName("format")
//                            var format: String? = "",
//                            @SerializedName("galleryIndex")
//                            var galleryIndex: Double? = 0.0,
//                            @SerializedName("imageType")
//                            var imageType: String? = "",
//                            @SerializedName("url")
//                            var url: String? = ""
//                        )
//                    }
//                }
//
//                data class Selected(
//                    @SerializedName("code")
//                    var code: String? = "",
//                    @SerializedName("priceData")
//                    var priceData: PriceData? = PriceData(),
//                    @SerializedName("stock")
//                    var stock: Stock? = Stock(),
//                    @SerializedName("url")
//                    var url: String? = "",
//                    @SerializedName("variantOptionQualifiers")
//                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
//                ) {
//                    data class PriceData(
//                        @SerializedName("currencyIso")
//                        var currencyIso: String? = "",
//                        @SerializedName("formattedValue")
//                        var formattedValue: String? = "",
//                        @SerializedName("maxQuantity")
//                        var maxQuantity: Double? = 0.0,
//                        @SerializedName("minQuantity")
//                        var minQuantity: Double? = 0.0,
//                        @SerializedName("priceType")
//                        var priceType: String? = "",
//                        @SerializedName("value")
//                        var value: Double? = 0.0
//                    )
//
//                    data class Stock(
//                        @SerializedName("stockLevel")
//                        var stockLevel: Double? = 0.0,
//                        @SerializedName("stockLevelStatus")
//                        var stockLevelStatus: String? = ""
//                    )
//
//                    data class VariantOptionQualifier(
//                        @SerializedName("image")
//                        var image: Image? = Image(),
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("qualifier")
//                        var qualifier: String? = "",
//                        @SerializedName("value")
//                        var value: String? = ""
//                    ) {
//                        data class Image(
//                            @SerializedName("altText")
//                            var altText: String? = "",
//                            @SerializedName("format")
//                            var format: String? = "",
//                            @SerializedName("galleryIndex")
//                            var galleryIndex: Double? = 0.0,
//                            @SerializedName("imageType")
//                            var imageType: String? = "",
//                            @SerializedName("url")
//                            var url: String? = ""
//                        )
//                    }
//                }
//            }
//
//            data class Category(
//                @SerializedName("code")
//                var code: String? = "",
//                @SerializedName("image")
//                var image: Image? = Image(),
//                @SerializedName("name")
//                var name: String? = "",
//                @SerializedName("plpPicture")
//                var plpPicture: PlpPicture? = PlpPicture(),
//                @SerializedName("plpPictureResponsive")
//                var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
//                @SerializedName("url")
//                var url: String? = ""
//            ) {
//                data class Image(
//                    @SerializedName("altText")
//                    var altText: String? = "",
//                    @SerializedName("format")
//                    var format: String? = "",
//                    @SerializedName("galleryIndex")
//                    var galleryIndex: Double? = 0.0,
//                    @SerializedName("imageType")
//                    var imageType: String? = "",
//                    @SerializedName("url")
//                    var url: String? = ""
//                )
//
//                data class PlpPicture(
//                    @SerializedName("altText")
//                    var altText: String? = "",
//                    @SerializedName("format")
//                    var format: String? = "",
//                    @SerializedName("galleryIndex")
//                    var galleryIndex: Double? = 0.0,
//                    @SerializedName("imageType")
//                    var imageType: String? = "",
//                    @SerializedName("url")
//                    var url: String? = ""
//                )
//
//                data class PlpPictureResponsive(
//                    @SerializedName("altText")
//                    var altText: String? = "",
//                    @SerializedName("format")
//                    var format: String? = "",
//                    @SerializedName("galleryIndex")
//                    var galleryIndex: Double? = 0.0,
//                    @SerializedName("imageType")
//                    var imageType: String? = "",
//                    @SerializedName("url")
//                    var url: String? = ""
//                )
//            }
//
//            data class Classification(
//                @SerializedName("code")
//                var code: String? = "",
//                @SerializedName("features")
//                var features: List<Feature?>? = listOf(),
//                @SerializedName("name")
//                var name: String? = ""
//            ) {
//                data class Feature(
//                    @SerializedName("code")
//                    var code: String? = "",
//                    @SerializedName("comparable")
//                    var comparable: Boolean? = false,
//                    @SerializedName("description")
//                    var description: String? = "",
//                    @SerializedName("featureUnit")
//                    var featureUnit: FeatureUnit? = FeatureUnit(),
//                    @SerializedName("featureValues")
//                    var featureValues: List<FeatureValue?>? = listOf(),
//                    @SerializedName("name")
//                    var name: String? = "",
//                    @SerializedName("range")
//                    var range: Boolean? = false,
//                    @SerializedName("type")
//                    var type: String? = ""
//                ) {
//                    data class FeatureUnit(
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("symbol")
//                        var symbol: String? = "",
//                        @SerializedName("unitType")
//                        var unitType: String? = ""
//                    )
//
//                    data class FeatureValue(
//                        @SerializedName("value")
//                        var value: String? = ""
//                    )
//                }
//            }
//
//            data class FutureStock(
//                @SerializedName("date")
//                var date: String? = "",
//                @SerializedName("formattedDate")
//                var formattedDate: String? = "",
//                @SerializedName("stock")
//                var stock: Stock? = Stock()
//            ) {
//                data class Stock(
//                    @SerializedName("stockLevel")
//                    var stockLevel: Double? = 0.0,
//                    @SerializedName("stockLevelStatus")
//                    var stockLevelStatus: String? = ""
//                )
//            }
//
//            data class Image(
//                @SerializedName("altText")
//                var altText: String? = "",
//                @SerializedName("format")
//                var format: String? = "",
//                @SerializedName("galleryIndex")
//                var galleryIndex: Double? = 0.0,
//                @SerializedName("imageType")
//                var imageType: String? = "",
//                @SerializedName("url")
//                var url: String? = ""
//            )
//
//            data class PotentialPromotion(
//                @SerializedName("code")
//                var code: String? = "",
//                @SerializedName("couldFireMessages")
//                var couldFireMessages: List<String?>? = listOf(),
//                @SerializedName("description")
//                var description: String? = "",
//                @SerializedName("enabled")
//                var enabled: Boolean? = false,
//                @SerializedName("endDate")
//                var endDate: String? = "",
//                @SerializedName("firedMessages")
//                var firedMessages: List<String?>? = listOf(),
//                @SerializedName("priority")
//                var priority: Double? = 0.0,
//                @SerializedName("productBanner")
//                var productBanner: ProductBanner? = ProductBanner(),
//                @SerializedName("promotionGroup")
//                var promotionGroup: String? = "",
//                @SerializedName("promotionType")
//                var promotionType: String? = "",
//                @SerializedName("restrictions")
//                var restrictions: List<Restriction?>? = listOf(),
//                @SerializedName("startDate")
//                var startDate: String? = "",
//                @SerializedName("title")
//                var title: String? = ""
//            ) {
//                data class ProductBanner(
//                    @SerializedName("altText")
//                    var altText: String? = "",
//                    @SerializedName("format")
//                    var format: String? = "",
//                    @SerializedName("galleryIndex")
//                    var galleryIndex: Double? = 0.0,
//                    @SerializedName("imageType")
//                    var imageType: String? = "",
//                    @SerializedName("url")
//                    var url: String? = ""
//                )
//
//                data class Restriction(
//                    @SerializedName("description")
//                    var description: String? = "",
//                    @SerializedName("restrictionType")
//                    var restrictionType: String? = ""
//                )
//            }
//
//            data class Price(
//                @SerializedName("currencyIso")
//                var currencyIso: String? = "",
//                @SerializedName("formattedValue")
//                var formattedValue: String? = "",
//                @SerializedName("maxQuantity")
//                var maxQuantity: Double? = 0.0,
//                @SerializedName("minQuantity")
//                var minQuantity: Double? = 0.0,
//                @SerializedName("priceType")
//                var priceType: String? = "",
//                @SerializedName("value")
//                var value: Double? = 0.0
//            )
//
//            data class PriceRange(
//                @SerializedName("maxPrice")
//                var maxPrice: MaxPrice? = MaxPrice(),
//                @SerializedName("minPrice")
//                var minPrice: MinPrice? = MinPrice()
//            ) {
//                data class MaxPrice(
//                    @SerializedName("currencyIso")
//                    var currencyIso: String? = "",
//                    @SerializedName("formattedValue")
//                    var formattedValue: String? = "",
//                    @SerializedName("maxQuantity")
//                    var maxQuantity: Double? = 0.0,
//                    @SerializedName("minQuantity")
//                    var minQuantity: Double? = 0.0,
//                    @SerializedName("priceType")
//                    var priceType: String? = "",
//                    @SerializedName("value")
//                    var value: Double? = 0.0
//                )
//
//                data class MinPrice(
//                    @SerializedName("currencyIso")
//                    var currencyIso: String? = "",
//                    @SerializedName("formattedValue")
//                    var formattedValue: String? = "",
//                    @SerializedName("maxQuantity")
//                    var maxQuantity: Double? = 0.0,
//                    @SerializedName("minQuantity")
//                    var minQuantity: Double? = 0.0,
//                    @SerializedName("priceType")
//                    var priceType: String? = "",
//                    @SerializedName("value")
//                    var value: Double? = 0.0
//                )
//            }
//
//            data class ProductReference(
//                @SerializedName("description")
//                var description: String? = "",
//                @SerializedName("preselected")
//                var preselected: Boolean? = false,
//                @SerializedName("quantity")
//                var quantity: Double? = 0.0,
//                @SerializedName("referenceType")
//                var referenceType: String? = ""
//            )
//
//            data class Review(
//                @SerializedName("alias")
//                var alias: String? = "",
//                @SerializedName("comment")
//                var comment: String? = "",
//                @SerializedName("date")
//                var date: String? = "",
//                @SerializedName("headline")
//                var headline: String? = "",
//                @SerializedName("id")
//                var id: String? = "",
//                @SerializedName("principal")
//                var principal: Principal? = Principal(),
//                @SerializedName("rating")
//                var rating: Double? = 0.0
//            ) {
//                data class Principal(
//                    @SerializedName("currency")
//                    var currency: Currency? = Currency(),
//                    @SerializedName("customerId")
//                    var customerId: String? = "",
//                    @SerializedName("deactivationDate")
//                    var deactivationDate: String? = "",
//                    @SerializedName("defaultAddress")
//                    var defaultAddress: DefaultAddress? = DefaultAddress(),
//                    @SerializedName("displayUid")
//                    var displayUid: String? = "",
//                    @SerializedName("firstName")
//                    var firstName: String? = "",
//                    @SerializedName("language")
//                    var language: Language? = Language(),
//                    @SerializedName("lastName")
//                    var lastName: String? = "",
//                    @SerializedName("mobileCountry")
//                    var mobileCountry: MobileCountry? = MobileCountry(),
//                    @SerializedName("mobileNumber")
//                    var mobileNumber: String? = "",
//                    @SerializedName("name")
//                    var name: String? = "",
//                    @SerializedName("title")
//                    var title: String? = "",
//                    @SerializedName("titleCode")
//                    var titleCode: String? = "",
//                    @SerializedName("uid")
//                    var uid: String? = ""
//                ) {
//                    data class Currency(
//                        @SerializedName("active")
//                        var active: Boolean? = false,
//                        @SerializedName("isocode")
//                        var isocode: String? = "",
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("symbol")
//                        var symbol: String? = ""
//                    )
//
//                    data class DefaultAddress(
//                        @SerializedName("addressName")
//                        var addressName: String? = "",
//                        @SerializedName("area")
//                        var area: Area? = Area(),
//                        @SerializedName("cellphone")
//                        var cellphone: String? = "",
//                        @SerializedName("city")
//                        var city: City? = City(),
//                        @SerializedName("companyName")
//                        var companyName: String? = "",
//                        @SerializedName("country")
//                        var country: Country? = Country(),
//                        @SerializedName("defaultAddress")
//                        var defaultAddress: Boolean? = false,
//                        @SerializedName("district")
//                        var district: String? = "",
//                        @SerializedName("email")
//                        var email: String? = "",
//                        @SerializedName("firstName")
//                        var firstName: String? = "",
//                        @SerializedName("formattedAddress")
//                        var formattedAddress: String? = "",
//                        @SerializedName("id")
//                        var id: String? = "",
//                        @SerializedName("lastName")
//                        var lastName: String? = "",
//                        @SerializedName("line1")
//                        var line1: String? = "",
//                        @SerializedName("line2")
//                        var line2: String? = "",
//                        @SerializedName("mobileCountry")
//                        var mobileCountry: MobileCountry? = MobileCountry(),
//                        @SerializedName("mobileNumber")
//                        var mobileNumber: String? = "",
//                        @SerializedName("nearestLandmark")
//                        var nearestLandmark: String? = "",
//                        @SerializedName("phone")
//                        var phone: String? = "",
//                        @SerializedName("postalCode")
//                        var postalCode: String? = "",
//                        @SerializedName("region")
//                        var region: Region? = Region(),
//                        @SerializedName("shippingAddress")
//                        var shippingAddress: Boolean? = false,
//                        @SerializedName("title")
//                        var title: String? = "",
//                        @SerializedName("titleCode")
//                        var titleCode: String? = "",
//                        @SerializedName("town")
//                        var town: String? = "",
//                        @SerializedName("visibleInAddressBook")
//                        var visibleInAddressBook: Boolean? = false
//                    ) {
//                        data class Area(
//                            @SerializedName("code")
//                            var code: String? = "",
//                            @SerializedName("name")
//                            var name: String? = ""
//                        )
//
//                        data class City(
//                            @SerializedName("areas")
//                            var areas: List<Area?>? = listOf(),
//                            @SerializedName("code")
//                            var code: String? = "",
//                            @SerializedName("name")
//                            var name: String? = ""
//                        ) {
//                            data class Area(
//                                @SerializedName("code")
//                                var code: String? = "",
//                                @SerializedName("name")
//                                var name: String? = ""
//                            )
//                        }
//
//                        data class Country(
//                            @SerializedName("isdcode")
//                            var isdcode: String? = "",
//                            @SerializedName("isocode")
//                            var isocode: String? = "",
//                            @SerializedName("name")
//                            var name: String? = ""
//                        )
//
//                        data class MobileCountry(
//                            @SerializedName("isdcode")
//                            var isdcode: String? = "",
//                            @SerializedName("isocode")
//                            var isocode: String? = "",
//                            @SerializedName("name")
//                            var name: String? = ""
//                        )
//
//                        data class Region(
//                            @SerializedName("countryIso")
//                            var countryIso: String? = "",
//                            @SerializedName("isocode")
//                            var isocode: String? = "",
//                            @SerializedName("isocodeShort")
//                            var isocodeShort: String? = "",
//                            @SerializedName("name")
//                            var name: String? = ""
//                        )
//                    }
//
//                    data class Language(
//                        @SerializedName("active")
//                        var active: Boolean? = false,
//                        @SerializedName("isocode")
//                        var isocode: String? = "",
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("nativeName")
//                        var nativeName: String? = ""
//                    )
//
//                    data class MobileCountry(
//                        @SerializedName("isdcode")
//                        var isdcode: String? = "",
//                        @SerializedName("isocode")
//                        var isocode: String? = "",
//                        @SerializedName("name")
//                        var name: String? = ""
//                    )
//                }
//            }
//
//            data class Stock(
//                @SerializedName("stockLevel")
//                var stockLevel: Double? = 0.0,
//                @SerializedName("stockLevelStatus")
//                var stockLevelStatus: String? = ""
//            )
//
//            data class VariantMatrix(
//                @SerializedName("elements")
//                var elements: List<Any?>? = listOf(),
//                @SerializedName("isLeaf")
//                var isLeaf: Boolean? = false,
//                @SerializedName("parentVariantCategory")
//                var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
//                @SerializedName("variantOption")
//                var variantOption: VariantOption? = VariantOption(),
//                @SerializedName("variantValueCategory")
//                var variantValueCategory: VariantValueCategory? = VariantValueCategory()
//            ) {
//                data class ParentVariantCategory(
//                    @SerializedName("hasImage")
//                    var hasImage: Boolean? = false,
//                    @SerializedName("name")
//                    var name: String? = "",
//                    @SerializedName("priority")
//                    var priority: Double? = 0.0
//                )
//
//                data class VariantOption(
//                    @SerializedName("code")
//                    var code: String? = "",
//                    @SerializedName("priceData")
//                    var priceData: PriceData? = PriceData(),
//                    @SerializedName("stock")
//                    var stock: Stock? = Stock(),
//                    @SerializedName("url")
//                    var url: String? = "",
//                    @SerializedName("variantOptionQualifiers")
//                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
//                ) {
//                    data class PriceData(
//                        @SerializedName("currencyIso")
//                        var currencyIso: String? = "",
//                        @SerializedName("formattedValue")
//                        var formattedValue: String? = "",
//                        @SerializedName("maxQuantity")
//                        var maxQuantity: Double? = 0.0,
//                        @SerializedName("minQuantity")
//                        var minQuantity: Double? = 0.0,
//                        @SerializedName("priceType")
//                        var priceType: String? = "",
//                        @SerializedName("value")
//                        var value: Double? = 0.0
//                    )
//
//                    data class Stock(
//                        @SerializedName("stockLevel")
//                        var stockLevel: Double? = 0.0,
//                        @SerializedName("stockLevelStatus")
//                        var stockLevelStatus: String? = ""
//                    )
//
//                    data class VariantOptionQualifier(
//                        @SerializedName("image")
//                        var image: Image? = Image(),
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("qualifier")
//                        var qualifier: String? = "",
//                        @SerializedName("value")
//                        var value: String? = ""
//                    ) {
//                        data class Image(
//                            @SerializedName("altText")
//                            var altText: String? = "",
//                            @SerializedName("format")
//                            var format: String? = "",
//                            @SerializedName("galleryIndex")
//                            var galleryIndex: Double? = 0.0,
//                            @SerializedName("imageType")
//                            var imageType: String? = "",
//                            @SerializedName("url")
//                            var url: String? = ""
//                        )
//                    }
//                }
//
//                data class VariantValueCategory(
//                    @SerializedName("name")
//                    var name: String? = "",
//                    @SerializedName("sequence")
//                    var sequence: Double? = 0.0,
//                    @SerializedName("superCategories")
//                    var superCategories: List<SuperCategory?>? = listOf()
//                ) {
//                    data class SuperCategory(
//                        @SerializedName("hasImage")
//                        var hasImage: Boolean? = false,
//                        @SerializedName("name")
//                        var name: String? = "",
//                        @SerializedName("priority")
//                        var priority: Double? = 0.0
//                    )
//                }
//            }
//
//            data class VariantOption(
//                @SerializedName("code")
//                var code: String? = "",
//                @SerializedName("priceData")
//                var priceData: PriceData? = PriceData(),
//                @SerializedName("stock")
//                var stock: Stock? = Stock(),
//                @SerializedName("url")
//                var url: String? = "",
//                @SerializedName("variantOptionQualifiers")
//                var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
//            ) {
//                data class PriceData(
//                    @SerializedName("currencyIso")
//                    var currencyIso: String? = "",
//                    @SerializedName("formattedValue")
//                    var formattedValue: String? = "",
//                    @SerializedName("maxQuantity")
//                    var maxQuantity: Double? = 0.0,
//                    @SerializedName("minQuantity")
//                    var minQuantity: Double? = 0.0,
//                    @SerializedName("priceType")
//                    var priceType: String? = "",
//                    @SerializedName("value")
//                    var value: Double? = 0.0
//                )
//
//                data class Stock(
//                    @SerializedName("stockLevel")
//                    var stockLevel: Double? = 0.0,
//                    @SerializedName("stockLevelStatus")
//                    var stockLevelStatus: String? = ""
//                )
//
//                data class VariantOptionQualifier(
//                    @SerializedName("image")
//                    var image: Image? = Image(),
//                    @SerializedName("name")
//                    var name: String? = "",
//                    @SerializedName("qualifier")
//                    var qualifier: String? = "",
//                    @SerializedName("value")
//                    var value: String? = ""
//                ) {
//                    data class Image(
//                        @SerializedName("altText")
//                        var altText: String? = "",
//                        @SerializedName("format")
//                        var format: String? = "",
//                        @SerializedName("galleryIndex")
//                        var galleryIndex: Double? = 0.0,
//                        @SerializedName("imageType")
//                        var imageType: String? = "",
//                        @SerializedName("url")
//                        var url: String? = ""
//                    )
//                }
//            }
//
//            data class VolumePrice(
//                @SerializedName("currencyIso")
//                var currencyIso: String? = "",
//                @SerializedName("formattedValue")
//                var formattedValue: String? = "",
//                @SerializedName("maxQuantity")
//                var maxQuantity: Double? = 0.0,
//                @SerializedName("minQuantity")
//                var minQuantity: Double? = 0.0,
//                @SerializedName("priceType")
//                var priceType: String? = "",
//                @SerializedName("value")
//                var value: Double? = 0.0
//            )
//        }
    }
}