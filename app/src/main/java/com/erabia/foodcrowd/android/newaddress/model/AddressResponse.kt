package com.erabia.foodcrowd.android.newaddress.model


import com.google.gson.annotations.SerializedName

data class AddressResponse(
    @SerializedName("addressName")
    var addressName: String? = "",
    @SerializedName("apartmentNumber")
    var apartmentNumber: String? = "",
    @SerializedName("area")
    var area: Area? = Area(),
    @SerializedName("buildingName")
    var buildingName: String? = "",
    @SerializedName("cellphone")
    var cellphone: String? = "",
    @SerializedName("city")
    var city: City? = City(),
    @SerializedName("companyName")
    var companyName: String? = "",
    @SerializedName("country")
    var country: Country? = Country(),
    @SerializedName("defaultAddress")
    var defaultAddress: Boolean? = false,
    @SerializedName("district")
    var district: String? = "",
    @SerializedName("email")
    var email: String? = "",
    @SerializedName("firstName")
    var firstName: String? = "",
    @SerializedName("formattedAddress")
    var formattedAddress: String? = "",
    @SerializedName("id")
    var id: String? = "",
    @SerializedName("lastName")
    var lastName: String? = "",
    @SerializedName("line1")
    var line1: String? = "",
    @SerializedName("line2")
    var line2: String? = "",
    @SerializedName("mobileCountry")
    var mobileCountry: MobileCountry? = MobileCountry(),
    @SerializedName("mobileNumber")
    var mobileNumber: String? = "",
    @SerializedName("nearestLandmark")
    var nearestLandmark: String? = "",
    @SerializedName("phone")
    var phone: String? = "",
    @SerializedName("postalCode")
    var postalCode: String? = "",
    @SerializedName("region")
    var region: Region? = Region(),
    @SerializedName("shippingAddress")
    var shippingAddress: Boolean? = false,
    @SerializedName("streetName")
    var streetName: String? = "",
    @SerializedName("title")
    var title: String? = "",
    @SerializedName("titleCode")
    var titleCode: String? = "",
    @SerializedName("town")
    var town: String? = "",
    @SerializedName("visibleInAddressBook")
    var visibleInAddressBook: Boolean? = false
) {
    data class Area(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )

    data class City(
        @SerializedName("areas")
        var areas: List<Area?>? = listOf(),
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) {
        data class Area(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )
    }

    data class Country(
        @SerializedName("isdcode")
        var isdcode: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )

    data class MobileCountry(
        @SerializedName("isdcode")
        var isdcode: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )

    data class Region(
        @SerializedName("countryIso")
        var countryIso: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("isocodeShort")
        var isocodeShort: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )
}