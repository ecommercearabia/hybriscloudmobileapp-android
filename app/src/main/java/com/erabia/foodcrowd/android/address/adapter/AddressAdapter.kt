package com.erabia.foodcrowd.android.address.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.viewmodel.AddressViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.databinding.RowAddressBinding


class AddressAdapter(
    var addressViewModel: AddressViewModel,
    val navController: NavController
) :
    RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {
    var onClick: IOnClick? = null
    private var context: Context? = null
    var addressList: List<Address>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var cameFrom: String? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    lateinit var binding: RowAddressBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        this.context = parent.context
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.row_address,
            parent,
            false
        )
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return addressList?.size ?: 0
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.view.addressModel = addressList?.get(position)
        holder.view.addressVM = addressViewModel
        holder.view.cardViewAddressBook.setOnClickListener(View.OnClickListener {
            if (cameFrom.equals(Constants.INTENT_KEY.CART) || cameFrom.equals(Constants.INTENT_KEY.CHECKOUT) ||
                cameFrom.equals(Constants.INTENT_KEY.CHECKOUT_BILLING)
            ) {
                onClick?.onClick(addressList?.get(position))
                holder.view.cardViewAddressBook.setBackgroundResource(R.drawable.shape_rectangle_border)
            }
        })
        if (cameFrom.equals(Constants.INTENT_KEY.CART) || cameFrom.equals(Constants.INTENT_KEY.CHECKOUT) ||
            cameFrom.equals(Constants.INTENT_KEY.CHECKOUT_BILLING)
        ) {
//            holder.view.buttonEdit.visibility = View.GONE
//            holder.view.buttonDelete.visibility = View.GONE
        }
    }

    class MyViewHolder(val view: RowAddressBinding) : RecyclerView.ViewHolder(view.root)

    override fun getItemId(position: Int): Long {
        return addressList?.get(position)?.id?.toLong() ?: 0
    }


}

interface IOnClick {
    fun onClick(address: Address?)
}

