package com.erabia.foodcrowd.android.wishlistmenu.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.storecredit.remote.repository.StoreCreditRepository
import com.erabia.foodcrowd.android.wishlistmenu.model.GetWishListResponse
import com.erabia.foodcrowd.android.wishlistmenu.model.ZipWishListModelResponse
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class WishListRepository(val mWishListService: WishListService) : Repository {
    companion object {
        const val TAG: String = "WishListRepository"

    }

    var wishListPK=""

//    val mOrderListLiveData: SingleLiveEvent<OrderHistoryListResponse> by lazy { SingleLiveEvent<OrderHistoryListResponse>() }
//    val mItemDetails: SingleLiveEvent<OrderDetailsResponse> by lazy { SingleLiveEvent<OrderDetailsResponse>() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    val mGetWishListLiveData: SingleLiveEvent<GetWishListResponse> by lazy { SingleLiveEvent<GetWishListResponse>() }
    val mAddWishListLiveData: SingleLiveEvent<Void> by lazy { SingleLiveEvent<Void>() }
    val mDeleteWishItemLiveData: SingleLiveEvent<Void> by lazy { SingleLiveEvent<Void>() }
    val mErrorServer: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }

    @SuppressLint("CheckResult")
    fun getWishList(){
        mWishListService.getCustomerWishList(
            "current"
            ,"FULL"
        ).get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE }
            .doOnTerminate { this.progressBarVisibility().value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200,201,202->{

                            wishListPK= it.body()?.pk.toString()
                            mGetWishListLiveData.value = it.body()

                        }

                        400->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }


    @SuppressLint("CheckResult")
    fun getWishListToDelete(mPrductCode:String){
        mWishListService.getCustomerWishList(
            "current"
            ,"FULL"
        ).get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE }
            .doOnTerminate { this.progressBarVisibility().value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200,201,202->{

                            wishListPK= it.body()?.pk.toString()
                            mGetWishListLiveData.value = it.body()
                            deleteWishItem(mPrductCode)
                        }

                        400->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun deleteWishItem(mPrductCode:String){
        mWishListService.deleteWishItem("current",mPrductCode, wishListPK)
            .flatMap {
            when(it.code()) {
                200,201,202-> {
                    mWishListService.getCustomerWishList(
                        "current"
                        ,"FULL"
                    )
                }
                400->{
                    throw (RuntimeException("Something wrong happened"))
                }
                else->{
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }
            .get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE }
            .doOnTerminate { this.progressBarVisibility().value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200,201,202->{
                            mGetWishListLiveData.value = it.body()
                        }

                        400->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getAndAddWishList(mPrductCode:String) {
        mWishListService.getCustomerWishList(
            "current",
            "FULL"
        ).flatMap {
            when(it.code()) {
                200,201,202-> {
                    it.body()?.pk?.let { it1 -> mWishListService.addWishList("current",mPrductCode, it1) }
                }
                400->{
                    throw (RuntimeException("Something wrong happened"))
                }
                else->{
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }
            .get().doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE }
            .doOnTerminate { this.progressBarVisibility().value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200,201->{
                            mAddWishListLiveData.value = it.body()
                        }
                        400->{
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    mErrorServer.value=it.message.toString()
                    Log.d(TAG, it.toString())
                }
            )
    }
}