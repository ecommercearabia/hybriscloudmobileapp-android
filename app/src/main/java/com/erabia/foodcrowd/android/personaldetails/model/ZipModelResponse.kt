package com.erabia.foodcrowd.android.personaldetails.model

import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import retrofit2.Response

sealed class ZipModelResponse {

    data class SuccessPersonal(val data: Response<PersonalDetailsModel>) : ZipModelResponse()
    data class SuccessTitle(val data: Response<TitleResponse>) : ZipModelResponse()
    data class SuccessCountry(val data: Response<CountryResponse>) : ZipModelResponse()
    data class SuccessNationality(val data: Response<NationalitiesResponse>) : ZipModelResponse()
    data class Error(val t: Throwable) : ZipModelResponse()
}