package com.erabia.foodcrowd.android.product.di

import com.erabia.foodcrowd.android.product.remote.repository.ProductRepository
import com.erabia.foodcrowd.android.product.view.AddReviewFragment
import com.erabia.foodcrowd.android.product.view.ProductDetailsFragment
import com.erabia.foodcrowd.android.product.view.ReviewListFragment
import dagger.Component

@Component(modules = [ProductModule::class])
interface IProductComponent {
    fun getProductRepo(): ProductRepository

    fun inject(productDetailsFragment: ProductDetailsFragment)

    fun inject(reviewListFragment: ReviewListFragment)
    fun inject(addReviewFragment: AddReviewFragment)
}