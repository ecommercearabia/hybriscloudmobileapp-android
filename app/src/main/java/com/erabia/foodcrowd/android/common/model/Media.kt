package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class Media(
    @SerializedName("altText")
    val altText: String = "",
    @SerializedName("code")
    val code: String = "",
    @SerializedName("desktop")
    @TypeConverters(DataConverter::class)
    val desktop: Desktop = Desktop(),
    @SerializedName("downloadUrl")
    val downloadUrl: String = "",
    @SerializedName("mime")
    val mime: String = "",
    @SerializedName("mobile")
    @TypeConverters(DataConverter::class)
    val mobile: Mobile = Mobile(),
    @SerializedName("tablet")
    @TypeConverters(DataConverter::class)
    val tablet: Tablet = Tablet(),
    @SerializedName("url")
    val url: String = "",
    @SerializedName("widescreen")
    @TypeConverters(DataConverter::class)
    val widescreen: Widescreen = Widescreen()
)