package com.erabia.foodcrowd.android.password.remote.repositry

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.password.remote.service.ChangePasswordService

import io.reactivex.rxkotlin.subscribeBy

class ChangePasswordRepository(val changePasswordService: ChangePasswordService) :
    Repository {
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mChangePasswordLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }

    companion object {
        const val TAG: String = "ForgetRepository"
    }


    @SuppressLint("CheckResult")
    fun changePassword(mNewPassword: String,mOldPassword: String) {
        changePasswordService.changePassword(
            "current",mNewPassword,mOldPassword
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mChangePasswordLiveData.value = it.body().toString()
                        }
                        201 -> {
                            mChangePasswordLiveData.value = it.body().toString()
                        }
                        202 -> {
                            mChangePasswordLiveData.value = it.body().toString()
                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                }
                ,
                onError =
                {
                    Log.d(TAG, it.toString())
                }
            )
    }


}
