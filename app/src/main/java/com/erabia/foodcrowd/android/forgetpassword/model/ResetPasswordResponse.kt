package com.erabia.foodcrowd.android.forgetpassword.model


import com.google.gson.annotations.SerializedName

data class ResetPasswordResponse(
    @SerializedName("errors")
    val errors: List<Error?>? = listOf()
) {
    data class Error(
        @SerializedName("message")
        val message: String? = "",
        @SerializedName("type")
        val type: String? = "")
}