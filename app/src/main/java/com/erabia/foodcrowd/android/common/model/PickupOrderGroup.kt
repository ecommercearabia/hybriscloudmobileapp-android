package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PickupOrderGroup(
    @SerializedName("deliveryPointOfService")
    val deliveryPointOfService: DeliveryPointOfService = DeliveryPointOfService(),
    @SerializedName("distance")
    val distance: Int = 0,
    @SerializedName("entries")
    val entries: List<Entry> = listOf(),
    @SerializedName("quantity")
    val quantity: Int = 0,
    @SerializedName("totalPriceWithTax")
    val totalPriceWithTax: TotalPriceWithTax = TotalPriceWithTax()
)