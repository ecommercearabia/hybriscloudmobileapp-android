package com.erabia.foodcrowd.android.signup.remote.service

import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.SiteConfig
import com.erabia.foodcrowd.android.signup.model.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface SignUpService {

    @POST("rest/v2/foodcrowd-ae/users")
    fun register
                (@Body body: RegisterPostData , @Query("fields") fields:String): Observable<Response<RegisterResponse>>

    @GET("rest/v2/foodcrowd-ae/titles")
    fun getLocalizedTitle(
        @Query("fields") fields: String
    ): Observable<Response<TitleResponse>>

    @GET("rest/v2/foodcrowd-ae/nationalities")
    fun getNationalities(
    ): Observable<Response<NationalitiesResponse>>


    @GET("rest/v2/foodcrowd-ae/countries")
    fun getCountry(
        @Query("fields") fields: String,
        @Query("type") type: String
    ): Observable<Response<CountryResponse>>

    @PATCH("rest/v2/foodcrowd-ae/users/{userId}/mobile-token")
    fun setFirebaseToken(
        @Path("userId") userId: String,
        @Query("mobileToken") mobileToken: String
    ): Observable<Response<Product>>

    @GET("rest/v2/foodcrowd-ae/config")
    fun getAppConfig(
        @Query("fields") fields: String
    ): Observable<Response<SiteConfig>>
}