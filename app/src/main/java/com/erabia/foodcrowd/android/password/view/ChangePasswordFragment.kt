package com.erabia.foodcrowd.android.password.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentChangeEmailBinding
import com.erabia.foodcrowd.android.databinding.FragmentChangePasswordBinding
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.email.viewmodel.ChangeEmailViewModel
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository
import com.erabia.foodcrowd.android.password.remote.service.ChangePasswordService
import com.erabia.foodcrowd.android.password.viewmodel.ChangePasswordViewModel
import io.reactivex.disposables.CompositeDisposable

class ChangePasswordFragment : Fragment() {

    var compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: SharedViewModel
    lateinit var binding: FragmentChangePasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = SharedViewModel.Factory(
            ChangePasswordRepository(
                RequestManager.getClient().create(
                    ChangePasswordService::class.java
                )
            ),
            ChangeEmailRepository(
                RequestManager.getClient().create(
                    ChangeEmailService::class.java
                ),  RequestManager.getClient().create(CartService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(SharedViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_change_password, container, false)
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel


        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerValidation()
        observerChangePassword()
        loadingVisibilityObserver()

    }

    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }
    fun observerValidation() {
        mViewModel.mErrorOldPasswordValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mOldPasswordEditText.error = it

        })

        mViewModel.mErrorNewPasswordValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mNewPassowrdEditText.error = it

        })
        mViewModel.mErrorConfirmPasswordValidationMessage.observe(viewLifecycleOwner, Observer {
            binding.mConfirmPasswordChangePasswordEditText.error = it

        })
    }


    fun observerChangePassword() {
        mViewModel.mChangePasswordLiveData.observe(viewLifecycleOwner, Observer {
            SharedPreference.getInstance().setFingerPrintEnabled(false)
            SharedPreference.getInstance().setLOGIN_TOKEN("")
            SharedPreference.getInstance().setLOGIN_EMAIL("")
            SharedPreference.getInstance().setRefreshToken("")
            SharedPreference.getInstance().setUser_TOKEN("")
            SharedPreference.getInstance().setApp_TOKEN("")
            SharedPreference.getInstance().setRememberMeLoginFlag(false)
            SharedPreference.getInstance().setIsLoginFirstTime(true)
//        SharedPreference.getInstance().setLOGIN_User("")
            SharedPreference.getInstance().clearSP()
            mViewModel.logoutItem.value = true

            AppUtil.showToastySuccess(  context , R.string.success)

            activity?.onBackPressed()
        })

        mViewModel.mErrorChangePasswordEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)


        })

    }


    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
}