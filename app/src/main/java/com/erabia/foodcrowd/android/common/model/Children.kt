package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

@Entity
data class Children(
    @SerializedName("children")
    @TypeConverters(DataConverter::class)
    val children: List<Children> = listOf(),
    @SerializedName("entries")
    @TypeConverters(DataConverter::class)
    val entries: List<Entry> = listOf(),
    @SerializedName("title")
    val title: String = "",
    @SerializedName("uid")
    val uid: String = "",
    @SerializedName("uuid")
    val uuid: String = ""
)