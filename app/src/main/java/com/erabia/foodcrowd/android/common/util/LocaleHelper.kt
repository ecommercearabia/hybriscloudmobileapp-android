package com.erabia.foodcrowd.android.common.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.erabia.foodcrowd.android.common.Constants
import java.util.*

class LocaleHelper {

    companion object {
        private const val ENGLISH_CODE = "en"
        private const val LANGUAGE = "language"
        private const val SUMMARY = "summary"
        private lateinit var preferences: SharedPreferences

        fun setLanguage(mContext: Context, language: String) {
            preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            preferences?.edit()?.putString(LANGUAGE, language)?.commit()
            setLocale(mContext)
        }

        fun getLanguage(mContext: Context): String? {
            preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences?.getString(LANGUAGE, ENGLISH_CODE)
        }

        fun setLocale(context: Context): Context {
            return updateResources(context, getLanguage(context))
        }

        fun setSummary(mContext: Context, summary: String) {
            preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            preferences.edit().putString(SUMMARY, summary)?.commit()

        }

        fun getSummary(mContext: Context): String {
            preferences =
                PreferenceManager.getDefaultSharedPreferences(mContext)
            return preferences.getString(SUMMARY, Constants.ENGLISH)?:""
        }

        fun updateResources(context: Context?, language: String?): Context {
            var locale = Locale(language)
            Locale.setDefault(locale)
            var res = context?.resources
            var config = res?.configuration
            config?.setLocale(locale)
            res?.updateConfiguration(config, res.displayMetrics)
            getSummary(context!!)

//            if (Build.VERSION.SDK_INT >= 17) {
//                config?.setLocale(locale)
//                //context =
//                context?.createConfigurationContext(config)
//            } else {
//                config?.locale = locale
//                res?.updateConfiguration(config, res.displayMetrics)
//            }
            return context!!
        }
    }
}
