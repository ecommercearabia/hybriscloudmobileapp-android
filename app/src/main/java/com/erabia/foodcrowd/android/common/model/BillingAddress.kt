package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class BillingAddress(
    @SerializedName("cellphone")
    var cellphone: String = "",
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("country")
    var country: Country = Country(),
    @SerializedName("defaultAddress")
    var defaultAddress: Boolean = false,
    @SerializedName("district")
    var district: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("firstName")
    var firstName: String = "",
    @SerializedName("formattedAddress")
    var formattedAddress: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("lastName")
    var lastName: String = "",
    @SerializedName("line1")
    var line1: String = "",
    @SerializedName("line2")
    var line2: String = "",
    @SerializedName("phone")
    var phone: String = "",
    @SerializedName("postalCode")
    var postalCode: String = "",
    @SerializedName("region")
    var region: Region = Region(),
    @SerializedName("shippingAddress")
    var shippingAddress: Boolean = false,
    @SerializedName("title")
    var title: String = "",
    @SerializedName("titleCode")
    var titleCode: String = "",
    @SerializedName("town")
    var town: String = "",
    @SerializedName("visibleInAddressBook")
    var visibleInAddressBook: Boolean = false
)