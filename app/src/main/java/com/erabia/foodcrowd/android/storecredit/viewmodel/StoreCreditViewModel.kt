package com.erabia.foodcrowd.android.storecredit.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import com.erabia.foodcrowd.android.storecredit.remote.repository.StoreCreditRepository

class StoreCreditViewModel(val mStoreCreditRepository: StoreCreditRepository) : ViewModel() {


    var mAvailableBalanceLiveData: LiveData<AvailableBalanceResponse> =
        mStoreCreditRepository.mAvailableBalanceLiveData
    var mStoreCreditListLiveData: LiveData<StoreCreditListResponse> =
        mStoreCreditRepository.mStoreCreditListLiveData
    val loadingVisibility: MutableLiveData<Int> = mStoreCreditRepository.loadingVisibility
    val mErrorEvent: SingleLiveEvent<String> = mStoreCreditRepository.error()
    var mAvailableBalance = ""
    var onViewOrderClickObserver: MutableLiveData<Int> = MutableLiveData()

    init {
        getAvailableBalance()
        Log.e("TAG", ":" )
    }

    private fun getAvailableBalance() {
        mStoreCreditRepository.getAvailableBalance()
    }


    class Factory(
        val mStoreCreditRepository: StoreCreditRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return StoreCreditViewModel(mStoreCreditRepository) as T
        }
    }
}