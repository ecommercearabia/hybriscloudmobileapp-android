package com.erabia.foodcrowd.android.personaldetails.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.personaldetails.model.Quadruple
import com.erabia.foodcrowd.android.personaldetails.model.ZipModelResponse
import com.erabia.foodcrowd.android.personaldetails.remote.service.PersonalDetailsService
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import com.erabia.foodcrowd.android.storecredit.model.SuperResponse
import com.erabia.foodcrowd.android.storecredit.remote.service.StoreCreditService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.Observable
import io.reactivex.functions.Function3
import io.reactivex.functions.Function4
import io.reactivex.rxkotlin.subscribeBy

class PersonalDetailsRepository(val mPersonalDetailsService: PersonalDetailsService) : Repository {
    companion object {
        const val TAG: String = "MyOrderRepository"
    }

    val mTitleListLiveData: SingleLiveEvent<TitleResponse> by lazy { SingleLiveEvent<TitleResponse>() }
    val mNationalitiesListLiveData: SingleLiveEvent<NationalitiesResponse> by lazy { SingleLiveEvent<NationalitiesResponse>() }
    val mCountryListLiveData: SingleLiveEvent<CountryResponse> by lazy { SingleLiveEvent<CountryResponse>() }
    val mPersonalDetailsModelLiveData: SingleLiveEvent<PersonalDetailsModel> by lazy { SingleLiveEvent<PersonalDetailsModel>() }
    val mUpdateProfileLiveData: SingleLiveEvent<Void> by lazy { SingleLiveEvent<Void>() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun getNationalityConfig() {
        mPersonalDetailsService.getAppConfig("FULL").get().subscribe(this, onSuccess_200 = {
            var nationalityFlag = it.body()?.registrationConfiguration?.nationalityConfigurations?.enabled
            if (nationalityFlag != null) {
                SharedPreference.getInstance().setNationalityEnabled(nationalityFlag)
            }
        }, onError_400 = {
            Log.d(SignUpRepository.TAG, it.toString())
        })
    }

    @SuppressLint("CheckResult")
    fun getNationalities() {
        mPersonalDetailsService.getNationalities().get()
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mNationalitiesListLiveData.value = it.body()
                        }
                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getTitleAndPersonalDetailsBalance() {
        var getCustomerProfileObservable = mPersonalDetailsService.getCustomerProfile(
            "current",
            "FULL"
        ).map<ZipModelResponse.SuccessPersonal> { ZipModelResponse.SuccessPersonal(it) }

        var getLocalizedTitleObservable = mPersonalDetailsService.getLocalizedTitle(
            "FULL"
        ).map<ZipModelResponse.SuccessTitle> { ZipModelResponse.SuccessTitle(it) }


        var getCountryObservable = mPersonalDetailsService.getCountry(
            "FULL", "MOBILE"
        ).map<ZipModelResponse.SuccessCountry> { ZipModelResponse.SuccessCountry(it) }

        var getNationalitiesObservable = mPersonalDetailsService.getNationalities()
            .map<ZipModelResponse.SuccessNationality> { ZipModelResponse.SuccessNationality(it) }

        Observable.zip(
            getCustomerProfileObservable,
            getLocalizedTitleObservable,
            getCountryObservable,
            getNationalitiesObservable,
            Function4 {
                    t1: ZipModelResponse.SuccessPersonal,
                    t2: ZipModelResponse.SuccessTitle,
                    t3: ZipModelResponse.SuccessCountry,
                    t4: ZipModelResponse.SuccessNationality ->
                var pairModel = Quadruple(t1, t2, t3, t4)
                pairModel
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.first.data.code()) {
                        200, 201, 202 -> {
                            mPersonalDetailsModelLiveData.value =
                                it.first.data.body()
                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.first.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.first.data))
                        }
                    }
                    when (it.second.data.code()) {
                        200, 201, 202 -> {
                            mTitleListLiveData.value =
                                it.second.data.body()
                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                    }
                    when (it.third.data.code()) {
                        200, 201, 202 -> {
                            mCountryListLiveData.value =
                                it.third.data.body()
                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.third.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.third.data))
                        }
                    }
                    when (it.fourth.data.code()) {
                        200, 201, 202 -> {
                            mNationalitiesListLiveData.value =
                                it.fourth.data.body()
                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.fourth.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.fourth.data))
                        }
                    }
                },
                onError = {
                    Log.d(TAG, it.toString())

                }
            )
    }


    @SuppressLint("CheckResult")
    fun updateProfile(){
        var lang =  LocaleHelper.getLanguage(MyApplication.getContext()!!)
        mPersonalDetailsService.updateProfile(
            "current", lang.toString() ,mPersonalDetailsModelLiveData.value
        ).get().doOnSubscribe { loadingVisibility.value= View.VISIBLE }
            .doOnTerminate { loadingVisibility.value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200, 201, 202->{
                            mUpdateProfileLiveData.value = it.body()

                        }

                        400, 401, 403 ->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }




}