package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

@Entity(tableName = "Home")
class Home() {
    @SerializedName("contentSlots")
    @TypeConverters(DataConverter::class)
    var contentSlots: ContentSlots = ContentSlots()

    @SerializedName("defaultPage")
    var defaultPage: Boolean = false

    @SerializedName("label")
    var label: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("template")
    var template: String = ""

    @SerializedName("title")
    var title: String = ""

    @SerializedName("typeCode")
    var typeCode: String = ""

    @PrimaryKey
    @SerializedName("uid")
    var uid: String = ""

    @SerializedName("uuid")
    var uuid: String = ""
}