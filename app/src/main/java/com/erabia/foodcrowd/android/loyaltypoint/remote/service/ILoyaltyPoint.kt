package com.erabia.foodcrowd.android.loyaltypoint.remote.service

import com.erabia.foodcrowd.android.loyaltypoint.model.LoyaltyPointData
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.Path
import retrofit2.http.Query

interface ILoyaltyPoint {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/loyaltypoint")
    fun getLoyaltyPoint(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<String>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/loyaltypoint/history")
    fun getLoyaltyPointHistory(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<LoyaltyPointData>>
}