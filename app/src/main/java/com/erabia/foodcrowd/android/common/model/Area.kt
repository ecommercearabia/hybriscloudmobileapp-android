package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Area(
    @SerializedName("code")
    var code: String? = null,
    @SerializedName("name")
    var name: String? = null
)
{
    fun getAreaName(areaList: List<Area>): List<String> {
        var areaNameList: MutableList<String> = ArrayList()
        for (i in 0 until areaList.size) {
            areaNameList.add(areaList.get(i).name ?: "")
        }
        return areaNameList
    }

    fun findAreaByName(areaList: List<Area>?, name: String): String? {
        return areaList?.filter { it.name == name }?.firstOrNull()?.code
    }

}