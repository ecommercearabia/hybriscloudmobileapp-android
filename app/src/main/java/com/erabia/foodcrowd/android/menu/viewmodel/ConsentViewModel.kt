package com.erabia.foodcrowd.android.menu.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.menu.remote.ConsentRepository

class ConsentViewModel (private val consentRepository: ConsentRepository): ViewModel() {


    var consentList = consentRepository.consentTemplateListLiveData
    val progressBarVisibility: SingleLiveEvent<Int> = consentRepository.progressBarVisibility()

    fun getConsent (userId: String){
        consentRepository.getConsents(userId)
    }

    fun giveConsent (userId: String, consentTemplateId: String, consentTemplateVersion: String )
    {
        consentRepository.giveConsent(userId ,consentTemplateId , consentTemplateVersion)
    }

    fun deleteConsent (userId: String, consentCode: String )
    {
        consentRepository.deleteConsent(userId , consentCode)
    }


    class Factory(
        private val consentRepository: ConsentRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ConsentViewModel(consentRepository) as T
        }
    }

}