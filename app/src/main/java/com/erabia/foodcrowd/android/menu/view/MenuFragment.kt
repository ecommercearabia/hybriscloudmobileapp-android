package com.erabia.foodcrowd.android.menu.view

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentMenuBinding
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.login.view.BiometricManager
import com.erabia.foodcrowd.android.login.view.LoginActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.menu.adapter.MenuRecyclerViewAdapter
import com.erabia.foodcrowd.android.menu.remote.ConsentRepository
import com.erabia.foodcrowd.android.menu.remote.ConsentService
import com.erabia.foodcrowd.android.menu.viewmodel.ConsentViewModel
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.order.view.MyAllOrderActivity
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository
import com.erabia.foodcrowd.android.password.remote.service.ChangePasswordService
import com.erabia.foodcrowd.android.signup.view.SignUpActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_menu.*

class MenuFragment : Fragment(), BiometricManager.BiometricCallBack {
    lateinit var adapter: MenuRecyclerViewAdapter
    var compositeDisposable = CompositeDisposable()
    lateinit var sharedViewModel: SharedViewModel
    lateinit var binding: FragmentMenuBinding
    lateinit var consentViewModel: ConsentViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val factory = SharedViewModel.Factory(
            ChangePasswordRepository(
                RequestManager.getClient().create(
                    ChangePasswordService::class.java
                )
            ),
            ChangeEmailRepository(
                RequestManager.getClient().create(
                    ChangeEmailService::class.java
                ),  RequestManager.getClient().create(CartService::class.java)
            )
        )
        val factoryConsent = ConsentViewModel.Factory(
            ConsentRepository(
                RequestManager.getClient().create(
                    ConsentService::class.java
                )
            )
        )
        sharedViewModel = ViewModelProvider(this, factory).get(SharedViewModel::class.java)
        consentViewModel = ViewModelProvider(this, factoryConsent).get(ConsentViewModel::class.java)
//        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()


        Log.d("life_cycle", sharedViewModel.toString())

    }

    override fun onStart() {
        super.onStart()
        adapter.setMenu(sharedViewModel.getMenuList())
        if (!SharedPreference.getInstance().getLoginEmail().isNullOrEmpty()) {
            consentViewModel.getConsent("current")
        }
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        Log.d("life_cycle", "start")
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
        Toast.makeText(context, "$hidden", Toast.LENGTH_LONG).show()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false)
        binding.menuVM = sharedViewModel
//        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
        sharedViewModel.getMenuText()

        sharedViewModel.dataToShare.observe(viewLifecycleOwner, Observer {
            sharedViewModel.getMenuText()
            mLoginImageView.setImageResource(R.drawable.ic_login)
            mlogimTextView.text = context?.getString(R.string.login)
            mSignUpImageView.setImageResource(R.drawable.ic_siginup)
            mSignUpTextView.text = context?.getString(R.string.sign_up)
            adapter.setMenu(sharedViewModel.getMenuList())

        })
        observeProgressBar()
        Log.d("life_cycle", "create_view")
        BiometricManager.onBiometricResult = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))

        observerIntentActivity()
        observerItemMenu()
        observerSocialMedia()
        observerNavigateToTerms()
        observerNavigateToPrivacy()
        observerLogout()
        observeSetSignature()
        adapter = MenuRecyclerViewAdapter(sharedViewModel , consentViewModel)
        binding.mMenuRecycleView.layoutManager = LinearLayoutManager(context)
        binding.mMenuRecycleView.adapter = adapter
        Log.d("life_cycle", "view_create_")

        if (!SharedPreference.getInstance().getLoginEmail().isNullOrEmpty()) {
            if (SharedPreference.getInstance().getLoginEmail()
                != SharedPreference.getInstance().getPreviousLoginEmail()
            ) {
                SharedPreference.getInstance().setFingerPrintEnabled(false)
                val biometricManager = context?.let { BiometricManager(it) }
                biometricManager?.getStatusBiometric()
            } else {
                val biometricManager = context?.let { BiometricManager(it) }
                biometricManager?.getStatusBiometric()
            }
        }

    }
    private fun observeProgressBar() {
       consentViewModel.progressBarVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            } else {
                (activity as MainActivity).binding.progressBar.visibility = it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }

        })
    }

    fun observerIntentActivity() {

        sharedViewModel.mLoginlObserver.observe(viewLifecycleOwner, Observer {
            startActivityForResult(Intent(context, LoginActivity::class.java), 99)
        })

        sharedViewModel.mOrderObserver.observe(viewLifecycleOwner, Observer {
            startActivityForResult(Intent(context, MyAllOrderActivity::class.java), 99)
        })

        sharedViewModel.mSignUpObserver.observe(viewLifecycleOwner, Observer {
            startActivityForResult(Intent(context, SignUpActivity::class.java), 99)
        })

        sharedViewModel.mPersonalObserver.observe(viewLifecycleOwner, Observer {
            findNavController().navigate(R.id.action_menuScreen_to_wishList_navigation)
        })

    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun observerItemMenu() {
        sharedViewModel.activitySelectedItem.observe(this, Observer {

        })
        sharedViewModel.customSelectedItem.observe(this, Observer {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:" + it.custom)
            startActivity(intent)
        })

        sharedViewModel.selectedItem.observe(this, Observer {
            findNavController().navigate(
                it.navAction ?: 0,
                bundleOf(Constants.COME_FROM to Constants.INTENT_KEY.MENU)
            )
        })


    }


    fun observerSocialMedia() {
        sharedViewModel.socialMedia.observe(this, Observer {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
        })
        sharedViewModel.socialMediaWhatsApp.observe(this, Observer {
            val url = it + "+97180036632&text=Hi!"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        })


    }


    fun observerNavigateToTerms() {
        sharedViewModel.termsAndConditionLiveData.observe(this, Observer {
            findNavController().navigate(R.id.action_menuScreen_to_termsAndConditionFragment)
        })
    }


    fun observerNavigateToPrivacy() {
        sharedViewModel.privacyPolicyLiveData.observe(this, Observer {
            findNavController().navigate(R.id.action_menuScreen_to_privacyPolicyFragment)
        })
    }


    fun observerLogout() {

        sharedViewModel.logoutItem.observe(this, Observer {
            sharedViewModel.getMenuText()
            mLoginImageView.setImageResource(R.drawable.ic_login)
            mlogimTextView.text = context?.getString(R.string.login)
            mSignUpImageView.setImageResource(R.drawable.ic_siginup)
            mSignUpTextView.text = context?.getString(R.string.sign_up)
            adapter.setMenu(sharedViewModel.getMenuList())
        })


    }

    private fun observeSetSignature() {
        sharedViewModel.updateSignatureLiveData.observe(viewLifecycleOwner, Observer {
            SharedPreference.getInstance().setFingerPrintEnabled(true)
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            sharedViewModel.getMenuText()
            mLoginImageView.setImageResource(R.drawable.ic_wishlist_up)
            mlogimTextView.text = context?.getString(R.string.my_wishlist)
            mSignUpImageView.setImageResource(R.drawable.ic_orders_up)
            mSignUpTextView.text = context?.getString(R.string.my_orders)

            if (!SharedPreference.getInstance().getLoginEmail().isNullOrEmpty()) {
                if (SharedPreference.getInstance().getLoginEmail()
                    != SharedPreference.getInstance().getPreviousLoginEmail()
                ) {
                    SharedPreference.getInstance().setFingerPrintEnabled(false)
                    val biometricManager = context?.let { BiometricManager(it) }
                    biometricManager?.getStatusBiometric()
                } else {
                    val biometricManager = context?.let { BiometricManager(it) }
                    biometricManager?.getStatusBiometric()
                }
            }

        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.hide()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onBiometricCallBack(message: String) {
        val deviceId = SharedPreference.getInstance().getSIGNATURE_ID()
        sharedViewModel.updateSignature(deviceId ?: "")
    }


}