package com.erabia.foodcrowd.android.managecards.remote.repository

import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.managecards.model.ManageCardData
import com.erabia.foodcrowd.android.managecards.remote.service.IManageCards

class ManageCardsRepository(val iManageCards: IManageCards) : Repository {
    companion object {
        const val TAG: String = "ManageCardsRepository"
    }

    val savedCardsLiveData: MutableLiveData<ManageCardData> by lazy { MutableLiveData<ManageCardData>() }
    val errorResponseObserver: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }


    fun loadSavedCards(
        userId: String
    ) {
        iManageCards.getSavedCards(
            "current",
            Constants.FIELDS_FULL
        ).get()
            .doOnSubscribe { this.progressBarVisibility().value= View.VISIBLE}
            .doOnTerminate { this.progressBarVisibility().value= View.GONE}
            .subscribe(this,
                onSuccess_200 = {
                    savedCardsLiveData.value = it?.body()
                },
                onError_400 = {

                    val errorResponse = getResponseErrorMessage(it)
                    errorResponseObserver.postValue(errorResponse)

//                    loadCart(userId, cartId, Constants.FIELDS_FULL)
                }
            )
    }

    fun deleteSavedCards(
        userId: String,
        customerCardId: String
    ) {
        Log.d("teem", "$userId - $customerCardId")
        iManageCards.deleteSavedCard(
            "current",
            customerCardId
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    iManageCards.getSavedCards("current", "FULL")
                }
                400, 401, 402, 403 -> {
                    error().postValue(getResponseErrorMessage(it))
                    throw (RuntimeException("Something wrong happened"))
                }
                else -> {
                    throw (RuntimeException("Something wrong happened"))
                }
            }
        }.get().doOnTerminate {
        }
            .doOnSubscribe {
//                this.progressBarVisibility().value = View.VISIBLE
            }.doFinally {
                Handler(Looper.getMainLooper()).postDelayed({
//                    this.progressBarVisibility().value = View.GONE
                }, 300)
            }
            .subscribe({
                loadSavedCards("current")
            }, {
                Log.d("deleteSavedCards", it.message ?: "")
            })
    }
//            .flatMap {
//            when (it.code()) {
//                200, 201, 202 -> {
//                    iManageCards.getSavedCards(SharedPreference.getInstance().getLoginEmail() ?: "", "FULL")
//                }
//                400, 401, 402, 403 -> {
//                    throw (RuntimeException("Something wrong happened"))
//                }
//                else -> {
//                    throw (RuntimeException("Something wrong happened"))
//                }
//            }
//        }


//        ).get()
//            .subscribe(this,
//                onSuccess_200 = {
//
//                    iManageCards.getSavedCards(SharedPreference.getInstance().getLoginEmail() ?: "", "FULL")
////                    loadSavedCards(SharedPreference.getInstance().getLoginEmail() ?: "")
//                },
//                doOnSubscribe = {}
//            )

}