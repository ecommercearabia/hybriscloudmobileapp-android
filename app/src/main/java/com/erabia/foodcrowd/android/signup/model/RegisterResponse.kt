package com.erabia.foodcrowd.android.signup.model


import com.google.gson.annotations.SerializedName

data class RegisterResponse(
    @SerializedName("currency")
    val currency: Currency? = Currency(),
    @SerializedName("customerId")
    val customerId: String? = "",
    @SerializedName("deactivationDate")
    val deactivationDate: String? = "",
    @SerializedName("defaultAddress")
    val defaultAddress: DefaultAddress? = DefaultAddress(),
    @SerializedName("displayUid")
    val displayUid: String? = "",
    @SerializedName("firstName")
    val firstName: String? = "",
    @SerializedName("language")
    val language: Language? = Language(),
    @SerializedName("lastName")
    val lastName: String? = "",
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("title")
    val title: String? = "",
    @SerializedName("titleCode")
    val titleCode: String? = "",
    @SerializedName("nationality")
    val nationality: NationalitiesResponse.Nationalities? = NationalitiesResponse.Nationalities(),
    @SerializedName("uid")
    val uid: String? = ""
) {
    data class Currency(
        @SerializedName("active")
        val active: Boolean? = false,
        @SerializedName("isocode")
        val isocode: String? = "",
        @SerializedName("name")
        val name: String? = "",
        @SerializedName("symbol")
        val symbol: String? = ""
    )

    data class DefaultAddress(
        @SerializedName("cellphone")
        val cellphone: String? = "",
        @SerializedName("companyName")
        val companyName: String? = "",
        @SerializedName("country")
        val country: Country? = Country(),
        @SerializedName("defaultAddress")
        val defaultAddress: Boolean? = false,
        @SerializedName("district")
        val district: String? = "",
        @SerializedName("email")
        val email: String? = "",
        @SerializedName("firstName")
        val firstName: String? = "",
        @SerializedName("formattedAddress")
        val formattedAddress: String? = "",
        @SerializedName("id")
        val id: String? = "",
        @SerializedName("lastName")
        val lastName: String? = "",
        @SerializedName("line1")
        val line1: String? = "",
        @SerializedName("line2")
        val line2: String? = "",
        @SerializedName("phone")
        val phone: String? = "",
        @SerializedName("postalCode")
        val postalCode: String? = "",
        @SerializedName("region")
        val region: Region? = Region(),
        @SerializedName("shippingAddress")
        val shippingAddress: Boolean? = false,
        @SerializedName("title")
        val title: String? = "",
        @SerializedName("titleCode")
        val titleCode: String? = "",
        @SerializedName("town")
        val town: String? = "",
        @SerializedName("visibleInAddressBook")
        val visibleInAddressBook: Boolean? = false
    ) {
        data class Country(
            @SerializedName("isocode")
            val isocode: String? = "",
            @SerializedName("name")
            val name: String? = ""
        )

        data class Region(
            @SerializedName("countryIso")
            val countryIso: String? = "",
            @SerializedName("isocode")
            val isocode: String? = "",
            @SerializedName("isocodeShort")
            val isocodeShort: String? = "",
            @SerializedName("name")
            val name: String? = ""
        )
    }

    data class Language(
        @SerializedName("active")
        val active: Boolean? = false,
        @SerializedName("isocode")
        val isocode: String? = "",
        @SerializedName("name")
        val name: String? = "",
        @SerializedName("nativeName")
        val nativeName: String? = ""
    )
}