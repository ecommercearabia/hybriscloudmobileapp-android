package com.erabia.foodcrowd.android.common.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.newaddress.model.AreaListResponse.Area
import com.erabia.foodcrowd.android.databinding.AddressRowAreaNameBinding


class AreaNameAdapter(
    var areaList: List<Area>) :
    RecyclerView.Adapter<AreaNameAdapter.MyViewHolder>(), Filterable {

    interface OnAreaClickListener {
        fun onAreaClick(area: Area)
    }

    lateinit var binding: AddressRowAreaNameBinding
    var areaFilterList: ArrayList<Area>
    var selectedItem = 0
    var onAreaClickListener: OnAreaClickListener? = null

    init {
        areaFilterList = java.util.ArrayList(areaList)
    }

    override fun getItemCount(): Int {
        return areaFilterList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.view.area = areaFilterList.get(position)

        holder.view.clArea.setOnClickListener(View.OnClickListener {
            selectedItem = position
            notifyDataSetChanged()
            onAreaClickListener?.onAreaClick(areaFilterList[position])
        })
        if (selectedItem == position) {
            holder.view.clArea.setBackgroundColor(holder.itemView.context.resources.getColor(R.color.light_gray))

        } else {
            holder.view.clArea.setBackgroundColor(0)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.address_row_area_name,
            parent,
            false
        )
        return MyViewHolder(binding)
    }


    class MyViewHolder(var view: AddressRowAreaNameBinding) : RecyclerView.ViewHolder(view.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    areaFilterList = java.util.ArrayList(areaList)
                } else {
                    val resultList = ArrayList<Area>()
                    for (row in areaList) {
                        if ((row.name ?: "").contains(charSearch, true)) {
                            resultList.add(row)
                        }
                    }
                    areaFilterList = resultList

                }
                val filterResults = FilterResults()
                filterResults.values = areaFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                areaFilterList = results?.values as ArrayList<Area>
                notifyDataSetChanged()
            }
        }
    }

    override fun getItemId(position: Int): Long {
        return areaList?.get(position).code.hashCode()?.toLong()
    }


}