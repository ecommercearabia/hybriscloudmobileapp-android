package com.erabia.foodcrowd.android.email.viewmodel

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.TokenModel


class ChangeEmailViewModel(val mChangeEmailRepository: ChangeEmailRepository) :
    ViewModel() {
    val loadingVisibility: MutableLiveData<Int> = mChangeEmailRepository.loadingVisibility
    var mChangeEmailLiveData: LiveData<String> = mChangeEmailRepository.mChangeEmailLiveData
    var mLoginLiveData: LiveData<TokenModel> = mChangeEmailRepository.mLoginLiveData

    val mErrorEvent: SingleLiveEvent<String> = mChangeEmailRepository.error()
    var mErrorNewEmailValidationMessage = MutableLiveData<String>()
    var mErrorConfrimEmailValidationMessage = MutableLiveData<String>()
    var mErrorPasswordEmailValidationMessage = MutableLiveData<String>()

    var mErrorValidationMessage = MutableLiveData<String>()
    var mNewEmail: String = ""
    var mConfirmEmail: String = ""
    var mPassword: String = ""



    fun changeEmail() {
        if (!Patterns.EMAIL_ADDRESS.matcher(mNewEmail).matches()) {
            mErrorNewEmailValidationMessage.value = "please Enter valid email"
        }
        if (!mConfirmEmail.equals(mNewEmail)) {
            mErrorConfrimEmailValidationMessage.value = "confirm email doesn't match new email"
        }
        if (mPassword.isEmpty()) {
            mErrorPasswordEmailValidationMessage.value = "please enter password"
        }

        if (Patterns.EMAIL_ADDRESS.matcher(mNewEmail).matches()&&mConfirmEmail.equals(mNewEmail)
            &&mPassword.isNotEmpty()){
            mChangeEmailRepository.changeEmail(mNewEmail,mPassword)

        }
    }


    class Factory(
        val mChangeEmailRepository: ChangeEmailRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ChangeEmailViewModel(
                mChangeEmailRepository
            ) as T
        }
    }

}
