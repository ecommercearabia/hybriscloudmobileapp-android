package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class SuperCategory(
    @SerializedName("hasImage")
    val hasImage: Boolean = false,
    @SerializedName("name")
    val name: String = "",
    @SerializedName("priority")
    val priority: Int = 0
)