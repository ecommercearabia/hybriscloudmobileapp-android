package com.erabia.foodcrowd.android.checkout.model

import com.google.gson.annotations.SerializedName

data class StoreCreditMode(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("storeCreditModeType")
    val storeCreditModeType: StoreCreditModeType? = null
)