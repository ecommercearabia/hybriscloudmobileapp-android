package com.erabia.foodcrowd.android.category.remote.service

import com.erabia.foodcrowd.android.category.model.CategoryResponse
import com.erabia.foodcrowd.android.category.model.Subcategory
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface CategoryService {
    @GET("rest/v2/foodcrowd-ae/catalogs/{catalogId}/{catalogVersionId}/categories/{categoryId}")
    fun getCategory(
        @Path("catalogId") catalogId: String,
        @Path("catalogVersionId") catalogVersionId: String,
        @Path("categoryId") categoryId: String
    ): Observable<Response<CategoryResponse>>


    @GET("rest/v2/foodcrowd-ae/catalogs/{catalogId}/{catalogVersionId}/categories/{categoryId}")
    fun getSubCategory(
        @Path("catalogId") catalogId: String,
        @Path("catalogVersionId") catalogVersionId: String,
        @Path("categoryId") categoryId: String
    ): Observable<Response<Subcategory>>
}