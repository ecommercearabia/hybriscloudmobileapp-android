package com.erabia.foodcrowd.android.network

import android.util.Log
import com.erabia.foodcrowd.android.common.Constants.BASE_URL
import com.facebook.stetho.okhttp3.StethoInterceptor
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RequestManager {

    companion object {
        const val TAG = "RequestManager"
        var ret: Retrofit? = null
        val authInterceptor = AuthInterceptor()
        fun getClient(): Retrofit {
            if (ret == null) {
                Log.d(TAG, "getClient: authInterceptor ${authInterceptor}")
                val interceptor = HttpLoggingInterceptor()
                interceptor.level = HttpLoggingInterceptor.Level.BODY
                val okHttpClient = OkHttpClient.Builder()
                okHttpClient.authenticator(authInterceptor)
//            okHttpClient.addInterceptor(AuthInterceptor())
                okHttpClient.addInterceptor(HeadersInterceptor())
//            okHttpClient.addInterceptor(ServerInterceptor())
                okHttpClient.networkInterceptors().add(StethoInterceptor())
                okHttpClient.addInterceptor(interceptor)
                okHttpClient.connectTimeout(15, TimeUnit.SECONDS)
                okHttpClient.readTimeout(15, TimeUnit.SECONDS)
                okHttpClient.writeTimeout(15, TimeUnit.SECONDS)
                okHttpClient.build()

                ret = Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
            }
            Log.d(TAG, "getClient: ${ret}")
            return ret!!
        }
    }
}