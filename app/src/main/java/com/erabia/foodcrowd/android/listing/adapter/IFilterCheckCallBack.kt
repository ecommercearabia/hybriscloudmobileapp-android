package com.erabia.foodcrowd.android.listing.adapter

import com.erabia.foodcrowd.android.common.model.Value


interface IFilterCheckCallBack {
    fun onCheckedFilter(valueFacetsList : Value?)
}