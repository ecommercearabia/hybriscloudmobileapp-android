package com.erabia.foodcrowd.android.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.databinding.ZoomImagesLayoutBinding

class ZoomImagesAdapter : RecyclerView.Adapter<ZoomImagesAdapter.ViewHolder>() {

    private var context: Context? = null
     var selectedItem = 0
    var onImageClickListener: OnImageClickListener? = null
    var urlList: List<Image>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        this.context = parent.context
        val binding = DataBindingUtil.inflate<ZoomImagesLayoutBinding>(
            LayoutInflater.from(context),
            R.layout.zoom_images_layout,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return urlList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.url = urlList?.get(position)?.url
        holder.bind(position, urlList?.get(position)?.url)
        if (position == selectedItem) {
            holder.binding.imageView.setBackgroundResource(R.drawable.selected_rounded_image_background)
        } else {
            holder.binding.imageView.setBackgroundResource(0)
        }
    }


    inner class ViewHolder(val binding: ZoomImagesLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var url: String? = ""
        var mPosition = -1
        fun bind(position: Int, url: String?) {
            this.mPosition = position
            this.url = url
        }

        init {
            binding.imageView.setOnClickListener {
                onImageClickListener?.onImageClick(url)
                selectedItem = mPosition
                notifyDataSetChanged()
            }
        }

    }

    interface OnImageClickListener {
        fun onImageClick(url: String?)
    }
}