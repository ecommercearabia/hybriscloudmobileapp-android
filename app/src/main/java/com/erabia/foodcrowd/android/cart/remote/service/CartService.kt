package com.erabia.foodcrowd.android.cart.remote.service

import com.erabia.foodcrowd.android.common.model.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.Query

interface CartService {
    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}")
    fun getCart(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<Cart>>

    @GET("rest/v2/foodcrowd-ae/bankoffers")
    fun getBankOffers(
        @Query("fields") fields: String
    ): Observable<Response<BankOfferResponse>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/shipment-types")
    fun getShipmentTypes(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<ShipmentTypes>>

    @GET("rest/v2/foodcrowd-ae/config")
    fun getAppConfig(
        @Query("fields") fields: String
    ): Observable<Response<SiteConfig>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/shipment-types/current")
    fun getSelectedShipmentType(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String
    ): Observable<Response<SupportedShipmentType>>

    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/entries/{entryNumber}")
    fun deleteEntry(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Path("entryNumber") entryNumber: Int,
        @Query("fields") fields: String
    ): Observable<Response<Unit>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/vouchers")
    fun applyVoucher(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("voucherId") voucherId: String
    ): Observable<Response<Unit>>


    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/vouchers/{voucherId}")
    fun deleteVoucher(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Path("voucherId") voucherId: String
    ): Observable<Response<Unit>>

    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/entries")
    fun addToCart(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Body entry: AddToCartRequest,
        @Query("fields") fields: String,
        @Query("salesApp") salesApp: String
    ): Observable<Response<AddToCartResponse>>


    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/verification")
    fun doValidation(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("fields") fields: String,
        @Query("lang") lang: String

    ): Observable<Response<CartVaildationResponse>>


    @POST("rest/v2/foodcrowd-ae/users/{userId}/carts")
    fun createCart(
        @Path("userId") userId: String,
        @Query("oldCartId") oldCartId: String?,
        @Query("toMergeCartGuid") toMergeCartGuid: String?,
        @Query("fields") fields: String
    ): Observable<Response<Cart>>


    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/entries/{entryNumber}")
    fun updateQuantity(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Path("entryNumber") entryNumber: Int,
        @Body entry: Entry,
        @Query("fields") fields: String
    ): Observable<Response<UpdateQuantityResponse>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{userId}/shipment-types")
    fun updateShipmentTypes(
        @Path("userId") userId: String,
        @Query("fields") fields: String,
        @Query("code") code: String
    ): Observable<Response<UpdateShipmentTypes>>


}