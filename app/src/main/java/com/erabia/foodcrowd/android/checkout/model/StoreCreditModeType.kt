package com.erabia.foodcrowd.android.checkout.model

import com.google.gson.annotations.SerializedName

data class StoreCreditModeType(
    @SerializedName("code")
    val code: String? = null
)