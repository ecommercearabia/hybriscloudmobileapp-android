package com.erabia.foodcrowd.android.product.adapter

import android.content.Context
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Classification
import com.erabia.foodcrowd.android.databinding.ClassificationDetailsLayoutBinding
import com.erabia.foodcrowd.android.databinding.RowClassificationBinding
import java.lang.Exception

class ClassificationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        private const val VIEW_DETAILS = 1
        private const val VIEW_LIST = 2
        private const val TAG = "ClassificationAdapter"
    }

    private var position: Int = 0
    private var context: Context? = null
    var classificationList: List<Classification>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    private var binding: ViewDataBinding? = null
    private var viewHolder: RecyclerView.ViewHolder? = null
    private var isReadMoreVisible: SparseArray<Boolean> = SparseArray()
    private var layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        context = parent.context
        if (viewType == VIEW_DETAILS) {
            binding = DataBindingUtil
                .inflate<ClassificationDetailsLayoutBinding>(
                    LayoutInflater.from(context),
                    R.layout.classification_details_layout,
                    parent,
                    false
                )
            viewHolder = DetailsViewHolder((binding as ClassificationDetailsLayoutBinding?)!!)
        } else if (viewType == VIEW_LIST) {
            binding = DataBindingUtil.inflate<RowClassificationBinding>(
                LayoutInflater.from(context),
                R.layout.row_classification,
                parent,
                false
            )
            viewHolder = ListViewHolder((binding as RowClassificationBinding?)!!)
        }
        return viewHolder!!
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        this.position = position
        when (getItemViewType(position)) {
            VIEW_DETAILS -> {
                holder as DetailsViewHolder
                if (isReadMoreVisible.get(position) != null && isReadMoreVisible.get(position)) {
                    holder.binding.textViewReadMore.visibility = View.VISIBLE
                } else {
                    holder.binding.textViewReadMore.visibility = View.GONE
                }
                try {
                    holder.binding.textViewProductDetailsDescription.text =
                        classificationList?.get(position)?.features?.get(0)?.featureValues?.get(0)?.value
                } catch (e: Exception) {
                    Log.d(TAG, e.message ?: "")
                }
            }
            VIEW_LIST -> {
                holder as ListViewHolder
                holder.bind(classificationList?.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return classificationList?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            classificationList?.get(position)?.features?.size == 1 -> VIEW_DETAILS
            classificationList?.get(position)?.features?.size ?: 0 > 1 -> VIEW_LIST
            else -> -1
        }
    }

    class DetailsViewHolder(val binding: ClassificationDetailsLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    inner class ListViewHolder(val binding: RowClassificationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var classificationListAdapter: ClassificationListAdapter = ClassificationListAdapter()
        fun bind(classification: Classification?) {

            binding.recyclerViewClassification.adapter = classificationListAdapter
            binding.recyclerViewClassification.layoutManager = layoutManager
            if (this@ClassificationAdapter.position == (classificationList?.size ?: 0) - 1) {
                binding.horizontalSeparator2.visibility = View.GONE
            }
            classificationListAdapter.featureList = classification?.features
            classificationListAdapter.notifyDataSetChanged()
        }

    }
}