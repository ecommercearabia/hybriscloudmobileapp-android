package com.erabia.foodcrowd.android.aboutus.remote

import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface AboutUsService {
    @GET("rest/v2/foodcrowd-ae/cms/components/{componentId}")
    fun getAboutUsServiceDetails(
            @Path("componentId") componentId: String,
            @Query("fields") fields: String
    ): Observable<Response<ContactUsResponse>>

}