package com.erabia.foodcrowd.android.product.di

import android.content.Context
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.common.db.DataBaseManager
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.repository.HighlightedProductRepository
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.product.remote.repository.ProductRepository
import com.erabia.foodcrowd.android.product.remote.service.ProductService
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService
import dagger.Module
import dagger.Provides

@Module
class ProductModule(val context: Context) {

    @Provides
    fun homeService() = RequestManager.getClient().create(HomeService::class.java)

    @Provides
    fun cartService() = RequestManager.getClient().create(CartService::class.java)

    @Provides
    fun homeDao() = DataBaseManager.getInstance(context).homeDao()


    @Provides
    fun homeRepository(homeService: HomeService, cartService: CartService, homeDao: HomeDao) =
        HomeRepository(
            homeService, cartService, homeDao
        )

    @Provides
    fun wishListRepository() =
        WishListRepository(RequestManager.getClient().create(WishListService::class.java))

    @Provides
    fun productRepository() =
        ProductRepository(RequestManager.getClient().create(ProductService::class.java))

    @Provides
    fun cartRepository() =
        CartRepository(RequestManager.getClient().create(CartService::class.java))

    @Provides
    fun highlightedProductRepository(homeService: HomeService, homeDao: HomeDao) =
        HighlightedProductRepository(homeService, homeDao)

    @Provides
    fun highLightedProductsViewModel(highlightedProductRepository: HighlightedProductRepository) =
        HighLightedProductsViewModel(highlightedProductRepository)


    @Provides
    fun factory(
        homeRepository: HomeRepository,
        wishListRepository: WishListRepository,
        productRepository: ProductRepository,
        cartRepository: CartRepository
    ) =
        ProductViewModel.Factory(
            homeRepository,
            wishListRepository,
            productRepository,
            cartRepository
        )


}