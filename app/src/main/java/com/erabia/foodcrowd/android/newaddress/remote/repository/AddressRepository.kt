package com.erabia.foodcrowd.android.newaddress.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.newaddress.model.*
import com.erabia.foodcrowd.android.newaddress.remote.service.AddressService
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import okhttp3.internal.filterList

class AddressRepository(
    private val mAddressService: AddressService

) : Repository {

    var mIsoCode = ""
    var mCityCode = ""
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mAddressListLiveData: SingleLiveEvent<AddressList> by lazy { SingleLiveEvent<AddressList>() }
    val mDeleteAddressLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val mCountryListLiveData: SingleLiveEvent<List<CountryResponse.Country>> by lazy { SingleLiveEvent<List<CountryResponse.Country>>() }
    val mMobileCountryListLiveData: SingleLiveEvent<List<MobileCountryResponse.Country>> by lazy { SingleLiveEvent<List<MobileCountryResponse.Country>>() }
    val mCityListLiveData: SingleLiveEvent<CityListResponse> by lazy { SingleLiveEvent<CityListResponse>() }
    val mErrorEvent: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val mAreaListLiveData: SingleLiveEvent<AreaListResponse> by lazy { SingleLiveEvent<AreaListResponse>() }
    val mTitleListLiveData: SingleLiveEvent<TitleResponse> by lazy { SingleLiveEvent<TitleResponse>() }
    val mPersonalDetailsLiveData: SingleLiveEvent<PersonalDetailsModel> by lazy { SingleLiveEvent<PersonalDetailsModel>() }
    val mAddAdressLiveEvent: SingleLiveEvent<Address> by lazy { SingleLiveEvent<Address>() }
    val mObserverEndLiveData: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val updateAddressEvent: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val navigateFromAddressToCheckout: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }

    companion object {
        const val TAG: String = "mAddressService"
    }

    @SuppressLint("CheckResult")
    fun loadAddressList() {
        mAddressService.getAddressList(
            "current", Constants.FIELDS_FULL
        ).get().subscribe(this, onSuccess_200={
            mAddressListLiveData.value = it.body()
        }, onSuccess_201={
            mAddressListLiveData.value = it.body()
        })
//        .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
//            .doOnTerminate { loadingVisibility.value = View.GONE }
//            .subscribeBy(
//                onNext =
//                {
//                    when (it.code()) {
//                        200, 201 -> {
//                            mAddressListLiveData.value = it.body()
//                        }
//                        400 -> {
//                            error().postValue(getResponseErrorMessage(it))
//
//                        }
//                        else -> {
//                            error().postValue(getResponseErrorMessage(it))
//                        }
//                    }
//                },
//                onError =
//                {
//                    Log.d(AddressRepository.TAG, it.toString())
//                }
//            )
    }

    @SuppressLint("CheckResult")
    fun deleteAddress(addressId: String) {
        mAddressService.deleteAddress(
            "current", addressId
        ).flatMap {
            when (it.code()) {
                200, 201, 202 -> {
                    mDeleteAddressLiveData.postValue("Address deleted")
                    mAddressService.getAddressList(
                        "current", Constants.FIELDS_FULL
                    )
                }
                400 -> {
                    throw (RuntimeException("Something wrong happened"))

                }
                else -> {
                    throw (RuntimeException("Something wrong happened"))

                }
            }
        }
            .get().subscribe(this, onSuccess_200={
                when (it.code()) {
                    200, 201, 202 -> {
                        mAddressListLiveData.value = it.body()
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },onSuccess_201 = {
                when (it.code()) {
                    200, 201, 202 -> {
                        mAddressListLiveData.value = it.body()
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            })
//            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
//            .doOnTerminate { loadingVisibility.value = View.GONE }
//            .subscribeBy(
//                onNext =
//                {
//                    when (it.code()) {
//                        200, 201, 202 -> {
//                            mAddressListLiveData.value = it.body()
//                        }
//                        400 -> {
//                            error().postValue(getResponseErrorMessage(it))
//                        }
//                        else -> {
//                            error().postValue(getResponseErrorMessage(it))
//                        }
//                    }
//                },
//                onError =
//                {
//                    Log.d(MyOrderRepository.TAG, it.toString())
//                }
//            )
    }

    @SuppressLint("CheckResult")
    fun getAllDataSpinnerForAddAddress() {

        var getLocalizedTitleObservable = mAddressService.getLocalizedTitle(
            "FULL"
        ).map<ZipModelAddressResponse.SuccessTitle> {
            ZipModelAddressResponse.SuccessTitle(it)
        }

        var getCountryObservable = mAddressService.getMobileCountryList(
            "FULL", "MOBILE"
        ).map<ZipModelAddressResponse.SuccessCountry> { ZipModelAddressResponse.SuccessCountry(it) }

        Observable.zip(
            getLocalizedTitleObservable,
            getCountryObservable,
            BiFunction() { t1: ZipModelAddressResponse.SuccessTitle, t2: ZipModelAddressResponse.SuccessCountry ->
                var pairModel = Pair(t1, t2)
                pairModel
            })
            .flatMap {

                when (it.first.data.code()) {
                    200, 201, 202 -> {
                        mTitleListLiveData.postValue(
                            it.first.data.body()
                        )
                    }
                    400, 401, 403 -> {
                        throw (RuntimeException(it.first.data.message()))
                    }
                    else -> {
                        throw (RuntimeException(it.first.data.message()))
                    }
                }



                when (it.second.data.code()) {
                    200, 201, 202 -> {
                        var mJsonElementMobileCountry = Gson().toJsonTree(it.second.data.body())

                        var mMobileCountryResponse = Gson().fromJson(
                            mJsonElementMobileCountry,
                            MobileCountryResponse::class.java
                        )
                        var mCountryListResponse =
                            Gson().fromJson(mJsonElementMobileCountry, CountryResponse::class.java)

                        val mobileListResponse=mMobileCountryResponse.countries

                        mMobileCountryListLiveData.postValue(mobileListResponse)


                        val countriesListResponse=mCountryListResponse.countries.filterList { this.isocode!="JO" }

                        mCountryListLiveData.postValue(countriesListResponse)


                        mIsoCode = mCountryListResponse?.countries?.get(0)?.isocode ?: ""

                        mAddressService.getCustomerProfile("current", "FULL")
//                    mAddressService.getCities(
//                        mIsoCode, "FULL"
//                    )
//                        .flatMap {
//                        when (it.code()) {
//
//                            200, 201, 202 -> {
//                                mCityListLiveData.postValue(it.body())
//                                mCityCode = it.body()?.cities?.get(0)?.code ?: ""
//                                mAddressService.getArea(
//                                    mIsoCode, mCityCode, "FULL"
//                                ).flatMap {
//                                    when (it.code()) {
//                                        200, 201, 202 -> {
//
//                                            mAreaListLiveData.postValue(
//                                                it.body()
//                                            )
//                                            mAddressService.getCustomerProfile("current", "FULL")
//                                        }
//                                        400, 401, 403 -> {
//                                            throw (RuntimeException(it.message()))
//                                        }
//                                        else -> {
//                                            throw (RuntimeException(it.message()))
//                                        }
//                                    }
//
//                                }
//
//                            }
//                            400, 401, 403 -> {
//                                throw (RuntimeException(it.message()))
//                            }
//                            else -> {
//                                throw (RuntimeException(it.message()))
//
//                            }
//                        }
//
//                    }
                    }
                    400, 401, 403 -> {
                        throw (RuntimeException(it.second.data.message()))
                    }
                    else -> {
                        throw (RuntimeException(it.second.data.message()))
                    }
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate {
                loadingVisibility.value = View.GONE
            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201, 202 -> {
                            mPersonalDetailsLiveData.postValue(
                                it.body()
                            )
                        }
                        400, 401, 403 -> {
                            throw (RuntimeException(it.message()))
                        }
                        else -> {
                            throw (RuntimeException(it.message()))
                        }
                    }
                },
                onError = {
                    mErrorEvent.value = it.message.toString()
                    Log.d(TAG, it.toString())
                }
            )
    }


    @SuppressLint("CheckResult")
    fun getAllDataSpinnerForEditAddress() {

        var getLocalizedTitleObservable = mAddressService.getLocalizedTitle(
            "FULL"
        ).map<ZipModelAddressResponse.SuccessTitle> { ZipModelAddressResponse.SuccessTitle(it) }


        var getCountryObservable = mAddressService.getMobileCountryList(
            "FULL", "MOBILE"
        ).map<ZipModelAddressResponse.SuccessCountry> { ZipModelAddressResponse.SuccessCountry(it) }

        Observable.zip(
            getLocalizedTitleObservable,
            getCountryObservable,
            BiFunction { t1: ZipModelAddressResponse.SuccessTitle, t2: ZipModelAddressResponse.SuccessCountry ->
                var pairModel = Pair(t1, t2)
                pairModel
            }).flatMap {

            when (it.first.data.code()) {
                200, 201, 202 -> {
                    mTitleListLiveData.postValue(
                        it.first.data.body()
                    )
                }
                400, 401, 403 -> {
                    throw (RuntimeException(it.first.data.message()))
                }
                else -> {
                    throw (RuntimeException(it.first.data.message()))
                }
            }

            when (it.second.data.code()) {
                200, 201, 202 -> {
                    var mJsonElementMobileCountry = Gson().toJsonTree(it.second.data.body())

                    var mMobileCountryResponse = Gson().fromJson(
                        mJsonElementMobileCountry,
                        MobileCountryResponse::class.java
                    )
                    var mCountryListResponse =
                        Gson().fromJson(mJsonElementMobileCountry, CountryResponse::class.java)
                    val mobileListResponse=mMobileCountryResponse.countries

                    mMobileCountryListLiveData.postValue(mobileListResponse)

                    val countriesListResponse=mCountryListResponse.countries.filterList { this.isocode!="JO" }

                    mCountryListLiveData.postValue(countriesListResponse)
//                    mCountryListLiveData.postValue(mCountryListResponse)

                    mIsoCode = countriesListResponse.get(0).isocode ?: ""

                    mAddressService.getCities(
                        mIsoCode, "FULL"
                    ).flatMap {
                        when (it.code()) {

                            200, 201, 202 -> {
                                mCityListLiveData.postValue(it.body())
                                mCityCode = it.body()?.cities?.get(0)?.code ?: ""
                                mAddressService.getArea(
                                    mIsoCode, mCityCode, "FULL"
                                )

                            }
                            400, 401, 403 -> {
                                throw (RuntimeException(it.message()))
                            }
                            else -> {
                                throw (RuntimeException(it.message()))

                            }
                        }

                    }
                }
                400, 401, 403 -> {
                    throw (RuntimeException(it.second.data.message()))
                }
                else -> {
                    throw (RuntimeException(it.second.data.message()))
                }
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate {
                loadingVisibility.value = View.GONE

            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201, 202 -> {

                            mAreaListLiveData.postValue(
                                it.body()
                            )
                        }
                        400, 401, 403 -> {
                            throw (RuntimeException(it.message()))
                        }
                        else -> {
                            throw (RuntimeException(it.message()))
                        }
                    }
                },
                onError = {
                    mErrorEvent.value = it.message.toString()
                    Log.d(TAG, it.toString())
                }
            )
    }


    @SuppressLint("CheckResult")
    fun getCityListAndArea(mIsoCode: String) {
        mAddressService.getCities(
            mIsoCode, "FULL"
        ).flatMap {
            when (it.code()) {

                200, 201, 202 -> {
                    mCityListLiveData.postValue(it.body())
                    mCityCode = it.body()?.cities?.get(0)?.code ?: ""
                    mAddressService.getArea(
                        mIsoCode, mCityCode, "FULL"
                    )

                }
                400, 401, 403 -> {
                    throw (RuntimeException(it.message()))
                }
                else -> {
                    throw (RuntimeException(it.message()))

                }
            }
        }
            .get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201, 202 -> {
                            mAreaListLiveData.postValue(
                                it.body()
                            )
                        }
                        400, 401, 403 -> {
                            throw (RuntimeException(it.message()))
                        }
                        else -> {
                            throw (RuntimeException(it.message()))
                        }
                    }
                },
                onError =
                {
                    mErrorEvent.value = it.message.toString()
                    Log.d(WishListRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getArea(mIsoCode: String, mCityCode: String) {
        mAddressService.getArea(
            mIsoCode, mCityCode, "FULL"
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate {
                loadingVisibility.value = View.GONE
            }
            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200, 201, 202 -> {
                            mAreaListLiveData.value = it.body()
                            mObserverEndLiveData.postValue(1)
                        }
                        400 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun createAddress(address: AddressRequest, comeFrom: String) {
        mAddressService.createAddress("current", address)
            .get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribe({
                when (it.code()) {
                    200, 201 -> {
                        if (comeFrom.equals(Constants.INTENT_KEY.CART) || comeFrom.equals(
                                Constants.INTENT_KEY.CHECKOUT
                            )
                        ) {
                            mAddAdressLiveEvent.postValue(it.body())
                        } else {
                            mAddAdressLiveEvent.postValue(it.body())

                        }
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                }
            },
                {
                    Log.d(TAG, it.toString())
                })
    }

    @SuppressLint("CheckResult")
    fun updateAddress(addressId: String, address: AddressRequest) {
        mAddressService.updateAddress(
            "current", addressId, address
        ).get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribe({
                when (it.code()) {
                    200, 201, 202 -> {
                        updateAddressEvent.postValue(R.string.update_address)
                    }
                    400 -> {
                        error().postValue(getResponseErrorMessage(it))
                    }
                    else -> {
                        error().postValue(getResponseErrorMessage(it))

                    }
                }
            },
                {
                    Log.d(TAG, it.toString())
                })
    }

    @SuppressLint("CheckResult")
    fun setCartDeliveryAddress(address: Address): LiveData<Int> {
        val liveData = MutableLiveData<Int>()
        mAddressService.setDeliveryAddress("current", "current", address.id)
            .get().subscribe(this, onSuccess_200={
                navigateFromAddressToCheckout.postValue(R.id.action_navigate_to_checkout)
            },onSuccess_201 = {
                navigateFromAddressToCheckout.postValue(R.id.action_navigate_to_checkout)
            })

//            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
//            .doOnTerminate { loadingVisibility.value = View.GONE }
//            .subscribe({
//                when (it.code()) {
//                    200, 201 -> {
//                        navigateFromAddressToCheckout.postValue(R.id.action_navigate_to_checkout)
//
//                    }
//                    400 -> {
//                        error().postValue(getResponseErrorMessage(it))
//                    }
//                    else -> {
//                        error().postValue(getResponseErrorMessage(it))
//                    }
//                }
//            })

        return liveData
    }

}
