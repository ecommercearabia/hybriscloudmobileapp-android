package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class QueryValue(
    @SerializedName("value")
    var value: String? = null
)

