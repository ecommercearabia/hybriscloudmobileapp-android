package com.erabia.foodcrowd.android.checkout.model

enum class CheckoutItemType {
    DELIVERY_TIME,
    PROMO_CODE,
    PAYMENT_METHOD,
    ORDER_SUMMARY,
    SHIPPING_ADDRESS,
    MY_ITEMS,
    CHECKOUT_BUTTON,
    NONE
}