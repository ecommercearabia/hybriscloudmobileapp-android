package com.erabia.foodcrowd.android.loginwithotp.remote.repository

import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.SendEmailOtp
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.home.remote.service.LoginWithOtpService

class LoginWithOtpRepository(
    private val loginWithOtpService: LoginWithOtpService
) :
    Repository {
    val sendEmailSuccessEvent: SingleLiveEvent<SendEmailOtp> by lazy { SingleLiveEvent<SendEmailOtp>() }
    val navigateToVerificationCode: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }

    companion object {
        const val TAG: String = "LoginWithOtpRepository"
    }

    fun loginWithOtp(email: String) {
        loginWithOtpService.sendOtp(email).get().subscribe(this, {
// teems
            if(it.body()?.status == false){
                AppUtil.showToastyError(MyApplication.getContext(), it.body()?.description)
            } else {
                sendEmailSuccessEvent.value = it.body()
                navigateToVerificationCode.value = true
            }
        })
    }


}
