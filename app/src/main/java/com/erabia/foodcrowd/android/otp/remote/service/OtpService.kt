package com.erabia.foodcrowd.android.otp.remote.service

import com.erabia.foodcrowd.android.otp.model.SendOtpResponse
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.RegisterPostData
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface OtpService {

    @POST("rest/v2/foodcrowd-ae/otp/send")
    fun sendOtp
                ( @Query("countryisoCode") countryisoCode:String,@Query("mobileNumber") mobileNumber:String):
            Observable<Response<SendOtpResponse>>


    @POST("rest/v2/foodcrowd-ae/otp/verify")
    fun verifyOtp
                ( @Query("code") code:String,@Query("countryisoCode") countryisoCode:String,@Query("mobileNumber") mobileNumber:String):
            Observable<Response<SendOtpResponse>>


    @GET("rest/v2/foodcrowd-ae/titles")
    fun getLocalizedTitle(
        @Query("fields") fields: String
    ): Observable<Response<TitleResponse>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}")
    fun updateProfile(
        @Path("userId") userId: String,
        @Query("lang") lang: String,
        @Body mPersonalDetailsModel: PersonalDetailsModel?
    ): Observable<Response<Void>>
}