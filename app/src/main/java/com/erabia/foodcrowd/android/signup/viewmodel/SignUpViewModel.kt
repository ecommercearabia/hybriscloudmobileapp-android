package com.erabia.foodcrowd.android.signup.viewmodel

import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.*
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.NationalitiesResponse
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository


class SignUpViewModel(
    val mSignUpRepository: SignUpRepository,
    val mLoginRepository: LoginRepository
) : ViewModel() {

    val loadingVisibility: MutableLiveData<Int> = mSignUpRepository.loadingVisibility
    var mRegisterLiveData: LiveData<RegisterResponse> = mSignUpRepository.mRegisterLiveData
    val mErrorEvent: SingleLiveEvent<String> = mSignUpRepository.error()
    var mLoginLiveData: LiveData<TokenModel> = mLoginRepository.mLoginLiveData
    var mTitlePostion: String = ""
    var mTitleListLiveData: MutableLiveData<TitleResponse> = MutableLiveData<TitleResponse>()
    var mNationalitiesListLiveData: MutableLiveData<NationalitiesResponse> = MutableLiveData<NationalitiesResponse>()
    var countryCode: CountryResponse.Country? = null
    val selectedCountry: MutableLiveData<CountryResponse.Country> by lazy { MutableLiveData<CountryResponse.Country>() }
    var mSignupNationalityFlag: LiveData<Boolean> = mSignUpRepository.nationalitiesConfigLiveData

    var onBackImageObserver: MutableLiveData<Int> = MutableLiveData()
    var codeNumber = ""

    var email: String = ""
    var referralCode: String = ""
    var mobile_number: String = ""
    var mFirstName: String = ""
    var mLastName: String = ""
    var mPassword: String = ""
    var mConfirmPassword: String = ""
    var mTermsChecked: Boolean = false
    var mErrorEmailValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorFirstNameValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorLastNameValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorCodeNumberValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorMobileNumberValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorPasswordValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorConfirmPasswordValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorTermsValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mTitleCode: MutableLiveData<String> = MutableLiveData()
    var mNationalityCode: MutableLiveData<String> = MutableLiveData()
    var country = MutableLiveData<CountryResponse>()
    var mValidationSuccess = MutableLiveData<String>()
    var countryCodeList = MutableLiveData<CountryResponse>()
    var countryCodeLiveData = mSignUpRepository.countryCodeLiveData
    var isoCodeLiveData = mSignUpRepository.isoCodeLiveData

    init {
        getCountryList()
        getTitleList()
        getNationalitiesList()
    }

    fun getTitleList() {
        mTitleListLiveData =
            mSignUpRepository.getTitle() as MutableLiveData<TitleResponse>
    }

    fun getNationalitiesList() {
        mNationalitiesListLiveData = mSignUpRepository.getNationalities() as MutableLiveData<NationalitiesResponse>
    }

    fun getNationalitiesConfig() {
        mSignUpRepository.getNationalityConfig()
    }

    fun getCountryList() {
        country = mSignUpRepository.getCountry() as MutableLiveData<CountryResponse>
    }

    fun countryCode() {
        Log.e("asdzxc", country.value?.countries?.get(0)?.isdcode.toString())
        countryCodeList.postValue(country.value)
    }


    fun doLogin() {
        mLoginRepository.doLogin(email, mPassword)
    }


    fun doRegister() {
        if (mFirstName.isEmpty()) {
            mErrorFirstNameValidationMessage.value = "please enter first name"
        }
        if (mLastName.isEmpty()) {
            mErrorLastNameValidationMessage.value = "please enter last name"
        }
        if (codeNumber.isEmpty()) {
            mErrorCodeNumberValidationMessage.value = "please select code number"
        }
        if (mobile_number.isEmpty()) {
            mErrorMobileNumberValidationMessage.value = "please enter mobile number"
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mErrorEmailValidationMessage.value = "Enter valid email"
        }
        if (mPassword.isEmpty()) {
            mErrorPasswordValidationMessage.value = "please enter password"
        }
        if (!mTermsChecked) {
            mErrorTermsValidationMessage.value = "please enter password"
        }

//        if (!mPassword.equals(mConfirmPassword)) {
//            mErrorConfirmPasswordValidationMessage.value =
//                "confirm password doesn't match your password"
//        }

        if (mFirstName.isNotEmpty() && mLastName.isNotEmpty() && email.isNotEmpty() &&
            codeNumber.isNotEmpty() && mobile_number.isNotEmpty()
            && Patterns.EMAIL_ADDRESS.matcher(email)
                .matches() && mPassword.isNotEmpty()
        ) {
            if (mTermsChecked) {
                mValidationSuccess.value = "success"
//
//                mSignUpRepository.doRegister(
//                    mFirstName,
//                    mLastName,
//                    mPassword,
//                    mTitlePostion,
//                    email
//                )
            }

        }
    }


    fun onCheckedChange(Checked: Boolean) {
        mTermsChecked = Checked
    }


    fun onSelectItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {
        mTitleCode.value = (parent?.adapter?.getItem(pos) as TitleResponse.Title).code.toString()

    }

    fun onSelectNationalitiesItem(
        parent: AdapterView<*>?,
        view: View?,
        pos: Int,
        id: Long
    ) {
        mNationalityCode.value = (parent?.adapter?.getItem(pos) as NationalitiesResponse.Nationalities).code.toString()

    }

    fun onClickBack() {
        onBackImageObserver.postValue(1)
    }

    class Factory(
        val signUpRepository: SignUpRepository,
        val mLoginRepository: LoginRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SignUpViewModel(signUpRepository, mLoginRepository) as T
        }
    }


}
