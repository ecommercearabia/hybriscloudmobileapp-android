package com.erabia.foodcrowd.android.common.model

data class TruncateQuery(
    val query: Query? = null,
    val url: String? = null
)