package com.erabia.foodcrowd.android.forgetpassword.viewmodel

import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.forgetpassword.model.ResetPasswordResponse
import com.erabia.foodcrowd.android.forgetpassword.remote.repositry.ForgetPasswordRepository
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository


class ForgetPasswordViewModel(val mForgetPasswordRepository: ForgetPasswordRepository) :
    ViewModel() {
    val loadingVisibility: MutableLiveData<Int> = mForgetPasswordRepository.loadingVisibility
    var mGeneratTokenLiveData: LiveData<String> = mForgetPasswordRepository.mGetTokenLiveData
    val mErrorEvent: SingleLiveEvent<String> = mForgetPasswordRepository.error()

    var mErrorValidationMessage = MutableLiveData<String>()
    var mEmail: String = ""

    fun generatToken() {

        if (!Patterns.EMAIL_ADDRESS.matcher(mEmail).matches()) {
            mErrorValidationMessage.value = "Enter valid email"
        } else {
            mForgetPasswordRepository.getToken(mEmail)
        }
    }


    class Factory(
        val mForgetPasswordRepository: ForgetPasswordRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ForgetPasswordViewModel(mForgetPasswordRepository) as T
        }
    }

}
