package com.erabia.foodcrowd.android.home.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.model.AddToCartRequest
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.home.remote.repository.HomeRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

open class ProductItemViewModel(
    val homeRepository: HomeRepository,
    val cartRepository: CartRepository,
    val wishListRepository: WishListRepository
) : ViewModel() {
    companion object {
        const val TAG = "ProductItemViewModel"
    }

    private var cartId = ""
    private var userId = ""
    private val compositeDisposable = CompositeDisposable()
    val startLoginEvent: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
    val navigateToProductDetails: SingleLiveEvent<Product> by lazy { SingleLiveEvent<Product>() }
    val navigatePosition: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }
    var badgeNumber: SingleLiveEvent<Int> = cartRepository.badgeNumber


    fun onAddToCartClick(product: Product, quantity: Int = 1) {
        val addToCartRequest = AddToCartRequest(product = product, quantity = quantity)
        if (SharedPreference.getInstance().getUserToken() == null) {
            if (SharedPreference.getInstance().getGUID() == null) {

                compositeDisposable.add(cartRepository.createCartSuccess.subscribe({
                    SharedPreference.getInstance().setGUID(it?.guid ?: "")
                    cartRepository.addToCart(
                        Constants.ANONYMOUS,
                        SharedPreference.getInstance().getGUID() ?: "",
                        addToCartRequest,
                        Constants.FIELDS_FULL
                    )
                }, {
                    Log.d("ProductViewModel", it.message ?: "")
                }))
                createCart("anonymous", null, null, Constants.FIELDS_FULL)
            } else {
                cartRepository.addToCart(
                    Constants.ANONYMOUS,
                    SharedPreference.getInstance().getGUID() ?: "",
                    addToCartRequest,
                    Constants.FIELDS_FULL
                )
            }

        } else {
            cartRepository.addToCart(
                Constants.CURRENT,
                Constants.CURRENT,
                addToCartRequest,
                Constants.FIELDS_FULL
            )
        }
    }


    fun getCart(userId: String, cartId: String, fields: String) {
        if (SharedPreference.getInstance().getUserToken()
                ?.isNotEmpty() == true || SharedPreference.getInstance().getGUID()
                ?.isNotEmpty() == true
        ) {
            loadCart("", "", Constants.FIELDS_FULL)
        } else {
            createCart(SharedPreference.getInstance().getUserToken()?.let { "current" }
                ?: run { "anonymous" },
                "", "", Constants.FIELDS_FULL
            )
        }
    }

    fun createCart(
        userId: String,
        oldCartId: String?,
        toMergeCartGuid: String?,
        fields: String
    ): BehaviorSubject<Cart> {
        compositeDisposable.add(cartRepository.createCartSuccess.subscribe({
            SharedPreference.getInstance().setGUID(it?.guid ?: "")
            loadCart("", "", Constants.FIELDS_FULL)
        }, {
            Log.d(TAG, it.message ?: "")
        }))
        return cartRepository.createCart(userId, oldCartId, toMergeCartGuid, fields)
    }

    fun loadCart(userId: String, cartId: String, fields: String): LiveData<Cart> {
        this.cartId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
            Constants.CURRENT
        } else {
            SharedPreference.getInstance().getGUID() ?: ""
        }
        this.userId = if (SharedPreference.getInstance().getUserToken()?.isNotEmpty() == true) {
            Constants.CURRENT
        } else {
            Constants.ANONYMOUS
        }
        return cartRepository.loadCart(
            SharedPreference.getInstance().getUserToken()?.let { "current" } ?: run { "anonymous" },
            SharedPreference.getInstance().getUserToken()?.let { "current" }
                ?: run { SharedPreference.getInstance().getGUID() ?: "" },
            ""
        )
    }

    fun onWishlistClick(productId: String) {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""
        if (userLogin.isNotEmpty()) {
            wishListRepository.getAndAddWishList(productId)
        } else {
            startLoginEvent.postValue(true)
        }
    }

    fun onWishlistClickDelete(productId: String) {
        wishListRepository.getWishListToDelete(productId)
    }

    fun onProductClick(product: Product,position :Int) {
        navigateToProductDetails.postValue(product)
        navigatePosition.postValue(position)
    }
}