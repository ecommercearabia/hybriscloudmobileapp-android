package com.erabia.foodcrowd.android.managecards.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.databinding.CardRowItemBinding
import com.erabia.foodcrowd.android.managecards.model.CustomerPaymentOption
import com.erabia.foodcrowd.android.managecards.model.ManageCardData
import com.erabia.foodcrowd.android.managecards.viewmodel.ManageCardsViewModel

class ManageCardsAdapter(
    val manageCardsViewModel: ManageCardsViewModel
) : RecyclerView.Adapter<ManageCardsAdapter.ViewHolder>() {

    private val TAG: String = "ManageCardsAdapter"
    private var context: Context? = null

    var manageCardDataList: ManageCardData? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var cardRowItemBinding: CardRowItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        cardRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.card_row_item, parent, false)
        return ViewHolder(cardRowItemBinding)
    }

    override fun getItemCount(): Int {
        return manageCardDataList?.customerPaymentOptions?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val manageCardData = manageCardDataList?.customerPaymentOptions?.get(position)
        holder.bind(manageCardData)
    }

    inner class ViewHolder(val itemBinding: CardRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        @SuppressLint("CheckResult")

        fun bind(customerPaymentOption: CustomerPaymentOption?) {
            itemBinding.viewModel = manageCardsViewModel
            itemBinding.customerModel = customerPaymentOption

            itemBinding.buttonDelete.setSafeOnClickListener {
                customerPaymentOption?.customerCardId?.let { it1 ->
                    manageCardsViewModel.deleteCard(
                        it1
                    )
                }
            }
        }
    }
}