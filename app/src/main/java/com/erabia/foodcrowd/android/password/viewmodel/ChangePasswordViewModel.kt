package com.erabia.foodcrowd.android.password.viewmodel

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository


class ChangePasswordViewModel(val mChangePasswordRepository: ChangePasswordRepository) :
    ViewModel() {
    val loadingVisibility: MutableLiveData<Int> = mChangePasswordRepository.loadingVisibility
    var mChangePasswordLiveData: LiveData<String> = mChangePasswordRepository.mChangePasswordLiveData
    val mErrorEvent: SingleLiveEvent<String> = mChangePasswordRepository.error()
    var mErrorOldPasswordValidationMessage = MutableLiveData<String>()
    var mErrorNewPasswordValidationMessage = MutableLiveData<String>()
    var mErrorConfirmPasswordValidationMessage = MutableLiveData<String>()
    var mOldPassword: String = ""
    var mNewPassword: String = ""
    var mConfirmPassword: String = ""


    fun changePassword() {
        if (mOldPassword.isEmpty()) {
            mErrorOldPasswordValidationMessage.value = "please enter old password"
        }
        if (mNewPassword.isEmpty()) {
            mErrorNewPasswordValidationMessage.value = "please enter new password"
        }
        if (!mNewPassword.equals(mConfirmPassword)) {
            mErrorConfirmPasswordValidationMessage.value= "confirm password doesn't match your password"
        }
        if (mOldPassword.isNotEmpty()&&mNewPassword.isNotEmpty()&&mConfirmPassword.isNotEmpty()&&
            mNewPassword.equals(mConfirmPassword)){
            mChangePasswordRepository.changePassword( mNewPassword, mOldPassword)
        }

    }


    class Factory(
        val mChangePasswordRepository: ChangePasswordRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ChangePasswordViewModel(
                mChangePasswordRepository
            ) as T
        }
    }

}
