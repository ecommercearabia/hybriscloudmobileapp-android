package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

class Saving {
    @SerializedName("currencyIso")
    val currencyIso: String? = null

    @SerializedName("formattedValue")
    val formattedValue: String? = null

    @SerializedName("priceType")
    val priceType: String? = null

    @SerializedName("symbolLoc")
    val symbolLoc: String? = null

    @SerializedName("value")
    val value: Double? = null
}