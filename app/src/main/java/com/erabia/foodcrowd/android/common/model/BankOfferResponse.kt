package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class BankOfferResponse(
    @SerializedName("bankOffers")
    var bankOffers: List<BankOffer?>? = listOf()
) {
    data class BankOffer(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("icon")
        var icon: Icon? = Icon(),
        @SerializedName("message")
        var message: String? = ""
    ) {
        data class Icon(
            @SerializedName("format")
            var format: String? = "",
            @SerializedName("url")
            var url: String? = ""
        )
    }
}