package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class AppliedProductPromotion(
    @SerializedName("consumedEntries")
    val consumedEntries: List<ConsumedEntry> = listOf(),
    @SerializedName("description")
    val description: String = "",
    @SerializedName("promotion")
    val promotion: Promotion = Promotion()
)