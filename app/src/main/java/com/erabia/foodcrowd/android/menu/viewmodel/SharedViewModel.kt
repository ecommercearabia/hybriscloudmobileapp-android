package com.erabia.foodcrowd.android.menu.viewmodel

import android.app.Activity
import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.menu.model.MenuModel
import com.erabia.foodcrowd.android.menu.model.PreparingMenuModel
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository

class SharedViewModel(
    val mChangePasswordRepository: ChangePasswordRepository,
    val mChangeEmailRepository: ChangeEmailRepository
) : ViewModel() {

    val loadingVisibility: MutableLiveData<Int> = mChangePasswordRepository.loadingVisibility
    val loadingVisibilityEmail: MutableLiveData<Int> = mChangeEmailRepository.loadingVisibility


    var menu: MutableList<MenuModel> = ArrayList()
    var updateSignatureLiveData = mChangeEmailRepository.updateSignatureLiveData

    var menuImage: MutableList<PreparingMenuModel> = ArrayList()
    val selectedItem: SingleLiveEvent<PreparingMenuModel> by lazy { SingleLiveEvent<PreparingMenuModel>() }
    val activitySelectedItem: SingleLiveEvent<PreparingMenuModel> by lazy { SingleLiveEvent<PreparingMenuModel>() }
    val customSelectedItem: SingleLiveEvent<PreparingMenuModel> by lazy { SingleLiveEvent<PreparingMenuModel>() }
    val loginActivity: SingleLiveEvent<Activity> by lazy { SingleLiveEvent<Activity>() }
    val socialMedia: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val termsAndConditionLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val privacyPolicyLiveData: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val socialMediaWhatsApp: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    val file: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
    var logoutItem: MutableLiveData<Boolean> = MutableLiveData()

    var mLoginlObserver: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mPersonalObserver: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mSignUpObserver: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mOrderObserver: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var mLoginToPersonalTexView: String = ""
    var mIsChangeImageView: Boolean = false
    var mSignUpToOrderTexView: String = ""

    var mChangePasswordLiveData: LiveData<String> =
        mChangePasswordRepository.mChangePasswordLiveData
    val mErrorChangePasswordEvent: SingleLiveEvent<String> = mChangePasswordRepository.error()
    var mErrorOldPasswordValidationMessage = MutableLiveData<String>()
    var mErrorNewPasswordValidationMessage = MutableLiveData<String>()
    var mErrorConfirmPasswordValidationMessage = MutableLiveData<String>()
    var mOldPassword: String = ""
    var mNewPassword: String = ""
    var mConfirmPassword: String = ""

    var mChangeEmailLiveData: LiveData<String> = mChangeEmailRepository.mChangeEmailLiveData
    var mLoginLiveData: LiveData<TokenModel> = mChangeEmailRepository.mLoginLiveData

    val mErrorEvent: SingleLiveEvent<String> = mChangeEmailRepository.error()
    var mErrorNewEmailValidationMessage = MutableLiveData<String>()
    var mErrorConfrimEmailValidationMessage = MutableLiveData<String>()
    var mErrorPasswordEmailValidationMessage = MutableLiveData<String>()

    var mErrorValidationMessage = MutableLiveData<String>()
    var mNewEmail: String = ""
    var mConfirmEmail: String = ""
    var mPassword: String = ""

    val dataToShare = MutableLiveData<String>()


    init {
        getMenuText()
    }

    fun updateData(data: String) {
        dataToShare.value = data
    }

    fun getMenuList(): List<MenuModel> {
        menu.clear()
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""

        if (userLogin.isNotEmpty()) {
            menu.addAll(
                PreparingMenuModel(
                    MyApplication.getContext()
                ).prepareLoginMenuData()
            )
        } else {
            menu.addAll(
                PreparingMenuModel(
                    MyApplication.getContext()
                ).prepareLogoutMenuData()
            )
        }
        return menu
    }

    fun updateSignature(deviceId: String) {
        mChangeEmailRepository.updateSignature(deviceId)
    }


    fun getMenuText() {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""

        if (userLogin.isNotEmpty()) {
            mLoginToPersonalTexView =
                MyApplication.getContext()?.getString(R.string.my_wishlist) ?: ""
            mSignUpToOrderTexView = MyApplication.getContext()?.getString(R.string.my_orders) ?: ""
            mIsChangeImageView = true
        } else {
            mLoginToPersonalTexView = MyApplication.getContext()?.getString(R.string.login) ?: ""
            mSignUpToOrderTexView = MyApplication.getContext()?.getString(R.string.sign_up) ?: ""
            mIsChangeImageView = false
        }
    }

    fun onClicksLogin() {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""

        if (userLogin.isNotEmpty()) {
            mPersonalObserver.value = true

        } else {
            mLoginlObserver.value = true

        }
    }


    fun onClicksSignUp() {
        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""

        if (userLogin.isNotEmpty()) {
            mOrderObserver.value = true
        } else {
            mSignUpObserver.value = true

        }
    }

    fun onFacebookClick() {

        socialMedia.postValue(Constants.FACEBOOK)

    }

    fun onInstaClick() {

        socialMedia.postValue(Constants.INSTAGRAM)

    }

    fun onGmailClick() {
        socialMedia.postValue(Constants.GMAIL)
    }

    fun onTwitterClick() {
        socialMedia.postValue(Constants.TWITTER)
    }

    fun onYoutubeClick() {
        socialMedia.postValue(Constants.YOUTUBE)
    }

    fun onWhatsAppClick() {
        socialMediaWhatsApp.postValue(Constants.WHATSAPP)
    }

    fun onLinksClick(text: String) {
        when (text) {
            "terms" -> termsAndConditionLiveData.postValue("https://prod.foodcrowd.com/foodcrowd-ae/en/termsAndConditions")
            "privacy" -> privacyPolicyLiveData.postValue("https://prod.foodcrowd.com/foodcrowd-ae/en/privacy-policy")
            "team" -> socialMedia.postValue("https://prod.foodcrowd.com/foodcrowd-ae/en/about-us")
        }
    }


    fun changePassword() {
        if (mOldPassword.isEmpty()) {
            mErrorOldPasswordValidationMessage.value = "please enter old password"
        }
        if (mNewPassword.isEmpty()) {
            mErrorNewPasswordValidationMessage.value = "please enter new password"
        }
        if (!mNewPassword.equals(mConfirmPassword)) {
            mErrorConfirmPasswordValidationMessage.value =
                "confirm password doesn't match your password"
        }
        if (mOldPassword.isNotEmpty() && mNewPassword.isNotEmpty() && mConfirmPassword.isNotEmpty() &&
            mNewPassword.equals(mConfirmPassword)
        ) {
            mChangePasswordRepository.changePassword(mNewPassword, mOldPassword)
        }

    }


    fun changeEmail() {
        if (!Patterns.EMAIL_ADDRESS.matcher(mNewEmail).matches()) {
            mErrorNewEmailValidationMessage.value = "please Enter valid email"
        }
        if (!mConfirmEmail.equals(mNewEmail)) {
            mErrorConfrimEmailValidationMessage.value = "confirm email doesn't match new email"
        }
        if (mPassword.isEmpty()) {
            mErrorPasswordEmailValidationMessage.value = "please enter password"
        }

        if (Patterns.EMAIL_ADDRESS.matcher(mNewEmail).matches() && mConfirmEmail.equals(mNewEmail)
            && mPassword.isNotEmpty()
        ) {
            mChangeEmailRepository.changeEmail(mNewEmail, mPassword)

        }
    }




    class Factory(
        val mChangePasswordRepository: ChangePasswordRepository,
        val mChangeEmailRepository: ChangeEmailRepository

    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SharedViewModel(
                mChangePasswordRepository, mChangeEmailRepository
            ) as T
        }
    }
}