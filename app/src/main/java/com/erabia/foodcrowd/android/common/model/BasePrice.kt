package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class BasePrice(
    @SerializedName("currencyIso")
    val currencyIso: String? = null,
    @SerializedName("formattedValue")
    val formattedValue: String? = null,
    @SerializedName("maxQuantity")
    val maxQuantity: Double? = null,
    @SerializedName("minQuantity")
    val minQuantity: Double? = null,
    @SerializedName("priceType")
    val priceType: String? = null,
    @SerializedName("value")
    val value: Double? = null
)