package com.erabia.foodcrowd.android.menu.model

import android.app.Activity
import android.content.Context
import androidx.fragment.app.Fragment
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants

class PreparingMenuModel {


    private var image: Int = 0
    private var text: String = ""
    private var context: Context? = null
    private var activity: Activity? = null
    private var fragmnet: Fragment? = null
    var navAction: Int? = null
    var custom: String? = null
    var isConsent = false


    val accountList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val storeList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val settingList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val logoutList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val offerList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val unLoginList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val menuImageList: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()
    val loginList: MutableList<MenuModel> = ArrayList<MenuModel>()

    constructor(context: Context?) {
        this.context = context

    }

    fun getText(): String {
        return text
    }

    fun setText(text: String) {
        this.text = text
    }

    fun getImage(): Int {
        return image
    }

    fun setImage(image: Int) {
        this.image = image
    }

    fun getActivity(): Activity? {
        return activity
    }

    fun setActivity(activity: Activity) {
        this.activity = activity
    }

    fun getFragment(): Fragment? {
        return fragmnet
    }

    fun setFragment(fragment: Fragment) {
        this.fragmnet = fragment
    }

    fun prepareLoginMenuData(): MutableList<MenuModel> {
        var preparingMenuModel: PreparingMenuModel
        var menuModel: MenuModel

        // login menu


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.email_address) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_email)
//        preparingMenuModel.setFragment(ChangeEmailActivity())
//        preparingMenuModel.setActivity(MainActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_changeEmailFragment
        accountList.add(preparingMenuModel)

        preparingMenuModel = PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.change_password) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_password)
//        preparingMenuModel.setActivity(MainActivity())
//        preparingMenuModel.setFragment(ChangePasswordActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_changePasswordFragment
        accountList.add(preparingMenuModel)


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.devlivery_address) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_delivery)

//        preparingMenuModel.setActivity(ChangePasswordActivity())
//        preparingMenuModel.setFragment(ChangePasswordActivity())
        preparingMenuModel.navAction = R.id.action_navigate_to_address
        accountList.add(preparingMenuModel)



        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.store_credit) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_store_credit)
//        preparingMenuModel.setActivity(MainActivity())

//        preparingMenuModel.setActivity(AddressActivity())
//        preparingMenuModel.setFragment(AddressActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_storeCreditFragment
        accountList.add(preparingMenuModel)


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.personal_details) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_person_profile)
//        preparingMenuModel.setActivity(MainActivity())

//        preparingMenuModel.setActivity(AddressActivity())
//        preparingMenuModel.setFragment(AddressActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_personalDetailsFragment
        accountList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.referral_code) ?: "")
        preparingMenuModel.setImage(R.drawable.gift)
//        preparingMenuModel.setActivity(MainActivity())
//        preparingMenuModel.setActivity(AddressActivity())
//        preparingMenuModel.setFragment(AddressActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_referralCodeFragment
        accountList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.manage_saved_cards) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_cash_card)
        preparingMenuModel.navAction = R.id.action_menuScreen_to_manageCardsFragment
        accountList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.loyalty_point) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_cash_card)
        preparingMenuModel.navAction = R.id.action_menuScreen_to_loyaltyPointFragment
        accountList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.consent_management).toString())
        preparingMenuModel.setImage(R.drawable.ic_consent)
//        preparingMenuModel.navAction = -1
        preparingMenuModel.isConsent = true
        accountList.add(preparingMenuModel)


        menuModel = MenuModel(
            context?.getString(R.string.my_account) ?: "", accountList
        )
        loginList.add(menuModel)


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.about_us) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_about)
//        preparingMenuModel.setActivity(MainActivity())

//        preparingMenuModel.setFragment(AboutUsActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_aboutUsFragment

        storeList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText("800 FOODC (36632)")

        preparingMenuModel.setImage(R.drawable.ic_call)
//        preparingMenuModel.setActivity(MainActivity())

          preparingMenuModel.custom = Constants.NUMBER_PHONE
        storeList.add(preparingMenuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.contact_us) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_email)
//        preparingMenuModel.setActivity(MainActivity())

//        preparingMenuModel.setFragment(AboutUsActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_contactUsFragment
        storeList.add(preparingMenuModel)




        menuModel = MenuModel(
            context?.getString(R.string.our_store) ?: "", storeList
        )
        loginList.add(menuModel)


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.setting) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_setting)
//        preparingMenuModel.setActivity(MainActivity())

//        preparingMenuModel.setFragment(SettingsFragment())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_settingsFragment
        settingList.add(preparingMenuModel)

        menuModel = MenuModel(
            context?.getString(R.string.app_setting) ?: "", settingList
        )
        loginList.add(menuModel)

        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.logout) ?: "")
        preparingMenuModel.setImage(0)
//        preparingMenuModel.setActivity(MainActivity())
        logoutList.add(preparingMenuModel)

        menuModel =
            MenuModel("", logoutList)
        loginList.add(menuModel)


        return loginList

    }

    fun prepareLogoutMenuData(): MutableList<MenuModel> {
        var preparingMenuModel: PreparingMenuModel
        var menuModel: MenuModel

        // logout menu


        preparingMenuModel = PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.about_us) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_about)
//        preparingMenuModel.setFragment(AboutUsActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_aboutUsFragment
        storeList.add(preparingMenuModel)


        preparingMenuModel = PreparingMenuModel(context)
        preparingMenuModel.setText("800 FOODC (36632)")

        preparingMenuModel.setImage(R.drawable.ic_call)
        preparingMenuModel.custom = Constants.NUMBER_PHONE
        storeList.add(preparingMenuModel)


        preparingMenuModel =
            PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.contact_us) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_email)
//        preparingMenuModel.setFragment(AboutUsActivity())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_contactUsFragment
        storeList.add(preparingMenuModel)


        menuModel = MenuModel(context?.getString(R.string.our_store) ?: "", storeList)
        loginList.add(menuModel)


        preparingMenuModel = PreparingMenuModel(context)
        preparingMenuModel.setText(context?.getString(R.string.app_setting) ?: "")
        preparingMenuModel.setImage(R.drawable.ic_setting)
//        preparingMenuModel.setFragment(SettingsFragment())
        preparingMenuModel.navAction = R.id.action_menuScreen_to_settingsFragment
        settingList.add(preparingMenuModel)

        menuModel = MenuModel(context?.getString(R.string.app_setting) ?: "", settingList)
        loginList.add(menuModel)



        return loginList

    }

}