package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PotentialPromotion(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("promotionType")
    val promotionType: String = "",
    @SerializedName("description")
    val description: String = ""
)