package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ConsumedEntry(
    @SerializedName("adjustedUnitPrice")
    val adjustedUnitPrice: Double = 0.0,
    @SerializedName("code")
    val code: String = "",
    @SerializedName("orderEntryNumber")
    val orderEntryNumber: Int = 0,
    @SerializedName("quantity")
    val quantity: Int = 0
)