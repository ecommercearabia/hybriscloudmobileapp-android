package com.erabia.foodcrowd.android.wishlistmenu.model

import retrofit2.Response

sealed class ZipWishListModelResponse {

    data class SuccessGetWishList(val data: Response<GetWishListResponse>) : ZipWishListModelResponse()
    data class SuccessAddWishList(val data: Response<Void>) : ZipWishListModelResponse()
    data class Error(val t: Throwable) : ZipWishListModelResponse()
}