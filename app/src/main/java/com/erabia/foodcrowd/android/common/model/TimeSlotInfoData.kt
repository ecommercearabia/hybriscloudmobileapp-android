package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.SerializedName

data class TimeSlotInfoData(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("day")
    val day: String = "",
    @SerializedName("end")
    val end: String = "",
    @SerializedName("periodCode")
    val periodCode: String = "",
    @SerializedName("start")
    val start: String = ""
)