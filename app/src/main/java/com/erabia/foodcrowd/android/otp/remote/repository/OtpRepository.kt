package com.erabia.foodcrowd.android.otp.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.otp.model.SendOtpResponse
import com.erabia.foodcrowd.android.otp.remote.service.OtpService
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.RegisterPostData
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import com.erabia.foodcrowd.android.signup.remote.service.SignUpService
import io.reactivex.rxkotlin.subscribeBy

class OtpRepository(val mOtpService: OtpService) :
    Repository {
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mSendOtpLiveData: SingleLiveEvent<SendOtpResponse> by lazy { SingleLiveEvent<SendOtpResponse>() }
    val mVerifyOtpLiveData: SingleLiveEvent<SendOtpResponse> by lazy { SingleLiveEvent<SendOtpResponse>() }
    val mUpdateProfileLiveData: SingleLiveEvent<Void> by lazy { SingleLiveEvent<Void>() }


    companion object {
        const val TAG: String = "SignUpRepository"
    }

    @SuppressLint("CheckResult")
    fun sendOtp(ISOCode:String,mMobileNumber:String) {

        mOtpService.sendOtp(
            ISOCode,
            mMobileNumber
        ).get().doOnSubscribe { loadingVisibility.value= View.VISIBLE }
            .doOnTerminate { loadingVisibility.value= View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.body()?.status) {
                        true -> {
                            mSendOtpLiveData.value = it.body()
                        }
                        false -> {
                            error().postValue(it.body()?.description)
                        }

                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )

    }

  @SuppressLint("CheckResult")
    fun verifyOtp(code:String,ISOCode:String,mMobileNumber:String) {

        mOtpService.verifyOtp(
            code,
            ISOCode,
            mMobileNumber
        ).get().doOnSubscribe { loadingVisibility.value= View.VISIBLE }
            .doOnTerminate { loadingVisibility.value= View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.body()?.status) {
                        true -> {
                            mVerifyOtpLiveData.value = it.body()
                        }
                        false -> {
                            error().postValue(it.body()?.description)
                        }

                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )

    }


    @SuppressLint("CheckResult")
    fun updateProfile(p : PersonalDetailsModel){
        var lang =  LocaleHelper.getLanguage(MyApplication.getContext()!!)
        mOtpService.updateProfile(
            "current", lang.toString() ,p
        ).get().doOnSubscribe { loadingVisibility.value= View.VISIBLE }
            .doOnTerminate { loadingVisibility.value= View.GONE }
            .subscribeBy(
                onNext =
                {
                    when(it.code()){
                        200, 201, 202->{
                            mUpdateProfileLiveData.value = it.body()

                        }

                        400, 401, 403 ->{
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it))
                        }


                    }
                }
                ,
                onError =
                {
                    Log.d(MyOrderRepository.TAG, it.toString())
                }
            )
    }


}
