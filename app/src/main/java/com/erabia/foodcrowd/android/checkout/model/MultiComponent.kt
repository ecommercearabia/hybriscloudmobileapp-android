package com.erabia.foodcrowd.android.checkout.model

/**
 * Created by Mohammad Al-Junaidi on 30,July,2021
 */
data class MultiComponent(
    val first: ZipModelCheckoutAllData.SuccessBankOffer,
    val second: ZipModelCheckoutAllData.SuccessPaymentModes,
    val third: ZipModelCheckoutAllData.SuccessStoreCreditAmount,
    val fourth: ZipModelCheckoutAllData.SuccessStoreCreditModes,
    val five: ZipModelCheckoutAllData.SuccessGetTimeSlot

    )

