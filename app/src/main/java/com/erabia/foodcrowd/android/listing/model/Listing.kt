package com.erabia.foodcrowd.android.listing.model

import com.erabia.foodcrowd.android.common.model.*
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Listing(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("breadcrumbs")
    val breadcrumbs: List<BreadCrumb>? = null,
    @SerializedName("currentQuery")
    val currentQuery: CurrentQuery? = null,
    @SerializedName("facets")
    val facets: List<Facet>? = null,
    @SerializedName("freeTextSearch")
    val freeTextSearch: String? = null,
    @SerializedName("pagination")
    val pagination: Pagination? = null,
    @SerializedName("products")
    val products: List<Product>? = listOf(),
    @SerializedName("sorts")
    val sorts: List<Sort>? = null
)