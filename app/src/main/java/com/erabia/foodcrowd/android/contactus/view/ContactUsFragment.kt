package com.erabia.foodcrowd.android.contactus.view

import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.contactus.remote.ContactUsRepository
import com.erabia.foodcrowd.android.contactus.remote.ContactUsService
import com.erabia.foodcrowd.android.contactus.viewmodel.ContactUsViewModel
import com.erabia.foodcrowd.android.databinding.FragmentContactUsBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.disposables.CompositeDisposable

class ContactUsFragment : Fragment() {
    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: ContactUsViewModel
    lateinit var binding: FragmentContactUsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val factory = ContactUsViewModel.Factory(
            ContactUsRepository(
                RequestManager.getClient().create(ContactUsService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(ContactUsViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = mViewModel
//
//        binding.mPhoneNumberCallTextView.clicks().filterRapidClicks().subscribe {
//            val intent = Intent(Intent.ACTION_DIAL)
//            intent.data = Uri.parse("tel:" + "80036632")
//            startActivity(intent)
//        }.addTo(compositeDisposable)
//

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.mContactUsResponseLiveData.observe(viewLifecycleOwner, Observer {

            var l1 = listOf("E-mail:")
            var l2 = listOf("800 36632")
            var mEmailStartIndex = it.content?.lastIndexOf("E-mail: ") ?: 0
            var mEmailEndIndex = it.content?.lastIndexOf("com") ?: 0

//            var mEmailColor=it.content!!.replaceRange(mEmailStartIndex+7,mEmailEndIndex+3, "<font color='#F08D39'>info@foodcrowd.com</font>")
//            var mEmailColor=it.content!!.replaceAfterLast("E-mail:", "<font color='#F08D39'>info@foodcrowd.com</font>")
//            var mEmailColor=it.content!!.replace("info@foodcrowd.com", "<font color='#F08D39'> info@foodcrowd.com</font>")
//
//            var mPhoneColor=mEmailColor.replace("800 36632", "<font color='#6FBB33'>800 36632</font>")
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                binding.mHtmlTextView.setText(
//                    Html.fromHtml(
//                        it.content,
//                        Html.FROM_HTML_MODE_COMPACT
//                    )
//                )
//            } else {
//                binding.mHtmlTextView.setText(Html.fromHtml(it.content))
//            }

            binding.webView.settings.javaScriptEnabled = true
            binding.webView.isHorizontalScrollBarEnabled = false
            binding.webView.loadDataWithBaseURL(
                null,
                it.content ?:"",
                "text/html; charset=utf-8",
                "UTF-8",
                null
            )
            binding.webView.setBackgroundColor(Color.TRANSPARENT);


        })
        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)

        })
        loadingVisibilityObserver()
    }
    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }
    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
        mViewModel.getContactUs()
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

}