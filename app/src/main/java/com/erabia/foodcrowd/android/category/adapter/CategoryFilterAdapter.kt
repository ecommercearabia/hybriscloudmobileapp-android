package com.erabia.foodcrowd.android.category.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.adapter.CategoryFilterAdapter.ViewHolder
import com.erabia.foodcrowd.android.common.model.BreadCrumb
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.databinding.FilterRowItemBinding

class CategoryFilterAdapter : RecyclerView.Adapter<ViewHolder>() {
    interface OnFilterChecked {
        fun onFilterChecked(facet: Facet?)
    }

    interface OnFilterUnChecked {
        fun onFilterUnChecked(breadCrumb: BreadCrumb?)
    }

    var breadCrumbsList: List<BreadCrumb>? = null
    private var filterList: ArrayList<Facet> = ArrayList()
    var onFilterChecked: OnFilterChecked? = null
    var onFilterUnChecked: OnFilterUnChecked? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: FilterRowItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.filter_row_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return filterList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position, filterList.get(position), breadCrumbsList)
    }

    fun clear() {
        filterList.clear()
        notifyDataSetChanged()
    }

    fun refresh(facetList: List<Facet>) {
        filterList.clear()
        filterList.addAll(facetList)
        notifyDataSetChanged()
    }


    inner class ViewHolder(val binding: FilterRowItemBinding) :
        RecyclerView.ViewHolder(binding.root),
        CompoundButton.OnCheckedChangeListener {
        private var mBreadCrumb: BreadCrumb? = null
        private var mFacet: Facet? = null

        fun bind(
            position: Int,
            facet: Facet,
            breadCrumbList: List<BreadCrumb>?
        ) {
            this.mFacet = facet
            val isItemChecked = breadCrumbList?.any {
                facet.name == it.facetName
            }
            mBreadCrumb = breadCrumbList?.firstOrNull {
                facet.name == it.facetName
            }
            binding.switchOrganic.isChecked = isItemChecked ?: false
            binding.buttonOrganic.text = facet.name
            binding.switchOrganic.setOnCheckedChangeListener(this)

        }

        override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
            if (isChecked) {
                onFilterChecked?.onFilterChecked(mFacet)
            } else {
                onFilterUnChecked?.onFilterUnChecked(mBreadCrumb)
            }
        }
    }

}