package com.erabia.foodcrowd.android.order.di

import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.network.RequestManager
import dagger.Module
import dagger.Provides

@Module
class MyOrderModule {

    @Provides
    fun myOrderRepository() =
        MyOrderRepository(RequestManager.getClient().create(MyOrderService::class.java))

//    @Provides
//    fun myOrderViewModel() = MyOrderViewModel()

}