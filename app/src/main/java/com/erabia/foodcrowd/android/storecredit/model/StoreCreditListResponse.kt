package com.erabia.foodcrowd.android.storecredit.model


import com.google.gson.annotations.SerializedName
data class StoreCreditListResponse(
    @SerializedName("storeCreditHistroy")
    var storeCreditHistroy: List<StoreCreditHistroy?> = listOf()
) {
    data class StoreCreditHistroy(
        @SerializedName("amount")
        var amount: Amount? = Amount(),
        @SerializedName("balance")
        var balance: Balance? = Balance(),
        @SerializedName("dateOfPurchase")
        var dateOfPurchase: String? = "",
        @SerializedName("orderCode")
        var orderCode: String? = ""
    ) {
        data class Amount(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double? = 0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double? = 0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("symbolLoc")
            var symbolLoc: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        )

        data class Balance(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double? = 0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double? = 0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("symbolLoc")
            var symbolLoc: String? = "",
            @SerializedName("value")
            var value: Double? = 0.0
        )
    }
}