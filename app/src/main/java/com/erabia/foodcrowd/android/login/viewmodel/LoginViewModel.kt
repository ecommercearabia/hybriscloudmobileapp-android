package com.erabia.foodcrowd.android.login.viewmodel

import android.util.Patterns
import androidx.lifecycle.*
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository

class LoginViewModel(val mLoginRepository: LoginRepository) : ViewModel() {
    val loadingVisibility: MutableLiveData<Int> = mLoginRepository.loadingVisibility

    var mLoginLiveData: LiveData<TokenModel> = mLoginRepository.mLoginLiveData
    var mNavigateToLoginOtpLiveData: SingleLiveEvent<Boolean> = mLoginRepository.mNavigateToOtpLiveData
    val mErrorEvent: SingleLiveEvent<String> = mLoginRepository.error()

    var mPassword: String = ""
    var mErrorEmailValidationMessage: MutableLiveData<String> = MutableLiveData()
    var mErrorPasswordValidationMessage: MutableLiveData<String> = MutableLiveData()
    var onBackImageObserver: MutableLiveData<Int> = MutableLiveData()

    var email: String = ""


    fun doLogin() {
//        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
//            mErrorEmailValidationMessage.value = "Enter valid email"
//        }
        if (mPassword.isEmpty()) {
            mErrorPasswordValidationMessage.value = "please enter password"
        }
        if (email.isEmpty()) {
            mErrorEmailValidationMessage.value = "please enter email or mobile number"
        }
        if (mPassword.isNotEmpty() && email.isNotEmpty()) {
            mLoginRepository.doLogin(email, mPassword)
        }
    }

       fun doFingerPrintLogin(email:String) {
            mLoginRepository.doFingerPrintLogin(SharedPreference.getInstance().getSIGNATURE_ID()?:"",email)
    }




    fun doLoginWithOtp() {
        mNavigateToLoginOtpLiveData.postValue(true)
    }

    fun onClickBack() {

        onBackImageObserver.postValue(1)
    }

    class Factory(
        val mLoginRepository: LoginRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginViewModel(mLoginRepository) as T
        }

    }


}