package com.erabia.foodcrowd.android.home.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.model.Product

@Dao
interface HomeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHome(home: Home)

    @Query("DELETE FROM Home")
    suspend fun deleteHome();

    @Query("SELECT * FROM home")
    suspend fun getHome(): Home

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(product: Product)

    @Query("SELECT * FROM Product WHERE code = :code")
    suspend fun getProduct(code: String): Product

    @Query("DELETE FROM Product WHERE code NOT IN (:productCodes)")
    suspend fun deleteNotExistProducts(productCodes: List<String>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBanners(bannerList: List<Component>)

    @Query("SELECT * FROM Component WHERE componentType = 0")
    suspend fun getBanners(): List<Component>

    @Query("DELETE FROM Component WHERE componentType = 0")
    suspend fun deleteBanners()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTabs(bannerList: List<Component>)

    @Query("SELECT * FROM Component WHERE componentType = 1")
    suspend fun getTabs(): List<Component>

    @Query("DELETE FROM Component WHERE componentType = 1")
    suspend fun deleteTabs()
}
