package com.erabia.foodcrowd.android.common.util

import androidx.room.TypeConverter
import com.erabia.foodcrowd.android.common.model.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class DataConverter {
    @TypeConverter
    fun fromContentSlotList(countryLang: List<ContentSlot?>?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<ContentSlot?>?>() {}.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toContentSlotList(contentSlotString: String?): List<ContentSlot>? {
        if (contentSlotString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<ContentSlot?>?>() {}.type
        return gson.fromJson<List<ContentSlot>>(contentSlotString, type)
    }


    @TypeConverter
    fun fromComponentList(component: List<Component?>?): String? {
        if (component == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Component?>?>() {}.type
        return gson.toJson(component, type)
    }

    @TypeConverter
    fun toComponentList(componentString: String?): List<Component>? {
        if (componentString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Component?>?>() {}.type
        return gson.fromJson<List<Component>>(componentString, type)
    }

    @TypeConverter
    fun fromChildrenList(children: List<Children?>?): String? {
        if (children == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Children?>?>() {}.type
        return gson.toJson(children, type)
    }

    @TypeConverter
    fun toChildrenList(childrenString: String?): List<Children>? {
        if (childrenString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Children?>?>() {}.type
        return gson.fromJson<List<Children>>(childrenString, type)
    }

    @TypeConverter
    fun fromEntryList(entry: List<Entry?>?): String? {
        if (entry == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Entry?>?>() {}.type
        return gson.toJson(entry, type)
    }

    @TypeConverter
    fun toEntryList(entryString: String?): List<Entry>? {
        if (entryString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Entry?>?>() {}.type
        return gson.fromJson<List<Entry>>(entryString, type)
    }

    @TypeConverter
    fun fromAnyList(any: List<Any?>?): String? {
        if (any == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Any?>?>() {}.type
        return gson.toJson(any, type)
    }

    @TypeConverter
    fun toAnyList(anyString: String?): List<Any>? {
        if (anyString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Any?>?>() {}.type
        return gson.fromJson<List<Any>>(anyString, type)
    }

    @TypeConverter
    fun fromContentSlot(countryLang: ContentSlots?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<ContentSlots>() {}.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toContentSlot(contentSlotString: String?): ContentSlots? {
        if (contentSlotString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<ContentSlots?>() {}.type
        return gson.fromJson<ContentSlots>(contentSlotString, type)
    }


    @TypeConverter
    fun fromComponents(components: Components?): String? {
        if (components == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Components?>() {}.type
        return gson.toJson(components, type)
    }

    @TypeConverter
    fun toComponents(componentsString: String?): Components? {
        if (componentsString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Components?>() {}.type
        return gson.fromJson<Components>(componentsString, type)
    }

    @TypeConverter
    fun fromDiscount(discount: Discount?): String? {
        if (discount == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Discount?>() {}.type
        return gson.toJson(discount, type)
    }

    @TypeConverter
    fun toDiscount(discountString: String?): Discount? {
        if (discountString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Discount?>() {}.type
        return gson.fromJson<Discount>(discountString, type)
    }

    @TypeConverter
    fun fromDiscountPrice(discountPrice: DiscountPrice?): String? {
        if (discountPrice == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<DiscountPrice?>() {}.type
        return gson.toJson(discountPrice, type)
    }

    @TypeConverter
    fun toDiscountPrice(discountPriceString: String?): DiscountPrice? {
        if (discountPriceString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<DiscountPrice?>() {}.type
        return gson.fromJson<DiscountPrice>(discountPriceString, type)
    }

    @TypeConverter
    fun fromSaving(saving: Saving?): String? {
        if (saving == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Saving?>() {}.type
        return gson.toJson(saving, type)
    }

    @TypeConverter
    fun toSaving(savingString: String?): Saving? {
        if (savingString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Saving?>() {}.type
        return gson.fromJson<Saving>(savingString, type)
    }

    @TypeConverter
    fun fromNavigationNode(components: NavigationNode?): String? {
        if (components == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<NavigationNode?>() {}.type
        return gson.toJson(components, type)
    }

    @TypeConverter
    fun toNavigationNode(componentsString: String?): NavigationNode? {
        if (componentsString == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<NavigationNode?>() {}.type
        return gson.fromJson<NavigationNode>(componentsString, type)
    }

    @TypeConverter
    fun fromContentVariantOptionQualifierList(list: List<VariantOptionQualifier?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<VariantOptionQualifier?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toContentVariantOptionQualifierList(str: String?): List<VariantOptionQualifier>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<VariantOptionQualifier?>?>() {}.type
        return gson.fromJson<List<VariantOptionQualifier>>(str, type)
    }

    @TypeConverter
    fun fromContentOptionList(list: List<Option?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Option?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toContentOptionList(str: String?): List<Option>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Option?>?>() {}.type
        return gson.fromJson<List<Option>>(str, type)
    }

    @TypeConverter
    fun fromContentBaseOptionList(list: List<BaseOption?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<BaseOption?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toContentBaseOptionList(str: String?): List<BaseOption>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<BaseOption?>?>() {}.type
        return gson.fromJson<List<BaseOption>>(str, type)
    }

    @TypeConverter
    fun fromCategoryList(list: List<Category?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Category?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toCategoryList(str: String?): List<Category>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Category?>?>() {}.type
        return gson.fromJson<List<Category>>(str, type)
    }


    @TypeConverter
    fun fromClassificationList(list: List<Classification?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Classification?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toClassificationList(str: String?): List<Classification>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Classification?>?>() {}.type
        return gson.fromJson<List<Classification>>(str, type)
    }

    @TypeConverter
    fun fromImageList(list: List<Image?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Image?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toImageList(str: String?): List<Image>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Image?>?>() {}.type
        return gson.fromJson<List<Image>>(str, type)
    }

    @TypeConverter
    fun fromPotentialPromotionList(list: List<PotentialPromotion?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<PotentialPromotion?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toPotentialPromotionList(str: String?): List<PotentialPromotion>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<PotentialPromotion?>?>() {}.type
        return gson.fromJson<List<PotentialPromotion>>(str, type)
    }

    @TypeConverter
    fun fromProductReferenceList(list: List<ProductReference?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<ProductReference?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toProductReferenceList(str: String?): List<ProductReference>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<ProductReference?>?>() {}.type
        return gson.fromJson<List<ProductReference>>(str, type)
    }

    @TypeConverter
    fun fromFeatureList(list: List<Feature?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Feature?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toFeatureList(str: String?): List<Feature>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<Feature?>?>() {}.type
        return gson.fromJson<List<Feature>>(str, type)
    }

    @TypeConverter
    fun fromFeatureValueList(list: List<FeatureValue?>?): String? {
        if (list == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<FeatureValue?>?>() {}.type
        return gson.toJson(list, type)
    }

    @TypeConverter
    fun toFeatureValueList(str: String?): List<FeatureValue>? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<List<FeatureValue?>?>() {}.type
        return gson.fromJson<List<FeatureValue>>(str, type)
    }

    @TypeConverter
    fun fromPrice(obj: Price?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Price?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toPrice(str: String?): Price? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Price?>() {}.type
        return gson.fromJson<Price>(str, type)
    }

    @TypeConverter
    fun fromAny(obj: Any?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Any?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toAny(str: String?): Any? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Any?>() {}.type
        return gson.fromJson<Any>(str, type)
    }

    @TypeConverter
    fun fromPriceRange(obj: PriceRange?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<PriceRange?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toPriceRange(str: String?): PriceRange? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<PriceRange?>() {}.type
        return gson.fromJson<PriceRange>(str, type)
    }

    @TypeConverter
    fun fromStock(obj: Stock?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Stock?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toStock(str: String?): Stock? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Stock?>() {}.type
        return gson.fromJson<Stock>(str, type)
    }

    @TypeConverter
    fun fromMaxPrice(obj: MaxPrice?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<MaxPrice?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toMaxPrice(str: String?): MaxPrice? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<MaxPrice?>() {}.type
        return gson.fromJson<MaxPrice>(str, type)
    }

    @TypeConverter
    fun fromMinPrice(obj: MinPrice?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<MinPrice?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toMinPrice(str: String?): MinPrice? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<MinPrice?>() {}.type
        return gson.fromJson<MinPrice>(str, type)
    }

    @TypeConverter
    fun fromMedia(obj: Media?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Media?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toMedia(str: String?): Media? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Media?>() {}.type
        return gson.fromJson<Media>(str, type)
    }

    @TypeConverter
    fun fromDesktop(obj: Desktop?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Desktop?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toDesktop(str: String?): Desktop? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Desktop?>() {}.type
        return gson.fromJson<Desktop>(str, type)
    }

    @TypeConverter
    fun fromTablet(obj: Tablet?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Tablet?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toTablet(str: String?): Tablet? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Tablet?>() {}.type
        return gson.fromJson<Tablet>(str, type)
    }

    @TypeConverter
    fun fromMobile(obj: Mobile?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Mobile?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toMobile(str: String?): Mobile? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Mobile?>() {}.type
        return gson.fromJson<Mobile>(str, type)
    }

    @TypeConverter
    fun fromWidescreen(obj: Widescreen?): String? {
        if (obj == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Widescreen?>() {}.type
        return gson.toJson(obj, type)
    }

    @TypeConverter
    fun toWidescreen(str: String?): Widescreen? {
        if (str == null) {
            return null
        }
        val gson = Gson()
        val type: Type = object : TypeToken<Widescreen?>() {}.type
        return gson.fromJson<Widescreen>(str, type)
    }
}