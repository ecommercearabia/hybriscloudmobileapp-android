package com.erabia.foodcrowd.android.signup.model


import com.google.gson.annotations.SerializedName

data class RegisterPostData(
    @SerializedName("firstName")
    var firstName: String? = "",
    @SerializedName("lastName")
    var lastName: String? = "",
    @SerializedName("password")
    var password: String? = "",
    @SerializedName("titleCode")
    var titleCode: String? = "",
    @SerializedName("mobileCountryCode")
    var mobileCountryCode: String? = "",
    @SerializedName("mobileNumber")
    var mobileNumber: String? = "",
    @SerializedName("referralCode")
    var referralCode: String? = "",
    @SerializedName("nationality")
    var nationality: String? = "",
    @SerializedName("uid")
    var uid: String? = ""
)