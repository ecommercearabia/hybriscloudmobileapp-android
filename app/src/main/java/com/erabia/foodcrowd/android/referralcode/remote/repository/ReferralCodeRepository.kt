package com.erabia.foodcrowd.android.referralcode.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeModel
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeResponse
import com.erabia.foodcrowd.android.referralcode.remote.service.ReferralCodeService
import com.erabia.foodcrowd.android.signup.model.RegisterResponse
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import io.reactivex.rxkotlin.subscribeBy

class ReferralCodeRepository(val referralCodeService: ReferralCodeService) :
    Repository {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val mReferralCodeLiveData: SingleLiveEvent<ReferralCodeResponse> by lazy { SingleLiveEvent<ReferralCodeResponse>() }
    val mReferralCode: SingleLiveEvent<ReferralCodeResponse> by lazy { SingleLiveEvent<ReferralCodeResponse>() }
    val shareViaEmail: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }


    @SuppressLint("CheckResult")
    fun generateReferralCode(

    ) {
        referralCodeService.generateReferralCode("current")
            .get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mReferralCodeLiveData.value = it.body()
                        }
                        201 -> {
                            mReferralCodeLiveData.value = it.body()
                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )

    }

    @SuppressLint("CheckResult")
    fun getReferralCode(

    ) {
        referralCodeService.getReferralCode("current", "FULL")
            .get().doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }

            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {
                            mReferralCode.value = it.body()

                        }
                        201 -> {
                            mReferralCode.value = it.body()
                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )

    }

    @SuppressLint("CheckResult")
    fun sendReferralSubscriptionEmails(
        emails: String
    ) {
        referralCodeService.sendReferralSubscriptionEmails("current", true, emails)
            .get().doOnSubscribe { }
            .doOnTerminate { shareViaEmail.postValue(true) }

            .subscribeBy(
                onNext =
                {
                    when (it.code()) {
                        200 -> {


                        }
                        201 -> {

                        }

                        401 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }
                },
                onError =
                {
                    Log.d(SignUpRepository.TAG, it.toString())
                }
            )


    }

    @SuppressLint("CheckResult")
    fun loadAppConfig() {
        referralCodeService.getAppConfig("FULL").get().subscribe(this, onSuccess_200 = {

                SharedPreference.getInstance().setReferralCode(it.body()?.registrationConfiguration?.referralCodeConfigurations?.senderRewardAmount.toString() ?: "")

        }, onError_400 = {
            Log.d(SignUpRepository.TAG, it.toString())
        })
    }

}