package com.erabia.foodcrowd.android.menu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.ItemDivider
import com.erabia.foodcrowd.android.menu.model.MenuModel
import com.erabia.foodcrowd.android.menu.viewmodel.ConsentViewModel
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import kotlinx.android.synthetic.main.menu_row.view.*


class MenuRecyclerViewAdapter(var sharedViewModel: SharedViewModel , var consentViewModel: ConsentViewModel):
    RecyclerView.Adapter<MenuHeaderViewHolder>() {
    var menuList: List<MenuModel>? = null

    private val TYPE_HEADER = 0
    private val TYPE_ITEM = 1
    private var mCurrentHeaderValue: String = ""


    override fun onBindViewHolder(holder: MenuHeaderViewHolder, position: Int) {
        if (menuList?.get(position)?.text != "") {
            holder.itemView.text_view_title.text = menuList?.get(position)?.text
            holder.itemView.text_view_title.visibility = View.VISIBLE

        }
        holder.itemView.recycler_view_sub_menu.addItemDecoration(
            ItemDivider(holder.itemView.context)
        )
        var adapter: SubMenuRecyclerViewAdapter =
            SubMenuRecyclerViewAdapter(
                sharedViewModel , consentViewModel
            )
        // adapter.setHasStableIds(true)
        holder.itemView.recycler_view_sub_menu.adapter = adapter
        adapter.setMenu(menuList?.get(position)?.list ?: mutableListOf())

    }


    fun setMenu(menuList: List<MenuModel>) {
        this.menuList = menuList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuHeaderViewHolder {
        return MenuHeaderViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.menu_row,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return menuList?.size ?: 0
    }
}

class MenuHeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

}
