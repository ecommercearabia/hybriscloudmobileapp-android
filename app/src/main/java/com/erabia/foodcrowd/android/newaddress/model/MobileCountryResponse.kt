package com.erabia.foodcrowd.android.newaddress.model


import com.google.gson.annotations.SerializedName

data class MobileCountryResponse(
    @SerializedName("countries")
    var countries: List<Country>? = listOf()
) {
    data class Country(
        @SerializedName("isdcode")
        var isdcode: String? = "",
        @SerializedName("isocode")
        var isocode: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) {

        override fun toString(): String {
            return isdcode ?: ""
        }

    }


}