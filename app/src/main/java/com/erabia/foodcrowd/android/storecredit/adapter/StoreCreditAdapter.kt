package com.erabia.foodcrowd.android.storecredit.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.StoreCreditRowItemBinding
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import java.text.DateFormat
import java.util.*


class StoreCreditAdapter(val mStoreCreditList: List<StoreCreditListResponse.StoreCreditHistroy?>?) :
    RecyclerView.Adapter<ViewHolder>() {
    private var context: Context? = null

    lateinit var mStoreCreditRowItemBinding: StoreCreditRowItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        mStoreCreditRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.store_credit_row_item, parent, false)
        return ViewHolder(
            mStoreCreditRowItemBinding
        )


    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        if (mStoreCreditList != null) {
            return mStoreCreditList.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mStoreCreditRowItemBinding.storeCreditHistroy = mStoreCreditList?.get(position)
//        2020-08-24T14:08:53+0000
      var date=  AppUtil.formatDate(
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "MMM dd, yyyy#h:mm a",
            mStoreCreditList?.get(position)?.dateOfPurchase
        )

        var dateList=date?.split("#")
        holder.mStoreCreditRowItemBinding.mDateValueTextView.text= dateList?.get(0) ?: ""
        holder.mStoreCreditRowItemBinding.mTimeValueTextView.text= dateList?.get(1) ?: ""


//            holder.mStoreCreditRowItemBinding.mTimeValueTextView.text= AppUtil.formatDate(
//            "HH:mm:ssZ",
//            "HH:mm a",
//            mStoreCreditList?.get(position)?.dateOfPurchase
//        )



//        holder.mStoreCreditRowItemBinding.mDateValueTextView.text =
//            AppUtil.formatDate(
//                "yyyy-MM-dd'T'HH:mm:ssZ",
//                "MMM dd, yyyy HH:mm a",
//                mStoreCreditList?.get(position)?.dateOfPurchase
//            )
//
//         holder.mStoreCreditRowItemBinding.mTimeValueTextView.text =
//            AppUtil.formatDate(
//                "yyyy-MM-dd'T'HH:mm:ssZ",
//                "MMM dd, yyyy HH:mm a",
//                mStoreCreditList?.get(position)?.dateOfPurchase
//            )
//

    }

}

class ViewHolder(val mStoreCreditRowItemBinding: StoreCreditRowItemBinding) :
    RecyclerView.ViewHolder(mStoreCreditRowItemBinding.root)

