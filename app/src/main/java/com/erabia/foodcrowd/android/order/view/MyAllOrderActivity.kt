package com.erabia.foodcrowd.android.order.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.order.remote.service.MyOrderService
import com.erabia.foodcrowd.android.databinding.FragmentMyOrderBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.order.adapter.MyOrderAdapter
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_main.toolbar

class MyAllOrderActivity : AppCompatActivity() {
    var compositeDisposable = CompositeDisposable()
    lateinit var mMyOrderViewModel: MyOrderViewModel
    lateinit var binding: FragmentMyOrderBinding
    private var mMyOrderAdapter: MyOrderAdapter? = null
    var myOrderList: List<String> = emptyList()
    var mOrderCode: String? = ""
    var mDatePlaced: String? = ""
    var ordersList: List<OrderHistoryListResponse.Order?> = listOf()
//    val daggerComponent: IMyOrderComponent by lazy {
//        DaggerIMyOrderComponent.builder().build()
//    }

    //    init {
//        daggerComponent.inject(this)
//    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_my_order)
        setSupportActionBar(toolbar)
        toolbar.title = ""

        val factory = MyOrderViewModel.Factory(
            MyOrderRepository(
                RequestManager.getClient().create(MyOrderService::class.java)
            )
        )
        mMyOrderViewModel = ViewModelProvider(this, factory).get(MyOrderViewModel::class.java)
        binding.lifecycleOwner = this
        binding.mMyOrderViewModel = mMyOrderViewModel
        mMyOrderViewModel.getOrderList()
        orderListObserver()

        onViewClickObserver()
        observeProgressBar()

        binding.mContinueShoppingOrderButton.clicks().subscribe {
            startActivity(Intent(this, MainActivity::class.java))
        }.addTo(compositeDisposable)


        toolbar.setNavigationOnClickListener {
            finish()
        }

    }

    fun onViewClickObserver() {

        mMyOrderViewModel.onViewOrderClickObserver.observe(this, Observer {
            mOrderCode = ordersList.get(it)?.code
            mDatePlaced = ordersList.get(it)?.placed

            var intent = Intent(this, OrderActivity::class.java)
            intent.putExtra("code", mOrderCode)
            intent.putExtra("date_placed", mDatePlaced)
            startActivity(intent)
        })
    }

    private fun observeProgressBar() {
        mMyOrderViewModel.progressBarVisibility.observe(this, Observer {
            if (it == View.VISIBLE) {
                binding.progressBar.visibility = it
                binding.progressBar.setFreezAndVisiable(this)
            } else {
                binding.progressBar.visibility = it
                binding.progressBar.hideFreezAndVisiableProgress(this)
            }
        })

    }

    fun orderListObserver() {
        mMyOrderViewModel.mOrderListLiveData.observe(this, Observer {
            ordersList = it.orders
            if (ordersList.isEmpty()) {
                binding.mListOrderGroup.visibility = View.GONE
                binding.mNoOrderGroup.visibility = View.VISIBLE
            } else {
                binding.mListOrderGroup.visibility = View.VISIBLE
                binding.mNoOrderGroup.visibility = View.GONE

                mMyOrderAdapter =
                    MyOrderAdapter(
                        ordersList,
                        mMyOrderViewModel
                    )
                mMyOrderAdapter?.setHasStableIds(true)
                binding?.mMyOrderRecycleView?.adapter = mMyOrderAdapter
            }
        })

        mMyOrderViewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)

        })

    }
}