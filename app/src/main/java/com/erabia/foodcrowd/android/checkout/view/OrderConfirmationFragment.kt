package com.erabia.foodcrowd.android.checkout.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.checkout.adapter.ItemsAdapter
import com.erabia.foodcrowd.android.checkout.adapter.ItemsConfirmationAdapter
import com.erabia.foodcrowd.android.checkout.model.PlaceOrderResponse
import com.erabia.foodcrowd.android.checkout.remote.repository.CheckoutRepository
import com.erabia.foodcrowd.android.checkout.remote.service.CheckoutService
import com.erabia.foodcrowd.android.checkout.viewmodel.CheckoutViewModel
import com.erabia.foodcrowd.android.checkout.viewmodel.OrderConfirmationViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Cart
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentOrderConfirmationBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_main.*

class OrderConfirmationFragment : Fragment() {


    lateinit var orderConfirmationViewModel: OrderConfirmationViewModel
    lateinit var binding: FragmentOrderConfirmationBinding
    var orderData: PlaceOrderResponse? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_order_confirmation,
            container,
            false
        )
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)
        val checkoutService = RequestManager.getClient().create(CheckoutService::class.java)
        val cartService = RequestManager.getClient().create(CartService::class.java)
        val factory = CheckoutViewModel.Factory(
            CheckoutRepository(checkoutService, cartService),
            CartRepository(cartService)
        )
        orderConfirmationViewModel =
            ViewModelProvider(this).get(OrderConfirmationViewModel::class.java)
        orderData = arguments?.let { OrderConfirmationFragmentArgs.fromBundle(it).orderData }

        orderConfirmationViewModel.seeAllClickEvent.observe(viewLifecycleOwner, Observer {

            val action = orderData?.let { it1 ->
                OrderConfirmationFragmentDirections.actionOrderConfirmationToAllOrderCheckoutFragment(
                    null, it1
                )
            }
            action?.let { it1 -> findNavController().navigate(it1) }

        })
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            // handle back event
        }


        binding.orderConfirmationVM = orderConfirmationViewModel
        SharedPreference.getInstance().setGUID(null)
        observeOrderData()
        observeContinueShopping()
        return binding.root

    }

    private fun observeContinueShopping() {
        orderConfirmationViewModel.continueShoppingEvent.observe(viewLifecycleOwner, Observer {
            findNavController().popBackStack()
            (activity as MainActivity).bottomNavigationView.selectedItemId = it
        })


    }

    private fun observeOrderData() {
        binding.placeOrder = orderData
        binding.textViewOrderConfirmationMessage.text =
            (getString(R.string.order_confirmation_number) + " " + orderData?.code + " " + getString(
                R.string.order_confirmation_details
            )
                    + " " + SharedPreference.getInstance().getLoginEmail())
        val itemsConfirmationAdapter = ItemsConfirmationAdapter(2)
        binding.recyclerViewItems.adapter = itemsConfirmationAdapter
        itemsConfirmationAdapter.entryList = orderData?.entries
        binding.textViewOrderDate.text =
            AppUtil.formatDate(
                "yyyy-MM-dd'T'HH:mm:ssZ",
                "MMM dd, yyyy HH:mm a",
                orderData?.created
            )
        //  binding.textViewOrderDate.text =
        //    orderData?.created?.toLong()?.let { AppUtil.formatTimeStamp(it, "MMM dd, yyyy HH:mm a") }

        var code = SharedPreference.getInstance().getShipmentType() ?: ""
        if (code == "1") {
            binding.cardViewDeliveryAddress.visibility = View.GONE
            binding.textViewDeliveryAddressLabel.visibility = View.GONE
        }

        val mFirebaseAnalytics = FirebaseAnalytics.getInstance(requireContext())

        var arrayOfProducts = arrayOf<Bundle?>()

        if (orderData?.entries?.size != null) {
            for (i in 0 until orderData?.entries?.size!!) {

                val product = orderData?.entries?.get(i)?.product
                val productBundle = Bundle().apply {
                    putString(FirebaseAnalytics.Param.ITEM_ID, product?.code ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_NAME, product?.name ?: "")
                    putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                    putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                    putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                    product?.baseOptions?.get(0)?.selected?.priceData?.value?.let { it1 ->
                        putDouble(
                            FirebaseAnalytics.Param.PRICE,
                            it1 ?: 0.0
                        )
                    }

                }
                arrayOfProducts = append(arrayOfProducts , productBundle)
            }

        }

        var coupons = ""
        for (i in 0 until (orderData?.appliedVouchers?.size ?: 0)) {
            coupons+=  orderData?.appliedVouchers?.get(i)?.code ?: ""
            if(i < (orderData?.appliedVouchers?.size ?: 0)){
                coupons += ", "
            }
        }

        val purchaseParams = Bundle()
        purchaseParams.putString(FirebaseAnalytics.Param.TRANSACTION_ID, orderData?.code ?: "")
        purchaseParams.putString(FirebaseAnalytics.Param.AFFILIATION, "Google Store")
        purchaseParams.putString(
            FirebaseAnalytics.Param.CURRENCY,
            orderData?.totalPriceWithTax?.currencyIso ?: ""
        )
        purchaseParams.putString(
            FirebaseAnalytics.Param.COUPON,
            coupons
        )
        orderData?.totalPriceWithTax?.value?.let {
            purchaseParams.putDouble(
                FirebaseAnalytics.Param.VALUE,
                it
            )
        }
        orderData?.totalTax?.value?.let {
            purchaseParams.putDouble(
                FirebaseAnalytics.Param.TAX,
                it ?: 0.0
            )
        }
        orderData?.deliveryCost?.value?.let {
            purchaseParams.putDouble(
                FirebaseAnalytics.Param.SHIPPING,
                it ?: 0.0
            )
        }
        purchaseParams.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOfProducts)

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE, purchaseParams)

        val addPaymentParams = Bundle()
        addPaymentParams.putString(
            FirebaseAnalytics.Param.CURRENCY,
            orderData?.totalPriceWithTax?.currencyIso ?: ""
        )
        orderData?.totalPriceWithTax?.value.let {
            addPaymentParams.putFloat(
                FirebaseAnalytics.Param.VALUE,
                it?.toFloat()!! ?: 0.0f
            )
        }
        addPaymentParams.putString(
            FirebaseAnalytics.Param.PAYMENT_TYPE,
            orderData?.paymentMode?.name ?: ""
        )
        addPaymentParams.putParcelableArray(
            FirebaseAnalytics.Param.ITEMS,
            arrayOfProducts
        )
        addPaymentParams.putString(
            FirebaseAnalytics.Param.COUPON,
            coupons
        )

        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO, addPaymentParams)

        val addShippingParams = Bundle()
        addShippingParams.putString(
            FirebaseAnalytics.Param.CURRENCY,
            orderData?.totalPriceWithTax?.currencyIso ?: ""
        )
        orderData?.totalPriceWithTax?.value?.let {
            addShippingParams.putDouble(
                FirebaseAnalytics.Param.VALUE,
                it ?: 0.0
            )
        }
        addShippingParams.putString(FirebaseAnalytics.Param.SHIPPING_TIER, "Ground")
        addShippingParams.putParcelableArray(
            FirebaseAnalytics.Param.ITEMS,
            arrayOfProducts
        )

        addShippingParams.putString(
            FirebaseAnalytics.Param.COUPON,
            coupons
        )


        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_SHIPPING_INFO, addShippingParams)
    }

    fun append(arr: Array<Bundle?>, element: Bundle): Array<Bundle?> {
        val array = arrayOfNulls<Bundle>(arr.size + 1)
        System.arraycopy(arr, 0, array, 0, arr.size)
        array[arr.size] = element
        return array
    }

}
