package com.erabia.foodcrowd.android.checkout.model


import com.google.gson.annotations.SerializedName

data class TimeSlot(
    @SerializedName("timeSlotDays")
    val timeSlotDays: List<TimeSlotDay>? = listOf()
)