package com.erabia.foodcrowd.android.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Feature
import com.erabia.foodcrowd.android.databinding.RowClassificationItemBinding

class ClassificationListAdapter :
    RecyclerView.Adapter<ClassificationListAdapter.MyViewHolder>() {
    var featureList: List<Feature>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val binding = DataBindingUtil.inflate<RowClassificationItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.row_classification_item,
            parent,
            false
        )
        context = parent.context
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return featureList?.size ?: 0
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (featureList?.get(position)!=null){
            holder.binding.feature = featureList?.get(position)
//        if (BuildConfig.FLAVOR.contains("Kaf")) {
            if (position % 2 == 0) holder.itemView.setBackgroundColor(
                ContextCompat.getColor(context, R.color.white)
            )
            else holder.itemView.setBackgroundColor(
                ContextCompat.getColor(context, R.color.grayRowColor)
            )
        }

    }

    class MyViewHolder(val binding: RowClassificationItemBinding) :
        RecyclerView.ViewHolder(binding.root)
}