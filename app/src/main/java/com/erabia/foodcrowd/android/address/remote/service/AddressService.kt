package com.erabia.foodcrowd.android.address.remote.service

import com.erabia.foodcrowd.android.address.model.AddressList
import com.erabia.foodcrowd.android.address.model.CityList
import com.erabia.foodcrowd.android.address.model.CountryList
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.model.City
import com.erabia.foodcrowd.android.common.model.Country
import com.erabia.foodcrowd.android.personaldetails.model.PersonalDetailsModel
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface AddressService {
    @POST("rest/v2/foodcrowd-ae/users/{userId}/addresses")
    fun createAddress(
        @Path("userId") userId: String,
        @Body body: Address
    ): Observable<Response<Address>>

    @GET("rest/v2/foodcrowd-ae/countries")
    fun getCountryList(
        @Query("fields") fields: String,
        @Query("type") type: String
    ): Observable<Response<CountryList>>


    @GET("rest/v2/foodcrowd-ae/users/{userId}/addresses")
    abstract fun getAddressList(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<AddressList>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/carts/{cartId}/addresses/delivery")
    fun setDeliveryAddress(
        @Path("userId") userId: String,
        @Path("cartId") cartId: String,
        @Query("addressId") addressId: String
    ): Observable<Response<Void>>

    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/addresses/{addressId}")
    fun deleteAddress(
        @Path("userId") userId: String,
        @Path("addressId") addressId: String
    ): Observable<Response<Void>>

    @GET("rest/v2/foodcrowd-ae/users/{userId}/addresses/{addressId}")
    fun getAddressDetails(
        @Path("userId") userId: String,
        @Path("addressId") addressId: String,
        @Query("fields") fields: String
    ): Observable<Response<Address>>

    @PUT("rest/v2/foodcrowd-ae/users/{userId}/addresses/{addressId}")
    fun updateAddress(
        @Path("userId") userId: String,
        @Path("addressId") addressId: String,
        @Body body: Address
    ): Observable<Response<Void>>


    @GET("rest/v2/foodcrowd-ae/countries/{countyIsoCode}/cities")
    fun getCities(
        @Path("countyIsoCode") countyIsoCode: String,
        @Query("fields") fields: String
    ): Observable<Response<CityList>>


    @GET("rest/v2/foodcrowd-ae/titles")
    fun getLocalizedTitle(
        @Query("fields") fields: String
    ): Observable<Response<TitleResponse>>


    @GET("rest/v2/foodcrowd-ae/users/{userId}")
    fun getCustomerProfile(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<PersonalDetailsModel>>


}