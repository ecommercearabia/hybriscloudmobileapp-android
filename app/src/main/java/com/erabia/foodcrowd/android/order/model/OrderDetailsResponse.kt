package com.erabia.foodcrowd.android.order.model

import com.google.gson.annotations.SerializedName
data class OrderDetailsResponse(
    @SerializedName("appliedOrderPromotions")
    var appliedOrderPromotions: List<AppliedOrderPromotion?>? = listOf(),
    @SerializedName("appliedProductPromotions")
    var appliedProductPromotions: List<AppliedProductPromotion?>? = listOf(),
    @SerializedName("appliedVouchers")
    var appliedVouchers: List<AppliedVoucher?>? = listOf(),
    @SerializedName("calculated")
    var calculated: Boolean? = false,
    @SerializedName("cancellable")
    var cancellable: Boolean? = false,
    @SerializedName("code")
    var code: String? = "",
    @SerializedName("consignments")
    var consignments: List<Consignment?>? = listOf(),
    @SerializedName("created")
    var created: String? = "",
    @SerializedName("deliveryAddress")
    var deliveryAddress: DeliveryAddress? = DeliveryAddress(),
    @SerializedName("deliveryCost")
    var deliveryCost: DeliveryCost? = DeliveryCost(),
    @SerializedName("deliveryItemsQuantity")
    var deliveryItemsQuantity: Double?=0.0,
    @SerializedName("deliveryMode")
    var deliveryMode: DeliveryMode? = DeliveryMode(),
    @SerializedName("deliveryOrderGroups")
    var deliveryOrderGroups: List<DeliveryOrderGroup?>? = listOf(),
    @SerializedName("deliveryStatus")
    var deliveryStatus: String? = "",
    @SerializedName("deliveryStatusDisplay")
    var deliveryStatusDisplay: String? = "",
    @SerializedName("entries")
    var entries: List<Entry?> = listOf(),
    @SerializedName("guestCustomer")
    var guestCustomer: Boolean? = false,
    @SerializedName("guid")
    var guid: String? = "",
    @SerializedName("net")
    var net: Boolean? = false,
    @SerializedName("orderDiscounts")
    var orderDiscounts: OrderDiscounts? = OrderDiscounts(),
    @SerializedName("paymentAddress")
    var paymentAddress: PaymentAddress? = PaymentAddress(),
    @SerializedName("paymentInfo")
    var paymentInfo: PaymentInfo? = PaymentInfo(),
    @SerializedName("paymentMode")
    var paymentMode: PaymentMode? = PaymentMode(),
    @SerializedName("pickupItemsQuantity")
    var pickupItemsQuantity: Double?=0.0,
    @SerializedName("pickupOrderGroups")
    var pickupOrderGroups: List<PickupOrderGroup?>? = listOf(),
    @SerializedName("productDiscounts")
    var productDiscounts: ProductDiscounts? = ProductDiscounts(),
    @SerializedName("returnable")
    var returnable: Boolean? = false,
    @SerializedName("site")
    var site: String? = "",
    @SerializedName("status")
    var status: String? = "",
    @SerializedName("statusDisplay")
    var statusDisplay: String? = "",
    @SerializedName("store")
    var store: String? = "",
    @SerializedName("storeCreditAmount")
    var storeCreditAmount: StoreCreditAmount? = StoreCreditAmount(),
    @SerializedName("storeCreditAmountSelected")
    var storeCreditAmountSelected: Double?=0.0,
    @SerializedName("storeCreditMode")
    var storeCreditMode: StoreCreditMode? = StoreCreditMode(),
    @SerializedName("subTotal")
    var subTotal: SubTotal? = SubTotal(),
    @SerializedName("timeSlotInfoData")
    var timeSlotInfoData: TimeSlotInfoData? = TimeSlotInfoData(),
    @SerializedName("totalDiscounts")
    var totalDiscounts: TotalDiscounts? = TotalDiscounts(),
    @SerializedName("totalItems")
    var totalItems: Double?=0.0,
    @SerializedName("totalPrice")
    var totalPrice: TotalPrice? = TotalPrice(),
    @SerializedName("totalPriceWithTax")
    var totalPriceWithTax: TotalPriceWithTax? = TotalPriceWithTax(),
    @SerializedName("totalTax")
    var totalTax: TotalTax? = TotalTax(),
    @SerializedName("unconsignedEntries")
    var unconsignedEntries: List<UnconsignedEntry?>? = listOf(),
    @SerializedName("user")
    var user: User? = User()
) {
    data class AppliedOrderPromotion(
        @SerializedName("consumedEntries")
        var consumedEntries: List<ConsumedEntry?>? = listOf(),
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("promotion")
        var promotion: Promotion? = Promotion()
    ) {
        data class ConsumedEntry(
            @SerializedName("adjustedUnitPrice")
            var adjustedUnitPrice: Double?=0.0,
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("orderEntryNumber")
            var orderEntryNumber: Double?=0.0,
            @SerializedName("quantity")
            var quantity: Double?=0.0
        )

        data class Promotion(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("couldFireMessages")
            var couldFireMessages: List<String?>? = listOf(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("enabled")
            var enabled: Boolean? = false,
            @SerializedName("endDate")
            var endDate: String? = "",
            @SerializedName("firedMessages")
            var firedMessages: List<String?>? = listOf(),
            @SerializedName("priority")
            var priority: Double?=0.0,
            @SerializedName("productBanner")
            var productBanner: ProductBanner? = ProductBanner(),
            @SerializedName("promotionGroup")
            var promotionGroup: String? = "",
            @SerializedName("promotionType")
            var promotionType: String? = "",
            @SerializedName("restrictions")
            var restrictions: List<Restriction?>? = listOf(),
            @SerializedName("startDate")
            var startDate: String? = "",
            @SerializedName("title")
            var title: String? = ""
        ) {
            data class ProductBanner(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class Restriction(
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("restrictionType")
                var restrictionType: String? = ""
            )
        }
    }

    data class AppliedProductPromotion(
        @SerializedName("consumedEntries")
        var consumedEntries: List<ConsumedEntry?>? = listOf(),
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("promotion")
        var promotion: Promotion? = Promotion()
    ) {
        data class ConsumedEntry(
            @SerializedName("adjustedUnitPrice")
            var adjustedUnitPrice: Double?=0.0,
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("orderEntryNumber")
            var orderEntryNumber: Double?=0.0,
            @SerializedName("quantity")
            var quantity: Double?=0.0
        )

        data class Promotion(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("couldFireMessages")
            var couldFireMessages: List<String?>? = listOf(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("enabled")
            var enabled: Boolean? = false,
            @SerializedName("endDate")
            var endDate: String? = "",
            @SerializedName("firedMessages")
            var firedMessages: List<String?>? = listOf(),
            @SerializedName("priority")
            var priority: Double?=0.0,
            @SerializedName("productBanner")
            var productBanner: ProductBanner? = ProductBanner(),
            @SerializedName("promotionGroup")
            var promotionGroup: String? = "",
            @SerializedName("promotionType")
            var promotionType: String? = "",
            @SerializedName("restrictions")
            var restrictions: List<Restriction?>? = listOf(),
            @SerializedName("startDate")
            var startDate: String? = "",
            @SerializedName("title")
            var title: String? = ""
        ) {
            data class ProductBanner(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class Restriction(
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("restrictionType")
                var restrictionType: String? = ""
            )
        }
    }

    data class AppliedVoucher(
        @SerializedName("appliedValue")
        var appliedValue: AppliedValue? = AppliedValue(),
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("currency")
        var currency: Currency? = Currency(),
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("freeShipping")
        var freeShipping: Boolean? = false,
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("value")
        var value: Double?=0.0,
        @SerializedName("valueFormatted")
        var valueFormatted: String? = "",
        @SerializedName("valueString")
        var valueString: String? = "",
        @SerializedName("voucherCode")
        var voucherCode: String? = ""
    ) {
        data class AppliedValue(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class Currency(
            @SerializedName("active")
            var active: Boolean? = false,
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("symbol")
            var symbol: String? = ""
        )
    }

    data class Consignment(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("deliveryMode")
        var deliveryMode: DeliveryMode? = DeliveryMode(),
        @SerializedName("deliveryPointOfService")
        var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
        @SerializedName("entries")
        var entries: List<Entry?>? = listOf(),
        @SerializedName("orderCode")
        var orderCode: String? = "",
        @SerializedName("packagingInfo")
        var packagingInfo: PackagingInfo? = PackagingInfo(),
        @SerializedName("shippingAddress")
        var shippingAddress: ShippingAddress? = ShippingAddress(),
        @SerializedName("shippingDate")
        var shippingDate: String? = "",
        @SerializedName("status")
        var status: String? = "",
        @SerializedName("statusDate")
        var statusDate: String? = "",
        @SerializedName("statusDisplay")
        var statusDisplay: String? = "",
        @SerializedName("trackingID")
        var trackingID: String? = "",
        @SerializedName("warehouseCode")
        var warehouseCode: String? = ""
    ) {
        data class DeliveryMode(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("deliveryCost")
            var deliveryCost: DeliveryCost? = DeliveryCost(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) {
            data class DeliveryCost(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class DeliveryPointOfService(
            @SerializedName("address")
            var address: Address? = Address(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("displayName")
            var displayName: String? = "",
            @SerializedName("distanceKm")
            var distanceKm: Double?=0.0,
            @SerializedName("features")
            var features: Features? = Features(),
            @SerializedName("formattedDistance")
            var formattedDistance: String? = "",
            @SerializedName("geoPoint")
            var geoPoint: GeoPoint? = GeoPoint(),
            @SerializedName("mapIcon")
            var mapIcon: MapIcon? = MapIcon(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("openingHours")
            var openingHours: OpeningHours? = OpeningHours(),
            @SerializedName("storeContent")
            var storeContent: String? = "",
            @SerializedName("storeImages")
            var storeImages: List<StoreImage?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("warehouseCodes")
            var warehouseCodes: List<String?>? = listOf()
        ) {
            data class Address(
                @SerializedName("addressName")
                var addressName: String? = "",
                @SerializedName("area")
                var area: Area? = Area(),
                @SerializedName("cellphone")
                var cellphone: String? = "",
                @SerializedName("city")
                var city: City? = City(),
                @SerializedName("companyName")
                var companyName: String? = "",
                @SerializedName("country")
                var country: Country? = Country(),
                @SerializedName("defaultAddress")
                var defaultAddress: Boolean? = false,
                @SerializedName("district")
                var district: String? = "",
                @SerializedName("email")
                var email: String? = "",
                @SerializedName("firstName")
                var firstName: String? = "",
                @SerializedName("formattedAddress")
                var formattedAddress: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("lastName")
                var lastName: String? = "",
                @SerializedName("line1")
                var line1: String? = "",
                @SerializedName("line2")
                var line2: String? = "",
                @SerializedName("mobileCountry")
                var mobileCountry: MobileCountry? = MobileCountry(),
                @SerializedName("mobileNumber")
                var mobileNumber: String? = "",
                @SerializedName("nearestLandmark")
                var nearestLandmark: String? = "",
                @SerializedName("phone")
                var phone: String? = "",
                @SerializedName("postalCode")
                var postalCode: String? = "",
                @SerializedName("region")
                var region: Region? = Region(),
                @SerializedName("shippingAddress")
                var shippingAddress: Boolean? = false,
                @SerializedName("title")
                var title: String? = "",
                @SerializedName("titleCode")
                var titleCode: String? = "",
                @SerializedName("town")
                var town: String? = "",
                @SerializedName("visibleInAddressBook")
                var visibleInAddressBook: Boolean? = false
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class City(
                    @SerializedName("areas")
                    var areas: List<Area?>? = listOf(),
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Country(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class MobileCountry(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class Region(
                    @SerializedName("countryIso")
                    var countryIso: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("isocodeShort")
                    var isocodeShort: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Features(
                @SerializedName("additionalProp1")
                var additionalProp1: String? = "",
                @SerializedName("additionalProp2")
                var additionalProp2: String? = "",
                @SerializedName("additionalProp3")
                var additionalProp3: String? = ""
            )

            data class GeoPoint(
                @SerializedName("latitude")
                var latitude: Double?=0.0,
                @SerializedName("longitude")
                var longitude: Double?=0.0
            )

            data class MapIcon(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class OpeningHours(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("specialDayOpeningList")
                var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                @SerializedName("weekDayOpeningList")
                var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
            ) {
                data class SpecialDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime()
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }

                data class WeekDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime(),
                    @SerializedName("weekDay")
                    var weekDay: String? = ""
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }
            }

            data class StoreImage(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )
        }

        data class Entry(
            @SerializedName("orderEntry")
            var orderEntry: OrderEntry? = OrderEntry(),
            @SerializedName("quantity")
            var quantity: Double?=0.0,
            @SerializedName("quantityDeclined")
            var quantityDeclined: Double?=0.0,
            @SerializedName("quantityPending")
            var quantityPending: Double?=0.0,
            @SerializedName("quantityShipped")
            var quantityShipped: Double?=0.0,
            @SerializedName("shippedQuantity")
            var shippedQuantity: Double?=0.0
        ) {
            data class OrderEntry(
                @SerializedName("basePrice")
                var basePrice: BasePrice? = BasePrice(),
                @SerializedName("cancellableQuantity")
                var cancellableQuantity: Double?=0.0,
                @SerializedName("cancelledItemsPrice")
                var cancelledItemsPrice: CancelledItemsPrice? = CancelledItemsPrice(),
                @SerializedName("configurationInfos")
                var configurationInfos: List<ConfigurationInfo?>? = listOf(),
                @SerializedName("deliveryMode")
                var deliveryMode: DeliveryMode? = DeliveryMode(),
                @SerializedName("deliveryPointOfService")
                var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
                @SerializedName("entryNumber")
                var entryNumber: Double?=0.0,
                @SerializedName("product")
                var product: Product? = Product(),
                @SerializedName("quantity")
                var quantity: Double?=0.0,
                @SerializedName("quantityAllocated")
                var quantityAllocated: Double?=0.0,
                @SerializedName("quantityCancelled")
                var quantityCancelled: Double?=0.0,
                @SerializedName("quantityPending")
                var quantityPending: Double?=0.0,
                @SerializedName("quantityReturned")
                var quantityReturned: Double?=0.0,
                @SerializedName("quantityShipped")
                var quantityShipped: Double?=0.0,
                @SerializedName("quantityUnallocated")
                var quantityUnallocated: Double?=0.0,
                @SerializedName("returnableQuantity")
                var returnableQuantity: Double?=0.0,
                @SerializedName("returnedItemsPrice")
                var returnedItemsPrice: ReturnedItemsPrice? = ReturnedItemsPrice(),
                @SerializedName("totalPrice")
                var totalPrice: TotalPrice? = TotalPrice(),
                @SerializedName("updateable")
                var updateable: Boolean? = false,
                @SerializedName("url")
                var url: String? = ""
            ) {
                data class BasePrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class CancelledItemsPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class ConfigurationInfo(
                    @SerializedName("configurationLabel")
                    var configurationLabel: String? = "",
                    @SerializedName("configurationValue")
                    var configurationValue: String? = "",
                    @SerializedName("configuratorType")
                    var configuratorType: String? = "",
                    @SerializedName("status")
                    var status: String? = ""
                )

                data class DeliveryMode(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("deliveryCost")
                    var deliveryCost: DeliveryCost? = DeliveryCost(),
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class DeliveryCost(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )
                }

                data class DeliveryPointOfService(
                    @SerializedName("address")
                    var address: Address? = Address(),
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("displayName")
                    var displayName: String? = "",
                    @SerializedName("distanceKm")
                    var distanceKm: Double?=0.0,
                    @SerializedName("features")
                    var features: Features? = Features(),
                    @SerializedName("formattedDistance")
                    var formattedDistance: String? = "",
                    @SerializedName("geoPoint")
                    var geoPoint: GeoPoint? = GeoPoint(),
                    @SerializedName("mapIcon")
                    var mapIcon: MapIcon? = MapIcon(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("openingHours")
                    var openingHours: OpeningHours? = OpeningHours(),
                    @SerializedName("storeContent")
                    var storeContent: String? = "",
                    @SerializedName("storeImages")
                    var storeImages: List<StoreImage?>? = listOf(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("warehouseCodes")
                    var warehouseCodes: List<String?>? = listOf()
                ) {
                    data class Address(
                        @SerializedName("addressName")
                        var addressName: String? = "",
                        @SerializedName("area")
                        var area: Area? = Area(),
                        @SerializedName("cellphone")
                        var cellphone: String? = "",
                        @SerializedName("city")
                        var city: City? = City(),
                        @SerializedName("companyName")
                        var companyName: String? = "",
                        @SerializedName("country")
                        var country: Country? = Country(),
                        @SerializedName("defaultAddress")
                        var defaultAddress: Boolean? = false,
                        @SerializedName("district")
                        var district: String? = "",
                        @SerializedName("email")
                        var email: String? = "",
                        @SerializedName("firstName")
                        var firstName: String? = "",
                        @SerializedName("formattedAddress")
                        var formattedAddress: String? = "",
                        @SerializedName("id")
                        var id: String? = "",
                        @SerializedName("lastName")
                        var lastName: String? = "",
                        @SerializedName("line1")
                        var line1: String? = "",
                        @SerializedName("line2")
                        var line2: String? = "",
                        @SerializedName("mobileCountry")
                        var mobileCountry: MobileCountry? = MobileCountry(),
                        @SerializedName("mobileNumber")
                        var mobileNumber: String? = "",
                        @SerializedName("nearestLandmark")
                        var nearestLandmark: String? = "",
                        @SerializedName("phone")
                        var phone: String? = "",
                        @SerializedName("postalCode")
                        var postalCode: String? = "",
                        @SerializedName("region")
                        var region: Region? = Region(),
                        @SerializedName("shippingAddress")
                        var shippingAddress: Boolean? = false,
                        @SerializedName("title")
                        var title: String? = "",
                        @SerializedName("titleCode")
                        var titleCode: String? = "",
                        @SerializedName("town")
                        var town: String? = "",
                        @SerializedName("visibleInAddressBook")
                        var visibleInAddressBook: Boolean? = false
                    ) {
                        data class Area(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class City(
                            @SerializedName("areas")
                            var areas: List<Area?>? = listOf(),
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        ) {
                            data class Area(
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }

                        data class Country(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class MobileCountry(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class Region(
                            @SerializedName("countryIso")
                            var countryIso: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("isocodeShort")
                            var isocodeShort: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }

                    data class Features(
                        @SerializedName("additionalProp1")
                        var additionalProp1: String? = "",
                        @SerializedName("additionalProp2")
                        var additionalProp2: String? = "",
                        @SerializedName("additionalProp3")
                        var additionalProp3: String? = ""
                    )

                    data class GeoPoint(
                        @SerializedName("latitude")
                        var latitude: Double?=0.0,
                        @SerializedName("longitude")
                        var longitude: Double?=0.0
                    )

                    data class MapIcon(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class OpeningHours(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("specialDayOpeningList")
                        var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                        @SerializedName("weekDayOpeningList")
                        var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
                    ) {
                        data class SpecialDayOpening(
                            @SerializedName("closed")
                            var closed: Boolean? = false,
                            @SerializedName("closingTime")
                            var closingTime: ClosingTime? = ClosingTime(),
                            @SerializedName("comment")
                            var comment: String? = "",
                            @SerializedName("date")
                            var date: String? = "",
                            @SerializedName("formattedDate")
                            var formattedDate: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("openingTime")
                            var openingTime: OpeningTime? = OpeningTime()
                        ) {
                            data class ClosingTime(
                                @SerializedName("formattedHour")
                                var formattedHour: String? = "",
                                @SerializedName("hour")
                                var hour: Double?=0.0,
                                @SerializedName("minute")
                                var minute: Double?=0.0
                            )

                            data class OpeningTime(
                                @SerializedName("formattedHour")
                                var formattedHour: String? = "",
                                @SerializedName("hour")
                                var hour: Double?=0.0,
                                @SerializedName("minute")
                                var minute: Double?=0.0
                            )
                        }

                        data class WeekDayOpening(
                            @SerializedName("closed")
                            var closed: Boolean? = false,
                            @SerializedName("closingTime")
                            var closingTime: ClosingTime? = ClosingTime(),
                            @SerializedName("openingTime")
                            var openingTime: OpeningTime? = OpeningTime(),
                            @SerializedName("weekDay")
                            var weekDay: String? = ""
                        ) {
                            data class ClosingTime(
                                @SerializedName("formattedHour")
                                var formattedHour: String? = "",
                                @SerializedName("hour")
                                var hour: Double?=0.0,
                                @SerializedName("minute")
                                var minute: Double?=0.0
                            )

                            data class OpeningTime(
                                @SerializedName("formattedHour")
                                var formattedHour: String? = "",
                                @SerializedName("hour")
                                var hour: Double?=0.0,
                                @SerializedName("minute")
                                var minute: Double?=0.0
                            )
                        }
                    }

                    data class StoreImage(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )
                }

                data class Product(
                    @SerializedName("availableForPickup")
                    var availableForPickup: Boolean? = false,
                    @SerializedName("nutritionFacts")
                    var nutritionFacts: String? = "",
                    @SerializedName("averageRating")
                    var averageRating: Double?=0.0,
                    @SerializedName("baseOptions")
                    var baseOptions: List<BaseOption?>? = listOf(),
                    @SerializedName("baseProduct")
                    var baseProduct: String? = "",
                    @SerializedName("categories")
                    var categories: List<Category?>? = listOf(),
                    @SerializedName("classifications")
                    var classifications: List<Classification?>? = listOf(),
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("configurable")
                    var configurable: Boolean? = false,
                    @SerializedName("configuratorType")
                    var configuratorType: String? = "",
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("futureStocks")
                    var futureStocks: List<FutureStock?>? = listOf(),
                    @SerializedName("images")
                    var images: List<Image?>? = listOf(),
                    @SerializedName("manufacturer")
                    var manufacturer: String? = "",
                    @SerializedName("multidimensional")
                    var multidimensional: Boolean? = false,
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("numberOfReviews")
                    var numberOfReviews: Double?=0.0,
                    @SerializedName("potentialPromotions")
                    var potentialPromotions: List<PotentialPromotion?>? = listOf(),
                    @SerializedName("price")
                    var price: Price? = Price(),
                    @SerializedName("priceRange")
                    var priceRange: PriceRange? = PriceRange(),
                    @SerializedName("productReferences")
                    var productReferences: List<ProductReference?>? = listOf(),
                    @SerializedName("purchasable")
                    var purchasable: Boolean? = false,
                    @SerializedName("reviews")
                    var reviews: List<Review?>? = listOf(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("summary")
                    var summary: String? = "",
                    @SerializedName("tags")
                    var tags: List<String?>? = listOf(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantMatrix")
                    var variantMatrix: List<VariantMatrix?>? = listOf(),
                    @SerializedName("variantOptions")
                    var variantOptions: List<VariantOption?>? = listOf(),
                    @SerializedName("variantType")
                    var variantType: String? = "",
                    @SerializedName("volumePrices")
                    var volumePrices: List<VolumePrice?>? = listOf(),
                    @SerializedName("volumePricesFlag")
                    var volumePricesFlag: Boolean? = false
                ) {
                    data class BaseOption(
                        @SerializedName("options")
                        var options: List<Option?>? = listOf(),
                        @SerializedName("selected")
                        var selected: Selected? = Selected(),
                        @SerializedName("variantType")
                        var variantType: String? = ""
                    ) {
                        data class Option(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("priceData")
                            var priceData: PriceData? = PriceData(),
                            @SerializedName("stock")
                            var stock: Stock? = Stock(),
                            @SerializedName("url")
                            var url: String? = "",
                            @SerializedName("variantOptionQualifiers")
                            var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                        ) {
                            data class PriceData(
                                @SerializedName("currencyIso")
                                var currencyIso: String? = "",
                                @SerializedName("formattedValue")
                                var formattedValue: String? = "",
                                @SerializedName("maxQuantity")
                                var maxQuantity: Double?=0.0,
                                @SerializedName("minQuantity")
                                var minQuantity: Double?=0.0,
                                @SerializedName("priceType")
                                var priceType: String? = "",
                                @SerializedName("value")
                                var value: Double?=0.0
                            )

                            data class Stock(
                                @SerializedName("stockLevel")
                                var stockLevel: Double?=0.0,
                                @SerializedName("stockLevelStatus")
                                var stockLevelStatus: String? = ""
                            )

                            data class VariantOptionQualifier(
                                @SerializedName("image")
                                var image: Image? = Image(),
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("qualifier")
                                var qualifier: String? = "",
                                @SerializedName("value")
                                var value: String? = ""
                            ) {
                                data class Image(
                                    @SerializedName("altText")
                                    var altText: String? = "",
                                    @SerializedName("format")
                                    var format: String? = "",
                                    @SerializedName("galleryIndex")
                                    var galleryIndex: Double?=0.0,
                                    @SerializedName("imageType")
                                    var imageType: String? = "",
                                    @SerializedName("url")
                                    var url: String? = ""
                                )
                            }
                        }

                        data class Selected(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("priceData")
                            var priceData: PriceData? = PriceData(),
                            @SerializedName("stock")
                            var stock: Stock? = Stock(),
                            @SerializedName("url")
                            var url: String? = "",
                            @SerializedName("variantOptionQualifiers")
                            var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                        ) {
                            data class PriceData(
                                @SerializedName("currencyIso")
                                var currencyIso: String? = "",
                                @SerializedName("formattedValue")
                                var formattedValue: String? = "",
                                @SerializedName("maxQuantity")
                                var maxQuantity: Double?=0.0,
                                @SerializedName("minQuantity")
                                var minQuantity: Double?=0.0,
                                @SerializedName("priceType")
                                var priceType: String? = "",
                                @SerializedName("value")
                                var value: Double?=0.0
                            )

                            data class Stock(
                                @SerializedName("stockLevel")
                                var stockLevel: Double?=0.0,
                                @SerializedName("stockLevelStatus")
                                var stockLevelStatus: String? = ""
                            )

                            data class VariantOptionQualifier(
                                @SerializedName("image")
                                var image: Image? = Image(),
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("qualifier")
                                var qualifier: String? = "",
                                @SerializedName("value")
                                var value: String? = ""
                            ) {
                                data class Image(
                                    @SerializedName("altText")
                                    var altText: String? = "",
                                    @SerializedName("format")
                                    var format: String? = "",
                                    @SerializedName("galleryIndex")
                                    var galleryIndex: Double?=0.0,
                                    @SerializedName("imageType")
                                    var imageType: String? = "",
                                    @SerializedName("url")
                                    var url: String? = ""
                                )
                            }
                        }
                    }

                    data class Category(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("plpPicture")
                        var plpPicture: PlpPicture? = PlpPicture(),
                        @SerializedName("plpPictureResponsive")
                        var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
                        @SerializedName("url")
                        var url: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )

                        data class PlpPicture(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )

                        data class PlpPictureResponsive(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }

                    data class Classification(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("features")
                        var features: List<Feature?>? = listOf(),
                        @SerializedName("name")
                        var name: String? = ""
                    ) {
                        data class Feature(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("comparable")
                            var comparable: Boolean? = false,
                            @SerializedName("description")
                            var description: String? = "",
                            @SerializedName("featureUnit")
                            var featureUnit: FeatureUnit? = FeatureUnit(),
                            @SerializedName("featureValues")
                            var featureValues: List<FeatureValue?>? = listOf(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("range")
                            var range: Boolean? = false,
                            @SerializedName("type")
                            var type: String? = ""
                        ) {
                            data class FeatureUnit(
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("symbol")
                                var symbol: String? = "",
                                @SerializedName("unitType")
                                var unitType: String? = ""
                            )

                            data class FeatureValue(
                                @SerializedName("value")
                                var value: String? = ""
                            )
                        }
                    }

                    data class FutureStock(
                        @SerializedName("date")
                        var date: String? = "",
                        @SerializedName("formattedDate")
                        var formattedDate: String? = "",
                        @SerializedName("stock")
                        var stock: Stock? = Stock()
                    ) {
                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )
                    }

                    data class Image(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class PotentialPromotion(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("couldFireMessages")
                        var couldFireMessages: List<String?>? = listOf(),
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("enabled")
                        var enabled: Boolean? = false,
                        @SerializedName("endDate")
                        var endDate: String? = "",
                        @SerializedName("firedMessages")
                        var firedMessages: List<String?>? = listOf(),
                        @SerializedName("priority")
                        var priority: Double?=0.0,
                        @SerializedName("productBanner")
                        var productBanner: ProductBanner? = ProductBanner(),
                        @SerializedName("promotionGroup")
                        var promotionGroup: String? = "",
                        @SerializedName("promotionType")
                        var promotionType: String? = "",
                        @SerializedName("restrictions")
                        var restrictions: List<Restriction?>? = listOf(),
                        @SerializedName("startDate")
                        var startDate: String? = "",
                        @SerializedName("title")
                        var title: String? = ""
                    ) {
                        data class ProductBanner(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )

                        data class Restriction(
                            @SerializedName("description")
                            var description: String? = "",
                            @SerializedName("restrictionType")
                            var restrictionType: String? = ""
                        )
                    }

                    data class Price(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class PriceRange(
                        @SerializedName("maxPrice")
                        var maxPrice: MaxPrice? = MaxPrice(),
                        @SerializedName("minPrice")
                        var minPrice: MinPrice? = MinPrice()
                    ) {
                        data class MaxPrice(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class MinPrice(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )
                    }

                    data class ProductReference(
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("preselected")
                        var preselected: Boolean? = false,
                        @SerializedName("quantity")
                        var quantity: Double?=0.0,
                        @SerializedName("referenceType")
                        var referenceType: String? = ""
                    )

                    data class Review(
                        @SerializedName("alias")
                        var alias: String? = "",
                        @SerializedName("comment")
                        var comment: String? = "",
                        @SerializedName("date")
                        var date: String? = "",
                        @SerializedName("headline")
                        var headline: String? = "",
                        @SerializedName("id")
                        var id: String? = "",
                        @SerializedName("principal")
                        var principal: Principal? = Principal(),
                        @SerializedName("rating")
                        var rating: Double?=0.0
                    ) {
                        data class Principal(
                            @SerializedName("currency")
                            var currency: Currency? = Currency(),
                            @SerializedName("customerId")
                            var customerId: String? = "",
                            @SerializedName("deactivationDate")
                            var deactivationDate: String? = "",
                            @SerializedName("defaultAddress")
                            var defaultAddress: DefaultAddress? = DefaultAddress(),
                            @SerializedName("displayUid")
                            var displayUid: String? = "",
                            @SerializedName("firstName")
                            var firstName: String? = "",
                            @SerializedName("language")
                            var language: Language? = Language(),
                            @SerializedName("lastName")
                            var lastName: String? = "",
                            @SerializedName("mobileCountry")
                            var mobileCountry: MobileCountry? = MobileCountry(),
                            @SerializedName("mobileNumber")
                            var mobileNumber: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("title")
                            var title: String? = "",
                            @SerializedName("titleCode")
                            var titleCode: String? = "",
                            @SerializedName("uid")
                            var uid: String? = ""
                        ) {
                            data class Currency(
                                @SerializedName("active")
                                var active: Boolean? = false,
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("symbol")
                                var symbol: String? = ""
                            )

                            data class DefaultAddress(
                                @SerializedName("addressName")
                                var addressName: String? = "",
                                @SerializedName("area")
                                var area: Area? = Area(),
                                @SerializedName("cellphone")
                                var cellphone: String? = "",
                                @SerializedName("city")
                                var city: City? = City(),
                                @SerializedName("companyName")
                                var companyName: String? = "",
                                @SerializedName("country")
                                var country: Country? = Country(),
                                @SerializedName("defaultAddress")
                                var defaultAddress: Boolean? = false,
                                @SerializedName("district")
                                var district: String? = "",
                                @SerializedName("email")
                                var email: String? = "",
                                @SerializedName("firstName")
                                var firstName: String? = "",
                                @SerializedName("formattedAddress")
                                var formattedAddress: String? = "",
                                @SerializedName("id")
                                var id: String? = "",
                                @SerializedName("lastName")
                                var lastName: String? = "",
                                @SerializedName("line1")
                                var line1: String? = "",
                                @SerializedName("line2")
                                var line2: String? = "",
                                @SerializedName("mobileCountry")
                                var mobileCountry: MobileCountry? = MobileCountry(),
                                @SerializedName("mobileNumber")
                                var mobileNumber: String? = "",
                                @SerializedName("nearestLandmark")
                                var nearestLandmark: String? = "",
                                @SerializedName("phone")
                                var phone: String? = "",
                                @SerializedName("postalCode")
                                var postalCode: String? = "",
                                @SerializedName("region")
                                var region: Region? = Region(),
                                @SerializedName("shippingAddress")
                                var shippingAddress: Boolean? = false,
                                @SerializedName("title")
                                var title: String? = "",
                                @SerializedName("titleCode")
                                var titleCode: String? = "",
                                @SerializedName("town")
                                var town: String? = "",
                                @SerializedName("visibleInAddressBook")
                                var visibleInAddressBook: Boolean? = false
                            ) {
                                data class Area(
                                    @SerializedName("code")
                                    var code: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )

                                data class City(
                                    @SerializedName("areas")
                                    var areas: List<Area?>? = listOf(),
                                    @SerializedName("code")
                                    var code: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                ) {
                                    data class Area(
                                        @SerializedName("code")
                                        var code: String? = "",
                                        @SerializedName("name")
                                        var name: String? = ""
                                    )
                                }

                                data class Country(
                                    @SerializedName("isdcode")
                                    var isdcode: String? = "",
                                    @SerializedName("isocode")
                                    var isocode: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )

                                data class MobileCountry(
                                    @SerializedName("isdcode")
                                    var isdcode: String? = "",
                                    @SerializedName("isocode")
                                    var isocode: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )

                                data class Region(
                                    @SerializedName("countryIso")
                                    var countryIso: String? = "",
                                    @SerializedName("isocode")
                                    var isocode: String? = "",
                                    @SerializedName("isocodeShort")
                                    var isocodeShort: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )
                            }

                            data class Language(
                                @SerializedName("active")
                                var active: Boolean? = false,
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("nativeName")
                                var nativeName: String? = ""
                            )

                            data class MobileCountry(
                                @SerializedName("isdcode")
                                var isdcode: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }
                    }

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantMatrix(
                        @SerializedName("elements")
                        var elements: List<Any?>? = listOf(),
                        @SerializedName("isLeaf")
                        var isLeaf: Boolean? = false,
                        @SerializedName("parentVariantCategory")
                        var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
                        @SerializedName("variantOption")
                        var variantOption: VariantOption? = VariantOption(),
                        @SerializedName("variantValueCategory")
                        var variantValueCategory: VariantValueCategory? = VariantValueCategory()
                    ) {
                        data class ParentVariantCategory(
                            @SerializedName("hasImage")
                            var hasImage: Boolean? = false,
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("priority")
                            var priority: Double?=0.0
                        )

                        data class VariantOption(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("priceData")
                            var priceData: PriceData? = PriceData(),
                            @SerializedName("stock")
                            var stock: Stock? = Stock(),
                            @SerializedName("url")
                            var url: String? = "",
                            @SerializedName("variantOptionQualifiers")
                            var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                        ) {
                            data class PriceData(
                                @SerializedName("currencyIso")
                                var currencyIso: String? = "",
                                @SerializedName("formattedValue")
                                var formattedValue: String? = "",
                                @SerializedName("maxQuantity")
                                var maxQuantity: Double?=0.0,
                                @SerializedName("minQuantity")
                                var minQuantity: Double?=0.0,
                                @SerializedName("priceType")
                                var priceType: String? = "",
                                @SerializedName("value")
                                var value: Double?=0.0
                            )

                            data class Stock(
                                @SerializedName("stockLevel")
                                var stockLevel: Double?=0.0,
                                @SerializedName("stockLevelStatus")
                                var stockLevelStatus: String? = ""
                            )

                            data class VariantOptionQualifier(
                                @SerializedName("image")
                                var image: Image? = Image(),
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("qualifier")
                                var qualifier: String? = "",
                                @SerializedName("value")
                                var value: String? = ""
                            ) {
                                data class Image(
                                    @SerializedName("altText")
                                    var altText: String? = "",
                                    @SerializedName("format")
                                    var format: String? = "",
                                    @SerializedName("galleryIndex")
                                    var galleryIndex: Double?=0.0,
                                    @SerializedName("imageType")
                                    var imageType: String? = "",
                                    @SerializedName("url")
                                    var url: String? = ""
                                )
                            }
                        }

                        data class VariantValueCategory(
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("sequence")
                            var sequence: Double?=0.0,
                            @SerializedName("superCategories")
                            var superCategories: List<SuperCategory?>? = listOf()
                        ) {
                            data class SuperCategory(
                                @SerializedName("hasImage")
                                var hasImage: Boolean? = false,
                                @SerializedName("name")
                                var name: String? = "",
                                @SerializedName("priority")
                                var priority: Double?=0.0
                            )
                        }
                    }

                    data class VariantOption(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }

                    data class VolumePrice(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )
                }

                data class ReturnedItemsPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class TotalPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }
        }

        data class PackagingInfo(
            @SerializedName("dimensionUnit")
            var dimensionUnit: String? = "",
            @SerializedName("grossWeight")
            var grossWeight: String? = "",
            @SerializedName("height")
            var height: String? = "",
            @SerializedName("insuredValue")
            var insuredValue: String? = "",
            @SerializedName("length")
            var length: String? = "",
            @SerializedName("weightUnit")
            var weightUnit: String? = "",
            @SerializedName("width")
            var width: String? = ""
        )

        data class ShippingAddress(
            @SerializedName("addressName")
            var addressName: String? = "",
            @SerializedName("area")
            var area: Area? = Area(),
            @SerializedName("cellphone")
            var cellphone: String? = "",
            @SerializedName("city")
            var city: City? = City(),
            @SerializedName("companyName")
            var companyName: String? = "",
            @SerializedName("country")
            var country: Country? = Country(),
            @SerializedName("defaultAddress")
            var defaultAddress: Boolean? = false,
            @SerializedName("district")
            var district: String? = "",
            @SerializedName("email")
            var email: String? = "",
            @SerializedName("firstName")
            var firstName: String? = "",
            @SerializedName("formattedAddress")
            var formattedAddress: String? = "",
            @SerializedName("id")
            var id: String? = "",
            @SerializedName("lastName")
            var lastName: String? = "",
            @SerializedName("line1")
            var line1: String? = "",
            @SerializedName("line2")
            var line2: String? = "",
            @SerializedName("mobileCountry")
            var mobileCountry: MobileCountry? = MobileCountry(),
            @SerializedName("mobileNumber")
            var mobileNumber: String? = "",
            @SerializedName("nearestLandmark")
            var nearestLandmark: String? = "",
            @SerializedName("phone")
            var phone: String? = "",
            @SerializedName("postalCode")
            var postalCode: String? = "",
            @SerializedName("region")
            var region: Region? = Region(),
            @SerializedName("shippingAddress")
            var shippingAddress: Boolean? = false,
            @SerializedName("title")
            var title: String? = "",
            @SerializedName("titleCode")
            var titleCode: String? = "",
            @SerializedName("town")
            var town: String? = "",
            @SerializedName("visibleInAddressBook")
            var visibleInAddressBook: Boolean? = false
        ) {
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class City(
                @SerializedName("areas")
                var areas: List<Area?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Country(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class MobileCountry(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class Region(
                @SerializedName("countryIso")
                var countryIso: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("isocodeShort")
                var isocodeShort: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )
        }
    }

    data class DeliveryAddress(
        @SerializedName("addressName")
        var addressName: String? = "",
        @SerializedName("area")
        var area: Area? = Area(),
        @SerializedName("cellphone")
        var cellphone: String? = "",
        @SerializedName("city")
        var city: City? = City(),
        @SerializedName("companyName")
        var companyName: String? = "",
        @SerializedName("country")
        var country: Country? = Country(),
        @SerializedName("defaultAddress")
        var defaultAddress: Boolean? = false,
        @SerializedName("district")
        var district: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("firstName")
        var firstName: String? = "",
        @SerializedName("formattedAddress")
        var formattedAddress: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("lastName")
        var lastName: String? = "",
        @SerializedName("line1")
        var line1: String? = "",
        @SerializedName("line2")
        var line2: String? = "",
        @SerializedName("mobileCountry")
        var mobileCountry: MobileCountry? = MobileCountry(),
        @SerializedName("mobileNumber")
        var mobileNumber: String? = "",
        @SerializedName("nearestLandmark")
        var nearestLandmark: String? = "",
        @SerializedName("phone")
        var phone: String? = "",
        @SerializedName("postalCode")
        var postalCode: String? = "",
        @SerializedName("region")
        var region: Region? = Region(),
        @SerializedName("shippingAddress")
        var shippingAddress: Boolean? = false,
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("titleCode")
        var titleCode: String? = "",
        @SerializedName("town")
        var town: String? = "",
        @SerializedName("visibleInAddressBook")
        var visibleInAddressBook: Boolean? = false
    ) {
        data class Area(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class City(
            @SerializedName("areas")
            var areas: List<Area?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) {
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )
        }

        data class Country(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class MobileCountry(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class Region(
            @SerializedName("countryIso")
            var countryIso: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("isocodeShort")
            var isocodeShort: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )
    }

    data class DeliveryCost(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class DeliveryMode(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("deliveryCost")
        var deliveryCost: DeliveryCost? = DeliveryCost(),
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("name")
        var name: String? = ""
    ) {
        data class DeliveryCost(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )
    }

    data class DeliveryOrderGroup(
        @SerializedName("deliveryAddress")
        var deliveryAddress: DeliveryAddress? = DeliveryAddress(),
        @SerializedName("entries")
        var entries: List<Entry?>? = listOf(),
        @SerializedName("quantity")
        var quantity: Double?=0.0,
        @SerializedName("totalPriceWithTax")
        var totalPriceWithTax: TotalPriceWithTax? = TotalPriceWithTax()
    ) {
        data class DeliveryAddress(
            @SerializedName("addressName")
            var addressName: String? = "",
            @SerializedName("area")
            var area: Area? = Area(),
            @SerializedName("cellphone")
            var cellphone: String? = "",
            @SerializedName("city")
            var city: City? = City(),
            @SerializedName("companyName")
            var companyName: String? = "",
            @SerializedName("country")
            var country: Country? = Country(),
            @SerializedName("defaultAddress")
            var defaultAddress: Boolean? = false,
            @SerializedName("district")
            var district: String? = "",
            @SerializedName("email")
            var email: String? = "",
            @SerializedName("firstName")
            var firstName: String? = "",
            @SerializedName("formattedAddress")
            var formattedAddress: String? = "",
            @SerializedName("id")
            var id: String? = "",
            @SerializedName("lastName")
            var lastName: String? = "",
            @SerializedName("line1")
            var line1: String? = "",
            @SerializedName("line2")
            var line2: String? = "",
            @SerializedName("mobileCountry")
            var mobileCountry: MobileCountry? = MobileCountry(),
            @SerializedName("mobileNumber")
            var mobileNumber: String? = "",
            @SerializedName("nearestLandmark")
            var nearestLandmark: String? = "",
            @SerializedName("phone")
            var phone: String? = "",
            @SerializedName("postalCode")
            var postalCode: String? = "",
            @SerializedName("region")
            var region: Region? = Region(),
            @SerializedName("shippingAddress")
            var shippingAddress: Boolean? = false,
            @SerializedName("title")
            var title: String? = "",
            @SerializedName("titleCode")
            var titleCode: String? = "",
            @SerializedName("town")
            var town: String? = "",
            @SerializedName("visibleInAddressBook")
            var visibleInAddressBook: Boolean? = false
        ) {
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class City(
                @SerializedName("areas")
                var areas: List<Area?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Country(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class MobileCountry(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class Region(
                @SerializedName("countryIso")
                var countryIso: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("isocodeShort")
                var isocodeShort: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )
        }

        data class Entry(
            @SerializedName("basePrice")
            var basePrice: BasePrice? = BasePrice(),
            @SerializedName("cancellableQuantity")
            var cancellableQuantity: Double?=0.0,
            @SerializedName("cancelledItemsPrice")
            var cancelledItemsPrice: CancelledItemsPrice? = CancelledItemsPrice(),
            @SerializedName("configurationInfos")
            var configurationInfos: List<ConfigurationInfo?>? = listOf(),
            @SerializedName("deliveryMode")
            var deliveryMode: DeliveryMode? = DeliveryMode(),
            @SerializedName("deliveryPointOfService")
            var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
            @SerializedName("entryNumber")
            var entryNumber: Double?=0.0,
            @SerializedName("product")
            var product: Product? = Product(),
            @SerializedName("quantity")
            var quantity: Double?=0.0,
            @SerializedName("quantityAllocated")
            var quantityAllocated: Double?=0.0,
            @SerializedName("quantityCancelled")
            var quantityCancelled: Double?=0.0,
            @SerializedName("quantityPending")
            var quantityPending: Double?=0.0,
            @SerializedName("quantityReturned")
            var quantityReturned: Double?=0.0,
            @SerializedName("quantityShipped")
            var quantityShipped: Double?=0.0,
            @SerializedName("quantityUnallocated")
            var quantityUnallocated: Double?=0.0,
            @SerializedName("returnableQuantity")
            var returnableQuantity: Double?=0.0,
            @SerializedName("returnedItemsPrice")
            var returnedItemsPrice: ReturnedItemsPrice? = ReturnedItemsPrice(),
            @SerializedName("totalPrice")
            var totalPrice: TotalPrice? = TotalPrice(),
            @SerializedName("updateable")
            var updateable: Boolean? = false,
            @SerializedName("url")
            var url: String? = ""
        ) {
            data class BasePrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class CancelledItemsPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class ConfigurationInfo(
                @SerializedName("configurationLabel")
                var configurationLabel: String? = "",
                @SerializedName("configurationValue")
                var configurationValue: String? = "",
                @SerializedName("configuratorType")
                var configuratorType: String? = "",
                @SerializedName("status")
                var status: String? = ""
            )

            data class DeliveryMode(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("deliveryCost")
                var deliveryCost: DeliveryCost? = DeliveryCost(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class DeliveryCost(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class DeliveryPointOfService(
                @SerializedName("address")
                var address: Address? = Address(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("displayName")
                var displayName: String? = "",
                @SerializedName("distanceKm")
                var distanceKm: Double?=0.0,
                @SerializedName("features")
                var features: Features? = Features(),
                @SerializedName("formattedDistance")
                var formattedDistance: String? = "",
                @SerializedName("geoPoint")
                var geoPoint: GeoPoint? = GeoPoint(),
                @SerializedName("mapIcon")
                var mapIcon: MapIcon? = MapIcon(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("openingHours")
                var openingHours: OpeningHours? = OpeningHours(),
                @SerializedName("storeContent")
                var storeContent: String? = "",
                @SerializedName("storeImages")
                var storeImages: List<StoreImage?>? = listOf(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("warehouseCodes")
                var warehouseCodes: List<String?>? = listOf()
            ) {
                data class Address(
                    @SerializedName("addressName")
                    var addressName: String? = "",
                    @SerializedName("area")
                    var area: Area? = Area(),
                    @SerializedName("cellphone")
                    var cellphone: String? = "",
                    @SerializedName("city")
                    var city: City? = City(),
                    @SerializedName("companyName")
                    var companyName: String? = "",
                    @SerializedName("country")
                    var country: Country? = Country(),
                    @SerializedName("defaultAddress")
                    var defaultAddress: Boolean? = false,
                    @SerializedName("district")
                    var district: String? = "",
                    @SerializedName("email")
                    var email: String? = "",
                    @SerializedName("firstName")
                    var firstName: String? = "",
                    @SerializedName("formattedAddress")
                    var formattedAddress: String? = "",
                    @SerializedName("id")
                    var id: String? = "",
                    @SerializedName("lastName")
                    var lastName: String? = "",
                    @SerializedName("line1")
                    var line1: String? = "",
                    @SerializedName("line2")
                    var line2: String? = "",
                    @SerializedName("mobileCountry")
                    var mobileCountry: MobileCountry? = MobileCountry(),
                    @SerializedName("mobileNumber")
                    var mobileNumber: String? = "",
                    @SerializedName("nearestLandmark")
                    var nearestLandmark: String? = "",
                    @SerializedName("phone")
                    var phone: String? = "",
                    @SerializedName("postalCode")
                    var postalCode: String? = "",
                    @SerializedName("region")
                    var region: Region? = Region(),
                    @SerializedName("shippingAddress")
                    var shippingAddress: Boolean? = false,
                    @SerializedName("title")
                    var title: String? = "",
                    @SerializedName("titleCode")
                    var titleCode: String? = "",
                    @SerializedName("town")
                    var town: String? = "",
                    @SerializedName("visibleInAddressBook")
                    var visibleInAddressBook: Boolean? = false
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class City(
                        @SerializedName("areas")
                        var areas: List<Area?>? = listOf(),
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    ) {
                        data class Area(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }

                    data class Country(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class MobileCountry(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class Region(
                        @SerializedName("countryIso")
                        var countryIso: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("isocodeShort")
                        var isocodeShort: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Features(
                    @SerializedName("additionalProp1")
                    var additionalProp1: String? = "",
                    @SerializedName("additionalProp2")
                    var additionalProp2: String? = "",
                    @SerializedName("additionalProp3")
                    var additionalProp3: String? = ""
                )

                data class GeoPoint(
                    @SerializedName("latitude")
                    var latitude: Double?=0.0,
                    @SerializedName("longitude")
                    var longitude: Double?=0.0
                )

                data class MapIcon(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class OpeningHours(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("specialDayOpeningList")
                    var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                    @SerializedName("weekDayOpeningList")
                    var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
                ) {
                    data class SpecialDayOpening(
                        @SerializedName("closed")
                        var closed: Boolean? = false,
                        @SerializedName("closingTime")
                        var closingTime: ClosingTime? = ClosingTime(),
                        @SerializedName("comment")
                        var comment: String? = "",
                        @SerializedName("date")
                        var date: String? = "",
                        @SerializedName("formattedDate")
                        var formattedDate: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("openingTime")
                        var openingTime: OpeningTime? = OpeningTime()
                    ) {
                        data class ClosingTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )

                        data class OpeningTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )
                    }

                    data class WeekDayOpening(
                        @SerializedName("closed")
                        var closed: Boolean? = false,
                        @SerializedName("closingTime")
                        var closingTime: ClosingTime? = ClosingTime(),
                        @SerializedName("openingTime")
                        var openingTime: OpeningTime? = OpeningTime(),
                        @SerializedName("weekDay")
                        var weekDay: String? = ""
                    ) {
                        data class ClosingTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )

                        data class OpeningTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )
                    }
                }

                data class StoreImage(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )
            }

            data class Product(
                @SerializedName("availableForPickup")
                var availableForPickup: Boolean? = false,
                @SerializedName("nutritionFacts")
                var nutritionFacts: String? = "",
                @SerializedName("averageRating")
                var averageRating: Double?=0.0,
                @SerializedName("baseOptions")
                var baseOptions: List<BaseOption?>? = listOf(),
                @SerializedName("baseProduct")
                var baseProduct: String? = "",
                @SerializedName("categories")
                var categories: List<Category?>? = listOf(),
                @SerializedName("classifications")
                var classifications: List<Classification?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("configurable")
                var configurable: Boolean? = false,
                @SerializedName("configuratorType")
                var configuratorType: String? = "",
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("futureStocks")
                var futureStocks: List<FutureStock?>? = listOf(),
                @SerializedName("images")
                var images: List<Image?>? = listOf(),
                @SerializedName("manufacturer")
                var manufacturer: String? = "",
                @SerializedName("multidimensional")
                var multidimensional: Boolean? = false,
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("numberOfReviews")
                var numberOfReviews: Double?=0.0,
                @SerializedName("potentialPromotions")
                var potentialPromotions: List<PotentialPromotion?>? = listOf(),
                @SerializedName("price")
                var price: Price? = Price(),
                @SerializedName("priceRange")
                var priceRange: PriceRange? = PriceRange(),
                @SerializedName("productReferences")
                var productReferences: List<ProductReference?>? = listOf(),
                @SerializedName("purchasable")
                var purchasable: Boolean? = false,
                @SerializedName("reviews")
                var reviews: List<Review?>? = listOf(),
                @SerializedName("stock")
                var stock: Stock? = Stock(),
                @SerializedName("summary")
                var summary: String? = "",
                @SerializedName("tags")
                var tags: List<String?>? = listOf(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("variantMatrix")
                var variantMatrix: List<VariantMatrix?>? = listOf(),
                @SerializedName("variantOptions")
                var variantOptions: List<VariantOption?>? = listOf(),
                @SerializedName("variantType")
                var variantType: String? = "",
                @SerializedName("volumePrices")
                var volumePrices: List<VolumePrice?>? = listOf(),
                @SerializedName("volumePricesFlag")
                var volumePricesFlag: Boolean? = false
            ) {
                data class BaseOption(
                    @SerializedName("options")
                    var options: List<Option?>? = listOf(),
                    @SerializedName("selected")
                    var selected: Selected? = Selected(),
                    @SerializedName("variantType")
                    var variantType: String? = ""
                ) {
                    data class Option(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }

                    data class Selected(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }
                }

                data class Category(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("image")
                    var image: Image? = Image(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("plpPicture")
                    var plpPicture: PlpPicture? = PlpPicture(),
                    @SerializedName("plpPictureResponsive")
                    var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
                    @SerializedName("url")
                    var url: String? = ""
                ) {
                    data class Image(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class PlpPicture(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class PlpPictureResponsive(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )
                }

                data class Classification(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("features")
                    var features: List<Feature?>? = listOf(),
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Feature(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("comparable")
                        var comparable: Boolean? = false,
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("featureUnit")
                        var featureUnit: FeatureUnit? = FeatureUnit(),
                        @SerializedName("featureValues")
                        var featureValues: List<FeatureValue?>? = listOf(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("range")
                        var range: Boolean? = false,
                        @SerializedName("type")
                        var type: String? = ""
                    ) {
                        data class FeatureUnit(
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("symbol")
                            var symbol: String? = "",
                            @SerializedName("unitType")
                            var unitType: String? = ""
                        )

                        data class FeatureValue(
                            @SerializedName("value")
                            var value: String? = ""
                        )
                    }
                }

                data class FutureStock(
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("stock")
                    var stock: Stock? = Stock()
                ) {
                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )
                }

                data class Image(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PotentialPromotion(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("couldFireMessages")
                    var couldFireMessages: List<String?>? = listOf(),
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("enabled")
                    var enabled: Boolean? = false,
                    @SerializedName("endDate")
                    var endDate: String? = "",
                    @SerializedName("firedMessages")
                    var firedMessages: List<String?>? = listOf(),
                    @SerializedName("priority")
                    var priority: Double?=0.0,
                    @SerializedName("productBanner")
                    var productBanner: ProductBanner? = ProductBanner(),
                    @SerializedName("promotionGroup")
                    var promotionGroup: String? = "",
                    @SerializedName("promotionType")
                    var promotionType: String? = "",
                    @SerializedName("restrictions")
                    var restrictions: List<Restriction?>? = listOf(),
                    @SerializedName("startDate")
                    var startDate: String? = "",
                    @SerializedName("title")
                    var title: String? = ""
                ) {
                    data class ProductBanner(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class Restriction(
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("restrictionType")
                        var restrictionType: String? = ""
                    )
                }

                data class Price(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class PriceRange(
                    @SerializedName("maxPrice")
                    var maxPrice: MaxPrice? = MaxPrice(),
                    @SerializedName("minPrice")
                    var minPrice: MinPrice? = MinPrice()
                ) {
                    data class MaxPrice(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class MinPrice(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )
                }

                data class ProductReference(
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("preselected")
                    var preselected: Boolean? = false,
                    @SerializedName("quantity")
                    var quantity: Double?=0.0,
                    @SerializedName("referenceType")
                    var referenceType: String? = ""
                )

                data class Review(
                    @SerializedName("alias")
                    var alias: String? = "",
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("headline")
                    var headline: String? = "",
                    @SerializedName("id")
                    var id: String? = "",
                    @SerializedName("principal")
                    var principal: Principal? = Principal(),
                    @SerializedName("rating")
                    var rating: Double?=0.0
                ) {
                    data class Principal(
                        @SerializedName("currency")
                        var currency: Currency? = Currency(),
                        @SerializedName("customerId")
                        var customerId: String? = "",
                        @SerializedName("deactivationDate")
                        var deactivationDate: String? = "",
                        @SerializedName("defaultAddress")
                        var defaultAddress: DefaultAddress? = DefaultAddress(),
                        @SerializedName("displayUid")
                        var displayUid: String? = "",
                        @SerializedName("firstName")
                        var firstName: String? = "",
                        @SerializedName("language")
                        var language: Language? = Language(),
                        @SerializedName("lastName")
                        var lastName: String? = "",
                        @SerializedName("mobileCountry")
                        var mobileCountry: MobileCountry? = MobileCountry(),
                        @SerializedName("mobileNumber")
                        var mobileNumber: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("title")
                        var title: String? = "",
                        @SerializedName("titleCode")
                        var titleCode: String? = "",
                        @SerializedName("uid")
                        var uid: String? = ""
                    ) {
                        data class Currency(
                            @SerializedName("active")
                            var active: Boolean? = false,
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("symbol")
                            var symbol: String? = ""
                        )

                        data class DefaultAddress(
                            @SerializedName("addressName")
                            var addressName: String? = "",
                            @SerializedName("area")
                            var area: Area? = Area(),
                            @SerializedName("cellphone")
                            var cellphone: String? = "",
                            @SerializedName("city")
                            var city: City? = City(),
                            @SerializedName("companyName")
                            var companyName: String? = "",
                            @SerializedName("country")
                            var country: Country? = Country(),
                            @SerializedName("defaultAddress")
                            var defaultAddress: Boolean? = false,
                            @SerializedName("district")
                            var district: String? = "",
                            @SerializedName("email")
                            var email: String? = "",
                            @SerializedName("firstName")
                            var firstName: String? = "",
                            @SerializedName("formattedAddress")
                            var formattedAddress: String? = "",
                            @SerializedName("id")
                            var id: String? = "",
                            @SerializedName("lastName")
                            var lastName: String? = "",
                            @SerializedName("line1")
                            var line1: String? = "",
                            @SerializedName("line2")
                            var line2: String? = "",
                            @SerializedName("mobileCountry")
                            var mobileCountry: MobileCountry? = MobileCountry(),
                            @SerializedName("mobileNumber")
                            var mobileNumber: String? = "",
                            @SerializedName("nearestLandmark")
                            var nearestLandmark: String? = "",
                            @SerializedName("phone")
                            var phone: String? = "",
                            @SerializedName("postalCode")
                            var postalCode: String? = "",
                            @SerializedName("region")
                            var region: Region? = Region(),
                            @SerializedName("shippingAddress")
                            var shippingAddress: Boolean? = false,
                            @SerializedName("title")
                            var title: String? = "",
                            @SerializedName("titleCode")
                            var titleCode: String? = "",
                            @SerializedName("town")
                            var town: String? = "",
                            @SerializedName("visibleInAddressBook")
                            var visibleInAddressBook: Boolean? = false
                        ) {
                            data class Area(
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class City(
                                @SerializedName("areas")
                                var areas: List<Area?>? = listOf(),
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            ) {
                                data class Area(
                                    @SerializedName("code")
                                    var code: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )
                            }

                            data class Country(
                                @SerializedName("isdcode")
                                var isdcode: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class MobileCountry(
                                @SerializedName("isdcode")
                                var isdcode: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class Region(
                                @SerializedName("countryIso")
                                var countryIso: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("isocodeShort")
                                var isocodeShort: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }

                        data class Language(
                            @SerializedName("active")
                            var active: Boolean? = false,
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("nativeName")
                            var nativeName: String? = ""
                        )

                        data class MobileCountry(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }
                }

                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )

                data class VariantMatrix(
                    @SerializedName("elements")
                    var elements: List<Any?>? = listOf(),
                    @SerializedName("isLeaf")
                    var isLeaf: Boolean? = false,
                    @SerializedName("parentVariantCategory")
                    var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
                    @SerializedName("variantOption")
                    var variantOption: VariantOption? = VariantOption(),
                    @SerializedName("variantValueCategory")
                    var variantValueCategory: VariantValueCategory? = VariantValueCategory()
                ) {
                    data class ParentVariantCategory(
                        @SerializedName("hasImage")
                        var hasImage: Boolean? = false,
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("priority")
                        var priority: Double?=0.0
                    )

                    data class VariantOption(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }

                    data class VariantValueCategory(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("sequence")
                        var sequence: Double?=0.0,
                        @SerializedName("superCategories")
                        var superCategories: List<SuperCategory?>? = listOf()
                    ) {
                        data class SuperCategory(
                            @SerializedName("hasImage")
                            var hasImage: Boolean? = false,
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("priority")
                            var priority: Double?=0.0
                        )
                    }
                }

                data class VariantOption(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class VolumePrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class ReturnedItemsPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class TotalPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class TotalPriceWithTax(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )
    }

    data class Entry(
        @SerializedName("basePrice")
        var basePrice: BasePrice? = BasePrice(),
        @SerializedName("cancellableQuantity")
        var cancellableQuantity: Double?=0.0,
        @SerializedName("cancelledItemsPrice")
        var cancelledItemsPrice: CancelledItemsPrice? = CancelledItemsPrice(),
        @SerializedName("configurationInfos")
        var configurationInfos: List<ConfigurationInfo?>? = listOf(),
        @SerializedName("deliveryMode")
        var deliveryMode: DeliveryMode? = DeliveryMode(),
        @SerializedName("deliveryPointOfService")
        var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
        @SerializedName("entryNumber")
        var entryNumber: Double?=0.0,
        @SerializedName("product")
        var product: Product? = Product(),
        @SerializedName("quantity")
        var quantity: Double?=0.0,
        @SerializedName("quantityAllocated")
        var quantityAllocated: Double?=0.0,
        @SerializedName("quantityCancelled")
        var quantityCancelled: Double?=0.0,
        @SerializedName("quantityPending")
        var quantityPending: Double?=0.0,
        @SerializedName("quantityReturned")
        var quantityReturned: Double?=0.0,
        @SerializedName("quantityShipped")
        var quantityShipped: Double?=0.0,
        @SerializedName("quantityUnallocated")
        var quantityUnallocated: Double?=0.0,
        @SerializedName("returnableQuantity")
        var returnableQuantity: Double?=0.0,
        @SerializedName("returnedItemsPrice")
        var returnedItemsPrice: ReturnedItemsPrice? = ReturnedItemsPrice(),
        @SerializedName("totalPrice")
        var totalPrice: TotalPrice? = TotalPrice(),
        @SerializedName("updateable")
        var updateable: Boolean? = false,
        @SerializedName("url")
        var url: String? = ""
    ) {
        data class BasePrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class CancelledItemsPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class ConfigurationInfo(
            @SerializedName("configurationLabel")
            var configurationLabel: String? = "",
            @SerializedName("configurationValue")
            var configurationValue: String? = "",
            @SerializedName("configuratorType")
            var configuratorType: String? = "",
            @SerializedName("status")
            var status: String? = ""
        )

        data class DeliveryMode(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("deliveryCost")
            var deliveryCost: DeliveryCost? = DeliveryCost(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) {
            data class DeliveryCost(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class DeliveryPointOfService(
            @SerializedName("address")
            var address: Address? = Address(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("displayName")
            var displayName: String? = "",
            @SerializedName("distanceKm")
            var distanceKm: Double?=0.0,
            @SerializedName("features")
            var features: Features? = Features(),
            @SerializedName("formattedDistance")
            var formattedDistance: String? = "",
            @SerializedName("geoPoint")
            var geoPoint: GeoPoint? = GeoPoint(),
            @SerializedName("mapIcon")
            var mapIcon: MapIcon? = MapIcon(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("openingHours")
            var openingHours: OpeningHours? = OpeningHours(),
            @SerializedName("storeContent")
            var storeContent: String? = "",
            @SerializedName("storeImages")
            var storeImages: List<StoreImage?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("warehouseCodes")
            var warehouseCodes: List<String?>? = listOf()
        ) {
            data class Address(
                @SerializedName("addressName")
                var addressName: String? = "",
                @SerializedName("area")
                var area: Area? = Area(),
                @SerializedName("cellphone")
                var cellphone: String? = "",
                @SerializedName("city")
                var city: City? = City(),
                @SerializedName("companyName")
                var companyName: String? = "",
                @SerializedName("country")
                var country: Country? = Country(),
                @SerializedName("defaultAddress")
                var defaultAddress: Boolean? = false,
                @SerializedName("district")
                var district: String? = "",
                @SerializedName("email")
                var email: String? = "",
                @SerializedName("firstName")
                var firstName: String? = "",
                @SerializedName("formattedAddress")
                var formattedAddress: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("lastName")
                var lastName: String? = "",
                @SerializedName("line1")
                var line1: String? = "",
                @SerializedName("line2")
                var line2: String? = "",
                @SerializedName("mobileCountry")
                var mobileCountry: MobileCountry? = MobileCountry(),
                @SerializedName("mobileNumber")
                var mobileNumber: String? = "",
                @SerializedName("nearestLandmark")
                var nearestLandmark: String? = "",
                @SerializedName("phone")
                var phone: String? = "",
                @SerializedName("postalCode")
                var postalCode: String? = "",
                @SerializedName("region")
                var region: Region? = Region(),
                @SerializedName("shippingAddress")
                var shippingAddress: Boolean? = false,
                @SerializedName("title")
                var title: String? = "",
                @SerializedName("titleCode")
                var titleCode: String? = "",
                @SerializedName("town")
                var town: String? = "",
                @SerializedName("visibleInAddressBook")
                var visibleInAddressBook: Boolean? = false
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class City(
                    @SerializedName("areas")
                    var areas: List<Area?>? = listOf(),
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Country(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class MobileCountry(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class Region(
                    @SerializedName("countryIso")
                    var countryIso: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("isocodeShort")
                    var isocodeShort: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Features(
                @SerializedName("additionalProp1")
                var additionalProp1: String? = "",
                @SerializedName("additionalProp2")
                var additionalProp2: String? = "",
                @SerializedName("additionalProp3")
                var additionalProp3: String? = ""
            )

            data class GeoPoint(
                @SerializedName("latitude")
                var latitude: Double?=0.0,
                @SerializedName("longitude")
                var longitude: Double?=0.0
            )

            data class MapIcon(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class OpeningHours(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("specialDayOpeningList")
                var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                @SerializedName("weekDayOpeningList")
                var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
            ) {
                data class SpecialDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime()
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }

                data class WeekDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime(),
                    @SerializedName("weekDay")
                    var weekDay: String? = ""
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }
            }

            data class StoreImage(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )
        }

        data class Product(
            @SerializedName("availableForPickup")
            var availableForPickup: Boolean? = false,
            @SerializedName("nutritionFacts")
            var nutritionFacts: String? = "",
            @SerializedName("averageRating")
            var averageRating: Double?=0.0,
            @SerializedName("baseOptions")
            var baseOptions: List<BaseOption?>? = listOf(),
            @SerializedName("baseProduct")
            var baseProduct: String? = "",
            @SerializedName("categories")
            var categories: List<Category?>? = listOf(),
            @SerializedName("classifications")
            var classifications: List<Classification?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("configurable")
            var configurable: Boolean? = false,
            @SerializedName("configuratorType")
            var configuratorType: String? = "",
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("futureStocks")
            var futureStocks: List<FutureStock?>? = listOf(),
            @SerializedName("images")
            var images: List<Image?>? = listOf(),
            @SerializedName("manufacturer")
            var manufacturer: String? = "",
            @SerializedName("multidimensional")
            var multidimensional: Boolean? = false,
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("numberOfReviews")
            var numberOfReviews: Double?=0.0,
            @SerializedName("potentialPromotions")
            var potentialPromotions: List<PotentialPromotion?>? = listOf(),
            @SerializedName("price")
            var price: Price? = Price(),
            @SerializedName("priceRange")
            var priceRange: PriceRange? = PriceRange(),
            @SerializedName("productReferences")
            var productReferences: List<ProductReference?>? = listOf(),
            @SerializedName("purchasable")
            var purchasable: Boolean? = false,
            @SerializedName("reviews")
            var reviews: List<Review?>? = listOf(),
            @SerializedName("stock")
            var stock: Stock? = Stock(),
            @SerializedName("summary")
            var summary: String? = "",
            @SerializedName("tags")
            var tags: List<String?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("variantMatrix")
            var variantMatrix: List<VariantMatrix?>? = listOf(),
            @SerializedName("variantOptions")
            var variantOptions: List<VariantOption?>? = listOf(),
            @SerializedName("variantType")
            var variantType: String? = "",
            @SerializedName("volumePrices")
            var volumePrices: List<VolumePrice?>? = listOf(),
            @SerializedName("volumePricesFlag")
            var volumePricesFlag: Boolean? = false
        ) {
            data class BaseOption(
                @SerializedName("options")
                var options: List<Option?>? = listOf(),
                @SerializedName("selected")
                var selected: Selected? = Selected(),
                @SerializedName("variantType")
                var variantType: String? = ""
            ) {
                data class Option(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class Selected(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }
            }

            data class Category(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("image")
                var image: Image? = Image(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("plpPicture")
                var plpPicture: PlpPicture? = PlpPicture(),
                @SerializedName("plpPictureResponsive")
                var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
                @SerializedName("url")
                var url: String? = ""
            ) {
                data class Image(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PlpPicture(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PlpPictureResponsive(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )
            }

            data class Classification(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("features")
                var features: List<Feature?>? = listOf(),
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class Feature(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("comparable")
                    var comparable: Boolean? = false,
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("featureUnit")
                    var featureUnit: FeatureUnit? = FeatureUnit(),
                    @SerializedName("featureValues")
                    var featureValues: List<FeatureValue?>? = listOf(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("range")
                    var range: Boolean? = false,
                    @SerializedName("type")
                    var type: String? = ""
                ) {
                    data class FeatureUnit(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("symbol")
                        var symbol: String? = "",
                        @SerializedName("unitType")
                        var unitType: String? = ""
                    )

                    data class FeatureValue(
                        @SerializedName("value")
                        var value: String? = ""
                    )
                }
            }

            data class FutureStock(
                @SerializedName("date")
                var date: String? = "",
                @SerializedName("formattedDate")
                var formattedDate: String? = "",
                @SerializedName("stock")
                var stock: Stock? = Stock()
            ) {
                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )
            }

            data class Image(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class PotentialPromotion(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("couldFireMessages")
                var couldFireMessages: List<String?>? = listOf(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("enabled")
                var enabled: Boolean? = false,
                @SerializedName("endDate")
                var endDate: String? = "",
                @SerializedName("firedMessages")
                var firedMessages: List<String?>? = listOf(),
                @SerializedName("priority")
                var priority: Double?=0.0,
                @SerializedName("productBanner")
                var productBanner: ProductBanner? = ProductBanner(),
                @SerializedName("promotionGroup")
                var promotionGroup: String? = "",
                @SerializedName("promotionType")
                var promotionType: String? = "",
                @SerializedName("restrictions")
                var restrictions: List<Restriction?>? = listOf(),
                @SerializedName("startDate")
                var startDate: String? = "",
                @SerializedName("title")
                var title: String? = ""
            ) {
                data class ProductBanner(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class Restriction(
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("restrictionType")
                    var restrictionType: String? = ""
                )
            }

            data class Price(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class PriceRange(
                @SerializedName("maxPrice")
                var maxPrice: MaxPrice? = MaxPrice(),
                @SerializedName("minPrice")
                var minPrice: MinPrice? = MinPrice()
            ) {
                data class MaxPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class MinPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class ProductReference(
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("preselected")
                var preselected: Boolean? = false,
                @SerializedName("quantity")
                var quantity: Double?=0.0,
                @SerializedName("referenceType")
                var referenceType: String? = ""
            )

            data class Review(
                @SerializedName("alias")
                var alias: String? = "",
                @SerializedName("comment")
                var comment: String? = "",
                @SerializedName("date")
                var date: String? = "",
                @SerializedName("headline")
                var headline: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("principal")
                var principal: Principal? = Principal(),
                @SerializedName("rating")
                var rating: Double?=0.0
            ) {
                data class Principal(
                    @SerializedName("currency")
                    var currency: Currency? = Currency(),
                    @SerializedName("customerId")
                    var customerId: String? = "",
                    @SerializedName("deactivationDate")
                    var deactivationDate: String? = "",
                    @SerializedName("defaultAddress")
                    var defaultAddress: DefaultAddress? = DefaultAddress(),
                    @SerializedName("displayUid")
                    var displayUid: String? = "",
                    @SerializedName("firstName")
                    var firstName: String? = "",
                    @SerializedName("language")
                    var language: Language? = Language(),
                    @SerializedName("lastName")
                    var lastName: String? = "",
                    @SerializedName("mobileCountry")
                    var mobileCountry: MobileCountry? = MobileCountry(),
                    @SerializedName("mobileNumber")
                    var mobileNumber: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("title")
                    var title: String? = "",
                    @SerializedName("titleCode")
                    var titleCode: String? = "",
                    @SerializedName("uid")
                    var uid: String? = ""
                ) {
                    data class Currency(
                        @SerializedName("active")
                        var active: Boolean? = false,
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("symbol")
                        var symbol: String? = ""
                    )

                    data class DefaultAddress(
                        @SerializedName("addressName")
                        var addressName: String? = "",
                        @SerializedName("area")
                        var area: Area? = Area(),
                        @SerializedName("cellphone")
                        var cellphone: String? = "",
                        @SerializedName("city")
                        var city: City? = City(),
                        @SerializedName("companyName")
                        var companyName: String? = "",
                        @SerializedName("country")
                        var country: Country? = Country(),
                        @SerializedName("defaultAddress")
                        var defaultAddress: Boolean? = false,
                        @SerializedName("district")
                        var district: String? = "",
                        @SerializedName("email")
                        var email: String? = "",
                        @SerializedName("firstName")
                        var firstName: String? = "",
                        @SerializedName("formattedAddress")
                        var formattedAddress: String? = "",
                        @SerializedName("id")
                        var id: String? = "",
                        @SerializedName("lastName")
                        var lastName: String? = "",
                        @SerializedName("line1")
                        var line1: String? = "",
                        @SerializedName("line2")
                        var line2: String? = "",
                        @SerializedName("mobileCountry")
                        var mobileCountry: MobileCountry? = MobileCountry(),
                        @SerializedName("mobileNumber")
                        var mobileNumber: String? = "",
                        @SerializedName("nearestLandmark")
                        var nearestLandmark: String? = "",
                        @SerializedName("phone")
                        var phone: String? = "",
                        @SerializedName("postalCode")
                        var postalCode: String? = "",
                        @SerializedName("region")
                        var region: Region? = Region(),
                        @SerializedName("shippingAddress")
                        var shippingAddress: Boolean? = false,
                        @SerializedName("title")
                        var title: String? = "",
                        @SerializedName("titleCode")
                        var titleCode: String? = "",
                        @SerializedName("town")
                        var town: String? = "",
                        @SerializedName("visibleInAddressBook")
                        var visibleInAddressBook: Boolean? = false
                    ) {
                        data class Area(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class City(
                            @SerializedName("areas")
                            var areas: List<Area?>? = listOf(),
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        ) {
                            data class Area(
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }

                        data class Country(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class MobileCountry(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class Region(
                            @SerializedName("countryIso")
                            var countryIso: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("isocodeShort")
                            var isocodeShort: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }

                    data class Language(
                        @SerializedName("active")
                        var active: Boolean? = false,
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("nativeName")
                        var nativeName: String? = ""
                    )

                    data class MobileCountry(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }
            }

            data class Stock(
                @SerializedName("stockLevel")
                var stockLevel: Double?=0.0,
                @SerializedName("stockLevelStatus")
                var stockLevelStatus: String? = ""
            )

            data class VariantMatrix(
                @SerializedName("elements")
                var elements: List<Any?>? = listOf(),
                @SerializedName("isLeaf")
                var isLeaf: Boolean? = false,
                @SerializedName("parentVariantCategory")
                var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
                @SerializedName("variantOption")
                var variantOption: VariantOption? = VariantOption(),
                @SerializedName("variantValueCategory")
                var variantValueCategory: VariantValueCategory? = VariantValueCategory()
            ) {
                data class ParentVariantCategory(
                    @SerializedName("hasImage")
                    var hasImage: Boolean? = false,
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("priority")
                    var priority: Double?=0.0
                )

                data class VariantOption(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class VariantValueCategory(
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("sequence")
                    var sequence: Double?=0.0,
                    @SerializedName("superCategories")
                    var superCategories: List<SuperCategory?>? = listOf()
                ) {
                    data class SuperCategory(
                        @SerializedName("hasImage")
                        var hasImage: Boolean? = false,
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("priority")
                        var priority: Double?=0.0
                    )
                }
            }

            data class VariantOption(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("priceData")
                var priceData: PriceData? = PriceData(),
                @SerializedName("stock")
                var stock: Stock? = Stock(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("variantOptionQualifiers")
                var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
            ) {
                data class PriceData(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )

                data class VariantOptionQualifier(
                    @SerializedName("image")
                    var image: Image? = Image(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("qualifier")
                    var qualifier: String? = "",
                    @SerializedName("value")
                    var value: String? = ""
                ) {
                    data class Image(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )
                }
            }

            data class VolumePrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class ReturnedItemsPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class TotalPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )
    }

    data class OrderDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class PaymentAddress(
        @SerializedName("addressName")
        var addressName: String? = "",
        @SerializedName("area")
        var area: Area? = Area(),
        @SerializedName("cellphone")
        var cellphone: String? = "",
        @SerializedName("city")
        var city: City? = City(),
        @SerializedName("companyName")
        var companyName: String? = "",
        @SerializedName("country")
        var country: Country? = Country(),
        @SerializedName("defaultAddress")
        var defaultAddress: Boolean? = false,
        @SerializedName("district")
        var district: String? = "",
        @SerializedName("email")
        var email: String? = "",
        @SerializedName("firstName")
        var firstName: String? = "",
        @SerializedName("formattedAddress")
        var formattedAddress: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("lastName")
        var lastName: String? = "",
        @SerializedName("line1")
        var line1: String? = "",
        @SerializedName("line2")
        var line2: String? = "",
        @SerializedName("mobileCountry")
        var mobileCountry: MobileCountry? = MobileCountry(),
        @SerializedName("mobileNumber")
        var mobileNumber: String? = "",
        @SerializedName("nearestLandmark")
        var nearestLandmark: String? = "",
        @SerializedName("phone")
        var phone: String? = "",
        @SerializedName("postalCode")
        var postalCode: String? = "",
        @SerializedName("region")
        var region: Region? = Region(),
        @SerializedName("shippingAddress")
        var shippingAddress: Boolean? = false,
        @SerializedName("title")
        var title: String? = "",
        @SerializedName("titleCode")
        var titleCode: String? = "",
        @SerializedName("town")
        var town: String? = "",
        @SerializedName("visibleInAddressBook")
        var visibleInAddressBook: Boolean? = false
    ) {
        data class Area(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class City(
            @SerializedName("areas")
            var areas: List<Area?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) {
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )
        }

        data class Country(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class MobileCountry(
            @SerializedName("isdcode")
            var isdcode: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )

        data class Region(
            @SerializedName("countryIso")
            var countryIso: String? = "",
            @SerializedName("isocode")
            var isocode: String? = "",
            @SerializedName("isocodeShort")
            var isocodeShort: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )
    }

    data class PaymentInfo(
        @SerializedName("accountHolderName")
        var accountHolderName: String? = "",
        @SerializedName("billingAddress")
        var billingAddress: BillingAddress? = BillingAddress(),
        @SerializedName("cardNumber")
        var cardNumber: String? = "",
        @SerializedName("cardType")
        var cardType: CardType? = CardType(),
        @SerializedName("defaultPayment")
        var defaultPayment: Boolean? = false,
        @SerializedName("expiryMonth")
        var expiryMonth: String? = "",
        @SerializedName("expiryYear")
        var expiryYear: String? = "",
        @SerializedName("id")
        var id: String? = "",
        @SerializedName("issueNumber")
        var issueNumber: String? = "",
        @SerializedName("saved")
        var saved: Boolean? = false,
        @SerializedName("startMonth")
        var startMonth: String? = "",
        @SerializedName("startYear")
        var startYear: String? = "",
        @SerializedName("subscriptionId")
        var subscriptionId: String? = ""
    ) {
        data class BillingAddress(
            @SerializedName("addressName")
            var addressName: String? = "",
            @SerializedName("area")
            var area: Area? = Area(),
            @SerializedName("cellphone")
            var cellphone: String? = "",
            @SerializedName("city")
            var city: City? = City(),
            @SerializedName("companyName")
            var companyName: String? = "",
            @SerializedName("country")
            var country: Country? = Country(),
            @SerializedName("defaultAddress")
            var defaultAddress: Boolean? = false,
            @SerializedName("district")
            var district: String? = "",
            @SerializedName("email")
            var email: String? = "",
            @SerializedName("firstName")
            var firstName: String? = "",
            @SerializedName("formattedAddress")
            var formattedAddress: String? = "",
            @SerializedName("id")
            var id: String? = "",
            @SerializedName("lastName")
            var lastName: String? = "",
            @SerializedName("line1")
            var line1: String? = "",
            @SerializedName("line2")
            var line2: String? = "",
            @SerializedName("mobileCountry")
            var mobileCountry: MobileCountry? = MobileCountry(),
            @SerializedName("mobileNumber")
            var mobileNumber: String? = "",
            @SerializedName("nearestLandmark")
            var nearestLandmark: String? = "",
            @SerializedName("phone")
            var phone: String? = "",
            @SerializedName("postalCode")
            var postalCode: String? = "",
            @SerializedName("region")
            var region: Region? = Region(),
            @SerializedName("shippingAddress")
            var shippingAddress: Boolean? = false,
            @SerializedName("title")
            var title: String? = "",
            @SerializedName("titleCode")
            var titleCode: String? = "",
            @SerializedName("town")
            var town: String? = "",
            @SerializedName("visibleInAddressBook")
            var visibleInAddressBook: Boolean? = false
        ) {
            data class Area(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class City(
                @SerializedName("areas")
                var areas: List<Area?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Country(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class MobileCountry(
                @SerializedName("isdcode")
                var isdcode: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )

            data class Region(
                @SerializedName("countryIso")
                var countryIso: String? = "",
                @SerializedName("isocode")
                var isocode: String? = "",
                @SerializedName("isocodeShort")
                var isocodeShort: String? = "",
                @SerializedName("name")
                var name: String? = ""
            )
        }

        data class CardType(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )
    }

    data class PaymentMode(
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("name")
        var name: String? = ""
    )

    data class PickupOrderGroup(
        @SerializedName("deliveryPointOfService")
        var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
        @SerializedName("distance")
        var distance: Double?=0.0,
        @SerializedName("entries")
        var entries: List<Entry?>? = listOf(),
        @SerializedName("quantity")
        var quantity: Double?=0.0,
        @SerializedName("totalPriceWithTax")
        var totalPriceWithTax: TotalPriceWithTax? = TotalPriceWithTax()
    ) {
        data class DeliveryPointOfService(
            @SerializedName("address")
            var address: Address? = Address(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("displayName")
            var displayName: String? = "",
            @SerializedName("distanceKm")
            var distanceKm: Double?=0.0,
            @SerializedName("features")
            var features: Features? = Features(),
            @SerializedName("formattedDistance")
            var formattedDistance: String? = "",
            @SerializedName("geoPoint")
            var geoPoint: GeoPoint? = GeoPoint(),
            @SerializedName("mapIcon")
            var mapIcon: MapIcon? = MapIcon(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("openingHours")
            var openingHours: OpeningHours? = OpeningHours(),
            @SerializedName("storeContent")
            var storeContent: String? = "",
            @SerializedName("storeImages")
            var storeImages: List<StoreImage?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("warehouseCodes")
            var warehouseCodes: List<String?>? = listOf()
        ) {
            data class Address(
                @SerializedName("addressName")
                var addressName: String? = "",
                @SerializedName("area")
                var area: Area? = Area(),
                @SerializedName("cellphone")
                var cellphone: String? = "",
                @SerializedName("city")
                var city: City? = City(),
                @SerializedName("companyName")
                var companyName: String? = "",
                @SerializedName("country")
                var country: Country? = Country(),
                @SerializedName("defaultAddress")
                var defaultAddress: Boolean? = false,
                @SerializedName("district")
                var district: String? = "",
                @SerializedName("email")
                var email: String? = "",
                @SerializedName("firstName")
                var firstName: String? = "",
                @SerializedName("formattedAddress")
                var formattedAddress: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("lastName")
                var lastName: String? = "",
                @SerializedName("line1")
                var line1: String? = "",
                @SerializedName("line2")
                var line2: String? = "",
                @SerializedName("mobileCountry")
                var mobileCountry: MobileCountry? = MobileCountry(),
                @SerializedName("mobileNumber")
                var mobileNumber: String? = "",
                @SerializedName("nearestLandmark")
                var nearestLandmark: String? = "",
                @SerializedName("phone")
                var phone: String? = "",
                @SerializedName("postalCode")
                var postalCode: String? = "",
                @SerializedName("region")
                var region: Region? = Region(),
                @SerializedName("shippingAddress")
                var shippingAddress: Boolean? = false,
                @SerializedName("title")
                var title: String? = "",
                @SerializedName("titleCode")
                var titleCode: String? = "",
                @SerializedName("town")
                var town: String? = "",
                @SerializedName("visibleInAddressBook")
                var visibleInAddressBook: Boolean? = false
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class City(
                    @SerializedName("areas")
                    var areas: List<Area?>? = listOf(),
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Country(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class MobileCountry(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class Region(
                    @SerializedName("countryIso")
                    var countryIso: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("isocodeShort")
                    var isocodeShort: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Features(
                @SerializedName("additionalProp1")
                var additionalProp1: String? = "",
                @SerializedName("additionalProp2")
                var additionalProp2: String? = "",
                @SerializedName("additionalProp3")
                var additionalProp3: String? = ""
            )

            data class GeoPoint(
                @SerializedName("latitude")
                var latitude: Double?=0.0,
                @SerializedName("longitude")
                var longitude: Double?=0.0
            )

            data class MapIcon(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class OpeningHours(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("specialDayOpeningList")
                var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                @SerializedName("weekDayOpeningList")
                var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
            ) {
                data class SpecialDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime()
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }

                data class WeekDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime(),
                    @SerializedName("weekDay")
                    var weekDay: String? = ""
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }
            }

            data class StoreImage(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )
        }

        data class Entry(
            @SerializedName("basePrice")
            var basePrice: BasePrice? = BasePrice(),
            @SerializedName("cancellableQuantity")
            var cancellableQuantity: Double?=0.0,
            @SerializedName("cancelledItemsPrice")
            var cancelledItemsPrice: CancelledItemsPrice? = CancelledItemsPrice(),
            @SerializedName("configurationInfos")
            var configurationInfos: List<ConfigurationInfo?>? = listOf(),
            @SerializedName("deliveryMode")
            var deliveryMode: DeliveryMode? = DeliveryMode(),
            @SerializedName("deliveryPointOfService")
            var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
            @SerializedName("entryNumber")
            var entryNumber: Double?=0.0,
            @SerializedName("product")
            var product: Product? = Product(),
            @SerializedName("quantity")
            var quantity: Double?=0.0,
            @SerializedName("quantityAllocated")
            var quantityAllocated: Double?=0.0,
            @SerializedName("quantityCancelled")
            var quantityCancelled: Double?=0.0,
            @SerializedName("quantityPending")
            var quantityPending: Double?=0.0,
            @SerializedName("quantityReturned")
            var quantityReturned: Double?=0.0,
            @SerializedName("quantityShipped")
            var quantityShipped: Double?=0.0,
            @SerializedName("quantityUnallocated")
            var quantityUnallocated: Double?=0.0,
            @SerializedName("returnableQuantity")
            var returnableQuantity: Double?=0.0,
            @SerializedName("returnedItemsPrice")
            var returnedItemsPrice: ReturnedItemsPrice? = ReturnedItemsPrice(),
            @SerializedName("totalPrice")
            var totalPrice: TotalPrice? = TotalPrice(),
            @SerializedName("updateable")
            var updateable: Boolean? = false,
            @SerializedName("url")
            var url: String? = ""
        ) {
            data class BasePrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class CancelledItemsPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class ConfigurationInfo(
                @SerializedName("configurationLabel")
                var configurationLabel: String? = "",
                @SerializedName("configurationValue")
                var configurationValue: String? = "",
                @SerializedName("configuratorType")
                var configuratorType: String? = "",
                @SerializedName("status")
                var status: String? = ""
            )

            data class DeliveryMode(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("deliveryCost")
                var deliveryCost: DeliveryCost? = DeliveryCost(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class DeliveryCost(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class DeliveryPointOfService(
                @SerializedName("address")
                var address: Address? = Address(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("displayName")
                var displayName: String? = "",
                @SerializedName("distanceKm")
                var distanceKm: Double?=0.0,
                @SerializedName("features")
                var features: Features? = Features(),
                @SerializedName("formattedDistance")
                var formattedDistance: String? = "",
                @SerializedName("geoPoint")
                var geoPoint: GeoPoint? = GeoPoint(),
                @SerializedName("mapIcon")
                var mapIcon: MapIcon? = MapIcon(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("openingHours")
                var openingHours: OpeningHours? = OpeningHours(),
                @SerializedName("storeContent")
                var storeContent: String? = "",
                @SerializedName("storeImages")
                var storeImages: List<StoreImage?>? = listOf(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("warehouseCodes")
                var warehouseCodes: List<String?>? = listOf()
            ) {
                data class Address(
                    @SerializedName("addressName")
                    var addressName: String? = "",
                    @SerializedName("area")
                    var area: Area? = Area(),
                    @SerializedName("cellphone")
                    var cellphone: String? = "",
                    @SerializedName("city")
                    var city: City? = City(),
                    @SerializedName("companyName")
                    var companyName: String? = "",
                    @SerializedName("country")
                    var country: Country? = Country(),
                    @SerializedName("defaultAddress")
                    var defaultAddress: Boolean? = false,
                    @SerializedName("district")
                    var district: String? = "",
                    @SerializedName("email")
                    var email: String? = "",
                    @SerializedName("firstName")
                    var firstName: String? = "",
                    @SerializedName("formattedAddress")
                    var formattedAddress: String? = "",
                    @SerializedName("id")
                    var id: String? = "",
                    @SerializedName("lastName")
                    var lastName: String? = "",
                    @SerializedName("line1")
                    var line1: String? = "",
                    @SerializedName("line2")
                    var line2: String? = "",
                    @SerializedName("mobileCountry")
                    var mobileCountry: MobileCountry? = MobileCountry(),
                    @SerializedName("mobileNumber")
                    var mobileNumber: String? = "",
                    @SerializedName("nearestLandmark")
                    var nearestLandmark: String? = "",
                    @SerializedName("phone")
                    var phone: String? = "",
                    @SerializedName("postalCode")
                    var postalCode: String? = "",
                    @SerializedName("region")
                    var region: Region? = Region(),
                    @SerializedName("shippingAddress")
                    var shippingAddress: Boolean? = false,
                    @SerializedName("title")
                    var title: String? = "",
                    @SerializedName("titleCode")
                    var titleCode: String? = "",
                    @SerializedName("town")
                    var town: String? = "",
                    @SerializedName("visibleInAddressBook")
                    var visibleInAddressBook: Boolean? = false
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class City(
                        @SerializedName("areas")
                        var areas: List<Area?>? = listOf(),
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    ) {
                        data class Area(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }

                    data class Country(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class MobileCountry(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )

                    data class Region(
                        @SerializedName("countryIso")
                        var countryIso: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("isocodeShort")
                        var isocodeShort: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Features(
                    @SerializedName("additionalProp1")
                    var additionalProp1: String? = "",
                    @SerializedName("additionalProp2")
                    var additionalProp2: String? = "",
                    @SerializedName("additionalProp3")
                    var additionalProp3: String? = ""
                )

                data class GeoPoint(
                    @SerializedName("latitude")
                    var latitude: Double?=0.0,
                    @SerializedName("longitude")
                    var longitude: Double?=0.0
                )

                data class MapIcon(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class OpeningHours(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("specialDayOpeningList")
                    var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                    @SerializedName("weekDayOpeningList")
                    var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
                ) {
                    data class SpecialDayOpening(
                        @SerializedName("closed")
                        var closed: Boolean? = false,
                        @SerializedName("closingTime")
                        var closingTime: ClosingTime? = ClosingTime(),
                        @SerializedName("comment")
                        var comment: String? = "",
                        @SerializedName("date")
                        var date: String? = "",
                        @SerializedName("formattedDate")
                        var formattedDate: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("openingTime")
                        var openingTime: OpeningTime? = OpeningTime()
                    ) {
                        data class ClosingTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )

                        data class OpeningTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )
                    }

                    data class WeekDayOpening(
                        @SerializedName("closed")
                        var closed: Boolean? = false,
                        @SerializedName("closingTime")
                        var closingTime: ClosingTime? = ClosingTime(),
                        @SerializedName("openingTime")
                        var openingTime: OpeningTime? = OpeningTime(),
                        @SerializedName("weekDay")
                        var weekDay: String? = ""
                    ) {
                        data class ClosingTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )

                        data class OpeningTime(
                            @SerializedName("formattedHour")
                            var formattedHour: String? = "",
                            @SerializedName("hour")
                            var hour: Double?=0.0,
                            @SerializedName("minute")
                            var minute: Double?=0.0
                        )
                    }
                }

                data class StoreImage(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )
            }

            data class Product(
                @SerializedName("availableForPickup")
                var availableForPickup: Boolean? = false,
                @SerializedName("nutritionFacts")
                var nutritionFacts: String? = "",
                @SerializedName("averageRating")
                var averageRating: Double?=0.0,
                @SerializedName("baseOptions")
                var baseOptions: List<BaseOption?>? = listOf(),
                @SerializedName("baseProduct")
                var baseProduct: String? = "",
                @SerializedName("categories")
                var categories: List<Category?>? = listOf(),
                @SerializedName("classifications")
                var classifications: List<Classification?>? = listOf(),
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("configurable")
                var configurable: Boolean? = false,
                @SerializedName("configuratorType")
                var configuratorType: String? = "",
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("futureStocks")
                var futureStocks: List<FutureStock?>? = listOf(),
                @SerializedName("images")
                var images: List<Image?>? = listOf(),
                @SerializedName("manufacturer")
                var manufacturer: String? = "",
                @SerializedName("multidimensional")
                var multidimensional: Boolean? = false,
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("numberOfReviews")
                var numberOfReviews: Double?=0.0,
                @SerializedName("potentialPromotions")
                var potentialPromotions: List<PotentialPromotion?>? = listOf(),
                @SerializedName("price")
                var price: Price? = Price(),
                @SerializedName("priceRange")
                var priceRange: PriceRange? = PriceRange(),
                @SerializedName("productReferences")
                var productReferences: List<ProductReference?>? = listOf(),
                @SerializedName("purchasable")
                var purchasable: Boolean? = false,
                @SerializedName("reviews")
                var reviews: List<Review?>? = listOf(),
                @SerializedName("stock")
                var stock: Stock? = Stock(),
                @SerializedName("summary")
                var summary: String? = "",
                @SerializedName("tags")
                var tags: List<String?>? = listOf(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("variantMatrix")
                var variantMatrix: List<VariantMatrix?>? = listOf(),
                @SerializedName("variantOptions")
                var variantOptions: List<VariantOption?>? = listOf(),
                @SerializedName("variantType")
                var variantType: String? = "",
                @SerializedName("volumePrices")
                var volumePrices: List<VolumePrice?>? = listOf(),
                @SerializedName("volumePricesFlag")
                var volumePricesFlag: Boolean? = false
            ) {
                data class BaseOption(
                    @SerializedName("options")
                    var options: List<Option?>? = listOf(),
                    @SerializedName("selected")
                    var selected: Selected? = Selected(),
                    @SerializedName("variantType")
                    var variantType: String? = ""
                ) {
                    data class Option(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }

                    data class Selected(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }
                }

                data class Category(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("image")
                    var image: Image? = Image(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("plpPicture")
                    var plpPicture: PlpPicture? = PlpPicture(),
                    @SerializedName("plpPictureResponsive")
                    var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
                    @SerializedName("url")
                    var url: String? = ""
                ) {
                    data class Image(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class PlpPicture(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class PlpPictureResponsive(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )
                }

                data class Classification(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("features")
                    var features: List<Feature?>? = listOf(),
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Feature(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("comparable")
                        var comparable: Boolean? = false,
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("featureUnit")
                        var featureUnit: FeatureUnit? = FeatureUnit(),
                        @SerializedName("featureValues")
                        var featureValues: List<FeatureValue?>? = listOf(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("range")
                        var range: Boolean? = false,
                        @SerializedName("type")
                        var type: String? = ""
                    ) {
                        data class FeatureUnit(
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("symbol")
                            var symbol: String? = "",
                            @SerializedName("unitType")
                            var unitType: String? = ""
                        )

                        data class FeatureValue(
                            @SerializedName("value")
                            var value: String? = ""
                        )
                    }
                }

                data class FutureStock(
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("stock")
                    var stock: Stock? = Stock()
                ) {
                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )
                }

                data class Image(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PotentialPromotion(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("couldFireMessages")
                    var couldFireMessages: List<String?>? = listOf(),
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("enabled")
                    var enabled: Boolean? = false,
                    @SerializedName("endDate")
                    var endDate: String? = "",
                    @SerializedName("firedMessages")
                    var firedMessages: List<String?>? = listOf(),
                    @SerializedName("priority")
                    var priority: Double?=0.0,
                    @SerializedName("productBanner")
                    var productBanner: ProductBanner? = ProductBanner(),
                    @SerializedName("promotionGroup")
                    var promotionGroup: String? = "",
                    @SerializedName("promotionType")
                    var promotionType: String? = "",
                    @SerializedName("restrictions")
                    var restrictions: List<Restriction?>? = listOf(),
                    @SerializedName("startDate")
                    var startDate: String? = "",
                    @SerializedName("title")
                    var title: String? = ""
                ) {
                    data class ProductBanner(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )

                    data class Restriction(
                        @SerializedName("description")
                        var description: String? = "",
                        @SerializedName("restrictionType")
                        var restrictionType: String? = ""
                    )
                }

                data class Price(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class PriceRange(
                    @SerializedName("maxPrice")
                    var maxPrice: MaxPrice? = MaxPrice(),
                    @SerializedName("minPrice")
                    var minPrice: MinPrice? = MinPrice()
                ) {
                    data class MaxPrice(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class MinPrice(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )
                }

                data class ProductReference(
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("preselected")
                    var preselected: Boolean? = false,
                    @SerializedName("quantity")
                    var quantity: Double?=0.0,
                    @SerializedName("referenceType")
                    var referenceType: String? = ""
                )

                data class Review(
                    @SerializedName("alias")
                    var alias: String? = "",
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("headline")
                    var headline: String? = "",
                    @SerializedName("id")
                    var id: String? = "",
                    @SerializedName("principal")
                    var principal: Principal? = Principal(),
                    @SerializedName("rating")
                    var rating: Double?=0.0
                ) {
                    data class Principal(
                        @SerializedName("currency")
                        var currency: Currency? = Currency(),
                        @SerializedName("customerId")
                        var customerId: String? = "",
                        @SerializedName("deactivationDate")
                        var deactivationDate: String? = "",
                        @SerializedName("defaultAddress")
                        var defaultAddress: DefaultAddress? = DefaultAddress(),
                        @SerializedName("displayUid")
                        var displayUid: String? = "",
                        @SerializedName("firstName")
                        var firstName: String? = "",
                        @SerializedName("language")
                        var language: Language? = Language(),
                        @SerializedName("lastName")
                        var lastName: String? = "",
                        @SerializedName("mobileCountry")
                        var mobileCountry: MobileCountry? = MobileCountry(),
                        @SerializedName("mobileNumber")
                        var mobileNumber: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("title")
                        var title: String? = "",
                        @SerializedName("titleCode")
                        var titleCode: String? = "",
                        @SerializedName("uid")
                        var uid: String? = ""
                    ) {
                        data class Currency(
                            @SerializedName("active")
                            var active: Boolean? = false,
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("symbol")
                            var symbol: String? = ""
                        )

                        data class DefaultAddress(
                            @SerializedName("addressName")
                            var addressName: String? = "",
                            @SerializedName("area")
                            var area: Area? = Area(),
                            @SerializedName("cellphone")
                            var cellphone: String? = "",
                            @SerializedName("city")
                            var city: City? = City(),
                            @SerializedName("companyName")
                            var companyName: String? = "",
                            @SerializedName("country")
                            var country: Country? = Country(),
                            @SerializedName("defaultAddress")
                            var defaultAddress: Boolean? = false,
                            @SerializedName("district")
                            var district: String? = "",
                            @SerializedName("email")
                            var email: String? = "",
                            @SerializedName("firstName")
                            var firstName: String? = "",
                            @SerializedName("formattedAddress")
                            var formattedAddress: String? = "",
                            @SerializedName("id")
                            var id: String? = "",
                            @SerializedName("lastName")
                            var lastName: String? = "",
                            @SerializedName("line1")
                            var line1: String? = "",
                            @SerializedName("line2")
                            var line2: String? = "",
                            @SerializedName("mobileCountry")
                            var mobileCountry: MobileCountry? = MobileCountry(),
                            @SerializedName("mobileNumber")
                            var mobileNumber: String? = "",
                            @SerializedName("nearestLandmark")
                            var nearestLandmark: String? = "",
                            @SerializedName("phone")
                            var phone: String? = "",
                            @SerializedName("postalCode")
                            var postalCode: String? = "",
                            @SerializedName("region")
                            var region: Region? = Region(),
                            @SerializedName("shippingAddress")
                            var shippingAddress: Boolean? = false,
                            @SerializedName("title")
                            var title: String? = "",
                            @SerializedName("titleCode")
                            var titleCode: String? = "",
                            @SerializedName("town")
                            var town: String? = "",
                            @SerializedName("visibleInAddressBook")
                            var visibleInAddressBook: Boolean? = false
                        ) {
                            data class Area(
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class City(
                                @SerializedName("areas")
                                var areas: List<Area?>? = listOf(),
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            ) {
                                data class Area(
                                    @SerializedName("code")
                                    var code: String? = "",
                                    @SerializedName("name")
                                    var name: String? = ""
                                )
                            }

                            data class Country(
                                @SerializedName("isdcode")
                                var isdcode: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class MobileCountry(
                                @SerializedName("isdcode")
                                var isdcode: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )

                            data class Region(
                                @SerializedName("countryIso")
                                var countryIso: String? = "",
                                @SerializedName("isocode")
                                var isocode: String? = "",
                                @SerializedName("isocodeShort")
                                var isocodeShort: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }

                        data class Language(
                            @SerializedName("active")
                            var active: Boolean? = false,
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("nativeName")
                            var nativeName: String? = ""
                        )

                        data class MobileCountry(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }
                }

                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )

                data class VariantMatrix(
                    @SerializedName("elements")
                    var elements: List<Any?>? = listOf(),
                    @SerializedName("isLeaf")
                    var isLeaf: Boolean? = false,
                    @SerializedName("parentVariantCategory")
                    var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
                    @SerializedName("variantOption")
                    var variantOption: VariantOption? = VariantOption(),
                    @SerializedName("variantValueCategory")
                    var variantValueCategory: VariantValueCategory? = VariantValueCategory()
                ) {
                    data class ParentVariantCategory(
                        @SerializedName("hasImage")
                        var hasImage: Boolean? = false,
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("priority")
                        var priority: Double?=0.0
                    )

                    data class VariantOption(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("priceData")
                        var priceData: PriceData? = PriceData(),
                        @SerializedName("stock")
                        var stock: Stock? = Stock(),
                        @SerializedName("url")
                        var url: String? = "",
                        @SerializedName("variantOptionQualifiers")
                        var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                    ) {
                        data class PriceData(
                            @SerializedName("currencyIso")
                            var currencyIso: String? = "",
                            @SerializedName("formattedValue")
                            var formattedValue: String? = "",
                            @SerializedName("maxQuantity")
                            var maxQuantity: Double?=0.0,
                            @SerializedName("minQuantity")
                            var minQuantity: Double?=0.0,
                            @SerializedName("priceType")
                            var priceType: String? = "",
                            @SerializedName("value")
                            var value: Double?=0.0
                        )

                        data class Stock(
                            @SerializedName("stockLevel")
                            var stockLevel: Double?=0.0,
                            @SerializedName("stockLevelStatus")
                            var stockLevelStatus: String? = ""
                        )

                        data class VariantOptionQualifier(
                            @SerializedName("image")
                            var image: Image? = Image(),
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("qualifier")
                            var qualifier: String? = "",
                            @SerializedName("value")
                            var value: String? = ""
                        ) {
                            data class Image(
                                @SerializedName("altText")
                                var altText: String? = "",
                                @SerializedName("format")
                                var format: String? = "",
                                @SerializedName("galleryIndex")
                                var galleryIndex: Double?=0.0,
                                @SerializedName("imageType")
                                var imageType: String? = "",
                                @SerializedName("url")
                                var url: String? = ""
                            )
                        }
                    }

                    data class VariantValueCategory(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("sequence")
                        var sequence: Double?=0.0,
                        @SerializedName("superCategories")
                        var superCategories: List<SuperCategory?>? = listOf()
                    ) {
                        data class SuperCategory(
                            @SerializedName("hasImage")
                            var hasImage: Boolean? = false,
                            @SerializedName("name")
                            var name: String? = "",
                            @SerializedName("priority")
                            var priority: Double?=0.0
                        )
                    }
                }

                data class VariantOption(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class VolumePrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class ReturnedItemsPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class TotalPrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class TotalPriceWithTax(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )
    }

    data class ProductDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class StoreCreditAmount(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class StoreCreditMode(
        @SerializedName("description")
        var description: String? = "",
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("storeCreditModeType")
        var storeCreditModeType: StoreCreditModeType? = StoreCreditModeType()
    ) {
        data class StoreCreditModeType(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("name")
            var name: String? = ""
        )
    }

    data class SubTotal(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class TimeSlotInfoData(
        @SerializedName("date")
        var date: String? = "",
        @SerializedName("day")
        var day: String? = "",
        @SerializedName("end")
        var end: String? = "",
        @SerializedName("periodCode")
        var periodCode: String? = "",
        @SerializedName("start")
        var start: String? = ""
    )

    data class TotalDiscounts(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class TotalPrice(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class TotalPriceWithTax(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class TotalTax(
        @SerializedName("currencyIso")
        var currencyIso: String? = "",
        @SerializedName("formattedValue")
        var formattedValue: String? = "",
        @SerializedName("maxQuantity")
        var maxQuantity: Double?=0.0,
        @SerializedName("minQuantity")
        var minQuantity: Double?=0.0,
        @SerializedName("priceType")
        var priceType: String? = "",
        @SerializedName("value")
        var value: Double?=0.0
    )

    data class UnconsignedEntry(
        @SerializedName("basePrice")
        var basePrice: BasePrice? = BasePrice(),
        @SerializedName("cancellableQuantity")
        var cancellableQuantity: Double?=0.0,
        @SerializedName("cancelledItemsPrice")
        var cancelledItemsPrice: CancelledItemsPrice? = CancelledItemsPrice(),
        @SerializedName("configurationInfos")
        var configurationInfos: List<ConfigurationInfo?>? = listOf(),
        @SerializedName("deliveryMode")
        var deliveryMode: DeliveryMode? = DeliveryMode(),
        @SerializedName("deliveryPointOfService")
        var deliveryPointOfService: DeliveryPointOfService? = DeliveryPointOfService(),
        @SerializedName("entryNumber")
        var entryNumber: Double?=0.0,
        @SerializedName("product")
        var product: Product? = Product(),
        @SerializedName("quantity")
        var quantity: Double?=0.0,
        @SerializedName("quantityAllocated")
        var quantityAllocated: Double?=0.0,
        @SerializedName("quantityCancelled")
        var quantityCancelled: Double?=0.0,
        @SerializedName("quantityPending")
        var quantityPending: Double?=0.0,
        @SerializedName("quantityReturned")
        var quantityReturned: Double?=0.0,
        @SerializedName("quantityShipped")
        var quantityShipped: Double?=0.0,
        @SerializedName("quantityUnallocated")
        var quantityUnallocated: Double?=0.0,
        @SerializedName("returnableQuantity")
        var returnableQuantity: Double?=0.0,
        @SerializedName("returnedItemsPrice")
        var returnedItemsPrice: ReturnedItemsPrice? = ReturnedItemsPrice(),
        @SerializedName("totalPrice")
        var totalPrice: TotalPrice? = TotalPrice(),
        @SerializedName("updateable")
        var updateable: Boolean? = false,
        @SerializedName("url")
        var url: String? = ""
    ) {
        data class BasePrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class CancelledItemsPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class ConfigurationInfo(
            @SerializedName("configurationLabel")
            var configurationLabel: String? = "",
            @SerializedName("configurationValue")
            var configurationValue: String? = "",
            @SerializedName("configuratorType")
            var configuratorType: String? = "",
            @SerializedName("status")
            var status: String? = ""
        )

        data class DeliveryMode(
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("deliveryCost")
            var deliveryCost: DeliveryCost? = DeliveryCost(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("name")
            var name: String? = ""
        ) {
            data class DeliveryCost(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class DeliveryPointOfService(
            @SerializedName("address")
            var address: Address? = Address(),
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("displayName")
            var displayName: String? = "",
            @SerializedName("distanceKm")
            var distanceKm: Double?=0.0,
            @SerializedName("features")
            var features: Features? = Features(),
            @SerializedName("formattedDistance")
            var formattedDistance: String? = "",
            @SerializedName("geoPoint")
            var geoPoint: GeoPoint? = GeoPoint(),
            @SerializedName("mapIcon")
            var mapIcon: MapIcon? = MapIcon(),
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("openingHours")
            var openingHours: OpeningHours? = OpeningHours(),
            @SerializedName("storeContent")
            var storeContent: String? = "",
            @SerializedName("storeImages")
            var storeImages: List<StoreImage?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("warehouseCodes")
            var warehouseCodes: List<String?>? = listOf()
        ) {
            data class Address(
                @SerializedName("addressName")
                var addressName: String? = "",
                @SerializedName("area")
                var area: Area? = Area(),
                @SerializedName("cellphone")
                var cellphone: String? = "",
                @SerializedName("city")
                var city: City? = City(),
                @SerializedName("companyName")
                var companyName: String? = "",
                @SerializedName("country")
                var country: Country? = Country(),
                @SerializedName("defaultAddress")
                var defaultAddress: Boolean? = false,
                @SerializedName("district")
                var district: String? = "",
                @SerializedName("email")
                var email: String? = "",
                @SerializedName("firstName")
                var firstName: String? = "",
                @SerializedName("formattedAddress")
                var formattedAddress: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("lastName")
                var lastName: String? = "",
                @SerializedName("line1")
                var line1: String? = "",
                @SerializedName("line2")
                var line2: String? = "",
                @SerializedName("mobileCountry")
                var mobileCountry: MobileCountry? = MobileCountry(),
                @SerializedName("mobileNumber")
                var mobileNumber: String? = "",
                @SerializedName("nearestLandmark")
                var nearestLandmark: String? = "",
                @SerializedName("phone")
                var phone: String? = "",
                @SerializedName("postalCode")
                var postalCode: String? = "",
                @SerializedName("region")
                var region: Region? = Region(),
                @SerializedName("shippingAddress")
                var shippingAddress: Boolean? = false,
                @SerializedName("title")
                var title: String? = "",
                @SerializedName("titleCode")
                var titleCode: String? = "",
                @SerializedName("town")
                var town: String? = "",
                @SerializedName("visibleInAddressBook")
                var visibleInAddressBook: Boolean? = false
            ) {
                data class Area(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class City(
                    @SerializedName("areas")
                    var areas: List<Area?>? = listOf(),
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                ) {
                    data class Area(
                        @SerializedName("code")
                        var code: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }

                data class Country(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class MobileCountry(
                    @SerializedName("isdcode")
                    var isdcode: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )

                data class Region(
                    @SerializedName("countryIso")
                    var countryIso: String? = "",
                    @SerializedName("isocode")
                    var isocode: String? = "",
                    @SerializedName("isocodeShort")
                    var isocodeShort: String? = "",
                    @SerializedName("name")
                    var name: String? = ""
                )
            }

            data class Features(
                @SerializedName("additionalProp1")
                var additionalProp1: String? = "",
                @SerializedName("additionalProp2")
                var additionalProp2: String? = "",
                @SerializedName("additionalProp3")
                var additionalProp3: String? = ""
            )

            data class GeoPoint(
                @SerializedName("latitude")
                var latitude: Double?=0.0,
                @SerializedName("longitude")
                var longitude: Double?=0.0
            )

            data class MapIcon(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class OpeningHours(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("specialDayOpeningList")
                var specialDayOpeningList: List<SpecialDayOpening?>? = listOf(),
                @SerializedName("weekDayOpeningList")
                var weekDayOpeningList: List<WeekDayOpening?>? = listOf()
            ) {
                data class SpecialDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("comment")
                    var comment: String? = "",
                    @SerializedName("date")
                    var date: String? = "",
                    @SerializedName("formattedDate")
                    var formattedDate: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime()
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }

                data class WeekDayOpening(
                    @SerializedName("closed")
                    var closed: Boolean? = false,
                    @SerializedName("closingTime")
                    var closingTime: ClosingTime? = ClosingTime(),
                    @SerializedName("openingTime")
                    var openingTime: OpeningTime? = OpeningTime(),
                    @SerializedName("weekDay")
                    var weekDay: String? = ""
                ) {
                    data class ClosingTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )

                    data class OpeningTime(
                        @SerializedName("formattedHour")
                        var formattedHour: String? = "",
                        @SerializedName("hour")
                        var hour: Double?=0.0,
                        @SerializedName("minute")
                        var minute: Double?=0.0
                    )
                }
            }

            data class StoreImage(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )
        }

        data class Product(
            @SerializedName("availableForPickup")
            var availableForPickup: Boolean? = false,
            @SerializedName("nutritionFacts")
            var nutritionFacts: String? = "",
            @SerializedName("averageRating")
            var averageRating: Double?=0.0,
            @SerializedName("baseOptions")
            var baseOptions: List<BaseOption?>? = listOf(),
            @SerializedName("baseProduct")
            var baseProduct: String? = "",
            @SerializedName("categories")
            var categories: List<Category?>? = listOf(),
            @SerializedName("classifications")
            var classifications: List<Classification?>? = listOf(),
            @SerializedName("code")
            var code: String? = "",
            @SerializedName("configurable")
            var configurable: Boolean? = false,
            @SerializedName("configuratorType")
            var configuratorType: String? = "",
            @SerializedName("description")
            var description: String? = "",
            @SerializedName("futureStocks")
            var futureStocks: List<FutureStock?>? = listOf(),
            @SerializedName("images")
            var images: List<Image?>? = listOf(),
            @SerializedName("manufacturer")
            var manufacturer: String? = "",
            @SerializedName("multidimensional")
            var multidimensional: Boolean? = false,
            @SerializedName("name")
            var name: String? = "",
            @SerializedName("numberOfReviews")
            var numberOfReviews: Double?=0.0,
            @SerializedName("potentialPromotions")
            var potentialPromotions: List<PotentialPromotion?>? = listOf(),
            @SerializedName("price")
            var price: Price? = Price(),
            @SerializedName("priceRange")
            var priceRange: PriceRange? = PriceRange(),
            @SerializedName("productReferences")
            var productReferences: List<ProductReference?>? = listOf(),
            @SerializedName("purchasable")
            var purchasable: Boolean? = false,
            @SerializedName("reviews")
            var reviews: List<Review?>? = listOf(),
            @SerializedName("stock")
            var stock: Stock? = Stock(),
            @SerializedName("summary")
            var summary: String? = "",
            @SerializedName("tags")
            var tags: List<String?>? = listOf(),
            @SerializedName("url")
            var url: String? = "",
            @SerializedName("variantMatrix")
            var variantMatrix: List<VariantMatrix?>? = listOf(),
            @SerializedName("variantOptions")
            var variantOptions: List<VariantOption?>? = listOf(),
            @SerializedName("variantType")
            var variantType: String? = "",
            @SerializedName("volumePrices")
            var volumePrices: List<VolumePrice?>? = listOf(),
            @SerializedName("volumePricesFlag")
            var volumePricesFlag: Boolean? = false
        ) {
            data class BaseOption(
                @SerializedName("options")
                var options: List<Option?>? = listOf(),
                @SerializedName("selected")
                var selected: Selected? = Selected(),
                @SerializedName("variantType")
                var variantType: String? = ""
            ) {
                data class Option(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class Selected(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }
            }

            data class Category(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("image")
                var image: Image? = Image(),
                @SerializedName("name")
                var name: String? = "",
                @SerializedName("plpPicture")
                var plpPicture: PlpPicture? = PlpPicture(),
                @SerializedName("plpPictureResponsive")
                var plpPictureResponsive: PlpPictureResponsive? = PlpPictureResponsive(),
                @SerializedName("url")
                var url: String? = ""
            ) {
                data class Image(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PlpPicture(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class PlpPictureResponsive(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )
            }

            data class Classification(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("features")
                var features: List<Feature?>? = listOf(),
                @SerializedName("name")
                var name: String? = ""
            ) {
                data class Feature(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("comparable")
                    var comparable: Boolean? = false,
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("featureUnit")
                    var featureUnit: FeatureUnit? = FeatureUnit(),
                    @SerializedName("featureValues")
                    var featureValues: List<FeatureValue?>? = listOf(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("range")
                    var range: Boolean? = false,
                    @SerializedName("type")
                    var type: String? = ""
                ) {
                    data class FeatureUnit(
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("symbol")
                        var symbol: String? = "",
                        @SerializedName("unitType")
                        var unitType: String? = ""
                    )

                    data class FeatureValue(
                        @SerializedName("value")
                        var value: String? = ""
                    )
                }
            }

            data class FutureStock(
                @SerializedName("date")
                var date: String? = "",
                @SerializedName("formattedDate")
                var formattedDate: String? = "",
                @SerializedName("stock")
                var stock: Stock? = Stock()
            ) {
                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )
            }

            data class Image(
                @SerializedName("altText")
                var altText: String? = "",
                @SerializedName("format")
                var format: String? = "",
                @SerializedName("galleryIndex")
                var galleryIndex: Double?=0.0,
                @SerializedName("imageType")
                var imageType: String? = "",
                @SerializedName("url")
                var url: String? = ""
            )

            data class PotentialPromotion(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("couldFireMessages")
                var couldFireMessages: List<String?>? = listOf(),
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("enabled")
                var enabled: Boolean? = false,
                @SerializedName("endDate")
                var endDate: String? = "",
                @SerializedName("firedMessages")
                var firedMessages: List<String?>? = listOf(),
                @SerializedName("priority")
                var priority: Double?=0.0,
                @SerializedName("productBanner")
                var productBanner: ProductBanner? = ProductBanner(),
                @SerializedName("promotionGroup")
                var promotionGroup: String? = "",
                @SerializedName("promotionType")
                var promotionType: String? = "",
                @SerializedName("restrictions")
                var restrictions: List<Restriction?>? = listOf(),
                @SerializedName("startDate")
                var startDate: String? = "",
                @SerializedName("title")
                var title: String? = ""
            ) {
                data class ProductBanner(
                    @SerializedName("altText")
                    var altText: String? = "",
                    @SerializedName("format")
                    var format: String? = "",
                    @SerializedName("galleryIndex")
                    var galleryIndex: Double?=0.0,
                    @SerializedName("imageType")
                    var imageType: String? = "",
                    @SerializedName("url")
                    var url: String? = ""
                )

                data class Restriction(
                    @SerializedName("description")
                    var description: String? = "",
                    @SerializedName("restrictionType")
                    var restrictionType: String? = ""
                )
            }

            data class Price(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )

            data class PriceRange(
                @SerializedName("maxPrice")
                var maxPrice: MaxPrice? = MaxPrice(),
                @SerializedName("minPrice")
                var minPrice: MinPrice? = MinPrice()
            ) {
                data class MaxPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class MinPrice(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )
            }

            data class ProductReference(
                @SerializedName("description")
                var description: String? = "",
                @SerializedName("preselected")
                var preselected: Boolean? = false,
                @SerializedName("quantity")
                var quantity: Double?=0.0,
                @SerializedName("referenceType")
                var referenceType: String? = ""
            )

            data class Review(
                @SerializedName("alias")
                var alias: String? = "",
                @SerializedName("comment")
                var comment: String? = "",
                @SerializedName("date")
                var date: String? = "",
                @SerializedName("headline")
                var headline: String? = "",
                @SerializedName("id")
                var id: String? = "",
                @SerializedName("principal")
                var principal: Principal? = Principal(),
                @SerializedName("rating")
                var rating: Double?=0.0
            ) {
                data class Principal(
                    @SerializedName("currency")
                    var currency: Currency? = Currency(),
                    @SerializedName("customerId")
                    var customerId: String? = "",
                    @SerializedName("deactivationDate")
                    var deactivationDate: String? = "",
                    @SerializedName("defaultAddress")
                    var defaultAddress: DefaultAddress? = DefaultAddress(),
                    @SerializedName("displayUid")
                    var displayUid: String? = "",
                    @SerializedName("firstName")
                    var firstName: String? = "",
                    @SerializedName("language")
                    var language: Language? = Language(),
                    @SerializedName("lastName")
                    var lastName: String? = "",
                    @SerializedName("mobileCountry")
                    var mobileCountry: MobileCountry? = MobileCountry(),
                    @SerializedName("mobileNumber")
                    var mobileNumber: String? = "",
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("title")
                    var title: String? = "",
                    @SerializedName("titleCode")
                    var titleCode: String? = "",
                    @SerializedName("uid")
                    var uid: String? = ""
                ) {
                    data class Currency(
                        @SerializedName("active")
                        var active: Boolean? = false,
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("symbol")
                        var symbol: String? = ""
                    )

                    data class DefaultAddress(
                        @SerializedName("addressName")
                        var addressName: String? = "",
                        @SerializedName("area")
                        var area: Area? = Area(),
                        @SerializedName("cellphone")
                        var cellphone: String? = "",
                        @SerializedName("city")
                        var city: City? = City(),
                        @SerializedName("companyName")
                        var companyName: String? = "",
                        @SerializedName("country")
                        var country: Country? = Country(),
                        @SerializedName("defaultAddress")
                        var defaultAddress: Boolean? = false,
                        @SerializedName("district")
                        var district: String? = "",
                        @SerializedName("email")
                        var email: String? = "",
                        @SerializedName("firstName")
                        var firstName: String? = "",
                        @SerializedName("formattedAddress")
                        var formattedAddress: String? = "",
                        @SerializedName("id")
                        var id: String? = "",
                        @SerializedName("lastName")
                        var lastName: String? = "",
                        @SerializedName("line1")
                        var line1: String? = "",
                        @SerializedName("line2")
                        var line2: String? = "",
                        @SerializedName("mobileCountry")
                        var mobileCountry: MobileCountry? = MobileCountry(),
                        @SerializedName("mobileNumber")
                        var mobileNumber: String? = "",
                        @SerializedName("nearestLandmark")
                        var nearestLandmark: String? = "",
                        @SerializedName("phone")
                        var phone: String? = "",
                        @SerializedName("postalCode")
                        var postalCode: String? = "",
                        @SerializedName("region")
                        var region: Region? = Region(),
                        @SerializedName("shippingAddress")
                        var shippingAddress: Boolean? = false,
                        @SerializedName("title")
                        var title: String? = "",
                        @SerializedName("titleCode")
                        var titleCode: String? = "",
                        @SerializedName("town")
                        var town: String? = "",
                        @SerializedName("visibleInAddressBook")
                        var visibleInAddressBook: Boolean? = false
                    ) {
                        data class Area(
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class City(
                            @SerializedName("areas")
                            var areas: List<Area?>? = listOf(),
                            @SerializedName("code")
                            var code: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        ) {
                            data class Area(
                                @SerializedName("code")
                                var code: String? = "",
                                @SerializedName("name")
                                var name: String? = ""
                            )
                        }

                        data class Country(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class MobileCountry(
                            @SerializedName("isdcode")
                            var isdcode: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )

                        data class Region(
                            @SerializedName("countryIso")
                            var countryIso: String? = "",
                            @SerializedName("isocode")
                            var isocode: String? = "",
                            @SerializedName("isocodeShort")
                            var isocodeShort: String? = "",
                            @SerializedName("name")
                            var name: String? = ""
                        )
                    }

                    data class Language(
                        @SerializedName("active")
                        var active: Boolean? = false,
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("nativeName")
                        var nativeName: String? = ""
                    )

                    data class MobileCountry(
                        @SerializedName("isdcode")
                        var isdcode: String? = "",
                        @SerializedName("isocode")
                        var isocode: String? = "",
                        @SerializedName("name")
                        var name: String? = ""
                    )
                }
            }

            data class Stock(
                @SerializedName("stockLevel")
                var stockLevel: Double?=0.0,
                @SerializedName("stockLevelStatus")
                var stockLevelStatus: String? = ""
            )

            data class VariantMatrix(
                @SerializedName("elements")
                var elements: List<Any?>? = listOf(),
                @SerializedName("isLeaf")
                var isLeaf: Boolean? = false,
                @SerializedName("parentVariantCategory")
                var parentVariantCategory: ParentVariantCategory? = ParentVariantCategory(),
                @SerializedName("variantOption")
                var variantOption: VariantOption? = VariantOption(),
                @SerializedName("variantValueCategory")
                var variantValueCategory: VariantValueCategory? = VariantValueCategory()
            ) {
                data class ParentVariantCategory(
                    @SerializedName("hasImage")
                    var hasImage: Boolean? = false,
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("priority")
                    var priority: Double?=0.0
                )

                data class VariantOption(
                    @SerializedName("code")
                    var code: String? = "",
                    @SerializedName("priceData")
                    var priceData: PriceData? = PriceData(),
                    @SerializedName("stock")
                    var stock: Stock? = Stock(),
                    @SerializedName("url")
                    var url: String? = "",
                    @SerializedName("variantOptionQualifiers")
                    var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
                ) {
                    data class PriceData(
                        @SerializedName("currencyIso")
                        var currencyIso: String? = "",
                        @SerializedName("formattedValue")
                        var formattedValue: String? = "",
                        @SerializedName("maxQuantity")
                        var maxQuantity: Double?=0.0,
                        @SerializedName("minQuantity")
                        var minQuantity: Double?=0.0,
                        @SerializedName("priceType")
                        var priceType: String? = "",
                        @SerializedName("value")
                        var value: Double?=0.0
                    )

                    data class Stock(
                        @SerializedName("stockLevel")
                        var stockLevel: Double?=0.0,
                        @SerializedName("stockLevelStatus")
                        var stockLevelStatus: String? = ""
                    )

                    data class VariantOptionQualifier(
                        @SerializedName("image")
                        var image: Image? = Image(),
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("qualifier")
                        var qualifier: String? = "",
                        @SerializedName("value")
                        var value: String? = ""
                    ) {
                        data class Image(
                            @SerializedName("altText")
                            var altText: String? = "",
                            @SerializedName("format")
                            var format: String? = "",
                            @SerializedName("galleryIndex")
                            var galleryIndex: Double?=0.0,
                            @SerializedName("imageType")
                            var imageType: String? = "",
                            @SerializedName("url")
                            var url: String? = ""
                        )
                    }
                }

                data class VariantValueCategory(
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("sequence")
                    var sequence: Double?=0.0,
                    @SerializedName("superCategories")
                    var superCategories: List<SuperCategory?>? = listOf()
                ) {
                    data class SuperCategory(
                        @SerializedName("hasImage")
                        var hasImage: Boolean? = false,
                        @SerializedName("name")
                        var name: String? = "",
                        @SerializedName("priority")
                        var priority: Double?=0.0
                    )
                }
            }

            data class VariantOption(
                @SerializedName("code")
                var code: String? = "",
                @SerializedName("priceData")
                var priceData: PriceData? = PriceData(),
                @SerializedName("stock")
                var stock: Stock? = Stock(),
                @SerializedName("url")
                var url: String? = "",
                @SerializedName("variantOptionQualifiers")
                var variantOptionQualifiers: List<VariantOptionQualifier?>? = listOf()
            ) {
                data class PriceData(
                    @SerializedName("currencyIso")
                    var currencyIso: String? = "",
                    @SerializedName("formattedValue")
                    var formattedValue: String? = "",
                    @SerializedName("maxQuantity")
                    var maxQuantity: Double?=0.0,
                    @SerializedName("minQuantity")
                    var minQuantity: Double?=0.0,
                    @SerializedName("priceType")
                    var priceType: String? = "",
                    @SerializedName("value")
                    var value: Double?=0.0
                )

                data class Stock(
                    @SerializedName("stockLevel")
                    var stockLevel: Double?=0.0,
                    @SerializedName("stockLevelStatus")
                    var stockLevelStatus: String? = ""
                )

                data class VariantOptionQualifier(
                    @SerializedName("image")
                    var image: Image? = Image(),
                    @SerializedName("name")
                    var name: String? = "",
                    @SerializedName("qualifier")
                    var qualifier: String? = "",
                    @SerializedName("value")
                    var value: String? = ""
                ) {
                    data class Image(
                        @SerializedName("altText")
                        var altText: String? = "",
                        @SerializedName("format")
                        var format: String? = "",
                        @SerializedName("galleryIndex")
                        var galleryIndex: Double?=0.0,
                        @SerializedName("imageType")
                        var imageType: String? = "",
                        @SerializedName("url")
                        var url: String? = ""
                    )
                }
            }

            data class VolumePrice(
                @SerializedName("currencyIso")
                var currencyIso: String? = "",
                @SerializedName("formattedValue")
                var formattedValue: String? = "",
                @SerializedName("maxQuantity")
                var maxQuantity: Double?=0.0,
                @SerializedName("minQuantity")
                var minQuantity: Double?=0.0,
                @SerializedName("priceType")
                var priceType: String? = "",
                @SerializedName("value")
                var value: Double?=0.0
            )
        }

        data class ReturnedItemsPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )

        data class TotalPrice(
            @SerializedName("currencyIso")
            var currencyIso: String? = "",
            @SerializedName("formattedValue")
            var formattedValue: String? = "",
            @SerializedName("maxQuantity")
            var maxQuantity: Double?=0.0,
            @SerializedName("minQuantity")
            var minQuantity: Double?=0.0,
            @SerializedName("priceType")
            var priceType: String? = "",
            @SerializedName("value")
            var value: Double?=0.0
        )
    }

    data class User(
        @SerializedName("name")
        var name: String? = "",
        @SerializedName("uid")
        var uid: String? = ""
    )
}