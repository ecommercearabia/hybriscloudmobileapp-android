package com.erabia.foodcrowd.android.home.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.adapter.SubCategoryAdapter
import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentSegmentCatogoryBinding
import com.erabia.foodcrowd.android.home.adapter.HomeSubCategoryAdapter
import com.erabia.foodcrowd.android.home.adapter.HomeListingAdapter
import com.erabia.foodcrowd.android.home.callback.OnAddToWishlistSuccessListener
import com.erabia.foodcrowd.android.home.callback.WishlistClickListener
import com.erabia.foodcrowd.android.home.db.HomeDao
import com.erabia.foodcrowd.android.home.remote.repository.HighlightedProductRepository
import com.erabia.foodcrowd.android.home.remote.service.HomeService
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.home.viewmodel.HomeViewModel
import com.erabia.foodcrowd.android.home.viewmodel.SegmentCategoryFragmentViewModel
import com.yarolegovich.discretescrollview.DiscreteScrollView
import com.yarolegovich.discretescrollview.InfiniteScrollAdapter
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer


class SegmentCategoryFragment(
    val homeViewModel: HomeViewModel,
    val segmentCategoryFragmentViewModel: SegmentCategoryFragmentViewModel,
    val categoryId: String,
    val homeService: HomeService,
    val homeDao: HomeDao,
    val component: Component?
) :
    Fragment(), WishlistClickListener,
    DiscreteScrollView.OnItemChangedListener<SubCategoryAdapter.ViewHolder>,
    OnAddToWishlistSuccessListener {
    private var itemBinding: FragmentSegmentCatogoryBinding? = null
    private lateinit var subCategoryAdapter: HomeSubCategoryAdapter
    private lateinit var homeListingAdapter: HomeListingAdapter
    private var wishListView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        itemBinding = DataBindingUtil.inflate<FragmentSegmentCatogoryBinding>(
            inflater,
            R.layout.fragment_segment_catogory,
            container,
            false
        )
        val highlightedProductRepository = HighlightedProductRepository(homeService, homeDao)
        val highLightedProductsViewModel =
            HighLightedProductsViewModel(highlightedProductRepository)
        homeListingAdapter = HomeListingAdapter(highLightedProductsViewModel, homeViewModel, false)
        subCategoryAdapter = HomeSubCategoryAdapter()
        val wrapper: InfiniteScrollAdapter<*> = InfiniteScrollAdapter.wrap(subCategoryAdapter)

        homeListingAdapter.setHasStableIds(true)
        itemBinding?.recyclerViewHighlightedProduct?.isFocusable = false
        itemBinding?.recyclerViewHighlightedProduct?.adapter = homeListingAdapter
//        observeCategoryProduct()
        itemBinding?.picker?.adapter = wrapper
        itemBinding?.picker?.setItemTransformer(
            ScaleTransformer.Builder()
                .setMaxScale(1.05f)
                .setMinScale(0.75f)
                .setPivotX(Pivot.X.CENTER) // CENTER is a default one
                .setPivotY(Pivot.Y.BOTTOM) // CENTER is a default one
                .build()
        )
        observeCategory()
//        observeCategoryProduct()
//        observeAddToWishlist()
//        observeNavigateToDetails()
//        observeAddToCartEvent()
//        observeAddToWishlistEvent()
//        observeStartLogin()
//        segmentCategoryFragmentViewModel.loadCategory(categoryId)
//        subCategoryAdapter.componentList = component
        //TODO remove comment here
//        itemBinding?.let {
//            prepareListingAdapter(
//                highLightedProductsViewModel,
//                itemBinding!!,
//                component?.productCodes?.split(" ")?.toMutableList() ?: mutableListOf()
//            )
//        }
        return itemBinding?.root
    }

    private fun observeCategory() {
        segmentCategoryFragmentViewModel.component.observe(viewLifecycleOwner, Observer {
            subCategoryAdapter.componentList = it
        })
    }

//    private fun observeCategoryProduct() {
//        segmentCategoryFragmentViewModel.categoryProductLiveData?.observe(
//            viewLifecycleOwner, Observer {
//                listingAdapter.productList = null
//                listingAdapter.productList = it?.products as? MutableList<Product>?
//                segmentCategoryFragmentViewModel.progressBar().postValue(View.GONE)
//            })
//    }

    override fun onStart() {
        super.onStart()
        itemBinding?.picker?.addOnItemChangedListener(this);
        segmentCategoryFragmentViewModel.highlightedProductAdapter?.wishlistClickListener = this
    }

    override fun onStop() {
        super.onStop()
        itemBinding?.picker?.removeItemChangedListener(this);
    }

    private fun prepareListingAdapter(
        itemViewModel: HighLightedProductsViewModel,
        binding: FragmentSegmentCatogoryBinding,
        productCodeList: MutableList<String>
    ) {
        val listingAdapter = HomeListingAdapter(itemViewModel, homeViewModel, false, -1)
        listingAdapter.setHasStableIds(true)
        listingAdapter.productList =
            productCodeList.map { Product().apply { code = it } }.toMutableList()
        binding.recyclerViewHighlightedProduct.setItemAnimator(null);

        binding.recyclerViewHighlightedProduct.adapter = listingAdapter
        observeHighlightedProductViewModel(
            itemViewModel,
            binding,
            listingAdapter
        )
    }


    private fun observeHighlightedProductViewModel(
        highLightedProductsViewModel: HighLightedProductsViewModel,
        highLightedProductViewHolder: FragmentSegmentCatogoryBinding,
        homeListingAdapter: HomeListingAdapter
    ) {
        highLightedProductsViewModel.productLiveData.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                //notifyItemChanged() should called on ui thread , it will crashed if its called on
                //background thread
                highLightedProductViewHolder.recyclerViewHighlightedProduct.post {
                    homeListingAdapter.updateItem(it)
                }
            })
    }

//    private fun observeAddToWishlist() {
//        segmentCategoryFragmentViewModel.addToWishlist.observe(this, Observer {
//            (wishListView as ImageView?)?.setImageResource(R.drawable.ic_selected_wishlist)
//            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
//        })
//    }
//
//    private fun observeNavigateToDetails() {
//        segmentCategoryFragmentViewModel.navigateToDetails.observe(this, Observer {
//            val bundle = Bundle()
//            bundle.putString("Product_Id", it)
//            val detailsFragment = ProductDetailsFragment()
//            detailsFragment.arguments = bundle
//            findNavController()?.navigate(R.id.action_navigate_to_details, bundle)
//        })
//    }
//
//    private fun observeAddToCartEvent() {
//        segmentCategoryFragmentViewModel.addToCartEvent.observe(this, Observer {
//            val addToCartBottomSheet = AddToCartBottomSheet()
//            val bundle = Bundle()
//            bundle.putString("Product_Id", it)
//            addToCartBottomSheet.arguments = bundle
//            addToCartBottomSheet.show(
//                (context as FragmentActivity).supportFragmentManager,
//                addToCartBottomSheet.tag
//            )
//        })
//    }
//
//    private fun observeAddToWishlistEvent() {
//        segmentCategoryFragmentViewModel.addToWishlistEvent.observe(this, Observer {
//            val addToWishlistBottomSheet = AddToWishlistBottomSheet()
//            addToWishlistBottomSheet?.onAddToWishlistSuccessListener = this
//            val bundle = Bundle()
//            bundle.putString("Product_Id", it)
//            addToWishlistBottomSheet.arguments = bundle
//            addToWishlistBottomSheet.show(
//                (context as FragmentActivity).supportFragmentManager,
//                addToWishlistBottomSheet.tag
//            )
//        })
//    }

//    private fun observeStartLogin() {
//        segmentCategoryFragmentViewModel.startLoginEvent.observe(this, Observer {
//            context?.let {
//                ContextCompat.startActivity(
//                    it,
//                    Intent(context, LoginActivity::class.java), null
//                )
//            }
//        })
//    }

    override fun onWishListClick(view: View) {
        this.wishListView = view
    }

    override fun onCurrentItemChanged(holder: SubCategoryAdapter.ViewHolder?, p1: Int) {
//        holder?.currentPosition(holder, p1)
//        itemBinding?.picker?.post { subCategoryAdapter.notifyDataSetChanged() }
//        val subCategory = holder?.subCategory
//        segmentCategoryFragmentViewModel.loadCategoryProduct(
//            subCategory?.id ?: "",
//            "0",
//            "8",
//            "",
//            ""
//        )
//        itemBinding?.textViewSubCategory?.text = subCategory?.name
//        Toast.makeText(context, "$p1", Toast.LENGTH_SHORT).show()
        //TODO call api here
    }

    override fun onAddToWishlistSuccess() {
        (wishListView as ImageView?)?.setImageResource(R.drawable.ic_selected_wishlist)
        AppUtil.showToastySuccess(  context , R.string.add_to_wishlist)

    }
}
