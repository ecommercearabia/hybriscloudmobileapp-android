package com.erabia.foodcrowd.android.common.model


import android.os.Build
import android.text.Html
import android.text.Spanned
import androidx.room.*
import androidx.room.ForeignKey.NO_ACTION
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName


@Entity(tableName = "Product")
class Product() {
    @SerializedName("availableForPickup")
    var availableForPickup: Boolean? = null

    @SerializedName("averageRating")
    var averageRating: Float? = 0.0f

    @SerializedName("baseOptions")
    @TypeConverters(DataConverter::class)
    var baseOptions: List<BaseOption>? = null

    @SerializedName("baseProduct")
    var baseProduct: String? = null

    @SerializedName("categories")
    @TypeConverters(DataConverter::class)
    var categories: List<Category>? = null

    @SerializedName("classifications")
    @TypeConverters(DataConverter::class)
    var classifications: List<Classification>? = listOf()

    @SerializedName("code")
    @PrimaryKey
    var code: String = ""

    @SerializedName("configurable")
    var configurable: Boolean? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("images")
    @TypeConverters(DataConverter::class)
    var images: List<Image>? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("nutritionFacts")
    var nutritionFacts: String? = ""

    @SerializedName("numberOfReviews")
    var numberOfReviews: Int? = null

    @SerializedName("potentialPromotions")
    @TypeConverters(DataConverter::class)
    var potentialPromotions: List<PotentialPromotion>? = null

    @SerializedName("price")
    @TypeConverters(DataConverter::class)
    var price: Price? = null

    @SerializedName("priceRange")
    @TypeConverters(DataConverter::class)
    var priceRange: PriceRange? = null

    @SerializedName("productReferences")
    @TypeConverters(DataConverter::class)
    var productReferences: List<ProductReference>? = null

    @SerializedName("purchasable")
    var purchasable: Boolean? = null

    @SerializedName("reviews")
    @TypeConverters(DataConverter::class)
    @Ignore
    var reviews: List<Any>? = null

    @SerializedName("stock")
    @TypeConverters(DataConverter::class)
    var stock: Stock? = null

    @SerializedName("summary")
    var summary: String? = null

    @SerializedName("url")
    var url: String? = null

    @SerializedName("countryOfOrigin")
    var countryOfOrigin: String? = null

    @SerializedName("countryOfOriginIsocode")
    var countryOfOriginIsocode: String? = null

    @SerializedName("unitOfMeasure")
    var unitOfMeasure: String? = null

    @SerializedName("unitOfMeasureDescription")
    var unitOfMeasureDescription: String? = null

    @SerializedName("organic")
    var organic: Boolean? = null

    @SerializedName("glutenFree")
    var glutenFree: Boolean? = null

    @SerializedName("chilled")
    var chilled: Boolean? = null

    @SerializedName("dry")
    var dry: Boolean? = null

    @SerializedName("frozen")
    var frozen: Boolean? = null

    @SerializedName("express")
    var express: Boolean? = null

    @SerializedName("inWishlist")
    var inWishlist: Boolean? = null

    @SerializedName("vegan")
    var vegan: Boolean? = null

    @SerializedName("discount")
    @TypeConverters(DataConverter::class)
    var discount: Discount? = null

    @ForeignKey(
        entity = Component::class,
        parentColumns = ["uid"],
        childColumns = ["componentId"],
        onDelete = NO_ACTION
    )
    var componentId: String = ""


    fun getDescriptionHtml(): Spanned? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT)
        } else {
            return Html.fromHtml(description)
        }
    }
}