package com.erabia.foodcrowd.android.storecredit.model


import com.google.gson.annotations.SerializedName

data class AvailableBalanceResponse(
    @SerializedName("currencyIso")
    var currencyIso: String? = "",
    @SerializedName("formattedValue")
    var formattedValue: String? = "",
    @SerializedName("maxQuantity")
    var maxQuantity: Int? = 0,
    @SerializedName("minQuantity")
    var minQuantity: Int? = 0,
    @SerializedName("priceType")
    var priceType: String? = "",
    @SerializedName("value")
    var value: Double? = 0.0
)