package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ProductReference(
    @SerializedName("preselected")
    val preselected: Boolean = false,
    @SerializedName("referenceType")
    val referenceType: String = "",
    @SerializedName("target")
    val target: Target = Target()
)