package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class MobileCountry(
    @SerializedName("isdcode")
    var isdcode: String? = "",

    @SerializedName("isocode")
    var isocode: String? = "",
    @SerializedName("name")
    var name: String? = ""
) {
    fun findCountryByName(countryList: List<Country>?, name: String): String? {
        return countryList?.filter { it.name == name }?.firstOrNull()?.isocode
    }
    fun findCountryByCode(countryList: List<Country>?, code: String): String? {
        return countryList?.filter { it.isdcode == code }?.firstOrNull()?.isocode
    }
}
