package com.erabia.foodcrowd.android.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.ItemReviewListBinding
import com.erabia.foodcrowd.android.databinding.MyOrderRowItemBinding
import com.erabia.foodcrowd.android.product.model.ReviewsResponse
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel


class ReviewListAdapter(val myReviewList: List<ReviewsResponse.Review?>?,val mViewModel: ProductViewModel) :
    RecyclerView.Adapter<ViewHolder>() {
    private var context: Context? = null
    lateinit var mItemReviewListBinding: ItemReviewListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context

        val layoutInflater = LayoutInflater.from(parent.context)
        mItemReviewListBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.item_review_list, parent, false)
        return ViewHolder(
            mItemReviewListBinding
        )



    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
    override fun getItemCount(): Int {
        return myReviewList?.size?:0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.review = myReviewList?.get(position)
        holder.view.mOrderViewModel = mViewModel
        holder.view.postion = position
//        holder.mMyOrderRowItemBinding.mDatePlacedValueTextView.text =
//        AppUtil.formatDate(
//            "yyyy-MM-dd'T'HH:mm:ssZ",
//            "MMM dd, yyyy HH:mm a",
//            myOrderList?.get(position)?.placed
//        )
//        holder.mMyOrderRowItemBinding.mViewOrderButton.setOnClickListener {
//            context?.startActivity(Intent(context,OrderActivity::class.java))
//        }
    }

}

class ViewHolder(val view: ItemReviewListBinding) : RecyclerView.ViewHolder(view.root)

