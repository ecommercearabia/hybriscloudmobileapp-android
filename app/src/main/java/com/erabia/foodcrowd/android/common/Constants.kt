package com.erabia.foodcrowd.android.common

import com.erabia.foodcrowd.android.BuildConfig
import com.erabia.foodcrowd.android.R

object Constants {
    const val PLACE_API_KE = "AIzaSyCD2tZCjaNQeGfqhS2pJTmtywU7-AEM5hc"
    const val FILTER_KEY = "filter"
    const val REQUEST_FILTER_KEY = "requestFilter"
    const val ANONYMOUS = "anonymous"
    const val CURRENT = "current"
    const val BASE_URL = BuildConfig.BASE_URL
    const val FIELDS_FULL: String = "FULL"
    const val DEFAULT: String = "DEFAULT"
    const val COME_FROM = "COME_FROM"
    const val INSTAGRAM = "https://www.instagram.com/foodcrowduae/"
    const val YOUTUBE = "https://www.youtube.com/channel/UCjDqDnSIqx4_ssb_FvYqQog"
    const val GMAIL = "https://www.Gmail.com/"
    const val TWITTER = "https://twitter.com/"
    const val WHATSAPP = "https://api.whatsapp.com/send?phone="
    const val FACEBOOK = "https://www.facebook.com/foodcrowduae/"
    const val NUMBER_PHONE = "80036632"
    const val ARABIC = "عربي"
    const val ENGLISH = "English"
    object INTENT {
        const val NOTIFICATION_TYPE = "notificationType"
        const val PRODUCT_ID = "productId"

    }

    object API_KEY {
        const val EMAIL_CUSTOMER: String = "customer.email"
        const val TRANSACTIONID: String = "merchantTransactionId"
        const val AUTHORIZATION: String = "Authorization"
        const val ENTITY_ID: String = "entityId"
        const val LANG: String = "lang"
        const val AMOUNT: String = "amount"
        const val CURRENCY: String = "currency"
        const val PAYMENT_TYPE = "paymentType"
        const val NOTIFICATION_URL = "notificationUrl"
        const val RESOURCE_PATH: String = "resourcePath"
        const val PAGE_ID: String = "homepage"
        const val QUERY: String = "query"
        const val SORT: String = "sort"
        const val ENTRY_INFO: String = "entryInfo"
        const val ACCESS_TOKEN: String = "access_token"
        const val ENTRY_NUMBER: String = "entryNumber"
        const val CART_ID: String = "cartId"
        const val ADD_TO_CART_BODY: String = "addToCartBody"
        const val CATALOG_ID: String = "catalogId"
        const val ANDROID: String = "ANDROID"

        // const val CATALOG_ID_VALUE = "ykafb2cProductCatalog"
        const val CATALOG_ID_VALUE = "ygiordanob2cProductCatalog"
        const val CATEGORY_ID = "categoryId"
        const val CATEGORY_ID_VALUE = "1"
        const val CATEGORY_FIELDS = "fields"
        const val CATEGORY_CODE: String = "categoryCode"
        const val CURRENT_PAGE = "currentPage"
        const val PAGE_SIZE = "pageSize"
        const val FIELDS = "fields"
        const val APP_ID = "appId"
        const val APP_VERSION = "appVersion"
        const val ENV = "ENV"
        const val USER_ID = "userId"
        const val TOKEN = "token"
        const val TO_GET_DELIVERY_MODE = "toGetDeliveryMode"
        const val TO_GET_TIME_SLOT = "toGetTimeslot"
        const val TIME_SLOT_BODY = "timeSlotBody"
        const val TO_GET_PAYMENT_MODE = "toGetPaymentMode"
        const val ACCEPT = "accept"
        const val CONTENT_TYPE = "content-type"

    }

    object HYBRIS_TYPE {
        const val BANNERS = "ResponsiveRotatingImagesComponent"

        //        const val SEGMENT = "ErabiaCategoryCombinedOWLProductCarouselTabs"
        const val TAB = "TabbedProductCarouselComponent"
        const val BANNER = "SimpleResponsiveBannerComponent"
        const val HIGHLIGHTED_PRODUCTS = "ProductCarouselComponent"
//        const val HIGHLIGHTED_PRODUCTS_GRID = "ErabiaProductCarouselComponent"
    }

    class INTENT_KEY {
        companion object {
            const val FILE: String = "FILE"
            const val MOBILE_NUMBER: String = "mobile_number"
            const val COUNTRY_CODE: String = "country_code"
            const val SUB_CATEGORY_ONE: String = "category"
            const val URL: String = "url"
            const val MY_ITEMS: String = "my_items"
            const val STORE: String = "store"
            const val ORDER_CODE: String = "orderCode"
            const val BREAD_CRUMB: String = "breadCrumb"
            const val DEFAULT_ADDRESS: String = "defaultAddress"
            const val ADDRESS_ID: String = "addressId"
            const val ADD_ADDRESS: String = "addAddress"
            const val EDIT_ADDRESS: String = "editAddress"
            const val TITLE_LISTING: String = "titleListing"
            const val SOURCE_SCREEN: String = "sourceScreen"
            const val SOURCE_SEARCH_SCREEN = "searchScreen"
            const val SOURCE_CATEGORY_SCREEN = "categoryScreen"
            const val QUERY: String = "query"
            const val CART = "cart"
            const val FACETS = "facets"
            const val CATEGORY_CODE = "categoryCodeKey"
            const val PRODUCT = "product"
            const val PRODUCT_CODE = "productCode"
            const val FIRST_NAME = "first_name"
            const val LAST_NAME = "last-name "
            const val CODE_NUMBER = "code_number"
            const val BIRTHDAY = "birthday"
            const val EMAIL = "email"
            const val PASSWORD = "password"
            const val TITLE_CODE = "title_code"
            const val PHONE_NUMBER = "phone_num"
            const val GENDER_CODE = "gender_code"
            const val NATIONALITY_CODE = "nationality_code"
            const val REFERRAL_CODE = "referralCode"
            const val KAF_ACCOUNT_TYPE = ""
            const val UFC_ACCOUNT_TYPE = "ufcAccountType"
            const val UFC_TYPE = "ufcType"
            const val TERMS_CONDITION = "termsAndCondition"
            const val PROFILE_IMAGE = "profileImage"
            const val MENU = "menu"
            const val CHECKOUT = "checkout"
            const val CHECKOUT_BILLING = "checkoutBilling"
            const val ADDRESS_FORM = "addressForm"


        }
    }

    object CHECKOUT_SECTION {
        const val SHIPPING_METHOD = 0
        const val DELIVERY_TIME = 1
//        const val GIFT_CARD = 2
        const val STORE_CREDIT = 3
        const val PAYMENT_METHOD = 4
        const val ORDER_SUMMARY = 5
        const val SHIPPING_ADDRESS = 6
        const val MY_ITEMS = 7
        const val PROMO_CODE = 2
        const val TERMS_AND_CONDITION = 8
        const val CHECKOUT_BUTTON = 9
    }

    object NavigationAction {
        const val CHECKOUT = R.id.action_navigate_to_checkout
        const val ADDRESS = R.id.action_navigate_to_address

    }
}