package com.erabia.foodcrowd.android.home.remote.service

import com.erabia.foodcrowd.android.common.model.Component
import com.erabia.foodcrowd.android.common.model.Home
import com.erabia.foodcrowd.android.common.model.Product
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface HomeService {
    @GET("rest/v2/foodcrowd-ae/cms/pages/{pageId}")
    fun getHome(@Path("pageId") pageId: String): Observable<Response<Home>>


    @POST("rest/v2/foodcrowd-ae/users/{userId}/signature")
    fun setSignature(
        @Path("userId") userId: String,
        @Query("signatureId") signatureId: String
    ): Observable<Response<Void>>


    @GET("rest/v2/foodcrowd-ae/cms/components/{componentId}")
    fun getComponent(@Path("componentId") componentId: String): Observable<Response<Component>>

    @GET("rest/v2/foodcrowd-ae/products/{productCode}")
    fun getProduct(
        @Path("productCode") productCode: String,
        @Query("fields") fields: String
    ): Observable<Response<Product>>


    @PATCH("rest/v2/foodcrowd-ae/users/{userId}/mobile-token")
    fun setFirebaseToken(
        @Path("userId") userId: String,
        @Query("mobileToken") mobileToken: String
    ): Observable<Response<Product>>
}