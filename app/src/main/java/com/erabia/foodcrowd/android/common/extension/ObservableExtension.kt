package com.erabia.foodcrowd.android.common.extension

import android.content.ContextWrapper
import android.os.SystemClock
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.erabia.foodcrowd.android.common.repository.Repository
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import java.util.concurrent.TimeUnit

/**
 * run process async
 */
const val TAG = "ObservableExtension"
fun <T> Observable<Response<T>>.get(): Observable<Response<T>> {
    return subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<Response<T>>.subscribe(
    repository: Repository,
    onSuccess_200: (Response<T>) -> Unit,
    onSuccess_201: (Response<T>) -> Unit = {},
    onError_400: (ResponseType: Response<*>) -> Unit = {
        if (repository.getResponseErrorMessage(it) != "JaloObjectNoLongerValidError") {
            repository.error().postValue(repository.getResponseErrorMessage(it))
        }
    }, onError_401: (ResponseType: Response<*>) -> Unit = {
        if (repository.getResponseErrorMessage(it) != "JaloObjectNoLongerValidError") {
            repository.error().postValue(repository.getResponseErrorMessage(it))
        }

    }, onError_500: (ResponseType: Response<*>) -> Unit = {
        repository.error().postValue("Internal Server Error")
    },
    doOnSubscribe: () -> Unit = {
        repository.progressBarVisibility().value = View.VISIBLE
    }, doOnTerminate: () -> Unit = {
        repository.progressBarVisibility().value = View.GONE
        repository.hideRefreshLayout().value = true
    }
): Disposable {
    return this.doOnSubscribe { doOnSubscribe() }
        .doOnTerminate {
            doOnTerminate()
        }.subscribe({
            when (it.code()) {
                200 -> {
                    onSuccess_200(it)
                }
                201 -> {
                    onSuccess_201(it)
                }
                400 -> {
                    onError_400(it)
                }
                401 -> {
                    onError_401(it)
                }
                500, 501, 502 -> {
                    onError_500(it)
                }
                else -> {
                    repository.error().postValue("Something wrong happened")
                    Log.d(TAG, repository.getResponseErrorMessage(it) ?: "")
                }
            }
        }, {
            Log.d(TAG, it.message ?: "")
            if (it.message?.contains("Unable to resolve host") == true) {
                repository.error().postValue("Please check your internet connection")
                repository.progressBarVisibility().value = View.GONE
                repository.hideRefreshLayout().value = true
            }
        })
}

fun View.getParentActivity(): AppCompatActivity? {
    var context = this.context
    while (context is ContextWrapper) {
        if (context is AppCompatActivity) {
            return context
        }
        context = context.baseContext
    }
    return null
}

fun View.setSafeOnClickListener(onClick: (View) -> Unit) {
    this.clicks().throttleFirst(1000, TimeUnit.MILLISECONDS).subscribe {
        onClick(this)
    }
}

fun <T> Observable<T>.filterRapidClicks() = throttleFirst(500, TimeUnit.MILLISECONDS)
var lastClickTime: Long = 0

fun View.clickWithDebounce(debounceTime: Long = 2000L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action()

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}