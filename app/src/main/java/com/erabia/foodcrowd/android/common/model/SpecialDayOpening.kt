package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class SpecialDayOpening(
    @SerializedName("closed")
    val closed: Boolean = false,
    @SerializedName("closingTime")
    val closingTime: ClosingTime = ClosingTime(),
    @SerializedName("comment")
    val comment: String = "",
    @SerializedName("date")
    val date: String = "",
    @SerializedName("formattedDate")
    val formattedDate: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("openingTime")
    val openingTime: OpeningTime = OpeningTime()
)