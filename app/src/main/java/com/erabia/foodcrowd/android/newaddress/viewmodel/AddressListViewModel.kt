package com.erabia.foodcrowd.android.newaddress.viewmodel

import android.util.Patterns
import android.view.View
import androidx.lifecycle.*
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.model.Address
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.login.remote.repository.LoginRepository
import com.erabia.foodcrowd.android.newaddress.model.AddressList
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse

class AddressListViewModel(val mAddressRepository: AddressRepository) : ViewModel() {

    var mAddressListLiveData: LiveData<AddressList> = mAddressRepository.mAddressListLiveData
    var mDeleteAddressLiveData: LiveData<String> = mAddressRepository.mDeleteAddressLiveData
    var mNavigateFromAddressToCheckout: LiveData<Int> = mAddressRepository.navigateFromAddressToCheckout
    val loadingVisibility: MutableLiveData<Int> = mAddressRepository.loadingVisibility
    val progressBarVisibility: MutableLiveData<Int> = mAddressRepository.progressBarVisibility()
    val mErrorEvent: SingleLiveEvent<String> = mAddressRepository.error()
    var addAddressLiveData = SingleLiveEvent<Int>()
    var editAddressLiveData = SingleLiveEvent<Address>()

    fun getAddressList() {
        mAddressRepository.loadAddressList()
    }

    fun deleteAddress(addressID: String) {
        mAddressRepository.deleteAddress(addressID)
    }

    fun editAddress(address: Address) {
        editAddressLiveData.value = address
    }

    fun addAddressClick() {
        addAddressLiveData.value = R.id.action_addressListFragment_to_mapAddressFragment
    }

    fun itemAddressClick(view: View, from: String, address: Address) {
        if (from.equals(Constants.INTENT_KEY.CART) || from.equals(Constants.INTENT_KEY.CHECKOUT) ||
            from.equals(Constants.INTENT_KEY.CHECKOUT_BILLING)
        ) {
            view.setBackgroundResource(R.drawable.shape_rectangle_border)
            mAddressRepository.setCartDeliveryAddress(address)
        }
    }


    class Factory(
        val mAddressRepository: AddressRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AddressListViewModel(mAddressRepository) as T
        }

    }


}