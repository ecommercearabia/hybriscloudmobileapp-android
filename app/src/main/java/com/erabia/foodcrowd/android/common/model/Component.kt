package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

@Entity
class Component() {
    companion object {
        const val BANNERS = 0
        const val TAB = 1

        fun parseToComponent(contentSlotList: List<ContentSlot>?): List<Component> {
            val componentList = mutableListOf<Component>()
            contentSlotList?.forEach { contentSlot ->
                contentSlot.components.component.forEach {
                    componentList.add(it)
                }
            }
            return componentList
        }
    }

    @SerializedName("autoRotate")
    var autoRotate: String = ""

    @SerializedName("banners")
    var banners: String = ""

    @SerializedName("container")
    var container: String = ""

    @SerializedName("content")
    var content: String = ""

    @SerializedName("contentPage")
    var contentPage: String = ""

    @SerializedName("contentPageLabelOrId")
    var contentPageLabelOrId: String = ""

    @SerializedName("displayNavigation")
    var displayNavigation: String = ""

    @SerializedName("displayPagination")
    var displayPagination: String = ""

    @SerializedName("displayProductImages")
    var displayProductImages: String = ""

    @SerializedName("displayProducts")
    var displayProducts: String = ""

    @SerializedName("displaySuggestions")
    var displaySuggestions: String = ""

    @SerializedName("effect")
    var effect: String = ""

    @SerializedName("external")
    var `external`: String = ""

    @SerializedName("maxProducts")
    var maxProducts: String = ""

    @SerializedName("maxSuggestions")
    var maxSuggestions: String = ""

    @SerializedName("media")
    @TypeConverters(DataConverter::class)
    var media: Media = Media()

    @SerializedName("minCharactersBeforeRequest")
    var minCharactersBeforeRequest: String = ""

    @SerializedName("modifiedTime")
    var modifiedTime: String = ""

    @SerializedName("name")
    var name: String = ""

    @SerializedName("navigationNode")
    @TypeConverters(DataConverter::class)
    var navigationNode: NavigationNode = NavigationNode()

    @SerializedName("page")
    var page: String = ""

    @SerializedName("pauseInHover")
    var pauseInHover: String = ""

    @SerializedName("popup")
    var popup: String = ""

    @SerializedName("position")
    var position: String = ""

    @SerializedName("productCarouselComponents")
    var productCarouselComponents: String = ""

    @SerializedName("productCodes")
    var productCodes: String = ""

    @SerializedName("rotationSpeed")
    var rotationSpeed: String = ""

    @SerializedName("scroll")
    var scroll: String = ""

    @SerializedName("showLanguageCurrency")
    var showLanguageCurrency: String = ""

    @SerializedName("shownProductCount")
    var shownProductCount: String = ""

    @SerializedName("target")
    var target: String = ""

    @SerializedName("timeout")
    var timeout: String = ""

    @SerializedName("title")
    var title: String = ""

    @SerializedName("totalDisplay")
    var totalDisplay: String = ""

    @SerializedName("typeCode")
    var typeCode: String = ""

    @SerializedName("uid")
    @PrimaryKey
    var uid: String = ""

    @SerializedName("url")
    var url: String = ""

    @TypeConverters(DataConverter::class)
    @SerializedName("urlLink")
    var urlLink: Any? = null

    @SerializedName("uuid")
    var uuid: String = ""

    @SerializedName("waitTimeBeforeRequest")
    var waitTimeBeforeRequest: String = ""

    @SerializedName("wrapAfter")
    var wrapAfter: String = ""

    var componentType: Int = -1


}
