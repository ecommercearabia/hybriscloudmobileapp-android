package com.erabia.foodcrowd.android.loginwithotp.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.ActivityLoginWithOtpBinding
import com.erabia.foodcrowd.android.home.remote.service.LoginWithOtpService
import com.erabia.foodcrowd.android.loginwithotp.remote.repository.LoginWithOtpRepository
import com.erabia.foodcrowd.android.loginwithotp.viewmodel.LoginWithOtpViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.verificationcode.view.VerificationCodeActivity

class LoginWithOtpActivity : AppCompatActivity() {
    companion object {
        const val TAG = "LoginWithOtpActivity"
    }


    private lateinit var binding: ActivityLoginWithOtpBinding
    lateinit var mViewModel: LoginWithOtpViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_login_with_otp
        )

        val factory = LoginWithOtpViewModel.Factory(
            LoginWithOtpRepository(
                RequestManager.getClient().create(LoginWithOtpService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(LoginWithOtpViewModel::class.java)
        binding.viewModel = mViewModel
        observeSendOtp()
        observeNavigateToVerification()
        observeError()
        observeProgressBar()
    }

    private fun observeError() {
        mViewModel.error.observe(this, Observer {
            AppUtil.showToastyError(this , it)
        })
    }


    private fun observeNavigateToVerification() {
        mViewModel.navigateToVerificationCode.observe(this, Observer {
            AppUtil.closeKeyboard()
            val intent = Intent(this, VerificationCodeActivity::class.java)
            intent.putExtra(Constants.INTENT_KEY.EMAIL, mViewModel.email)
            startActivity(intent)
            finish()
        })
    }

    private fun observeSendOtp() {
        mViewModel.sendSuccessEvent.observe(this, Observer {
//            Toast.makeText(this, it.description, Toast.LENGTH_SHORT).show()
        })
    }


    private fun observeProgressBar() {
        mViewModel.progressBarVisibility.observe(this, Observer {
            binding.progressBar.visibility = it
        })
    }
}