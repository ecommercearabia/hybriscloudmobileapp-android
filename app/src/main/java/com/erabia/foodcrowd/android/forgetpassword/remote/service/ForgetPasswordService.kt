package com.erabia.foodcrowd.android.forgetpassword.remote.service

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface ForgetPasswordService {
    @POST("rest/v2/foodcrowd-ae/forgottenpasswordtokens")
    fun generateToken
                (@Query("userId") userId:String): Observable<Response<Void>>


}