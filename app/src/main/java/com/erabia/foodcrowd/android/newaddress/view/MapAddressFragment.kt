package com.erabia.foodcrowd.android.newaddress.view

import android.Manifest
import android.app.AlertDialog
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController

import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentMapAddressBinding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.newaddress.viewmodel.MapViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import permissions.dispatcher.*
import java.lang.Exception
import java.util.*

@RuntimePermissions
class MapAddressFragment : Fragment(), OnMapReadyCallback {
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    lateinit var mapFragment: SupportMapFragment
    lateinit var mMap: GoogleMap
    lateinit var mViewModel: MapViewModel
    lateinit var binding: FragmentMapAddressBinding
    lateinit var mFusedLocationProviderClient: FusedLocationProviderClient
    var mComeFrom = ""
    lateinit var mFrom_Add: String
    lateinit var addressId: String
    lateinit var geoAddress: Address

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mComeFrom = arguments?.getString(Constants.COME_FROM) ?: ""
        mFrom_Add = arguments?.getString("FROM_ADD") ?: ""
        addressId = arguments?.getString("addressId") ?: ""

        Log.e("Froooom", mComeFrom)
        val factory = MapViewModel.Factory()
        mViewModel = ViewModelProvider(this, factory).get(MapViewModel::class.java)
        mFusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_map_address, container, false)

        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getLocationWithPermissionCheck()

        mViewModel.outOfRangeObserver.observe(
            viewLifecycleOwner,
            Observer {
                if (it) {

                    if (mFrom_Add == "edit") {
                        findNavController().navigate(
                            R.id.action_mapAddressFragment_to_editAddressFragment,
                            bundleOf(
                                "COME_FROM" to mComeFrom,
                                "geo" to geoAddress,
                                "addressId" to addressId
                            )
                        )
                    } else if (mFrom_Add == "add") {
                        findNavController().navigate(
                            R.id.action_mapAddressFragment_to_addAddressFragment2,
                            bundleOf("COME_FROM" to mComeFrom, "geo" to geoAddress)
                        )
                    }

                } else {


                    AppUtil.showToastyWarning(context ,R.string.out_of_the_range)
                }
            })

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }

    @NeedsPermission(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun getLocation() {
        mapFragment.getMapAsync(this)
    }

    @OnShowRationale(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun explainWyNeeded(request: PermissionRequest) {
        showRationaleDialog(request)

    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun deniedPermission() {
        mapFragment.getMapAsync(this)
    }


    @OnNeverAskAgain(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
    fun onNeverAskAgainPermission() {
        mapFragment.getMapAsync(this)

    }

    fun setGeoAddress(lat: Double, lang: Double) {
        val geo = Geocoder(activity?.applicationContext, Locale.getDefault())
        val addresses: List<Address> = geo.getFromLocation(lat, lang, 1)
        if (addresses.isEmpty()) {
            mViewModel.outOfRang = false
        } else {
            if (addresses.size > 0) {
                geoAddress = addresses.get(0)
                val adminArea = addresses.get(0).countryName ?: ""
                mViewModel.outOfRang =
                    adminArea == "United Arab Emirates" || adminArea == "الإمارات العربية المتحدة"
            }
        }
    }

    fun getLocationData() {
        mFusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            var addresses: List<Address>? = null
            if (location != null) {
                val geo = Geocoder(context, Locale.getDefault())
                addresses =
                    geo.getFromLocation(location.latitude, location.longitude, 1)
                mMap.uiSettings.isMyLocationButtonEnabled = true
                mMap.isMyLocationEnabled = true


                AppUtil.showToastyWarning(context ,R.string.waiting_for_location)

            }

            if (addresses.isNullOrEmpty()) {
                mViewModel.outOfRang = false
            } else {
                if (addresses.isNotEmpty()) {
                    val adminArea = addresses.get(0).countryName ?: ""
                    if (adminArea == "United Arab Emirates" || adminArea == "الإمارات العربية المتحدة") {
                        val currentLatLng = LatLng(location.latitude, location.longitude)
                        mMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                currentLatLng,
                                15f
                            )
                        )
                        mViewModel.outOfRang = true

                    } else {
                        val currentLatLng = LatLng(24.466667, 54.366669)
                        mMap.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                currentLatLng,
                                7f
                            )
                        )
                        mViewModel.outOfRang = false

                    }
                }
            }

        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        try{
        if (p0 != null) {
            mMap = p0
        }
        getLocationData()
        var lastLat: Double = 24.466667
        var lastLong: Double = 54.366669
        var newLat: Double = 0.0
        var newLong: Double = 0.0
        mMap.setOnCameraIdleListener {
            newLat = p0?.cameraPosition?.target?.latitude ?: 0.0
            newLong = p0?.cameraPosition?.target?.longitude ?: 0.0
            latitude = p0?.cameraPosition?.target?.latitude ?: 0.0
            longitude = p0?.cameraPosition?.target?.longitude ?: 0.0
            val geo = Geocoder(context, Locale.getDefault())
            var addresses = geo.getFromLocation(latitude, longitude, 1)




            if (!addresses.isNullOrEmpty()){

            val adminArea = addresses.get(0).countryName ?: ""
            if (adminArea == "United Arab Emirates" || adminArea == "الإمارات العربية المتحدة") {
                setGeoAddress(newLat, newLong)
                lastLat = p0?.cameraPosition?.target?.latitude ?: 0.0
                lastLong = p0?.cameraPosition?.target?.longitude ?: 0.0
            } else {
//                setGeoAddress(lastLat, lastLong)
                val lastLatLng = LatLng(lastLat, lastLong)
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        lastLatLng,
                        15f
                    )
                )
                Toast.makeText(
                    requireContext(),
                    "Please Choose location inside UAE",
                    Toast.LENGTH_SHORT
                ).show()
            }
            }
            else{

                val lastLatLng = LatLng(lastLat, lastLong)

                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        lastLatLng,
                        15f
                    )
                )
                Toast.makeText(
                    requireContext(),
                    "Please Choose location inside UAE",
                    Toast.LENGTH_SHORT
                ).show()
            }

            }

        }catch (e: Exception){

        }
    }

    private fun showRationaleDialog(request: PermissionRequest) {
        AlertDialog.Builder(context)
            .setTitle("Set your location")
            .setMessage("location permission")
            .setPositiveButton(R.string.ok) { _, _ -> request.proceed() }
            .setCancelable(false)
            .create()
            .show()
    }

}