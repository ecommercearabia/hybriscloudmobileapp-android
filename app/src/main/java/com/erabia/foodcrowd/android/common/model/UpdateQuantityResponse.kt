package com.erabia.foodcrowd.android.common.model

data class UpdateQuantityResponse(
    val entry: Entry = Entry(),
    val quantity: Int = 0,
    val quantityAdded: Int = 0,
    val statusCode: String = ""
)
