package com.erabia.foodcrowd.android.referralcode.model

import com.google.gson.annotations.SerializedName


data class ReferralCodeModel(
    @SerializedName("code")
    var referralCode: String? = ""
)