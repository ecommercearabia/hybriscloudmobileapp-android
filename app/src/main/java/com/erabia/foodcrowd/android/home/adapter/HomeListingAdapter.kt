package com.erabia.foodcrowd.android.home.adapter

import android.graphics.Paint
import android.util.Log
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.DiscountPriceManager
import com.erabia.foodcrowd.android.common.util.ImageLoader
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ListingRowItemBinding
import com.erabia.foodcrowd.android.databinding.RowProgressBinding
import com.erabia.foodcrowd.android.home.callback.WishlistClickListener
import com.erabia.foodcrowd.android.home.viewmodel.HighLightedProductsViewModel
import com.erabia.foodcrowd.android.home.viewmodel.ProductItemViewModel
import kotlinx.coroutines.runBlocking


/**
 * @param maxItems -1 means infinite
 */
class HomeListingAdapter(
    val itemViewModel: HighLightedProductsViewModel,
    val viewModel: ProductItemViewModel,
    var withPagination: Boolean = true,
    var maxItems: Int = -1
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var totalResult: Int = 0
    val TYPE_ONE_ITEM: Int = 1
    var position: Int = 0
    var mProductItemCardView: CardView? = null
    val TYPE_GRID_TWO_ITEM: Int = 2
    val TYPE_PROGRESS: Int = 0
    val sparseArrayIsLoaded: SparseArray<Boolean?> = SparseArray()
    var wishlistClickListener: WishlistClickListener? = null
    private var quantitySaveStateArray: Array<Int?>? = null

    companion object {
        const val TAG = "ListingAdapter"
    }

    var productList: MutableList<Product>? = null
        set(value) {
            if (field == null)
                field = value
            else if (value != null) {
                (field)?.addAll(value)
            } else {
                field = value
            }
            quantitySaveStateArray = arrayOfNulls<Int>(value?.size ?: 0)
//            runBlocking {
            notifyDataSetChanged()
//            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        lateinit var binding: ViewDataBinding

        if (viewType == TYPE_ONE_ITEM) {
            binding = DataBindingUtil.inflate<ListingRowItemBinding>(
                layoutInflater,
                R.layout.listing_row_item,
                parent,
                false
            )
            return ViewHolder(binding)

        } else if (viewType == TYPE_GRID_TWO_ITEM) {
            binding = DataBindingUtil.inflate<ListingRowItemBinding>(
                layoutInflater,
                R.layout.listing_row_item,
                parent,
                false
            )
            return ViewHolder(binding)
        } else {
            binding = DataBindingUtil.inflate<RowProgressBinding>(
                layoutInflater, R.layout.row_progress, parent, false
            )
            return ProgressViewHolder(binding)
        }

    }

    override fun getItemCount(): Int {
        var listSize = productList?.size
        if (productList?.size != null && productList?.size != 0 && maxItems > 0 &&
            maxItems < (productList?.size ?: 0)
        ) {
            listSize = maxItems
        }
        return if (withPagination) {
            if (productList != null) (listSize ?: 0) + 1 else 0
        } else {
            if (productList != null) (listSize ?: 0) else 0
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        if (getItemViewType(position) == TYPE_ONE_ITEM) {
            holder as ViewHolder

//            productList?.get(position)?.let { viewModel.getProduct(it) }
            mProductItemCardView = holder.binding.mProductItemCardView
            this.position = position
            try {
                val url =
                    productList?.get(position)?.images?.firstOrNull() { it.format == "zoom" }?.url
                holder.binding.url = url
            } catch (e: Exception) {
                Log.d(TAG, "image issue : ${e.message}")
            }
            if ((sparseArrayIsLoaded.get(position) ?: false) != true) {
                holder.binding.viewProgress.visibility = View.VISIBLE
                holder.binding.progressbar.visibility = View.VISIBLE
                runBlocking {
                    productList?.get(position)?.let { itemViewModel.getProduct(it.code) }
                }
                sparseArrayIsLoaded.put(position, true)
            } else {
                holder.binding.viewProgress.visibility = View.GONE
                holder.binding.progressbar.visibility = View.GONE
            }

            (holder as ViewHolder).bind(
                viewModel,
                productList?.get(position) ?: Product(),
                position
            )

//            holder.binding.lifecycleOwner?.let {
//                itemViewModel.errorProductLiveData.observe(
//                    it,
//                    androidx.lifecycle.Observer { errorMessage ->
//                        Toast.makeText(holder.binding.root.context, errorMessage, Toast.LENGTH_LONG)
//                            .show()
//                        //notifyItemChanged() should called on ui thread , it will crashed if its called on
//                        //background thread
//
//                    })
//            }
        } else {
            (holder as ProgressViewHolder).itemBinding as RowProgressBinding
            when {
                productList?.isEmpty() == true -> {
                    holder.itemBinding.progressbar.visibility = View.GONE
                }
                itemCount - 1 == totalResult -> {
                    holder.itemBinding.progressbar.visibility = View.GONE
                }
                else -> {
                    holder.itemBinding.progressbar.visibility = View.VISIBLE

                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        Log.v("listingItemCount t", "$itemCount - position $position")
        return if (withPagination) {
            if ((position == itemCount - 1)) {
                TYPE_PROGRESS
            } else {
                TYPE_ONE_ITEM
            }
        } else TYPE_ONE_ITEM

    }

    override fun getItemId(position: Int): Long {
        return if (position < productList?.size ?: 0) {
            productList?.get(position)?.code?.hashCode()?.toLong() ?: 0
        } else {
            0
        }
    }

    fun reset() {
        productList?.clear()
    }

    fun resetToNull() {
        productList = null
    }

    fun updateItem(product: Product?) {

        productList?.forEachIndexed { index, it ->
            val isIndexMatched = it.code == product?.code
            if (isIndexMatched) {
                product?.let { productList?.set(index, it) }
                notifyItemChanged(index)
            }
        }

    }


    fun deleteNullItem(id: String) {

        val iterator: MutableIterator<Product>? = productList?.iterator()


//            for(item in productList?.size?:0){
//                println(item)
//            }

//        for(i in 0 until (productList?.size?.minus(1) ?: 0)) {
//            val isIndexMatched = productList?.get(i)?.code == id
//            if (isIndexMatched) {
//                productList?.remove(productList?.get(i))
//                notifyItemRangeRemoved(this.position,productList?.size?:0)
//            }
//        }

//        while (iterator?.hasNext() == true) {
////            iterator.asSequence().elementAt(position)
//
////            val value = iterator.next()
//            val isIndexMatched = iterator.asSequence().elementAt(position).code == id
//            if (isIndexMatched) {
////                iterator.remove()
//                productList?.removeAt(position)
//                notifyItemRemoved(position)
//            }
//        }
//        list?.forEachIndexed { index, it ->
//            val isIndexMatched = it.code == id
//            if (isIndexMatched) {
//                productList?.remove(it)
////                notifyItemRemoved(index)
//            }
//
//        }


    }


    inner class ViewHolder(val binding: ListingRowItemBinding) :
        RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {
        var product: Product? = null
        var quantity: Int = 1

        val textViewQuantity: TextView?
        var quantityPickerContainer: ConstraintLayout? = null
        var mPosition = -1

        init {
            binding.root.findViewById<ImageView>(R.id.image_view_wishlist)?.setOnClickListener(this)
            binding.root.findViewById<ImageView>(R.id.image_view_wishlist_selected_home)?.setOnClickListener(this)
            binding.root.findViewById<ImageView>(R.id.image_view_add)?.setOnClickListener(this)
            binding.root.findViewById<ImageView>(R.id.image_view_minus)?.setOnClickListener(this)

            binding.root.findViewById<ImageView>(R.id.button_add_to_cart)?.setOnClickListener(this)
            binding.textViewProductOldPrice.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            textViewQuantity = binding.root.findViewById<TextView>(R.id.text_view_quantity)
            quantityPickerContainer = binding.root.findViewById(R.id.cl_quantity_container)
        }

        fun bind(
            viewModel: ProductItemViewModel,
            product: Product,
            position: Int
        ) {
            binding.viewModel = viewModel
            binding.product = product
            binding.position = position
            this.mPosition = position



            try {
                binding.textViewNote.text =
                    "/ ${product.baseOptions?.get(0)?.selected?.variantOptionQualifiers?.get(0)?.value}"
                val discountPriceManager = DiscountPriceManager(product)
                discountPriceManager.setupPriceView(
                    binding.textViewPrice,
                    binding.textViewProductOldPrice,
                    binding.textViewDiscount
                )
                // to ensure there is default value in case of null
                if (quantitySaveStateArray?.get(position) == null) {
                    quantitySaveStateArray?.set(position, 1)
                }
                textViewQuantity?.text = quantitySaveStateArray?.get(position).toString()
            } catch (e: Exception) {
                Log.d(TAG, e.message ?: "")
            }
            try {
                val countryOfOrigin = if (product.countryOfOrigin?.isNotEmpty() == true) {
                    product.countryOfOrigin ?: ""
                } else {
                    product.countryOfOriginIsocode ?: ""
                }
                ImageLoader().setImageFlag(binding.imageViewFlag, countryOfOrigin)
                binding.textViewOrigin.text = countryOfOrigin
            } catch (e: Exception) {
                Log.d(TAG, e.message ?: "")
            }
            this.product = product
            //the product in this funtion  has only id when called first time
            //this condition to make sure we have all data so hide progress
            if (this.product?.name?.isNotEmpty() == true || this.product?.images?.isNotEmpty() == true) {
                binding.viewProgress.visibility = View.GONE
                binding.progressbar.visibility = View.GONE
            }

            // Classification Tags home page
            if (this.product!!.chilled == true) {
                binding.mChilledImageView.visibility = View.VISIBLE
            } else {
                binding.mChilledImageView.visibility = View.GONE
            }

            if (this.product!!.dry == true) {
                binding.mDryImageView.visibility = View.VISIBLE
            } else {
                binding.mDryImageView.visibility = View.GONE
            }

            if (this.product!!.frozen == true) {
                binding.mFrozenImageView.visibility = View.VISIBLE
            } else {
                binding.mFrozenImageView.visibility = View.GONE
            }

            if (this.product!!.express == true) {
                binding.expressDelivery.visibility = View.VISIBLE
            } else {
                binding.expressDelivery.visibility = View.GONE
            }

            // wishlist check in home
            if (this.product!!.inWishlist == true) {
                binding.imageViewWishlistSelectedHome.visibility = View.VISIBLE
                binding.imageViewWishlist.visibility = View.GONE
            } else {
                binding.imageViewWishlist.visibility = View.VISIBLE
                binding.imageViewWishlistSelectedHome.visibility = View.GONE
            }

//            val listingOutOfStock: View? = binding.root.findViewById<View>(R.id.listing_outOfStock)
            if ((this.product?.stock?.stockLevel ?: 0) > 0) {
                quantityPickerContainer?.visibility = View.VISIBLE
                binding.root.findViewById<ImageView>(R.id.button_add_to_cart).visibility =
                    View.VISIBLE

                binding.root.findViewById<AppCompatTextView>(R.id.mOutOfStokeTextView).visibility =
                    View.INVISIBLE

//                listingOutOfStock?.visibility = View.INVISIBLE
            } else {
                quantityPickerContainer?.visibility = View.INVISIBLE
                binding.root.findViewById<ImageView>(R.id.button_add_to_cart).visibility =
                    View.INVISIBLE
//
                binding.root.findViewById<AppCompatTextView>(R.id.mOutOfStokeTextView).visibility =
                    View.VISIBLE


//                Timer("SettingUp", false).schedule(500) {
//
//                }
//                Handler().postDelayed({
//                    if (product.name.isNullOrEmpty())
//                        binding.mProductItemCardView.visibility = View.GONE
//                }, 1000)

//                listingOutOfStock?.visibility = View.VISIBLE
            }
            val param = itemView.layoutParams as RecyclerView.LayoutParams
//                val params: ViewGroup.LayoutParams = binding.mProductItemCardView.layoutParams

//            binding.mProductItemCardView.layoutParams = params
//
//            if (product.name == null) {
////                binding.mProductItemCardView.visibility = View.GONE
////                val params: ViewGroup.LayoutParams = binding.mProductItemCardView.layoutParams
//                param.height = 0
//                param.width = 0
//                itemView.visibility = View.GONE;
//
//            }
//            else {
////                params.height = ViewGroup.LayoutParams.WRAP_CONTENT
//                param.height = LinearLayout.LayoutParams.WRAP_CONTENT;
//                param.width = LinearLayout.LayoutParams.WRAP_CONTENT;
//                itemView.visibility = View.VISIBLE;
//
////                binding.mProductItemCardView.visibility = View.VISIBLE
//
//            }

            binding.mProductItemCardView.layoutParams = param
        }


        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.image_view_wishlist -> {
                    viewModel.onWishlistClick(product?.code ?: "")
                    v?.let {
                        var userLogin = SharedPreference.getInstance().getLoginEmail() ?: ""
                        if (userLogin.isNotEmpty()) {
                            binding.imageViewWishlistSelectedHome.visibility = View.VISIBLE
                            binding.imageViewWishlist.visibility = View.GONE
                        }
                        wishlistClickListener?.onWishListClick(it)
                    }
                }
                R.id.image_view_wishlist_selected_home -> {
                    binding.imageViewWishlist.visibility = View.VISIBLE
                    binding.imageViewWishlistSelectedHome.visibility = View.GONE
                    viewModel.onWishlistClickDelete(product?.code ?: "")
                }
                R.id.image_view_minus -> {
                    if (quantity > 1) {
                        quantity--
                        textViewQuantity?.text = quantity.toString()
                        quantitySaveStateArray?.set(mPosition, quantity)
                    }
                }
                R.id.image_view_add -> {
                    if (quantity < this.product?.stock?.stockLevel ?: 0) {
                        quantity++
                        textViewQuantity?.text = quantity.toString()
                        quantitySaveStateArray?.set(mPosition, quantity)
                    }
//                        Toast.makeText(
//                            MyApplication.getContext(),
//                            R.string.out_of_stock,
//                            Toast.LENGTH_SHORT
//                        ).show()
                }

                R.id.button_add_to_cart -> {
                    this.product?.let { viewModel.onAddToCartClick(it, quantity) }
                }
            }
        }
    }

    class ProgressViewHolder(val itemBinding: RowProgressBinding) :
        RecyclerView.ViewHolder(itemBinding.root)


}