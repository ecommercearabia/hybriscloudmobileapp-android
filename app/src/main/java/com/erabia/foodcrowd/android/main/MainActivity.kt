package com.erabia.foodcrowd.android.main

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.view.CategoryFragment
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.setupWithNavController
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityMainBinding
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallState
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*


class MainActivity : AppCompatActivity(), InstallStateUpdatedListener {
    companion object {
        const val TAG = "MainActivity"
        const val isFirstTimeLogin = true
    }

    lateinit var badgeDrawable: BadgeDrawable
    private var appUpdateManager: AppUpdateManager? = null
    private var currentNavController: LiveData<NavController>? = null
    private var mBottomNavigationView: BottomNavigationView? = null
    private val UPDATE_REQUEST_CODE: Int = 0
    private var navController: NavController? = null
    lateinit var binding: ActivityMainBinding
    lateinit var toolbar: Toolbar
    private lateinit var gestureDetector: GestureDetector
    var startScale: Float = 1.0f
    var categoryLayoutType = CategoryLayoutType.TWO_ITEM

    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        LocaleHelper.setLocale(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        supportActionBar?.setDisplayHomeAsUpEnabled(true);

        val mBottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNavigationView)
//        SharedPreference.getInstance().setUser_TOKEN("Bearer ztXAofz1jIOx2Uuo3iooN_3w1Nw")

        toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = ""
        setSupportActionBar(toolbar)
//        val navHostFragment: Fragment? =
//            supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
//        val currentFragment =
//            navHostFragment?.childFragmentManager?.fragments?.get(0)
//        if (currentFragment is CategoryFragment) {
//            currentFragment.changeLayoutManager(4)
//        }

        mBottomNavigationView.setOnNavigationItemReselectedListener { // Do nothing to ignore the reselection
        }


        if (savedInstanceState == null) {
            setupBottomNavigationBar(mBottomNavigationView)
        }

        inAppUpdate()
        initKeyboardVisibilityListener()
        gestureDetector = GestureDetector(this, GestureListener())
        initZoomGesture()
        getTokenFirebaseMessaging()
        badgeDrawable = mBottomNavigationView.getOrCreateBadge(R.id.base_checkout)// menu item id
//        badgeDrawable.isVisible = false
        badgeDrawable.backgroundColor = ContextCompat.getColor(this, R.color.badgeBackgroundColor)
        badgeDrawable.badgeTextColor = ContextCompat.getColor(this, R.color.badgeTextColor)

        LocaleHelper.getLanguage(this)?.let { Log.d("lang", it) }
    }

    private fun initKeyboardVisibilityListener() {
        binding.container.viewTreeObserver.addOnGlobalLayoutListener(OnGlobalLayoutListener {
            val heightDiff: Int = binding.container.rootView.height - binding.container.height
            Log.d(
                TAG,
                "initKeyboardVisibilityListener: rootViewHeight = ${binding.container.rootView.height} viewHeight = ${binding.container.height}"
            )
            if (heightDiff > AppUtil.dpToPx(this, 200)) {
                //keyboard is open
                binding.bottomNavigationView.visibility = View.GONE
            } else {
                //keyboard is hide
                binding.bottomNavigationView.visibility = View.VISIBLE
            }
        })
    }

    private fun getTokenFirebaseMessaging() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            SharedPreference.getInstance().setFIREBASE_TOKEN(token.toString())
            // Log and toast
//            val msg = getString(R.string.msg_token_fmt, token)
            Log.d(TAG, "FCM token : ${token.toString()}")
//            Toast.makeText(baseContext, token.toString(), Toast.LENGTH_SHORT).show()
        })


    }


    private suspend fun setupDeepLinkNotification() {
        try {
            val notificationType = intent.getStringExtra(Constants.INTENT.NOTIFICATION_TYPE)
            val id = intent.getStringExtra(Constants.INTENT_KEY.PRODUCT_CODE)
            val test = intent.getStringExtra("testKey")
            Log.d(
                TAG,
                "onCreate: notificationType : $notificationType"
            )
            Log.d(
                TAG,
                "onCreate: meetingId : $id"
            )
            Log.d(
                TAG,
                "onCreate: test : $test"
            )
            if (this.intent != null && notificationType != null
                && id != null && !id.isEmpty()
            ) {
                val bundle = Bundle()
//                val navController = findNavController(R.id.nav_host_container);
//                binding.bottomNavigationView.selectedItemId = R.id.home
                delay(10)
//                Timer().schedule(object : TimerTask() {
//                    override fun run() {
                when (notificationType) {
                    "product" -> {
//                                binding.bottomNavigationView.selectedItemId = R.id.home

                        bundle.putString(Constants.INTENT_KEY.PRODUCT_CODE, id)
                        navController?.navigate(
                            R.id.action_navigate_to_details,
                            bundle
                        )
                    }
                    "brand", "listing", "category" -> {
//                                binding.bottomNavigationView.selectedItemId = R.id.category

                        bundle.putString(Constants.API_KEY.CATEGORY_ID, id)
                        navController?.navigate(
                            R.id.action_navigate_to_category,
                            bundle
                        )
                    }
                }
//                    }
//                }, 50)
            }
        } catch (e: Exception) {
            Log.e(TAG, "onResume: ${e.message}")
        }
    }

    override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            setupDeepLinkNotification()
        }
    }

    private fun inAppUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(this)

        // Returns an intent object that you use to check for an update.


        appUpdateManager?.registerListener(this)


        // Checks that the platform will allow the specified type of update.
        appUpdateManager?.appUpdateInfo?.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                // For a flexible update, use AppUpdateType.FLEXIBLE
                && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
            ) {
                // Request the update.


                AppUtil.showToastyWarning(this ,R.string.force_update)

                appUpdateManager?.startUpdateFlowForResult(
                    appUpdateInfo, AppUpdateType.IMMEDIATE, this, UPDATE_REQUEST_CODE
                );

            }
        }
    }
//    override fun onBackPressed() {
//        super.onBackPressed()
//        Log.e(TAG, "onBackPressed: $navController.currentDestination?.id")
//        if (navController?.currentDestination?.id == R.id.categoryFragment) {
//            setSupportActionBar(toolbar)
//            supportActionBar?.setDisplayHomeAsUpEnabled(true);
//            supportActionBar?.setDisplayShowHomeEnabled(true);
//
//        }
//    }

    private fun setupBottomNavigationBar(mBottomNavigationView: BottomNavigationView) {

        val navGraphIds = listOf(
            R.navigation.home,
            R.navigation.category,
            R.navigation.base_checkout,
            R.navigation.community,
            R.navigation.menu
        )

        // Setup the bottom navigation view with a list of navigation graphs
        val controller = mBottomNavigationView.setupWithNavController(
            navGraphIds = navGraphIds,
            fragmentManager = supportFragmentManager,
            containerId = R.id.nav_host_fragment,
            intent = intent
        )


        // Whenever the selected controller changes, setup the action bar.
        controller.observe(this, Observer { navController ->
            setupActionBarWithNavController(navController)
            this.navController = navController
            setupWithNavController(toolbar, navController)
            toolbar.title = ""

//            when(navController.currentDestination?.id){
//                R.id.menuScreen->
//                    toolbar.visibility= View.GONE
//
//                else->
//                    toolbar.visibility= View.VISIBLE
//            }
        })
        currentNavController = controller
    }

    override fun onStateUpdate(state: InstallState) {
        if (state.installStatus() == InstallStatus.DOWNLOADED) {
            popupSnackbarForCompleteUpdate()
        } else {
//            AppUtil.showToastyError(this, "some error happened")

        }
    }

    fun popupSnackbarForCompleteUpdate() {
        Snackbar.make(
            findViewById(R.id.nav_host_fragment),
            "An update has just been downloaded.",
            Snackbar.LENGTH_INDEFINITE
        ).apply {
            setAction("RESTART") { appUpdateManager?.completeUpdate() }
            setActionTextColor(resources.getColor(R.color.app_color))
            show()
        }
    }

    private lateinit var mScaleDetector: ScaleGestureDetector

    private fun initZoomGesture() {
        mScaleDetector = ScaleGestureDetector(
            this,
            object : ScaleGestureDetector.OnScaleGestureListener {
                override fun onScale(detector: ScaleGestureDetector?): Boolean {
                    this@MainActivity.startScale = detector?.scaleFactor ?: 1.0f

                    return true
                }

                override fun onScaleBegin(p0: ScaleGestureDetector?): Boolean {
                    return true
                }

                override fun onScaleEnd(detector: ScaleGestureDetector?) {
                    val endScale = detector?.scaleFactor ?: 1.0f
                    val navHostFragment: Fragment? =
                        supportFragmentManager.findFragmentById(R.id.nav_host_fragment)
                    val currentFragment =
                        navHostFragment?.childFragmentManager?.fragments?.get(0)
                    if (currentFragment is CategoryFragment) {// do something with f
                        if (startScale > endScale) {
                            //Zoom in
                            if (categoryLayoutType == CategoryLayoutType.FOUR_ITEM) {
                                categoryLayoutType = CategoryLayoutType.THREE_ITEM
                                currentFragment.changeLayoutManager(3)
                            } else if (categoryLayoutType == CategoryLayoutType.THREE_ITEM) {
                                categoryLayoutType = CategoryLayoutType.TWO_ITEM
                                currentFragment.changeLayoutManager(2)
                            } else if (categoryLayoutType == CategoryLayoutType.TWO_ITEM) {
                                categoryLayoutType = CategoryLayoutType.ONE_ITEM
                                currentFragment.changeLayoutManager(1)
                            }
                        } else if (startScale < endScale) {
                            //Zoom out
                            if (categoryLayoutType == CategoryLayoutType.ONE_ITEM) {
                                categoryLayoutType = CategoryLayoutType.TWO_ITEM
                                currentFragment.changeLayoutManager(2)
                            } else if (categoryLayoutType == CategoryLayoutType.TWO_ITEM) {
                                categoryLayoutType = CategoryLayoutType.THREE_ITEM
                                currentFragment.changeLayoutManager(3)
                            } else if (categoryLayoutType == CategoryLayoutType.THREE_ITEM) {
                                categoryLayoutType = CategoryLayoutType.FOUR_ITEM
                                currentFragment.changeLayoutManager(4)
                            }
                        }
                    }


                }

            })


    }

    override fun dispatchTouchEvent(event: MotionEvent?): Boolean {
        super.dispatchTouchEvent(event)
        mScaleDetector.onTouchEvent(event)
        gestureDetector.onTouchEvent(event)
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            return true
        }
    }

    enum class CategoryLayoutType {
        ONE_ITEM,
        TWO_ITEM,
        THREE_ITEM,
        FOUR_ITEM,
        SIX_ITEM
    }

    //HMS push notification
//    private fun getHMSToken() {
//        object : Thread() {
//            override fun run() {
//                try {
//                    // read from agconnect-services.json
//                    val appId = AGConnectServicesConfig.fromContext(this@MainActivity)
//                        .getString("client/app_id")
//                    val token = HmsInstanceId.getInstance(this@MainActivity).getToken(appId, "HCM")
//                    Log.d(TAG, "get token:$token")
//                    if (!TextUtils.isEmpty(token)) {
//                        sendRegTokenToServer(token)
//                    }
//                } catch (e: ApiException) {
//                    Log.d(TAG, "get token failed, $e")
//                }
//            }
//        }.start()
//    }

    private fun sendRegTokenToServer(token: String) {
        Log.d(TAG, "sending token to server. token:$token")
    }

}