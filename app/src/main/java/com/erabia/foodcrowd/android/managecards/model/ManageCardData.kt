package com.erabia.foodcrowd.android.managecards.model

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

data class ManageCardData (
    @SerializedName("customerPaymentOptions")
    @Expose
    var customerPaymentOptions: List<CustomerPaymentOption>? = listOf()
)