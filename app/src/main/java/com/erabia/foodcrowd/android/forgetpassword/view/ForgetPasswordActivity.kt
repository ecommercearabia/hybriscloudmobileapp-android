package com.erabia.foodcrowd.android.forgetpassword.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.ActivityForgetPasswordBinding
import com.erabia.foodcrowd.android.forgetpassword.remote.repositry.ForgetPasswordRepository
import com.erabia.foodcrowd.android.forgetpassword.remote.service.ForgetPasswordService
import com.erabia.foodcrowd.android.forgetpassword.viewmodel.ForgetPasswordViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_forget_password.*

class ForgetPasswordActivity : AppCompatActivity() {
    val compositeDisposable = CompositeDisposable()
    private lateinit var mViewModel: ForgetPasswordViewModel

    private lateinit var binding: ActivityForgetPasswordBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forget_password)
        setSupportActionBar(toolbar)

        val factory = ForgetPasswordViewModel.Factory(
            ForgetPasswordRepository(
                RequestManager.getClient().create(ForgetPasswordService::class.java)
            )
        )

        mViewModel = ViewModelProvider(this, factory).get(ForgetPasswordViewModel::class.java)

        binding.lifecycleOwner = this
        binding.mForgetPasswordViewModel = mViewModel

        toolbar.setNavigationOnClickListener {
            finish()
        }
        mViewModel.mErrorValidationMessage.observe(this, Observer {
            mEmailForgetPassowrdEditText.error = it
        })
        mViewModel.mGeneratTokenLiveData.observe(this, Observer {
            SharedPreference.getInstance().setFingerPrintEnabled(false)
            SharedPreference.getInstance().setIsLoginFirstTime(true)
            AppUtil.showToastySuccess(  this , R.string.success_message_for_forget_password)
            setResult(RESULT_OK)
            finish()
        })

        mViewModel.mErrorEvent.observe(this, Observer {
            AppUtil.showToastyError(this , it)

        })

    }


}