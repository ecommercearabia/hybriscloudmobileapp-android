package com.erabia.foodcrowd.android.home.callback

import android.view.View

interface WishlistClickListener {
    fun onWishListClick(view: View)

}