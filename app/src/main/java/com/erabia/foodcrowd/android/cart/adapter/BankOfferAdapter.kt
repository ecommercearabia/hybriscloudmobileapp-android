package com.erabia.foodcrowd.android.cart.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.common.model.BankOfferResponse
import com.erabia.foodcrowd.android.common.model.Entry
import com.erabia.foodcrowd.android.databinding.BankOfferRowItemBinding
import com.erabia.foodcrowd.android.databinding.CartRowItemBinding
import kotlinx.android.synthetic.main.cart_row_item.view.*

class BankOfferAdapter() :
    RecyclerView.Adapter<BankOfferAdapter.ViewHolder>() {
    private val TAG: String = "CartAdapter"
    private var context: Context? = null
    var mBankOfferList: List<BankOfferResponse.BankOffer?>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var bankOfferRowItemBinding: BankOfferRowItemBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        bankOfferRowItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.bank_offer_row_item, parent, false)
        return ViewHolder(bankOfferRowItemBinding)
    }

    override fun getItemCount(): Int {
        return mBankOfferList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val entry = mBankOfferList?.get(position)
//        val product = entry?.product
        holder.bind(entry)

    }


    override fun getItemId(position: Int): Long {
        return mBankOfferList?.get(position)?.code.hashCode().toLong()
    }
    inner class ViewHolder(val bankOfferRowItemBinding: BankOfferRowItemBinding) :
        RecyclerView.ViewHolder(bankOfferRowItemBinding.root) {
        fun bind(entry: BankOfferResponse.BankOffer?) {
            bankOfferRowItemBinding.bankOffer = entry
//            itemView.text_view_product_quantity.text = entry?.quantity.toString()
        }
    }


}