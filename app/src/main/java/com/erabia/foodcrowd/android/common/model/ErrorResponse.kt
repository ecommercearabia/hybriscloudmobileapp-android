package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName


data class ErrorResponse(
    @SerializedName("errors")
    val errors: List<Error> = listOf()
)