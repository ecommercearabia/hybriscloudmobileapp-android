package com.erabia.foodcrowd.android.wishlist

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.main.MainActivity
import kotlinx.android.synthetic.main.fragment_about_us.*
import kotlinx.android.synthetic.main.fragment_wishlist.*

class WishlistFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wishlist, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
        val url = "https://blog.foodcrowd.com/"
        mWhiteListWebView.settings.javaScriptEnabled = true
        mWhiteListWebView.webChromeClient = WebChromeClient()
        mWhiteListWebView.loadUrl(url)
    }
}