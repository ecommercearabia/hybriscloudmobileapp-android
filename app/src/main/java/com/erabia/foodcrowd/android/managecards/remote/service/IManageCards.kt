package com.erabia.foodcrowd.android.managecards.remote.service

import com.erabia.foodcrowd.android.managecards.model.ManageCardData
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IManageCards {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/customer-payment-options")
    fun getSavedCards(
        @Path("userId") userId: String,
        @Query("fields") fields: String
    ): Observable<Response<ManageCardData>>

    @DELETE("rest/v2/foodcrowd-ae/users/{userId}/customer-payment-options/{customerCardId}")
    fun deleteSavedCard(
        @Path("userId") userId: String,
        @Path("customerCardId") customerCardId: String
    ): Observable<Response<Void>>
}