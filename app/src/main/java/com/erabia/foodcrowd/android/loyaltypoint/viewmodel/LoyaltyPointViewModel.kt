package com.erabia.foodcrowd.android.loyaltypoint.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.loyaltypoint.remote.repository.LoyaltyPointRepository
import com.erabia.foodcrowd.android.loyaltypoint.model.LoyaltyPointData

class LoyaltyPointViewModel (var loyaltyPointRepository: LoyaltyPointRepository) : ViewModel(){

    val pointList: LiveData<LoyaltyPointData> = loyaltyPointRepository.loyaltyPointLiveData
    val pointAmountList: LiveData<String> = loyaltyPointRepository.loyaltyPointAmountLiveData

    fun getLoyaltyPointList() {
        loyaltyPointRepository.loadLoyaltyPoint(SharedPreference.getInstance().getLoginEmail() ?: "")
    }

    fun getLoyaltyPointAmount() {
        loyaltyPointRepository.loadLoyaltyPointAmount(SharedPreference.getInstance().getLoginEmail() ?: "")
    }

    class Factory(
        private val loyaltyPointRepository: LoyaltyPointRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoyaltyPointViewModel(loyaltyPointRepository) as T
        }
    }
}