package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("uid")
    val uid: String = ""
)