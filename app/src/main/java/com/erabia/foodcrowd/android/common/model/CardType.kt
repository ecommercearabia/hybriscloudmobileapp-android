package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class CardType(
    @SerializedName("code")
    var code: String = "",
    @SerializedName("name")
    var name: String = ""
)