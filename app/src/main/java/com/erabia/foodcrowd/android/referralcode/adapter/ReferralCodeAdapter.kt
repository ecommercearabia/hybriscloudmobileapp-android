package com.erabia.foodcrowd.android.referralcode.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.ReferralCodeRowBinding
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeResponse
import java.text.DecimalFormat

class ReferralCodeAdapter(val referralList: List<ReferralCodeResponse.Histories?>) :
    RecyclerView.Adapter<ViewHolder>() {

    private var context: Context? = null

    lateinit var mReferralCodeRowBinding: ReferralCodeRowBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val layoutInflater = LayoutInflater.from(parent.context)
        mReferralCodeRowBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.referral_code_row, parent, false)
        return ViewHolder(
            mReferralCodeRowBinding
        )
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return referralList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mReferralCodeRowBinding.referral = referralList[position]
        val dec = DecimalFormat("#,#00.00#")
        holder.mReferralCodeRowBinding.amountTextView.text =
            dec.format(referralList[position]?.amount).toString()
        holder.mReferralCodeRowBinding.dateTextView.text = AppUtil.formatDate(
            "yyyy-MM-dd'T'HH:mm:ssZ",
            "dd-MM-yyyy",
            referralList[position]?.deactivationDate
        )
    }
}

class ViewHolder(val mReferralCodeRowBinding: ReferralCodeRowBinding) :
    RecyclerView.ViewHolder(mReferralCodeRowBinding.root)