package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class Currency(
    @SerializedName("active")
    val active: Boolean = false,
    @SerializedName("isocode")
    val isocode: String = "",
    @SerializedName("name")
    val name: String = "",
    @SerializedName("symbol")
    val symbol: String = ""
)