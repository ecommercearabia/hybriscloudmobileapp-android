package com.erabia.foodcrowd.android.common.util

import android.content.Context
import android.util.DisplayMetrics
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import es.dmoral.toasty.Toasty
import java.sql.Timestamp
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt


object AppUtil {
    fun formatTimeStamp(timeStamp: Long, newFormat: String?): String? {
        val timestamp = Timestamp(timeStamp)
        val date = Date(timestamp.time)
        val mDateFormat: DateFormat =
            SimpleDateFormat(newFormat, Locale.ENGLISH)
        return mDateFormat.format(date)
    }


    fun formatDate(
        oldFormat: String?,
        newFormat: String?,
        dateValue: String?
    ): String? {
        val dateFormat: DateFormat =
            SimpleDateFormat(oldFormat, Locale.ENGLISH)
        var date: Date? = null
        try {
            date = dateFormat.parse(dateValue)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val mDateFormat: DateFormat =
            SimpleDateFormat(newFormat, Locale.ENGLISH)
        return mDateFormat.format(date)
    }

    fun showKeyboard() {
        val inputMethodManager: InputMethodManager =
            MyApplication.getContext()
                ?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun closeKeyboard() {
        val inputMethodManager: InputMethodManager =
            MyApplication.getContext()
                ?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    fun dpToPx(context: Context, dp: Int): Int {
        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        return (dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    fun getMobileNumberWithoutIsoCode(mobileNumber: String): String {
        return if (mobileNumber.length >= 12) {
            mobileNumber.replace("971", "")
        } else {
            mobileNumber
        }
    }

    fun showToastySuccess( ctx: Context?, text : Int) {

        Toasty.custom(ctx!!,text,null, R.color.colorPrimary
            , R.color.white,
            Toast.LENGTH_SHORT , false , true).show()
    }

    fun showToastySuccessReferral( ctx: Context?, text : String , color : Int , textColor : Int) {

        Toasty.custom(ctx!!,text,null, color
            , textColor,
            Toast.LENGTH_LONG , false , true).show()
    }

    fun showToastyError(ctx: Context?, text : String?) {
        Toasty.error(ctx!!,text ?: "",Toast.LENGTH_SHORT,false).show()
    }

    fun showToastyWarning(ctx: Context?, text : Int) {

        Toasty.warning(ctx!!,text,Toast.LENGTH_SHORT,false).show()
    }
}