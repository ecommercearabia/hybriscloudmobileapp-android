package com.erabia.foodcrowd.android.common.model

data class BreadCrumb(
    val facetCode: String? = null,
    val facetName: String? = null,
    val facetValueCode: String? = null,
    val facetValueName: String? = null,
    val removeQuery: RemoveQuery? = null,
    val truncateQuery: TruncateQuery? = null
)