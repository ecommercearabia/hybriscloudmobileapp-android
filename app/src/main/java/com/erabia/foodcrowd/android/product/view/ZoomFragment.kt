package com.erabia.foodcrowd.android.product.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Image
import com.erabia.foodcrowd.android.databinding.FragmentZoomBinding
import com.erabia.foodcrowd.android.product.adapter.ZoomImagesAdapter


class ZoomFragment : Fragment(), ZoomImagesAdapter.OnImageClickListener {
    private var selectedPosition: Int = 0
    private var binding: FragmentZoomBinding? = null
    var url: String = ""
    var urlList: List<Image>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        url = arguments?.getString("url", "") ?: ""
        urlList = arguments?.get("imageList") as List<Image>?
        selectedPosition = arguments?.getInt("selectedPosition", 0) ?: 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_zoom,
            container,
            false
        )
        binding?.url = url
        val adapter = ZoomImagesAdapter()
        adapter.selectedItem = selectedPosition
        adapter.urlList = urlList
        adapter.onImageClickListener = this
        binding?.recyclerView?.adapter = adapter
        return binding?.root
    }

    override fun onImageClick(url: String?) {
        if (url != null) {
            binding?.url = url
        }
    }

}