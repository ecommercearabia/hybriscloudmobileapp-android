package com.erabia.foodcrowd.android.product.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentAddReviewBinding
import com.erabia.foodcrowd.android.databinding.FragmentMyOrderBinding
import com.erabia.foodcrowd.android.order.viewmodel.MyOrderViewModel
import com.erabia.foodcrowd.android.product.di.DaggerIProductComponent
import com.erabia.foodcrowd.android.product.di.IProductComponent
import com.erabia.foodcrowd.android.product.di.ProductModule
import com.erabia.foodcrowd.android.product.viewmodel.ProductViewModel
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import javax.inject.Inject

class AddReviewFragment : Fragment() {
    lateinit var mViewModel: ProductViewModel
    lateinit var binding: FragmentAddReviewBinding
    lateinit var daggerComponent: IProductComponent
    private var productCode: String = ""
    private var rate: Double = 0.0


    @Inject
    lateinit var factory: ProductViewModel.Factory
    var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        daggerComponent =
            DaggerIProductComponent.builder().productModule(context?.let { ProductModule(it) })
                .build()
        daggerComponent.inject(this)
        productCode = ""
        arguments?.let { productCode = it.getString(Constants.INTENT_KEY.PRODUCT_CODE) ?: "" }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_review, container, false)
        mViewModel = ViewModelProvider(this, factory).get(ProductViewModel::class.java)
        binding.viewModel = mViewModel
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerValidationError()
        observerAddReviewSuccess()
        binding.ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            rate = rating.toDouble()
        }
        binding.mWriteReviewButton.clicks().filterRapidClicks().subscribe {
            mViewModel.addReview(productCode, rate)

        }.addTo(compositeDisposable)
    }


    fun observerValidationError() {
        mViewModel.mErrorTitleValidationMessage.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })

        mViewModel.mErrorRateValidationMessage.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })

        mViewModel.error.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })
    }

    fun observerAddReviewSuccess() {
        mViewModel.addReviewResponseLiveData.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context ,R.string.success)
            activity?.onBackPressed()
        })
    }


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
}