package com.erabia.foodcrowd.android.product.model

import com.erabia.foodcrowd.android.common.model.Product
import retrofit2.Response

sealed class ZipModelReviewProductResponse {

    data class SuccessProduct(val data: Response<Product>) : ZipModelReviewProductResponse()
    data class SuccessReviewList(val data: Response<ReviewsResponse>) : ZipModelReviewProductResponse()
    data class Error(val t: Throwable) : ZipModelReviewProductResponse()
}