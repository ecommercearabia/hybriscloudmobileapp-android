package com.erabia.foodcrowd.android.loyaltypoint.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.databinding.FragmentLoyaltyPointBinding
import com.erabia.foodcrowd.android.loyaltypoint.adapter.LoyaltyPointAdapter
import com.erabia.foodcrowd.android.loyaltypoint.remote.repository.LoyaltyPointRepository
import com.erabia.foodcrowd.android.loyaltypoint.remote.service.ILoyaltyPoint
import com.erabia.foodcrowd.android.loyaltypoint.viewmodel.LoyaltyPointViewModel
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import androidx.lifecycle.Observer

class LoyaltyPointFragment : Fragment() {

    lateinit var binding: FragmentLoyaltyPointBinding
    lateinit var loyaltyPointViewModel: LoyaltyPointViewModel
    private var loyaltyPointAdapter: LoyaltyPointAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        val factory = LoyaltyPointViewModel.Factory(
            LoyaltyPointRepository(
                RequestManager.getClient().create(ILoyaltyPoint::class.java)
            )
        )

        loyaltyPointViewModel =
            ViewModelProvider(this, factory).get(LoyaltyPointViewModel::class.java)
        loyaltyPointViewModel.getLoyaltyPointList()
        loyaltyPointViewModel.getLoyaltyPointAmount()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as MainActivity).toolbar.title = ""

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_loyalty_point,
            container,
            false
        )
        observeLoyaltyPointList()
        observeLoyaltyAmount()
        initView()
        return binding.root
    }

    private fun initView() {
        loyaltyPointAdapter = LoyaltyPointAdapter(loyaltyPointViewModel)
        loyaltyPointAdapter?.setHasStableIds(true)
        binding?.mMyLoyaltyRecycleView?.adapter = loyaltyPointAdapter

    }

    private fun observeLoyaltyAmount() {
        loyaltyPointViewModel?.pointAmountList.observe(viewLifecycleOwner, Observer {
            loyaltyPointAdapter?.loyaltyPointAmountDataList = it
            binding.mAvailableBalanceValueTextView.text = it
        })
    }

    private fun observeLoyaltyPointList() {
        loyaltyPointViewModel?.pointList.observe(viewLifecycleOwner, Observer {
            loyaltyPointAdapter?.loyaltyPointDataList = it

            if (it.loyaltyPointHistroy?.isEmpty() == true) {
                binding.mListOrderGroup.visibility = View.GONE
                binding.mAvailableBalanceTextView.visibility = View.GONE
                binding.mAvailableBalanceValueTextView.visibility = View.GONE
                binding.mNoPointsGroup.visibility = View.VISIBLE
            } else {
                binding.mListOrderGroup.visibility = View.VISIBLE
                binding.mAvailableBalanceTextView.visibility = View.VISIBLE
                binding.mAvailableBalanceValueTextView.visibility = View.VISIBLE
                binding.mNoPointsGroup.visibility = View.GONE
            }
        })
    }
}