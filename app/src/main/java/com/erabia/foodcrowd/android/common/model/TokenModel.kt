package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class TokenModel(

    @SerializedName("access_token")
    val accessToken: String? = "",
    @SerializedName("token_type")
    val tokenType: String? = "",

    @SerializedName("expires_in")
    val expiresIn: Int? = 0,

    @SerializedName("refresh_token")
    val refreshToken: String? = "",

    @SerializedName("scope")
    val scope: Any? = ""
) {


}