package com.erabia.foodcrowd.android.common.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SiteConfig(
    @SerializedName("pickupInStoreEnabled")
    @Expose
    var pickupInStoreEnabled: Boolean? = null,

    @SerializedName("registrationConfiguration")
    var registrationConfiguration: RegistrationConfiguration = RegistrationConfiguration(),

    @SerializedName("storeCreditConfiguration")
    var storeCreditConfiguration: StoreCreditConfiguration = StoreCreditConfiguration()


)

data class RegistrationConfiguration(
    @SerializedName("nationalityConfigurations")
    var nationalityConfigurations: NationalityConfigurations = NationalityConfigurations(),

    @SerializedName("referralCodeConfigurations")
    var referralCodeConfigurations
    : ReferralCodeConfigurations = ReferralCodeConfigurations()
)

data class StoreCreditConfiguration(
    @SerializedName("totalPriceWithStoreCreditLimit")
    @Expose
    var totalPriceWithStoreCreditLimit: Int? = 0,

    @SerializedName("enableCheckTotalPriceWithStoreCreditLimit")
    @Expose
    var enableCheckTotalPriceWithStoreCreditLimit: Boolean? = null
)

data class NationalityConfigurations(
    @SerializedName("enabled")
    @Expose
    var enabled: Boolean? = null
)


data class ReferralCodeConfigurations(
    @SerializedName("enabled")
    @Expose
    var enabled: Boolean? = null,
    @SerializedName("receiverRewardAmount")
    @Expose
    var receiverRewardAmount: Double? = 0.0,

    @SerializedName("senderRewardAmount")
    @Expose
    var senderRewardAmount: Int? = 0
)
