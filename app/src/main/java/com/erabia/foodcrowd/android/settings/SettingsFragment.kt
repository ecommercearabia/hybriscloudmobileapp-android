package com.erabia.foodcrowd.android.settings

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreferenceCompat
import com.erabia.foodcrowd.android.BuildConfig
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.email.remote.repositry.ChangeEmailRepository
import com.erabia.foodcrowd.android.email.remote.service.ChangeEmailService
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.menu.viewmodel.SharedViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.password.remote.repositry.ChangePasswordRepository
import com.erabia.foodcrowd.android.password.remote.service.ChangePasswordService

class SettingsFragment : PreferenceFragmentCompat() {
    lateinit var settingsViewModel: SettingsViewModel

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey)
        val factory = SettingsViewModel.Factory(
            SettingsRepository(
                RequestManager.getClient().create(
                    SettingsService::class.java
                )
            )
        )
        settingsViewModel = ViewModelProvider(this, factory).get(SettingsViewModel::class.java)
        val pref = preferenceScreen.findPreference<ListPreference>("setting_language")
        val mTouchIdSwitch = preferenceScreen.findPreference<SwitchPreferenceCompat>("touch_id")
        mTouchIdSwitch?.isVisible = !SharedPreference.getInstance().getLoginEmail().isNullOrEmpty()

        mTouchIdSwitch?.isChecked = SharedPreference.getInstance().isFingerPrintEnabled()

        var selectedLang = activity?.baseContext?.let { LocaleHelper.getLanguage(it) }
        if (selectedLang == "ar") {
            pref?.setValueIndex(1)
        } else {
            pref?.setValueIndex(0)
        }

        val mVersionName = preferenceScreen.findPreference<Preference>("setting_version")
        mVersionName?.summary = BuildConfig.VERSION_NAME


        mTouchIdSwitch?.setOnPreferenceChangeListener { preference, newValue ->
            if (newValue as Boolean) {

                settingsViewModel.updateSignature(
                    SharedPreference.getInstance().getSIGNATURE_ID() ?: ""
                )
                SharedPreference.getInstance().setIsLoginFirstTime(false)

            } else {
                SharedPreference.getInstance().setFingerPrintEnabled(false)
                SharedPreference.getInstance().setIsLoginFirstTime(false)

            }
            true
        }


        pref?.setOnPreferenceChangeListener { preference, newValue ->
            when (newValue.toString()) {
                Constants.ENGLISH -> {
                    LocaleHelper.setSummary(context as MainActivity, Constants.ENGLISH)
                    setNewLocale(activity as MainActivity, "en")
                }

                Constants.ARABIC -> {
                    LocaleHelper.setSummary(context as MainActivity, Constants.ARABIC)
                    setNewLocale(activity as MainActivity, "ar")

                }
            }
            true
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeSetSignature()
        //onViewCreated
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).toolbar.title = ""
    }

    private fun observeSetSignature() {
        settingsViewModel.updateSignatureLiveData.observe(viewLifecycleOwner, Observer {
            SharedPreference.getInstance().setFingerPrintEnabled(true)
        })
    }

    fun setNewLocale(activity: Activity, language: String) {
        LocaleHelper.setLanguage(activity.baseContext, language)
        val intent = activity.getIntent();
        activity.finish()
        startActivity(intent)
        Runtime.getRuntime().exit(0)


    }

}