package com.erabia.foodcrowd.android.common.util

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import com.blongho.country_data.World
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.model.StreamEncoder
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.erabia.foodcrowd.android.BuildConfig
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import java.io.InputStream

class ImageLoader {

    companion object {
        const val TAG: String = "ImageLoader"
    }


    fun loadImage(url: String?, imageView: ImageView) {

        val imageUrl = "${BuildConfig.BASE_URL}$url"
        if (imageView.context != null)
            Glide.with(MyApplication.getInstance()?.applicationContext ?: imageView.context)
                .load(imageUrl).fitCenter().into(imageView)

    }

    fun loadToBitmapImage(url: String?, imageView: ImageView) {

        val imageUrl = "${BuildConfig.BASE_URL}$url"
        Glide.with(MyApplication.getInstance()?.applicationContext ?: imageView.context)
            .asBitmap()
            .load(imageUrl)
            .into(object : CustomTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                    imageView?.setImageBitmap(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
    }

    fun loadImageWithPlaceHolder(url: String?, imageView: ImageView) {
        val imageUrl = "${BuildConfig.BASE_URL}$url"
        if (imageView.context != null)
            Glide.with(MyApplication.getInstance()?.applicationContext ?: imageView.context)
                .load(imageUrl).placeholder(R.drawable.ic_image_placeholder).fitCenter()
                .into(imageView)
    }

    fun loadSvgImageWithPlaceHolder(url: String?, imageView: ImageView) {
        val imageUrl = "${BuildConfig.BASE_URL}$url"
        GlideToVectorYou
            .init()
            .with(MyApplication.getInstance()?.applicationContext)
            .setPlaceHolder(R.drawable.ic_image_placeholder, 0)
            .load(Uri.parse(imageUrl), imageView);

    }

    fun setImageFlag(view: View?, country: String?) {
        var mCountry: String? = country?.toLowerCase()
        if (mCountry != null) {
            when (country?.toLowerCase()) {
                "rsa" -> mCountry = "South Africa"
                "uae" -> mCountry = "ae"
                "Netherland" -> mCountry = "NLD"
            }
            (view as? ImageView)?.setImageResource(World.getFlagOf(mCountry))
        }
    }
}