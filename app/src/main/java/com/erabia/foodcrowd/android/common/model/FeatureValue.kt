package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class FeatureValue(
    @SerializedName("value")
    val value: String = ""
)