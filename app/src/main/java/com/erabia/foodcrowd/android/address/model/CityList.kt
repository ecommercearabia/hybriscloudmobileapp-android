package com.erabia.foodcrowd.android.address.model

import com.erabia.foodcrowd.android.common.model.City
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class CityList(
    @SerializedName("cities")
    var cities: List<City>? = null


)