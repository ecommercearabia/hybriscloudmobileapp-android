package com.erabia.foodcrowd.android.common.model


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("altText")
    var altText: String? = "",
    @SerializedName("format")
    var format: String? = "",
    @SerializedName("galleryIndex")
    var galleryIndex: Int = 0,
    @SerializedName("imageType")
    var imageType: String? = "",
    @SerializedName("url")
    var url: String? = ""
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(altText)
        parcel.writeString(format)
        parcel.writeInt(galleryIndex)
        parcel.writeString(imageType)
        parcel.writeString(url)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Image> {
        override fun createFromParcel(parcel: Parcel): Image {
            return Image(parcel)
        }

        override fun newArray(size: Int): Array<Image?> {
            return arrayOfNulls(size)
        }
    }
}