package com.erabia.foodcrowd.android.aboutus.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.aboutus.remote.AboutUsRepository
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse


class AboutUsViewModel(val mAboutUsRepository: AboutUsRepository) : ViewModel() {

    var mAboutUsResponseLiveData: LiveData<ContactUsResponse> =
            mAboutUsRepository.mAboutUsResponseLiveData
    val loadingVisibility: MutableLiveData<Int> = mAboutUsRepository.loadingVisibility
    val mErrorEvent: SingleLiveEvent<String> = mAboutUsRepository.error()

    fun getAboutUs() {
        mAboutUsRepository.getAboutUsDetails()
    }

    class Factory(
            val mAboutUsRepository: AboutUsRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return AboutUsViewModel(mAboutUsRepository) as T
        }

    }
}