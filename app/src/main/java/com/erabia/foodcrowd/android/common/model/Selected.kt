package com.erabia.foodcrowd.android.common.model


import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

data class Selected(
    @SerializedName("code")
    val code: String = "",
    @SerializedName("priceData")
    val priceData: PriceData = PriceData(),
    @SerializedName("stock")
    val stock: Stock = Stock(),
    @SerializedName("url")
    val url: String = "",
    @SerializedName("variantOptionQualifiers")
    @TypeConverters(DataConverter::class)
    val variantOptionQualifiers: List<VariantOptionQualifier> = listOf()
)