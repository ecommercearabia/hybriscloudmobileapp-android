package com.erabia.foodcrowd.android.home.remote.service

import com.erabia.foodcrowd.android.common.model.SendEmailOtp
import com.erabia.foodcrowd.android.common.model.TokenModel
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

interface VerificationService {
    @POST("rest/v2/foodcrowd-ae/otp/verify/login")
    fun verify(
        @Query("code") code: String,
        @Query("email") email: String,
        @Query("name") name: String
    ): Observable<Response<TokenModel>>

}