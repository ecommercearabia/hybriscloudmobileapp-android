package com.erabia.foodcrowd.android.managecards.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.managecards.model.ManageCardData
import com.erabia.foodcrowd.android.managecards.remote.repository.ManageCardsRepository

class ManageCardsViewModel(
    var manageCardsRepository: ManageCardsRepository
) : ViewModel(){

    companion object {
        private const val TAG: String = "ManageCardsViewModel"
    }

    val cardList: LiveData<ManageCardData> = manageCardsRepository.savedCardsLiveData

    val errorResponseObserve: SingleLiveEvent<String> = manageCardsRepository.errorResponseObserver

    val progressBarVisibility: SingleLiveEvent<Int> = manageCardsRepository.progressBarVisibility()


    fun getSavedCardsList() {
        manageCardsRepository.loadSavedCards(SharedPreference.getInstance().getLoginEmail() ?: "")
    }

    fun deleteCard(customerCardId : String){
        manageCardsRepository.deleteSavedCards(
            SharedPreference.getInstance().getLoginEmail() ?: "",
            customerCardId
        )
    }

    class Factory(
        private val manageCardsRepository: ManageCardsRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ManageCardsViewModel(manageCardsRepository) as T
        }
    }
}