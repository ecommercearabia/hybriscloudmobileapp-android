package com.erabia.foodcrowd.android.referralcode.view

import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.marginStart
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.BuildConfig
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.erabia.foodcrowd.android.databinding.FragmentReferralCodeBinding
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.referralcode.adapter.ReferralCodeAdapter
import com.erabia.foodcrowd.android.referralcode.model.ReferralCodeResponse
import com.erabia.foodcrowd.android.referralcode.remote.repository.ReferralCodeRepository
import com.erabia.foodcrowd.android.referralcode.remote.service.ReferralCodeService
import com.erabia.foodcrowd.android.referralcode.viewmodel.ReferralCodeViewModel
import io.reactivex.disposables.CompositeDisposable
import java.text.DecimalFormat


class ReferralCodeFragment : Fragment() {

    var compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: ReferralCodeViewModel
    lateinit var binding: FragmentReferralCodeBinding

    private var mReferralCodeAdapter: ReferralCodeAdapter? = null
    var myReferralCodeList: List<ReferralCodeResponse.Histories?> = listOf()

    var share : Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val factory = ReferralCodeViewModel.Factory(
            ReferralCodeRepository(
                RequestManager.getClient().create(ReferralCodeService::class.java)
            )
        )

        mViewModel = ViewModelProvider(this, factory).get(ReferralCodeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_referral_code, container, false)
        binding.lifecycleOwner = this
        binding.mViewModel = mViewModel
        mViewModel.getReferralCode()
        mViewModel.getConfig()
        if (LocaleHelper.getLanguage(MyApplication.getContext()!!).equals("ar")) {

            binding.textView7.setPadding(0, 0, 170, 0)
            binding.textView8.setPadding(0, 0, 70, 0)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observerGetReferralCodeList()
        observerGetReferralCode()
        observeShareEvent()
        observeClickShareEvent()
        observerShareViaEmail()
    }

    private fun observeShareEvent() {
        mViewModel.shareClickEvent.observe(context as LifecycleOwner, Observer {

            var message = ""

            if(BuildConfig.FLAVOR == "stg") {

                message = context?.resources?.getString(R.string.referral) + " " + SharedPreference.getInstance()
                    .getReferralCode() + " "+ context?.resources?.getString(R.string.referral_con2) + " " +
                        "https://stg.foodcrowd.com/en/register?referralCode=$it " + context?.resources?.getString(R.string.referral_con3) }
            else {

                message = context?.resources?.getString(R.string.referral) + " " + SharedPreference.getInstance()
                    .getReferralCode() + " "+ context?.resources?.getString(R.string.referral_con2) + " " +
                        "https://foodcrowd.com/en/register?referralCode=$it " + context?.resources?.getString(R.string.referral_con3)

            }
            if (Build.VERSION.SDK_INT >= 23) {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.type = "text/plain"
//                val pendIntent =
//                    PendingIntent.getActivity(
//                        context, 0, sendIntent, PendingIntent.FLAG_IMMUTABLE
//                    )
                val chooser = Intent.createChooser(sendIntent, null)
                this.context?.startActivity(chooser)
                share = true
            } else {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.type = "text/plain"
                this.context?.startActivity(sendIntent)
                share = true
            }
        })
    }

    private fun observeClickShareEvent() {
        mViewModel.shareClick.observe(context as LifecycleOwner, Observer {

            DialogData.getShareVia(this.context, mViewModel, it)

        })
    }

    fun observerShareViaEmail() {
        mViewModel.shareViaEmail.observe(this, Observer {
            if (it) {
                AppUtil.showToastySuccessReferral(context, context?.resources?.getString(R.string.share_referral) + " " +SharedPreference.getInstance().getReferralCode() + " "+ context?.resources?.getString(R.string.share_referral_con) ,
                    resources.getColor(R.color.colorPrimary)
                    , resources.getColor(R.color.white))

            }
        })
    }

    fun observerGetReferralCode() {
        mViewModel.mReferralCodeLiveData.observe(this, Observer {

            binding.mReferralCodeModel = it

            if (!it.code.isNullOrEmpty()) {
                binding.imageViewShare.visibility = View.VISIBLE
                binding.codeButton.text = it.code
                binding.codeButton.isEnabled = false
                binding.codeButton.textSize = 10.0.toFloat()

            }

            else {
                binding.imageViewShare.visibility = View.GONE
            }

        })
    }

    fun observerGetReferralCodeList() {
        binding.amountTextView.text = "00.0"

        mViewModel.mReferralCode.observe(this, Observer {
            myReferralCodeList = it.histories
            binding.mReferralCodeModel = it
            val dec = DecimalFormat("#,#00.0#")
            binding.amountTextView.text = dec.format(it.totalAmount ?: 0.0).toString()

            if (myReferralCodeList.isEmpty()) {
                if (it.code != null) {
                    binding.codeButton.text = it.code
                    binding.codeButton.isEnabled = false
                    binding.codeButton.textSize = 10.0.toFloat()
                    binding.imageViewShare.visibility = View.VISIBLE
                }
                else {
                    binding.imageViewShare.visibility = View.GONE
                }

            } else {


                mReferralCodeAdapter =
                    ReferralCodeAdapter(
                        myReferralCodeList
                    )
                mReferralCodeAdapter?.setHasStableIds(true)
                binding?.referralCodeRV?.adapter = mReferralCodeAdapter

                if (it.code != null) {
                    binding.codeButton.text = it.code
                    binding.codeButton.isEnabled = false
                    binding.codeButton.textSize = 10.0.toFloat()
                    binding.imageViewShare.visibility = View.VISIBLE
                }
                else {
                    binding.imageViewShare.visibility = View.GONE
                }

            }

        })
    }


    override fun onResume() {
        super.onResume()
        if (share) {

            AppUtil.showToastySuccessReferral(context, context?.resources?.getString(R.string.share_referral) + " " +SharedPreference.getInstance().getReferralCode() + " "+ context?.resources?.getString(R.string.share_referral_con) ,
                resources.getColor(R.color.colorPrimary)
            , resources.getColor(R.color.white))


            share = false

        }


    }

}