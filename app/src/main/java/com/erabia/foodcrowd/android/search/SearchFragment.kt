package com.erabia.foodcrowd.android.search

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.category.adapter.CategoryFilterAdapter
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.extension.TAG
import com.erabia.foodcrowd.android.common.model.BreadCrumb
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentSearchBinding
import com.erabia.foodcrowd.android.listing.adapter.ListingAdapter
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.remote.service.ListingService
import com.erabia.foodcrowd.android.login.view.LoginActivity
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.product.di.DaggerIProductComponent
import com.erabia.foodcrowd.android.product.di.ProductModule
import com.erabia.foodcrowd.android.search.viewmodel.SearchViewModel
import com.erabia.foodcrowd.android.wishlistmenu.remote.repository.WishListRepository
import com.erabia.foodcrowd.android.wishlistmenu.remote.service.WishListService


class SearchFragment : Fragment(), ListingAdapter.IListingItemClick,
    CategoryFilterAdapter.OnFilterChecked, CategoryFilterAdapter.OnFilterUnChecked {

    private var isLoading = false
    lateinit var viewModel: SearchViewModel
    lateinit var binding: FragmentSearchBinding
    private var lastPage = 0
    private lateinit var listingAdapter: ListingAdapter

    private var categoryFilterAdapter: CategoryFilterAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        val factory = SearchViewModel.Factory(
            ListingRepository(RequestManager.getClient().create(ListingService::class.java)),
            CartRepository(RequestManager.getClient().create(CartService::class.java)),
            WishListRepository(RequestManager.getClient().create(WishListService::class.java))
        )
        viewModel = ViewModelProvider(this, factory).get(SearchViewModel::class.java)
        binding.viewModel = viewModel
        setFragmentResultListener("SEARCH_FRAGMENT") { key, bundle ->
            var SearchKey = bundle.getString("SearchKey") ?: ""

        }
        viewModel.categoryId = arguments?.getString("categoryCode") ?: ""
        listingAdapter = ListingAdapter(viewModel)
        categoryFilterAdapter = CategoryFilterAdapter()
        categoryFilterAdapter?.onFilterChecked = this
        categoryFilterAdapter?.onFilterUnChecked = this
        binding.recyclerViewFilter.adapter = categoryFilterAdapter
        binding.recyclerViewProduct.adapter = listingAdapter
        observeProductList()
        observeToggleBreadCrumbList()
        observeToggleFacetList()
        observeItemsVisibility()
        observeSearchImageViewVisibility()
        observeResetAdapter()
        observeProgressBar()
        observeStartLogin()
        observeBadgeNumber()
        observeAddToWishlistEvent()
        observeAddToCart()
        pagination()
        listingAdapter.onListingItemClick = this

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as MainActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainActivity).toolbar.navigationIcon =
            ContextCompat.getDrawable((activity as MainActivity), R.drawable.ic_arrow_back_24dp)
        setHasOptionsMenu(true)

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                this.findNavController().popBackStack()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun observeStartLogin() {
        viewModel.startLoginEvent.observe(this, Observer {
            context?.let {
                ContextCompat.startActivity(
                    it, Intent(context, LoginActivity::class.java), null
                )
            }
        })
    }


    private fun observeBadgeNumber() {
        viewModel.badgeNumber.observe(viewLifecycleOwner, Observer {
            (activity as? MainActivity)?.badgeDrawable?.number = it
        })
    }

    override fun onListingItemClick(key: String, productCode: String) {
        if (key.equals("onAllList")) {

            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to productCode)
            )
        } else if (key.equals("onWishList")) {
            viewModel.addToWishList(productCode)
        } else if (key.equals("onRemoveWishList")) {
            viewModel.removeFromWishList(productCode)
        }


    }


    private fun pagination() {
        binding.recyclerViewProduct.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager =
                    recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                Log.d("total", totalItemCount.toString() + "")
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                if ((lastVisibleItem == totalItemCount - 1) && (viewModel.currentPage < lastPage - 1) && !isLoading && (dy > 0)) {
                    isLoading = true
                    viewModel.currentPage = viewModel.currentPage++
                    viewModel.currentPage++
                    Log.d("current", viewModel.currentPage.toString())
                    viewModel.getProductList(
                        viewModel.currentPage
                    )
                }
            }
        })
    }

    private fun observeProductList() {
        viewModel.productListSearch.observe(viewLifecycleOwner, Observer {
            isLoading = false

//            listingAdapter.reset(binding.recyclerViewProduct.scrollState)
            listingAdapter.refresh(it.products as MutableList<Product>)
            lastPage = it.pagination?.totalPages ?: 0
            listingAdapter.totalResult = it.pagination?.totalResults ?: 0


//            if (it?.products.isNotEmpty()) {
//                val categoryFilterFacets: MutableList<Facet> = mutableListOf()
//                categoryFilterAdapter = CategoryFilterAdapter()
//                categoryFilterAdapter?.breadCrumbsList = it.breadcrumbs
//            categoryFilterAdapter?.refresh(it.facets ?: listOf())
//                it.facets?.forEach {
//                    if (it.name == "Vegan" || it.name == "Organic") {
//                        categoryFilterFacets.add(it)
//                    }
//                }
//                categoryFilterAdapter?.refresh(categoryFilterFacets)
//                categoryFilterAdapter?.onFilterChecked = this
//                categoryFilterAdapter?.onFilterUnChecked = this
//            } else {
//                categoryFilterAdapter?.clear()
//            }
        })
    }

    private fun observeToggleFacetList() {
        viewModel.toggleFacetList.observe(viewLifecycleOwner, Observer {
            categoryFilterAdapter?.refresh(it)
        })
    }

    private fun observeToggleBreadCrumbList() {
        viewModel.toggleBreadCrumbList.observe(viewLifecycleOwner, Observer {
            categoryFilterAdapter?.breadCrumbsList = it
        })
    }

    private fun observeSearchImageViewVisibility() {
        viewModel.searchImageViewVisibility.observe(viewLifecycleOwner, Observer<Int> {
            binding.imageViewEmpty.visibility = it
        })
    }

    private fun observeResetAdapter() {
        viewModel.resetAdapter.observe(viewLifecycleOwner, Observer {
            listingAdapter.reset(binding.recyclerViewProduct.scrollState)
        })
    }

    private fun observeItemsVisibility() {
        viewModel.itemsVisibility.observe(viewLifecycleOwner, Observer {
            binding.recyclerViewProduct.visibility = it
        })
    }


    private fun observeAddToCart() {
        viewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context , R.string.added_to_cart_message)

        })
    }

    private fun observeAddToWishlistEvent() {
        viewModel.mAddWishListLiveData.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastySuccess(  context , R.string.add_to_wishlist)
        })
    }

    private fun observeProgressBar() {
        viewModel.progressBarVisibilityCart.observe(viewLifecycleOwner,
            Observer { binding.progressBarSearch.visibility = it })

        viewModel.progressBarVisibilityListing.observe(viewLifecycleOwner,
            Observer { binding.progressBarSearch.visibility = it })

        viewModel.progressBarVisibilityWishlist.observe(viewLifecycleOwner,
            Observer { binding.progressBarSearch.visibility = it })
    }

    var filterQueryHashMap = HashMap<String, String>()
    override fun onFilterChecked(facet: Facet?) {
        //TODO add hashmap here <facetName, value>
        var query = facet?.values?.get(0)?.query?.query?.value?.replace("false", "true")
        filterQueryHashMap.put(facet?.name ?: "", query ?: "")
//        Log.d(
//            TAG,
//            "onFilterChecked: getQuery -> ${getQuery()} query -> ${query} \n filterQuery -> ${getQuery().plus(
//                query
//            )}"
//        )
//        Log.d(TAG, "onFilterChecked: filterQuery -> ${filterQuery}")
        viewModel.currentPage = 0

        viewModel.getProductList(
            viewModel.categoryId.toString(),
            viewModel.currentPage,
            query ?: "", true ,"search"
        )

    }

    override fun onFilterUnChecked(breadCrumb: BreadCrumb?) {
        breadCrumb?.removeQuery?.query?.value?.let {
//            filterQuery = it
            viewModel.currentPage = 0

            viewModel.getProductList(
                viewModel.categoryId.toString(),
                viewModel.currentPage,
                it, true , "search"
            )
        }
//        filterQuery = ":relevance:category:$categoryId"

    }
}