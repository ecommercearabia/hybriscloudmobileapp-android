package com.erabia.foodcrowd.android.network

import android.annotation.SuppressLint
import android.util.Log
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.model.TokenModel
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import com.erabia.foodcrowd.android.common.util.SharedPreference
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.reactivex.Observable
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query
import java.io.IOException
import java.net.URL


class AuthInterceptor
    : Authenticator {
    companion object {
        const val TAG = "AuthInterceptor"
    }

    interface IApi {

        @POST("/authorizationserver/oauth/token")
        fun getOauth(
            @Query("grant_type") grant_type: String,
            @Query("client_id") client_id: String,
            @Query("client_secret") client_secret: String

        ): Observable<retrofit2.Response<TokenModel>>

        @POST("/authorizationserver/oauth/token")
        fun refreshToken(
            @Query("grant_type") grant_type: String,
            @Query("client_id") client_id: String,
            @Query("client_secret") client_secret: String,
            @Query("refresh_token") refresh_token: String
        ): Observable<retrofit2.Response<TokenModel>>

    }

//    class TokenModel {
//        @SerializedName("access_token")
//        @Expose
//        val accessToken: String? = null
//
//        @SerializedName("token_type")
//        @Expose
//        val tokenType: String? = null
//
//        @SerializedName("expires_in")
//        @Expose
//        val expiresIn: Int? = null
//
//        @SerializedName("refresh_token")
//        @Expose
//        val refreshToken: Int? = null
//
//
//        @SerializedName("scope")
//        @Expose
//        val scope: String? = null
//    }

    //    @SuppressLint("CheckResult")
//    override fun intercept(chain: Interceptor.Chain): Response {
//        var re: Retrofit = Retrofit.Builder()
//            .baseUrl("https://api.casvowwxta-aldahraho1-s1-public.model-t.cc.commerce.ondemand.com/")
//            .client(OkHttpClient())
//            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
//
//        re.create(IApi::class.java).getOauth("client_credentials", "mobile_android", "secret")
//            .get().subscribe() {
//                SharedPreference.getInstance().setApp_TOKEN(it.body()?.accessToken ?: "")
//                Log.d("token", "code is".plus(it.code()))
//
//            }
//
//        return chain.proceed(
//            chain.request().newBuilder()
//                .header(
//                    Constants.API_KEY.AUTHORIZATION,
//                    SharedPreference.getInstance().getAppToken() ?: ""
//                )
//                .build()
//        )
//    }

    val lock = Any()
    var isFetchingToken = false
    var responseList = mutableListOf<Response>()

    @SuppressLint("CheckResult")
    override fun authenticate(route: Route?, response: Response): Request? {

        Log.d(
            TAG,
            "authenticate: currentThread is ${Thread.currentThread().name} -- id is ${Thread.currentThread().id}"
        )
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY



//        var client = OkHttpClient().newBuilder()
//        client.networkInterceptors().add(StethoInterceptor())
//        client.addInterceptor(interceptor)
//        var re: Retrofit = Retrofit.Builder()
//            .baseUrl(Constants.BASE_URL)
//            .client(client.build())
//            .addConverterFactory(GsonConverterFactory.create())
//            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()

        synchronized(lock) {
            Log.d(
                TAG,
                "authenticate: Authorization header : ${response.request.header("Authorization")} userToken ${
                    SharedPreference.getInstance().getUserToken()
                }"
            )
            var userAccessToken = SharedPreference.getInstance().getUserToken()

            if (userAccessToken != null && userAccessToken.isNotEmpty()) {
                if (response.request.header("Authorization") == "Bearer ${
                        SharedPreference.getInstance().getUserToken()
                    }"
                ) {
                    var tokenModel =
                        RequestManager.getClient().create(IApi::class.java).refreshToken(
                            "refresh_token",
                            "occ_mobile",
                            "Erabia@123",
                            SharedPreference.getInstance().getRefreshToken() ?: ""
                        )
                            .blockingSingle().body()
                    Log.d(TAG, "authenticate: userToken from network $tokenModel")
                    if (tokenModel != null) {
                        SharedPreference.getInstance().setUser_TOKEN(tokenModel?.accessToken)
                        SharedPreference.getInstance().setRefreshToken(tokenModel?.refreshToken)

                    } else {
                        Log.d(TAG, "authenticate: 1 refreshToken & GUID null")
                        SharedPreference.getInstance().setLOGIN_TOKEN("")
//                        SharedPreference.getInstance().setLOGIN_EMAIL("")
                        SharedPreference.getInstance().setRefreshToken("")
                        SharedPreference.getInstance().setUser_TOKEN("")
                        SharedPreference.getInstance().setApp_TOKEN("")
//        SharedPreference.getInstance().setLOGIN_User("")
                        SharedPreference.getInstance().clearSP()

                    }
                    Log.d(
                        TAG,
                        "authenticate:1 url ${response.request.url}"
                    )
                    Log.d(
                        TAG,
                        "authenticate:1 userAccessToken ${
                            SharedPreference.getInstance().getUserToken()
                        }"
                    )
                    var r = response.request.newBuilder().header(
                        Constants.API_KEY.AUTHORIZATION,
                        "Bearer ${SharedPreference.getInstance().getUserToken()}" ?: ""
                    )
                        .build()
                    return r
                } else {
                    return response.request.newBuilder().header(
                        Constants.API_KEY.AUTHORIZATION,
                        "Bearer ${SharedPreference.getInstance().getUserToken()}" ?: ""
                    ).build()
                }
            } else {
                if ((response.request.header("Authorization") == "Bearer ${
                        SharedPreference.getInstance().getAppToken()
                    }" || SharedPreference.getInstance().getAppToken().isNullOrEmpty())
                ) {
                    var token =
                        RequestManager.getClient().create(IApi::class.java)
                            .getOauth("client_credentials", "occ_mobile", "Erabia@123")
                            .blockingSingle().body()?.accessToken

                    if (token != null) {
                        SharedPreference.getInstance().setApp_TOKEN(token)
                        // SharedPreference.getInstance().setGUID(null)

                    } else {
                        SharedPreference.getInstance().setGUID(null)
                        SharedPreference.getInstance().setApp_TOKEN(null)
                        Log.d(TAG, "authenticate: 2 GUID null")

                    }
                    Log.d(
                        TAG,
                        "authenticate:2 url ${response.request.url}"
                    )
                    Log.d(
                        TAG,
                        "authenticate:2 userAccessToken ${
                            SharedPreference.getInstance().getUserToken()
                        }"
                    )
                    return response.request.newBuilder().header(
                        Constants.API_KEY.AUTHORIZATION,
                        "Bearer $token" ?: ""
                    )
                        .build()
                } else {
                    return response.request.newBuilder().header(
                        Constants.API_KEY.AUTHORIZATION,
                        "Bearer ${SharedPreference.getInstance().getAppToken()}" ?: ""
                    ).build()
                }
            }
        }

    }


}
