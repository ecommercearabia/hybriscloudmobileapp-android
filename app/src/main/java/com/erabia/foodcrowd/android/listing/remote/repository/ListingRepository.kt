package com.erabia.foodcrowd.android.listing.remote.repository

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.get
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.common.extension.subscribe
import com.erabia.foodcrowd.android.common.model.BreadCrumb
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.listing.model.Listing
import com.erabia.foodcrowd.android.listing.remote.service.ListingService
import com.google.firebase.analytics.FirebaseAnalytics

class ListingRepository(val listingService: ListingService) : Repository {
    companion object {
        const val TAG: String = "ListingRepository"
    }


    val productListLiveData = SingleLiveEvent<Listing>()
    val productListSearchLiveData = MutableLiveData<Listing>()
    val toggleFacetList by lazy { MutableLiveData<List<Facet>>() }
    val toggleBreadCrumbList by lazy { MutableLiveData<List<BreadCrumb>>() }

    @SuppressLint("CheckResult")
    fun loadProduct(
        category: String,
        currentPage: Int,
        query: String,
        sort: String,
        isLoadingShow: Boolean,
        fromFragment: String
    ): SingleLiveEvent<Listing> {

        if (query.isNotEmpty()) {
//            if (isLoadingShow)
//            progressBarVisibility().value = (View.VISIBLE)
            listingService.getProductList(
                category,
                currentPage, 30,
                query, Constants.FIELDS_FULL, sort
            ).get()
                .subscribe(this,
                    onSuccess_200 = {
                        if (fromFragment == "listing") {
                            productListLiveData.value = it?.body() ?: Listing()
                        } else if (fromFragment == "search") {
                            productListSearchLiveData.value = it?.body() ?: Listing()
                            val mFirebaseAnalytics =
                                FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

                            val params = Bundle()

                            params.putString(FirebaseAnalytics.Param.SEARCH_TERM, query ?: "")
                            mFirebaseAnalytics.logEvent(
                                FirebaseAnalytics.Event.SEARCH,
                                params
                            )
                        }
                        val categoryFilterFacets: MutableList<Facet> = mutableListOf()
                        it.body()?.facets?.forEach {
                            if (it.name == "Vegan" || it.name == "Organic") {
                                categoryFilterFacets.add(it)
                            }
                        }
                        toggleFacetList.value = categoryFilterFacets
                        toggleBreadCrumbList.value = it.body()?.breadcrumbs ?: listOf()
                    },
                    doOnSubscribe = {
                        if (isLoadingShow) {
                            progressBarVisibility().value = (View.VISIBLE)

                        } else {
                            progressBarVisibility().value = (View.GONE)

                        }
                    }
                )
        }
        return productListLiveData
    }


}