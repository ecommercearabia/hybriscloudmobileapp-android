package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class ClosingTime(
    @SerializedName("formattedHour")
    val formattedHour: String = "",
    @SerializedName("hour")
    val hour: Int = 0,
    @SerializedName("minute")
    val minute: Int = 0
)