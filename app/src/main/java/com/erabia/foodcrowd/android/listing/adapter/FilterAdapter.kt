package com.erabia.foodcrowd.android.listing.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Value
import com.erabia.foodcrowd.android.databinding.FilterItemRowBinding
import com.erabia.foodcrowd.android.databinding.FilterRowBinding

class FilterAdapter(var query: String = "", var sortSelected: String = "") :
    RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

    var filterList: List<Facet>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    lateinit var binding: FilterRowBinding
    var isFirstTime = true
    var isFirstTime2 = true
    var deleteQuery = ""
    var onChecked: IOnChecked? = null
    var charQeury: Char = ' '
    var charQeuryApi: Char = ' '

    //        set(value) {
//            field = value
//            notifyDataSetChanged()
//        }

    var queryListApi = ArrayList<String>()
    var queryListFromListing = ArrayList<String>()
    val fooSet: MutableSet<String> = LinkedHashSet(queryListFromListing)
    val finalFoo: List<String> = ArrayList(fooSet)

    var queryList = ArrayList<String>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.filter_row,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return filterList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemBinding.facetModel = filterList?.get(position)
        var filterChildAdapter = FilterChildAdapter()
        holder.itemBinding.recyclerViewFilterData.adapter = filterChildAdapter
        filterChildAdapter.childFilterList =
            filterList?.get(position)?.values?.filterNot { it.name == "Categories" }
        holder.itemBinding.imageViewArrow.setOnClickListener(View.OnClickListener {
            if (holder.itemBinding.recyclerViewFilterData.visibility == View.VISIBLE) {
                holder.itemBinding.recyclerViewFilterData.visibility = View.GONE
                holder.itemBinding.imageViewArrow.setImageResource(R.drawable.ic_arrow_down)
            } else {
                holder.itemBinding.recyclerViewFilterData.visibility = View.VISIBLE
                holder.itemBinding.imageViewArrow.setImageResource(R.drawable.ic_arrow_up)
            }
        })
    }

    class ViewHolder(var itemBinding: FilterRowBinding) : RecyclerView.ViewHolder(itemBinding.root)


    inner class FilterChildAdapter : RecyclerView.Adapter<FilterChildAdapter.ChildViewHolder>() {

        lateinit var childBinding: FilterItemRowBinding
        var skipSecondCharToChangeFromQueryFlag = true
        var skipSecondCharToChangeFromFilterListQueryFlag = true
        var childFilterList: List<Value>? = null
            set(value) {
                field = value
                notifyDataSetChanged()
            }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChildViewHolder {
            childBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.filter_item_row,
                parent,
                false
            )
            return ChildViewHolder(childBinding)
        }

        override fun getItemCount(): Int {
            return childFilterList?.size ?: 0
        }

        override fun onBindViewHolder(holder: ChildViewHolder, position: Int) {
            holder.itemBinding.valueModel = childFilterList?.get(position)
            holder.itemBinding.checkBoxFilter.isChecked =
                childFilterList?.get(position)?.selected ?: false
            holder.itemBinding.checkBoxFilter.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener
            { buttonView, isChecked ->
                if (isChecked) {
//                    query = ""
                  var filterQuery= childFilterList?.get(position)?.query?.query?.value?.replace(
                        sortSelected,
                        ""
                    ) ?: ""


                    queryListFromListing = query.split(":") as ArrayList<String>
                    queryListApi =
                        childFilterList?.get(position)?.query?.query?.value?.split(":") as ArrayList<String>


                    query.forEachIndexed { index, c ->
                        if (skipSecondCharToChangeFromQueryFlag) {
                            if (query[index] == ':') {
                                query = changeCharInPosition(index, '$', query) ?: ""

                                skipSecondCharToChangeFromQueryFlag = false
                            }
                        } else {
                            if (query[index] == ':') {
                                skipSecondCharToChangeFromQueryFlag = true
                            }
                        }
                    }


                    filterQuery.forEachIndexed { index, c ->
                        if (skipSecondCharToChangeFromFilterListQueryFlag) {
                            if (filterQuery[index] == ':') {
                                filterQuery =
                                    changeCharInPosition(
                                        index,
                                        '$',
                                        filterQuery.replace(
                                            sortSelected,
                                            ""
                                        ) ?: ""
                                    ) ?: ""

                                skipSecondCharToChangeFromFilterListQueryFlag = false
                            }
                        } else {
                            if (filterQuery[index] == ':'
                            ) {
                                skipSecondCharToChangeFromFilterListQueryFlag = true
                            }
                        }
                    }


                    var queryList = query.split("$").toMutableList() ?: mutableListOf()
                    var filterQueryList =
                        filterQuery.split("$").toMutableList() ?: mutableListOf()

                    fooSet.addAll(queryListApi)
                    val finalFoo: List<String> = ArrayList(fooSet)



                    Log.e("TAG", "onBindViewHolder: $finalFoo")

//                    if (query == sortSelected) {
//                        query += childFilterList?.get(position)?.query?.query?.value?.replace(
//                            sortSelected,
//                            ""
//                        )?.replace('$', ':') ?: ""

//                    } else {


//                        var childFilterList= childFilterList?.get(position)?.query?.query?.value

//                            query = query.replaceFirst(':', '$')
//                        childFilterList = childFilterList?.replaceFirst(':', '$')

//                        childFilterList =   childFilterList?.substring(
//                            IntRange(
//                                childFilterList.indexOfFirst { it == ':' },
//                                childFilterList.length - 1
//                            )
//                        )?.replace('$', ':')

//                        query =
//                            childFilterList?.get(position)?.query?.query?.value +
//                                    query.substring(
//                                        IntRange(
//                                            query.indexOfFirst { it == ':' },
//                                            query.length - 1
//                                        )
//                                    )?.replace('$', ':')
//                        query =  query.replace(query,":category:PF011304:category:PF011307:category:PF011306:category:FM01")

//                    }


                } else {

                    if (isFirstTime) {
//                        query = ""

                        query =
                            query.replace(
                                childFilterList?.get(position)?.query?.query?.value?.replace(
                                    sortSelected,
                                    ""
                                ) ?: "", ""
                            )


//                        isFirstTime = false

                    } else {

                        var filterQuery =
                            childFilterList?.get(position)?.query?.query?.value?.replace(
                                sortSelected,
                                ""
                            ) ?: ""

                        query.forEachIndexed { index, c ->
                            if (skipSecondCharToChangeFromQueryFlag) {
                                if (query[index] == ':') {
                                    query = changeCharInPosition(index, '$', query) ?: ""

                                    skipSecondCharToChangeFromQueryFlag = false
                                }
                            } else {
                                if (query[index] == ':') {
                                    skipSecondCharToChangeFromQueryFlag = true
                                }
                            }

                        }
                        filterQuery.forEachIndexed { index, c ->
                            if (skipSecondCharToChangeFromFilterListQueryFlag) {
                                if (filterQuery[index] == ':') {
                                    filterQuery =
                                        changeCharInPosition(
                                            index,
                                            '$',
                                            filterQuery.replace(
                                                sortSelected,
                                                ""
                                            ) ?: ""
                                        ) ?: ""

                                    skipSecondCharToChangeFromFilterListQueryFlag = false
                                }
                            } else {
                                if (filterQuery[index] == ':'
                                ) {
                                    skipSecondCharToChangeFromFilterListQueryFlag = true
                                }
                            }

                        }


                        var queryList = query.split("$").toMutableList() ?: mutableListOf()
                        var filterQueryList =
                            filterQuery.split("$").toMutableList() ?: mutableListOf()

                        var lastResultQuery = ArrayList<String>()
                        lastResultQuery.addAll(queryList)
                        lastResultQuery.removeAll(filterQueryList)
                        queryList.removeAll(lastResultQuery)


                        query = ""
                        queryList.forEachIndexed { index, s ->
                            if (index >= 1) {
                                query = query + ":" + queryList[index]

                            }
                        }

                    }


                }

                onChecked?.onChecked(query)


            })
        }

        inner class ChildViewHolder(var itemBinding: FilterItemRowBinding) :
            RecyclerView.ViewHolder(itemBinding.root)
    }

    interface IOnChecked {
        fun onChecked(filterQuery: String)
    }

    fun changeCharInPosition(position: Int, ch: Char, str: String): String? {
        val charArray = str.toCharArray()
        charArray[position] = ch
        return String(charArray)
    }

    fun reset() {
        query = ""
        onChecked?.onChecked(query)
    }

    fun difference(str1: String?, str2: String?): String? {
        if (str1 == null) {
            return str2
        }
        if (str2 == null) {
            return str1
        }
        val at: Int = indexOfDifference(str1, str2)
        return if (at == 1) {
            ""
        } else str2.substring(at)
    }

    fun indexOfDifference(cs1: CharSequence?, cs2: CharSequence?): Int {
        if (cs1 === cs2) {
            return 1
        }
        if (cs1 == null || cs2 == null) {
            return 0
        }
        var i: Int
        i = 0
        while (i < cs1.length && i < cs2.length) {
            if (cs1[i] != cs2[i]) {
                break
            }
            ++i
        }
        return if (i < cs2.length || i < cs1.length) {
            i
        } else 1
    }

}

