package com.erabia.foodcrowd.android.common.extension

import android.util.Log
import androidx.lifecycle.ViewModel
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.model.ErrorResponse
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.network.RequestManager
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Response
import java.io.IOException

val errorEvent: SingleLiveEvent<String> by lazy { SingleLiveEvent<String>() }
fun Repository.error(): SingleLiveEvent<String> {
    return errorEvent
}


fun ViewModel.error2(): SingleLiveEvent<String> {
    return errorEvent
}

val progressBarEvent: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }

val hideRefreshLayout: SingleLiveEvent<Boolean> by lazy { SingleLiveEvent<Boolean>() }
fun Repository.progressBarVisibility(): SingleLiveEvent<Int> {

    return progressBarEvent
}

fun Repository.hideRefreshLayout(): SingleLiveEvent<Boolean> {
    return hideRefreshLayout
}


//fun <T> Repository.Observable(): Observable<T> {
//    return Observable<T>.get()
//    return
//    Observable.
//    Observable.su(
//        onSuccess_200: () -> Unit,
//    onSuccess_201: () -> Unit = {},
//    onError_400: () -> Unit = {   kotlin.error().postValue(getResponseErrorMessage(it))
//    }
//    )
//}

//fun <T> Observable<T>.subscribe(
//    onSuccess_200: () -> Unit,
//    onSuccess_201: () -> Unit = {},
//    onError_400: () -> Unit = {
//        error().postValue(getResponseErrorMessage(it))
//    }
//) {
//repository.error()
//}
//

fun <ResponseType : Response<*>?> Repository.getResponseErrorMessage(response: ResponseType): String {
    var errorResponse1: ErrorResponse? = null
    var errorMessage = "Something wrong happened"
    return if (response!!.errorBody() != null) {
        val converter: Converter<ResponseBody?, ErrorResponse> =
            RequestManager.getClient()
                .responseBodyConverter(
                    ErrorResponse::class.java,
                    arrayOfNulls<Annotation?>(0)
                )
        try {
            errorResponse1 = converter.convert(response.errorBody())
        } catch (e1: IOException) {
            Log.d("myError", e1.message ?: "")
        }
        if (errorResponse1?.errors != null && errorResponse1?.errors?.get(0)?.message != null){
            errorMessage = if (errorResponse1?.errors?.get(0)?.type=="JaloObjectNoLongerValidError"){
                errorResponse1?.errors?.get(0)?.type
            }else{
                errorResponse1?.errors?.get(0)?.message ?: "Something wrong happened"
            }
        }
        errorMessage
    } else
        errorMessage
}


fun <ResponseType : Response<*>?> ViewModel.getResponseErrorMessage2(response: ResponseType): String {
    var errorResponse1: ErrorResponse? = null
    var errorMessage = "Something wrong happened"
    return if (response!!.errorBody() != null) {
        val converter: Converter<ResponseBody?, ErrorResponse> =
            RequestManager.getClient()
                .responseBodyConverter(
                    ErrorResponse::class.java,
                    arrayOfNulls<Annotation?>(0)
                )
        try {
            errorResponse1 = converter.convert(response.errorBody())
        } catch (e1: IOException) {
            Log.d("myError", e1.message ?: "")
        }
        if (errorResponse1?.errors != null &&
            errorResponse1?.errors?.get(0)?.message != null
        ) errorMessage =
            errorResponse1?.errors?.get(0)?.message ?: "Something wrong happened"
        errorMessage
    } else errorMessage
}



