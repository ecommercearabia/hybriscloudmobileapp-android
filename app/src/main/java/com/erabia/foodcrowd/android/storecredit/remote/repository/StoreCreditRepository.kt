package com.erabia.foodcrowd.android.storecredit.remote.repository

import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.getResponseErrorMessage
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.storecredit.model.AvailableBalanceResponse
import com.erabia.foodcrowd.android.storecredit.model.StoreCreditListResponse
import com.erabia.foodcrowd.android.storecredit.model.SuperResponse
import com.erabia.foodcrowd.android.storecredit.remote.service.StoreCreditService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import io.reactivex.Observable
import io.reactivex.rxkotlin.subscribeBy
import retrofit2.Response

class StoreCreditRepository(val mStoreCreditService: StoreCreditService) : Repository {
    companion object {
        const val TAG: String = "MyOrderRepository"
    }
    val mStoreCreditListLiveData: SingleLiveEvent<StoreCreditListResponse> by lazy { SingleLiveEvent<StoreCreditListResponse>() }
    val mAvailableBalanceLiveData: SingleLiveEvent<AvailableBalanceResponse> by lazy { SingleLiveEvent<AvailableBalanceResponse>() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun getAvailableBalance() {
        var getAvailableBalanceObservable = mStoreCreditService.getAvailableBalance(
            "current",
            "FULL"
        ).map<SuperResponse.SuccessAval> { SuperResponse.SuccessAval(it) }

        var getStoreHistoryObservable = mStoreCreditService.getStoreHistory(
            "current",
            "FULL"
        ).map<SuperResponse.SuccessStore> { SuperResponse.SuccessStore(it) }

        Observable.zip(
            getAvailableBalanceObservable,
            getStoreHistoryObservable,
            BiFunction { t1: SuperResponse.SuccessAval, t2: SuperResponse.SuccessStore ->

                var pairModel = Pair(t1, t2)
                pairModel
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.first.data.code()) {
                        200, 201, 202 -> {
                            mAvailableBalanceLiveData.value =
                                it.first.data.body()

                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.first.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                    }
                    when (it.second.data.code()) {
                        200, 201, 202 -> {
                            mStoreCreditListLiveData.value =
                                it.second.data.body()
                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                        else-> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }

                    }
                },
                onError = {
                    Log.d(TAG, it.toString())

                }
            )
    }


}