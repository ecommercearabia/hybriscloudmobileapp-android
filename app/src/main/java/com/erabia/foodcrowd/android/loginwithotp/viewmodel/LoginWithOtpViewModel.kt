package com.erabia.foodcrowd.android.loginwithotp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.loginwithotp.remote.repository.LoginWithOtpRepository

class LoginWithOtpViewModel(private val loginWithOtpRepository: LoginWithOtpRepository) : ViewModel() {
    var email = ""
    val sendSuccessEvent = loginWithOtpRepository.sendEmailSuccessEvent
    val navigateToVerificationCode = loginWithOtpRepository.navigateToVerificationCode
    val progressBarVisibility: SingleLiveEvent<Int> = loginWithOtpRepository.progressBarVisibility()
    val error = loginWithOtpRepository.error()
    fun sendOtpCode() {
        loginWithOtpRepository.loginWithOtp(email)
    }

    class Factory(
        private val mLoginWithOtpRepository: LoginWithOtpRepository
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return LoginWithOtpViewModel(mLoginWithOtpRepository) as T
        }

    }
}