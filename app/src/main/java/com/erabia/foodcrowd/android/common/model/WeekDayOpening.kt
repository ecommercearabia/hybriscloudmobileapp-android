package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class WeekDayOpening(
    @SerializedName("closed")
    val closed: Boolean = false,
    @SerializedName("closingTime")
    val closingTime: ClosingTime = ClosingTime(),
    @SerializedName("openingTime")
    val openingTime: OpeningTime = OpeningTime(),
    @SerializedName("weekDay")
    val weekDay: String = ""
)