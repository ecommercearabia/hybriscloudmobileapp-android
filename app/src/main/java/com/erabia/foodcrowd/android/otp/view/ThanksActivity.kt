package com.erabia.foodcrowd.android.otp.view

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.extension.filterRapidClicks
import com.jakewharton.rxbinding3.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import kotlinx.android.synthetic.main.activity_otp.*
import kotlinx.android.synthetic.main.activity_thanks.*
import kotlinx.android.synthetic.main.activity_thanks.mBackImageView

class ThanksActivity : AppCompatActivity() {

    var compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_thanks)
        mStartShoppingButton.clicks().filterRapidClicks().subscribe {
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(compositeDisposable)
        mBackImageView.clicks().filterRapidClicks().subscribe {
            setResult(Activity.RESULT_OK)
            finish()
        }.addTo(compositeDisposable)

    }


    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}