package com.erabia.foodcrowd.android.order.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.error
import com.erabia.foodcrowd.android.common.extension.progressBarVisibility
import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import com.erabia.foodcrowd.android.order.remote.repository.MyOrderRepository

class MyOrderViewModel(val mMyOrderRepository: MyOrderRepository)  : ViewModel() {


    var mOrderListLiveData: LiveData<OrderHistoryListResponse> = mMyOrderRepository.mOrderListLiveData
    var mItemDetails: MutableLiveData<OrderDetailsResponse> = mMyOrderRepository.mItemDetails
    val loadingVisibility: MutableLiveData<Int> = mMyOrderRepository.loadingVisibility
    val mErrorEvent: SingleLiveEvent<String> = mMyOrderRepository.error()
    val progressBarVisibility: SingleLiveEvent<Int> = mMyOrderRepository.progressBarVisibility()

    var onViewOrderClickObserver :MutableLiveData<Int> = MutableLiveData()

//    init {
//        getOrderList()
//    }

    fun getOrderList(){
        mMyOrderRepository.loadOrder()
    }

    fun getItemDetails(mOrderCode:String){
        mMyOrderRepository.getItemDetails(mOrderCode)
    }


    fun onClickOrderView(position: Int){
        onViewOrderClickObserver.postValue(position)
    }


    fun subAmount():String{
       return mItemDetails.value?.subTotal?.formattedValue.toString()
    }





    class Factory(
        val mMyOrderRepository: MyOrderRepository
    ) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MyOrderViewModel(mMyOrderRepository) as T
        }
    }
}