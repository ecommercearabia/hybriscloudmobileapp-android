package com.erabia.foodcrowd.android.common.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.checkout.model.Period
import com.erabia.foodcrowd.android.common.extension.setSafeOnClickListener
import com.erabia.foodcrowd.android.databinding.DeliveryTimeRowItemBinding

class DeliveryTimeAdapter() :
    RecyclerView.Adapter<DeliveryTimeAdapter.ViewHolder>() {

    private var inflter: LayoutInflater? = null
    var slotList: List<Period?>? = null
    private var selectedPosition = -1
        set(value) {
            field = value
            notifyDataSetChanged()
        }
     var preSelectedPosition = -1
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    private var onTimeClickListener: OnTimeClickListener? = null
    lateinit var context: Context
    private var deliveryTimeRowItemBinding: DeliveryTimeRowItemBinding? = null

    var selectedPeriodCode: String? = null
        set(value) {
            field = value
            val selectedTimeSlot = slotList?.singleOrNull() {
                it?.code == value
            }
            preSelectedPosition = slotList?.indexOf(selectedTimeSlot ?: Period()) ?: -1
        }

    fun setListener(onTimeClickListener: OnTimeClickListener?) {
        this.onTimeClickListener = onTimeClickListener
    }

    interface OnTimeClickListener {
        fun onTimeClickListener(slot: Period?)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val itemBinding: DeliveryTimeRowItemBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(context),
                R.layout.delivery_time_row_item,
                parent,
                false
            )
        return ViewHolder(itemBinding)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val slot = slotList!![position]
        holder.itemBinding.textViewDeliveryTime.text = slot?.intervalFormattedValue
//        Log.v("timeSlotValid", slot?.valid.toString())
        if (slot?.enabled == false) {
//            holder.itemBinding.cardViewDeliveryTime.background = context.resources.getDrawable(
//                R.drawable.disable_delivery_time_slot_background,
//                null
//            )
            holder.itemBinding.textViewDeliveryTime.text =
                "${holder.itemBinding.textViewDeliveryTime.text} \n Unavailable"
            holder.itemBinding.textViewDeliveryTime.setTextColor(context.resources.getColor(R.color.timeUnavailableColor))

        }
        holder.itemBinding.cardViewDeliveryTime.setSafeOnClickListener {
            selectedPosition = position
        }

        if (selectedPosition == position) {
            if (slot?.enabled == true) {
                holder.itemBinding.cardViewDeliveryTime.background = context.resources.getDrawable(
                    R.drawable.address_background,
                    null
                )
                onTimeClickListener?.onTimeClickListener(slot)
            }
        } else {
            if (slot?.enabled == true) {
                holder.itemBinding.cardViewDeliveryTime.background = context.resources.getDrawable(
                    R.drawable.cardview_background, null
                )
            }
        }

        if (preSelectedPosition == position) {
            if (slot?.enabled == true) {
                holder.itemBinding.cardViewDeliveryTime.background = context.resources.getDrawable(
                    R.drawable.address_background,
                    null
                )
            }
        } else {
            holder.itemBinding.cardViewDeliveryTime.background = context.resources.getDrawable(
                R.drawable.cardview_background, null
            )
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemCount(): Int {
        return slotList?.size ?: 0
    }

    class ViewHolder(val itemBinding: DeliveryTimeRowItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root)

}