package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class MinPrice(
    @SerializedName("currencyIso")
    val currencyIso: String = "",
    @SerializedName("value")
    val value: Double = 0.0
)