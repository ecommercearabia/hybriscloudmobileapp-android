package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

class Discount {
    @SerializedName("discountPrice")
    val discountPrice: DiscountPrice? = null

    @SerializedName("percentage")
    val percentage: Int? = null

    @SerializedName("price")
    val price: Price? = null

    @SerializedName("saving")
    val saving: Saving? = null
}