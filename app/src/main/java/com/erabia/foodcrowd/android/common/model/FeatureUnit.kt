package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class FeatureUnit(
    @SerializedName("name")
    val name: String = "",
    @SerializedName("symbol")
    val symbol: String = "",
    @SerializedName("unitType")
    val unitType: String = ""
)