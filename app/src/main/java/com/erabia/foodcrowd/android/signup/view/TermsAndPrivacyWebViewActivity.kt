package com.erabia.foodcrowd.android.signup.view

import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.util.LocaleHelper
import kotlinx.android.synthetic.main.fragment_terms_and_condition.*
import java.util.*

class TermsAndPrivacyWebViewActivity : AppCompatActivity() {
    lateinit var toolbar: Toolbar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_and_privacy_web_view)
        when (LocaleHelper.getSummary(this)) {
            Constants.ENGLISH -> {
                MyApplication.getInstance()?.changeLocale("en");
                changeLocale("en")
            }
            Constants.ARABIC -> {
                MyApplication.getInstance()?.changeLocale("ar");
                changeLocale("ar")
            }
        }

        toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val url = intent.getStringExtra("url")

        toolbar.setNavigationOnClickListener {
            finish()
        }

        progress_bar?.visibility = View.VISIBLE
        webView.webViewClient = MyWebView()
        webView.settings.javaScriptEnabled = true
        webView.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK

        url?.let { webView.loadUrl(it) }

    }

    private inner class MyWebView : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            val url: String = request?.url.toString();
            view?.loadUrl(url)
            return true
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            super.onPageStarted(view, url, favicon)
            progress_bar?.visibility = View.VISIBLE
        }

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            progress_bar?.visibility = View.GONE

            webView?.loadUrl(
                "javascript:(function() { " +
                        "var head = document.getElementsByTagName('header')[0].style.display='none'; " +
                        "var head = document.getElementsByTagName('footer')[0].style.display='none'; " +
                        "})()"
            )
//            layoutView.web_view_about_us.loadUrl("javascript:document.getElementsByClassName('content-page-design').style.display ='none';");

        }

    }

    override fun onResume() {
        super.onResume()
        toolbar.title = ""
    }

    override fun onStop() {
        super.onStop()
        toolbar.title = ""
    }

    fun changeLocale(lang: String?) {
        val mLocale = Locale(lang)
        val config = this.resources
            ?.configuration
        Locale.setDefault(mLocale)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config?.setLocale(mLocale)
        } else config?.locale = mLocale
        this?.resources?.updateConfiguration(config, this?.resources?.displayMetrics)
    }
}