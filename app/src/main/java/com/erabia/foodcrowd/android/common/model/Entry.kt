package com.erabia.foodcrowd.android.common.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


data class Entry(
    @SerializedName("basePrice")
    @Expose
    var basePrice: BasePrice? = null,

    @SerializedName("cancellableQuantity")
    @Expose
    var cancellableQuantity: Int? = null,

    @SerializedName("cancelledItemsPrice")
    @Expose
    var cancelledItemsPrice: CancelledItemsPrice? = null,

    @SerializedName("configurationInfos")
    @Expose
    var configurationInfos: List<ConfigurationInfo>? = null,

    @SerializedName("deliveryMode")
    @Expose
    var deliveryMode: DeliveryMode? = null,

    @SerializedName("deliveryPointOfService")
    @Expose
    var deliveryPointOfService: DeliveryPointOfService? = null,

    @SerializedName("entryNumber")
    @Expose
    var entryNumber: Int? = null,

    @SerializedName("product")
    @Expose
    var product: Product? = null,

    @SerializedName("quantity")
    @Expose
    var quantity: Int? = null,

    @SerializedName("quantityAllocated")
    @Expose
    var quantityAllocated: Int? = null,

    @SerializedName("quantityCancelled")
    @Expose
    var quantityCancelled: Int? = null,

    @SerializedName("quantityPending")
    @Expose
    var quantityPending: Int? = null,

    @SerializedName("quantityReturned")
    @Expose
    var quantityReturned: Int? = null,

    @SerializedName("quantityShipped")
    @Expose
    var quantityShipped: Int? = null,

    @SerializedName("quantityUnallocated")
    @Expose
    var quantityUnallocated: Int? = null,

    @SerializedName("returnableQuantity")
    @Expose
    var returnableQuantity: Int? = null,

    @SerializedName("returnedItemsPrice")
    @Expose
    var returnedItemsPrice: ReturnedItemsPrice? = null,

    @SerializedName("totalPrice")
    @Expose
    var totalPrice: TotalPrice? = null,

    @SerializedName("updateable")
    @Expose
    var updateable: Boolean? = null,

    @SerializedName("url")
    @Expose
    var url: String? = null

) : Parcelable {
    constructor(parcel: Parcel) : this(
        TODO("basePrice"),
        parcel.readInt(),
        TODO("cancelledItemsPrice"),
        TODO("configurationInfos"),
        TODO("deliveryMode"),
        TODO("deliveryPointOfService"),
        parcel.readInt(),
        TODO("product"),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt(),
        TODO("returnedItemsPrice"),
        TODO("totalPrice"),
        parcel.readByte() != 0.toByte(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        TODO("Not yet implemented")
    }

    override fun describeContents(): Int {
        TODO("Not yet implemented")
    }

    companion object CREATOR : Parcelable.Creator<Entry> {
        override fun createFromParcel(parcel: Parcel): Entry {
            return Entry(parcel)
        }

        override fun newArray(size: Int): Array<Entry?> {
            return arrayOfNulls(size)
        }
    }
}