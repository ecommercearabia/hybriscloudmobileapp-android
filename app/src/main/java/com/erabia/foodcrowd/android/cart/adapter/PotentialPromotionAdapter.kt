package com.erabia.foodcrowd.android.cart.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.cart.viewmodel.CartViewModel
import com.erabia.foodcrowd.android.common.model.AppliedVoucher
import com.erabia.foodcrowd.android.common.model.BankOfferResponse
import com.erabia.foodcrowd.android.common.model.PotentialOrderPromotion
import com.erabia.foodcrowd.android.databinding.RowPromoCodeBinding
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.row_potential_promtion.view.*

class PotentialPromotionAdapter() :
    RecyclerView.Adapter<PotentialPromotionAdapter.ssViewHolder>() {


//    lateinit var binding: RowPotentialPromtionBinding
    var potentialPromotionList: MutableList<PotentialOrderPromotion>? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ssViewHolder {
//        binding = DataBindingUtil.inflate(
//            LayoutInflater.from(parent.context),
//            R.layout.row_potential_promtion,
//            parent,
//            false
//        )
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_potential_promtion, parent, false)
        return ssViewHolder(v)

    }

    override fun getItemCount(): Int {
        return potentialPromotionList?.size ?: 0
    }

    override fun onBindViewHolder(holder: ssViewHolder, position: Int) {
        holder.itemBinding.textView_potentialPromotionValue.text = potentialPromotionList?.get(position)?.description?:""
//        holder.itemBinding.textViewPotentialPromotionValue.text = potentialPromotionList?.get(position)?.description?:""
    }


    class ssViewHolder(var itemBinding: View) :
        RecyclerView.ViewHolder(itemBinding)


}