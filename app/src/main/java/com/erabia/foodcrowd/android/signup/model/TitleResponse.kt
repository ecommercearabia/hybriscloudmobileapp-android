package com.erabia.foodcrowd.android.signup.model


import com.google.gson.annotations.SerializedName

data class TitleResponse(
    @SerializedName("titles")
    val titles: ArrayList<Title>? = arrayListOf()
) {
    data class Title(
        @SerializedName("code")
        val code: String? = "",
        @SerializedName("name")
        val name: String? = ""


    ){
        override fun toString(): String {
            return name ?: ""
        }

    }
}