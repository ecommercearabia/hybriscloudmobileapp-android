package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PaymentDetailsRequest(
    @SerializedName("accountHolderName")
    var accountHolderName: String = "",
    @SerializedName("billingAddress")
    var billingAddress: Address = Address(),
    @SerializedName("cardNumber")
    var cardNumber: String = "",
    @SerializedName("cardType")
    var cardType: CardType = CardType(),
    @SerializedName("defaultPayment")
    var defaultPayment: Boolean = false,
    @SerializedName("expiryMonth")
    var expiryMonth: String = "",
    @SerializedName("expiryYear")
    var expiryYear: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("issueNumber")
    var issueNumber: String = "",
    @SerializedName("saved")
    var saved: Boolean = false,
    @SerializedName("startMonth")
    var startMonth: String = "",
    @SerializedName("startYear")
    var startYear: String = "",
    @SerializedName("subscriptionId")
    var subscriptionId: String = ""
)