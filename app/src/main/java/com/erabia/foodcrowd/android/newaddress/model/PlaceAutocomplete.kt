package com.erabia.foodcrowd.android.newaddress.model

data class PlaceAutocomplete internal constructor(
        var placeId: CharSequence = "",
        var area: CharSequence = "",
        var address: CharSequence = ""
    ) 