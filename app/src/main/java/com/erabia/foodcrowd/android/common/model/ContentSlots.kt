package com.erabia.foodcrowd.android.common.model


import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.TypeConverters
import com.erabia.foodcrowd.android.common.util.DataConverter
import com.google.gson.annotations.SerializedName

//@Entity
data class ContentSlots(
    @TypeConverters(DataConverter::class)
    @SerializedName("contentSlot")
    @Ignore
    val contentSlot: List<ContentSlot> = listOf()
)