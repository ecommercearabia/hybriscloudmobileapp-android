package com.erabia.foodcrowd.android.order.remote.service

import com.erabia.foodcrowd.android.order.model.OrderDetailsResponse
import com.erabia.foodcrowd.android.order.model.OrderHistoryListResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MyOrderService {

    @GET("rest/v2/foodcrowd-ae/users/{userId}/orders")
    fun getOrder(
        @Path("userId") userId: String,
        @Query("currentPage") currentPage: Int,
        @Query("pageSize") pageSize: Int,
        @Query("fields") fields: String
    ): Observable<Response<OrderHistoryListResponse>>



    @GET("rest/v2/foodcrowd-ae/users/{userId}/orders/{code}")
    fun getDetailsOrder(
        @Path("userId") userId: String,
        @Path("code") code: String,
        @Query("fields") fields: String
    ): Observable<Response<OrderDetailsResponse>>






}