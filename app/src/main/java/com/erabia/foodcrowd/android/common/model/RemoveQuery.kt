package com.erabia.foodcrowd.android.common.model

data class RemoveQuery(
    val query: Query? = null,
    val url: String? = null
)