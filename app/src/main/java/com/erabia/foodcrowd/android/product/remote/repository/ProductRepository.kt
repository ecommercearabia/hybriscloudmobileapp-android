package com.erabia.foodcrowd.android.product.remote.repository

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.MyApplication
import com.erabia.foodcrowd.android.common.SingleLiveEvent
import com.erabia.foodcrowd.android.common.extension.*
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.ProductReference
import com.erabia.foodcrowd.android.common.repository.Repository
import com.erabia.foodcrowd.android.personaldetails.remote.repository.PersonalDetailsRepository
import com.erabia.foodcrowd.android.product.model.AddReviewPostData
import com.erabia.foodcrowd.android.product.model.AddReviewResponse
import com.erabia.foodcrowd.android.product.model.ReviewsResponse
import com.erabia.foodcrowd.android.product.model.ZipModelReviewProductResponse
import com.erabia.foodcrowd.android.product.remote.service.ProductService
import com.google.firebase.analytics.FirebaseAnalytics
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class ProductRepository(val productService: ProductService) : Repository {
    val productReferenceLiveData: MutableLiveData<List<ProductReference>> by lazy { MutableLiveData<List<ProductReference>>() }
    val youMayAlsoLikeVisibility: SingleLiveEvent<Int> by lazy { SingleLiveEvent<Int>() }
    val mProductLiveData: SingleLiveEvent<Product> by lazy { SingleLiveEvent<Product>() }
    val productReviewLiveData: SingleLiveEvent<ReviewsResponse> by lazy { SingleLiveEvent<ReviewsResponse>() }
    val addReviewResponseLiveData: SingleLiveEvent<AddReviewResponse> by lazy { SingleLiveEvent<AddReviewResponse>() }
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()

    companion object {
        const val TAG: String = "ProductRepository"
    }


    @SuppressLint("CheckResult")
    fun loadProduct(productCode: String): LiveData<Product> {
        val liveData = MutableLiveData<Product>()
        productService.getProductDetails(
            productCode, Constants.FIELDS_FULL
        ).get().subscribe(this, onSuccess_200 = {
            liveData.value = it.body()

            val mFirebaseAnalytics =  FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.body()?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.body()?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.body()?.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }

            val viewItemParams = Bundle()
            it.body()?.price?.currencyIso?.let { it1 -> viewItemParams.putString(
                FirebaseAnalytics.Param.CURRENCY, it1 ?: "") }
            it.body()?.price?.value?.let { it1 -> viewItemParams.putDouble(
                FirebaseAnalytics.Param.VALUE, it1 ?: 0.0) }
            viewItemParams.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(productBundle))

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, viewItemParams)

            productReferenceLiveData.value = it.body()?.productReferences
            if (it.body()?.productReferences?.size == 0) {
                youMayAlsoLikeVisibility.value = View.GONE
            } else {
                youMayAlsoLikeVisibility.value = View.VISIBLE
            }
        },onSuccess_201 = {
            liveData.value = it.body()

            val mFirebaseAnalytics =  FirebaseAnalytics.getInstance(MyApplication.getContext()!!)

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.body()?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.body()?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.body()?.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }

            val viewItemParams = Bundle()
            it.body()?.price?.currencyIso?.let { it1 -> viewItemParams.putString(
                FirebaseAnalytics.Param.CURRENCY, it1 ?: "") }
            it.body()?.price?.value?.let { it1 -> viewItemParams.putDouble(
                FirebaseAnalytics.Param.VALUE, it1 ?: 0.0) }
            viewItemParams.putParcelableArray(FirebaseAnalytics.Param.ITEMS, arrayOf(productBundle))

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM, viewItemParams)

            productReferenceLiveData.value = it.body()?.productReferences
            if (it.body()?.productReferences?.size == 0) {
                youMayAlsoLikeVisibility.value = View.GONE
            } else {
                youMayAlsoLikeVisibility.value = View.VISIBLE
            }
        })
        return liveData
    }


    @SuppressLint("CheckResult")
    fun loadReviews(productCode: String) {
        progressBarVisibility().value = (View.VISIBLE)
        productService.getReviewDetails(
            productCode, Constants.FIELDS_FULL, 20
        ).get()
            .subscribe(
                {
                    when (it.code()) {
                        200, 201 -> {
                            productReviewLiveData.value = it.body()
                            progressBarVisibility().value = (View.GONE)
                        }
                        400, 401, 404 -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }

                },
                {
                    Log.d(ProductRepository.TAG, it.toString())
                }
            )
    }


    @SuppressLint("CheckResult")
    fun addReview(productCode: String, addReviewPostData: AddReviewPostData) {
        productService.addReview(
            productCode, Constants.FIELDS_FULL, addReviewPostData
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribe(
                {
                    when (it.code()) {
                        200, 201, 202 -> {
                            addReviewResponseLiveData.value = it.body()
                        }
                        400 -> {
                            error().postValue(getResponseErrorMessage(it))

                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it))
                        }
                    }

                },
                {
                    Log.d(ProductRepository.TAG, it.toString())
                }
            )
    }

    @SuppressLint("CheckResult")
    fun getProductAndReview(productCode: String) {
        var getCustomerProfileObservable = productService.getProductDetails(
            productCode, Constants.FIELDS_FULL
        ).map<ZipModelReviewProductResponse.SuccessProduct> {
            ZipModelReviewProductResponse.SuccessProduct(it)
        }

        var getLocalizedTitleObservable = productService.getReviewDetails(
            productCode, Constants.FIELDS_FULL, 20
        ).map<ZipModelReviewProductResponse.SuccessReviewList> {
            ZipModelReviewProductResponse.SuccessReviewList(it)
        }

        Observable.zip(
            getCustomerProfileObservable,
            getLocalizedTitleObservable,

            BiFunction { t1: ZipModelReviewProductResponse.SuccessProduct, t2: ZipModelReviewProductResponse.SuccessReviewList ->
                var pairModel = Pair(t1, t2)
                pairModel
            })
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { loadingVisibility.value = View.VISIBLE }
            .doOnTerminate { loadingVisibility.value = View.GONE }
            .subscribeBy(
                onNext =
                {
                    when (it.first.data.code()) {
                        200, 201, 202 -> {
                            mProductLiveData.value =
                                it.first.data.body()

                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.first.data))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it.first.data))
                        }
                    }
                    when (it.second.data.code()) {
                        200, 201, 202 -> {
                            productReviewLiveData.value =
                                it.second.data.body()

                        }
                        400, 401, 403 -> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                        else -> {
                            error().postValue(getResponseErrorMessage(it.second.data))
                        }
                    }

                },
                onError = {
                    Log.d(PersonalDetailsRepository.TAG, it.toString())

                }
            )
    }


}