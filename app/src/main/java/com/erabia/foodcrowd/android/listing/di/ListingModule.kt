package com.erabia.foodcrowd.android.listing.di

import com.erabia.foodcrowd.android.cart.remote.repository.CartRepository
import com.erabia.foodcrowd.android.cart.remote.service.CartService
import com.erabia.foodcrowd.android.listing.remote.repository.ListingRepository
import com.erabia.foodcrowd.android.listing.remote.service.ListingService
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import com.erabia.foodcrowd.android.network.RequestManager
import dagger.Module
import dagger.Provides

@Module
class ListingModule {

    @Provides
    fun listingRepository() =
        ListingRepository(RequestManager.getClient().create(ListingService::class.java))

    @Provides
    fun cartRepository() =
        CartRepository(RequestManager.getClient().create(CartService::class.java))

    @Provides
    fun factory(listingRepository: ListingRepository, cartRepository: CartRepository) =
        ListingViewModel.Factory(listingRepository, cartRepository)
}