package com.erabia.foodcrowd.android.checkout.model


import com.google.gson.annotations.SerializedName

data class TimeSlotDay(
    @SerializedName("date")
    val date: String = "",
    @SerializedName("day")
    val day: String = "",
    @SerializedName("periods")
    val periods: List<Period> = listOf()
)