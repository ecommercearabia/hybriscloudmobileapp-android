package com.erabia.foodcrowd.android.newaddress.view

import android.location.Address
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.common.util.DialogData
import com.erabia.foodcrowd.android.databinding.FragmentAddAddress2Binding
import com.erabia.foodcrowd.android.main.MainActivity
import com.erabia.foodcrowd.android.network.RequestManager
import com.erabia.foodcrowd.android.newaddress.adapter.PlacesAutoCompleteAdapter
import com.erabia.foodcrowd.android.newaddress.model.AreaListResponse
import com.erabia.foodcrowd.android.newaddress.model.CityListResponse
import com.erabia.foodcrowd.android.newaddress.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.newaddress.remote.service.AddressService
import com.erabia.foodcrowd.android.newaddress.viewmodel.AddAddressViewModel
import com.erabia.foodcrowd.android.signup.model.CountryResponse
import com.erabia.foodcrowd.android.signup.model.TitleResponse
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import io.reactivex.disposables.CompositeDisposable


class AddAddressFragment : Fragment(), PlacesAutoCompleteAdapter.HasPlacesNotifiedClickListener {
    companion object {
        const val TAG = "AddAddressFragment"
    }

    val compositeDisposable = CompositeDisposable()
    lateinit var mViewModel: AddAddressViewModel
    lateinit var binding: FragmentAddAddress2Binding
    var geoAddress: Address? = null
    var mComeFrom = ""
    var dummyTitleList: List<TitleResponse.Title> = listOf(TitleResponse.Title("", "Title"))
    var dummyCountryList: List<CountryResponse.Country> =
        listOf(CountryResponse.Country("", "", "Country"))
    var dummyCityList: List<CityListResponse.City> =
        listOf(CityListResponse.City(emptyList(), "", "City"))
    var dummyAreaList: List<AreaListResponse.Area> = listOf(AreaListResponse.Area("", "Area"))
    private var mAutoCompleteAdapter: PlacesAutoCompleteAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mComeFrom = arguments?.getString(Constants.COME_FROM).toString()

        val factory = AddAddressViewModel.Factory(
            AddressRepository(
                RequestManager.getClient().create(AddressService::class.java)
            )
        )
        mViewModel = ViewModelProvider(this, factory).get(AddAddressViewModel::class.java)
        //TODO add apiKey
        context?.applicationContext?.let {

            // Initialize the SDK
            Places.initialize(it, Constants.PLACE_API_KE)

            // Create a new PlacesClient instance
            val placesClient: PlacesClient = Places.createClient(requireContext())
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_add_address2, container, false)
        try {
            geoAddress = arguments?.get("geo") as Address
            geoAddress?.let { it -> it.getAddressLine(0)?.let { mViewModel.setAdminArea(it) } }
            geoAddress?.let { it ->
                mViewModel.setCityAndAreaFromMap(
                    it.locality ?: "",
                    it.subLocality ?: ""
                )
            }

        } catch (e: Exception) {
            Log.d(TAG, "onCreateView: ${e.message}")
        }

        mAutoCompleteAdapter = PlacesAutoCompleteAdapter(requireContext())

        initStreetNameTextWatcher()
        initAutoCompletePlace()
        initKeyboardEnterClickButton()
        binding.lifecycleOwner = this
        binding.viewModel = mViewModel
        mViewModel.onSelectItemCountry2()

        return binding.root
    }

    private fun initKeyboardEnterClickButton() {
        binding.streetNamePlaceSearch.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                mAutoCompleteAdapter?.filter?.filter(null)
                binding.placesRecyclerView.visibility = View.GONE
            }
            false
        }
    }

    private fun initAutoCompletePlace() {
        binding.placesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        mAutoCompleteAdapter?.clickListener = object :
            PlacesAutoCompleteAdapter.ClickListener {
            override fun click(place: String?) {
                binding.streetNamePlaceSearch.setText(place)
                binding.placesRecyclerView.visibility = View.GONE
            }

        }
        mAutoCompleteAdapter?.hasPlacesNotifiedClickListener = this
        binding.placesRecyclerView.adapter = mAutoCompleteAdapter
        mAutoCompleteAdapter?.notifyDataSetChanged()

    }

    private fun initStreetNameTextWatcher() {
        binding.streetNamePlaceSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                Log.d(TAG, "onTextChanged: ")
            }

            override fun afterTextChanged(s: Editable?) {
                if (s.toString() != "") {
                    mAutoCompleteAdapter?.filter?.filter(s.toString())
                    if (binding.placesRecyclerView.visibility == View.GONE) {
                        binding.placesRecyclerView.visibility = View.VISIBLE
                    }
                } else {
                    if (binding.placesRecyclerView.visibility == View.VISIBLE) {
                        binding.placesRecyclerView.visibility = View.VISIBLE
                        binding.placesRecyclerView.visibility = View.GONE
                    }
                }
            }

        })

    }

    override fun onResume() {
        super.onResume()
        mViewModel.getAllDataSpinner()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.mComeFrom = mComeFrom
        var titleArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyTitleList
                )
            }

        var countryArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyCountryList
                )
            }
        var cityArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyCityList
                )
            }
        var areaArrayAdapter =
            context?.let {
                ArrayAdapter(
                    it,
                    R.layout.item_spinner,
                    R.id.mSpinnerTextView,
                    dummyAreaList
                )
            }


        binding.mTitleSpinner.adapter = titleArrayAdapter
        binding.mCountrySpinner.adapter = countryArrayAdapter
        binding.mCitySpinner.adapter = cityArrayAdapter
//        binding.mAreaSpinner.adapter = areaArrayAdapter
        observeAddAddress()
        observeContainerLayoutClickEvent()
        observerPersonalDetails()
        observerEnableButton()
        observerError()
        observeNavigateToMap()
        observeListAreaSelected()
        observeAreaSelected()
        loadingVisibilityObserver()

    }

    fun loadingVisibilityObserver() {
        mViewModel.loadingVisibility.observe(viewLifecycleOwner, Observer {
            if (it == View.VISIBLE) {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.setFreezAndVisiable((activity as MainActivity))
            }
            else {
                (activity as MainActivity).binding.progressBar.visibility=it
                (activity as MainActivity).binding.progressBar.hideFreezAndVisiableProgress((activity as MainActivity))
            }
        })
    }


    private fun observeAreaSelected() {
        mViewModel.mAreaSelection.observe(viewLifecycleOwner, Observer {
            binding.editTextArea.setText(it.name ?: "")
        })
    }

    private fun observeListAreaSelected() {
        mViewModel.areaListForPickerLiveData.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                val list = arrayListOf<AreaListResponse.Area>()
                it.areas?.forEach { area ->
                    if (area != null)
                        list.add(area)
                }
                DialogData.getAreaDialog(context, mViewModel, null, list)
            }
        })
    }

    fun observerPersonalDetails() {
        mViewModel.mPersonalDetailsLiveData.observe(viewLifecycleOwner, Observer {
            binding.mPersonalDetailsModel = it

            val pos =
                mViewModel.mTitleListLiveData.value?.titles?.indexOfFirst { title ->
                    title.code == it?.titleCode
                }
            binding.mTitleSpinner.setSelection(pos ?: 0)
        })
    }

    fun observerError() {

        mViewModel.mErrorEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)

        })

        mViewModel.mErrorAreaAndCreateEvent.observe(viewLifecycleOwner, Observer {
            AppUtil.showToastyError(context , it)
        })
    }

    fun observerEnableButton() {
        mViewModel.mEnableButton.observe(viewLifecycleOwner, Observer {
            binding.mSaveNewAddressButton.isEnabled = it
        })
    }

    private fun observeAddAddress() {
        mViewModel.mAddAddressLiveEvent.observe(
            viewLifecycleOwner,
            Observer {
                findNavController().popBackStack()
            })
    }

    private fun observeContainerLayoutClickEvent() {
        mViewModel.resetAndHideAdapter.observe(viewLifecycleOwner, Observer {
            mAutoCompleteAdapter?.filter?.filter(null)
            binding.placesRecyclerView.visibility = View.GONE
        })
    }

    private fun observeNavigateToMap() {
        mViewModel.mNavigateToMap.observe(
            viewLifecycleOwner,
            Observer {
//                val action =
//                    AddAddressFragmentDirections.actionNavigateToMapFragment(
//                        mComeFrom, "add"
//                    )
//
//                findNavController().navigate(action)
            })
    }

    override fun hasPlacesNotified(hasNotified: Boolean) {
        if (hasNotified) {
            Log.d(TAG, "hasItemsShown: $hasNotified")
            Log.d(TAG, "hasItemsShown: recyclerView ${binding.placesRecyclerView.height}")
            binding.scrollView.smoothScrollTo(0, binding.mStreetNameTextInputLayout.top)
        }
    }


}