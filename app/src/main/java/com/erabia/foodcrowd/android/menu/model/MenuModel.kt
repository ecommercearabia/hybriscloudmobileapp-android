package com.erabia.foodcrowd.android.menu.model


class MenuModel {
    var text: String = ""
    var list: MutableList<PreparingMenuModel> = ArrayList<PreparingMenuModel>()

    constructor(text: String, list: MutableList<PreparingMenuModel>) {
        this.text = text
        this.list.addAll(list)
//    data class Menu(
//        val itemName: String? = "",
//        val itemimage: Int? = null
//    )

//    data class Title(
//        val titleName: String? = ""
//
//    )


    }
}