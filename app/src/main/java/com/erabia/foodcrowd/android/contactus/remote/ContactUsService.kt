package com.erabia.foodcrowd.android.contactus.remote

import com.erabia.foodcrowd.android.checkout.model.*
import com.erabia.foodcrowd.android.checkout.model.PaymentInfo
import com.erabia.foodcrowd.android.common.model.*
import com.erabia.foodcrowd.android.contactus.model.ContactUsResponse
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import retrofit2.http.Query

interface ContactUsService {
    @GET("rest/v2/foodcrowd-ae/cms/components/{componentId}")
    fun getContactUsDetails(
        @Path("componentId") componentId: String,
        @Query("fields") fields: String
    ): Observable<Response<ContactUsResponse>>

}