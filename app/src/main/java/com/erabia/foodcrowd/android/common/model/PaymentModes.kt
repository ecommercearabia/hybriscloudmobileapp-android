package com.erabia.foodcrowd.android.common.model


import com.google.gson.annotations.SerializedName

data class PaymentModes(
    @SerializedName("paymentModes")
    var paymentModes: List<PaymentMode> = listOf()
)