package com.erabia.foodcrowd.android.listing.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.erabia.foodcrowd.android.R
import com.erabia.foodcrowd.android.category.viewmodel.CategoryViewModel
import com.erabia.foodcrowd.android.common.Constants
import com.erabia.foodcrowd.android.common.model.Facet
import com.erabia.foodcrowd.android.common.model.Product
import com.erabia.foodcrowd.android.common.model.Sort
import com.erabia.foodcrowd.android.common.util.AppUtil
import com.erabia.foodcrowd.android.databinding.FragmentListingBinding
import com.erabia.foodcrowd.android.listing.adapter.ListingAdapter
import com.erabia.foodcrowd.android.listing.di.DaggerIListingComponent
import com.erabia.foodcrowd.android.listing.di.IListingComponent
import com.erabia.foodcrowd.android.listing.viewmodel.ListingViewModel
import com.google.firebase.analytics.FirebaseAnalytics
import javax.inject.Inject


class ListingFragment : Fragment(), SortDialogFragment.IOnClick, ListingViewModel.IFilter,
    ListingAdapter.IListingItemClick {

    val daggerComponent: IListingComponent by lazy {
        DaggerIListingComponent.builder().build()
    }

    init {
        daggerComponent.inject(this)
    }

    lateinit var listingViewModel: ListingViewModel
    lateinit var binding: FragmentListingBinding
    lateinit var listingAdapter: ListingAdapter
    private var categoryId: String? = null
    private var currentPage = 0
    private lateinit var facetList: List<Facet>
    private lateinit var sortList: List<Sort>
    var lastPage = 0

    @Inject
    lateinit var factory: ListingViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        listingViewModel = ViewModelProvider(this, factory).get(ListingViewModel::class.java)

        arguments.let { categoryId = it?.getString(Constants.API_KEY.CATEGORY_ID) }
        listingViewModel.filter = this


    }
    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }

    override fun onStop() {
        super.onStop()
        (activity as AppCompatActivity?)!!.supportActionBar!!.title = ""
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_listing, container, false)
//        listingViewModel = ViewModelProvider(this, factory).get(ListingViewModel::class.java)

        listingAdapter = ListingAdapter(listingViewModel)
        listingAdapter.setHasStableIds(true)
        this.categoryId?.let {
            listingViewModel.getProductList(
                it,
                currentPage,
                ":relevance:category:".plus(categoryId),true, "listing"
            )
        }
        listingAdapter.onListingItemClick = this

        binding.recyclerViewProduct.adapter = listingAdapter
        observeProductList()
        buttonClicks()
        pagination()
        observeAddToCart()
        return binding.root
    }

    private fun buttonClicks() {
        binding.buttonSortBy.setOnClickListener {
            val sortDialogFragment = SortDialogFragment(sortList)
            sortDialogFragment.show(childFragmentManager, "")
            sortDialogFragment.onClick = this
        }

        binding.buttonFilter.setOnClickListener {
            findNavController().navigate(
                R.id.action_navigate_to_filter,
                bundleOf(Constants.INTENT_KEY.FACETS to facetList)
            )


        }
    }

    private fun observeProductList() {
        listingViewModel.productList.observe(viewLifecycleOwner, Observer {
            listingAdapter.refresh(it.products as MutableList<Product>)
            facetList = it.facets ?: ArrayList()
            sortList = it.sorts ?: ArrayList()
            lastPage = it.pagination?.totalPages ?: 0
            listingAdapter.totalResult = it.pagination?.totalResults ?: 0
        })
    }

    fun pagination() {

        binding.recyclerViewProduct.addOnScrollListener(object : OnScrollListener() {
            override fun onScrollStateChanged(
                recyclerView: RecyclerView,
                newState: Int
            ) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(
                recyclerView: RecyclerView,
                dx: Int,
                dy: Int
            ) {
                super.onScrolled(recyclerView, dx, dy)
                val layoutManager =
                    recyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = layoutManager.itemCount
                Log.d("total", totalItemCount.toString() + "")
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                Log.d("total_last", lastVisibleItem.toString() + "")
                if ((lastVisibleItem == totalItemCount - 1) && (currentPage < lastPage - 1)) {
                    currentPage++
                    Log.d("current", currentPage.toString())
                    this@ListingFragment.categoryId?.let {
                        listingViewModel.getProductList(
                            it,
                            currentPage,
                            ":relevance:category:".plus(categoryId),false, "listing"
                        )
                    }
                    observeProductList()
                }
            }
        })
    }

    override fun onClick(code: String) {
        Log.d("sort_code", code)
        listingAdapter.reset()
        currentPage = 0

        this.categoryId?.let {
            listingViewModel.sort = code
            listingViewModel.getProductList(
                it,
                currentPage,
                ":relevance:category:".plus(categoryId),true, "listing"
            )
        }
        observeProductList()
    }

    override fun filter(query: String) {
        Log.d("listing_query", query)
        listingAdapter.reset()
        currentPage = 0
        this.categoryId?.let {
            listingViewModel.sort = ""
            listingViewModel.getProductList(
                it,
                currentPage,
                query,true,"listing"
            )
        }
    }

    override fun onListingItemClick(key: String, productCode: String) {

        if (key.equals("onAllList")) {
            findNavController().navigate(
                R.id.action_navigate_to_details,
                bundleOf(Constants.INTENT_KEY.PRODUCT_CODE to productCode)
            )
        }

    }

    private fun observeAddToCart() {
        listingViewModel.addToCartSuccess.observe(viewLifecycleOwner, Observer {
            val mFirebaseAnalytics =  FirebaseAnalytics.getInstance(requireContext())

            val productBundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, it.entry.product?.code ?: "")
                putString(FirebaseAnalytics.Param.ITEM_NAME, it.entry.product?.name ?: "")
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, null)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, null)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, null)
                it.entry.product?.price?.value?.let { it1 -> putDouble(FirebaseAnalytics.Param.PRICE, it1 ?: 0.0) }
            }


            val addToCartParams = Bundle()

            it.entry.product?.price?.currencyIso?.let { it1 -> addToCartParams.putString(
                FirebaseAnalytics.Param.CURRENCY, it1 ?: "") }
            it.entry.product?.price?.value?.let { it1 -> addToCartParams.putDouble(FirebaseAnalytics.Param.VALUE, it1 ?: 0.0) }

            addToCartParams.putParcelableArray(
                FirebaseAnalytics.Param.ITEMS,
                arrayOf(productBundle)
            )

            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART, addToCartParams)
            AppUtil.showToastySuccess(  context ,R.string.added_to_cart_message)

        })
    }
}