package com.erabia.foodcrowd.android.address.di

import com.erabia.foodcrowd.android.address.remote.repository.AddressRepository
import com.erabia.foodcrowd.android.address.view.AddAddressFragment
import com.erabia.foodcrowd.android.address.view.AddressFragment
import com.erabia.foodcrowd.android.address.view.AddressMapFragment
import com.erabia.foodcrowd.android.checkout.view.CheckoutFragment
import com.erabia.foodcrowd.android.signup.remote.repository.SignUpRepository
import dagger.Component

@Component(modules = [AddressModule::class])
interface IAddressComponent {

    fun inject(addressFragment: AddressFragment)

    fun inject(addAddressFragment: AddAddressFragment)

    fun inject(addressMapFragment: AddressMapFragment)

    fun inject(checkoutFragment: CheckoutFragment)

    fun getAddressRepo(): AddressRepository
    fun getSignUpRepo(): SignUpRepository
}